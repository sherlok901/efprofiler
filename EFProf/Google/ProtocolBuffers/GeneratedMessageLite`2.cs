﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.GeneratedMessageLite`2
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Google.ProtocolBuffers
{
  [Serializable]
  public abstract class GeneratedMessageLite<TMessage, TBuilder> : AbstractMessageLite<TMessage, TBuilder> where TMessage : GeneratedMessageLite<TMessage, TBuilder> where TBuilder : GeneratedBuilderLite<TMessage, TBuilder>
  {
    protected abstract TMessage ThisMessage { get; }

    public override sealed string ToString()
    {
      using (StringWriter stringWriter = new StringWriter())
      {
        this.PrintTo((TextWriter) stringWriter);
        return stringWriter.ToString();
      }
    }

    protected static void PrintField<T>(string name, IList<T> value, TextWriter writer)
    {
      foreach (T obj in (IEnumerable<T>) value)
        GeneratedMessageLite<TMessage, TBuilder>.PrintField(name, true, (object) obj, writer);
    }

    protected static void PrintField(string name, bool hasValue, object value, TextWriter writer)
    {
      if (!hasValue)
        return;
      if (value is IMessageLite)
      {
        writer.WriteLine("{0} {{", (object) name);
        ((IMessageLite) value).PrintTo(writer);
        writer.WriteLine("}");
      }
      else if (value is ByteString || value is string)
      {
        writer.Write("{0}: \"", (object) name);
        if (value is string)
          GeneratedMessageLite<TMessage, TBuilder>.EscapeBytes((IEnumerable<byte>) Encoding.UTF8.GetBytes((string) value), writer);
        else
          GeneratedMessageLite<TMessage, TBuilder>.EscapeBytes((IEnumerable<byte>) value, writer);
        writer.WriteLine("\"");
      }
      else if (value is bool)
        writer.WriteLine("{0}: {1}", (object) name, (bool) value ? (object) "true" : (object) "false");
      else if (value is IEnumLite)
        writer.WriteLine("{0}: {1}", (object) name, (object) ((IEnumLite) value).Name);
      else
        writer.WriteLine("{0}: {1}", (object) name, (object) ((IConvertible) value).ToString((IFormatProvider) FrameworkPortability.InvariantCulture));
    }

    private static void EscapeBytes(IEnumerable<byte> input, TextWriter writer)
    {
      foreach (byte num in input)
      {
        switch (num)
        {
          case 7:
            writer.Write("\\a");
            continue;
          case 8:
            writer.Write("\\b");
            continue;
          case 9:
            writer.Write("\\t");
            continue;
          case 10:
            writer.Write("\\n");
            continue;
          case 11:
            writer.Write("\\v");
            continue;
          case 12:
            writer.Write("\\f");
            continue;
          case 13:
            writer.Write("\\r");
            continue;
          case 34:
            writer.Write("\\\"");
            continue;
          case 39:
            writer.Write("\\'");
            continue;
          case 92:
            writer.Write("\\\\");
            continue;
          default:
            if ((int) num >= 32 && (int) num < 128)
            {
              writer.Write((char) num);
              continue;
            }
            writer.Write('\\');
            writer.Write((char) (48 + ((int) num >> 6 & 3)));
            writer.Write((char) (48 + ((int) num >> 3 & 7)));
            writer.Write((char) (48 + ((int) num & 7)));
            continue;
        }
      }
    }
  }
}
