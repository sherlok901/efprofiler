﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.EnumLiteMap`1
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using System;

namespace Google.ProtocolBuffers
{
  public class EnumLiteMap<TEnum> : IEnumLiteMap<IEnumLite>, IEnumLiteMap where TEnum : struct, IComparable, IFormattable
  {
    public IEnumLite FindValueByNumber(int number)
    {
      if (Enum.IsDefined(typeof (TEnum), (object) number))
        return (IEnumLite) new EnumLiteMap<TEnum>.EnumValue((TEnum) (ValueType) number);
      return (IEnumLite) null;
    }

    public IEnumLite FindValueByName(string name)
    {
      if (Enum.IsDefined(typeof (TEnum), (object) name))
        return (IEnumLite) new EnumLiteMap<TEnum>.EnumValue((TEnum) Enum.Parse(typeof (TEnum), name, false));
      return (IEnumLite) null;
    }

    public bool IsValidValue(IEnumLite value)
    {
      return Enum.IsDefined(typeof (TEnum), (object) value.Number);
    }

    private struct EnumValue : IEnumLite
    {
      private readonly TEnum value;

      public EnumValue(TEnum value)
      {
        this.value = value;
      }

      int IEnumLite.Number
      {
        get
        {
          return Convert.ToInt32((object) this.value);
        }
      }

      string IEnumLite.Name
      {
        get
        {
          return this.value.ToString();
        }
      }
    }
  }
}
