﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.IMessage
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers.Descriptors;
using System.Collections.Generic;
using System.IO;

namespace Google.ProtocolBuffers
{
  public interface IMessage : IMessageLite
  {
    MessageDescriptor DescriptorForType { get; }

    IDictionary<FieldDescriptor, object> AllFields { get; }

    bool HasField(FieldDescriptor field);

    object this[FieldDescriptor field] { get; }

    int GetRepeatedFieldCount(FieldDescriptor field);

    object this[FieldDescriptor field, int index] { get; }

    UnknownFieldSet UnknownFields { get; }

    new bool IsInitialized { get; }

    new void WriteTo(ICodedOutputStream output);

    new void WriteDelimitedTo(Stream output);

    new int SerializedSize { get; }

    new bool Equals(object other);

    new int GetHashCode();

    new string ToString();

    new ByteString ToByteString();

    new byte[] ToByteArray();

    new void WriteTo(Stream output);

    IBuilder WeakCreateBuilderForType();

    IBuilder WeakToBuilder();

    IMessage WeakDefaultInstanceForType { get; }
  }
}
