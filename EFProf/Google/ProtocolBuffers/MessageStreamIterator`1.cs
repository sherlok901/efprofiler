﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.MessageStreamIterator`1
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace Google.ProtocolBuffers
{
  public class MessageStreamIterator<TMessage> : IEnumerable<TMessage>, IEnumerable where TMessage : IMessage<TMessage>
  {
    private static readonly TMessage defaultMessageInstance = MessageStreamIterator<TMessage>.CreateDefaultInstance();
    private static readonly uint ExpectedTag = WireFormat.MakeTag(1, WireFormat.WireType.LengthDelimited);
    private readonly StreamProvider streamProvider;
    private readonly ExtensionRegistry extensionRegistry;
    private readonly int sizeLimit;
    private static Exception typeInitializationException;

    private static TMessage CreateDefaultInstance()
    {
      try
      {
        return (TMessage) typeof (TMessage).GetProperty("DefaultInstance", typeof (TMessage), new Type[0]).GetValue((object) null, (object[]) null);
      }
      catch (Exception ex)
      {
        MessageStreamIterator<TMessage>.typeInitializationException = ex;
        return default (TMessage);
      }
    }

    private MessageStreamIterator(StreamProvider streamProvider, ExtensionRegistry extensionRegistry, int sizeLimit)
    {
      if (object.ReferenceEquals((object) MessageStreamIterator<TMessage>.defaultMessageInstance, (object) null))
        throw new TargetInvocationException(MessageStreamIterator<TMessage>.typeInitializationException);
      this.streamProvider = streamProvider;
      this.extensionRegistry = extensionRegistry;
      this.sizeLimit = sizeLimit;
    }

    private MessageStreamIterator(StreamProvider streamProvider, ExtensionRegistry extensionRegistry)
      : this(streamProvider, extensionRegistry, 67108864)
    {
    }

    public MessageStreamIterator<TMessage> WithExtensionRegistry(ExtensionRegistry newRegistry)
    {
      return new MessageStreamIterator<TMessage>(this.streamProvider, newRegistry, this.sizeLimit);
    }

    public MessageStreamIterator<TMessage> WithSizeLimit(int newSizeLimit)
    {
      return new MessageStreamIterator<TMessage>(this.streamProvider, this.extensionRegistry, newSizeLimit);
    }

    public static MessageStreamIterator<TMessage> FromStreamProvider(StreamProvider streamProvider)
    {
      return new MessageStreamIterator<TMessage>(streamProvider, ExtensionRegistry.Empty);
    }

    public IEnumerator<TMessage> GetEnumerator()
    {
      using (Stream input1 = this.streamProvider())
      {
        CodedInputStream input = CodedInputStream.CreateInstance(input1);
        input.SetSizeLimit(this.sizeLimit);
        uint tag;
        string name;
        while (input.ReadTag(out tag, out name))
        {
          if (((int) tag != 0 || !(name == "item")) && (int) tag != (int) MessageStreamIterator<TMessage>.ExpectedTag)
            throw InvalidProtocolBufferException.InvalidMessageStreamTag();
          IBuilder builder = MessageStreamIterator<TMessage>.defaultMessageInstance.WeakCreateBuilderForType();
          input.ReadMessage((IBuilderLite) builder, this.extensionRegistry);
          yield return (TMessage) builder.WeakBuild();
          input.ResetSizeCounter();
        }
      }
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      return (IEnumerator) this.GetEnumerator();
    }
  }
}
