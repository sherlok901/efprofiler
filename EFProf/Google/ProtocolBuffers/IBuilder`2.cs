﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.IBuilder`2
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers.Descriptors;
using System.IO;

namespace Google.ProtocolBuffers
{
  public interface IBuilder<TMessage, TBuilder> : IBuilder, IBuilderLite<TMessage, TBuilder>, IBuilderLite where TMessage : IMessage<TMessage, TBuilder> where TBuilder : IBuilder<TMessage, TBuilder>
  {
    TBuilder SetUnknownFields(UnknownFieldSet unknownFields);

    new TBuilder Clear();

    TBuilder MergeFrom(IMessage other);

    new TMessage Build();

    new TMessage BuildPartial();

    new TBuilder Clone();

    new TBuilder MergeFrom(ICodedInputStream input);

    new TBuilder MergeFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry);

    new TMessage DefaultInstanceForType { get; }

    TBuilder ClearField(FieldDescriptor field);

    TBuilder AddRepeatedField(FieldDescriptor field, object value);

    TBuilder MergeUnknownFields(UnknownFieldSet unknownFields);

    new TBuilder MergeDelimitedFrom(Stream input);

    new TBuilder MergeDelimitedFrom(Stream input, ExtensionRegistry extensionRegistry);

    new TBuilder MergeFrom(ByteString data);

    new TBuilder MergeFrom(ByteString data, ExtensionRegistry extensionRegistry);

    new TBuilder MergeFrom(byte[] data);

    new TBuilder MergeFrom(byte[] data, ExtensionRegistry extensionRegistry);

    new TBuilder MergeFrom(Stream input);

    new TBuilder MergeFrom(Stream input, ExtensionRegistry extensionRegistry);
  }
}
