﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.IBuilder
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers.Descriptors;
using System.Collections.Generic;

namespace Google.ProtocolBuffers
{
  public interface IBuilder : IBuilderLite
  {
    new bool IsInitialized { get; }

    IBuilder SetField(FieldDescriptor field, object value);

    IBuilder SetRepeatedField(FieldDescriptor field, int index, object value);

    IDictionary<FieldDescriptor, object> AllFields { get; }

    object this[FieldDescriptor field] { get; set; }

    MessageDescriptor DescriptorForType { get; }

    int GetRepeatedFieldCount(FieldDescriptor field);

    object this[FieldDescriptor field, int index] { get; set; }

    bool HasField(FieldDescriptor field);

    UnknownFieldSet UnknownFields { get; set; }

    IBuilder CreateBuilderForField(FieldDescriptor field);

    IBuilder WeakAddRepeatedField(FieldDescriptor field, object value);

    IBuilder WeakClear();

    IBuilder WeakClearField(FieldDescriptor field);

    IBuilder WeakMergeFrom(IMessage message);

    IBuilder WeakMergeFrom(ByteString data);

    IBuilder WeakMergeFrom(ByteString data, ExtensionRegistry registry);

    IBuilder WeakMergeFrom(ICodedInputStream input);

    IBuilder WeakMergeFrom(ICodedInputStream input, ExtensionRegistry registry);

    IMessage WeakBuild();

    IMessage WeakBuildPartial();

    IBuilder WeakClone();

    IMessage WeakDefaultInstanceForType { get; }
  }
}
