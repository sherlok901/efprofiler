﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.DescriptorProtos.MessageOptions
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers.Collections;
using Google.ProtocolBuffers.Descriptors;
using Google.ProtocolBuffers.FieldAccess;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace Google.ProtocolBuffers.DescriptorProtos
{
  [DebuggerNonUserCode]
  public sealed class MessageOptions : ExtendableMessage<MessageOptions, MessageOptions.Builder>
  {
    private static readonly MessageOptions defaultInstance = new MessageOptions().MakeReadOnly();
    private static readonly string[] _messageOptionsFieldNames = new string[3]
    {
      "message_set_wire_format",
      "no_standard_descriptor_accessor",
      "uninterpreted_option"
    };
    private static readonly uint[] _messageOptionsFieldTags = new uint[3]
    {
      8U,
      16U,
      7994U
    };
    private PopsicleList<UninterpretedOption> uninterpretedOption_ = new PopsicleList<UninterpretedOption>();
    private int memoizedSerializedSize = -1;
    public const int MessageSetWireFormatFieldNumber = 1;
    public const int NoStandardDescriptorAccessorFieldNumber = 2;
    public const int UninterpretedOptionFieldNumber = 999;
    private bool hasMessageSetWireFormat;
    private bool messageSetWireFormat_;
    private bool hasNoStandardDescriptorAccessor;
    private bool noStandardDescriptorAccessor_;

    private MessageOptions()
    {
    }

    public static MessageOptions DefaultInstance
    {
      get
      {
        return MessageOptions.defaultInstance;
      }
    }

    public override MessageOptions DefaultInstanceForType
    {
      get
      {
        return MessageOptions.DefaultInstance;
      }
    }

    protected override MessageOptions ThisMessage
    {
      get
      {
        return this;
      }
    }

    public static MessageDescriptor Descriptor
    {
      get
      {
        return DescriptorProtoFile.internal__static_google_protobuf_MessageOptions__Descriptor;
      }
    }

    protected override FieldAccessorTable<MessageOptions, MessageOptions.Builder> InternalFieldAccessors
    {
      get
      {
        return DescriptorProtoFile.internal__static_google_protobuf_MessageOptions__FieldAccessorTable;
      }
    }

    public bool HasMessageSetWireFormat
    {
      get
      {
        return this.hasMessageSetWireFormat;
      }
    }

    public bool MessageSetWireFormat
    {
      get
      {
        return this.messageSetWireFormat_;
      }
    }

    public bool HasNoStandardDescriptorAccessor
    {
      get
      {
        return this.hasNoStandardDescriptorAccessor;
      }
    }

    public bool NoStandardDescriptorAccessor
    {
      get
      {
        return this.noStandardDescriptorAccessor_;
      }
    }

    public IList<UninterpretedOption> UninterpretedOptionList
    {
      get
      {
        return (IList<UninterpretedOption>) this.uninterpretedOption_;
      }
    }

    public int UninterpretedOptionCount
    {
      get
      {
        return this.uninterpretedOption_.Count;
      }
    }

    public UninterpretedOption GetUninterpretedOption(int index)
    {
      return this.uninterpretedOption_[index];
    }

    public override bool IsInitialized
    {
      get
      {
        foreach (AbstractMessageLite<UninterpretedOption, UninterpretedOption.Builder> uninterpretedOption in (IEnumerable<UninterpretedOption>) this.UninterpretedOptionList)
        {
          if (!uninterpretedOption.IsInitialized)
            return false;
        }
        return this.ExtensionsAreInitialized;
      }
    }

    public override void WriteTo(ICodedOutputStream output)
    {
      int serializedSize = this.SerializedSize;
      string[] optionsFieldNames = MessageOptions._messageOptionsFieldNames;
      ExtendableMessage<MessageOptions, MessageOptions.Builder>.ExtensionWriter extensionWriter = this.CreateExtensionWriter((ExtendableMessage<MessageOptions, MessageOptions.Builder>) this);
      if (this.hasMessageSetWireFormat)
        output.WriteBool(1, optionsFieldNames[0], this.MessageSetWireFormat);
      if (this.hasNoStandardDescriptorAccessor)
        output.WriteBool(2, optionsFieldNames[1], this.NoStandardDescriptorAccessor);
      if (this.uninterpretedOption_.Count > 0)
        output.WriteMessageArray<UninterpretedOption>(999, optionsFieldNames[2], (IEnumerable<UninterpretedOption>) this.uninterpretedOption_);
      extensionWriter.WriteUntil(536870912, output);
      this.UnknownFields.WriteTo(output);
    }

    public override int SerializedSize
    {
      get
      {
        int memoizedSerializedSize = this.memoizedSerializedSize;
        if (memoizedSerializedSize != -1)
          return memoizedSerializedSize;
        int num1 = 0;
        if (this.hasMessageSetWireFormat)
          num1 += CodedOutputStream.ComputeBoolSize(1, this.MessageSetWireFormat);
        if (this.hasNoStandardDescriptorAccessor)
          num1 += CodedOutputStream.ComputeBoolSize(2, this.NoStandardDescriptorAccessor);
        foreach (UninterpretedOption uninterpretedOption in (IEnumerable<UninterpretedOption>) this.UninterpretedOptionList)
          num1 += CodedOutputStream.ComputeMessageSize(999, (IMessageLite) uninterpretedOption);
        int num2 = num1 + this.ExtensionsSerializedSize + this.UnknownFields.SerializedSize;
        this.memoizedSerializedSize = num2;
        return num2;
      }
    }

    public static MessageOptions ParseFrom(ByteString data)
    {
      return MessageOptions.CreateBuilder().MergeFrom(data).BuildParsed();
    }

    public static MessageOptions ParseFrom(ByteString data, ExtensionRegistry extensionRegistry)
    {
      return MessageOptions.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
    }

    public static MessageOptions ParseFrom(byte[] data)
    {
      return MessageOptions.CreateBuilder().MergeFrom(data).BuildParsed();
    }

    public static MessageOptions ParseFrom(byte[] data, ExtensionRegistry extensionRegistry)
    {
      return MessageOptions.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
    }

    public static MessageOptions ParseFrom(Stream input)
    {
      return MessageOptions.CreateBuilder().MergeFrom(input).BuildParsed();
    }

    public static MessageOptions ParseFrom(Stream input, ExtensionRegistry extensionRegistry)
    {
      return MessageOptions.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
    }

    public static MessageOptions ParseDelimitedFrom(Stream input)
    {
      return MessageOptions.CreateBuilder().MergeDelimitedFrom(input).BuildParsed();
    }

    public static MessageOptions ParseDelimitedFrom(Stream input, ExtensionRegistry extensionRegistry)
    {
      return MessageOptions.CreateBuilder().MergeDelimitedFrom(input, extensionRegistry).BuildParsed();
    }

    public static MessageOptions ParseFrom(ICodedInputStream input)
    {
      return MessageOptions.CreateBuilder().MergeFrom(input).BuildParsed();
    }

    public static MessageOptions ParseFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
    {
      return MessageOptions.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
    }

    private MessageOptions MakeReadOnly()
    {
      this.uninterpretedOption_.MakeReadOnly();
      return this;
    }

    public static MessageOptions.Builder CreateBuilder()
    {
      return new MessageOptions.Builder();
    }

    public override MessageOptions.Builder ToBuilder()
    {
      return MessageOptions.CreateBuilder(this);
    }

    public override MessageOptions.Builder CreateBuilderForType()
    {
      return new MessageOptions.Builder();
    }

    public static MessageOptions.Builder CreateBuilder(MessageOptions prototype)
    {
      return new MessageOptions.Builder(prototype);
    }

    static MessageOptions()
    {
      object.ReferenceEquals((object) DescriptorProtoFile.Descriptor, (object) null);
    }

    [DebuggerNonUserCode]
    public sealed class Builder : ExtendableBuilder<MessageOptions, MessageOptions.Builder>
    {
      private bool resultIsReadOnly;
      private MessageOptions result;

      protected override MessageOptions.Builder ThisBuilder
      {
        get
        {
          return this;
        }
      }

      public Builder()
      {
        this.result = MessageOptions.DefaultInstance;
        this.resultIsReadOnly = true;
      }

      internal Builder(MessageOptions cloneFrom)
      {
        this.result = cloneFrom;
        this.resultIsReadOnly = true;
      }

      private MessageOptions PrepareBuilder()
      {
        if (this.resultIsReadOnly)
        {
          MessageOptions result = this.result;
          this.result = new MessageOptions();
          this.resultIsReadOnly = false;
          this.MergeFrom(result);
        }
        return this.result;
      }

      public override bool IsInitialized
      {
        get
        {
          return this.result.IsInitialized;
        }
      }

      protected override MessageOptions MessageBeingBuilt
      {
        get
        {
          return this.PrepareBuilder();
        }
      }

      public override MessageOptions.Builder Clear()
      {
        this.result = MessageOptions.DefaultInstance;
        this.resultIsReadOnly = true;
        return this;
      }

      public override MessageOptions.Builder Clone()
      {
        if (this.resultIsReadOnly)
          return new MessageOptions.Builder(this.result);
        return new MessageOptions.Builder().MergeFrom(this.result);
      }

      public override MessageDescriptor DescriptorForType
      {
        get
        {
          return MessageOptions.Descriptor;
        }
      }

      public override MessageOptions DefaultInstanceForType
      {
        get
        {
          return MessageOptions.DefaultInstance;
        }
      }

      public override MessageOptions BuildPartial()
      {
        if (this.resultIsReadOnly)
          return this.result;
        this.resultIsReadOnly = true;
        return this.result.MakeReadOnly();
      }

      public override MessageOptions.Builder MergeFrom(IMessage other)
      {
        if (other is MessageOptions)
          return this.MergeFrom((MessageOptions) other);
        base.MergeFrom(other);
        return this;
      }

      public override MessageOptions.Builder MergeFrom(MessageOptions other)
      {
        if (other == MessageOptions.DefaultInstance)
          return this;
        this.PrepareBuilder();
        if (other.HasMessageSetWireFormat)
          this.MessageSetWireFormat = other.MessageSetWireFormat;
        if (other.HasNoStandardDescriptorAccessor)
          this.NoStandardDescriptorAccessor = other.NoStandardDescriptorAccessor;
        if (other.uninterpretedOption_.Count != 0)
          this.result.uninterpretedOption_.Add((IEnumerable<UninterpretedOption>) other.uninterpretedOption_);
        this.MergeExtensionFields((ExtendableMessage<MessageOptions, MessageOptions.Builder>) other);
        this.MergeUnknownFields(other.UnknownFields);
        return this;
      }

      public override MessageOptions.Builder MergeFrom(ICodedInputStream input)
      {
        return this.MergeFrom(input, ExtensionRegistry.Empty);
      }

      public override MessageOptions.Builder MergeFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
      {
        this.PrepareBuilder();
        UnknownFieldSet.Builder unknownFields = (UnknownFieldSet.Builder) null;
        uint fieldTag;
        string fieldName;
        while (input.ReadTag(out fieldTag, out fieldName))
        {
          if ((int) fieldTag == 0 && fieldName != null)
          {
            int index = Array.BinarySearch<string>(MessageOptions._messageOptionsFieldNames, fieldName, (IComparer<string>) StringComparer.Ordinal);
            if (index >= 0)
            {
              fieldTag = MessageOptions._messageOptionsFieldTags[index];
            }
            else
            {
              if (unknownFields == null)
                unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
              this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
              continue;
            }
          }
          switch (fieldTag)
          {
            case 0:
              throw InvalidProtocolBufferException.InvalidTag();
            case 8:
              this.result.hasMessageSetWireFormat = input.ReadBool(ref this.result.messageSetWireFormat_);
              continue;
            case 16:
              this.result.hasNoStandardDescriptorAccessor = input.ReadBool(ref this.result.noStandardDescriptorAccessor_);
              continue;
            case 7994:
              input.ReadMessageArray<UninterpretedOption>(fieldTag, fieldName, (ICollection<UninterpretedOption>) this.result.uninterpretedOption_, UninterpretedOption.DefaultInstance, extensionRegistry);
              continue;
            default:
              if (WireFormat.IsEndGroupTag(fieldTag))
              {
                if (unknownFields != null)
                  this.UnknownFields = unknownFields.Build();
                return this;
              }
              if (unknownFields == null)
                unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
              this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
              continue;
          }
        }
        if (unknownFields != null)
          this.UnknownFields = unknownFields.Build();
        return this;
      }

      public bool HasMessageSetWireFormat
      {
        get
        {
          return this.result.hasMessageSetWireFormat;
        }
      }

      public bool MessageSetWireFormat
      {
        get
        {
          return this.result.MessageSetWireFormat;
        }
        set
        {
          this.SetMessageSetWireFormat(value);
        }
      }

      public MessageOptions.Builder SetMessageSetWireFormat(bool value)
      {
        this.PrepareBuilder();
        this.result.hasMessageSetWireFormat = true;
        this.result.messageSetWireFormat_ = value;
        return this;
      }

      public MessageOptions.Builder ClearMessageSetWireFormat()
      {
        this.PrepareBuilder();
        this.result.hasMessageSetWireFormat = false;
        this.result.messageSetWireFormat_ = false;
        return this;
      }

      public bool HasNoStandardDescriptorAccessor
      {
        get
        {
          return this.result.hasNoStandardDescriptorAccessor;
        }
      }

      public bool NoStandardDescriptorAccessor
      {
        get
        {
          return this.result.NoStandardDescriptorAccessor;
        }
        set
        {
          this.SetNoStandardDescriptorAccessor(value);
        }
      }

      public MessageOptions.Builder SetNoStandardDescriptorAccessor(bool value)
      {
        this.PrepareBuilder();
        this.result.hasNoStandardDescriptorAccessor = true;
        this.result.noStandardDescriptorAccessor_ = value;
        return this;
      }

      public MessageOptions.Builder ClearNoStandardDescriptorAccessor()
      {
        this.PrepareBuilder();
        this.result.hasNoStandardDescriptorAccessor = false;
        this.result.noStandardDescriptorAccessor_ = false;
        return this;
      }

      public IPopsicleList<UninterpretedOption> UninterpretedOptionList
      {
        get
        {
          return (IPopsicleList<UninterpretedOption>) this.PrepareBuilder().uninterpretedOption_;
        }
      }

      public int UninterpretedOptionCount
      {
        get
        {
          return this.result.UninterpretedOptionCount;
        }
      }

      public UninterpretedOption GetUninterpretedOption(int index)
      {
        return this.result.GetUninterpretedOption(index);
      }

      public MessageOptions.Builder SetUninterpretedOption(int index, UninterpretedOption value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.uninterpretedOption_[index] = value;
        return this;
      }

      public MessageOptions.Builder SetUninterpretedOption(int index, UninterpretedOption.Builder builderForValue)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
        this.PrepareBuilder();
        this.result.uninterpretedOption_[index] = builderForValue.Build();
        return this;
      }

      public MessageOptions.Builder AddUninterpretedOption(UninterpretedOption value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.uninterpretedOption_.Add(value);
        return this;
      }

      public MessageOptions.Builder AddUninterpretedOption(UninterpretedOption.Builder builderForValue)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
        this.PrepareBuilder();
        this.result.uninterpretedOption_.Add(builderForValue.Build());
        return this;
      }

      public MessageOptions.Builder AddRangeUninterpretedOption(IEnumerable<UninterpretedOption> values)
      {
        this.PrepareBuilder();
        this.result.uninterpretedOption_.Add(values);
        return this;
      }

      public MessageOptions.Builder ClearUninterpretedOption()
      {
        this.PrepareBuilder();
        this.result.uninterpretedOption_.Clear();
        return this;
      }
    }
  }
}
