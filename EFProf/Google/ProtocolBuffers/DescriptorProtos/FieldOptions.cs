﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.DescriptorProtos.FieldOptions
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers.Collections;
using Google.ProtocolBuffers.Descriptors;
using Google.ProtocolBuffers.FieldAccess;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace Google.ProtocolBuffers.DescriptorProtos
{
  [DebuggerNonUserCode]
  public sealed class FieldOptions : ExtendableMessage<FieldOptions, FieldOptions.Builder>
  {
    private static readonly FieldOptions defaultInstance = new FieldOptions().MakeReadOnly();
    private static readonly string[] _fieldOptionsFieldNames = new string[5]
    {
      "ctype",
      "deprecated",
      "experimental_map_key",
      "packed",
      "uninterpreted_option"
    };
    private static readonly uint[] _fieldOptionsFieldTags = new uint[5]
    {
      8U,
      24U,
      74U,
      16U,
      7994U
    };
    private string experimentalMapKey_ = "";
    private PopsicleList<UninterpretedOption> uninterpretedOption_ = new PopsicleList<UninterpretedOption>();
    private int memoizedSerializedSize = -1;
    public const int CtypeFieldNumber = 1;
    public const int PackedFieldNumber = 2;
    public const int DeprecatedFieldNumber = 3;
    public const int ExperimentalMapKeyFieldNumber = 9;
    public const int UninterpretedOptionFieldNumber = 999;
    private bool hasCtype;
    private FieldOptions.Types.CType ctype_;
    private bool hasPacked;
    private bool packed_;
    private bool hasDeprecated;
    private bool deprecated_;
    private bool hasExperimentalMapKey;

    private FieldOptions()
    {
    }

    public static FieldOptions DefaultInstance
    {
      get
      {
        return FieldOptions.defaultInstance;
      }
    }

    public override FieldOptions DefaultInstanceForType
    {
      get
      {
        return FieldOptions.DefaultInstance;
      }
    }

    protected override FieldOptions ThisMessage
    {
      get
      {
        return this;
      }
    }

    public static MessageDescriptor Descriptor
    {
      get
      {
        return DescriptorProtoFile.internal__static_google_protobuf_FieldOptions__Descriptor;
      }
    }

    protected override FieldAccessorTable<FieldOptions, FieldOptions.Builder> InternalFieldAccessors
    {
      get
      {
        return DescriptorProtoFile.internal__static_google_protobuf_FieldOptions__FieldAccessorTable;
      }
    }

    public bool HasCtype
    {
      get
      {
        return this.hasCtype;
      }
    }

    public FieldOptions.Types.CType Ctype
    {
      get
      {
        return this.ctype_;
      }
    }

    public bool HasPacked
    {
      get
      {
        return this.hasPacked;
      }
    }

    public bool Packed
    {
      get
      {
        return this.packed_;
      }
    }

    public bool HasDeprecated
    {
      get
      {
        return this.hasDeprecated;
      }
    }

    public bool Deprecated
    {
      get
      {
        return this.deprecated_;
      }
    }

    public bool HasExperimentalMapKey
    {
      get
      {
        return this.hasExperimentalMapKey;
      }
    }

    public string ExperimentalMapKey
    {
      get
      {
        return this.experimentalMapKey_;
      }
    }

    public IList<UninterpretedOption> UninterpretedOptionList
    {
      get
      {
        return (IList<UninterpretedOption>) this.uninterpretedOption_;
      }
    }

    public int UninterpretedOptionCount
    {
      get
      {
        return this.uninterpretedOption_.Count;
      }
    }

    public UninterpretedOption GetUninterpretedOption(int index)
    {
      return this.uninterpretedOption_[index];
    }

    public override bool IsInitialized
    {
      get
      {
        foreach (AbstractMessageLite<UninterpretedOption, UninterpretedOption.Builder> uninterpretedOption in (IEnumerable<UninterpretedOption>) this.UninterpretedOptionList)
        {
          if (!uninterpretedOption.IsInitialized)
            return false;
        }
        return this.ExtensionsAreInitialized;
      }
    }

    public override void WriteTo(ICodedOutputStream output)
    {
      int serializedSize = this.SerializedSize;
      string[] optionsFieldNames = FieldOptions._fieldOptionsFieldNames;
      ExtendableMessage<FieldOptions, FieldOptions.Builder>.ExtensionWriter extensionWriter = this.CreateExtensionWriter((ExtendableMessage<FieldOptions, FieldOptions.Builder>) this);
      if (this.hasCtype)
        output.WriteEnum(1, optionsFieldNames[0], (int) this.Ctype, (object) this.Ctype);
      if (this.hasPacked)
        output.WriteBool(2, optionsFieldNames[3], this.Packed);
      if (this.hasDeprecated)
        output.WriteBool(3, optionsFieldNames[1], this.Deprecated);
      if (this.hasExperimentalMapKey)
        output.WriteString(9, optionsFieldNames[2], this.ExperimentalMapKey);
      if (this.uninterpretedOption_.Count > 0)
        output.WriteMessageArray<UninterpretedOption>(999, optionsFieldNames[4], (IEnumerable<UninterpretedOption>) this.uninterpretedOption_);
      extensionWriter.WriteUntil(536870912, output);
      this.UnknownFields.WriteTo(output);
    }

    public override int SerializedSize
    {
      get
      {
        int memoizedSerializedSize = this.memoizedSerializedSize;
        if (memoizedSerializedSize != -1)
          return memoizedSerializedSize;
        int num1 = 0;
        if (this.hasCtype)
          num1 += CodedOutputStream.ComputeEnumSize(1, (int) this.Ctype);
        if (this.hasPacked)
          num1 += CodedOutputStream.ComputeBoolSize(2, this.Packed);
        if (this.hasDeprecated)
          num1 += CodedOutputStream.ComputeBoolSize(3, this.Deprecated);
        if (this.hasExperimentalMapKey)
          num1 += CodedOutputStream.ComputeStringSize(9, this.ExperimentalMapKey);
        foreach (UninterpretedOption uninterpretedOption in (IEnumerable<UninterpretedOption>) this.UninterpretedOptionList)
          num1 += CodedOutputStream.ComputeMessageSize(999, (IMessageLite) uninterpretedOption);
        int num2 = num1 + this.ExtensionsSerializedSize + this.UnknownFields.SerializedSize;
        this.memoizedSerializedSize = num2;
        return num2;
      }
    }

    public static FieldOptions ParseFrom(ByteString data)
    {
      return FieldOptions.CreateBuilder().MergeFrom(data).BuildParsed();
    }

    public static FieldOptions ParseFrom(ByteString data, ExtensionRegistry extensionRegistry)
    {
      return FieldOptions.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
    }

    public static FieldOptions ParseFrom(byte[] data)
    {
      return FieldOptions.CreateBuilder().MergeFrom(data).BuildParsed();
    }

    public static FieldOptions ParseFrom(byte[] data, ExtensionRegistry extensionRegistry)
    {
      return FieldOptions.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
    }

    public static FieldOptions ParseFrom(Stream input)
    {
      return FieldOptions.CreateBuilder().MergeFrom(input).BuildParsed();
    }

    public static FieldOptions ParseFrom(Stream input, ExtensionRegistry extensionRegistry)
    {
      return FieldOptions.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
    }

    public static FieldOptions ParseDelimitedFrom(Stream input)
    {
      return FieldOptions.CreateBuilder().MergeDelimitedFrom(input).BuildParsed();
    }

    public static FieldOptions ParseDelimitedFrom(Stream input, ExtensionRegistry extensionRegistry)
    {
      return FieldOptions.CreateBuilder().MergeDelimitedFrom(input, extensionRegistry).BuildParsed();
    }

    public static FieldOptions ParseFrom(ICodedInputStream input)
    {
      return FieldOptions.CreateBuilder().MergeFrom(input).BuildParsed();
    }

    public static FieldOptions ParseFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
    {
      return FieldOptions.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
    }

    private FieldOptions MakeReadOnly()
    {
      this.uninterpretedOption_.MakeReadOnly();
      return this;
    }

    public static FieldOptions.Builder CreateBuilder()
    {
      return new FieldOptions.Builder();
    }

    public override FieldOptions.Builder ToBuilder()
    {
      return FieldOptions.CreateBuilder(this);
    }

    public override FieldOptions.Builder CreateBuilderForType()
    {
      return new FieldOptions.Builder();
    }

    public static FieldOptions.Builder CreateBuilder(FieldOptions prototype)
    {
      return new FieldOptions.Builder(prototype);
    }

    static FieldOptions()
    {
      object.ReferenceEquals((object) DescriptorProtoFile.Descriptor, (object) null);
    }

    [DebuggerNonUserCode]
    public static class Types
    {
      public enum CType
      {
        STRING,
        CORD,
        STRING_PIECE,
      }
    }

    [DebuggerNonUserCode]
    public sealed class Builder : ExtendableBuilder<FieldOptions, FieldOptions.Builder>
    {
      private bool resultIsReadOnly;
      private FieldOptions result;

      protected override FieldOptions.Builder ThisBuilder
      {
        get
        {
          return this;
        }
      }

      public Builder()
      {
        this.result = FieldOptions.DefaultInstance;
        this.resultIsReadOnly = true;
      }

      internal Builder(FieldOptions cloneFrom)
      {
        this.result = cloneFrom;
        this.resultIsReadOnly = true;
      }

      private FieldOptions PrepareBuilder()
      {
        if (this.resultIsReadOnly)
        {
          FieldOptions result = this.result;
          this.result = new FieldOptions();
          this.resultIsReadOnly = false;
          this.MergeFrom(result);
        }
        return this.result;
      }

      public override bool IsInitialized
      {
        get
        {
          return this.result.IsInitialized;
        }
      }

      protected override FieldOptions MessageBeingBuilt
      {
        get
        {
          return this.PrepareBuilder();
        }
      }

      public override FieldOptions.Builder Clear()
      {
        this.result = FieldOptions.DefaultInstance;
        this.resultIsReadOnly = true;
        return this;
      }

      public override FieldOptions.Builder Clone()
      {
        if (this.resultIsReadOnly)
          return new FieldOptions.Builder(this.result);
        return new FieldOptions.Builder().MergeFrom(this.result);
      }

      public override MessageDescriptor DescriptorForType
      {
        get
        {
          return FieldOptions.Descriptor;
        }
      }

      public override FieldOptions DefaultInstanceForType
      {
        get
        {
          return FieldOptions.DefaultInstance;
        }
      }

      public override FieldOptions BuildPartial()
      {
        if (this.resultIsReadOnly)
          return this.result;
        this.resultIsReadOnly = true;
        return this.result.MakeReadOnly();
      }

      public override FieldOptions.Builder MergeFrom(IMessage other)
      {
        if (other is FieldOptions)
          return this.MergeFrom((FieldOptions) other);
        base.MergeFrom(other);
        return this;
      }

      public override FieldOptions.Builder MergeFrom(FieldOptions other)
      {
        if (other == FieldOptions.DefaultInstance)
          return this;
        this.PrepareBuilder();
        if (other.HasCtype)
          this.Ctype = other.Ctype;
        if (other.HasPacked)
          this.Packed = other.Packed;
        if (other.HasDeprecated)
          this.Deprecated = other.Deprecated;
        if (other.HasExperimentalMapKey)
          this.ExperimentalMapKey = other.ExperimentalMapKey;
        if (other.uninterpretedOption_.Count != 0)
          this.result.uninterpretedOption_.Add((IEnumerable<UninterpretedOption>) other.uninterpretedOption_);
        this.MergeExtensionFields((ExtendableMessage<FieldOptions, FieldOptions.Builder>) other);
        this.MergeUnknownFields(other.UnknownFields);
        return this;
      }

      public override FieldOptions.Builder MergeFrom(ICodedInputStream input)
      {
        return this.MergeFrom(input, ExtensionRegistry.Empty);
      }

      public override FieldOptions.Builder MergeFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
      {
        this.PrepareBuilder();
        UnknownFieldSet.Builder unknownFields = (UnknownFieldSet.Builder) null;
        uint fieldTag;
        string fieldName;
        while (input.ReadTag(out fieldTag, out fieldName))
        {
          if ((int) fieldTag == 0 && fieldName != null)
          {
            int index = Array.BinarySearch<string>(FieldOptions._fieldOptionsFieldNames, fieldName, (IComparer<string>) StringComparer.Ordinal);
            if (index >= 0)
            {
              fieldTag = FieldOptions._fieldOptionsFieldTags[index];
            }
            else
            {
              if (unknownFields == null)
                unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
              this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
              continue;
            }
          }
          switch (fieldTag)
          {
            case 0:
              throw InvalidProtocolBufferException.InvalidTag();
            case 8:
              object unknown;
              if (input.ReadEnum<FieldOptions.Types.CType>(ref this.result.ctype_, out unknown))
              {
                this.result.hasCtype = true;
                continue;
              }
              if (unknown is int)
              {
                if (unknownFields == null)
                  unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
                unknownFields.MergeVarintField(1, (ulong) (int) unknown);
                continue;
              }
              continue;
            case 16:
              this.result.hasPacked = input.ReadBool(ref this.result.packed_);
              continue;
            case 24:
              this.result.hasDeprecated = input.ReadBool(ref this.result.deprecated_);
              continue;
            case 74:
              this.result.hasExperimentalMapKey = input.ReadString(ref this.result.experimentalMapKey_);
              continue;
            case 7994:
              input.ReadMessageArray<UninterpretedOption>(fieldTag, fieldName, (ICollection<UninterpretedOption>) this.result.uninterpretedOption_, UninterpretedOption.DefaultInstance, extensionRegistry);
              continue;
            default:
              if (WireFormat.IsEndGroupTag(fieldTag))
              {
                if (unknownFields != null)
                  this.UnknownFields = unknownFields.Build();
                return this;
              }
              if (unknownFields == null)
                unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
              this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
              continue;
          }
        }
        if (unknownFields != null)
          this.UnknownFields = unknownFields.Build();
        return this;
      }

      public bool HasCtype
      {
        get
        {
          return this.result.hasCtype;
        }
      }

      public FieldOptions.Types.CType Ctype
      {
        get
        {
          return this.result.Ctype;
        }
        set
        {
          this.SetCtype(value);
        }
      }

      public FieldOptions.Builder SetCtype(FieldOptions.Types.CType value)
      {
        this.PrepareBuilder();
        this.result.hasCtype = true;
        this.result.ctype_ = value;
        return this;
      }

      public FieldOptions.Builder ClearCtype()
      {
        this.PrepareBuilder();
        this.result.hasCtype = false;
        this.result.ctype_ = FieldOptions.Types.CType.STRING;
        return this;
      }

      public bool HasPacked
      {
        get
        {
          return this.result.hasPacked;
        }
      }

      public bool Packed
      {
        get
        {
          return this.result.Packed;
        }
        set
        {
          this.SetPacked(value);
        }
      }

      public FieldOptions.Builder SetPacked(bool value)
      {
        this.PrepareBuilder();
        this.result.hasPacked = true;
        this.result.packed_ = value;
        return this;
      }

      public FieldOptions.Builder ClearPacked()
      {
        this.PrepareBuilder();
        this.result.hasPacked = false;
        this.result.packed_ = false;
        return this;
      }

      public bool HasDeprecated
      {
        get
        {
          return this.result.hasDeprecated;
        }
      }

      public bool Deprecated
      {
        get
        {
          return this.result.Deprecated;
        }
        set
        {
          this.SetDeprecated(value);
        }
      }

      public FieldOptions.Builder SetDeprecated(bool value)
      {
        this.PrepareBuilder();
        this.result.hasDeprecated = true;
        this.result.deprecated_ = value;
        return this;
      }

      public FieldOptions.Builder ClearDeprecated()
      {
        this.PrepareBuilder();
        this.result.hasDeprecated = false;
        this.result.deprecated_ = false;
        return this;
      }

      public bool HasExperimentalMapKey
      {
        get
        {
          return this.result.hasExperimentalMapKey;
        }
      }

      public string ExperimentalMapKey
      {
        get
        {
          return this.result.ExperimentalMapKey;
        }
        set
        {
          this.SetExperimentalMapKey(value);
        }
      }

      public FieldOptions.Builder SetExperimentalMapKey(string value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.hasExperimentalMapKey = true;
        this.result.experimentalMapKey_ = value;
        return this;
      }

      public FieldOptions.Builder ClearExperimentalMapKey()
      {
        this.PrepareBuilder();
        this.result.hasExperimentalMapKey = false;
        this.result.experimentalMapKey_ = "";
        return this;
      }

      public IPopsicleList<UninterpretedOption> UninterpretedOptionList
      {
        get
        {
          return (IPopsicleList<UninterpretedOption>) this.PrepareBuilder().uninterpretedOption_;
        }
      }

      public int UninterpretedOptionCount
      {
        get
        {
          return this.result.UninterpretedOptionCount;
        }
      }

      public UninterpretedOption GetUninterpretedOption(int index)
      {
        return this.result.GetUninterpretedOption(index);
      }

      public FieldOptions.Builder SetUninterpretedOption(int index, UninterpretedOption value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.uninterpretedOption_[index] = value;
        return this;
      }

      public FieldOptions.Builder SetUninterpretedOption(int index, UninterpretedOption.Builder builderForValue)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
        this.PrepareBuilder();
        this.result.uninterpretedOption_[index] = builderForValue.Build();
        return this;
      }

      public FieldOptions.Builder AddUninterpretedOption(UninterpretedOption value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.uninterpretedOption_.Add(value);
        return this;
      }

      public FieldOptions.Builder AddUninterpretedOption(UninterpretedOption.Builder builderForValue)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
        this.PrepareBuilder();
        this.result.uninterpretedOption_.Add(builderForValue.Build());
        return this;
      }

      public FieldOptions.Builder AddRangeUninterpretedOption(IEnumerable<UninterpretedOption> values)
      {
        this.PrepareBuilder();
        this.result.uninterpretedOption_.Add(values);
        return this;
      }

      public FieldOptions.Builder ClearUninterpretedOption()
      {
        this.PrepareBuilder();
        this.result.uninterpretedOption_.Clear();
        return this;
      }
    }
  }
}
