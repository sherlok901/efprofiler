﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.DescriptorProtos.FileDescriptorProto
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers.Collections;
using Google.ProtocolBuffers.Descriptors;
using Google.ProtocolBuffers.FieldAccess;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace Google.ProtocolBuffers.DescriptorProtos
{
  [DebuggerNonUserCode]
  public sealed class FileDescriptorProto : GeneratedMessage<FileDescriptorProto, FileDescriptorProto.Builder>, IDescriptorProto<FileOptions>
  {
    private static readonly FileDescriptorProto defaultInstance = new FileDescriptorProto().MakeReadOnly();
    private static readonly string[] _fileDescriptorProtoFieldNames = new string[9]
    {
      "dependency",
      "enum_type",
      "extension",
      "message_type",
      "name",
      "options",
      "package",
      "service",
      "source_code_info"
    };
    private static readonly uint[] _fileDescriptorProtoFieldTags = new uint[9]
    {
      26U,
      42U,
      58U,
      34U,
      10U,
      66U,
      18U,
      50U,
      74U
    };
    private string name_ = "";
    private string package_ = "";
    private PopsicleList<string> dependency_ = new PopsicleList<string>();
    private PopsicleList<DescriptorProto> messageType_ = new PopsicleList<DescriptorProto>();
    private PopsicleList<EnumDescriptorProto> enumType_ = new PopsicleList<EnumDescriptorProto>();
    private PopsicleList<ServiceDescriptorProto> service_ = new PopsicleList<ServiceDescriptorProto>();
    private PopsicleList<FieldDescriptorProto> extension_ = new PopsicleList<FieldDescriptorProto>();
    private int memoizedSerializedSize = -1;
    public const int NameFieldNumber = 1;
    public const int PackageFieldNumber = 2;
    public const int DependencyFieldNumber = 3;
    public const int MessageTypeFieldNumber = 4;
    public const int EnumTypeFieldNumber = 5;
    public const int ServiceFieldNumber = 6;
    public const int ExtensionFieldNumber = 7;
    public const int OptionsFieldNumber = 8;
    public const int SourceCodeInfoFieldNumber = 9;
    private bool hasName;
    private bool hasPackage;
    private bool hasOptions;
    private FileOptions options_;
    private bool hasSourceCodeInfo;
    private SourceCodeInfo sourceCodeInfo_;

    private FileDescriptorProto()
    {
    }

    public static FileDescriptorProto DefaultInstance
    {
      get
      {
        return FileDescriptorProto.defaultInstance;
      }
    }

    public override FileDescriptorProto DefaultInstanceForType
    {
      get
      {
        return FileDescriptorProto.DefaultInstance;
      }
    }

    protected override FileDescriptorProto ThisMessage
    {
      get
      {
        return this;
      }
    }

    public static MessageDescriptor Descriptor
    {
      get
      {
        return DescriptorProtoFile.internal__static_google_protobuf_FileDescriptorProto__Descriptor;
      }
    }

    protected override FieldAccessorTable<FileDescriptorProto, FileDescriptorProto.Builder> InternalFieldAccessors
    {
      get
      {
        return DescriptorProtoFile.internal__static_google_protobuf_FileDescriptorProto__FieldAccessorTable;
      }
    }

    public bool HasName
    {
      get
      {
        return this.hasName;
      }
    }

    public string Name
    {
      get
      {
        return this.name_;
      }
    }

    public bool HasPackage
    {
      get
      {
        return this.hasPackage;
      }
    }

    public string Package
    {
      get
      {
        return this.package_;
      }
    }

    public IList<string> DependencyList
    {
      get
      {
        return Lists.AsReadOnly<string>((IList<string>) this.dependency_);
      }
    }

    public int DependencyCount
    {
      get
      {
        return this.dependency_.Count;
      }
    }

    public string GetDependency(int index)
    {
      return this.dependency_[index];
    }

    public IList<DescriptorProto> MessageTypeList
    {
      get
      {
        return (IList<DescriptorProto>) this.messageType_;
      }
    }

    public int MessageTypeCount
    {
      get
      {
        return this.messageType_.Count;
      }
    }

    public DescriptorProto GetMessageType(int index)
    {
      return this.messageType_[index];
    }

    public IList<EnumDescriptorProto> EnumTypeList
    {
      get
      {
        return (IList<EnumDescriptorProto>) this.enumType_;
      }
    }

    public int EnumTypeCount
    {
      get
      {
        return this.enumType_.Count;
      }
    }

    public EnumDescriptorProto GetEnumType(int index)
    {
      return this.enumType_[index];
    }

    public IList<ServiceDescriptorProto> ServiceList
    {
      get
      {
        return (IList<ServiceDescriptorProto>) this.service_;
      }
    }

    public int ServiceCount
    {
      get
      {
        return this.service_.Count;
      }
    }

    public ServiceDescriptorProto GetService(int index)
    {
      return this.service_[index];
    }

    public IList<FieldDescriptorProto> ExtensionList
    {
      get
      {
        return (IList<FieldDescriptorProto>) this.extension_;
      }
    }

    public int ExtensionCount
    {
      get
      {
        return this.extension_.Count;
      }
    }

    public FieldDescriptorProto GetExtension(int index)
    {
      return this.extension_[index];
    }

    public bool HasOptions
    {
      get
      {
        return this.hasOptions;
      }
    }

    public FileOptions Options
    {
      get
      {
        return this.options_ ?? FileOptions.DefaultInstance;
      }
    }

    public bool HasSourceCodeInfo
    {
      get
      {
        return this.hasSourceCodeInfo;
      }
    }

    public SourceCodeInfo SourceCodeInfo
    {
      get
      {
        return this.sourceCodeInfo_ ?? SourceCodeInfo.DefaultInstance;
      }
    }

    public override bool IsInitialized
    {
      get
      {
        foreach (AbstractMessageLite<DescriptorProto, DescriptorProto.Builder> messageType in (IEnumerable<DescriptorProto>) this.MessageTypeList)
        {
          if (!messageType.IsInitialized)
            return false;
        }
        foreach (AbstractMessageLite<EnumDescriptorProto, EnumDescriptorProto.Builder> enumType in (IEnumerable<EnumDescriptorProto>) this.EnumTypeList)
        {
          if (!enumType.IsInitialized)
            return false;
        }
        foreach (AbstractMessageLite<ServiceDescriptorProto, ServiceDescriptorProto.Builder> service in (IEnumerable<ServiceDescriptorProto>) this.ServiceList)
        {
          if (!service.IsInitialized)
            return false;
        }
        foreach (AbstractMessageLite<FieldDescriptorProto, FieldDescriptorProto.Builder> extension in (IEnumerable<FieldDescriptorProto>) this.ExtensionList)
        {
          if (!extension.IsInitialized)
            return false;
        }
        return !this.HasOptions || this.Options.IsInitialized;
      }
    }

    public override void WriteTo(ICodedOutputStream output)
    {
      int serializedSize = this.SerializedSize;
      string[] descriptorProtoFieldNames = FileDescriptorProto._fileDescriptorProtoFieldNames;
      if (this.hasName)
        output.WriteString(1, descriptorProtoFieldNames[4], this.Name);
      if (this.hasPackage)
        output.WriteString(2, descriptorProtoFieldNames[6], this.Package);
      if (this.dependency_.Count > 0)
        output.WriteStringArray(3, descriptorProtoFieldNames[0], (IEnumerable<string>) this.dependency_);
      if (this.messageType_.Count > 0)
        output.WriteMessageArray<DescriptorProto>(4, descriptorProtoFieldNames[3], (IEnumerable<DescriptorProto>) this.messageType_);
      if (this.enumType_.Count > 0)
        output.WriteMessageArray<EnumDescriptorProto>(5, descriptorProtoFieldNames[1], (IEnumerable<EnumDescriptorProto>) this.enumType_);
      if (this.service_.Count > 0)
        output.WriteMessageArray<ServiceDescriptorProto>(6, descriptorProtoFieldNames[7], (IEnumerable<ServiceDescriptorProto>) this.service_);
      if (this.extension_.Count > 0)
        output.WriteMessageArray<FieldDescriptorProto>(7, descriptorProtoFieldNames[2], (IEnumerable<FieldDescriptorProto>) this.extension_);
      if (this.hasOptions)
        output.WriteMessage(8, descriptorProtoFieldNames[5], (IMessageLite) this.Options);
      if (this.hasSourceCodeInfo)
        output.WriteMessage(9, descriptorProtoFieldNames[8], (IMessageLite) this.SourceCodeInfo);
      this.UnknownFields.WriteTo(output);
    }

    public override int SerializedSize
    {
      get
      {
        int memoizedSerializedSize = this.memoizedSerializedSize;
        if (memoizedSerializedSize != -1)
          return memoizedSerializedSize;
        int num1 = 0;
        if (this.hasName)
          num1 += CodedOutputStream.ComputeStringSize(1, this.Name);
        if (this.hasPackage)
          num1 += CodedOutputStream.ComputeStringSize(2, this.Package);
        int num2 = 0;
        foreach (string dependency in (IEnumerable<string>) this.DependencyList)
          num2 += CodedOutputStream.ComputeStringSizeNoTag(dependency);
        int num3 = num1 + num2 + this.dependency_.Count;
        foreach (DescriptorProto messageType in (IEnumerable<DescriptorProto>) this.MessageTypeList)
          num3 += CodedOutputStream.ComputeMessageSize(4, (IMessageLite) messageType);
        foreach (EnumDescriptorProto enumType in (IEnumerable<EnumDescriptorProto>) this.EnumTypeList)
          num3 += CodedOutputStream.ComputeMessageSize(5, (IMessageLite) enumType);
        foreach (ServiceDescriptorProto service in (IEnumerable<ServiceDescriptorProto>) this.ServiceList)
          num3 += CodedOutputStream.ComputeMessageSize(6, (IMessageLite) service);
        foreach (FieldDescriptorProto extension in (IEnumerable<FieldDescriptorProto>) this.ExtensionList)
          num3 += CodedOutputStream.ComputeMessageSize(7, (IMessageLite) extension);
        if (this.hasOptions)
          num3 += CodedOutputStream.ComputeMessageSize(8, (IMessageLite) this.Options);
        if (this.hasSourceCodeInfo)
          num3 += CodedOutputStream.ComputeMessageSize(9, (IMessageLite) this.SourceCodeInfo);
        int num4 = num3 + this.UnknownFields.SerializedSize;
        this.memoizedSerializedSize = num4;
        return num4;
      }
    }

    public static FileDescriptorProto ParseFrom(ByteString data)
    {
      return FileDescriptorProto.CreateBuilder().MergeFrom(data).BuildParsed();
    }

    public static FileDescriptorProto ParseFrom(ByteString data, ExtensionRegistry extensionRegistry)
    {
      return FileDescriptorProto.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
    }

    public static FileDescriptorProto ParseFrom(byte[] data)
    {
      return FileDescriptorProto.CreateBuilder().MergeFrom(data).BuildParsed();
    }

    public static FileDescriptorProto ParseFrom(byte[] data, ExtensionRegistry extensionRegistry)
    {
      return FileDescriptorProto.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
    }

    public static FileDescriptorProto ParseFrom(Stream input)
    {
      return FileDescriptorProto.CreateBuilder().MergeFrom(input).BuildParsed();
    }

    public static FileDescriptorProto ParseFrom(Stream input, ExtensionRegistry extensionRegistry)
    {
      return FileDescriptorProto.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
    }

    public static FileDescriptorProto ParseDelimitedFrom(Stream input)
    {
      return FileDescriptorProto.CreateBuilder().MergeDelimitedFrom(input).BuildParsed();
    }

    public static FileDescriptorProto ParseDelimitedFrom(Stream input, ExtensionRegistry extensionRegistry)
    {
      return FileDescriptorProto.CreateBuilder().MergeDelimitedFrom(input, extensionRegistry).BuildParsed();
    }

    public static FileDescriptorProto ParseFrom(ICodedInputStream input)
    {
      return FileDescriptorProto.CreateBuilder().MergeFrom(input).BuildParsed();
    }

    public static FileDescriptorProto ParseFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
    {
      return FileDescriptorProto.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
    }

    private FileDescriptorProto MakeReadOnly()
    {
      this.dependency_.MakeReadOnly();
      this.messageType_.MakeReadOnly();
      this.enumType_.MakeReadOnly();
      this.service_.MakeReadOnly();
      this.extension_.MakeReadOnly();
      return this;
    }

    public static FileDescriptorProto.Builder CreateBuilder()
    {
      return new FileDescriptorProto.Builder();
    }

    public override FileDescriptorProto.Builder ToBuilder()
    {
      return FileDescriptorProto.CreateBuilder(this);
    }

    public override FileDescriptorProto.Builder CreateBuilderForType()
    {
      return new FileDescriptorProto.Builder();
    }

    public static FileDescriptorProto.Builder CreateBuilder(FileDescriptorProto prototype)
    {
      return new FileDescriptorProto.Builder(prototype);
    }

    static FileDescriptorProto()
    {
      object.ReferenceEquals((object) DescriptorProtoFile.Descriptor, (object) null);
    }

    [DebuggerNonUserCode]
    public sealed class Builder : GeneratedBuilder<FileDescriptorProto, FileDescriptorProto.Builder>
    {
      private bool resultIsReadOnly;
      private FileDescriptorProto result;

      protected override FileDescriptorProto.Builder ThisBuilder
      {
        get
        {
          return this;
        }
      }

      public Builder()
      {
        this.result = FileDescriptorProto.DefaultInstance;
        this.resultIsReadOnly = true;
      }

      internal Builder(FileDescriptorProto cloneFrom)
      {
        this.result = cloneFrom;
        this.resultIsReadOnly = true;
      }

      private FileDescriptorProto PrepareBuilder()
      {
        if (this.resultIsReadOnly)
        {
          FileDescriptorProto result = this.result;
          this.result = new FileDescriptorProto();
          this.resultIsReadOnly = false;
          this.MergeFrom(result);
        }
        return this.result;
      }

      public override bool IsInitialized
      {
        get
        {
          return this.result.IsInitialized;
        }
      }

      protected override FileDescriptorProto MessageBeingBuilt
      {
        get
        {
          return this.PrepareBuilder();
        }
      }

      public override FileDescriptorProto.Builder Clear()
      {
        this.result = FileDescriptorProto.DefaultInstance;
        this.resultIsReadOnly = true;
        return this;
      }

      public override FileDescriptorProto.Builder Clone()
      {
        if (this.resultIsReadOnly)
          return new FileDescriptorProto.Builder(this.result);
        return new FileDescriptorProto.Builder().MergeFrom(this.result);
      }

      public override MessageDescriptor DescriptorForType
      {
        get
        {
          return FileDescriptorProto.Descriptor;
        }
      }

      public override FileDescriptorProto DefaultInstanceForType
      {
        get
        {
          return FileDescriptorProto.DefaultInstance;
        }
      }

      public override FileDescriptorProto BuildPartial()
      {
        if (this.resultIsReadOnly)
          return this.result;
        this.resultIsReadOnly = true;
        return this.result.MakeReadOnly();
      }

      public override FileDescriptorProto.Builder MergeFrom(IMessage other)
      {
        if (other is FileDescriptorProto)
          return this.MergeFrom((FileDescriptorProto) other);
        base.MergeFrom(other);
        return this;
      }

      public override FileDescriptorProto.Builder MergeFrom(FileDescriptorProto other)
      {
        if (other == FileDescriptorProto.DefaultInstance)
          return this;
        this.PrepareBuilder();
        if (other.HasName)
          this.Name = other.Name;
        if (other.HasPackage)
          this.Package = other.Package;
        if (other.dependency_.Count != 0)
          this.result.dependency_.Add((IEnumerable<string>) other.dependency_);
        if (other.messageType_.Count != 0)
          this.result.messageType_.Add((IEnumerable<DescriptorProto>) other.messageType_);
        if (other.enumType_.Count != 0)
          this.result.enumType_.Add((IEnumerable<EnumDescriptorProto>) other.enumType_);
        if (other.service_.Count != 0)
          this.result.service_.Add((IEnumerable<ServiceDescriptorProto>) other.service_);
        if (other.extension_.Count != 0)
          this.result.extension_.Add((IEnumerable<FieldDescriptorProto>) other.extension_);
        if (other.HasOptions)
          this.MergeOptions(other.Options);
        if (other.HasSourceCodeInfo)
          this.MergeSourceCodeInfo(other.SourceCodeInfo);
        this.MergeUnknownFields(other.UnknownFields);
        return this;
      }

      public override FileDescriptorProto.Builder MergeFrom(ICodedInputStream input)
      {
        return this.MergeFrom(input, ExtensionRegistry.Empty);
      }

      public override FileDescriptorProto.Builder MergeFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
      {
        this.PrepareBuilder();
        UnknownFieldSet.Builder unknownFields = (UnknownFieldSet.Builder) null;
        uint fieldTag;
        string fieldName;
        while (input.ReadTag(out fieldTag, out fieldName))
        {
          if ((int) fieldTag == 0 && fieldName != null)
          {
            int index = Array.BinarySearch<string>(FileDescriptorProto._fileDescriptorProtoFieldNames, fieldName, (IComparer<string>) StringComparer.Ordinal);
            if (index >= 0)
            {
              fieldTag = FileDescriptorProto._fileDescriptorProtoFieldTags[index];
            }
            else
            {
              if (unknownFields == null)
                unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
              this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
              continue;
            }
          }
          switch (fieldTag)
          {
            case 0:
              throw InvalidProtocolBufferException.InvalidTag();
            case 10:
              this.result.hasName = input.ReadString(ref this.result.name_);
              continue;
            case 18:
              this.result.hasPackage = input.ReadString(ref this.result.package_);
              continue;
            case 26:
              input.ReadStringArray(fieldTag, fieldName, (ICollection<string>) this.result.dependency_);
              continue;
            case 34:
              input.ReadMessageArray<DescriptorProto>(fieldTag, fieldName, (ICollection<DescriptorProto>) this.result.messageType_, DescriptorProto.DefaultInstance, extensionRegistry);
              continue;
            case 42:
              input.ReadMessageArray<EnumDescriptorProto>(fieldTag, fieldName, (ICollection<EnumDescriptorProto>) this.result.enumType_, EnumDescriptorProto.DefaultInstance, extensionRegistry);
              continue;
            case 50:
              input.ReadMessageArray<ServiceDescriptorProto>(fieldTag, fieldName, (ICollection<ServiceDescriptorProto>) this.result.service_, ServiceDescriptorProto.DefaultInstance, extensionRegistry);
              continue;
            case 58:
              input.ReadMessageArray<FieldDescriptorProto>(fieldTag, fieldName, (ICollection<FieldDescriptorProto>) this.result.extension_, FieldDescriptorProto.DefaultInstance, extensionRegistry);
              continue;
            case 66:
              FileOptions.Builder builder1 = FileOptions.CreateBuilder();
              if (this.result.hasOptions)
                builder1.MergeFrom(this.Options);
              input.ReadMessage((IBuilderLite) builder1, extensionRegistry);
              this.Options = builder1.BuildPartial();
              continue;
            case 74:
              SourceCodeInfo.Builder builder2 = SourceCodeInfo.CreateBuilder();
              if (this.result.hasSourceCodeInfo)
                builder2.MergeFrom(this.SourceCodeInfo);
              input.ReadMessage((IBuilderLite) builder2, extensionRegistry);
              this.SourceCodeInfo = builder2.BuildPartial();
              continue;
            default:
              if (WireFormat.IsEndGroupTag(fieldTag))
              {
                if (unknownFields != null)
                  this.UnknownFields = unknownFields.Build();
                return this;
              }
              if (unknownFields == null)
                unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
              this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
              continue;
          }
        }
        if (unknownFields != null)
          this.UnknownFields = unknownFields.Build();
        return this;
      }

      public bool HasName
      {
        get
        {
          return this.result.hasName;
        }
      }

      public string Name
      {
        get
        {
          return this.result.Name;
        }
        set
        {
          this.SetName(value);
        }
      }

      public FileDescriptorProto.Builder SetName(string value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.hasName = true;
        this.result.name_ = value;
        return this;
      }

      public FileDescriptorProto.Builder ClearName()
      {
        this.PrepareBuilder();
        this.result.hasName = false;
        this.result.name_ = "";
        return this;
      }

      public bool HasPackage
      {
        get
        {
          return this.result.hasPackage;
        }
      }

      public string Package
      {
        get
        {
          return this.result.Package;
        }
        set
        {
          this.SetPackage(value);
        }
      }

      public FileDescriptorProto.Builder SetPackage(string value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.hasPackage = true;
        this.result.package_ = value;
        return this;
      }

      public FileDescriptorProto.Builder ClearPackage()
      {
        this.PrepareBuilder();
        this.result.hasPackage = false;
        this.result.package_ = "";
        return this;
      }

      public IPopsicleList<string> DependencyList
      {
        get
        {
          return (IPopsicleList<string>) this.PrepareBuilder().dependency_;
        }
      }

      public int DependencyCount
      {
        get
        {
          return this.result.DependencyCount;
        }
      }

      public string GetDependency(int index)
      {
        return this.result.GetDependency(index);
      }

      public FileDescriptorProto.Builder SetDependency(int index, string value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.dependency_[index] = value;
        return this;
      }

      public FileDescriptorProto.Builder AddDependency(string value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.dependency_.Add(value);
        return this;
      }

      public FileDescriptorProto.Builder AddRangeDependency(IEnumerable<string> values)
      {
        this.PrepareBuilder();
        this.result.dependency_.Add(values);
        return this;
      }

      public FileDescriptorProto.Builder ClearDependency()
      {
        this.PrepareBuilder();
        this.result.dependency_.Clear();
        return this;
      }

      public IPopsicleList<DescriptorProto> MessageTypeList
      {
        get
        {
          return (IPopsicleList<DescriptorProto>) this.PrepareBuilder().messageType_;
        }
      }

      public int MessageTypeCount
      {
        get
        {
          return this.result.MessageTypeCount;
        }
      }

      public DescriptorProto GetMessageType(int index)
      {
        return this.result.GetMessageType(index);
      }

      public FileDescriptorProto.Builder SetMessageType(int index, DescriptorProto value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.messageType_[index] = value;
        return this;
      }

      public FileDescriptorProto.Builder SetMessageType(int index, DescriptorProto.Builder builderForValue)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
        this.PrepareBuilder();
        this.result.messageType_[index] = builderForValue.Build();
        return this;
      }

      public FileDescriptorProto.Builder AddMessageType(DescriptorProto value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.messageType_.Add(value);
        return this;
      }

      public FileDescriptorProto.Builder AddMessageType(DescriptorProto.Builder builderForValue)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
        this.PrepareBuilder();
        this.result.messageType_.Add(builderForValue.Build());
        return this;
      }

      public FileDescriptorProto.Builder AddRangeMessageType(IEnumerable<DescriptorProto> values)
      {
        this.PrepareBuilder();
        this.result.messageType_.Add(values);
        return this;
      }

      public FileDescriptorProto.Builder ClearMessageType()
      {
        this.PrepareBuilder();
        this.result.messageType_.Clear();
        return this;
      }

      public IPopsicleList<EnumDescriptorProto> EnumTypeList
      {
        get
        {
          return (IPopsicleList<EnumDescriptorProto>) this.PrepareBuilder().enumType_;
        }
      }

      public int EnumTypeCount
      {
        get
        {
          return this.result.EnumTypeCount;
        }
      }

      public EnumDescriptorProto GetEnumType(int index)
      {
        return this.result.GetEnumType(index);
      }

      public FileDescriptorProto.Builder SetEnumType(int index, EnumDescriptorProto value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.enumType_[index] = value;
        return this;
      }

      public FileDescriptorProto.Builder SetEnumType(int index, EnumDescriptorProto.Builder builderForValue)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
        this.PrepareBuilder();
        this.result.enumType_[index] = builderForValue.Build();
        return this;
      }

      public FileDescriptorProto.Builder AddEnumType(EnumDescriptorProto value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.enumType_.Add(value);
        return this;
      }

      public FileDescriptorProto.Builder AddEnumType(EnumDescriptorProto.Builder builderForValue)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
        this.PrepareBuilder();
        this.result.enumType_.Add(builderForValue.Build());
        return this;
      }

      public FileDescriptorProto.Builder AddRangeEnumType(IEnumerable<EnumDescriptorProto> values)
      {
        this.PrepareBuilder();
        this.result.enumType_.Add(values);
        return this;
      }

      public FileDescriptorProto.Builder ClearEnumType()
      {
        this.PrepareBuilder();
        this.result.enumType_.Clear();
        return this;
      }

      public IPopsicleList<ServiceDescriptorProto> ServiceList
      {
        get
        {
          return (IPopsicleList<ServiceDescriptorProto>) this.PrepareBuilder().service_;
        }
      }

      public int ServiceCount
      {
        get
        {
          return this.result.ServiceCount;
        }
      }

      public ServiceDescriptorProto GetService(int index)
      {
        return this.result.GetService(index);
      }

      public FileDescriptorProto.Builder SetService(int index, ServiceDescriptorProto value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.service_[index] = value;
        return this;
      }

      public FileDescriptorProto.Builder SetService(int index, ServiceDescriptorProto.Builder builderForValue)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
        this.PrepareBuilder();
        this.result.service_[index] = builderForValue.Build();
        return this;
      }

      public FileDescriptorProto.Builder AddService(ServiceDescriptorProto value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.service_.Add(value);
        return this;
      }

      public FileDescriptorProto.Builder AddService(ServiceDescriptorProto.Builder builderForValue)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
        this.PrepareBuilder();
        this.result.service_.Add(builderForValue.Build());
        return this;
      }

      public FileDescriptorProto.Builder AddRangeService(IEnumerable<ServiceDescriptorProto> values)
      {
        this.PrepareBuilder();
        this.result.service_.Add(values);
        return this;
      }

      public FileDescriptorProto.Builder ClearService()
      {
        this.PrepareBuilder();
        this.result.service_.Clear();
        return this;
      }

      public IPopsicleList<FieldDescriptorProto> ExtensionList
      {
        get
        {
          return (IPopsicleList<FieldDescriptorProto>) this.PrepareBuilder().extension_;
        }
      }

      public int ExtensionCount
      {
        get
        {
          return this.result.ExtensionCount;
        }
      }

      public FieldDescriptorProto GetExtension(int index)
      {
        return this.result.GetExtension(index);
      }

      public FileDescriptorProto.Builder SetExtension(int index, FieldDescriptorProto value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.extension_[index] = value;
        return this;
      }

      public FileDescriptorProto.Builder SetExtension(int index, FieldDescriptorProto.Builder builderForValue)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
        this.PrepareBuilder();
        this.result.extension_[index] = builderForValue.Build();
        return this;
      }

      public FileDescriptorProto.Builder AddExtension(FieldDescriptorProto value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.extension_.Add(value);
        return this;
      }

      public FileDescriptorProto.Builder AddExtension(FieldDescriptorProto.Builder builderForValue)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
        this.PrepareBuilder();
        this.result.extension_.Add(builderForValue.Build());
        return this;
      }

      public FileDescriptorProto.Builder AddRangeExtension(IEnumerable<FieldDescriptorProto> values)
      {
        this.PrepareBuilder();
        this.result.extension_.Add(values);
        return this;
      }

      public FileDescriptorProto.Builder ClearExtension()
      {
        this.PrepareBuilder();
        this.result.extension_.Clear();
        return this;
      }

      public bool HasOptions
      {
        get
        {
          return this.result.hasOptions;
        }
      }

      public FileOptions Options
      {
        get
        {
          return this.result.Options;
        }
        set
        {
          this.SetOptions(value);
        }
      }

      public FileDescriptorProto.Builder SetOptions(FileOptions value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.hasOptions = true;
        this.result.options_ = value;
        return this;
      }

      public FileDescriptorProto.Builder SetOptions(FileOptions.Builder builderForValue)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
        this.PrepareBuilder();
        this.result.hasOptions = true;
        this.result.options_ = builderForValue.Build();
        return this;
      }

      public FileDescriptorProto.Builder MergeOptions(FileOptions value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.options_ = !this.result.hasOptions || this.result.options_ == FileOptions.DefaultInstance ? value : FileOptions.CreateBuilder(this.result.options_).MergeFrom(value).BuildPartial();
        this.result.hasOptions = true;
        return this;
      }

      public FileDescriptorProto.Builder ClearOptions()
      {
        this.PrepareBuilder();
        this.result.hasOptions = false;
        this.result.options_ = (FileOptions) null;
        return this;
      }

      public bool HasSourceCodeInfo
      {
        get
        {
          return this.result.hasSourceCodeInfo;
        }
      }

      public SourceCodeInfo SourceCodeInfo
      {
        get
        {
          return this.result.SourceCodeInfo;
        }
        set
        {
          this.SetSourceCodeInfo(value);
        }
      }

      public FileDescriptorProto.Builder SetSourceCodeInfo(SourceCodeInfo value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.hasSourceCodeInfo = true;
        this.result.sourceCodeInfo_ = value;
        return this;
      }

      public FileDescriptorProto.Builder SetSourceCodeInfo(SourceCodeInfo.Builder builderForValue)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
        this.PrepareBuilder();
        this.result.hasSourceCodeInfo = true;
        this.result.sourceCodeInfo_ = builderForValue.Build();
        return this;
      }

      public FileDescriptorProto.Builder MergeSourceCodeInfo(SourceCodeInfo value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.sourceCodeInfo_ = !this.result.hasSourceCodeInfo || this.result.sourceCodeInfo_ == SourceCodeInfo.DefaultInstance ? value : SourceCodeInfo.CreateBuilder(this.result.sourceCodeInfo_).MergeFrom(value).BuildPartial();
        this.result.hasSourceCodeInfo = true;
        return this;
      }

      public FileDescriptorProto.Builder ClearSourceCodeInfo()
      {
        this.PrepareBuilder();
        this.result.hasSourceCodeInfo = false;
        this.result.sourceCodeInfo_ = (SourceCodeInfo) null;
        return this;
      }
    }
  }
}
