﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.DescriptorProtos.EnumOptions
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers.Collections;
using Google.ProtocolBuffers.Descriptors;
using Google.ProtocolBuffers.FieldAccess;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace Google.ProtocolBuffers.DescriptorProtos
{
  [DebuggerNonUserCode]
  public sealed class EnumOptions : ExtendableMessage<EnumOptions, EnumOptions.Builder>
  {
    private static readonly EnumOptions defaultInstance = new EnumOptions().MakeReadOnly();
    private static readonly string[] _enumOptionsFieldNames = new string[1]
    {
      "uninterpreted_option"
    };
    private static readonly uint[] _enumOptionsFieldTags = new uint[1]
    {
      7994U
    };
    private PopsicleList<UninterpretedOption> uninterpretedOption_ = new PopsicleList<UninterpretedOption>();
    private int memoizedSerializedSize = -1;
    public const int UninterpretedOptionFieldNumber = 999;

    private EnumOptions()
    {
    }

    public static EnumOptions DefaultInstance
    {
      get
      {
        return EnumOptions.defaultInstance;
      }
    }

    public override EnumOptions DefaultInstanceForType
    {
      get
      {
        return EnumOptions.DefaultInstance;
      }
    }

    protected override EnumOptions ThisMessage
    {
      get
      {
        return this;
      }
    }

    public static MessageDescriptor Descriptor
    {
      get
      {
        return DescriptorProtoFile.internal__static_google_protobuf_EnumOptions__Descriptor;
      }
    }

    protected override FieldAccessorTable<EnumOptions, EnumOptions.Builder> InternalFieldAccessors
    {
      get
      {
        return DescriptorProtoFile.internal__static_google_protobuf_EnumOptions__FieldAccessorTable;
      }
    }

    public IList<UninterpretedOption> UninterpretedOptionList
    {
      get
      {
        return (IList<UninterpretedOption>) this.uninterpretedOption_;
      }
    }

    public int UninterpretedOptionCount
    {
      get
      {
        return this.uninterpretedOption_.Count;
      }
    }

    public UninterpretedOption GetUninterpretedOption(int index)
    {
      return this.uninterpretedOption_[index];
    }

    public override bool IsInitialized
    {
      get
      {
        foreach (AbstractMessageLite<UninterpretedOption, UninterpretedOption.Builder> uninterpretedOption in (IEnumerable<UninterpretedOption>) this.UninterpretedOptionList)
        {
          if (!uninterpretedOption.IsInitialized)
            return false;
        }
        return this.ExtensionsAreInitialized;
      }
    }

    public override void WriteTo(ICodedOutputStream output)
    {
      int serializedSize = this.SerializedSize;
      string[] optionsFieldNames = EnumOptions._enumOptionsFieldNames;
      ExtendableMessage<EnumOptions, EnumOptions.Builder>.ExtensionWriter extensionWriter = this.CreateExtensionWriter((ExtendableMessage<EnumOptions, EnumOptions.Builder>) this);
      if (this.uninterpretedOption_.Count > 0)
        output.WriteMessageArray<UninterpretedOption>(999, optionsFieldNames[0], (IEnumerable<UninterpretedOption>) this.uninterpretedOption_);
      extensionWriter.WriteUntil(536870912, output);
      this.UnknownFields.WriteTo(output);
    }

    public override int SerializedSize
    {
      get
      {
        int memoizedSerializedSize = this.memoizedSerializedSize;
        if (memoizedSerializedSize != -1)
          return memoizedSerializedSize;
        int num1 = 0;
        foreach (UninterpretedOption uninterpretedOption in (IEnumerable<UninterpretedOption>) this.UninterpretedOptionList)
          num1 += CodedOutputStream.ComputeMessageSize(999, (IMessageLite) uninterpretedOption);
        int num2 = num1 + this.ExtensionsSerializedSize + this.UnknownFields.SerializedSize;
        this.memoizedSerializedSize = num2;
        return num2;
      }
    }

    public static EnumOptions ParseFrom(ByteString data)
    {
      return EnumOptions.CreateBuilder().MergeFrom(data).BuildParsed();
    }

    public static EnumOptions ParseFrom(ByteString data, ExtensionRegistry extensionRegistry)
    {
      return EnumOptions.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
    }

    public static EnumOptions ParseFrom(byte[] data)
    {
      return EnumOptions.CreateBuilder().MergeFrom(data).BuildParsed();
    }

    public static EnumOptions ParseFrom(byte[] data, ExtensionRegistry extensionRegistry)
    {
      return EnumOptions.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
    }

    public static EnumOptions ParseFrom(Stream input)
    {
      return EnumOptions.CreateBuilder().MergeFrom(input).BuildParsed();
    }

    public static EnumOptions ParseFrom(Stream input, ExtensionRegistry extensionRegistry)
    {
      return EnumOptions.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
    }

    public static EnumOptions ParseDelimitedFrom(Stream input)
    {
      return EnumOptions.CreateBuilder().MergeDelimitedFrom(input).BuildParsed();
    }

    public static EnumOptions ParseDelimitedFrom(Stream input, ExtensionRegistry extensionRegistry)
    {
      return EnumOptions.CreateBuilder().MergeDelimitedFrom(input, extensionRegistry).BuildParsed();
    }

    public static EnumOptions ParseFrom(ICodedInputStream input)
    {
      return EnumOptions.CreateBuilder().MergeFrom(input).BuildParsed();
    }

    public static EnumOptions ParseFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
    {
      return EnumOptions.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
    }

    private EnumOptions MakeReadOnly()
    {
      this.uninterpretedOption_.MakeReadOnly();
      return this;
    }

    public static EnumOptions.Builder CreateBuilder()
    {
      return new EnumOptions.Builder();
    }

    public override EnumOptions.Builder ToBuilder()
    {
      return EnumOptions.CreateBuilder(this);
    }

    public override EnumOptions.Builder CreateBuilderForType()
    {
      return new EnumOptions.Builder();
    }

    public static EnumOptions.Builder CreateBuilder(EnumOptions prototype)
    {
      return new EnumOptions.Builder(prototype);
    }

    static EnumOptions()
    {
      object.ReferenceEquals((object) DescriptorProtoFile.Descriptor, (object) null);
    }

    [DebuggerNonUserCode]
    public sealed class Builder : ExtendableBuilder<EnumOptions, EnumOptions.Builder>
    {
      private bool resultIsReadOnly;
      private EnumOptions result;

      protected override EnumOptions.Builder ThisBuilder
      {
        get
        {
          return this;
        }
      }

      public Builder()
      {
        this.result = EnumOptions.DefaultInstance;
        this.resultIsReadOnly = true;
      }

      internal Builder(EnumOptions cloneFrom)
      {
        this.result = cloneFrom;
        this.resultIsReadOnly = true;
      }

      private EnumOptions PrepareBuilder()
      {
        if (this.resultIsReadOnly)
        {
          EnumOptions result = this.result;
          this.result = new EnumOptions();
          this.resultIsReadOnly = false;
          this.MergeFrom(result);
        }
        return this.result;
      }

      public override bool IsInitialized
      {
        get
        {
          return this.result.IsInitialized;
        }
      }

      protected override EnumOptions MessageBeingBuilt
      {
        get
        {
          return this.PrepareBuilder();
        }
      }

      public override EnumOptions.Builder Clear()
      {
        this.result = EnumOptions.DefaultInstance;
        this.resultIsReadOnly = true;
        return this;
      }

      public override EnumOptions.Builder Clone()
      {
        if (this.resultIsReadOnly)
          return new EnumOptions.Builder(this.result);
        return new EnumOptions.Builder().MergeFrom(this.result);
      }

      public override MessageDescriptor DescriptorForType
      {
        get
        {
          return EnumOptions.Descriptor;
        }
      }

      public override EnumOptions DefaultInstanceForType
      {
        get
        {
          return EnumOptions.DefaultInstance;
        }
      }

      public override EnumOptions BuildPartial()
      {
        if (this.resultIsReadOnly)
          return this.result;
        this.resultIsReadOnly = true;
        return this.result.MakeReadOnly();
      }

      public override EnumOptions.Builder MergeFrom(IMessage other)
      {
        if (other is EnumOptions)
          return this.MergeFrom((EnumOptions) other);
        base.MergeFrom(other);
        return this;
      }

      public override EnumOptions.Builder MergeFrom(EnumOptions other)
      {
        if (other == EnumOptions.DefaultInstance)
          return this;
        this.PrepareBuilder();
        if (other.uninterpretedOption_.Count != 0)
          this.result.uninterpretedOption_.Add((IEnumerable<UninterpretedOption>) other.uninterpretedOption_);
        this.MergeExtensionFields((ExtendableMessage<EnumOptions, EnumOptions.Builder>) other);
        this.MergeUnknownFields(other.UnknownFields);
        return this;
      }

      public override EnumOptions.Builder MergeFrom(ICodedInputStream input)
      {
        return this.MergeFrom(input, ExtensionRegistry.Empty);
      }

      public override EnumOptions.Builder MergeFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
      {
        this.PrepareBuilder();
        UnknownFieldSet.Builder unknownFields = (UnknownFieldSet.Builder) null;
        uint fieldTag;
        string fieldName;
        while (input.ReadTag(out fieldTag, out fieldName))
        {
          if ((int) fieldTag == 0 && fieldName != null)
          {
            int index = Array.BinarySearch<string>(EnumOptions._enumOptionsFieldNames, fieldName, (IComparer<string>) StringComparer.Ordinal);
            if (index >= 0)
            {
              fieldTag = EnumOptions._enumOptionsFieldTags[index];
            }
            else
            {
              if (unknownFields == null)
                unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
              this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
              continue;
            }
          }
          switch (fieldTag)
          {
            case 0:
              throw InvalidProtocolBufferException.InvalidTag();
            case 7994:
              input.ReadMessageArray<UninterpretedOption>(fieldTag, fieldName, (ICollection<UninterpretedOption>) this.result.uninterpretedOption_, UninterpretedOption.DefaultInstance, extensionRegistry);
              continue;
            default:
              if (WireFormat.IsEndGroupTag(fieldTag))
              {
                if (unknownFields != null)
                  this.UnknownFields = unknownFields.Build();
                return this;
              }
              if (unknownFields == null)
                unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
              this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
              continue;
          }
        }
        if (unknownFields != null)
          this.UnknownFields = unknownFields.Build();
        return this;
      }

      public IPopsicleList<UninterpretedOption> UninterpretedOptionList
      {
        get
        {
          return (IPopsicleList<UninterpretedOption>) this.PrepareBuilder().uninterpretedOption_;
        }
      }

      public int UninterpretedOptionCount
      {
        get
        {
          return this.result.UninterpretedOptionCount;
        }
      }

      public UninterpretedOption GetUninterpretedOption(int index)
      {
        return this.result.GetUninterpretedOption(index);
      }

      public EnumOptions.Builder SetUninterpretedOption(int index, UninterpretedOption value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.uninterpretedOption_[index] = value;
        return this;
      }

      public EnumOptions.Builder SetUninterpretedOption(int index, UninterpretedOption.Builder builderForValue)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
        this.PrepareBuilder();
        this.result.uninterpretedOption_[index] = builderForValue.Build();
        return this;
      }

      public EnumOptions.Builder AddUninterpretedOption(UninterpretedOption value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.uninterpretedOption_.Add(value);
        return this;
      }

      public EnumOptions.Builder AddUninterpretedOption(UninterpretedOption.Builder builderForValue)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
        this.PrepareBuilder();
        this.result.uninterpretedOption_.Add(builderForValue.Build());
        return this;
      }

      public EnumOptions.Builder AddRangeUninterpretedOption(IEnumerable<UninterpretedOption> values)
      {
        this.PrepareBuilder();
        this.result.uninterpretedOption_.Add(values);
        return this;
      }

      public EnumOptions.Builder ClearUninterpretedOption()
      {
        this.PrepareBuilder();
        this.result.uninterpretedOption_.Clear();
        return this;
      }
    }
  }
}
