﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.DescriptorProtos.CSharpServiceOptions
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers.Descriptors;
using Google.ProtocolBuffers.FieldAccess;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace Google.ProtocolBuffers.DescriptorProtos
{
  [DebuggerNonUserCode]
  public sealed class CSharpServiceOptions : GeneratedMessage<CSharpServiceOptions, CSharpServiceOptions.Builder>
  {
    private static readonly CSharpServiceOptions defaultInstance = new CSharpServiceOptions().MakeReadOnly();
    private static readonly string[] _cSharpServiceOptionsFieldNames = new string[1]
    {
      "interface_id"
    };
    private static readonly uint[] _cSharpServiceOptionsFieldTags = new uint[1]
    {
      10U
    };
    private string interfaceId_ = "";
    private int memoizedSerializedSize = -1;
    public const int InterfaceIdFieldNumber = 1;
    private bool hasInterfaceId;

    private CSharpServiceOptions()
    {
    }

    public static CSharpServiceOptions DefaultInstance
    {
      get
      {
        return CSharpServiceOptions.defaultInstance;
      }
    }

    public override CSharpServiceOptions DefaultInstanceForType
    {
      get
      {
        return CSharpServiceOptions.DefaultInstance;
      }
    }

    protected override CSharpServiceOptions ThisMessage
    {
      get
      {
        return this;
      }
    }

    public static MessageDescriptor Descriptor
    {
      get
      {
        return CSharpOptions.internal__static_google_protobuf_CSharpServiceOptions__Descriptor;
      }
    }

    protected override FieldAccessorTable<CSharpServiceOptions, CSharpServiceOptions.Builder> InternalFieldAccessors
    {
      get
      {
        return CSharpOptions.internal__static_google_protobuf_CSharpServiceOptions__FieldAccessorTable;
      }
    }

    public bool HasInterfaceId
    {
      get
      {
        return this.hasInterfaceId;
      }
    }

    public string InterfaceId
    {
      get
      {
        return this.interfaceId_;
      }
    }

    public override bool IsInitialized
    {
      get
      {
        return true;
      }
    }

    public override void WriteTo(ICodedOutputStream output)
    {
      int serializedSize = this.SerializedSize;
      string[] optionsFieldNames = CSharpServiceOptions._cSharpServiceOptionsFieldNames;
      if (this.hasInterfaceId)
        output.WriteString(1, optionsFieldNames[0], this.InterfaceId);
      this.UnknownFields.WriteTo(output);
    }

    public override int SerializedSize
    {
      get
      {
        int memoizedSerializedSize = this.memoizedSerializedSize;
        if (memoizedSerializedSize != -1)
          return memoizedSerializedSize;
        int num1 = 0;
        if (this.hasInterfaceId)
          num1 += CodedOutputStream.ComputeStringSize(1, this.InterfaceId);
        int num2 = num1 + this.UnknownFields.SerializedSize;
        this.memoizedSerializedSize = num2;
        return num2;
      }
    }

    public static CSharpServiceOptions ParseFrom(ByteString data)
    {
      return CSharpServiceOptions.CreateBuilder().MergeFrom(data).BuildParsed();
    }

    public static CSharpServiceOptions ParseFrom(ByteString data, ExtensionRegistry extensionRegistry)
    {
      return CSharpServiceOptions.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
    }

    public static CSharpServiceOptions ParseFrom(byte[] data)
    {
      return CSharpServiceOptions.CreateBuilder().MergeFrom(data).BuildParsed();
    }

    public static CSharpServiceOptions ParseFrom(byte[] data, ExtensionRegistry extensionRegistry)
    {
      return CSharpServiceOptions.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
    }

    public static CSharpServiceOptions ParseFrom(Stream input)
    {
      return CSharpServiceOptions.CreateBuilder().MergeFrom(input).BuildParsed();
    }

    public static CSharpServiceOptions ParseFrom(Stream input, ExtensionRegistry extensionRegistry)
    {
      return CSharpServiceOptions.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
    }

    public static CSharpServiceOptions ParseDelimitedFrom(Stream input)
    {
      return CSharpServiceOptions.CreateBuilder().MergeDelimitedFrom(input).BuildParsed();
    }

    public static CSharpServiceOptions ParseDelimitedFrom(Stream input, ExtensionRegistry extensionRegistry)
    {
      return CSharpServiceOptions.CreateBuilder().MergeDelimitedFrom(input, extensionRegistry).BuildParsed();
    }

    public static CSharpServiceOptions ParseFrom(ICodedInputStream input)
    {
      return CSharpServiceOptions.CreateBuilder().MergeFrom(input).BuildParsed();
    }

    public static CSharpServiceOptions ParseFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
    {
      return CSharpServiceOptions.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
    }

    private CSharpServiceOptions MakeReadOnly()
    {
      return this;
    }

    public static CSharpServiceOptions.Builder CreateBuilder()
    {
      return new CSharpServiceOptions.Builder();
    }

    public override CSharpServiceOptions.Builder ToBuilder()
    {
      return CSharpServiceOptions.CreateBuilder(this);
    }

    public override CSharpServiceOptions.Builder CreateBuilderForType()
    {
      return new CSharpServiceOptions.Builder();
    }

    public static CSharpServiceOptions.Builder CreateBuilder(CSharpServiceOptions prototype)
    {
      return new CSharpServiceOptions.Builder(prototype);
    }

    static CSharpServiceOptions()
    {
      object.ReferenceEquals((object) CSharpOptions.Descriptor, (object) null);
    }

    [DebuggerNonUserCode]
    public sealed class Builder : GeneratedBuilder<CSharpServiceOptions, CSharpServiceOptions.Builder>
    {
      private bool resultIsReadOnly;
      private CSharpServiceOptions result;

      protected override CSharpServiceOptions.Builder ThisBuilder
      {
        get
        {
          return this;
        }
      }

      public Builder()
      {
        this.result = CSharpServiceOptions.DefaultInstance;
        this.resultIsReadOnly = true;
      }

      internal Builder(CSharpServiceOptions cloneFrom)
      {
        this.result = cloneFrom;
        this.resultIsReadOnly = true;
      }

      private CSharpServiceOptions PrepareBuilder()
      {
        if (this.resultIsReadOnly)
        {
          CSharpServiceOptions result = this.result;
          this.result = new CSharpServiceOptions();
          this.resultIsReadOnly = false;
          this.MergeFrom(result);
        }
        return this.result;
      }

      public override bool IsInitialized
      {
        get
        {
          return this.result.IsInitialized;
        }
      }

      protected override CSharpServiceOptions MessageBeingBuilt
      {
        get
        {
          return this.PrepareBuilder();
        }
      }

      public override CSharpServiceOptions.Builder Clear()
      {
        this.result = CSharpServiceOptions.DefaultInstance;
        this.resultIsReadOnly = true;
        return this;
      }

      public override CSharpServiceOptions.Builder Clone()
      {
        if (this.resultIsReadOnly)
          return new CSharpServiceOptions.Builder(this.result);
        return new CSharpServiceOptions.Builder().MergeFrom(this.result);
      }

      public override MessageDescriptor DescriptorForType
      {
        get
        {
          return CSharpServiceOptions.Descriptor;
        }
      }

      public override CSharpServiceOptions DefaultInstanceForType
      {
        get
        {
          return CSharpServiceOptions.DefaultInstance;
        }
      }

      public override CSharpServiceOptions BuildPartial()
      {
        if (this.resultIsReadOnly)
          return this.result;
        this.resultIsReadOnly = true;
        return this.result.MakeReadOnly();
      }

      public override CSharpServiceOptions.Builder MergeFrom(IMessage other)
      {
        if (other is CSharpServiceOptions)
          return this.MergeFrom((CSharpServiceOptions) other);
        base.MergeFrom(other);
        return this;
      }

      public override CSharpServiceOptions.Builder MergeFrom(CSharpServiceOptions other)
      {
        if (other == CSharpServiceOptions.DefaultInstance)
          return this;
        this.PrepareBuilder();
        if (other.HasInterfaceId)
          this.InterfaceId = other.InterfaceId;
        this.MergeUnknownFields(other.UnknownFields);
        return this;
      }

      public override CSharpServiceOptions.Builder MergeFrom(ICodedInputStream input)
      {
        return this.MergeFrom(input, ExtensionRegistry.Empty);
      }

      public override CSharpServiceOptions.Builder MergeFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
      {
        this.PrepareBuilder();
        UnknownFieldSet.Builder unknownFields = (UnknownFieldSet.Builder) null;
        uint fieldTag;
        string fieldName;
        while (input.ReadTag(out fieldTag, out fieldName))
        {
          if ((int) fieldTag == 0 && fieldName != null)
          {
            int index = Array.BinarySearch<string>(CSharpServiceOptions._cSharpServiceOptionsFieldNames, fieldName, (IComparer<string>) StringComparer.Ordinal);
            if (index >= 0)
            {
              fieldTag = CSharpServiceOptions._cSharpServiceOptionsFieldTags[index];
            }
            else
            {
              if (unknownFields == null)
                unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
              this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
              continue;
            }
          }
          switch (fieldTag)
          {
            case 0:
              throw InvalidProtocolBufferException.InvalidTag();
            case 10:
              this.result.hasInterfaceId = input.ReadString(ref this.result.interfaceId_);
              continue;
            default:
              if (WireFormat.IsEndGroupTag(fieldTag))
              {
                if (unknownFields != null)
                  this.UnknownFields = unknownFields.Build();
                return this;
              }
              if (unknownFields == null)
                unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
              this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
              continue;
          }
        }
        if (unknownFields != null)
          this.UnknownFields = unknownFields.Build();
        return this;
      }

      public bool HasInterfaceId
      {
        get
        {
          return this.result.hasInterfaceId;
        }
      }

      public string InterfaceId
      {
        get
        {
          return this.result.InterfaceId;
        }
        set
        {
          this.SetInterfaceId(value);
        }
      }

      public CSharpServiceOptions.Builder SetInterfaceId(string value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.hasInterfaceId = true;
        this.result.interfaceId_ = value;
        return this;
      }

      public CSharpServiceOptions.Builder ClearInterfaceId()
      {
        this.PrepareBuilder();
        this.result.hasInterfaceId = false;
        this.result.interfaceId_ = "";
        return this;
      }
    }
  }
}
