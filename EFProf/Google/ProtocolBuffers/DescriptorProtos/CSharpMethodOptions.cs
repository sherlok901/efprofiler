﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.DescriptorProtos.CSharpMethodOptions
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers.Descriptors;
using Google.ProtocolBuffers.FieldAccess;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace Google.ProtocolBuffers.DescriptorProtos
{
  [DebuggerNonUserCode]
  public sealed class CSharpMethodOptions : GeneratedMessage<CSharpMethodOptions, CSharpMethodOptions.Builder>
  {
    private static readonly CSharpMethodOptions defaultInstance = new CSharpMethodOptions().MakeReadOnly();
    private static readonly string[] _cSharpMethodOptionsFieldNames = new string[1]
    {
      "dispatch_id"
    };
    private static readonly uint[] _cSharpMethodOptionsFieldTags = new uint[1]
    {
      8U
    };
    private int memoizedSerializedSize = -1;
    public const int DispatchIdFieldNumber = 1;
    private bool hasDispatchId;
    private int dispatchId_;

    private CSharpMethodOptions()
    {
    }

    public static CSharpMethodOptions DefaultInstance
    {
      get
      {
        return CSharpMethodOptions.defaultInstance;
      }
    }

    public override CSharpMethodOptions DefaultInstanceForType
    {
      get
      {
        return CSharpMethodOptions.DefaultInstance;
      }
    }

    protected override CSharpMethodOptions ThisMessage
    {
      get
      {
        return this;
      }
    }

    public static MessageDescriptor Descriptor
    {
      get
      {
        return CSharpOptions.internal__static_google_protobuf_CSharpMethodOptions__Descriptor;
      }
    }

    protected override FieldAccessorTable<CSharpMethodOptions, CSharpMethodOptions.Builder> InternalFieldAccessors
    {
      get
      {
        return CSharpOptions.internal__static_google_protobuf_CSharpMethodOptions__FieldAccessorTable;
      }
    }

    public bool HasDispatchId
    {
      get
      {
        return this.hasDispatchId;
      }
    }

    public int DispatchId
    {
      get
      {
        return this.dispatchId_;
      }
    }

    public override bool IsInitialized
    {
      get
      {
        return true;
      }
    }

    public override void WriteTo(ICodedOutputStream output)
    {
      int serializedSize = this.SerializedSize;
      string[] optionsFieldNames = CSharpMethodOptions._cSharpMethodOptionsFieldNames;
      if (this.hasDispatchId)
        output.WriteInt32(1, optionsFieldNames[0], this.DispatchId);
      this.UnknownFields.WriteTo(output);
    }

    public override int SerializedSize
    {
      get
      {
        int memoizedSerializedSize = this.memoizedSerializedSize;
        if (memoizedSerializedSize != -1)
          return memoizedSerializedSize;
        int num1 = 0;
        if (this.hasDispatchId)
          num1 += CodedOutputStream.ComputeInt32Size(1, this.DispatchId);
        int num2 = num1 + this.UnknownFields.SerializedSize;
        this.memoizedSerializedSize = num2;
        return num2;
      }
    }

    public static CSharpMethodOptions ParseFrom(ByteString data)
    {
      return CSharpMethodOptions.CreateBuilder().MergeFrom(data).BuildParsed();
    }

    public static CSharpMethodOptions ParseFrom(ByteString data, ExtensionRegistry extensionRegistry)
    {
      return CSharpMethodOptions.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
    }

    public static CSharpMethodOptions ParseFrom(byte[] data)
    {
      return CSharpMethodOptions.CreateBuilder().MergeFrom(data).BuildParsed();
    }

    public static CSharpMethodOptions ParseFrom(byte[] data, ExtensionRegistry extensionRegistry)
    {
      return CSharpMethodOptions.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
    }

    public static CSharpMethodOptions ParseFrom(Stream input)
    {
      return CSharpMethodOptions.CreateBuilder().MergeFrom(input).BuildParsed();
    }

    public static CSharpMethodOptions ParseFrom(Stream input, ExtensionRegistry extensionRegistry)
    {
      return CSharpMethodOptions.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
    }

    public static CSharpMethodOptions ParseDelimitedFrom(Stream input)
    {
      return CSharpMethodOptions.CreateBuilder().MergeDelimitedFrom(input).BuildParsed();
    }

    public static CSharpMethodOptions ParseDelimitedFrom(Stream input, ExtensionRegistry extensionRegistry)
    {
      return CSharpMethodOptions.CreateBuilder().MergeDelimitedFrom(input, extensionRegistry).BuildParsed();
    }

    public static CSharpMethodOptions ParseFrom(ICodedInputStream input)
    {
      return CSharpMethodOptions.CreateBuilder().MergeFrom(input).BuildParsed();
    }

    public static CSharpMethodOptions ParseFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
    {
      return CSharpMethodOptions.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
    }

    private CSharpMethodOptions MakeReadOnly()
    {
      return this;
    }

    public static CSharpMethodOptions.Builder CreateBuilder()
    {
      return new CSharpMethodOptions.Builder();
    }

    public override CSharpMethodOptions.Builder ToBuilder()
    {
      return CSharpMethodOptions.CreateBuilder(this);
    }

    public override CSharpMethodOptions.Builder CreateBuilderForType()
    {
      return new CSharpMethodOptions.Builder();
    }

    public static CSharpMethodOptions.Builder CreateBuilder(CSharpMethodOptions prototype)
    {
      return new CSharpMethodOptions.Builder(prototype);
    }

    static CSharpMethodOptions()
    {
      object.ReferenceEquals((object) CSharpOptions.Descriptor, (object) null);
    }

    [DebuggerNonUserCode]
    public sealed class Builder : GeneratedBuilder<CSharpMethodOptions, CSharpMethodOptions.Builder>
    {
      private bool resultIsReadOnly;
      private CSharpMethodOptions result;

      protected override CSharpMethodOptions.Builder ThisBuilder
      {
        get
        {
          return this;
        }
      }

      public Builder()
      {
        this.result = CSharpMethodOptions.DefaultInstance;
        this.resultIsReadOnly = true;
      }

      internal Builder(CSharpMethodOptions cloneFrom)
      {
        this.result = cloneFrom;
        this.resultIsReadOnly = true;
      }

      private CSharpMethodOptions PrepareBuilder()
      {
        if (this.resultIsReadOnly)
        {
          CSharpMethodOptions result = this.result;
          this.result = new CSharpMethodOptions();
          this.resultIsReadOnly = false;
          this.MergeFrom(result);
        }
        return this.result;
      }

      public override bool IsInitialized
      {
        get
        {
          return this.result.IsInitialized;
        }
      }

      protected override CSharpMethodOptions MessageBeingBuilt
      {
        get
        {
          return this.PrepareBuilder();
        }
      }

      public override CSharpMethodOptions.Builder Clear()
      {
        this.result = CSharpMethodOptions.DefaultInstance;
        this.resultIsReadOnly = true;
        return this;
      }

      public override CSharpMethodOptions.Builder Clone()
      {
        if (this.resultIsReadOnly)
          return new CSharpMethodOptions.Builder(this.result);
        return new CSharpMethodOptions.Builder().MergeFrom(this.result);
      }

      public override MessageDescriptor DescriptorForType
      {
        get
        {
          return CSharpMethodOptions.Descriptor;
        }
      }

      public override CSharpMethodOptions DefaultInstanceForType
      {
        get
        {
          return CSharpMethodOptions.DefaultInstance;
        }
      }

      public override CSharpMethodOptions BuildPartial()
      {
        if (this.resultIsReadOnly)
          return this.result;
        this.resultIsReadOnly = true;
        return this.result.MakeReadOnly();
      }

      public override CSharpMethodOptions.Builder MergeFrom(IMessage other)
      {
        if (other is CSharpMethodOptions)
          return this.MergeFrom((CSharpMethodOptions) other);
        base.MergeFrom(other);
        return this;
      }

      public override CSharpMethodOptions.Builder MergeFrom(CSharpMethodOptions other)
      {
        if (other == CSharpMethodOptions.DefaultInstance)
          return this;
        this.PrepareBuilder();
        if (other.HasDispatchId)
          this.DispatchId = other.DispatchId;
        this.MergeUnknownFields(other.UnknownFields);
        return this;
      }

      public override CSharpMethodOptions.Builder MergeFrom(ICodedInputStream input)
      {
        return this.MergeFrom(input, ExtensionRegistry.Empty);
      }

      public override CSharpMethodOptions.Builder MergeFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
      {
        this.PrepareBuilder();
        UnknownFieldSet.Builder unknownFields = (UnknownFieldSet.Builder) null;
        uint fieldTag;
        string fieldName;
        while (input.ReadTag(out fieldTag, out fieldName))
        {
          if ((int) fieldTag == 0 && fieldName != null)
          {
            int index = Array.BinarySearch<string>(CSharpMethodOptions._cSharpMethodOptionsFieldNames, fieldName, (IComparer<string>) StringComparer.Ordinal);
            if (index >= 0)
            {
              fieldTag = CSharpMethodOptions._cSharpMethodOptionsFieldTags[index];
            }
            else
            {
              if (unknownFields == null)
                unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
              this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
              continue;
            }
          }
          switch (fieldTag)
          {
            case 0:
              throw InvalidProtocolBufferException.InvalidTag();
            case 8:
              this.result.hasDispatchId = input.ReadInt32(ref this.result.dispatchId_);
              continue;
            default:
              if (WireFormat.IsEndGroupTag(fieldTag))
              {
                if (unknownFields != null)
                  this.UnknownFields = unknownFields.Build();
                return this;
              }
              if (unknownFields == null)
                unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
              this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
              continue;
          }
        }
        if (unknownFields != null)
          this.UnknownFields = unknownFields.Build();
        return this;
      }

      public bool HasDispatchId
      {
        get
        {
          return this.result.hasDispatchId;
        }
      }

      public int DispatchId
      {
        get
        {
          return this.result.DispatchId;
        }
        set
        {
          this.SetDispatchId(value);
        }
      }

      public CSharpMethodOptions.Builder SetDispatchId(int value)
      {
        this.PrepareBuilder();
        this.result.hasDispatchId = true;
        this.result.dispatchId_ = value;
        return this;
      }

      public CSharpMethodOptions.Builder ClearDispatchId()
      {
        this.PrepareBuilder();
        this.result.hasDispatchId = false;
        this.result.dispatchId_ = 0;
        return this;
      }
    }
  }
}
