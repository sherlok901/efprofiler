﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.DescriptorProtos.DescriptorProtoFile
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers.Descriptors;
using Google.ProtocolBuffers.FieldAccess;
using System;
using System.Diagnostics;

namespace Google.ProtocolBuffers.DescriptorProtos
{
  [DebuggerNonUserCode]
  public static class DescriptorProtoFile
  {
    internal static MessageDescriptor internal__static_google_protobuf_FileDescriptorSet__Descriptor;
    internal static FieldAccessorTable<FileDescriptorSet, FileDescriptorSet.Builder> internal__static_google_protobuf_FileDescriptorSet__FieldAccessorTable;
    internal static MessageDescriptor internal__static_google_protobuf_FileDescriptorProto__Descriptor;
    internal static FieldAccessorTable<FileDescriptorProto, FileDescriptorProto.Builder> internal__static_google_protobuf_FileDescriptorProto__FieldAccessorTable;
    internal static MessageDescriptor internal__static_google_protobuf_DescriptorProto__Descriptor;
    internal static FieldAccessorTable<DescriptorProto, DescriptorProto.Builder> internal__static_google_protobuf_DescriptorProto__FieldAccessorTable;
    internal static MessageDescriptor internal__static_google_protobuf_DescriptorProto_ExtensionRange__Descriptor;
    internal static FieldAccessorTable<DescriptorProto.Types.ExtensionRange, DescriptorProto.Types.ExtensionRange.Builder> internal__static_google_protobuf_DescriptorProto_ExtensionRange__FieldAccessorTable;
    internal static MessageDescriptor internal__static_google_protobuf_FieldDescriptorProto__Descriptor;
    internal static FieldAccessorTable<FieldDescriptorProto, FieldDescriptorProto.Builder> internal__static_google_protobuf_FieldDescriptorProto__FieldAccessorTable;
    internal static MessageDescriptor internal__static_google_protobuf_EnumDescriptorProto__Descriptor;
    internal static FieldAccessorTable<EnumDescriptorProto, EnumDescriptorProto.Builder> internal__static_google_protobuf_EnumDescriptorProto__FieldAccessorTable;
    internal static MessageDescriptor internal__static_google_protobuf_EnumValueDescriptorProto__Descriptor;
    internal static FieldAccessorTable<EnumValueDescriptorProto, EnumValueDescriptorProto.Builder> internal__static_google_protobuf_EnumValueDescriptorProto__FieldAccessorTable;
    internal static MessageDescriptor internal__static_google_protobuf_ServiceDescriptorProto__Descriptor;
    internal static FieldAccessorTable<ServiceDescriptorProto, ServiceDescriptorProto.Builder> internal__static_google_protobuf_ServiceDescriptorProto__FieldAccessorTable;
    internal static MessageDescriptor internal__static_google_protobuf_MethodDescriptorProto__Descriptor;
    internal static FieldAccessorTable<MethodDescriptorProto, MethodDescriptorProto.Builder> internal__static_google_protobuf_MethodDescriptorProto__FieldAccessorTable;
    internal static MessageDescriptor internal__static_google_protobuf_FileOptions__Descriptor;
    internal static FieldAccessorTable<FileOptions, FileOptions.Builder> internal__static_google_protobuf_FileOptions__FieldAccessorTable;
    internal static MessageDescriptor internal__static_google_protobuf_MessageOptions__Descriptor;
    internal static FieldAccessorTable<MessageOptions, MessageOptions.Builder> internal__static_google_protobuf_MessageOptions__FieldAccessorTable;
    internal static MessageDescriptor internal__static_google_protobuf_FieldOptions__Descriptor;
    internal static FieldAccessorTable<FieldOptions, FieldOptions.Builder> internal__static_google_protobuf_FieldOptions__FieldAccessorTable;
    internal static MessageDescriptor internal__static_google_protobuf_EnumOptions__Descriptor;
    internal static FieldAccessorTable<EnumOptions, EnumOptions.Builder> internal__static_google_protobuf_EnumOptions__FieldAccessorTable;
    internal static MessageDescriptor internal__static_google_protobuf_EnumValueOptions__Descriptor;
    internal static FieldAccessorTable<EnumValueOptions, EnumValueOptions.Builder> internal__static_google_protobuf_EnumValueOptions__FieldAccessorTable;
    internal static MessageDescriptor internal__static_google_protobuf_ServiceOptions__Descriptor;
    internal static FieldAccessorTable<ServiceOptions, ServiceOptions.Builder> internal__static_google_protobuf_ServiceOptions__FieldAccessorTable;
    internal static MessageDescriptor internal__static_google_protobuf_MethodOptions__Descriptor;
    internal static FieldAccessorTable<MethodOptions, MethodOptions.Builder> internal__static_google_protobuf_MethodOptions__FieldAccessorTable;
    internal static MessageDescriptor internal__static_google_protobuf_UninterpretedOption__Descriptor;
    internal static FieldAccessorTable<UninterpretedOption, UninterpretedOption.Builder> internal__static_google_protobuf_UninterpretedOption__FieldAccessorTable;
    internal static MessageDescriptor internal__static_google_protobuf_UninterpretedOption_NamePart__Descriptor;
    internal static FieldAccessorTable<UninterpretedOption.Types.NamePart, UninterpretedOption.Types.NamePart.Builder> internal__static_google_protobuf_UninterpretedOption_NamePart__FieldAccessorTable;
    internal static MessageDescriptor internal__static_google_protobuf_SourceCodeInfo__Descriptor;
    internal static FieldAccessorTable<SourceCodeInfo, SourceCodeInfo.Builder> internal__static_google_protobuf_SourceCodeInfo__FieldAccessorTable;
    internal static MessageDescriptor internal__static_google_protobuf_SourceCodeInfo_Location__Descriptor;
    internal static FieldAccessorTable<SourceCodeInfo.Types.Location, SourceCodeInfo.Types.Location.Builder> internal__static_google_protobuf_SourceCodeInfo_Location__FieldAccessorTable;
    private static FileDescriptor descriptor;

    public static void RegisterAllExtensions(ExtensionRegistry registry)
    {
    }

    public static FileDescriptor Descriptor
    {
      get
      {
        return DescriptorProtoFile.descriptor;
      }
    }

    static DescriptorProtoFile()
    {
      FileDescriptor.InternalBuildGeneratedFileFrom(Convert.FromBase64String("CiBnb29nbGUvcHJvdG9idWYvZGVzY3JpcHRvci5wcm90bxIPZ29vZ2xlLnBy" + "b3RvYnVmIkcKEUZpbGVEZXNjcmlwdG9yU2V0EjIKBGZpbGUYASADKAsyJC5n" + "b29nbGUucHJvdG9idWYuRmlsZURlc2NyaXB0b3JQcm90byKXAwoTRmlsZURl" + "c2NyaXB0b3JQcm90bxIMCgRuYW1lGAEgASgJEg8KB3BhY2thZ2UYAiABKAkS" + "EgoKZGVwZW5kZW5jeRgDIAMoCRI2CgxtZXNzYWdlX3R5cGUYBCADKAsyIC5n" + "b29nbGUucHJvdG9idWYuRGVzY3JpcHRvclByb3RvEjcKCWVudW1fdHlwZRgF" + "IAMoCzIkLmdvb2dsZS5wcm90b2J1Zi5FbnVtRGVzY3JpcHRvclByb3RvEjgK" + "B3NlcnZpY2UYBiADKAsyJy5nb29nbGUucHJvdG9idWYuU2VydmljZURlc2Ny" + "aXB0b3JQcm90bxI4CglleHRlbnNpb24YByADKAsyJS5nb29nbGUucHJvdG9i" + "dWYuRmllbGREZXNjcmlwdG9yUHJvdG8SLQoHb3B0aW9ucxgIIAEoCzIcLmdv" + "b2dsZS5wcm90b2J1Zi5GaWxlT3B0aW9ucxI5ChBzb3VyY2VfY29kZV9pbmZv" + "GAkgASgLMh8uZ29vZ2xlLnByb3RvYnVmLlNvdXJjZUNvZGVJbmZvIqkDCg9E" + "ZXNjcmlwdG9yUHJvdG8SDAoEbmFtZRgBIAEoCRI0CgVmaWVsZBgCIAMoCzIl" + "Lmdvb2dsZS5wcm90b2J1Zi5GaWVsZERlc2NyaXB0b3JQcm90bxI4CglleHRl" + "bnNpb24YBiADKAsyJS5nb29nbGUucHJvdG9idWYuRmllbGREZXNjcmlwdG9y" + "UHJvdG8SNQoLbmVzdGVkX3R5cGUYAyADKAsyIC5nb29nbGUucHJvdG9idWYu" + "RGVzY3JpcHRvclByb3RvEjcKCWVudW1fdHlwZRgEIAMoCzIkLmdvb2dsZS5w" + "cm90b2J1Zi5FbnVtRGVzY3JpcHRvclByb3RvEkgKD2V4dGVuc2lvbl9yYW5n" + "ZRgFIAMoCzIvLmdvb2dsZS5wcm90b2J1Zi5EZXNjcmlwdG9yUHJvdG8uRXh0" + "ZW5zaW9uUmFuZ2USMAoHb3B0aW9ucxgHIAEoCzIfLmdvb2dsZS5wcm90b2J1" + "Zi5NZXNzYWdlT3B0aW9ucxosCg5FeHRlbnNpb25SYW5nZRINCgVzdGFydBgB" + "IAEoBRILCgNlbmQYAiABKAUilAUKFEZpZWxkRGVzY3JpcHRvclByb3RvEgwK" + "BG5hbWUYASABKAkSDgoGbnVtYmVyGAMgASgFEjoKBWxhYmVsGAQgASgOMisu" + "Z29vZ2xlLnByb3RvYnVmLkZpZWxkRGVzY3JpcHRvclByb3RvLkxhYmVsEjgK" + "BHR5cGUYBSABKA4yKi5nb29nbGUucHJvdG9idWYuRmllbGREZXNjcmlwdG9y" + "UHJvdG8uVHlwZRIRCgl0eXBlX25hbWUYBiABKAkSEAoIZXh0ZW5kZWUYAiAB" + "KAkSFQoNZGVmYXVsdF92YWx1ZRgHIAEoCRIuCgdvcHRpb25zGAggASgLMh0u" + "Z29vZ2xlLnByb3RvYnVmLkZpZWxkT3B0aW9ucyK2AgoEVHlwZRIPCgtUWVBF" + "X0RPVUJMRRABEg4KClRZUEVfRkxPQVQQAhIOCgpUWVBFX0lOVDY0EAMSDwoL" + "VFlQRV9VSU5UNjQQBBIOCgpUWVBFX0lOVDMyEAUSEAoMVFlQRV9GSVhFRDY0" + "EAYSEAoMVFlQRV9GSVhFRDMyEAcSDQoJVFlQRV9CT09MEAgSDwoLVFlQRV9T" + "VFJJTkcQCRIOCgpUWVBFX0dST1VQEAoSEAoMVFlQRV9NRVNTQUdFEAsSDgoK" + "VFlQRV9CWVRFUxAMEg8KC1RZUEVfVUlOVDMyEA0SDQoJVFlQRV9FTlVNEA4S" + "EQoNVFlQRV9TRklYRUQzMhAPEhEKDVRZUEVfU0ZJWEVENjQQEBIPCgtUWVBF" + "X1NJTlQzMhAREg8KC1RZUEVfU0lOVDY0EBIiQwoFTGFiZWwSEgoOTEFCRUxf" + "T1BUSU9OQUwQARISCg5MQUJFTF9SRVFVSVJFRBACEhIKDkxBQkVMX1JFUEVB" + "VEVEEAMijAEKE0VudW1EZXNjcmlwdG9yUHJvdG8SDAoEbmFtZRgBIAEoCRI4" + "CgV2YWx1ZRgCIAMoCzIpLmdvb2dsZS5wcm90b2J1Zi5FbnVtVmFsdWVEZXNj" + "cmlwdG9yUHJvdG8SLQoHb3B0aW9ucxgDIAEoCzIcLmdvb2dsZS5wcm90b2J1" + "Zi5FbnVtT3B0aW9ucyJsChhFbnVtVmFsdWVEZXNjcmlwdG9yUHJvdG8SDAoE" + "bmFtZRgBIAEoCRIOCgZudW1iZXIYAiABKAUSMgoHb3B0aW9ucxgDIAEoCzIh" + "Lmdvb2dsZS5wcm90b2J1Zi5FbnVtVmFsdWVPcHRpb25zIpABChZTZXJ2aWNl" + "RGVzY3JpcHRvclByb3RvEgwKBG5hbWUYASABKAkSNgoGbWV0aG9kGAIgAygL" + "MiYuZ29vZ2xlLnByb3RvYnVmLk1ldGhvZERlc2NyaXB0b3JQcm90bxIwCgdv" + "cHRpb25zGAMgASgLMh8uZ29vZ2xlLnByb3RvYnVmLlNlcnZpY2VPcHRpb25z" + "In8KFU1ldGhvZERlc2NyaXB0b3JQcm90bxIMCgRuYW1lGAEgASgJEhIKCmlu" + "cHV0X3R5cGUYAiABKAkSEwoLb3V0cHV0X3R5cGUYAyABKAkSLwoHb3B0aW9u" + "cxgEIAEoCzIeLmdvb2dsZS5wcm90b2J1Zi5NZXRob2RPcHRpb25zItUDCgtG" + "aWxlT3B0aW9ucxIUCgxqYXZhX3BhY2thZ2UYASABKAkSHAoUamF2YV9vdXRl" + "cl9jbGFzc25hbWUYCCABKAkSIgoTamF2YV9tdWx0aXBsZV9maWxlcxgKIAEo" + "CDoFZmFsc2USLAodamF2YV9nZW5lcmF0ZV9lcXVhbHNfYW5kX2hhc2gYFCAB" + "KAg6BWZhbHNlEkYKDG9wdGltaXplX2ZvchgJIAEoDjIpLmdvb2dsZS5wcm90" + "b2J1Zi5GaWxlT3B0aW9ucy5PcHRpbWl6ZU1vZGU6BVNQRUVEEiIKE2NjX2dl" + "bmVyaWNfc2VydmljZXMYECABKAg6BWZhbHNlEiQKFWphdmFfZ2VuZXJpY19z" + "ZXJ2aWNlcxgRIAEoCDoFZmFsc2USIgoTcHlfZ2VuZXJpY19zZXJ2aWNlcxgS" + "IAEoCDoFZmFsc2USQwoUdW5pbnRlcnByZXRlZF9vcHRpb24Y5wcgAygLMiQu" + "Z29vZ2xlLnByb3RvYnVmLlVuaW50ZXJwcmV0ZWRPcHRpb24iOgoMT3B0aW1p" + "emVNb2RlEgkKBVNQRUVEEAESDQoJQ09ERV9TSVpFEAISEAoMTElURV9SVU5U" + "SU1FEAMqCQjoBxCAgICAAiK4AQoOTWVzc2FnZU9wdGlvbnMSJgoXbWVzc2Fn" + "ZV9zZXRfd2lyZV9mb3JtYXQYASABKAg6BWZhbHNlEi4KH25vX3N0YW5kYXJk" + "X2Rlc2NyaXB0b3JfYWNjZXNzb3IYAiABKAg6BWZhbHNlEkMKFHVuaW50ZXJw" + "cmV0ZWRfb3B0aW9uGOcHIAMoCzIkLmdvb2dsZS5wcm90b2J1Zi5VbmludGVy" + "cHJldGVkT3B0aW9uKgkI6AcQgICAgAIilAIKDEZpZWxkT3B0aW9ucxI6CgVj" + "dHlwZRgBIAEoDjIjLmdvb2dsZS5wcm90b2J1Zi5GaWVsZE9wdGlvbnMuQ1R5" + "cGU6BlNUUklORxIOCgZwYWNrZWQYAiABKAgSGQoKZGVwcmVjYXRlZBgDIAEo" + "CDoFZmFsc2USHAoUZXhwZXJpbWVudGFsX21hcF9rZXkYCSABKAkSQwoUdW5p" + "bnRlcnByZXRlZF9vcHRpb24Y5wcgAygLMiQuZ29vZ2xlLnByb3RvYnVmLlVu" + "aW50ZXJwcmV0ZWRPcHRpb24iLwoFQ1R5cGUSCgoGU1RSSU5HEAASCAoEQ09S" + "RBABEhAKDFNUUklOR19QSUVDRRACKgkI6AcQgICAgAIiXQoLRW51bU9wdGlv" + "bnMSQwoUdW5pbnRlcnByZXRlZF9vcHRpb24Y5wcgAygLMiQuZ29vZ2xlLnBy" + "b3RvYnVmLlVuaW50ZXJwcmV0ZWRPcHRpb24qCQjoBxCAgICAAiJiChBFbnVt" + "VmFsdWVPcHRpb25zEkMKFHVuaW50ZXJwcmV0ZWRfb3B0aW9uGOcHIAMoCzIk" + "Lmdvb2dsZS5wcm90b2J1Zi5VbmludGVycHJldGVkT3B0aW9uKgkI6AcQgICA" + "gAIiYAoOU2VydmljZU9wdGlvbnMSQwoUdW5pbnRlcnByZXRlZF9vcHRpb24Y" + "5wcgAygLMiQuZ29vZ2xlLnByb3RvYnVmLlVuaW50ZXJwcmV0ZWRPcHRpb24q" + "CQjoBxCAgICAAiJfCg1NZXRob2RPcHRpb25zEkMKFHVuaW50ZXJwcmV0ZWRf" + "b3B0aW9uGOcHIAMoCzIkLmdvb2dsZS5wcm90b2J1Zi5VbmludGVycHJldGVk" + "T3B0aW9uKgkI6AcQgICAgAIingIKE1VuaW50ZXJwcmV0ZWRPcHRpb24SOwoE" + "bmFtZRgCIAMoCzItLmdvb2dsZS5wcm90b2J1Zi5VbmludGVycHJldGVkT3B0" + "aW9uLk5hbWVQYXJ0EhgKEGlkZW50aWZpZXJfdmFsdWUYAyABKAkSGgoScG9z" + "aXRpdmVfaW50X3ZhbHVlGAQgASgEEhoKEm5lZ2F0aXZlX2ludF92YWx1ZRgF" + "IAEoAxIUCgxkb3VibGVfdmFsdWUYBiABKAESFAoMc3RyaW5nX3ZhbHVlGAcg" + "ASgMEhcKD2FnZ3JlZ2F0ZV92YWx1ZRgIIAEoCRozCghOYW1lUGFydBIRCglu" + "YW1lX3BhcnQYASACKAkSFAoMaXNfZXh0ZW5zaW9uGAIgAigIInwKDlNvdXJj" + "ZUNvZGVJbmZvEjoKCGxvY2F0aW9uGAEgAygLMiguZ29vZ2xlLnByb3RvYnVm" + "LlNvdXJjZUNvZGVJbmZvLkxvY2F0aW9uGi4KCExvY2F0aW9uEhAKBHBhdGgY" + "ASADKAVCAhABEhAKBHNwYW4YAiADKAVCAhABQikKE2NvbS5nb29nbGUucHJv" + "dG9idWZCEERlc2NyaXB0b3JQcm90b3NIAQ=="), new FileDescriptor[0], (FileDescriptor.InternalDescriptorAssigner) (root =>
      {
        DescriptorProtoFile.descriptor = root;
        DescriptorProtoFile.internal__static_google_protobuf_FileDescriptorSet__Descriptor = DescriptorProtoFile.Descriptor.MessageTypes[0];
        DescriptorProtoFile.internal__static_google_protobuf_FileDescriptorSet__FieldAccessorTable = new FieldAccessorTable<FileDescriptorSet, FileDescriptorSet.Builder>(DescriptorProtoFile.internal__static_google_protobuf_FileDescriptorSet__Descriptor, new string[1]
        {
          "File"
        });
        DescriptorProtoFile.internal__static_google_protobuf_FileDescriptorProto__Descriptor = DescriptorProtoFile.Descriptor.MessageTypes[1];
        DescriptorProtoFile.internal__static_google_protobuf_FileDescriptorProto__FieldAccessorTable = new FieldAccessorTable<FileDescriptorProto, FileDescriptorProto.Builder>(DescriptorProtoFile.internal__static_google_protobuf_FileDescriptorProto__Descriptor, new string[9]
        {
          "Name",
          "Package",
          "Dependency",
          "MessageType",
          "EnumType",
          "Service",
          "Extension",
          "Options",
          "SourceCodeInfo"
        });
        DescriptorProtoFile.internal__static_google_protobuf_DescriptorProto__Descriptor = DescriptorProtoFile.Descriptor.MessageTypes[2];
        DescriptorProtoFile.internal__static_google_protobuf_DescriptorProto__FieldAccessorTable = new FieldAccessorTable<DescriptorProto, DescriptorProto.Builder>(DescriptorProtoFile.internal__static_google_protobuf_DescriptorProto__Descriptor, new string[7]
        {
          "Name",
          "Field",
          "Extension",
          "NestedType",
          "EnumType",
          "ExtensionRange",
          "Options"
        });
        DescriptorProtoFile.internal__static_google_protobuf_DescriptorProto_ExtensionRange__Descriptor = DescriptorProtoFile.internal__static_google_protobuf_DescriptorProto__Descriptor.NestedTypes[0];
        DescriptorProtoFile.internal__static_google_protobuf_DescriptorProto_ExtensionRange__FieldAccessorTable = new FieldAccessorTable<DescriptorProto.Types.ExtensionRange, DescriptorProto.Types.ExtensionRange.Builder>(DescriptorProtoFile.internal__static_google_protobuf_DescriptorProto_ExtensionRange__Descriptor, new string[2]
        {
          "Start",
          "End"
        });
        DescriptorProtoFile.internal__static_google_protobuf_FieldDescriptorProto__Descriptor = DescriptorProtoFile.Descriptor.MessageTypes[3];
        DescriptorProtoFile.internal__static_google_protobuf_FieldDescriptorProto__FieldAccessorTable = new FieldAccessorTable<FieldDescriptorProto, FieldDescriptorProto.Builder>(DescriptorProtoFile.internal__static_google_protobuf_FieldDescriptorProto__Descriptor, new string[8]
        {
          "Name",
          "Number",
          "Label",
          "Type",
          "TypeName",
          "Extendee",
          "DefaultValue",
          "Options"
        });
        DescriptorProtoFile.internal__static_google_protobuf_EnumDescriptorProto__Descriptor = DescriptorProtoFile.Descriptor.MessageTypes[4];
        DescriptorProtoFile.internal__static_google_protobuf_EnumDescriptorProto__FieldAccessorTable = new FieldAccessorTable<EnumDescriptorProto, EnumDescriptorProto.Builder>(DescriptorProtoFile.internal__static_google_protobuf_EnumDescriptorProto__Descriptor, new string[3]
        {
          "Name",
          "Value",
          "Options"
        });
        DescriptorProtoFile.internal__static_google_protobuf_EnumValueDescriptorProto__Descriptor = DescriptorProtoFile.Descriptor.MessageTypes[5];
        DescriptorProtoFile.internal__static_google_protobuf_EnumValueDescriptorProto__FieldAccessorTable = new FieldAccessorTable<EnumValueDescriptorProto, EnumValueDescriptorProto.Builder>(DescriptorProtoFile.internal__static_google_protobuf_EnumValueDescriptorProto__Descriptor, new string[3]
        {
          "Name",
          "Number",
          "Options"
        });
        DescriptorProtoFile.internal__static_google_protobuf_ServiceDescriptorProto__Descriptor = DescriptorProtoFile.Descriptor.MessageTypes[6];
        DescriptorProtoFile.internal__static_google_protobuf_ServiceDescriptorProto__FieldAccessorTable = new FieldAccessorTable<ServiceDescriptorProto, ServiceDescriptorProto.Builder>(DescriptorProtoFile.internal__static_google_protobuf_ServiceDescriptorProto__Descriptor, new string[3]
        {
          "Name",
          "Method",
          "Options"
        });
        DescriptorProtoFile.internal__static_google_protobuf_MethodDescriptorProto__Descriptor = DescriptorProtoFile.Descriptor.MessageTypes[7];
        DescriptorProtoFile.internal__static_google_protobuf_MethodDescriptorProto__FieldAccessorTable = new FieldAccessorTable<MethodDescriptorProto, MethodDescriptorProto.Builder>(DescriptorProtoFile.internal__static_google_protobuf_MethodDescriptorProto__Descriptor, new string[4]
        {
          "Name",
          "InputType",
          "OutputType",
          "Options"
        });
        DescriptorProtoFile.internal__static_google_protobuf_FileOptions__Descriptor = DescriptorProtoFile.Descriptor.MessageTypes[8];
        DescriptorProtoFile.internal__static_google_protobuf_FileOptions__FieldAccessorTable = new FieldAccessorTable<FileOptions, FileOptions.Builder>(DescriptorProtoFile.internal__static_google_protobuf_FileOptions__Descriptor, new string[9]
        {
          "JavaPackage",
          "JavaOuterClassname",
          "JavaMultipleFiles",
          "JavaGenerateEqualsAndHash",
          "OptimizeFor",
          "CcGenericServices",
          "JavaGenericServices",
          "PyGenericServices",
          "UninterpretedOption"
        });
        DescriptorProtoFile.internal__static_google_protobuf_MessageOptions__Descriptor = DescriptorProtoFile.Descriptor.MessageTypes[9];
        DescriptorProtoFile.internal__static_google_protobuf_MessageOptions__FieldAccessorTable = new FieldAccessorTable<MessageOptions, MessageOptions.Builder>(DescriptorProtoFile.internal__static_google_protobuf_MessageOptions__Descriptor, new string[3]
        {
          "MessageSetWireFormat",
          "NoStandardDescriptorAccessor",
          "UninterpretedOption"
        });
        DescriptorProtoFile.internal__static_google_protobuf_FieldOptions__Descriptor = DescriptorProtoFile.Descriptor.MessageTypes[10];
        DescriptorProtoFile.internal__static_google_protobuf_FieldOptions__FieldAccessorTable = new FieldAccessorTable<FieldOptions, FieldOptions.Builder>(DescriptorProtoFile.internal__static_google_protobuf_FieldOptions__Descriptor, new string[5]
        {
          "Ctype",
          "Packed",
          "Deprecated",
          "ExperimentalMapKey",
          "UninterpretedOption"
        });
        DescriptorProtoFile.internal__static_google_protobuf_EnumOptions__Descriptor = DescriptorProtoFile.Descriptor.MessageTypes[11];
        DescriptorProtoFile.internal__static_google_protobuf_EnumOptions__FieldAccessorTable = new FieldAccessorTable<EnumOptions, EnumOptions.Builder>(DescriptorProtoFile.internal__static_google_protobuf_EnumOptions__Descriptor, new string[1]
        {
          "UninterpretedOption"
        });
        DescriptorProtoFile.internal__static_google_protobuf_EnumValueOptions__Descriptor = DescriptorProtoFile.Descriptor.MessageTypes[12];
        DescriptorProtoFile.internal__static_google_protobuf_EnumValueOptions__FieldAccessorTable = new FieldAccessorTable<EnumValueOptions, EnumValueOptions.Builder>(DescriptorProtoFile.internal__static_google_protobuf_EnumValueOptions__Descriptor, new string[1]
        {
          "UninterpretedOption"
        });
        DescriptorProtoFile.internal__static_google_protobuf_ServiceOptions__Descriptor = DescriptorProtoFile.Descriptor.MessageTypes[13];
        DescriptorProtoFile.internal__static_google_protobuf_ServiceOptions__FieldAccessorTable = new FieldAccessorTable<ServiceOptions, ServiceOptions.Builder>(DescriptorProtoFile.internal__static_google_protobuf_ServiceOptions__Descriptor, new string[1]
        {
          "UninterpretedOption"
        });
        DescriptorProtoFile.internal__static_google_protobuf_MethodOptions__Descriptor = DescriptorProtoFile.Descriptor.MessageTypes[14];
        DescriptorProtoFile.internal__static_google_protobuf_MethodOptions__FieldAccessorTable = new FieldAccessorTable<MethodOptions, MethodOptions.Builder>(DescriptorProtoFile.internal__static_google_protobuf_MethodOptions__Descriptor, new string[1]
        {
          "UninterpretedOption"
        });
        DescriptorProtoFile.internal__static_google_protobuf_UninterpretedOption__Descriptor = DescriptorProtoFile.Descriptor.MessageTypes[15];
        DescriptorProtoFile.internal__static_google_protobuf_UninterpretedOption__FieldAccessorTable = new FieldAccessorTable<UninterpretedOption, UninterpretedOption.Builder>(DescriptorProtoFile.internal__static_google_protobuf_UninterpretedOption__Descriptor, new string[7]
        {
          "Name",
          "IdentifierValue",
          "PositiveIntValue",
          "NegativeIntValue",
          "DoubleValue",
          "StringValue",
          "AggregateValue"
        });
        DescriptorProtoFile.internal__static_google_protobuf_UninterpretedOption_NamePart__Descriptor = DescriptorProtoFile.internal__static_google_protobuf_UninterpretedOption__Descriptor.NestedTypes[0];
        DescriptorProtoFile.internal__static_google_protobuf_UninterpretedOption_NamePart__FieldAccessorTable = new FieldAccessorTable<UninterpretedOption.Types.NamePart, UninterpretedOption.Types.NamePart.Builder>(DescriptorProtoFile.internal__static_google_protobuf_UninterpretedOption_NamePart__Descriptor, new string[2]
        {
          "NamePart_",
          "IsExtension"
        });
        DescriptorProtoFile.internal__static_google_protobuf_SourceCodeInfo__Descriptor = DescriptorProtoFile.Descriptor.MessageTypes[16];
        DescriptorProtoFile.internal__static_google_protobuf_SourceCodeInfo__FieldAccessorTable = new FieldAccessorTable<SourceCodeInfo, SourceCodeInfo.Builder>(DescriptorProtoFile.internal__static_google_protobuf_SourceCodeInfo__Descriptor, new string[1]
        {
          "Location"
        });
        DescriptorProtoFile.internal__static_google_protobuf_SourceCodeInfo_Location__Descriptor = DescriptorProtoFile.internal__static_google_protobuf_SourceCodeInfo__Descriptor.NestedTypes[0];
        DescriptorProtoFile.internal__static_google_protobuf_SourceCodeInfo_Location__FieldAccessorTable = new FieldAccessorTable<SourceCodeInfo.Types.Location, SourceCodeInfo.Types.Location.Builder>(DescriptorProtoFile.internal__static_google_protobuf_SourceCodeInfo_Location__Descriptor, new string[2]
        {
          "Path",
          "Span"
        });
        return (ExtensionRegistry) null;
      }));
    }
  }
}
