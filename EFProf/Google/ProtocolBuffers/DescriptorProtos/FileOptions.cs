﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.DescriptorProtos.FileOptions
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers.Collections;
using Google.ProtocolBuffers.Descriptors;
using Google.ProtocolBuffers.FieldAccess;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace Google.ProtocolBuffers.DescriptorProtos
{
  [DebuggerNonUserCode]
  public sealed class FileOptions : ExtendableMessage<FileOptions, FileOptions.Builder>
  {
    private static readonly FileOptions defaultInstance = new FileOptions().MakeReadOnly();
    private static readonly string[] _fileOptionsFieldNames = new string[9]
    {
      "cc_generic_services",
      "java_generate_equals_and_hash",
      "java_generic_services",
      "java_multiple_files",
      "java_outer_classname",
      "java_package",
      "optimize_for",
      "py_generic_services",
      "uninterpreted_option"
    };
    private static readonly uint[] _fileOptionsFieldTags = new uint[9]
    {
      128U,
      160U,
      136U,
      80U,
      66U,
      10U,
      72U,
      144U,
      7994U
    };
    private string javaPackage_ = "";
    private string javaOuterClassname_ = "";
    private FileOptions.Types.OptimizeMode optimizeFor_ = FileOptions.Types.OptimizeMode.SPEED;
    private PopsicleList<UninterpretedOption> uninterpretedOption_ = new PopsicleList<UninterpretedOption>();
    private int memoizedSerializedSize = -1;
    public const int JavaPackageFieldNumber = 1;
    public const int JavaOuterClassnameFieldNumber = 8;
    public const int JavaMultipleFilesFieldNumber = 10;
    public const int JavaGenerateEqualsAndHashFieldNumber = 20;
    public const int OptimizeForFieldNumber = 9;
    public const int CcGenericServicesFieldNumber = 16;
    public const int JavaGenericServicesFieldNumber = 17;
    public const int PyGenericServicesFieldNumber = 18;
    public const int UninterpretedOptionFieldNumber = 999;
    private bool hasJavaPackage;
    private bool hasJavaOuterClassname;
    private bool hasJavaMultipleFiles;
    private bool javaMultipleFiles_;
    private bool hasJavaGenerateEqualsAndHash;
    private bool javaGenerateEqualsAndHash_;
    private bool hasOptimizeFor;
    private bool hasCcGenericServices;
    private bool ccGenericServices_;
    private bool hasJavaGenericServices;
    private bool javaGenericServices_;
    private bool hasPyGenericServices;
    private bool pyGenericServices_;

    private FileOptions()
    {
    }

    public static FileOptions DefaultInstance
    {
      get
      {
        return FileOptions.defaultInstance;
      }
    }

    public override FileOptions DefaultInstanceForType
    {
      get
      {
        return FileOptions.DefaultInstance;
      }
    }

    protected override FileOptions ThisMessage
    {
      get
      {
        return this;
      }
    }

    public static MessageDescriptor Descriptor
    {
      get
      {
        return DescriptorProtoFile.internal__static_google_protobuf_FileOptions__Descriptor;
      }
    }

    protected override FieldAccessorTable<FileOptions, FileOptions.Builder> InternalFieldAccessors
    {
      get
      {
        return DescriptorProtoFile.internal__static_google_protobuf_FileOptions__FieldAccessorTable;
      }
    }

    public bool HasJavaPackage
    {
      get
      {
        return this.hasJavaPackage;
      }
    }

    public string JavaPackage
    {
      get
      {
        return this.javaPackage_;
      }
    }

    public bool HasJavaOuterClassname
    {
      get
      {
        return this.hasJavaOuterClassname;
      }
    }

    public string JavaOuterClassname
    {
      get
      {
        return this.javaOuterClassname_;
      }
    }

    public bool HasJavaMultipleFiles
    {
      get
      {
        return this.hasJavaMultipleFiles;
      }
    }

    public bool JavaMultipleFiles
    {
      get
      {
        return this.javaMultipleFiles_;
      }
    }

    public bool HasJavaGenerateEqualsAndHash
    {
      get
      {
        return this.hasJavaGenerateEqualsAndHash;
      }
    }

    public bool JavaGenerateEqualsAndHash
    {
      get
      {
        return this.javaGenerateEqualsAndHash_;
      }
    }

    public bool HasOptimizeFor
    {
      get
      {
        return this.hasOptimizeFor;
      }
    }

    public FileOptions.Types.OptimizeMode OptimizeFor
    {
      get
      {
        return this.optimizeFor_;
      }
    }

    public bool HasCcGenericServices
    {
      get
      {
        return this.hasCcGenericServices;
      }
    }

    public bool CcGenericServices
    {
      get
      {
        return this.ccGenericServices_;
      }
    }

    public bool HasJavaGenericServices
    {
      get
      {
        return this.hasJavaGenericServices;
      }
    }

    public bool JavaGenericServices
    {
      get
      {
        return this.javaGenericServices_;
      }
    }

    public bool HasPyGenericServices
    {
      get
      {
        return this.hasPyGenericServices;
      }
    }

    public bool PyGenericServices
    {
      get
      {
        return this.pyGenericServices_;
      }
    }

    public IList<UninterpretedOption> UninterpretedOptionList
    {
      get
      {
        return (IList<UninterpretedOption>) this.uninterpretedOption_;
      }
    }

    public int UninterpretedOptionCount
    {
      get
      {
        return this.uninterpretedOption_.Count;
      }
    }

    public UninterpretedOption GetUninterpretedOption(int index)
    {
      return this.uninterpretedOption_[index];
    }

    public override bool IsInitialized
    {
      get
      {
        foreach (AbstractMessageLite<UninterpretedOption, UninterpretedOption.Builder> uninterpretedOption in (IEnumerable<UninterpretedOption>) this.UninterpretedOptionList)
        {
          if (!uninterpretedOption.IsInitialized)
            return false;
        }
        return this.ExtensionsAreInitialized;
      }
    }

    public override void WriteTo(ICodedOutputStream output)
    {
      int serializedSize = this.SerializedSize;
      string[] optionsFieldNames = FileOptions._fileOptionsFieldNames;
      ExtendableMessage<FileOptions, FileOptions.Builder>.ExtensionWriter extensionWriter = this.CreateExtensionWriter((ExtendableMessage<FileOptions, FileOptions.Builder>) this);
      if (this.hasJavaPackage)
        output.WriteString(1, optionsFieldNames[5], this.JavaPackage);
      if (this.hasJavaOuterClassname)
        output.WriteString(8, optionsFieldNames[4], this.JavaOuterClassname);
      if (this.hasOptimizeFor)
        output.WriteEnum(9, optionsFieldNames[6], (int) this.OptimizeFor, (object) this.OptimizeFor);
      if (this.hasJavaMultipleFiles)
        output.WriteBool(10, optionsFieldNames[3], this.JavaMultipleFiles);
      if (this.hasCcGenericServices)
        output.WriteBool(16, optionsFieldNames[0], this.CcGenericServices);
      if (this.hasJavaGenericServices)
        output.WriteBool(17, optionsFieldNames[2], this.JavaGenericServices);
      if (this.hasPyGenericServices)
        output.WriteBool(18, optionsFieldNames[7], this.PyGenericServices);
      if (this.hasJavaGenerateEqualsAndHash)
        output.WriteBool(20, optionsFieldNames[1], this.JavaGenerateEqualsAndHash);
      if (this.uninterpretedOption_.Count > 0)
        output.WriteMessageArray<UninterpretedOption>(999, optionsFieldNames[8], (IEnumerable<UninterpretedOption>) this.uninterpretedOption_);
      extensionWriter.WriteUntil(536870912, output);
      this.UnknownFields.WriteTo(output);
    }

    public override int SerializedSize
    {
      get
      {
        int memoizedSerializedSize = this.memoizedSerializedSize;
        if (memoizedSerializedSize != -1)
          return memoizedSerializedSize;
        int num1 = 0;
        if (this.hasJavaPackage)
          num1 += CodedOutputStream.ComputeStringSize(1, this.JavaPackage);
        if (this.hasJavaOuterClassname)
          num1 += CodedOutputStream.ComputeStringSize(8, this.JavaOuterClassname);
        if (this.hasJavaMultipleFiles)
          num1 += CodedOutputStream.ComputeBoolSize(10, this.JavaMultipleFiles);
        if (this.hasJavaGenerateEqualsAndHash)
          num1 += CodedOutputStream.ComputeBoolSize(20, this.JavaGenerateEqualsAndHash);
        if (this.hasOptimizeFor)
          num1 += CodedOutputStream.ComputeEnumSize(9, (int) this.OptimizeFor);
        if (this.hasCcGenericServices)
          num1 += CodedOutputStream.ComputeBoolSize(16, this.CcGenericServices);
        if (this.hasJavaGenericServices)
          num1 += CodedOutputStream.ComputeBoolSize(17, this.JavaGenericServices);
        if (this.hasPyGenericServices)
          num1 += CodedOutputStream.ComputeBoolSize(18, this.PyGenericServices);
        foreach (UninterpretedOption uninterpretedOption in (IEnumerable<UninterpretedOption>) this.UninterpretedOptionList)
          num1 += CodedOutputStream.ComputeMessageSize(999, (IMessageLite) uninterpretedOption);
        int num2 = num1 + this.ExtensionsSerializedSize + this.UnknownFields.SerializedSize;
        this.memoizedSerializedSize = num2;
        return num2;
      }
    }

    public static FileOptions ParseFrom(ByteString data)
    {
      return FileOptions.CreateBuilder().MergeFrom(data).BuildParsed();
    }

    public static FileOptions ParseFrom(ByteString data, ExtensionRegistry extensionRegistry)
    {
      return FileOptions.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
    }

    public static FileOptions ParseFrom(byte[] data)
    {
      return FileOptions.CreateBuilder().MergeFrom(data).BuildParsed();
    }

    public static FileOptions ParseFrom(byte[] data, ExtensionRegistry extensionRegistry)
    {
      return FileOptions.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
    }

    public static FileOptions ParseFrom(Stream input)
    {
      return FileOptions.CreateBuilder().MergeFrom(input).BuildParsed();
    }

    public static FileOptions ParseFrom(Stream input, ExtensionRegistry extensionRegistry)
    {
      return FileOptions.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
    }

    public static FileOptions ParseDelimitedFrom(Stream input)
    {
      return FileOptions.CreateBuilder().MergeDelimitedFrom(input).BuildParsed();
    }

    public static FileOptions ParseDelimitedFrom(Stream input, ExtensionRegistry extensionRegistry)
    {
      return FileOptions.CreateBuilder().MergeDelimitedFrom(input, extensionRegistry).BuildParsed();
    }

    public static FileOptions ParseFrom(ICodedInputStream input)
    {
      return FileOptions.CreateBuilder().MergeFrom(input).BuildParsed();
    }

    public static FileOptions ParseFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
    {
      return FileOptions.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
    }

    private FileOptions MakeReadOnly()
    {
      this.uninterpretedOption_.MakeReadOnly();
      return this;
    }

    public static FileOptions.Builder CreateBuilder()
    {
      return new FileOptions.Builder();
    }

    public override FileOptions.Builder ToBuilder()
    {
      return FileOptions.CreateBuilder(this);
    }

    public override FileOptions.Builder CreateBuilderForType()
    {
      return new FileOptions.Builder();
    }

    public static FileOptions.Builder CreateBuilder(FileOptions prototype)
    {
      return new FileOptions.Builder(prototype);
    }

    static FileOptions()
    {
      object.ReferenceEquals((object) DescriptorProtoFile.Descriptor, (object) null);
    }

    [DebuggerNonUserCode]
    public static class Types
    {
      public enum OptimizeMode
      {
        SPEED = 1,
        CODE_SIZE = 2,
        LITE_RUNTIME = 3,
      }
    }

    [DebuggerNonUserCode]
    public sealed class Builder : ExtendableBuilder<FileOptions, FileOptions.Builder>
    {
      private bool resultIsReadOnly;
      private FileOptions result;

      protected override FileOptions.Builder ThisBuilder
      {
        get
        {
          return this;
        }
      }

      public Builder()
      {
        this.result = FileOptions.DefaultInstance;
        this.resultIsReadOnly = true;
      }

      internal Builder(FileOptions cloneFrom)
      {
        this.result = cloneFrom;
        this.resultIsReadOnly = true;
      }

      private FileOptions PrepareBuilder()
      {
        if (this.resultIsReadOnly)
        {
          FileOptions result = this.result;
          this.result = new FileOptions();
          this.resultIsReadOnly = false;
          this.MergeFrom(result);
        }
        return this.result;
      }

      public override bool IsInitialized
      {
        get
        {
          return this.result.IsInitialized;
        }
      }

      protected override FileOptions MessageBeingBuilt
      {
        get
        {
          return this.PrepareBuilder();
        }
      }

      public override FileOptions.Builder Clear()
      {
        this.result = FileOptions.DefaultInstance;
        this.resultIsReadOnly = true;
        return this;
      }

      public override FileOptions.Builder Clone()
      {
        if (this.resultIsReadOnly)
          return new FileOptions.Builder(this.result);
        return new FileOptions.Builder().MergeFrom(this.result);
      }

      public override MessageDescriptor DescriptorForType
      {
        get
        {
          return FileOptions.Descriptor;
        }
      }

      public override FileOptions DefaultInstanceForType
      {
        get
        {
          return FileOptions.DefaultInstance;
        }
      }

      public override FileOptions BuildPartial()
      {
        if (this.resultIsReadOnly)
          return this.result;
        this.resultIsReadOnly = true;
        return this.result.MakeReadOnly();
      }

      public override FileOptions.Builder MergeFrom(IMessage other)
      {
        if (other is FileOptions)
          return this.MergeFrom((FileOptions) other);
        base.MergeFrom(other);
        return this;
      }

      public override FileOptions.Builder MergeFrom(FileOptions other)
      {
        if (other == FileOptions.DefaultInstance)
          return this;
        this.PrepareBuilder();
        if (other.HasJavaPackage)
          this.JavaPackage = other.JavaPackage;
        if (other.HasJavaOuterClassname)
          this.JavaOuterClassname = other.JavaOuterClassname;
        if (other.HasJavaMultipleFiles)
          this.JavaMultipleFiles = other.JavaMultipleFiles;
        if (other.HasJavaGenerateEqualsAndHash)
          this.JavaGenerateEqualsAndHash = other.JavaGenerateEqualsAndHash;
        if (other.HasOptimizeFor)
          this.OptimizeFor = other.OptimizeFor;
        if (other.HasCcGenericServices)
          this.CcGenericServices = other.CcGenericServices;
        if (other.HasJavaGenericServices)
          this.JavaGenericServices = other.JavaGenericServices;
        if (other.HasPyGenericServices)
          this.PyGenericServices = other.PyGenericServices;
        if (other.uninterpretedOption_.Count != 0)
          this.result.uninterpretedOption_.Add((IEnumerable<UninterpretedOption>) other.uninterpretedOption_);
        this.MergeExtensionFields((ExtendableMessage<FileOptions, FileOptions.Builder>) other);
        this.MergeUnknownFields(other.UnknownFields);
        return this;
      }

      public override FileOptions.Builder MergeFrom(ICodedInputStream input)
      {
        return this.MergeFrom(input, ExtensionRegistry.Empty);
      }

      public override FileOptions.Builder MergeFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
      {
        this.PrepareBuilder();
        UnknownFieldSet.Builder unknownFields = (UnknownFieldSet.Builder) null;
        uint fieldTag;
        string fieldName;
        while (input.ReadTag(out fieldTag, out fieldName))
        {
          if ((int) fieldTag == 0 && fieldName != null)
          {
            int index = Array.BinarySearch<string>(FileOptions._fileOptionsFieldNames, fieldName, (IComparer<string>) StringComparer.Ordinal);
            if (index >= 0)
            {
              fieldTag = FileOptions._fileOptionsFieldTags[index];
            }
            else
            {
              if (unknownFields == null)
                unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
              this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
              continue;
            }
          }
          switch (fieldTag)
          {
            case 0:
              throw InvalidProtocolBufferException.InvalidTag();
            case 10:
              this.result.hasJavaPackage = input.ReadString(ref this.result.javaPackage_);
              continue;
            case 66:
              this.result.hasJavaOuterClassname = input.ReadString(ref this.result.javaOuterClassname_);
              continue;
            case 72:
              object unknown;
              if (input.ReadEnum<FileOptions.Types.OptimizeMode>(ref this.result.optimizeFor_, out unknown))
              {
                this.result.hasOptimizeFor = true;
                continue;
              }
              if (unknown is int)
              {
                if (unknownFields == null)
                  unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
                unknownFields.MergeVarintField(9, (ulong) (int) unknown);
                continue;
              }
              continue;
            case 80:
              this.result.hasJavaMultipleFiles = input.ReadBool(ref this.result.javaMultipleFiles_);
              continue;
            case 128:
              this.result.hasCcGenericServices = input.ReadBool(ref this.result.ccGenericServices_);
              continue;
            case 136:
              this.result.hasJavaGenericServices = input.ReadBool(ref this.result.javaGenericServices_);
              continue;
            case 144:
              this.result.hasPyGenericServices = input.ReadBool(ref this.result.pyGenericServices_);
              continue;
            case 160:
              this.result.hasJavaGenerateEqualsAndHash = input.ReadBool(ref this.result.javaGenerateEqualsAndHash_);
              continue;
            case 7994:
              input.ReadMessageArray<UninterpretedOption>(fieldTag, fieldName, (ICollection<UninterpretedOption>) this.result.uninterpretedOption_, UninterpretedOption.DefaultInstance, extensionRegistry);
              continue;
            default:
              if (WireFormat.IsEndGroupTag(fieldTag))
              {
                if (unknownFields != null)
                  this.UnknownFields = unknownFields.Build();
                return this;
              }
              if (unknownFields == null)
                unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
              this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
              continue;
          }
        }
        if (unknownFields != null)
          this.UnknownFields = unknownFields.Build();
        return this;
      }

      public bool HasJavaPackage
      {
        get
        {
          return this.result.hasJavaPackage;
        }
      }

      public string JavaPackage
      {
        get
        {
          return this.result.JavaPackage;
        }
        set
        {
          this.SetJavaPackage(value);
        }
      }

      public FileOptions.Builder SetJavaPackage(string value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.hasJavaPackage = true;
        this.result.javaPackage_ = value;
        return this;
      }

      public FileOptions.Builder ClearJavaPackage()
      {
        this.PrepareBuilder();
        this.result.hasJavaPackage = false;
        this.result.javaPackage_ = "";
        return this;
      }

      public bool HasJavaOuterClassname
      {
        get
        {
          return this.result.hasJavaOuterClassname;
        }
      }

      public string JavaOuterClassname
      {
        get
        {
          return this.result.JavaOuterClassname;
        }
        set
        {
          this.SetJavaOuterClassname(value);
        }
      }

      public FileOptions.Builder SetJavaOuterClassname(string value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.hasJavaOuterClassname = true;
        this.result.javaOuterClassname_ = value;
        return this;
      }

      public FileOptions.Builder ClearJavaOuterClassname()
      {
        this.PrepareBuilder();
        this.result.hasJavaOuterClassname = false;
        this.result.javaOuterClassname_ = "";
        return this;
      }

      public bool HasJavaMultipleFiles
      {
        get
        {
          return this.result.hasJavaMultipleFiles;
        }
      }

      public bool JavaMultipleFiles
      {
        get
        {
          return this.result.JavaMultipleFiles;
        }
        set
        {
          this.SetJavaMultipleFiles(value);
        }
      }

      public FileOptions.Builder SetJavaMultipleFiles(bool value)
      {
        this.PrepareBuilder();
        this.result.hasJavaMultipleFiles = true;
        this.result.javaMultipleFiles_ = value;
        return this;
      }

      public FileOptions.Builder ClearJavaMultipleFiles()
      {
        this.PrepareBuilder();
        this.result.hasJavaMultipleFiles = false;
        this.result.javaMultipleFiles_ = false;
        return this;
      }

      public bool HasJavaGenerateEqualsAndHash
      {
        get
        {
          return this.result.hasJavaGenerateEqualsAndHash;
        }
      }

      public bool JavaGenerateEqualsAndHash
      {
        get
        {
          return this.result.JavaGenerateEqualsAndHash;
        }
        set
        {
          this.SetJavaGenerateEqualsAndHash(value);
        }
      }

      public FileOptions.Builder SetJavaGenerateEqualsAndHash(bool value)
      {
        this.PrepareBuilder();
        this.result.hasJavaGenerateEqualsAndHash = true;
        this.result.javaGenerateEqualsAndHash_ = value;
        return this;
      }

      public FileOptions.Builder ClearJavaGenerateEqualsAndHash()
      {
        this.PrepareBuilder();
        this.result.hasJavaGenerateEqualsAndHash = false;
        this.result.javaGenerateEqualsAndHash_ = false;
        return this;
      }

      public bool HasOptimizeFor
      {
        get
        {
          return this.result.hasOptimizeFor;
        }
      }

      public FileOptions.Types.OptimizeMode OptimizeFor
      {
        get
        {
          return this.result.OptimizeFor;
        }
        set
        {
          this.SetOptimizeFor(value);
        }
      }

      public FileOptions.Builder SetOptimizeFor(FileOptions.Types.OptimizeMode value)
      {
        this.PrepareBuilder();
        this.result.hasOptimizeFor = true;
        this.result.optimizeFor_ = value;
        return this;
      }

      public FileOptions.Builder ClearOptimizeFor()
      {
        this.PrepareBuilder();
        this.result.hasOptimizeFor = false;
        this.result.optimizeFor_ = FileOptions.Types.OptimizeMode.SPEED;
        return this;
      }

      public bool HasCcGenericServices
      {
        get
        {
          return this.result.hasCcGenericServices;
        }
      }

      public bool CcGenericServices
      {
        get
        {
          return this.result.CcGenericServices;
        }
        set
        {
          this.SetCcGenericServices(value);
        }
      }

      public FileOptions.Builder SetCcGenericServices(bool value)
      {
        this.PrepareBuilder();
        this.result.hasCcGenericServices = true;
        this.result.ccGenericServices_ = value;
        return this;
      }

      public FileOptions.Builder ClearCcGenericServices()
      {
        this.PrepareBuilder();
        this.result.hasCcGenericServices = false;
        this.result.ccGenericServices_ = false;
        return this;
      }

      public bool HasJavaGenericServices
      {
        get
        {
          return this.result.hasJavaGenericServices;
        }
      }

      public bool JavaGenericServices
      {
        get
        {
          return this.result.JavaGenericServices;
        }
        set
        {
          this.SetJavaGenericServices(value);
        }
      }

      public FileOptions.Builder SetJavaGenericServices(bool value)
      {
        this.PrepareBuilder();
        this.result.hasJavaGenericServices = true;
        this.result.javaGenericServices_ = value;
        return this;
      }

      public FileOptions.Builder ClearJavaGenericServices()
      {
        this.PrepareBuilder();
        this.result.hasJavaGenericServices = false;
        this.result.javaGenericServices_ = false;
        return this;
      }

      public bool HasPyGenericServices
      {
        get
        {
          return this.result.hasPyGenericServices;
        }
      }

      public bool PyGenericServices
      {
        get
        {
          return this.result.PyGenericServices;
        }
        set
        {
          this.SetPyGenericServices(value);
        }
      }

      public FileOptions.Builder SetPyGenericServices(bool value)
      {
        this.PrepareBuilder();
        this.result.hasPyGenericServices = true;
        this.result.pyGenericServices_ = value;
        return this;
      }

      public FileOptions.Builder ClearPyGenericServices()
      {
        this.PrepareBuilder();
        this.result.hasPyGenericServices = false;
        this.result.pyGenericServices_ = false;
        return this;
      }

      public IPopsicleList<UninterpretedOption> UninterpretedOptionList
      {
        get
        {
          return (IPopsicleList<UninterpretedOption>) this.PrepareBuilder().uninterpretedOption_;
        }
      }

      public int UninterpretedOptionCount
      {
        get
        {
          return this.result.UninterpretedOptionCount;
        }
      }

      public UninterpretedOption GetUninterpretedOption(int index)
      {
        return this.result.GetUninterpretedOption(index);
      }

      public FileOptions.Builder SetUninterpretedOption(int index, UninterpretedOption value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.uninterpretedOption_[index] = value;
        return this;
      }

      public FileOptions.Builder SetUninterpretedOption(int index, UninterpretedOption.Builder builderForValue)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
        this.PrepareBuilder();
        this.result.uninterpretedOption_[index] = builderForValue.Build();
        return this;
      }

      public FileOptions.Builder AddUninterpretedOption(UninterpretedOption value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.uninterpretedOption_.Add(value);
        return this;
      }

      public FileOptions.Builder AddUninterpretedOption(UninterpretedOption.Builder builderForValue)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
        this.PrepareBuilder();
        this.result.uninterpretedOption_.Add(builderForValue.Build());
        return this;
      }

      public FileOptions.Builder AddRangeUninterpretedOption(IEnumerable<UninterpretedOption> values)
      {
        this.PrepareBuilder();
        this.result.uninterpretedOption_.Add(values);
        return this;
      }

      public FileOptions.Builder ClearUninterpretedOption()
      {
        this.PrepareBuilder();
        this.result.uninterpretedOption_.Clear();
        return this;
      }
    }
  }
}
