﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.DescriptorProtos.CSharpOptions
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers.Descriptors;
using Google.ProtocolBuffers.FieldAccess;
using System;
using System.Diagnostics;

namespace Google.ProtocolBuffers.DescriptorProtos
{
  [DebuggerNonUserCode]
  public static class CSharpOptions
  {
    public const int CSharpFileOptionsFieldNumber = 1000;
    public const int CSharpFieldOptionsFieldNumber = 1000;
    public const int CsharpServiceOptionsFieldNumber = 1000;
    public const int CsharpMethodOptionsFieldNumber = 1000;
    public static GeneratedExtensionBase<Google.ProtocolBuffers.DescriptorProtos.CSharpFileOptions> CSharpFileOptions;
    public static GeneratedExtensionBase<Google.ProtocolBuffers.DescriptorProtos.CSharpFieldOptions> CSharpFieldOptions;
    public static GeneratedExtensionBase<CSharpServiceOptions> CsharpServiceOptions;
    public static GeneratedExtensionBase<CSharpMethodOptions> CsharpMethodOptions;
    internal static MessageDescriptor internal__static_google_protobuf_CSharpFileOptions__Descriptor;
    internal static FieldAccessorTable<Google.ProtocolBuffers.DescriptorProtos.CSharpFileOptions, Google.ProtocolBuffers.DescriptorProtos.CSharpFileOptions.Builder> internal__static_google_protobuf_CSharpFileOptions__FieldAccessorTable;
    internal static MessageDescriptor internal__static_google_protobuf_CSharpFieldOptions__Descriptor;
    internal static FieldAccessorTable<Google.ProtocolBuffers.DescriptorProtos.CSharpFieldOptions, Google.ProtocolBuffers.DescriptorProtos.CSharpFieldOptions.Builder> internal__static_google_protobuf_CSharpFieldOptions__FieldAccessorTable;
    internal static MessageDescriptor internal__static_google_protobuf_CSharpServiceOptions__Descriptor;
    internal static FieldAccessorTable<CSharpServiceOptions, CSharpServiceOptions.Builder> internal__static_google_protobuf_CSharpServiceOptions__FieldAccessorTable;
    internal static MessageDescriptor internal__static_google_protobuf_CSharpMethodOptions__Descriptor;
    internal static FieldAccessorTable<CSharpMethodOptions, CSharpMethodOptions.Builder> internal__static_google_protobuf_CSharpMethodOptions__FieldAccessorTable;
    private static FileDescriptor descriptor;

    public static void RegisterAllExtensions(ExtensionRegistry registry)
    {
      registry.Add<Google.ProtocolBuffers.DescriptorProtos.CSharpFileOptions>(CSharpOptions.CSharpFileOptions);
      registry.Add<Google.ProtocolBuffers.DescriptorProtos.CSharpFieldOptions>(CSharpOptions.CSharpFieldOptions);
      registry.Add<CSharpServiceOptions>(CSharpOptions.CsharpServiceOptions);
      registry.Add<CSharpMethodOptions>(CSharpOptions.CsharpMethodOptions);
    }

    public static FileDescriptor Descriptor
    {
      get
      {
        return CSharpOptions.descriptor;
      }
    }

    static CSharpOptions()
    {
      FileDescriptor.InternalBuildGeneratedFileFrom(Convert.FromBase64String("CiRnb29nbGUvcHJvdG9idWYvY3NoYXJwX29wdGlvbnMucHJvdG8SD2dvb2ds" + "ZS5wcm90b2J1ZhogZ29vZ2xlL3Byb3RvYnVmL2Rlc2NyaXB0b3IucHJvdG8i" + "pwQKEUNTaGFycEZpbGVPcHRpb25zEhEKCW5hbWVzcGFjZRgBIAEoCRIaChJ1" + "bWJyZWxsYV9jbGFzc25hbWUYAiABKAkSHAoOcHVibGljX2NsYXNzZXMYAyAB" + "KAg6BHRydWUSFgoObXVsdGlwbGVfZmlsZXMYBCABKAgSFAoMbmVzdF9jbGFz" + "c2VzGAUgASgIEhYKDmNvZGVfY29udHJhY3RzGAYgASgIEiQKHGV4cGFuZF9u" + "YW1lc3BhY2VfZGlyZWN0b3JpZXMYByABKAgSHAoOY2xzX2NvbXBsaWFuY2UY" + "CCABKAg6BHRydWUSHwoQYWRkX3NlcmlhbGl6YWJsZRgJIAEoCDoFZmFsc2US" + "IwoVZ2VuZXJhdGVfcHJpdmF0ZV9jdG9yGAogASgIOgR0cnVlEhwKDmZpbGVf" + "ZXh0ZW5zaW9uGN0BIAEoCToDLmNzEhsKEnVtYnJlbGxhX25hbWVzcGFjZRje" + "ASABKAkSHAoQb3V0cHV0X2RpcmVjdG9yeRjfASABKAk6AS4SJgoWaWdub3Jl" + "X2dvb2dsZV9wcm90b2J1ZhjgASABKAg6BWZhbHNlEkkKFnNlcnZpY2VfZ2Vu" + "ZXJhdG9yX3R5cGUY4QEgASgOMiIuZ29vZ2xlLnByb3RvYnVmLkNTaGFycFNl" + "cnZpY2VUeXBlOgROT05FEikKGWdlbmVyYXRlZF9jb2RlX2F0dHJpYnV0ZXMY" + "4gEgASgIOgVmYWxzZSIrChJDU2hhcnBGaWVsZE9wdGlvbnMSFQoNcHJvcGVy" + "dHlfbmFtZRgBIAEoCSIsChRDU2hhcnBTZXJ2aWNlT3B0aW9ucxIUCgxpbnRl" + "cmZhY2VfaWQYASABKAkiKgoTQ1NoYXJwTWV0aG9kT3B0aW9ucxITCgtkaXNw" + "YXRjaF9pZBgBIAEoBSpLChFDU2hhcnBTZXJ2aWNlVHlwZRIICgROT05FEAAS" + "CwoHR0VORVJJQxABEg0KCUlOVEVSRkFDRRACEhAKDElSUENESVNQQVRDSBAD" + "Ol4KE2NzaGFycF9maWxlX29wdGlvbnMSHC5nb29nbGUucHJvdG9idWYuRmls" + "ZU9wdGlvbnMY6AcgASgLMiIuZ29vZ2xlLnByb3RvYnVmLkNTaGFycEZpbGVP" + "cHRpb25zOmEKFGNzaGFycF9maWVsZF9vcHRpb25zEh0uZ29vZ2xlLnByb3Rv" + "YnVmLkZpZWxkT3B0aW9ucxjoByABKAsyIy5nb29nbGUucHJvdG9idWYuQ1No" + "YXJwRmllbGRPcHRpb25zOmcKFmNzaGFycF9zZXJ2aWNlX29wdGlvbnMSHy5n" + "b29nbGUucHJvdG9idWYuU2VydmljZU9wdGlvbnMY6AcgASgLMiUuZ29vZ2xl" + "LnByb3RvYnVmLkNTaGFycFNlcnZpY2VPcHRpb25zOmQKFWNzaGFycF9tZXRo" + "b2Rfb3B0aW9ucxIeLmdvb2dsZS5wcm90b2J1Zi5NZXRob2RPcHRpb25zGOgH" + "IAEoCzIkLmdvb2dsZS5wcm90b2J1Zi5DU2hhcnBNZXRob2RPcHRpb25z"), new FileDescriptor[1]
      {
        DescriptorProtoFile.Descriptor
      }, (FileDescriptor.InternalDescriptorAssigner) (root =>
      {
        CSharpOptions.descriptor = root;
        CSharpOptions.internal__static_google_protobuf_CSharpFileOptions__Descriptor = CSharpOptions.Descriptor.MessageTypes[0];
        CSharpOptions.internal__static_google_protobuf_CSharpFileOptions__FieldAccessorTable = new FieldAccessorTable<Google.ProtocolBuffers.DescriptorProtos.CSharpFileOptions, Google.ProtocolBuffers.DescriptorProtos.CSharpFileOptions.Builder>(CSharpOptions.internal__static_google_protobuf_CSharpFileOptions__Descriptor, new string[16]
        {
          "Namespace",
          "UmbrellaClassname",
          "PublicClasses",
          "MultipleFiles",
          "NestClasses",
          "CodeContracts",
          "ExpandNamespaceDirectories",
          "ClsCompliance",
          "AddSerializable",
          "GeneratePrivateCtor",
          "FileExtension",
          "UmbrellaNamespace",
          "OutputDirectory",
          "IgnoreGoogleProtobuf",
          "ServiceGeneratorType",
          "GeneratedCodeAttributes"
        });
        CSharpOptions.internal__static_google_protobuf_CSharpFieldOptions__Descriptor = CSharpOptions.Descriptor.MessageTypes[1];
        CSharpOptions.internal__static_google_protobuf_CSharpFieldOptions__FieldAccessorTable = new FieldAccessorTable<Google.ProtocolBuffers.DescriptorProtos.CSharpFieldOptions, Google.ProtocolBuffers.DescriptorProtos.CSharpFieldOptions.Builder>(CSharpOptions.internal__static_google_protobuf_CSharpFieldOptions__Descriptor, new string[1]
        {
          "PropertyName"
        });
        CSharpOptions.internal__static_google_protobuf_CSharpServiceOptions__Descriptor = CSharpOptions.Descriptor.MessageTypes[2];
        CSharpOptions.internal__static_google_protobuf_CSharpServiceOptions__FieldAccessorTable = new FieldAccessorTable<CSharpServiceOptions, CSharpServiceOptions.Builder>(CSharpOptions.internal__static_google_protobuf_CSharpServiceOptions__Descriptor, new string[1]
        {
          "InterfaceId"
        });
        CSharpOptions.internal__static_google_protobuf_CSharpMethodOptions__Descriptor = CSharpOptions.Descriptor.MessageTypes[3];
        CSharpOptions.internal__static_google_protobuf_CSharpMethodOptions__FieldAccessorTable = new FieldAccessorTable<CSharpMethodOptions, CSharpMethodOptions.Builder>(CSharpOptions.internal__static_google_protobuf_CSharpMethodOptions__Descriptor, new string[1]
        {
          "DispatchId"
        });
        CSharpOptions.CSharpFileOptions = (GeneratedExtensionBase<Google.ProtocolBuffers.DescriptorProtos.CSharpFileOptions>) GeneratedSingleExtension<Google.ProtocolBuffers.DescriptorProtos.CSharpFileOptions>.CreateInstance(CSharpOptions.Descriptor.Extensions[0]);
        CSharpOptions.CSharpFieldOptions = (GeneratedExtensionBase<Google.ProtocolBuffers.DescriptorProtos.CSharpFieldOptions>) GeneratedSingleExtension<Google.ProtocolBuffers.DescriptorProtos.CSharpFieldOptions>.CreateInstance(CSharpOptions.Descriptor.Extensions[1]);
        CSharpOptions.CsharpServiceOptions = (GeneratedExtensionBase<CSharpServiceOptions>) GeneratedSingleExtension<CSharpServiceOptions>.CreateInstance(CSharpOptions.Descriptor.Extensions[2]);
        CSharpOptions.CsharpMethodOptions = (GeneratedExtensionBase<CSharpMethodOptions>) GeneratedSingleExtension<CSharpMethodOptions>.CreateInstance(CSharpOptions.Descriptor.Extensions[3]);
        return (ExtensionRegistry) null;
      }));
    }
  }
}
