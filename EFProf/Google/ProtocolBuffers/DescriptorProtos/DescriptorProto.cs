﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.DescriptorProtos.DescriptorProto
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers.Collections;
using Google.ProtocolBuffers.Descriptors;
using Google.ProtocolBuffers.FieldAccess;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace Google.ProtocolBuffers.DescriptorProtos
{
  [DebuggerNonUserCode]
  public sealed class DescriptorProto : GeneratedMessage<DescriptorProto, DescriptorProto.Builder>, IDescriptorProto<MessageOptions>
  {
    private static readonly DescriptorProto defaultInstance = new DescriptorProto().MakeReadOnly();
    private static readonly string[] _descriptorProtoFieldNames = new string[7]
    {
      "enum_type",
      "extension",
      "extension_range",
      "field",
      "name",
      "nested_type",
      "options"
    };
    private static readonly uint[] _descriptorProtoFieldTags = new uint[7]
    {
      34U,
      50U,
      42U,
      18U,
      10U,
      26U,
      58U
    };
    private string name_ = "";
    private PopsicleList<FieldDescriptorProto> field_ = new PopsicleList<FieldDescriptorProto>();
    private PopsicleList<FieldDescriptorProto> extension_ = new PopsicleList<FieldDescriptorProto>();
    private PopsicleList<DescriptorProto> nestedType_ = new PopsicleList<DescriptorProto>();
    private PopsicleList<EnumDescriptorProto> enumType_ = new PopsicleList<EnumDescriptorProto>();
    private PopsicleList<DescriptorProto.Types.ExtensionRange> extensionRange_ = new PopsicleList<DescriptorProto.Types.ExtensionRange>();
    private int memoizedSerializedSize = -1;
    public const int NameFieldNumber = 1;
    public const int FieldFieldNumber = 2;
    public const int ExtensionFieldNumber = 6;
    public const int NestedTypeFieldNumber = 3;
    public const int EnumTypeFieldNumber = 4;
    public const int ExtensionRangeFieldNumber = 5;
    public const int OptionsFieldNumber = 7;
    private bool hasName;
    private bool hasOptions;
    private MessageOptions options_;

    private DescriptorProto()
    {
    }

    public static DescriptorProto DefaultInstance
    {
      get
      {
        return DescriptorProto.defaultInstance;
      }
    }

    public override DescriptorProto DefaultInstanceForType
    {
      get
      {
        return DescriptorProto.DefaultInstance;
      }
    }

    protected override DescriptorProto ThisMessage
    {
      get
      {
        return this;
      }
    }

    public static MessageDescriptor Descriptor
    {
      get
      {
        return DescriptorProtoFile.internal__static_google_protobuf_DescriptorProto__Descriptor;
      }
    }

    protected override FieldAccessorTable<DescriptorProto, DescriptorProto.Builder> InternalFieldAccessors
    {
      get
      {
        return DescriptorProtoFile.internal__static_google_protobuf_DescriptorProto__FieldAccessorTable;
      }
    }

    public bool HasName
    {
      get
      {
        return this.hasName;
      }
    }

    public string Name
    {
      get
      {
        return this.name_;
      }
    }

    public IList<FieldDescriptorProto> FieldList
    {
      get
      {
        return (IList<FieldDescriptorProto>) this.field_;
      }
    }

    public int FieldCount
    {
      get
      {
        return this.field_.Count;
      }
    }

    public FieldDescriptorProto GetField(int index)
    {
      return this.field_[index];
    }

    public IList<FieldDescriptorProto> ExtensionList
    {
      get
      {
        return (IList<FieldDescriptorProto>) this.extension_;
      }
    }

    public int ExtensionCount
    {
      get
      {
        return this.extension_.Count;
      }
    }

    public FieldDescriptorProto GetExtension(int index)
    {
      return this.extension_[index];
    }

    public IList<DescriptorProto> NestedTypeList
    {
      get
      {
        return (IList<DescriptorProto>) this.nestedType_;
      }
    }

    public int NestedTypeCount
    {
      get
      {
        return this.nestedType_.Count;
      }
    }

    public DescriptorProto GetNestedType(int index)
    {
      return this.nestedType_[index];
    }

    public IList<EnumDescriptorProto> EnumTypeList
    {
      get
      {
        return (IList<EnumDescriptorProto>) this.enumType_;
      }
    }

    public int EnumTypeCount
    {
      get
      {
        return this.enumType_.Count;
      }
    }

    public EnumDescriptorProto GetEnumType(int index)
    {
      return this.enumType_[index];
    }

    public IList<DescriptorProto.Types.ExtensionRange> ExtensionRangeList
    {
      get
      {
        return (IList<DescriptorProto.Types.ExtensionRange>) this.extensionRange_;
      }
    }

    public int ExtensionRangeCount
    {
      get
      {
        return this.extensionRange_.Count;
      }
    }

    public DescriptorProto.Types.ExtensionRange GetExtensionRange(int index)
    {
      return this.extensionRange_[index];
    }

    public bool HasOptions
    {
      get
      {
        return this.hasOptions;
      }
    }

    public MessageOptions Options
    {
      get
      {
        return this.options_ ?? MessageOptions.DefaultInstance;
      }
    }

    public override bool IsInitialized
    {
      get
      {
        foreach (AbstractMessageLite<FieldDescriptorProto, FieldDescriptorProto.Builder> field in (IEnumerable<FieldDescriptorProto>) this.FieldList)
        {
          if (!field.IsInitialized)
            return false;
        }
        foreach (AbstractMessageLite<FieldDescriptorProto, FieldDescriptorProto.Builder> extension in (IEnumerable<FieldDescriptorProto>) this.ExtensionList)
        {
          if (!extension.IsInitialized)
            return false;
        }
        foreach (AbstractMessageLite<DescriptorProto, DescriptorProto.Builder> nestedType in (IEnumerable<DescriptorProto>) this.NestedTypeList)
        {
          if (!nestedType.IsInitialized)
            return false;
        }
        foreach (AbstractMessageLite<EnumDescriptorProto, EnumDescriptorProto.Builder> enumType in (IEnumerable<EnumDescriptorProto>) this.EnumTypeList)
        {
          if (!enumType.IsInitialized)
            return false;
        }
        return !this.HasOptions || this.Options.IsInitialized;
      }
    }

    public override void WriteTo(ICodedOutputStream output)
    {
      int serializedSize = this.SerializedSize;
      string[] descriptorProtoFieldNames = DescriptorProto._descriptorProtoFieldNames;
      if (this.hasName)
        output.WriteString(1, descriptorProtoFieldNames[4], this.Name);
      if (this.field_.Count > 0)
        output.WriteMessageArray<FieldDescriptorProto>(2, descriptorProtoFieldNames[3], (IEnumerable<FieldDescriptorProto>) this.field_);
      if (this.nestedType_.Count > 0)
        output.WriteMessageArray<DescriptorProto>(3, descriptorProtoFieldNames[5], (IEnumerable<DescriptorProto>) this.nestedType_);
      if (this.enumType_.Count > 0)
        output.WriteMessageArray<EnumDescriptorProto>(4, descriptorProtoFieldNames[0], (IEnumerable<EnumDescriptorProto>) this.enumType_);
      if (this.extensionRange_.Count > 0)
        output.WriteMessageArray<DescriptorProto.Types.ExtensionRange>(5, descriptorProtoFieldNames[2], (IEnumerable<DescriptorProto.Types.ExtensionRange>) this.extensionRange_);
      if (this.extension_.Count > 0)
        output.WriteMessageArray<FieldDescriptorProto>(6, descriptorProtoFieldNames[1], (IEnumerable<FieldDescriptorProto>) this.extension_);
      if (this.hasOptions)
        output.WriteMessage(7, descriptorProtoFieldNames[6], (IMessageLite) this.Options);
      this.UnknownFields.WriteTo(output);
    }

    public override int SerializedSize
    {
      get
      {
        int memoizedSerializedSize = this.memoizedSerializedSize;
        if (memoizedSerializedSize != -1)
          return memoizedSerializedSize;
        int num1 = 0;
        if (this.hasName)
          num1 += CodedOutputStream.ComputeStringSize(1, this.Name);
        foreach (FieldDescriptorProto field in (IEnumerable<FieldDescriptorProto>) this.FieldList)
          num1 += CodedOutputStream.ComputeMessageSize(2, (IMessageLite) field);
        foreach (FieldDescriptorProto extension in (IEnumerable<FieldDescriptorProto>) this.ExtensionList)
          num1 += CodedOutputStream.ComputeMessageSize(6, (IMessageLite) extension);
        foreach (DescriptorProto nestedType in (IEnumerable<DescriptorProto>) this.NestedTypeList)
          num1 += CodedOutputStream.ComputeMessageSize(3, (IMessageLite) nestedType);
        foreach (EnumDescriptorProto enumType in (IEnumerable<EnumDescriptorProto>) this.EnumTypeList)
          num1 += CodedOutputStream.ComputeMessageSize(4, (IMessageLite) enumType);
        foreach (DescriptorProto.Types.ExtensionRange extensionRange in (IEnumerable<DescriptorProto.Types.ExtensionRange>) this.ExtensionRangeList)
          num1 += CodedOutputStream.ComputeMessageSize(5, (IMessageLite) extensionRange);
        if (this.hasOptions)
          num1 += CodedOutputStream.ComputeMessageSize(7, (IMessageLite) this.Options);
        int num2 = num1 + this.UnknownFields.SerializedSize;
        this.memoizedSerializedSize = num2;
        return num2;
      }
    }

    public static DescriptorProto ParseFrom(ByteString data)
    {
      return DescriptorProto.CreateBuilder().MergeFrom(data).BuildParsed();
    }

    public static DescriptorProto ParseFrom(ByteString data, ExtensionRegistry extensionRegistry)
    {
      return DescriptorProto.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
    }

    public static DescriptorProto ParseFrom(byte[] data)
    {
      return DescriptorProto.CreateBuilder().MergeFrom(data).BuildParsed();
    }

    public static DescriptorProto ParseFrom(byte[] data, ExtensionRegistry extensionRegistry)
    {
      return DescriptorProto.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
    }

    public static DescriptorProto ParseFrom(Stream input)
    {
      return DescriptorProto.CreateBuilder().MergeFrom(input).BuildParsed();
    }

    public static DescriptorProto ParseFrom(Stream input, ExtensionRegistry extensionRegistry)
    {
      return DescriptorProto.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
    }

    public static DescriptorProto ParseDelimitedFrom(Stream input)
    {
      return DescriptorProto.CreateBuilder().MergeDelimitedFrom(input).BuildParsed();
    }

    public static DescriptorProto ParseDelimitedFrom(Stream input, ExtensionRegistry extensionRegistry)
    {
      return DescriptorProto.CreateBuilder().MergeDelimitedFrom(input, extensionRegistry).BuildParsed();
    }

    public static DescriptorProto ParseFrom(ICodedInputStream input)
    {
      return DescriptorProto.CreateBuilder().MergeFrom(input).BuildParsed();
    }

    public static DescriptorProto ParseFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
    {
      return DescriptorProto.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
    }

    private DescriptorProto MakeReadOnly()
    {
      this.field_.MakeReadOnly();
      this.extension_.MakeReadOnly();
      this.nestedType_.MakeReadOnly();
      this.enumType_.MakeReadOnly();
      this.extensionRange_.MakeReadOnly();
      return this;
    }

    public static DescriptorProto.Builder CreateBuilder()
    {
      return new DescriptorProto.Builder();
    }

    public override DescriptorProto.Builder ToBuilder()
    {
      return DescriptorProto.CreateBuilder(this);
    }

    public override DescriptorProto.Builder CreateBuilderForType()
    {
      return new DescriptorProto.Builder();
    }

    public static DescriptorProto.Builder CreateBuilder(DescriptorProto prototype)
    {
      return new DescriptorProto.Builder(prototype);
    }

    static DescriptorProto()
    {
      object.ReferenceEquals((object) DescriptorProtoFile.Descriptor, (object) null);
    }

    [DebuggerNonUserCode]
    public static class Types
    {
      [DebuggerNonUserCode]
      public sealed class ExtensionRange : GeneratedMessage<DescriptorProto.Types.ExtensionRange, DescriptorProto.Types.ExtensionRange.Builder>
      {
        private static readonly DescriptorProto.Types.ExtensionRange defaultInstance = new DescriptorProto.Types.ExtensionRange().MakeReadOnly();
        private static readonly string[] _extensionRangeFieldNames = new string[2]
        {
          "end",
          "start"
        };
        private static readonly uint[] _extensionRangeFieldTags = new uint[2]
        {
          16U,
          8U
        };
        private int memoizedSerializedSize = -1;
        public const int StartFieldNumber = 1;
        public const int EndFieldNumber = 2;
        private bool hasStart;
        private int start_;
        private bool hasEnd;
        private int end_;

        private ExtensionRange()
        {
        }

        public static DescriptorProto.Types.ExtensionRange DefaultInstance
        {
          get
          {
            return DescriptorProto.Types.ExtensionRange.defaultInstance;
          }
        }

        public override DescriptorProto.Types.ExtensionRange DefaultInstanceForType
        {
          get
          {
            return DescriptorProto.Types.ExtensionRange.DefaultInstance;
          }
        }

        protected override DescriptorProto.Types.ExtensionRange ThisMessage
        {
          get
          {
            return this;
          }
        }

        public static MessageDescriptor Descriptor
        {
          get
          {
            return DescriptorProtoFile.internal__static_google_protobuf_DescriptorProto_ExtensionRange__Descriptor;
          }
        }

        protected override FieldAccessorTable<DescriptorProto.Types.ExtensionRange, DescriptorProto.Types.ExtensionRange.Builder> InternalFieldAccessors
        {
          get
          {
            return DescriptorProtoFile.internal__static_google_protobuf_DescriptorProto_ExtensionRange__FieldAccessorTable;
          }
        }

        public bool HasStart
        {
          get
          {
            return this.hasStart;
          }
        }

        public int Start
        {
          get
          {
            return this.start_;
          }
        }

        public bool HasEnd
        {
          get
          {
            return this.hasEnd;
          }
        }

        public int End
        {
          get
          {
            return this.end_;
          }
        }

        public override bool IsInitialized
        {
          get
          {
            return true;
          }
        }

        public override void WriteTo(ICodedOutputStream output)
        {
          int serializedSize = this.SerializedSize;
          string[] extensionRangeFieldNames = DescriptorProto.Types.ExtensionRange._extensionRangeFieldNames;
          if (this.hasStart)
            output.WriteInt32(1, extensionRangeFieldNames[1], this.Start);
          if (this.hasEnd)
            output.WriteInt32(2, extensionRangeFieldNames[0], this.End);
          this.UnknownFields.WriteTo(output);
        }

        public override int SerializedSize
        {
          get
          {
            int memoizedSerializedSize = this.memoizedSerializedSize;
            if (memoizedSerializedSize != -1)
              return memoizedSerializedSize;
            int num1 = 0;
            if (this.hasStart)
              num1 += CodedOutputStream.ComputeInt32Size(1, this.Start);
            if (this.hasEnd)
              num1 += CodedOutputStream.ComputeInt32Size(2, this.End);
            int num2 = num1 + this.UnknownFields.SerializedSize;
            this.memoizedSerializedSize = num2;
            return num2;
          }
        }

        public static DescriptorProto.Types.ExtensionRange ParseFrom(ByteString data)
        {
          return DescriptorProto.Types.ExtensionRange.CreateBuilder().MergeFrom(data).BuildParsed();
        }

        public static DescriptorProto.Types.ExtensionRange ParseFrom(ByteString data, ExtensionRegistry extensionRegistry)
        {
          return DescriptorProto.Types.ExtensionRange.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
        }

        public static DescriptorProto.Types.ExtensionRange ParseFrom(byte[] data)
        {
          return DescriptorProto.Types.ExtensionRange.CreateBuilder().MergeFrom(data).BuildParsed();
        }

        public static DescriptorProto.Types.ExtensionRange ParseFrom(byte[] data, ExtensionRegistry extensionRegistry)
        {
          return DescriptorProto.Types.ExtensionRange.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
        }

        public static DescriptorProto.Types.ExtensionRange ParseFrom(Stream input)
        {
          return DescriptorProto.Types.ExtensionRange.CreateBuilder().MergeFrom(input).BuildParsed();
        }

        public static DescriptorProto.Types.ExtensionRange ParseFrom(Stream input, ExtensionRegistry extensionRegistry)
        {
          return DescriptorProto.Types.ExtensionRange.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
        }

        public static DescriptorProto.Types.ExtensionRange ParseDelimitedFrom(Stream input)
        {
          return DescriptorProto.Types.ExtensionRange.CreateBuilder().MergeDelimitedFrom(input).BuildParsed();
        }

        public static DescriptorProto.Types.ExtensionRange ParseDelimitedFrom(Stream input, ExtensionRegistry extensionRegistry)
        {
          return DescriptorProto.Types.ExtensionRange.CreateBuilder().MergeDelimitedFrom(input, extensionRegistry).BuildParsed();
        }

        public static DescriptorProto.Types.ExtensionRange ParseFrom(ICodedInputStream input)
        {
          return DescriptorProto.Types.ExtensionRange.CreateBuilder().MergeFrom(input).BuildParsed();
        }

        public static DescriptorProto.Types.ExtensionRange ParseFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
        {
          return DescriptorProto.Types.ExtensionRange.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
        }

        private DescriptorProto.Types.ExtensionRange MakeReadOnly()
        {
          return this;
        }

        public static DescriptorProto.Types.ExtensionRange.Builder CreateBuilder()
        {
          return new DescriptorProto.Types.ExtensionRange.Builder();
        }

        public override DescriptorProto.Types.ExtensionRange.Builder ToBuilder()
        {
          return DescriptorProto.Types.ExtensionRange.CreateBuilder(this);
        }

        public override DescriptorProto.Types.ExtensionRange.Builder CreateBuilderForType()
        {
          return new DescriptorProto.Types.ExtensionRange.Builder();
        }

        public static DescriptorProto.Types.ExtensionRange.Builder CreateBuilder(DescriptorProto.Types.ExtensionRange prototype)
        {
          return new DescriptorProto.Types.ExtensionRange.Builder(prototype);
        }

        static ExtensionRange()
        {
          object.ReferenceEquals((object) DescriptorProtoFile.Descriptor, (object) null);
        }

        [DebuggerNonUserCode]
        public sealed class Builder : GeneratedBuilder<DescriptorProto.Types.ExtensionRange, DescriptorProto.Types.ExtensionRange.Builder>
        {
          private bool resultIsReadOnly;
          private DescriptorProto.Types.ExtensionRange result;

          protected override DescriptorProto.Types.ExtensionRange.Builder ThisBuilder
          {
            get
            {
              return this;
            }
          }

          public Builder()
          {
            this.result = DescriptorProto.Types.ExtensionRange.DefaultInstance;
            this.resultIsReadOnly = true;
          }

          internal Builder(DescriptorProto.Types.ExtensionRange cloneFrom)
          {
            this.result = cloneFrom;
            this.resultIsReadOnly = true;
          }

          private DescriptorProto.Types.ExtensionRange PrepareBuilder()
          {
            if (this.resultIsReadOnly)
            {
              DescriptorProto.Types.ExtensionRange result = this.result;
              this.result = new DescriptorProto.Types.ExtensionRange();
              this.resultIsReadOnly = false;
              this.MergeFrom(result);
            }
            return this.result;
          }

          public override bool IsInitialized
          {
            get
            {
              return this.result.IsInitialized;
            }
          }

          protected override DescriptorProto.Types.ExtensionRange MessageBeingBuilt
          {
            get
            {
              return this.PrepareBuilder();
            }
          }

          public override DescriptorProto.Types.ExtensionRange.Builder Clear()
          {
            this.result = DescriptorProto.Types.ExtensionRange.DefaultInstance;
            this.resultIsReadOnly = true;
            return this;
          }

          public override DescriptorProto.Types.ExtensionRange.Builder Clone()
          {
            if (this.resultIsReadOnly)
              return new DescriptorProto.Types.ExtensionRange.Builder(this.result);
            return new DescriptorProto.Types.ExtensionRange.Builder().MergeFrom(this.result);
          }

          public override MessageDescriptor DescriptorForType
          {
            get
            {
              return DescriptorProto.Types.ExtensionRange.Descriptor;
            }
          }

          public override DescriptorProto.Types.ExtensionRange DefaultInstanceForType
          {
            get
            {
              return DescriptorProto.Types.ExtensionRange.DefaultInstance;
            }
          }

          public override DescriptorProto.Types.ExtensionRange BuildPartial()
          {
            if (this.resultIsReadOnly)
              return this.result;
            this.resultIsReadOnly = true;
            return this.result.MakeReadOnly();
          }

          public override DescriptorProto.Types.ExtensionRange.Builder MergeFrom(IMessage other)
          {
            if (other is DescriptorProto.Types.ExtensionRange)
              return this.MergeFrom((DescriptorProto.Types.ExtensionRange) other);
            base.MergeFrom(other);
            return this;
          }

          public override DescriptorProto.Types.ExtensionRange.Builder MergeFrom(DescriptorProto.Types.ExtensionRange other)
          {
            if (other == DescriptorProto.Types.ExtensionRange.DefaultInstance)
              return this;
            this.PrepareBuilder();
            if (other.HasStart)
              this.Start = other.Start;
            if (other.HasEnd)
              this.End = other.End;
            this.MergeUnknownFields(other.UnknownFields);
            return this;
          }

          public override DescriptorProto.Types.ExtensionRange.Builder MergeFrom(ICodedInputStream input)
          {
            return this.MergeFrom(input, ExtensionRegistry.Empty);
          }

          public override DescriptorProto.Types.ExtensionRange.Builder MergeFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
          {
            this.PrepareBuilder();
            UnknownFieldSet.Builder unknownFields = (UnknownFieldSet.Builder) null;
            uint fieldTag;
            string fieldName;
            while (input.ReadTag(out fieldTag, out fieldName))
            {
              if ((int) fieldTag == 0 && fieldName != null)
              {
                int index = Array.BinarySearch<string>(DescriptorProto.Types.ExtensionRange._extensionRangeFieldNames, fieldName, (IComparer<string>) StringComparer.Ordinal);
                if (index >= 0)
                {
                  fieldTag = DescriptorProto.Types.ExtensionRange._extensionRangeFieldTags[index];
                }
                else
                {
                  if (unknownFields == null)
                    unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
                  this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
                  continue;
                }
              }
              switch (fieldTag)
              {
                case 0:
                  throw InvalidProtocolBufferException.InvalidTag();
                case 8:
                  this.result.hasStart = input.ReadInt32(ref this.result.start_);
                  continue;
                case 16:
                  this.result.hasEnd = input.ReadInt32(ref this.result.end_);
                  continue;
                default:
                  if (WireFormat.IsEndGroupTag(fieldTag))
                  {
                    if (unknownFields != null)
                      this.UnknownFields = unknownFields.Build();
                    return this;
                  }
                  if (unknownFields == null)
                    unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
                  this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
                  continue;
              }
            }
            if (unknownFields != null)
              this.UnknownFields = unknownFields.Build();
            return this;
          }

          public bool HasStart
          {
            get
            {
              return this.result.hasStart;
            }
          }

          public int Start
          {
            get
            {
              return this.result.Start;
            }
            set
            {
              this.SetStart(value);
            }
          }

          public DescriptorProto.Types.ExtensionRange.Builder SetStart(int value)
          {
            this.PrepareBuilder();
            this.result.hasStart = true;
            this.result.start_ = value;
            return this;
          }

          public DescriptorProto.Types.ExtensionRange.Builder ClearStart()
          {
            this.PrepareBuilder();
            this.result.hasStart = false;
            this.result.start_ = 0;
            return this;
          }

          public bool HasEnd
          {
            get
            {
              return this.result.hasEnd;
            }
          }

          public int End
          {
            get
            {
              return this.result.End;
            }
            set
            {
              this.SetEnd(value);
            }
          }

          public DescriptorProto.Types.ExtensionRange.Builder SetEnd(int value)
          {
            this.PrepareBuilder();
            this.result.hasEnd = true;
            this.result.end_ = value;
            return this;
          }

          public DescriptorProto.Types.ExtensionRange.Builder ClearEnd()
          {
            this.PrepareBuilder();
            this.result.hasEnd = false;
            this.result.end_ = 0;
            return this;
          }
        }
      }
    }

    [DebuggerNonUserCode]
    public sealed class Builder : GeneratedBuilder<DescriptorProto, DescriptorProto.Builder>
    {
      private bool resultIsReadOnly;
      private DescriptorProto result;

      protected override DescriptorProto.Builder ThisBuilder
      {
        get
        {
          return this;
        }
      }

      public Builder()
      {
        this.result = DescriptorProto.DefaultInstance;
        this.resultIsReadOnly = true;
      }

      internal Builder(DescriptorProto cloneFrom)
      {
        this.result = cloneFrom;
        this.resultIsReadOnly = true;
      }

      private DescriptorProto PrepareBuilder()
      {
        if (this.resultIsReadOnly)
        {
          DescriptorProto result = this.result;
          this.result = new DescriptorProto();
          this.resultIsReadOnly = false;
          this.MergeFrom(result);
        }
        return this.result;
      }

      public override bool IsInitialized
      {
        get
        {
          return this.result.IsInitialized;
        }
      }

      protected override DescriptorProto MessageBeingBuilt
      {
        get
        {
          return this.PrepareBuilder();
        }
      }

      public override DescriptorProto.Builder Clear()
      {
        this.result = DescriptorProto.DefaultInstance;
        this.resultIsReadOnly = true;
        return this;
      }

      public override DescriptorProto.Builder Clone()
      {
        if (this.resultIsReadOnly)
          return new DescriptorProto.Builder(this.result);
        return new DescriptorProto.Builder().MergeFrom(this.result);
      }

      public override MessageDescriptor DescriptorForType
      {
        get
        {
          return DescriptorProto.Descriptor;
        }
      }

      public override DescriptorProto DefaultInstanceForType
      {
        get
        {
          return DescriptorProto.DefaultInstance;
        }
      }

      public override DescriptorProto BuildPartial()
      {
        if (this.resultIsReadOnly)
          return this.result;
        this.resultIsReadOnly = true;
        return this.result.MakeReadOnly();
      }

      public override DescriptorProto.Builder MergeFrom(IMessage other)
      {
        if (other is DescriptorProto)
          return this.MergeFrom((DescriptorProto) other);
        base.MergeFrom(other);
        return this;
      }

      public override DescriptorProto.Builder MergeFrom(DescriptorProto other)
      {
        if (other == DescriptorProto.DefaultInstance)
          return this;
        this.PrepareBuilder();
        if (other.HasName)
          this.Name = other.Name;
        if (other.field_.Count != 0)
          this.result.field_.Add((IEnumerable<FieldDescriptorProto>) other.field_);
        if (other.extension_.Count != 0)
          this.result.extension_.Add((IEnumerable<FieldDescriptorProto>) other.extension_);
        if (other.nestedType_.Count != 0)
          this.result.nestedType_.Add((IEnumerable<DescriptorProto>) other.nestedType_);
        if (other.enumType_.Count != 0)
          this.result.enumType_.Add((IEnumerable<EnumDescriptorProto>) other.enumType_);
        if (other.extensionRange_.Count != 0)
          this.result.extensionRange_.Add((IEnumerable<DescriptorProto.Types.ExtensionRange>) other.extensionRange_);
        if (other.HasOptions)
          this.MergeOptions(other.Options);
        this.MergeUnknownFields(other.UnknownFields);
        return this;
      }

      public override DescriptorProto.Builder MergeFrom(ICodedInputStream input)
      {
        return this.MergeFrom(input, ExtensionRegistry.Empty);
      }

      public override DescriptorProto.Builder MergeFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
      {
        this.PrepareBuilder();
        UnknownFieldSet.Builder unknownFields = (UnknownFieldSet.Builder) null;
        uint fieldTag;
        string fieldName;
        while (input.ReadTag(out fieldTag, out fieldName))
        {
          if ((int) fieldTag == 0 && fieldName != null)
          {
            int index = Array.BinarySearch<string>(DescriptorProto._descriptorProtoFieldNames, fieldName, (IComparer<string>) StringComparer.Ordinal);
            if (index >= 0)
            {
              fieldTag = DescriptorProto._descriptorProtoFieldTags[index];
            }
            else
            {
              if (unknownFields == null)
                unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
              this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
              continue;
            }
          }
          switch (fieldTag)
          {
            case 0:
              throw InvalidProtocolBufferException.InvalidTag();
            case 10:
              this.result.hasName = input.ReadString(ref this.result.name_);
              continue;
            case 18:
              input.ReadMessageArray<FieldDescriptorProto>(fieldTag, fieldName, (ICollection<FieldDescriptorProto>) this.result.field_, FieldDescriptorProto.DefaultInstance, extensionRegistry);
              continue;
            case 26:
              input.ReadMessageArray<DescriptorProto>(fieldTag, fieldName, (ICollection<DescriptorProto>) this.result.nestedType_, DescriptorProto.DefaultInstance, extensionRegistry);
              continue;
            case 34:
              input.ReadMessageArray<EnumDescriptorProto>(fieldTag, fieldName, (ICollection<EnumDescriptorProto>) this.result.enumType_, EnumDescriptorProto.DefaultInstance, extensionRegistry);
              continue;
            case 42:
              input.ReadMessageArray<DescriptorProto.Types.ExtensionRange>(fieldTag, fieldName, (ICollection<DescriptorProto.Types.ExtensionRange>) this.result.extensionRange_, DescriptorProto.Types.ExtensionRange.DefaultInstance, extensionRegistry);
              continue;
            case 50:
              input.ReadMessageArray<FieldDescriptorProto>(fieldTag, fieldName, (ICollection<FieldDescriptorProto>) this.result.extension_, FieldDescriptorProto.DefaultInstance, extensionRegistry);
              continue;
            case 58:
              MessageOptions.Builder builder = MessageOptions.CreateBuilder();
              if (this.result.hasOptions)
                builder.MergeFrom(this.Options);
              input.ReadMessage((IBuilderLite) builder, extensionRegistry);
              this.Options = builder.BuildPartial();
              continue;
            default:
              if (WireFormat.IsEndGroupTag(fieldTag))
              {
                if (unknownFields != null)
                  this.UnknownFields = unknownFields.Build();
                return this;
              }
              if (unknownFields == null)
                unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
              this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
              continue;
          }
        }
        if (unknownFields != null)
          this.UnknownFields = unknownFields.Build();
        return this;
      }

      public bool HasName
      {
        get
        {
          return this.result.hasName;
        }
      }

      public string Name
      {
        get
        {
          return this.result.Name;
        }
        set
        {
          this.SetName(value);
        }
      }

      public DescriptorProto.Builder SetName(string value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.hasName = true;
        this.result.name_ = value;
        return this;
      }

      public DescriptorProto.Builder ClearName()
      {
        this.PrepareBuilder();
        this.result.hasName = false;
        this.result.name_ = "";
        return this;
      }

      public IPopsicleList<FieldDescriptorProto> FieldList
      {
        get
        {
          return (IPopsicleList<FieldDescriptorProto>) this.PrepareBuilder().field_;
        }
      }

      public int FieldCount
      {
        get
        {
          return this.result.FieldCount;
        }
      }

      public FieldDescriptorProto GetField(int index)
      {
        return this.result.GetField(index);
      }

      public DescriptorProto.Builder SetField(int index, FieldDescriptorProto value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.field_[index] = value;
        return this;
      }

      public DescriptorProto.Builder SetField(int index, FieldDescriptorProto.Builder builderForValue)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
        this.PrepareBuilder();
        this.result.field_[index] = builderForValue.Build();
        return this;
      }

      public DescriptorProto.Builder AddField(FieldDescriptorProto value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.field_.Add(value);
        return this;
      }

      public DescriptorProto.Builder AddField(FieldDescriptorProto.Builder builderForValue)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
        this.PrepareBuilder();
        this.result.field_.Add(builderForValue.Build());
        return this;
      }

      public DescriptorProto.Builder AddRangeField(IEnumerable<FieldDescriptorProto> values)
      {
        this.PrepareBuilder();
        this.result.field_.Add(values);
        return this;
      }

      public DescriptorProto.Builder ClearField()
      {
        this.PrepareBuilder();
        this.result.field_.Clear();
        return this;
      }

      public IPopsicleList<FieldDescriptorProto> ExtensionList
      {
        get
        {
          return (IPopsicleList<FieldDescriptorProto>) this.PrepareBuilder().extension_;
        }
      }

      public int ExtensionCount
      {
        get
        {
          return this.result.ExtensionCount;
        }
      }

      public FieldDescriptorProto GetExtension(int index)
      {
        return this.result.GetExtension(index);
      }

      public DescriptorProto.Builder SetExtension(int index, FieldDescriptorProto value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.extension_[index] = value;
        return this;
      }

      public DescriptorProto.Builder SetExtension(int index, FieldDescriptorProto.Builder builderForValue)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
        this.PrepareBuilder();
        this.result.extension_[index] = builderForValue.Build();
        return this;
      }

      public DescriptorProto.Builder AddExtension(FieldDescriptorProto value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.extension_.Add(value);
        return this;
      }

      public DescriptorProto.Builder AddExtension(FieldDescriptorProto.Builder builderForValue)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
        this.PrepareBuilder();
        this.result.extension_.Add(builderForValue.Build());
        return this;
      }

      public DescriptorProto.Builder AddRangeExtension(IEnumerable<FieldDescriptorProto> values)
      {
        this.PrepareBuilder();
        this.result.extension_.Add(values);
        return this;
      }

      public DescriptorProto.Builder ClearExtension()
      {
        this.PrepareBuilder();
        this.result.extension_.Clear();
        return this;
      }

      public IPopsicleList<DescriptorProto> NestedTypeList
      {
        get
        {
          return (IPopsicleList<DescriptorProto>) this.PrepareBuilder().nestedType_;
        }
      }

      public int NestedTypeCount
      {
        get
        {
          return this.result.NestedTypeCount;
        }
      }

      public DescriptorProto GetNestedType(int index)
      {
        return this.result.GetNestedType(index);
      }

      public DescriptorProto.Builder SetNestedType(int index, DescriptorProto value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.nestedType_[index] = value;
        return this;
      }

      public DescriptorProto.Builder SetNestedType(int index, DescriptorProto.Builder builderForValue)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
        this.PrepareBuilder();
        this.result.nestedType_[index] = builderForValue.Build();
        return this;
      }

      public DescriptorProto.Builder AddNestedType(DescriptorProto value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.nestedType_.Add(value);
        return this;
      }

      public DescriptorProto.Builder AddNestedType(DescriptorProto.Builder builderForValue)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
        this.PrepareBuilder();
        this.result.nestedType_.Add(builderForValue.Build());
        return this;
      }

      public DescriptorProto.Builder AddRangeNestedType(IEnumerable<DescriptorProto> values)
      {
        this.PrepareBuilder();
        this.result.nestedType_.Add(values);
        return this;
      }

      public DescriptorProto.Builder ClearNestedType()
      {
        this.PrepareBuilder();
        this.result.nestedType_.Clear();
        return this;
      }

      public IPopsicleList<EnumDescriptorProto> EnumTypeList
      {
        get
        {
          return (IPopsicleList<EnumDescriptorProto>) this.PrepareBuilder().enumType_;
        }
      }

      public int EnumTypeCount
      {
        get
        {
          return this.result.EnumTypeCount;
        }
      }

      public EnumDescriptorProto GetEnumType(int index)
      {
        return this.result.GetEnumType(index);
      }

      public DescriptorProto.Builder SetEnumType(int index, EnumDescriptorProto value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.enumType_[index] = value;
        return this;
      }

      public DescriptorProto.Builder SetEnumType(int index, EnumDescriptorProto.Builder builderForValue)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
        this.PrepareBuilder();
        this.result.enumType_[index] = builderForValue.Build();
        return this;
      }

      public DescriptorProto.Builder AddEnumType(EnumDescriptorProto value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.enumType_.Add(value);
        return this;
      }

      public DescriptorProto.Builder AddEnumType(EnumDescriptorProto.Builder builderForValue)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
        this.PrepareBuilder();
        this.result.enumType_.Add(builderForValue.Build());
        return this;
      }

      public DescriptorProto.Builder AddRangeEnumType(IEnumerable<EnumDescriptorProto> values)
      {
        this.PrepareBuilder();
        this.result.enumType_.Add(values);
        return this;
      }

      public DescriptorProto.Builder ClearEnumType()
      {
        this.PrepareBuilder();
        this.result.enumType_.Clear();
        return this;
      }

      public IPopsicleList<DescriptorProto.Types.ExtensionRange> ExtensionRangeList
      {
        get
        {
          return (IPopsicleList<DescriptorProto.Types.ExtensionRange>) this.PrepareBuilder().extensionRange_;
        }
      }

      public int ExtensionRangeCount
      {
        get
        {
          return this.result.ExtensionRangeCount;
        }
      }

      public DescriptorProto.Types.ExtensionRange GetExtensionRange(int index)
      {
        return this.result.GetExtensionRange(index);
      }

      public DescriptorProto.Builder SetExtensionRange(int index, DescriptorProto.Types.ExtensionRange value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.extensionRange_[index] = value;
        return this;
      }

      public DescriptorProto.Builder SetExtensionRange(int index, DescriptorProto.Types.ExtensionRange.Builder builderForValue)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
        this.PrepareBuilder();
        this.result.extensionRange_[index] = builderForValue.Build();
        return this;
      }

      public DescriptorProto.Builder AddExtensionRange(DescriptorProto.Types.ExtensionRange value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.extensionRange_.Add(value);
        return this;
      }

      public DescriptorProto.Builder AddExtensionRange(DescriptorProto.Types.ExtensionRange.Builder builderForValue)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
        this.PrepareBuilder();
        this.result.extensionRange_.Add(builderForValue.Build());
        return this;
      }

      public DescriptorProto.Builder AddRangeExtensionRange(IEnumerable<DescriptorProto.Types.ExtensionRange> values)
      {
        this.PrepareBuilder();
        this.result.extensionRange_.Add(values);
        return this;
      }

      public DescriptorProto.Builder ClearExtensionRange()
      {
        this.PrepareBuilder();
        this.result.extensionRange_.Clear();
        return this;
      }

      public bool HasOptions
      {
        get
        {
          return this.result.hasOptions;
        }
      }

      public MessageOptions Options
      {
        get
        {
          return this.result.Options;
        }
        set
        {
          this.SetOptions(value);
        }
      }

      public DescriptorProto.Builder SetOptions(MessageOptions value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.hasOptions = true;
        this.result.options_ = value;
        return this;
      }

      public DescriptorProto.Builder SetOptions(MessageOptions.Builder builderForValue)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
        this.PrepareBuilder();
        this.result.hasOptions = true;
        this.result.options_ = builderForValue.Build();
        return this;
      }

      public DescriptorProto.Builder MergeOptions(MessageOptions value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.options_ = !this.result.hasOptions || this.result.options_ == MessageOptions.DefaultInstance ? value : MessageOptions.CreateBuilder(this.result.options_).MergeFrom(value).BuildPartial();
        this.result.hasOptions = true;
        return this;
      }

      public DescriptorProto.Builder ClearOptions()
      {
        this.PrepareBuilder();
        this.result.hasOptions = false;
        this.result.options_ = (MessageOptions) null;
        return this;
      }
    }
  }
}
