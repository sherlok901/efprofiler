﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.DescriptorProtos.MethodDescriptorProto
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers.Descriptors;
using Google.ProtocolBuffers.FieldAccess;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace Google.ProtocolBuffers.DescriptorProtos
{
  [DebuggerNonUserCode]
  public sealed class MethodDescriptorProto : GeneratedMessage<MethodDescriptorProto, MethodDescriptorProto.Builder>, IDescriptorProto<MethodOptions>
  {
    private static readonly MethodDescriptorProto defaultInstance = new MethodDescriptorProto().MakeReadOnly();
    private static readonly string[] _methodDescriptorProtoFieldNames = new string[4]
    {
      "input_type",
      "name",
      "options",
      "output_type"
    };
    private static readonly uint[] _methodDescriptorProtoFieldTags = new uint[4]
    {
      18U,
      10U,
      34U,
      26U
    };
    private string name_ = "";
    private string inputType_ = "";
    private string outputType_ = "";
    private int memoizedSerializedSize = -1;
    public const int NameFieldNumber = 1;
    public const int InputTypeFieldNumber = 2;
    public const int OutputTypeFieldNumber = 3;
    public const int OptionsFieldNumber = 4;
    private bool hasName;
    private bool hasInputType;
    private bool hasOutputType;
    private bool hasOptions;
    private MethodOptions options_;

    private MethodDescriptorProto()
    {
    }

    public static MethodDescriptorProto DefaultInstance
    {
      get
      {
        return MethodDescriptorProto.defaultInstance;
      }
    }

    public override MethodDescriptorProto DefaultInstanceForType
    {
      get
      {
        return MethodDescriptorProto.DefaultInstance;
      }
    }

    protected override MethodDescriptorProto ThisMessage
    {
      get
      {
        return this;
      }
    }

    public static MessageDescriptor Descriptor
    {
      get
      {
        return DescriptorProtoFile.internal__static_google_protobuf_MethodDescriptorProto__Descriptor;
      }
    }

    protected override FieldAccessorTable<MethodDescriptorProto, MethodDescriptorProto.Builder> InternalFieldAccessors
    {
      get
      {
        return DescriptorProtoFile.internal__static_google_protobuf_MethodDescriptorProto__FieldAccessorTable;
      }
    }

    public bool HasName
    {
      get
      {
        return this.hasName;
      }
    }

    public string Name
    {
      get
      {
        return this.name_;
      }
    }

    public bool HasInputType
    {
      get
      {
        return this.hasInputType;
      }
    }

    public string InputType
    {
      get
      {
        return this.inputType_;
      }
    }

    public bool HasOutputType
    {
      get
      {
        return this.hasOutputType;
      }
    }

    public string OutputType
    {
      get
      {
        return this.outputType_;
      }
    }

    public bool HasOptions
    {
      get
      {
        return this.hasOptions;
      }
    }

    public MethodOptions Options
    {
      get
      {
        return this.options_ ?? MethodOptions.DefaultInstance;
      }
    }

    public override bool IsInitialized
    {
      get
      {
        return !this.HasOptions || this.Options.IsInitialized;
      }
    }

    public override void WriteTo(ICodedOutputStream output)
    {
      int serializedSize = this.SerializedSize;
      string[] descriptorProtoFieldNames = MethodDescriptorProto._methodDescriptorProtoFieldNames;
      if (this.hasName)
        output.WriteString(1, descriptorProtoFieldNames[1], this.Name);
      if (this.hasInputType)
        output.WriteString(2, descriptorProtoFieldNames[0], this.InputType);
      if (this.hasOutputType)
        output.WriteString(3, descriptorProtoFieldNames[3], this.OutputType);
      if (this.hasOptions)
        output.WriteMessage(4, descriptorProtoFieldNames[2], (IMessageLite) this.Options);
      this.UnknownFields.WriteTo(output);
    }

    public override int SerializedSize
    {
      get
      {
        int memoizedSerializedSize = this.memoizedSerializedSize;
        if (memoizedSerializedSize != -1)
          return memoizedSerializedSize;
        int num1 = 0;
        if (this.hasName)
          num1 += CodedOutputStream.ComputeStringSize(1, this.Name);
        if (this.hasInputType)
          num1 += CodedOutputStream.ComputeStringSize(2, this.InputType);
        if (this.hasOutputType)
          num1 += CodedOutputStream.ComputeStringSize(3, this.OutputType);
        if (this.hasOptions)
          num1 += CodedOutputStream.ComputeMessageSize(4, (IMessageLite) this.Options);
        int num2 = num1 + this.UnknownFields.SerializedSize;
        this.memoizedSerializedSize = num2;
        return num2;
      }
    }

    public static MethodDescriptorProto ParseFrom(ByteString data)
    {
      return MethodDescriptorProto.CreateBuilder().MergeFrom(data).BuildParsed();
    }

    public static MethodDescriptorProto ParseFrom(ByteString data, ExtensionRegistry extensionRegistry)
    {
      return MethodDescriptorProto.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
    }

    public static MethodDescriptorProto ParseFrom(byte[] data)
    {
      return MethodDescriptorProto.CreateBuilder().MergeFrom(data).BuildParsed();
    }

    public static MethodDescriptorProto ParseFrom(byte[] data, ExtensionRegistry extensionRegistry)
    {
      return MethodDescriptorProto.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
    }

    public static MethodDescriptorProto ParseFrom(Stream input)
    {
      return MethodDescriptorProto.CreateBuilder().MergeFrom(input).BuildParsed();
    }

    public static MethodDescriptorProto ParseFrom(Stream input, ExtensionRegistry extensionRegistry)
    {
      return MethodDescriptorProto.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
    }

    public static MethodDescriptorProto ParseDelimitedFrom(Stream input)
    {
      return MethodDescriptorProto.CreateBuilder().MergeDelimitedFrom(input).BuildParsed();
    }

    public static MethodDescriptorProto ParseDelimitedFrom(Stream input, ExtensionRegistry extensionRegistry)
    {
      return MethodDescriptorProto.CreateBuilder().MergeDelimitedFrom(input, extensionRegistry).BuildParsed();
    }

    public static MethodDescriptorProto ParseFrom(ICodedInputStream input)
    {
      return MethodDescriptorProto.CreateBuilder().MergeFrom(input).BuildParsed();
    }

    public static MethodDescriptorProto ParseFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
    {
      return MethodDescriptorProto.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
    }

    private MethodDescriptorProto MakeReadOnly()
    {
      return this;
    }

    public static MethodDescriptorProto.Builder CreateBuilder()
    {
      return new MethodDescriptorProto.Builder();
    }

    public override MethodDescriptorProto.Builder ToBuilder()
    {
      return MethodDescriptorProto.CreateBuilder(this);
    }

    public override MethodDescriptorProto.Builder CreateBuilderForType()
    {
      return new MethodDescriptorProto.Builder();
    }

    public static MethodDescriptorProto.Builder CreateBuilder(MethodDescriptorProto prototype)
    {
      return new MethodDescriptorProto.Builder(prototype);
    }

    static MethodDescriptorProto()
    {
      object.ReferenceEquals((object) DescriptorProtoFile.Descriptor, (object) null);
    }

    [DebuggerNonUserCode]
    public sealed class Builder : GeneratedBuilder<MethodDescriptorProto, MethodDescriptorProto.Builder>
    {
      private bool resultIsReadOnly;
      private MethodDescriptorProto result;

      protected override MethodDescriptorProto.Builder ThisBuilder
      {
        get
        {
          return this;
        }
      }

      public Builder()
      {
        this.result = MethodDescriptorProto.DefaultInstance;
        this.resultIsReadOnly = true;
      }

      internal Builder(MethodDescriptorProto cloneFrom)
      {
        this.result = cloneFrom;
        this.resultIsReadOnly = true;
      }

      private MethodDescriptorProto PrepareBuilder()
      {
        if (this.resultIsReadOnly)
        {
          MethodDescriptorProto result = this.result;
          this.result = new MethodDescriptorProto();
          this.resultIsReadOnly = false;
          this.MergeFrom(result);
        }
        return this.result;
      }

      public override bool IsInitialized
      {
        get
        {
          return this.result.IsInitialized;
        }
      }

      protected override MethodDescriptorProto MessageBeingBuilt
      {
        get
        {
          return this.PrepareBuilder();
        }
      }

      public override MethodDescriptorProto.Builder Clear()
      {
        this.result = MethodDescriptorProto.DefaultInstance;
        this.resultIsReadOnly = true;
        return this;
      }

      public override MethodDescriptorProto.Builder Clone()
      {
        if (this.resultIsReadOnly)
          return new MethodDescriptorProto.Builder(this.result);
        return new MethodDescriptorProto.Builder().MergeFrom(this.result);
      }

      public override MessageDescriptor DescriptorForType
      {
        get
        {
          return MethodDescriptorProto.Descriptor;
        }
      }

      public override MethodDescriptorProto DefaultInstanceForType
      {
        get
        {
          return MethodDescriptorProto.DefaultInstance;
        }
      }

      public override MethodDescriptorProto BuildPartial()
      {
        if (this.resultIsReadOnly)
          return this.result;
        this.resultIsReadOnly = true;
        return this.result.MakeReadOnly();
      }

      public override MethodDescriptorProto.Builder MergeFrom(IMessage other)
      {
        if (other is MethodDescriptorProto)
          return this.MergeFrom((MethodDescriptorProto) other);
        base.MergeFrom(other);
        return this;
      }

      public override MethodDescriptorProto.Builder MergeFrom(MethodDescriptorProto other)
      {
        if (other == MethodDescriptorProto.DefaultInstance)
          return this;
        this.PrepareBuilder();
        if (other.HasName)
          this.Name = other.Name;
        if (other.HasInputType)
          this.InputType = other.InputType;
        if (other.HasOutputType)
          this.OutputType = other.OutputType;
        if (other.HasOptions)
          this.MergeOptions(other.Options);
        this.MergeUnknownFields(other.UnknownFields);
        return this;
      }

      public override MethodDescriptorProto.Builder MergeFrom(ICodedInputStream input)
      {
        return this.MergeFrom(input, ExtensionRegistry.Empty);
      }

      public override MethodDescriptorProto.Builder MergeFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
      {
        this.PrepareBuilder();
        UnknownFieldSet.Builder unknownFields = (UnknownFieldSet.Builder) null;
        uint fieldTag;
        string fieldName;
        while (input.ReadTag(out fieldTag, out fieldName))
        {
          if ((int) fieldTag == 0 && fieldName != null)
          {
            int index = Array.BinarySearch<string>(MethodDescriptorProto._methodDescriptorProtoFieldNames, fieldName, (IComparer<string>) StringComparer.Ordinal);
            if (index >= 0)
            {
              fieldTag = MethodDescriptorProto._methodDescriptorProtoFieldTags[index];
            }
            else
            {
              if (unknownFields == null)
                unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
              this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
              continue;
            }
          }
          switch (fieldTag)
          {
            case 0:
              throw InvalidProtocolBufferException.InvalidTag();
            case 10:
              this.result.hasName = input.ReadString(ref this.result.name_);
              continue;
            case 18:
              this.result.hasInputType = input.ReadString(ref this.result.inputType_);
              continue;
            case 26:
              this.result.hasOutputType = input.ReadString(ref this.result.outputType_);
              continue;
            case 34:
              MethodOptions.Builder builder = MethodOptions.CreateBuilder();
              if (this.result.hasOptions)
                builder.MergeFrom(this.Options);
              input.ReadMessage((IBuilderLite) builder, extensionRegistry);
              this.Options = builder.BuildPartial();
              continue;
            default:
              if (WireFormat.IsEndGroupTag(fieldTag))
              {
                if (unknownFields != null)
                  this.UnknownFields = unknownFields.Build();
                return this;
              }
              if (unknownFields == null)
                unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
              this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
              continue;
          }
        }
        if (unknownFields != null)
          this.UnknownFields = unknownFields.Build();
        return this;
      }

      public bool HasName
      {
        get
        {
          return this.result.hasName;
        }
      }

      public string Name
      {
        get
        {
          return this.result.Name;
        }
        set
        {
          this.SetName(value);
        }
      }

      public MethodDescriptorProto.Builder SetName(string value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.hasName = true;
        this.result.name_ = value;
        return this;
      }

      public MethodDescriptorProto.Builder ClearName()
      {
        this.PrepareBuilder();
        this.result.hasName = false;
        this.result.name_ = "";
        return this;
      }

      public bool HasInputType
      {
        get
        {
          return this.result.hasInputType;
        }
      }

      public string InputType
      {
        get
        {
          return this.result.InputType;
        }
        set
        {
          this.SetInputType(value);
        }
      }

      public MethodDescriptorProto.Builder SetInputType(string value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.hasInputType = true;
        this.result.inputType_ = value;
        return this;
      }

      public MethodDescriptorProto.Builder ClearInputType()
      {
        this.PrepareBuilder();
        this.result.hasInputType = false;
        this.result.inputType_ = "";
        return this;
      }

      public bool HasOutputType
      {
        get
        {
          return this.result.hasOutputType;
        }
      }

      public string OutputType
      {
        get
        {
          return this.result.OutputType;
        }
        set
        {
          this.SetOutputType(value);
        }
      }

      public MethodDescriptorProto.Builder SetOutputType(string value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.hasOutputType = true;
        this.result.outputType_ = value;
        return this;
      }

      public MethodDescriptorProto.Builder ClearOutputType()
      {
        this.PrepareBuilder();
        this.result.hasOutputType = false;
        this.result.outputType_ = "";
        return this;
      }

      public bool HasOptions
      {
        get
        {
          return this.result.hasOptions;
        }
      }

      public MethodOptions Options
      {
        get
        {
          return this.result.Options;
        }
        set
        {
          this.SetOptions(value);
        }
      }

      public MethodDescriptorProto.Builder SetOptions(MethodOptions value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.hasOptions = true;
        this.result.options_ = value;
        return this;
      }

      public MethodDescriptorProto.Builder SetOptions(MethodOptions.Builder builderForValue)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
        this.PrepareBuilder();
        this.result.hasOptions = true;
        this.result.options_ = builderForValue.Build();
        return this;
      }

      public MethodDescriptorProto.Builder MergeOptions(MethodOptions value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.options_ = !this.result.hasOptions || this.result.options_ == MethodOptions.DefaultInstance ? value : MethodOptions.CreateBuilder(this.result.options_).MergeFrom(value).BuildPartial();
        this.result.hasOptions = true;
        return this;
      }

      public MethodDescriptorProto.Builder ClearOptions()
      {
        this.PrepareBuilder();
        this.result.hasOptions = false;
        this.result.options_ = (MethodOptions) null;
        return this;
      }
    }
  }
}
