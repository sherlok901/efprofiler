﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.DescriptorProtos.MethodOptions
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers.Collections;
using Google.ProtocolBuffers.Descriptors;
using Google.ProtocolBuffers.FieldAccess;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace Google.ProtocolBuffers.DescriptorProtos
{
  [DebuggerNonUserCode]
  public sealed class MethodOptions : ExtendableMessage<MethodOptions, MethodOptions.Builder>
  {
    private static readonly MethodOptions defaultInstance = new MethodOptions().MakeReadOnly();
    private static readonly string[] _methodOptionsFieldNames = new string[1]
    {
      "uninterpreted_option"
    };
    private static readonly uint[] _methodOptionsFieldTags = new uint[1]
    {
      7994U
    };
    private PopsicleList<UninterpretedOption> uninterpretedOption_ = new PopsicleList<UninterpretedOption>();
    private int memoizedSerializedSize = -1;
    public const int UninterpretedOptionFieldNumber = 999;

    private MethodOptions()
    {
    }

    public static MethodOptions DefaultInstance
    {
      get
      {
        return MethodOptions.defaultInstance;
      }
    }

    public override MethodOptions DefaultInstanceForType
    {
      get
      {
        return MethodOptions.DefaultInstance;
      }
    }

    protected override MethodOptions ThisMessage
    {
      get
      {
        return this;
      }
    }

    public static MessageDescriptor Descriptor
    {
      get
      {
        return DescriptorProtoFile.internal__static_google_protobuf_MethodOptions__Descriptor;
      }
    }

    protected override FieldAccessorTable<MethodOptions, MethodOptions.Builder> InternalFieldAccessors
    {
      get
      {
        return DescriptorProtoFile.internal__static_google_protobuf_MethodOptions__FieldAccessorTable;
      }
    }

    public IList<UninterpretedOption> UninterpretedOptionList
    {
      get
      {
        return (IList<UninterpretedOption>) this.uninterpretedOption_;
      }
    }

    public int UninterpretedOptionCount
    {
      get
      {
        return this.uninterpretedOption_.Count;
      }
    }

    public UninterpretedOption GetUninterpretedOption(int index)
    {
      return this.uninterpretedOption_[index];
    }

    public override bool IsInitialized
    {
      get
      {
        foreach (AbstractMessageLite<UninterpretedOption, UninterpretedOption.Builder> uninterpretedOption in (IEnumerable<UninterpretedOption>) this.UninterpretedOptionList)
        {
          if (!uninterpretedOption.IsInitialized)
            return false;
        }
        return this.ExtensionsAreInitialized;
      }
    }

    public override void WriteTo(ICodedOutputStream output)
    {
      int serializedSize = this.SerializedSize;
      string[] optionsFieldNames = MethodOptions._methodOptionsFieldNames;
      ExtendableMessage<MethodOptions, MethodOptions.Builder>.ExtensionWriter extensionWriter = this.CreateExtensionWriter((ExtendableMessage<MethodOptions, MethodOptions.Builder>) this);
      if (this.uninterpretedOption_.Count > 0)
        output.WriteMessageArray<UninterpretedOption>(999, optionsFieldNames[0], (IEnumerable<UninterpretedOption>) this.uninterpretedOption_);
      extensionWriter.WriteUntil(536870912, output);
      this.UnknownFields.WriteTo(output);
    }

    public override int SerializedSize
    {
      get
      {
        int memoizedSerializedSize = this.memoizedSerializedSize;
        if (memoizedSerializedSize != -1)
          return memoizedSerializedSize;
        int num1 = 0;
        foreach (UninterpretedOption uninterpretedOption in (IEnumerable<UninterpretedOption>) this.UninterpretedOptionList)
          num1 += CodedOutputStream.ComputeMessageSize(999, (IMessageLite) uninterpretedOption);
        int num2 = num1 + this.ExtensionsSerializedSize + this.UnknownFields.SerializedSize;
        this.memoizedSerializedSize = num2;
        return num2;
      }
    }

    public static MethodOptions ParseFrom(ByteString data)
    {
      return MethodOptions.CreateBuilder().MergeFrom(data).BuildParsed();
    }

    public static MethodOptions ParseFrom(ByteString data, ExtensionRegistry extensionRegistry)
    {
      return MethodOptions.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
    }

    public static MethodOptions ParseFrom(byte[] data)
    {
      return MethodOptions.CreateBuilder().MergeFrom(data).BuildParsed();
    }

    public static MethodOptions ParseFrom(byte[] data, ExtensionRegistry extensionRegistry)
    {
      return MethodOptions.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
    }

    public static MethodOptions ParseFrom(Stream input)
    {
      return MethodOptions.CreateBuilder().MergeFrom(input).BuildParsed();
    }

    public static MethodOptions ParseFrom(Stream input, ExtensionRegistry extensionRegistry)
    {
      return MethodOptions.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
    }

    public static MethodOptions ParseDelimitedFrom(Stream input)
    {
      return MethodOptions.CreateBuilder().MergeDelimitedFrom(input).BuildParsed();
    }

    public static MethodOptions ParseDelimitedFrom(Stream input, ExtensionRegistry extensionRegistry)
    {
      return MethodOptions.CreateBuilder().MergeDelimitedFrom(input, extensionRegistry).BuildParsed();
    }

    public static MethodOptions ParseFrom(ICodedInputStream input)
    {
      return MethodOptions.CreateBuilder().MergeFrom(input).BuildParsed();
    }

    public static MethodOptions ParseFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
    {
      return MethodOptions.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
    }

    private MethodOptions MakeReadOnly()
    {
      this.uninterpretedOption_.MakeReadOnly();
      return this;
    }

    public static MethodOptions.Builder CreateBuilder()
    {
      return new MethodOptions.Builder();
    }

    public override MethodOptions.Builder ToBuilder()
    {
      return MethodOptions.CreateBuilder(this);
    }

    public override MethodOptions.Builder CreateBuilderForType()
    {
      return new MethodOptions.Builder();
    }

    public static MethodOptions.Builder CreateBuilder(MethodOptions prototype)
    {
      return new MethodOptions.Builder(prototype);
    }

    static MethodOptions()
    {
      object.ReferenceEquals((object) DescriptorProtoFile.Descriptor, (object) null);
    }

    [DebuggerNonUserCode]
    public sealed class Builder : ExtendableBuilder<MethodOptions, MethodOptions.Builder>
    {
      private bool resultIsReadOnly;
      private MethodOptions result;

      protected override MethodOptions.Builder ThisBuilder
      {
        get
        {
          return this;
        }
      }

      public Builder()
      {
        this.result = MethodOptions.DefaultInstance;
        this.resultIsReadOnly = true;
      }

      internal Builder(MethodOptions cloneFrom)
      {
        this.result = cloneFrom;
        this.resultIsReadOnly = true;
      }

      private MethodOptions PrepareBuilder()
      {
        if (this.resultIsReadOnly)
        {
          MethodOptions result = this.result;
          this.result = new MethodOptions();
          this.resultIsReadOnly = false;
          this.MergeFrom(result);
        }
        return this.result;
      }

      public override bool IsInitialized
      {
        get
        {
          return this.result.IsInitialized;
        }
      }

      protected override MethodOptions MessageBeingBuilt
      {
        get
        {
          return this.PrepareBuilder();
        }
      }

      public override MethodOptions.Builder Clear()
      {
        this.result = MethodOptions.DefaultInstance;
        this.resultIsReadOnly = true;
        return this;
      }

      public override MethodOptions.Builder Clone()
      {
        if (this.resultIsReadOnly)
          return new MethodOptions.Builder(this.result);
        return new MethodOptions.Builder().MergeFrom(this.result);
      }

      public override MessageDescriptor DescriptorForType
      {
        get
        {
          return MethodOptions.Descriptor;
        }
      }

      public override MethodOptions DefaultInstanceForType
      {
        get
        {
          return MethodOptions.DefaultInstance;
        }
      }

      public override MethodOptions BuildPartial()
      {
        if (this.resultIsReadOnly)
          return this.result;
        this.resultIsReadOnly = true;
        return this.result.MakeReadOnly();
      }

      public override MethodOptions.Builder MergeFrom(IMessage other)
      {
        if (other is MethodOptions)
          return this.MergeFrom((MethodOptions) other);
        base.MergeFrom(other);
        return this;
      }

      public override MethodOptions.Builder MergeFrom(MethodOptions other)
      {
        if (other == MethodOptions.DefaultInstance)
          return this;
        this.PrepareBuilder();
        if (other.uninterpretedOption_.Count != 0)
          this.result.uninterpretedOption_.Add((IEnumerable<UninterpretedOption>) other.uninterpretedOption_);
        this.MergeExtensionFields((ExtendableMessage<MethodOptions, MethodOptions.Builder>) other);
        this.MergeUnknownFields(other.UnknownFields);
        return this;
      }

      public override MethodOptions.Builder MergeFrom(ICodedInputStream input)
      {
        return this.MergeFrom(input, ExtensionRegistry.Empty);
      }

      public override MethodOptions.Builder MergeFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
      {
        this.PrepareBuilder();
        UnknownFieldSet.Builder unknownFields = (UnknownFieldSet.Builder) null;
        uint fieldTag;
        string fieldName;
        while (input.ReadTag(out fieldTag, out fieldName))
        {
          if ((int) fieldTag == 0 && fieldName != null)
          {
            int index = Array.BinarySearch<string>(MethodOptions._methodOptionsFieldNames, fieldName, (IComparer<string>) StringComparer.Ordinal);
            if (index >= 0)
            {
              fieldTag = MethodOptions._methodOptionsFieldTags[index];
            }
            else
            {
              if (unknownFields == null)
                unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
              this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
              continue;
            }
          }
          switch (fieldTag)
          {
            case 0:
              throw InvalidProtocolBufferException.InvalidTag();
            case 7994:
              input.ReadMessageArray<UninterpretedOption>(fieldTag, fieldName, (ICollection<UninterpretedOption>) this.result.uninterpretedOption_, UninterpretedOption.DefaultInstance, extensionRegistry);
              continue;
            default:
              if (WireFormat.IsEndGroupTag(fieldTag))
              {
                if (unknownFields != null)
                  this.UnknownFields = unknownFields.Build();
                return this;
              }
              if (unknownFields == null)
                unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
              this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
              continue;
          }
        }
        if (unknownFields != null)
          this.UnknownFields = unknownFields.Build();
        return this;
      }

      public IPopsicleList<UninterpretedOption> UninterpretedOptionList
      {
        get
        {
          return (IPopsicleList<UninterpretedOption>) this.PrepareBuilder().uninterpretedOption_;
        }
      }

      public int UninterpretedOptionCount
      {
        get
        {
          return this.result.UninterpretedOptionCount;
        }
      }

      public UninterpretedOption GetUninterpretedOption(int index)
      {
        return this.result.GetUninterpretedOption(index);
      }

      public MethodOptions.Builder SetUninterpretedOption(int index, UninterpretedOption value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.uninterpretedOption_[index] = value;
        return this;
      }

      public MethodOptions.Builder SetUninterpretedOption(int index, UninterpretedOption.Builder builderForValue)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
        this.PrepareBuilder();
        this.result.uninterpretedOption_[index] = builderForValue.Build();
        return this;
      }

      public MethodOptions.Builder AddUninterpretedOption(UninterpretedOption value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.uninterpretedOption_.Add(value);
        return this;
      }

      public MethodOptions.Builder AddUninterpretedOption(UninterpretedOption.Builder builderForValue)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
        this.PrepareBuilder();
        this.result.uninterpretedOption_.Add(builderForValue.Build());
        return this;
      }

      public MethodOptions.Builder AddRangeUninterpretedOption(IEnumerable<UninterpretedOption> values)
      {
        this.PrepareBuilder();
        this.result.uninterpretedOption_.Add(values);
        return this;
      }

      public MethodOptions.Builder ClearUninterpretedOption()
      {
        this.PrepareBuilder();
        this.result.uninterpretedOption_.Clear();
        return this;
      }
    }
  }
}
