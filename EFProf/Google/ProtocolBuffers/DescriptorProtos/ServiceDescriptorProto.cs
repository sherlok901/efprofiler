﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.DescriptorProtos.ServiceDescriptorProto
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers.Collections;
using Google.ProtocolBuffers.Descriptors;
using Google.ProtocolBuffers.FieldAccess;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace Google.ProtocolBuffers.DescriptorProtos
{
  [DebuggerNonUserCode]
  public sealed class ServiceDescriptorProto : GeneratedMessage<ServiceDescriptorProto, ServiceDescriptorProto.Builder>, IDescriptorProto<ServiceOptions>
  {
    private static readonly ServiceDescriptorProto defaultInstance = new ServiceDescriptorProto().MakeReadOnly();
    private static readonly string[] _serviceDescriptorProtoFieldNames = new string[3]
    {
      "method",
      "name",
      "options"
    };
    private static readonly uint[] _serviceDescriptorProtoFieldTags = new uint[3]
    {
      18U,
      10U,
      26U
    };
    private string name_ = "";
    private PopsicleList<MethodDescriptorProto> method_ = new PopsicleList<MethodDescriptorProto>();
    private int memoizedSerializedSize = -1;
    public const int NameFieldNumber = 1;
    public const int MethodFieldNumber = 2;
    public const int OptionsFieldNumber = 3;
    private bool hasName;
    private bool hasOptions;
    private ServiceOptions options_;

    private ServiceDescriptorProto()
    {
    }

    public static ServiceDescriptorProto DefaultInstance
    {
      get
      {
        return ServiceDescriptorProto.defaultInstance;
      }
    }

    public override ServiceDescriptorProto DefaultInstanceForType
    {
      get
      {
        return ServiceDescriptorProto.DefaultInstance;
      }
    }

    protected override ServiceDescriptorProto ThisMessage
    {
      get
      {
        return this;
      }
    }

    public static MessageDescriptor Descriptor
    {
      get
      {
        return DescriptorProtoFile.internal__static_google_protobuf_ServiceDescriptorProto__Descriptor;
      }
    }

    protected override FieldAccessorTable<ServiceDescriptorProto, ServiceDescriptorProto.Builder> InternalFieldAccessors
    {
      get
      {
        return DescriptorProtoFile.internal__static_google_protobuf_ServiceDescriptorProto__FieldAccessorTable;
      }
    }

    public bool HasName
    {
      get
      {
        return this.hasName;
      }
    }

    public string Name
    {
      get
      {
        return this.name_;
      }
    }

    public IList<MethodDescriptorProto> MethodList
    {
      get
      {
        return (IList<MethodDescriptorProto>) this.method_;
      }
    }

    public int MethodCount
    {
      get
      {
        return this.method_.Count;
      }
    }

    public MethodDescriptorProto GetMethod(int index)
    {
      return this.method_[index];
    }

    public bool HasOptions
    {
      get
      {
        return this.hasOptions;
      }
    }

    public ServiceOptions Options
    {
      get
      {
        return this.options_ ?? ServiceOptions.DefaultInstance;
      }
    }

    public override bool IsInitialized
    {
      get
      {
        foreach (AbstractMessageLite<MethodDescriptorProto, MethodDescriptorProto.Builder> method in (IEnumerable<MethodDescriptorProto>) this.MethodList)
        {
          if (!method.IsInitialized)
            return false;
        }
        return !this.HasOptions || this.Options.IsInitialized;
      }
    }

    public override void WriteTo(ICodedOutputStream output)
    {
      int serializedSize = this.SerializedSize;
      string[] descriptorProtoFieldNames = ServiceDescriptorProto._serviceDescriptorProtoFieldNames;
      if (this.hasName)
        output.WriteString(1, descriptorProtoFieldNames[1], this.Name);
      if (this.method_.Count > 0)
        output.WriteMessageArray<MethodDescriptorProto>(2, descriptorProtoFieldNames[0], (IEnumerable<MethodDescriptorProto>) this.method_);
      if (this.hasOptions)
        output.WriteMessage(3, descriptorProtoFieldNames[2], (IMessageLite) this.Options);
      this.UnknownFields.WriteTo(output);
    }

    public override int SerializedSize
    {
      get
      {
        int memoizedSerializedSize = this.memoizedSerializedSize;
        if (memoizedSerializedSize != -1)
          return memoizedSerializedSize;
        int num1 = 0;
        if (this.hasName)
          num1 += CodedOutputStream.ComputeStringSize(1, this.Name);
        foreach (MethodDescriptorProto method in (IEnumerable<MethodDescriptorProto>) this.MethodList)
          num1 += CodedOutputStream.ComputeMessageSize(2, (IMessageLite) method);
        if (this.hasOptions)
          num1 += CodedOutputStream.ComputeMessageSize(3, (IMessageLite) this.Options);
        int num2 = num1 + this.UnknownFields.SerializedSize;
        this.memoizedSerializedSize = num2;
        return num2;
      }
    }

    public static ServiceDescriptorProto ParseFrom(ByteString data)
    {
      return ServiceDescriptorProto.CreateBuilder().MergeFrom(data).BuildParsed();
    }

    public static ServiceDescriptorProto ParseFrom(ByteString data, ExtensionRegistry extensionRegistry)
    {
      return ServiceDescriptorProto.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
    }

    public static ServiceDescriptorProto ParseFrom(byte[] data)
    {
      return ServiceDescriptorProto.CreateBuilder().MergeFrom(data).BuildParsed();
    }

    public static ServiceDescriptorProto ParseFrom(byte[] data, ExtensionRegistry extensionRegistry)
    {
      return ServiceDescriptorProto.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
    }

    public static ServiceDescriptorProto ParseFrom(Stream input)
    {
      return ServiceDescriptorProto.CreateBuilder().MergeFrom(input).BuildParsed();
    }

    public static ServiceDescriptorProto ParseFrom(Stream input, ExtensionRegistry extensionRegistry)
    {
      return ServiceDescriptorProto.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
    }

    public static ServiceDescriptorProto ParseDelimitedFrom(Stream input)
    {
      return ServiceDescriptorProto.CreateBuilder().MergeDelimitedFrom(input).BuildParsed();
    }

    public static ServiceDescriptorProto ParseDelimitedFrom(Stream input, ExtensionRegistry extensionRegistry)
    {
      return ServiceDescriptorProto.CreateBuilder().MergeDelimitedFrom(input, extensionRegistry).BuildParsed();
    }

    public static ServiceDescriptorProto ParseFrom(ICodedInputStream input)
    {
      return ServiceDescriptorProto.CreateBuilder().MergeFrom(input).BuildParsed();
    }

    public static ServiceDescriptorProto ParseFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
    {
      return ServiceDescriptorProto.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
    }

    private ServiceDescriptorProto MakeReadOnly()
    {
      this.method_.MakeReadOnly();
      return this;
    }

    public static ServiceDescriptorProto.Builder CreateBuilder()
    {
      return new ServiceDescriptorProto.Builder();
    }

    public override ServiceDescriptorProto.Builder ToBuilder()
    {
      return ServiceDescriptorProto.CreateBuilder(this);
    }

    public override ServiceDescriptorProto.Builder CreateBuilderForType()
    {
      return new ServiceDescriptorProto.Builder();
    }

    public static ServiceDescriptorProto.Builder CreateBuilder(ServiceDescriptorProto prototype)
    {
      return new ServiceDescriptorProto.Builder(prototype);
    }

    static ServiceDescriptorProto()
    {
      object.ReferenceEquals((object) DescriptorProtoFile.Descriptor, (object) null);
    }

    [DebuggerNonUserCode]
    public sealed class Builder : GeneratedBuilder<ServiceDescriptorProto, ServiceDescriptorProto.Builder>
    {
      private bool resultIsReadOnly;
      private ServiceDescriptorProto result;

      protected override ServiceDescriptorProto.Builder ThisBuilder
      {
        get
        {
          return this;
        }
      }

      public Builder()
      {
        this.result = ServiceDescriptorProto.DefaultInstance;
        this.resultIsReadOnly = true;
      }

      internal Builder(ServiceDescriptorProto cloneFrom)
      {
        this.result = cloneFrom;
        this.resultIsReadOnly = true;
      }

      private ServiceDescriptorProto PrepareBuilder()
      {
        if (this.resultIsReadOnly)
        {
          ServiceDescriptorProto result = this.result;
          this.result = new ServiceDescriptorProto();
          this.resultIsReadOnly = false;
          this.MergeFrom(result);
        }
        return this.result;
      }

      public override bool IsInitialized
      {
        get
        {
          return this.result.IsInitialized;
        }
      }

      protected override ServiceDescriptorProto MessageBeingBuilt
      {
        get
        {
          return this.PrepareBuilder();
        }
      }

      public override ServiceDescriptorProto.Builder Clear()
      {
        this.result = ServiceDescriptorProto.DefaultInstance;
        this.resultIsReadOnly = true;
        return this;
      }

      public override ServiceDescriptorProto.Builder Clone()
      {
        if (this.resultIsReadOnly)
          return new ServiceDescriptorProto.Builder(this.result);
        return new ServiceDescriptorProto.Builder().MergeFrom(this.result);
      }

      public override MessageDescriptor DescriptorForType
      {
        get
        {
          return ServiceDescriptorProto.Descriptor;
        }
      }

      public override ServiceDescriptorProto DefaultInstanceForType
      {
        get
        {
          return ServiceDescriptorProto.DefaultInstance;
        }
      }

      public override ServiceDescriptorProto BuildPartial()
      {
        if (this.resultIsReadOnly)
          return this.result;
        this.resultIsReadOnly = true;
        return this.result.MakeReadOnly();
      }

      public override ServiceDescriptorProto.Builder MergeFrom(IMessage other)
      {
        if (other is ServiceDescriptorProto)
          return this.MergeFrom((ServiceDescriptorProto) other);
        base.MergeFrom(other);
        return this;
      }

      public override ServiceDescriptorProto.Builder MergeFrom(ServiceDescriptorProto other)
      {
        if (other == ServiceDescriptorProto.DefaultInstance)
          return this;
        this.PrepareBuilder();
        if (other.HasName)
          this.Name = other.Name;
        if (other.method_.Count != 0)
          this.result.method_.Add((IEnumerable<MethodDescriptorProto>) other.method_);
        if (other.HasOptions)
          this.MergeOptions(other.Options);
        this.MergeUnknownFields(other.UnknownFields);
        return this;
      }

      public override ServiceDescriptorProto.Builder MergeFrom(ICodedInputStream input)
      {
        return this.MergeFrom(input, ExtensionRegistry.Empty);
      }

      public override ServiceDescriptorProto.Builder MergeFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
      {
        this.PrepareBuilder();
        UnknownFieldSet.Builder unknownFields = (UnknownFieldSet.Builder) null;
        uint fieldTag;
        string fieldName;
        while (input.ReadTag(out fieldTag, out fieldName))
        {
          if ((int) fieldTag == 0 && fieldName != null)
          {
            int index = Array.BinarySearch<string>(ServiceDescriptorProto._serviceDescriptorProtoFieldNames, fieldName, (IComparer<string>) StringComparer.Ordinal);
            if (index >= 0)
            {
              fieldTag = ServiceDescriptorProto._serviceDescriptorProtoFieldTags[index];
            }
            else
            {
              if (unknownFields == null)
                unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
              this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
              continue;
            }
          }
          switch (fieldTag)
          {
            case 0:
              throw InvalidProtocolBufferException.InvalidTag();
            case 10:
              this.result.hasName = input.ReadString(ref this.result.name_);
              continue;
            case 18:
              input.ReadMessageArray<MethodDescriptorProto>(fieldTag, fieldName, (ICollection<MethodDescriptorProto>) this.result.method_, MethodDescriptorProto.DefaultInstance, extensionRegistry);
              continue;
            case 26:
              ServiceOptions.Builder builder = ServiceOptions.CreateBuilder();
              if (this.result.hasOptions)
                builder.MergeFrom(this.Options);
              input.ReadMessage((IBuilderLite) builder, extensionRegistry);
              this.Options = builder.BuildPartial();
              continue;
            default:
              if (WireFormat.IsEndGroupTag(fieldTag))
              {
                if (unknownFields != null)
                  this.UnknownFields = unknownFields.Build();
                return this;
              }
              if (unknownFields == null)
                unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
              this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
              continue;
          }
        }
        if (unknownFields != null)
          this.UnknownFields = unknownFields.Build();
        return this;
      }

      public bool HasName
      {
        get
        {
          return this.result.hasName;
        }
      }

      public string Name
      {
        get
        {
          return this.result.Name;
        }
        set
        {
          this.SetName(value);
        }
      }

      public ServiceDescriptorProto.Builder SetName(string value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.hasName = true;
        this.result.name_ = value;
        return this;
      }

      public ServiceDescriptorProto.Builder ClearName()
      {
        this.PrepareBuilder();
        this.result.hasName = false;
        this.result.name_ = "";
        return this;
      }

      public IPopsicleList<MethodDescriptorProto> MethodList
      {
        get
        {
          return (IPopsicleList<MethodDescriptorProto>) this.PrepareBuilder().method_;
        }
      }

      public int MethodCount
      {
        get
        {
          return this.result.MethodCount;
        }
      }

      public MethodDescriptorProto GetMethod(int index)
      {
        return this.result.GetMethod(index);
      }

      public ServiceDescriptorProto.Builder SetMethod(int index, MethodDescriptorProto value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.method_[index] = value;
        return this;
      }

      public ServiceDescriptorProto.Builder SetMethod(int index, MethodDescriptorProto.Builder builderForValue)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
        this.PrepareBuilder();
        this.result.method_[index] = builderForValue.Build();
        return this;
      }

      public ServiceDescriptorProto.Builder AddMethod(MethodDescriptorProto value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.method_.Add(value);
        return this;
      }

      public ServiceDescriptorProto.Builder AddMethod(MethodDescriptorProto.Builder builderForValue)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
        this.PrepareBuilder();
        this.result.method_.Add(builderForValue.Build());
        return this;
      }

      public ServiceDescriptorProto.Builder AddRangeMethod(IEnumerable<MethodDescriptorProto> values)
      {
        this.PrepareBuilder();
        this.result.method_.Add(values);
        return this;
      }

      public ServiceDescriptorProto.Builder ClearMethod()
      {
        this.PrepareBuilder();
        this.result.method_.Clear();
        return this;
      }

      public bool HasOptions
      {
        get
        {
          return this.result.hasOptions;
        }
      }

      public ServiceOptions Options
      {
        get
        {
          return this.result.Options;
        }
        set
        {
          this.SetOptions(value);
        }
      }

      public ServiceDescriptorProto.Builder SetOptions(ServiceOptions value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.hasOptions = true;
        this.result.options_ = value;
        return this;
      }

      public ServiceDescriptorProto.Builder SetOptions(ServiceOptions.Builder builderForValue)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
        this.PrepareBuilder();
        this.result.hasOptions = true;
        this.result.options_ = builderForValue.Build();
        return this;
      }

      public ServiceDescriptorProto.Builder MergeOptions(ServiceOptions value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.options_ = !this.result.hasOptions || this.result.options_ == ServiceOptions.DefaultInstance ? value : ServiceOptions.CreateBuilder(this.result.options_).MergeFrom(value).BuildPartial();
        this.result.hasOptions = true;
        return this;
      }

      public ServiceDescriptorProto.Builder ClearOptions()
      {
        this.PrepareBuilder();
        this.result.hasOptions = false;
        this.result.options_ = (ServiceOptions) null;
        return this;
      }
    }
  }
}
