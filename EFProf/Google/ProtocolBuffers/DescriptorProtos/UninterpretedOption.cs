﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.DescriptorProtos.UninterpretedOption
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers.Collections;
using Google.ProtocolBuffers.Descriptors;
using Google.ProtocolBuffers.FieldAccess;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace Google.ProtocolBuffers.DescriptorProtos
{
  [DebuggerNonUserCode]
  public sealed class UninterpretedOption : GeneratedMessage<UninterpretedOption, UninterpretedOption.Builder>
  {
    private static readonly UninterpretedOption defaultInstance = new UninterpretedOption().MakeReadOnly();
    private static readonly string[] _uninterpretedOptionFieldNames = new string[7]
    {
      "aggregate_value",
      "double_value",
      "identifier_value",
      "name",
      "negative_int_value",
      "positive_int_value",
      "string_value"
    };
    private static readonly uint[] _uninterpretedOptionFieldTags = new uint[7]
    {
      66U,
      49U,
      26U,
      18U,
      40U,
      32U,
      58U
    };
    private PopsicleList<UninterpretedOption.Types.NamePart> name_ = new PopsicleList<UninterpretedOption.Types.NamePart>();
    private string identifierValue_ = "";
    private ByteString stringValue_ = ByteString.Empty;
    private string aggregateValue_ = "";
    private int memoizedSerializedSize = -1;
    public const int NameFieldNumber = 2;
    public const int IdentifierValueFieldNumber = 3;
    public const int PositiveIntValueFieldNumber = 4;
    public const int NegativeIntValueFieldNumber = 5;
    public const int DoubleValueFieldNumber = 6;
    public const int StringValueFieldNumber = 7;
    public const int AggregateValueFieldNumber = 8;
    private bool hasIdentifierValue;
    private bool hasPositiveIntValue;
    private ulong positiveIntValue_;
    private bool hasNegativeIntValue;
    private long negativeIntValue_;
    private bool hasDoubleValue;
    private double doubleValue_;
    private bool hasStringValue;
    private bool hasAggregateValue;

    private UninterpretedOption()
    {
    }

    public static UninterpretedOption DefaultInstance
    {
      get
      {
        return UninterpretedOption.defaultInstance;
      }
    }

    public override UninterpretedOption DefaultInstanceForType
    {
      get
      {
        return UninterpretedOption.DefaultInstance;
      }
    }

    protected override UninterpretedOption ThisMessage
    {
      get
      {
        return this;
      }
    }

    public static MessageDescriptor Descriptor
    {
      get
      {
        return DescriptorProtoFile.internal__static_google_protobuf_UninterpretedOption__Descriptor;
      }
    }

    protected override FieldAccessorTable<UninterpretedOption, UninterpretedOption.Builder> InternalFieldAccessors
    {
      get
      {
        return DescriptorProtoFile.internal__static_google_protobuf_UninterpretedOption__FieldAccessorTable;
      }
    }

    public IList<UninterpretedOption.Types.NamePart> NameList
    {
      get
      {
        return (IList<UninterpretedOption.Types.NamePart>) this.name_;
      }
    }

    public int NameCount
    {
      get
      {
        return this.name_.Count;
      }
    }

    public UninterpretedOption.Types.NamePart GetName(int index)
    {
      return this.name_[index];
    }

    public bool HasIdentifierValue
    {
      get
      {
        return this.hasIdentifierValue;
      }
    }

    public string IdentifierValue
    {
      get
      {
        return this.identifierValue_;
      }
    }

    public bool HasPositiveIntValue
    {
      get
      {
        return this.hasPositiveIntValue;
      }
    }

    [CLSCompliant(false)]
    public ulong PositiveIntValue
    {
      get
      {
        return this.positiveIntValue_;
      }
    }

    public bool HasNegativeIntValue
    {
      get
      {
        return this.hasNegativeIntValue;
      }
    }

    public long NegativeIntValue
    {
      get
      {
        return this.negativeIntValue_;
      }
    }

    public bool HasDoubleValue
    {
      get
      {
        return this.hasDoubleValue;
      }
    }

    public double DoubleValue
    {
      get
      {
        return this.doubleValue_;
      }
    }

    public bool HasStringValue
    {
      get
      {
        return this.hasStringValue;
      }
    }

    public ByteString StringValue
    {
      get
      {
        return this.stringValue_;
      }
    }

    public bool HasAggregateValue
    {
      get
      {
        return this.hasAggregateValue;
      }
    }

    public string AggregateValue
    {
      get
      {
        return this.aggregateValue_;
      }
    }

    public override bool IsInitialized
    {
      get
      {
        foreach (AbstractMessageLite<UninterpretedOption.Types.NamePart, UninterpretedOption.Types.NamePart.Builder> name in (IEnumerable<UninterpretedOption.Types.NamePart>) this.NameList)
        {
          if (!name.IsInitialized)
            return false;
        }
        return true;
      }
    }

    public override void WriteTo(ICodedOutputStream output)
    {
      int serializedSize = this.SerializedSize;
      string[] optionFieldNames = UninterpretedOption._uninterpretedOptionFieldNames;
      if (this.name_.Count > 0)
        output.WriteMessageArray<UninterpretedOption.Types.NamePart>(2, optionFieldNames[3], (IEnumerable<UninterpretedOption.Types.NamePart>) this.name_);
      if (this.hasIdentifierValue)
        output.WriteString(3, optionFieldNames[2], this.IdentifierValue);
      if (this.hasPositiveIntValue)
        output.WriteUInt64(4, optionFieldNames[5], this.PositiveIntValue);
      if (this.hasNegativeIntValue)
        output.WriteInt64(5, optionFieldNames[4], this.NegativeIntValue);
      if (this.hasDoubleValue)
        output.WriteDouble(6, optionFieldNames[1], this.DoubleValue);
      if (this.hasStringValue)
        output.WriteBytes(7, optionFieldNames[6], this.StringValue);
      if (this.hasAggregateValue)
        output.WriteString(8, optionFieldNames[0], this.AggregateValue);
      this.UnknownFields.WriteTo(output);
    }

    public override int SerializedSize
    {
      get
      {
        int memoizedSerializedSize = this.memoizedSerializedSize;
        if (memoizedSerializedSize != -1)
          return memoizedSerializedSize;
        int num1 = 0;
        foreach (UninterpretedOption.Types.NamePart name in (IEnumerable<UninterpretedOption.Types.NamePart>) this.NameList)
          num1 += CodedOutputStream.ComputeMessageSize(2, (IMessageLite) name);
        if (this.hasIdentifierValue)
          num1 += CodedOutputStream.ComputeStringSize(3, this.IdentifierValue);
        if (this.hasPositiveIntValue)
          num1 += CodedOutputStream.ComputeUInt64Size(4, this.PositiveIntValue);
        if (this.hasNegativeIntValue)
          num1 += CodedOutputStream.ComputeInt64Size(5, this.NegativeIntValue);
        if (this.hasDoubleValue)
          num1 += CodedOutputStream.ComputeDoubleSize(6, this.DoubleValue);
        if (this.hasStringValue)
          num1 += CodedOutputStream.ComputeBytesSize(7, this.StringValue);
        if (this.hasAggregateValue)
          num1 += CodedOutputStream.ComputeStringSize(8, this.AggregateValue);
        int num2 = num1 + this.UnknownFields.SerializedSize;
        this.memoizedSerializedSize = num2;
        return num2;
      }
    }

    public static UninterpretedOption ParseFrom(ByteString data)
    {
      return UninterpretedOption.CreateBuilder().MergeFrom(data).BuildParsed();
    }

    public static UninterpretedOption ParseFrom(ByteString data, ExtensionRegistry extensionRegistry)
    {
      return UninterpretedOption.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
    }

    public static UninterpretedOption ParseFrom(byte[] data)
    {
      return UninterpretedOption.CreateBuilder().MergeFrom(data).BuildParsed();
    }

    public static UninterpretedOption ParseFrom(byte[] data, ExtensionRegistry extensionRegistry)
    {
      return UninterpretedOption.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
    }

    public static UninterpretedOption ParseFrom(Stream input)
    {
      return UninterpretedOption.CreateBuilder().MergeFrom(input).BuildParsed();
    }

    public static UninterpretedOption ParseFrom(Stream input, ExtensionRegistry extensionRegistry)
    {
      return UninterpretedOption.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
    }

    public static UninterpretedOption ParseDelimitedFrom(Stream input)
    {
      return UninterpretedOption.CreateBuilder().MergeDelimitedFrom(input).BuildParsed();
    }

    public static UninterpretedOption ParseDelimitedFrom(Stream input, ExtensionRegistry extensionRegistry)
    {
      return UninterpretedOption.CreateBuilder().MergeDelimitedFrom(input, extensionRegistry).BuildParsed();
    }

    public static UninterpretedOption ParseFrom(ICodedInputStream input)
    {
      return UninterpretedOption.CreateBuilder().MergeFrom(input).BuildParsed();
    }

    public static UninterpretedOption ParseFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
    {
      return UninterpretedOption.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
    }

    private UninterpretedOption MakeReadOnly()
    {
      this.name_.MakeReadOnly();
      return this;
    }

    public static UninterpretedOption.Builder CreateBuilder()
    {
      return new UninterpretedOption.Builder();
    }

    public override UninterpretedOption.Builder ToBuilder()
    {
      return UninterpretedOption.CreateBuilder(this);
    }

    public override UninterpretedOption.Builder CreateBuilderForType()
    {
      return new UninterpretedOption.Builder();
    }

    public static UninterpretedOption.Builder CreateBuilder(UninterpretedOption prototype)
    {
      return new UninterpretedOption.Builder(prototype);
    }

    static UninterpretedOption()
    {
      object.ReferenceEquals((object) DescriptorProtoFile.Descriptor, (object) null);
    }

    [DebuggerNonUserCode]
    public static class Types
    {
      [DebuggerNonUserCode]
      public sealed class NamePart : GeneratedMessage<UninterpretedOption.Types.NamePart, UninterpretedOption.Types.NamePart.Builder>
      {
        private static readonly UninterpretedOption.Types.NamePart defaultInstance = new UninterpretedOption.Types.NamePart().MakeReadOnly();
        private static readonly string[] _namePartFieldNames = new string[2]
        {
          "is_extension",
          "name_part"
        };
        private static readonly uint[] _namePartFieldTags = new uint[2]
        {
          16U,
          10U
        };
        private string namePart_ = "";
        private int memoizedSerializedSize = -1;
        public const int NamePart_FieldNumber = 1;
        public const int IsExtensionFieldNumber = 2;
        private bool hasNamePart_;
        private bool hasIsExtension;
        private bool isExtension_;

        private NamePart()
        {
        }

        public static UninterpretedOption.Types.NamePart DefaultInstance
        {
          get
          {
            return UninterpretedOption.Types.NamePart.defaultInstance;
          }
        }

        public override UninterpretedOption.Types.NamePart DefaultInstanceForType
        {
          get
          {
            return UninterpretedOption.Types.NamePart.DefaultInstance;
          }
        }

        protected override UninterpretedOption.Types.NamePart ThisMessage
        {
          get
          {
            return this;
          }
        }

        public static MessageDescriptor Descriptor
        {
          get
          {
            return DescriptorProtoFile.internal__static_google_protobuf_UninterpretedOption_NamePart__Descriptor;
          }
        }

        protected override FieldAccessorTable<UninterpretedOption.Types.NamePart, UninterpretedOption.Types.NamePart.Builder> InternalFieldAccessors
        {
          get
          {
            return DescriptorProtoFile.internal__static_google_protobuf_UninterpretedOption_NamePart__FieldAccessorTable;
          }
        }

        public bool HasNamePart_
        {
          get
          {
            return this.hasNamePart_;
          }
        }

        public string NamePart_
        {
          get
          {
            return this.namePart_;
          }
        }

        public bool HasIsExtension
        {
          get
          {
            return this.hasIsExtension;
          }
        }

        public bool IsExtension
        {
          get
          {
            return this.isExtension_;
          }
        }

        public override bool IsInitialized
        {
          get
          {
            return this.hasNamePart_ && this.hasIsExtension;
          }
        }

        public override void WriteTo(ICodedOutputStream output)
        {
          int serializedSize = this.SerializedSize;
          string[] namePartFieldNames = UninterpretedOption.Types.NamePart._namePartFieldNames;
          if (this.hasNamePart_)
            output.WriteString(1, namePartFieldNames[1], this.NamePart_);
          if (this.hasIsExtension)
            output.WriteBool(2, namePartFieldNames[0], this.IsExtension);
          this.UnknownFields.WriteTo(output);
        }

        public override int SerializedSize
        {
          get
          {
            int memoizedSerializedSize = this.memoizedSerializedSize;
            if (memoizedSerializedSize != -1)
              return memoizedSerializedSize;
            int num1 = 0;
            if (this.hasNamePart_)
              num1 += CodedOutputStream.ComputeStringSize(1, this.NamePart_);
            if (this.hasIsExtension)
              num1 += CodedOutputStream.ComputeBoolSize(2, this.IsExtension);
            int num2 = num1 + this.UnknownFields.SerializedSize;
            this.memoizedSerializedSize = num2;
            return num2;
          }
        }

        public static UninterpretedOption.Types.NamePart ParseFrom(ByteString data)
        {
          return UninterpretedOption.Types.NamePart.CreateBuilder().MergeFrom(data).BuildParsed();
        }

        public static UninterpretedOption.Types.NamePart ParseFrom(ByteString data, ExtensionRegistry extensionRegistry)
        {
          return UninterpretedOption.Types.NamePart.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
        }

        public static UninterpretedOption.Types.NamePart ParseFrom(byte[] data)
        {
          return UninterpretedOption.Types.NamePart.CreateBuilder().MergeFrom(data).BuildParsed();
        }

        public static UninterpretedOption.Types.NamePart ParseFrom(byte[] data, ExtensionRegistry extensionRegistry)
        {
          return UninterpretedOption.Types.NamePart.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
        }

        public static UninterpretedOption.Types.NamePart ParseFrom(Stream input)
        {
          return UninterpretedOption.Types.NamePart.CreateBuilder().MergeFrom(input).BuildParsed();
        }

        public static UninterpretedOption.Types.NamePart ParseFrom(Stream input, ExtensionRegistry extensionRegistry)
        {
          return UninterpretedOption.Types.NamePart.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
        }

        public static UninterpretedOption.Types.NamePart ParseDelimitedFrom(Stream input)
        {
          return UninterpretedOption.Types.NamePart.CreateBuilder().MergeDelimitedFrom(input).BuildParsed();
        }

        public static UninterpretedOption.Types.NamePart ParseDelimitedFrom(Stream input, ExtensionRegistry extensionRegistry)
        {
          return UninterpretedOption.Types.NamePart.CreateBuilder().MergeDelimitedFrom(input, extensionRegistry).BuildParsed();
        }

        public static UninterpretedOption.Types.NamePart ParseFrom(ICodedInputStream input)
        {
          return UninterpretedOption.Types.NamePart.CreateBuilder().MergeFrom(input).BuildParsed();
        }

        public static UninterpretedOption.Types.NamePart ParseFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
        {
          return UninterpretedOption.Types.NamePart.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
        }

        private UninterpretedOption.Types.NamePart MakeReadOnly()
        {
          return this;
        }

        public static UninterpretedOption.Types.NamePart.Builder CreateBuilder()
        {
          return new UninterpretedOption.Types.NamePart.Builder();
        }

        public override UninterpretedOption.Types.NamePart.Builder ToBuilder()
        {
          return UninterpretedOption.Types.NamePart.CreateBuilder(this);
        }

        public override UninterpretedOption.Types.NamePart.Builder CreateBuilderForType()
        {
          return new UninterpretedOption.Types.NamePart.Builder();
        }

        public static UninterpretedOption.Types.NamePart.Builder CreateBuilder(UninterpretedOption.Types.NamePart prototype)
        {
          return new UninterpretedOption.Types.NamePart.Builder(prototype);
        }

        static NamePart()
        {
          object.ReferenceEquals((object) DescriptorProtoFile.Descriptor, (object) null);
        }

        [DebuggerNonUserCode]
        public sealed class Builder : GeneratedBuilder<UninterpretedOption.Types.NamePart, UninterpretedOption.Types.NamePart.Builder>
        {
          private bool resultIsReadOnly;
          private UninterpretedOption.Types.NamePart result;

          protected override UninterpretedOption.Types.NamePart.Builder ThisBuilder
          {
            get
            {
              return this;
            }
          }

          public Builder()
          {
            this.result = UninterpretedOption.Types.NamePart.DefaultInstance;
            this.resultIsReadOnly = true;
          }

          internal Builder(UninterpretedOption.Types.NamePart cloneFrom)
          {
            this.result = cloneFrom;
            this.resultIsReadOnly = true;
          }

          private UninterpretedOption.Types.NamePart PrepareBuilder()
          {
            if (this.resultIsReadOnly)
            {
              UninterpretedOption.Types.NamePart result = this.result;
              this.result = new UninterpretedOption.Types.NamePart();
              this.resultIsReadOnly = false;
              this.MergeFrom(result);
            }
            return this.result;
          }

          public override bool IsInitialized
          {
            get
            {
              return this.result.IsInitialized;
            }
          }

          protected override UninterpretedOption.Types.NamePart MessageBeingBuilt
          {
            get
            {
              return this.PrepareBuilder();
            }
          }

          public override UninterpretedOption.Types.NamePart.Builder Clear()
          {
            this.result = UninterpretedOption.Types.NamePart.DefaultInstance;
            this.resultIsReadOnly = true;
            return this;
          }

          public override UninterpretedOption.Types.NamePart.Builder Clone()
          {
            if (this.resultIsReadOnly)
              return new UninterpretedOption.Types.NamePart.Builder(this.result);
            return new UninterpretedOption.Types.NamePart.Builder().MergeFrom(this.result);
          }

          public override MessageDescriptor DescriptorForType
          {
            get
            {
              return UninterpretedOption.Types.NamePart.Descriptor;
            }
          }

          public override UninterpretedOption.Types.NamePart DefaultInstanceForType
          {
            get
            {
              return UninterpretedOption.Types.NamePart.DefaultInstance;
            }
          }

          public override UninterpretedOption.Types.NamePart BuildPartial()
          {
            if (this.resultIsReadOnly)
              return this.result;
            this.resultIsReadOnly = true;
            return this.result.MakeReadOnly();
          }

          public override UninterpretedOption.Types.NamePart.Builder MergeFrom(IMessage other)
          {
            if (other is UninterpretedOption.Types.NamePart)
              return this.MergeFrom((UninterpretedOption.Types.NamePart) other);
            base.MergeFrom(other);
            return this;
          }

          public override UninterpretedOption.Types.NamePart.Builder MergeFrom(UninterpretedOption.Types.NamePart other)
          {
            if (other == UninterpretedOption.Types.NamePart.DefaultInstance)
              return this;
            this.PrepareBuilder();
            if (other.HasNamePart_)
              this.NamePart_ = other.NamePart_;
            if (other.HasIsExtension)
              this.IsExtension = other.IsExtension;
            this.MergeUnknownFields(other.UnknownFields);
            return this;
          }

          public override UninterpretedOption.Types.NamePart.Builder MergeFrom(ICodedInputStream input)
          {
            return this.MergeFrom(input, ExtensionRegistry.Empty);
          }

          public override UninterpretedOption.Types.NamePart.Builder MergeFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
          {
            this.PrepareBuilder();
            UnknownFieldSet.Builder unknownFields = (UnknownFieldSet.Builder) null;
            uint fieldTag;
            string fieldName;
            while (input.ReadTag(out fieldTag, out fieldName))
            {
              if ((int) fieldTag == 0 && fieldName != null)
              {
                int index = Array.BinarySearch<string>(UninterpretedOption.Types.NamePart._namePartFieldNames, fieldName, (IComparer<string>) StringComparer.Ordinal);
                if (index >= 0)
                {
                  fieldTag = UninterpretedOption.Types.NamePart._namePartFieldTags[index];
                }
                else
                {
                  if (unknownFields == null)
                    unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
                  this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
                  continue;
                }
              }
              switch (fieldTag)
              {
                case 0:
                  throw InvalidProtocolBufferException.InvalidTag();
                case 10:
                  this.result.hasNamePart_ = input.ReadString(ref this.result.namePart_);
                  continue;
                case 16:
                  this.result.hasIsExtension = input.ReadBool(ref this.result.isExtension_);
                  continue;
                default:
                  if (WireFormat.IsEndGroupTag(fieldTag))
                  {
                    if (unknownFields != null)
                      this.UnknownFields = unknownFields.Build();
                    return this;
                  }
                  if (unknownFields == null)
                    unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
                  this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
                  continue;
              }
            }
            if (unknownFields != null)
              this.UnknownFields = unknownFields.Build();
            return this;
          }

          public bool HasNamePart_
          {
            get
            {
              return this.result.hasNamePart_;
            }
          }

          public string NamePart_
          {
            get
            {
              return this.result.NamePart_;
            }
            set
            {
              this.SetNamePart_(value);
            }
          }

          public UninterpretedOption.Types.NamePart.Builder SetNamePart_(string value)
          {
            Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
            this.PrepareBuilder();
            this.result.hasNamePart_ = true;
            this.result.namePart_ = value;
            return this;
          }

          public UninterpretedOption.Types.NamePart.Builder ClearNamePart_()
          {
            this.PrepareBuilder();
            this.result.hasNamePart_ = false;
            this.result.namePart_ = "";
            return this;
          }

          public bool HasIsExtension
          {
            get
            {
              return this.result.hasIsExtension;
            }
          }

          public bool IsExtension
          {
            get
            {
              return this.result.IsExtension;
            }
            set
            {
              this.SetIsExtension(value);
            }
          }

          public UninterpretedOption.Types.NamePart.Builder SetIsExtension(bool value)
          {
            this.PrepareBuilder();
            this.result.hasIsExtension = true;
            this.result.isExtension_ = value;
            return this;
          }

          public UninterpretedOption.Types.NamePart.Builder ClearIsExtension()
          {
            this.PrepareBuilder();
            this.result.hasIsExtension = false;
            this.result.isExtension_ = false;
            return this;
          }
        }
      }
    }

    [DebuggerNonUserCode]
    public sealed class Builder : GeneratedBuilder<UninterpretedOption, UninterpretedOption.Builder>
    {
      private bool resultIsReadOnly;
      private UninterpretedOption result;

      protected override UninterpretedOption.Builder ThisBuilder
      {
        get
        {
          return this;
        }
      }

      public Builder()
      {
        this.result = UninterpretedOption.DefaultInstance;
        this.resultIsReadOnly = true;
      }

      internal Builder(UninterpretedOption cloneFrom)
      {
        this.result = cloneFrom;
        this.resultIsReadOnly = true;
      }

      private UninterpretedOption PrepareBuilder()
      {
        if (this.resultIsReadOnly)
        {
          UninterpretedOption result = this.result;
          this.result = new UninterpretedOption();
          this.resultIsReadOnly = false;
          this.MergeFrom(result);
        }
        return this.result;
      }

      public override bool IsInitialized
      {
        get
        {
          return this.result.IsInitialized;
        }
      }

      protected override UninterpretedOption MessageBeingBuilt
      {
        get
        {
          return this.PrepareBuilder();
        }
      }

      public override UninterpretedOption.Builder Clear()
      {
        this.result = UninterpretedOption.DefaultInstance;
        this.resultIsReadOnly = true;
        return this;
      }

      public override UninterpretedOption.Builder Clone()
      {
        if (this.resultIsReadOnly)
          return new UninterpretedOption.Builder(this.result);
        return new UninterpretedOption.Builder().MergeFrom(this.result);
      }

      public override MessageDescriptor DescriptorForType
      {
        get
        {
          return UninterpretedOption.Descriptor;
        }
      }

      public override UninterpretedOption DefaultInstanceForType
      {
        get
        {
          return UninterpretedOption.DefaultInstance;
        }
      }

      public override UninterpretedOption BuildPartial()
      {
        if (this.resultIsReadOnly)
          return this.result;
        this.resultIsReadOnly = true;
        return this.result.MakeReadOnly();
      }

      public override UninterpretedOption.Builder MergeFrom(IMessage other)
      {
        if (other is UninterpretedOption)
          return this.MergeFrom((UninterpretedOption) other);
        base.MergeFrom(other);
        return this;
      }

      public override UninterpretedOption.Builder MergeFrom(UninterpretedOption other)
      {
        if (other == UninterpretedOption.DefaultInstance)
          return this;
        this.PrepareBuilder();
        if (other.name_.Count != 0)
          this.result.name_.Add((IEnumerable<UninterpretedOption.Types.NamePart>) other.name_);
        if (other.HasIdentifierValue)
          this.IdentifierValue = other.IdentifierValue;
        if (other.HasPositiveIntValue)
          this.PositiveIntValue = other.PositiveIntValue;
        if (other.HasNegativeIntValue)
          this.NegativeIntValue = other.NegativeIntValue;
        if (other.HasDoubleValue)
          this.DoubleValue = other.DoubleValue;
        if (other.HasStringValue)
          this.StringValue = other.StringValue;
        if (other.HasAggregateValue)
          this.AggregateValue = other.AggregateValue;
        this.MergeUnknownFields(other.UnknownFields);
        return this;
      }

      public override UninterpretedOption.Builder MergeFrom(ICodedInputStream input)
      {
        return this.MergeFrom(input, ExtensionRegistry.Empty);
      }

      public override UninterpretedOption.Builder MergeFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
      {
        this.PrepareBuilder();
        UnknownFieldSet.Builder unknownFields = (UnknownFieldSet.Builder) null;
        uint fieldTag;
        string fieldName;
        while (input.ReadTag(out fieldTag, out fieldName))
        {
          if ((int) fieldTag == 0 && fieldName != null)
          {
            int index = Array.BinarySearch<string>(UninterpretedOption._uninterpretedOptionFieldNames, fieldName, (IComparer<string>) StringComparer.Ordinal);
            if (index >= 0)
            {
              fieldTag = UninterpretedOption._uninterpretedOptionFieldTags[index];
            }
            else
            {
              if (unknownFields == null)
                unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
              this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
              continue;
            }
          }
          switch (fieldTag)
          {
            case 0:
              throw InvalidProtocolBufferException.InvalidTag();
            case 18:
              input.ReadMessageArray<UninterpretedOption.Types.NamePart>(fieldTag, fieldName, (ICollection<UninterpretedOption.Types.NamePart>) this.result.name_, UninterpretedOption.Types.NamePart.DefaultInstance, extensionRegistry);
              continue;
            case 26:
              this.result.hasIdentifierValue = input.ReadString(ref this.result.identifierValue_);
              continue;
            case 32:
              this.result.hasPositiveIntValue = input.ReadUInt64(ref this.result.positiveIntValue_);
              continue;
            case 40:
              this.result.hasNegativeIntValue = input.ReadInt64(ref this.result.negativeIntValue_);
              continue;
            case 49:
              this.result.hasDoubleValue = input.ReadDouble(ref this.result.doubleValue_);
              continue;
            case 58:
              this.result.hasStringValue = input.ReadBytes(ref this.result.stringValue_);
              continue;
            case 66:
              this.result.hasAggregateValue = input.ReadString(ref this.result.aggregateValue_);
              continue;
            default:
              if (WireFormat.IsEndGroupTag(fieldTag))
              {
                if (unknownFields != null)
                  this.UnknownFields = unknownFields.Build();
                return this;
              }
              if (unknownFields == null)
                unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
              this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
              continue;
          }
        }
        if (unknownFields != null)
          this.UnknownFields = unknownFields.Build();
        return this;
      }

      public IPopsicleList<UninterpretedOption.Types.NamePart> NameList
      {
        get
        {
          return (IPopsicleList<UninterpretedOption.Types.NamePart>) this.PrepareBuilder().name_;
        }
      }

      public int NameCount
      {
        get
        {
          return this.result.NameCount;
        }
      }

      public UninterpretedOption.Types.NamePart GetName(int index)
      {
        return this.result.GetName(index);
      }

      public UninterpretedOption.Builder SetName(int index, UninterpretedOption.Types.NamePart value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.name_[index] = value;
        return this;
      }

      public UninterpretedOption.Builder SetName(int index, UninterpretedOption.Types.NamePart.Builder builderForValue)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
        this.PrepareBuilder();
        this.result.name_[index] = builderForValue.Build();
        return this;
      }

      public UninterpretedOption.Builder AddName(UninterpretedOption.Types.NamePart value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.name_.Add(value);
        return this;
      }

      public UninterpretedOption.Builder AddName(UninterpretedOption.Types.NamePart.Builder builderForValue)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
        this.PrepareBuilder();
        this.result.name_.Add(builderForValue.Build());
        return this;
      }

      public UninterpretedOption.Builder AddRangeName(IEnumerable<UninterpretedOption.Types.NamePart> values)
      {
        this.PrepareBuilder();
        this.result.name_.Add(values);
        return this;
      }

      public UninterpretedOption.Builder ClearName()
      {
        this.PrepareBuilder();
        this.result.name_.Clear();
        return this;
      }

      public bool HasIdentifierValue
      {
        get
        {
          return this.result.hasIdentifierValue;
        }
      }

      public string IdentifierValue
      {
        get
        {
          return this.result.IdentifierValue;
        }
        set
        {
          this.SetIdentifierValue(value);
        }
      }

      public UninterpretedOption.Builder SetIdentifierValue(string value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.hasIdentifierValue = true;
        this.result.identifierValue_ = value;
        return this;
      }

      public UninterpretedOption.Builder ClearIdentifierValue()
      {
        this.PrepareBuilder();
        this.result.hasIdentifierValue = false;
        this.result.identifierValue_ = "";
        return this;
      }

      public bool HasPositiveIntValue
      {
        get
        {
          return this.result.hasPositiveIntValue;
        }
      }

      [CLSCompliant(false)]
      public ulong PositiveIntValue
      {
        get
        {
          return this.result.PositiveIntValue;
        }
        set
        {
          this.SetPositiveIntValue(value);
        }
      }

      [CLSCompliant(false)]
      public UninterpretedOption.Builder SetPositiveIntValue(ulong value)
      {
        this.PrepareBuilder();
        this.result.hasPositiveIntValue = true;
        this.result.positiveIntValue_ = value;
        return this;
      }

      public UninterpretedOption.Builder ClearPositiveIntValue()
      {
        this.PrepareBuilder();
        this.result.hasPositiveIntValue = false;
        this.result.positiveIntValue_ = 0UL;
        return this;
      }

      public bool HasNegativeIntValue
      {
        get
        {
          return this.result.hasNegativeIntValue;
        }
      }

      public long NegativeIntValue
      {
        get
        {
          return this.result.NegativeIntValue;
        }
        set
        {
          this.SetNegativeIntValue(value);
        }
      }

      public UninterpretedOption.Builder SetNegativeIntValue(long value)
      {
        this.PrepareBuilder();
        this.result.hasNegativeIntValue = true;
        this.result.negativeIntValue_ = value;
        return this;
      }

      public UninterpretedOption.Builder ClearNegativeIntValue()
      {
        this.PrepareBuilder();
        this.result.hasNegativeIntValue = false;
        this.result.negativeIntValue_ = 0L;
        return this;
      }

      public bool HasDoubleValue
      {
        get
        {
          return this.result.hasDoubleValue;
        }
      }

      public double DoubleValue
      {
        get
        {
          return this.result.DoubleValue;
        }
        set
        {
          this.SetDoubleValue(value);
        }
      }

      public UninterpretedOption.Builder SetDoubleValue(double value)
      {
        this.PrepareBuilder();
        this.result.hasDoubleValue = true;
        this.result.doubleValue_ = value;
        return this;
      }

      public UninterpretedOption.Builder ClearDoubleValue()
      {
        this.PrepareBuilder();
        this.result.hasDoubleValue = false;
        this.result.doubleValue_ = 0.0;
        return this;
      }

      public bool HasStringValue
      {
        get
        {
          return this.result.hasStringValue;
        }
      }

      public ByteString StringValue
      {
        get
        {
          return this.result.StringValue;
        }
        set
        {
          this.SetStringValue(value);
        }
      }

      public UninterpretedOption.Builder SetStringValue(ByteString value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.hasStringValue = true;
        this.result.stringValue_ = value;
        return this;
      }

      public UninterpretedOption.Builder ClearStringValue()
      {
        this.PrepareBuilder();
        this.result.hasStringValue = false;
        this.result.stringValue_ = ByteString.Empty;
        return this;
      }

      public bool HasAggregateValue
      {
        get
        {
          return this.result.hasAggregateValue;
        }
      }

      public string AggregateValue
      {
        get
        {
          return this.result.AggregateValue;
        }
        set
        {
          this.SetAggregateValue(value);
        }
      }

      public UninterpretedOption.Builder SetAggregateValue(string value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.hasAggregateValue = true;
        this.result.aggregateValue_ = value;
        return this;
      }

      public UninterpretedOption.Builder ClearAggregateValue()
      {
        this.PrepareBuilder();
        this.result.hasAggregateValue = false;
        this.result.aggregateValue_ = "";
        return this;
      }
    }
  }
}
