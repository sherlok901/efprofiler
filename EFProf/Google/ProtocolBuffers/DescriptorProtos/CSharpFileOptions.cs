﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.DescriptorProtos.CSharpFileOptions
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers.Descriptors;
using Google.ProtocolBuffers.FieldAccess;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace Google.ProtocolBuffers.DescriptorProtos
{
  [DebuggerNonUserCode]
  public sealed class CSharpFileOptions : GeneratedMessage<CSharpFileOptions, CSharpFileOptions.Builder>
  {
    private static readonly CSharpFileOptions defaultInstance = new CSharpFileOptions().MakeReadOnly();
    private static readonly string[] _cSharpFileOptionsFieldNames = new string[16]
    {
      "add_serializable",
      "cls_compliance",
      "code_contracts",
      "expand_namespace_directories",
      "file_extension",
      "generate_private_ctor",
      "generated_code_attributes",
      "ignore_google_protobuf",
      "multiple_files",
      "namespace",
      "nest_classes",
      "output_directory",
      "public_classes",
      "service_generator_type",
      "umbrella_classname",
      "umbrella_namespace"
    };
    private static readonly uint[] _cSharpFileOptionsFieldTags = new uint[16]
    {
      72U,
      64U,
      48U,
      56U,
      1770U,
      80U,
      1808U,
      1792U,
      32U,
      10U,
      40U,
      1786U,
      24U,
      1800U,
      18U,
      1778U
    };
    private string namespace_ = "";
    private string umbrellaClassname_ = "";
    private bool publicClasses_ = true;
    private bool clsCompliance_ = true;
    private bool generatePrivateCtor_ = true;
    private string fileExtension_ = ".cs";
    private string umbrellaNamespace_ = "";
    private string outputDirectory_ = ".";
    private int memoizedSerializedSize = -1;
    public const int NamespaceFieldNumber = 1;
    public const int UmbrellaClassnameFieldNumber = 2;
    public const int PublicClassesFieldNumber = 3;
    public const int MultipleFilesFieldNumber = 4;
    public const int NestClassesFieldNumber = 5;
    public const int CodeContractsFieldNumber = 6;
    public const int ExpandNamespaceDirectoriesFieldNumber = 7;
    public const int ClsComplianceFieldNumber = 8;
    public const int AddSerializableFieldNumber = 9;
    public const int GeneratePrivateCtorFieldNumber = 10;
    public const int FileExtensionFieldNumber = 221;
    public const int UmbrellaNamespaceFieldNumber = 222;
    public const int OutputDirectoryFieldNumber = 223;
    public const int IgnoreGoogleProtobufFieldNumber = 224;
    public const int ServiceGeneratorTypeFieldNumber = 225;
    public const int GeneratedCodeAttributesFieldNumber = 226;
    private bool hasNamespace;
    private bool hasUmbrellaClassname;
    private bool hasPublicClasses;
    private bool hasMultipleFiles;
    private bool multipleFiles_;
    private bool hasNestClasses;
    private bool nestClasses_;
    private bool hasCodeContracts;
    private bool codeContracts_;
    private bool hasExpandNamespaceDirectories;
    private bool expandNamespaceDirectories_;
    private bool hasClsCompliance;
    private bool hasAddSerializable;
    private bool addSerializable_;
    private bool hasGeneratePrivateCtor;
    private bool hasFileExtension;
    private bool hasUmbrellaNamespace;
    private bool hasOutputDirectory;
    private bool hasIgnoreGoogleProtobuf;
    private bool ignoreGoogleProtobuf_;
    private bool hasServiceGeneratorType;
    private CSharpServiceType serviceGeneratorType_;
    private bool hasGeneratedCodeAttributes;
    private bool generatedCodeAttributes_;

    private CSharpFileOptions()
    {
    }

    public static CSharpFileOptions DefaultInstance
    {
      get
      {
        return CSharpFileOptions.defaultInstance;
      }
    }

    public override CSharpFileOptions DefaultInstanceForType
    {
      get
      {
        return CSharpFileOptions.DefaultInstance;
      }
    }

    protected override CSharpFileOptions ThisMessage
    {
      get
      {
        return this;
      }
    }

    public static MessageDescriptor Descriptor
    {
      get
      {
        return CSharpOptions.internal__static_google_protobuf_CSharpFileOptions__Descriptor;
      }
    }

    protected override FieldAccessorTable<CSharpFileOptions, CSharpFileOptions.Builder> InternalFieldAccessors
    {
      get
      {
        return CSharpOptions.internal__static_google_protobuf_CSharpFileOptions__FieldAccessorTable;
      }
    }

    public bool HasNamespace
    {
      get
      {
        return this.hasNamespace;
      }
    }

    public string Namespace
    {
      get
      {
        return this.namespace_;
      }
    }

    public bool HasUmbrellaClassname
    {
      get
      {
        return this.hasUmbrellaClassname;
      }
    }

    public string UmbrellaClassname
    {
      get
      {
        return this.umbrellaClassname_;
      }
    }

    public bool HasPublicClasses
    {
      get
      {
        return this.hasPublicClasses;
      }
    }

    public bool PublicClasses
    {
      get
      {
        return this.publicClasses_;
      }
    }

    public bool HasMultipleFiles
    {
      get
      {
        return this.hasMultipleFiles;
      }
    }

    public bool MultipleFiles
    {
      get
      {
        return this.multipleFiles_;
      }
    }

    public bool HasNestClasses
    {
      get
      {
        return this.hasNestClasses;
      }
    }

    public bool NestClasses
    {
      get
      {
        return this.nestClasses_;
      }
    }

    public bool HasCodeContracts
    {
      get
      {
        return this.hasCodeContracts;
      }
    }

    public bool CodeContracts
    {
      get
      {
        return this.codeContracts_;
      }
    }

    public bool HasExpandNamespaceDirectories
    {
      get
      {
        return this.hasExpandNamespaceDirectories;
      }
    }

    public bool ExpandNamespaceDirectories
    {
      get
      {
        return this.expandNamespaceDirectories_;
      }
    }

    public bool HasClsCompliance
    {
      get
      {
        return this.hasClsCompliance;
      }
    }

    public bool ClsCompliance
    {
      get
      {
        return this.clsCompliance_;
      }
    }

    public bool HasAddSerializable
    {
      get
      {
        return this.hasAddSerializable;
      }
    }

    public bool AddSerializable
    {
      get
      {
        return this.addSerializable_;
      }
    }

    public bool HasGeneratePrivateCtor
    {
      get
      {
        return this.hasGeneratePrivateCtor;
      }
    }

    public bool GeneratePrivateCtor
    {
      get
      {
        return this.generatePrivateCtor_;
      }
    }

    public bool HasFileExtension
    {
      get
      {
        return this.hasFileExtension;
      }
    }

    public string FileExtension
    {
      get
      {
        return this.fileExtension_;
      }
    }

    public bool HasUmbrellaNamespace
    {
      get
      {
        return this.hasUmbrellaNamespace;
      }
    }

    public string UmbrellaNamespace
    {
      get
      {
        return this.umbrellaNamespace_;
      }
    }

    public bool HasOutputDirectory
    {
      get
      {
        return this.hasOutputDirectory;
      }
    }

    public string OutputDirectory
    {
      get
      {
        return this.outputDirectory_;
      }
    }

    public bool HasIgnoreGoogleProtobuf
    {
      get
      {
        return this.hasIgnoreGoogleProtobuf;
      }
    }

    public bool IgnoreGoogleProtobuf
    {
      get
      {
        return this.ignoreGoogleProtobuf_;
      }
    }

    public bool HasServiceGeneratorType
    {
      get
      {
        return this.hasServiceGeneratorType;
      }
    }

    public CSharpServiceType ServiceGeneratorType
    {
      get
      {
        return this.serviceGeneratorType_;
      }
    }

    public bool HasGeneratedCodeAttributes
    {
      get
      {
        return this.hasGeneratedCodeAttributes;
      }
    }

    public bool GeneratedCodeAttributes
    {
      get
      {
        return this.generatedCodeAttributes_;
      }
    }

    public override bool IsInitialized
    {
      get
      {
        return true;
      }
    }

    public override void WriteTo(ICodedOutputStream output)
    {
      int serializedSize = this.SerializedSize;
      string[] optionsFieldNames = CSharpFileOptions._cSharpFileOptionsFieldNames;
      if (this.hasNamespace)
        output.WriteString(1, optionsFieldNames[9], this.Namespace);
      if (this.hasUmbrellaClassname)
        output.WriteString(2, optionsFieldNames[14], this.UmbrellaClassname);
      if (this.hasPublicClasses)
        output.WriteBool(3, optionsFieldNames[12], this.PublicClasses);
      if (this.hasMultipleFiles)
        output.WriteBool(4, optionsFieldNames[8], this.MultipleFiles);
      if (this.hasNestClasses)
        output.WriteBool(5, optionsFieldNames[10], this.NestClasses);
      if (this.hasCodeContracts)
        output.WriteBool(6, optionsFieldNames[2], this.CodeContracts);
      if (this.hasExpandNamespaceDirectories)
        output.WriteBool(7, optionsFieldNames[3], this.ExpandNamespaceDirectories);
      if (this.hasClsCompliance)
        output.WriteBool(8, optionsFieldNames[1], this.ClsCompliance);
      if (this.hasAddSerializable)
        output.WriteBool(9, optionsFieldNames[0], this.AddSerializable);
      if (this.hasGeneratePrivateCtor)
        output.WriteBool(10, optionsFieldNames[5], this.GeneratePrivateCtor);
      if (this.hasFileExtension)
        output.WriteString(221, optionsFieldNames[4], this.FileExtension);
      if (this.hasUmbrellaNamespace)
        output.WriteString(222, optionsFieldNames[15], this.UmbrellaNamespace);
      if (this.hasOutputDirectory)
        output.WriteString(223, optionsFieldNames[11], this.OutputDirectory);
      if (this.hasIgnoreGoogleProtobuf)
        output.WriteBool(224, optionsFieldNames[7], this.IgnoreGoogleProtobuf);
      if (this.hasServiceGeneratorType)
        output.WriteEnum(225, optionsFieldNames[13], (int) this.ServiceGeneratorType, (object) this.ServiceGeneratorType);
      if (this.hasGeneratedCodeAttributes)
        output.WriteBool(226, optionsFieldNames[6], this.GeneratedCodeAttributes);
      this.UnknownFields.WriteTo(output);
    }

    public override int SerializedSize
    {
      get
      {
        int memoizedSerializedSize = this.memoizedSerializedSize;
        if (memoizedSerializedSize != -1)
          return memoizedSerializedSize;
        int num1 = 0;
        if (this.hasNamespace)
          num1 += CodedOutputStream.ComputeStringSize(1, this.Namespace);
        if (this.hasUmbrellaClassname)
          num1 += CodedOutputStream.ComputeStringSize(2, this.UmbrellaClassname);
        if (this.hasPublicClasses)
          num1 += CodedOutputStream.ComputeBoolSize(3, this.PublicClasses);
        if (this.hasMultipleFiles)
          num1 += CodedOutputStream.ComputeBoolSize(4, this.MultipleFiles);
        if (this.hasNestClasses)
          num1 += CodedOutputStream.ComputeBoolSize(5, this.NestClasses);
        if (this.hasCodeContracts)
          num1 += CodedOutputStream.ComputeBoolSize(6, this.CodeContracts);
        if (this.hasExpandNamespaceDirectories)
          num1 += CodedOutputStream.ComputeBoolSize(7, this.ExpandNamespaceDirectories);
        if (this.hasClsCompliance)
          num1 += CodedOutputStream.ComputeBoolSize(8, this.ClsCompliance);
        if (this.hasAddSerializable)
          num1 += CodedOutputStream.ComputeBoolSize(9, this.AddSerializable);
        if (this.hasGeneratePrivateCtor)
          num1 += CodedOutputStream.ComputeBoolSize(10, this.GeneratePrivateCtor);
        if (this.hasFileExtension)
          num1 += CodedOutputStream.ComputeStringSize(221, this.FileExtension);
        if (this.hasUmbrellaNamespace)
          num1 += CodedOutputStream.ComputeStringSize(222, this.UmbrellaNamespace);
        if (this.hasOutputDirectory)
          num1 += CodedOutputStream.ComputeStringSize(223, this.OutputDirectory);
        if (this.hasIgnoreGoogleProtobuf)
          num1 += CodedOutputStream.ComputeBoolSize(224, this.IgnoreGoogleProtobuf);
        if (this.hasServiceGeneratorType)
          num1 += CodedOutputStream.ComputeEnumSize(225, (int) this.ServiceGeneratorType);
        if (this.hasGeneratedCodeAttributes)
          num1 += CodedOutputStream.ComputeBoolSize(226, this.GeneratedCodeAttributes);
        int num2 = num1 + this.UnknownFields.SerializedSize;
        this.memoizedSerializedSize = num2;
        return num2;
      }
    }

    public static CSharpFileOptions ParseFrom(ByteString data)
    {
      return CSharpFileOptions.CreateBuilder().MergeFrom(data).BuildParsed();
    }

    public static CSharpFileOptions ParseFrom(ByteString data, ExtensionRegistry extensionRegistry)
    {
      return CSharpFileOptions.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
    }

    public static CSharpFileOptions ParseFrom(byte[] data)
    {
      return CSharpFileOptions.CreateBuilder().MergeFrom(data).BuildParsed();
    }

    public static CSharpFileOptions ParseFrom(byte[] data, ExtensionRegistry extensionRegistry)
    {
      return CSharpFileOptions.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
    }

    public static CSharpFileOptions ParseFrom(Stream input)
    {
      return CSharpFileOptions.CreateBuilder().MergeFrom(input).BuildParsed();
    }

    public static CSharpFileOptions ParseFrom(Stream input, ExtensionRegistry extensionRegistry)
    {
      return CSharpFileOptions.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
    }

    public static CSharpFileOptions ParseDelimitedFrom(Stream input)
    {
      return CSharpFileOptions.CreateBuilder().MergeDelimitedFrom(input).BuildParsed();
    }

    public static CSharpFileOptions ParseDelimitedFrom(Stream input, ExtensionRegistry extensionRegistry)
    {
      return CSharpFileOptions.CreateBuilder().MergeDelimitedFrom(input, extensionRegistry).BuildParsed();
    }

    public static CSharpFileOptions ParseFrom(ICodedInputStream input)
    {
      return CSharpFileOptions.CreateBuilder().MergeFrom(input).BuildParsed();
    }

    public static CSharpFileOptions ParseFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
    {
      return CSharpFileOptions.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
    }

    private CSharpFileOptions MakeReadOnly()
    {
      return this;
    }

    public static CSharpFileOptions.Builder CreateBuilder()
    {
      return new CSharpFileOptions.Builder();
    }

    public override CSharpFileOptions.Builder ToBuilder()
    {
      return CSharpFileOptions.CreateBuilder(this);
    }

    public override CSharpFileOptions.Builder CreateBuilderForType()
    {
      return new CSharpFileOptions.Builder();
    }

    public static CSharpFileOptions.Builder CreateBuilder(CSharpFileOptions prototype)
    {
      return new CSharpFileOptions.Builder(prototype);
    }

    static CSharpFileOptions()
    {
      object.ReferenceEquals((object) CSharpOptions.Descriptor, (object) null);
    }

    [DebuggerNonUserCode]
    public sealed class Builder : GeneratedBuilder<CSharpFileOptions, CSharpFileOptions.Builder>
    {
      private bool resultIsReadOnly;
      private CSharpFileOptions result;

      protected override CSharpFileOptions.Builder ThisBuilder
      {
        get
        {
          return this;
        }
      }

      public Builder()
      {
        this.result = CSharpFileOptions.DefaultInstance;
        this.resultIsReadOnly = true;
      }

      internal Builder(CSharpFileOptions cloneFrom)
      {
        this.result = cloneFrom;
        this.resultIsReadOnly = true;
      }

      private CSharpFileOptions PrepareBuilder()
      {
        if (this.resultIsReadOnly)
        {
          CSharpFileOptions result = this.result;
          this.result = new CSharpFileOptions();
          this.resultIsReadOnly = false;
          this.MergeFrom(result);
        }
        return this.result;
      }

      public override bool IsInitialized
      {
        get
        {
          return this.result.IsInitialized;
        }
      }

      protected override CSharpFileOptions MessageBeingBuilt
      {
        get
        {
          return this.PrepareBuilder();
        }
      }

      public override CSharpFileOptions.Builder Clear()
      {
        this.result = CSharpFileOptions.DefaultInstance;
        this.resultIsReadOnly = true;
        return this;
      }

      public override CSharpFileOptions.Builder Clone()
      {
        if (this.resultIsReadOnly)
          return new CSharpFileOptions.Builder(this.result);
        return new CSharpFileOptions.Builder().MergeFrom(this.result);
      }

      public override MessageDescriptor DescriptorForType
      {
        get
        {
          return CSharpFileOptions.Descriptor;
        }
      }

      public override CSharpFileOptions DefaultInstanceForType
      {
        get
        {
          return CSharpFileOptions.DefaultInstance;
        }
      }

      public override CSharpFileOptions BuildPartial()
      {
        if (this.resultIsReadOnly)
          return this.result;
        this.resultIsReadOnly = true;
        return this.result.MakeReadOnly();
      }

      public override CSharpFileOptions.Builder MergeFrom(IMessage other)
      {
        if (other is CSharpFileOptions)
          return this.MergeFrom((CSharpFileOptions) other);
        base.MergeFrom(other);
        return this;
      }

      public override CSharpFileOptions.Builder MergeFrom(CSharpFileOptions other)
      {
        if (other == CSharpFileOptions.DefaultInstance)
          return this;
        this.PrepareBuilder();
        if (other.HasNamespace)
          this.Namespace = other.Namespace;
        if (other.HasUmbrellaClassname)
          this.UmbrellaClassname = other.UmbrellaClassname;
        if (other.HasPublicClasses)
          this.PublicClasses = other.PublicClasses;
        if (other.HasMultipleFiles)
          this.MultipleFiles = other.MultipleFiles;
        if (other.HasNestClasses)
          this.NestClasses = other.NestClasses;
        if (other.HasCodeContracts)
          this.CodeContracts = other.CodeContracts;
        if (other.HasExpandNamespaceDirectories)
          this.ExpandNamespaceDirectories = other.ExpandNamespaceDirectories;
        if (other.HasClsCompliance)
          this.ClsCompliance = other.ClsCompliance;
        if (other.HasAddSerializable)
          this.AddSerializable = other.AddSerializable;
        if (other.HasGeneratePrivateCtor)
          this.GeneratePrivateCtor = other.GeneratePrivateCtor;
        if (other.HasFileExtension)
          this.FileExtension = other.FileExtension;
        if (other.HasUmbrellaNamespace)
          this.UmbrellaNamespace = other.UmbrellaNamespace;
        if (other.HasOutputDirectory)
          this.OutputDirectory = other.OutputDirectory;
        if (other.HasIgnoreGoogleProtobuf)
          this.IgnoreGoogleProtobuf = other.IgnoreGoogleProtobuf;
        if (other.HasServiceGeneratorType)
          this.ServiceGeneratorType = other.ServiceGeneratorType;
        if (other.HasGeneratedCodeAttributes)
          this.GeneratedCodeAttributes = other.GeneratedCodeAttributes;
        this.MergeUnknownFields(other.UnknownFields);
        return this;
      }

      public override CSharpFileOptions.Builder MergeFrom(ICodedInputStream input)
      {
        return this.MergeFrom(input, ExtensionRegistry.Empty);
      }

      public override CSharpFileOptions.Builder MergeFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
      {
        this.PrepareBuilder();
        UnknownFieldSet.Builder unknownFields = (UnknownFieldSet.Builder) null;
        uint fieldTag;
        string fieldName;
        while (input.ReadTag(out fieldTag, out fieldName))
        {
          if ((int) fieldTag == 0 && fieldName != null)
          {
            int index = Array.BinarySearch<string>(CSharpFileOptions._cSharpFileOptionsFieldNames, fieldName, (IComparer<string>) StringComparer.Ordinal);
            if (index >= 0)
            {
              fieldTag = CSharpFileOptions._cSharpFileOptionsFieldTags[index];
            }
            else
            {
              if (unknownFields == null)
                unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
              this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
              continue;
            }
          }
          switch (fieldTag)
          {
            case 0:
              throw InvalidProtocolBufferException.InvalidTag();
            case 10:
              this.result.hasNamespace = input.ReadString(ref this.result.namespace_);
              continue;
            case 18:
              this.result.hasUmbrellaClassname = input.ReadString(ref this.result.umbrellaClassname_);
              continue;
            case 24:
              this.result.hasPublicClasses = input.ReadBool(ref this.result.publicClasses_);
              continue;
            case 32:
              this.result.hasMultipleFiles = input.ReadBool(ref this.result.multipleFiles_);
              continue;
            case 40:
              this.result.hasNestClasses = input.ReadBool(ref this.result.nestClasses_);
              continue;
            case 48:
              this.result.hasCodeContracts = input.ReadBool(ref this.result.codeContracts_);
              continue;
            case 56:
              this.result.hasExpandNamespaceDirectories = input.ReadBool(ref this.result.expandNamespaceDirectories_);
              continue;
            case 64:
              this.result.hasClsCompliance = input.ReadBool(ref this.result.clsCompliance_);
              continue;
            case 72:
              this.result.hasAddSerializable = input.ReadBool(ref this.result.addSerializable_);
              continue;
            case 80:
              this.result.hasGeneratePrivateCtor = input.ReadBool(ref this.result.generatePrivateCtor_);
              continue;
            case 1770:
              this.result.hasFileExtension = input.ReadString(ref this.result.fileExtension_);
              continue;
            case 1778:
              this.result.hasUmbrellaNamespace = input.ReadString(ref this.result.umbrellaNamespace_);
              continue;
            case 1786:
              this.result.hasOutputDirectory = input.ReadString(ref this.result.outputDirectory_);
              continue;
            case 1792:
              this.result.hasIgnoreGoogleProtobuf = input.ReadBool(ref this.result.ignoreGoogleProtobuf_);
              continue;
            case 1800:
              object unknown;
              if (input.ReadEnum<CSharpServiceType>(ref this.result.serviceGeneratorType_, out unknown))
              {
                this.result.hasServiceGeneratorType = true;
                continue;
              }
              if (unknown is int)
              {
                if (unknownFields == null)
                  unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
                unknownFields.MergeVarintField(225, (ulong) (int) unknown);
                continue;
              }
              continue;
            case 1808:
              this.result.hasGeneratedCodeAttributes = input.ReadBool(ref this.result.generatedCodeAttributes_);
              continue;
            default:
              if (WireFormat.IsEndGroupTag(fieldTag))
              {
                if (unknownFields != null)
                  this.UnknownFields = unknownFields.Build();
                return this;
              }
              if (unknownFields == null)
                unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
              this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
              continue;
          }
        }
        if (unknownFields != null)
          this.UnknownFields = unknownFields.Build();
        return this;
      }

      public bool HasNamespace
      {
        get
        {
          return this.result.hasNamespace;
        }
      }

      public string Namespace
      {
        get
        {
          return this.result.Namespace;
        }
        set
        {
          this.SetNamespace(value);
        }
      }

      public CSharpFileOptions.Builder SetNamespace(string value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.hasNamespace = true;
        this.result.namespace_ = value;
        return this;
      }

      public CSharpFileOptions.Builder ClearNamespace()
      {
        this.PrepareBuilder();
        this.result.hasNamespace = false;
        this.result.namespace_ = "";
        return this;
      }

      public bool HasUmbrellaClassname
      {
        get
        {
          return this.result.hasUmbrellaClassname;
        }
      }

      public string UmbrellaClassname
      {
        get
        {
          return this.result.UmbrellaClassname;
        }
        set
        {
          this.SetUmbrellaClassname(value);
        }
      }

      public CSharpFileOptions.Builder SetUmbrellaClassname(string value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.hasUmbrellaClassname = true;
        this.result.umbrellaClassname_ = value;
        return this;
      }

      public CSharpFileOptions.Builder ClearUmbrellaClassname()
      {
        this.PrepareBuilder();
        this.result.hasUmbrellaClassname = false;
        this.result.umbrellaClassname_ = "";
        return this;
      }

      public bool HasPublicClasses
      {
        get
        {
          return this.result.hasPublicClasses;
        }
      }

      public bool PublicClasses
      {
        get
        {
          return this.result.PublicClasses;
        }
        set
        {
          this.SetPublicClasses(value);
        }
      }

      public CSharpFileOptions.Builder SetPublicClasses(bool value)
      {
        this.PrepareBuilder();
        this.result.hasPublicClasses = true;
        this.result.publicClasses_ = value;
        return this;
      }

      public CSharpFileOptions.Builder ClearPublicClasses()
      {
        this.PrepareBuilder();
        this.result.hasPublicClasses = false;
        this.result.publicClasses_ = true;
        return this;
      }

      public bool HasMultipleFiles
      {
        get
        {
          return this.result.hasMultipleFiles;
        }
      }

      public bool MultipleFiles
      {
        get
        {
          return this.result.MultipleFiles;
        }
        set
        {
          this.SetMultipleFiles(value);
        }
      }

      public CSharpFileOptions.Builder SetMultipleFiles(bool value)
      {
        this.PrepareBuilder();
        this.result.hasMultipleFiles = true;
        this.result.multipleFiles_ = value;
        return this;
      }

      public CSharpFileOptions.Builder ClearMultipleFiles()
      {
        this.PrepareBuilder();
        this.result.hasMultipleFiles = false;
        this.result.multipleFiles_ = false;
        return this;
      }

      public bool HasNestClasses
      {
        get
        {
          return this.result.hasNestClasses;
        }
      }

      public bool NestClasses
      {
        get
        {
          return this.result.NestClasses;
        }
        set
        {
          this.SetNestClasses(value);
        }
      }

      public CSharpFileOptions.Builder SetNestClasses(bool value)
      {
        this.PrepareBuilder();
        this.result.hasNestClasses = true;
        this.result.nestClasses_ = value;
        return this;
      }

      public CSharpFileOptions.Builder ClearNestClasses()
      {
        this.PrepareBuilder();
        this.result.hasNestClasses = false;
        this.result.nestClasses_ = false;
        return this;
      }

      public bool HasCodeContracts
      {
        get
        {
          return this.result.hasCodeContracts;
        }
      }

      public bool CodeContracts
      {
        get
        {
          return this.result.CodeContracts;
        }
        set
        {
          this.SetCodeContracts(value);
        }
      }

      public CSharpFileOptions.Builder SetCodeContracts(bool value)
      {
        this.PrepareBuilder();
        this.result.hasCodeContracts = true;
        this.result.codeContracts_ = value;
        return this;
      }

      public CSharpFileOptions.Builder ClearCodeContracts()
      {
        this.PrepareBuilder();
        this.result.hasCodeContracts = false;
        this.result.codeContracts_ = false;
        return this;
      }

      public bool HasExpandNamespaceDirectories
      {
        get
        {
          return this.result.hasExpandNamespaceDirectories;
        }
      }

      public bool ExpandNamespaceDirectories
      {
        get
        {
          return this.result.ExpandNamespaceDirectories;
        }
        set
        {
          this.SetExpandNamespaceDirectories(value);
        }
      }

      public CSharpFileOptions.Builder SetExpandNamespaceDirectories(bool value)
      {
        this.PrepareBuilder();
        this.result.hasExpandNamespaceDirectories = true;
        this.result.expandNamespaceDirectories_ = value;
        return this;
      }

      public CSharpFileOptions.Builder ClearExpandNamespaceDirectories()
      {
        this.PrepareBuilder();
        this.result.hasExpandNamespaceDirectories = false;
        this.result.expandNamespaceDirectories_ = false;
        return this;
      }

      public bool HasClsCompliance
      {
        get
        {
          return this.result.hasClsCompliance;
        }
      }

      public bool ClsCompliance
      {
        get
        {
          return this.result.ClsCompliance;
        }
        set
        {
          this.SetClsCompliance(value);
        }
      }

      public CSharpFileOptions.Builder SetClsCompliance(bool value)
      {
        this.PrepareBuilder();
        this.result.hasClsCompliance = true;
        this.result.clsCompliance_ = value;
        return this;
      }

      public CSharpFileOptions.Builder ClearClsCompliance()
      {
        this.PrepareBuilder();
        this.result.hasClsCompliance = false;
        this.result.clsCompliance_ = true;
        return this;
      }

      public bool HasAddSerializable
      {
        get
        {
          return this.result.hasAddSerializable;
        }
      }

      public bool AddSerializable
      {
        get
        {
          return this.result.AddSerializable;
        }
        set
        {
          this.SetAddSerializable(value);
        }
      }

      public CSharpFileOptions.Builder SetAddSerializable(bool value)
      {
        this.PrepareBuilder();
        this.result.hasAddSerializable = true;
        this.result.addSerializable_ = value;
        return this;
      }

      public CSharpFileOptions.Builder ClearAddSerializable()
      {
        this.PrepareBuilder();
        this.result.hasAddSerializable = false;
        this.result.addSerializable_ = false;
        return this;
      }

      public bool HasGeneratePrivateCtor
      {
        get
        {
          return this.result.hasGeneratePrivateCtor;
        }
      }

      public bool GeneratePrivateCtor
      {
        get
        {
          return this.result.GeneratePrivateCtor;
        }
        set
        {
          this.SetGeneratePrivateCtor(value);
        }
      }

      public CSharpFileOptions.Builder SetGeneratePrivateCtor(bool value)
      {
        this.PrepareBuilder();
        this.result.hasGeneratePrivateCtor = true;
        this.result.generatePrivateCtor_ = value;
        return this;
      }

      public CSharpFileOptions.Builder ClearGeneratePrivateCtor()
      {
        this.PrepareBuilder();
        this.result.hasGeneratePrivateCtor = false;
        this.result.generatePrivateCtor_ = true;
        return this;
      }

      public bool HasFileExtension
      {
        get
        {
          return this.result.hasFileExtension;
        }
      }

      public string FileExtension
      {
        get
        {
          return this.result.FileExtension;
        }
        set
        {
          this.SetFileExtension(value);
        }
      }

      public CSharpFileOptions.Builder SetFileExtension(string value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.hasFileExtension = true;
        this.result.fileExtension_ = value;
        return this;
      }

      public CSharpFileOptions.Builder ClearFileExtension()
      {
        this.PrepareBuilder();
        this.result.hasFileExtension = false;
        this.result.fileExtension_ = ".cs";
        return this;
      }

      public bool HasUmbrellaNamespace
      {
        get
        {
          return this.result.hasUmbrellaNamespace;
        }
      }

      public string UmbrellaNamespace
      {
        get
        {
          return this.result.UmbrellaNamespace;
        }
        set
        {
          this.SetUmbrellaNamespace(value);
        }
      }

      public CSharpFileOptions.Builder SetUmbrellaNamespace(string value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.hasUmbrellaNamespace = true;
        this.result.umbrellaNamespace_ = value;
        return this;
      }

      public CSharpFileOptions.Builder ClearUmbrellaNamespace()
      {
        this.PrepareBuilder();
        this.result.hasUmbrellaNamespace = false;
        this.result.umbrellaNamespace_ = "";
        return this;
      }

      public bool HasOutputDirectory
      {
        get
        {
          return this.result.hasOutputDirectory;
        }
      }

      public string OutputDirectory
      {
        get
        {
          return this.result.OutputDirectory;
        }
        set
        {
          this.SetOutputDirectory(value);
        }
      }

      public CSharpFileOptions.Builder SetOutputDirectory(string value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.hasOutputDirectory = true;
        this.result.outputDirectory_ = value;
        return this;
      }

      public CSharpFileOptions.Builder ClearOutputDirectory()
      {
        this.PrepareBuilder();
        this.result.hasOutputDirectory = false;
        this.result.outputDirectory_ = ".";
        return this;
      }

      public bool HasIgnoreGoogleProtobuf
      {
        get
        {
          return this.result.hasIgnoreGoogleProtobuf;
        }
      }

      public bool IgnoreGoogleProtobuf
      {
        get
        {
          return this.result.IgnoreGoogleProtobuf;
        }
        set
        {
          this.SetIgnoreGoogleProtobuf(value);
        }
      }

      public CSharpFileOptions.Builder SetIgnoreGoogleProtobuf(bool value)
      {
        this.PrepareBuilder();
        this.result.hasIgnoreGoogleProtobuf = true;
        this.result.ignoreGoogleProtobuf_ = value;
        return this;
      }

      public CSharpFileOptions.Builder ClearIgnoreGoogleProtobuf()
      {
        this.PrepareBuilder();
        this.result.hasIgnoreGoogleProtobuf = false;
        this.result.ignoreGoogleProtobuf_ = false;
        return this;
      }

      public bool HasServiceGeneratorType
      {
        get
        {
          return this.result.hasServiceGeneratorType;
        }
      }

      public CSharpServiceType ServiceGeneratorType
      {
        get
        {
          return this.result.ServiceGeneratorType;
        }
        set
        {
          this.SetServiceGeneratorType(value);
        }
      }

      public CSharpFileOptions.Builder SetServiceGeneratorType(CSharpServiceType value)
      {
        this.PrepareBuilder();
        this.result.hasServiceGeneratorType = true;
        this.result.serviceGeneratorType_ = value;
        return this;
      }

      public CSharpFileOptions.Builder ClearServiceGeneratorType()
      {
        this.PrepareBuilder();
        this.result.hasServiceGeneratorType = false;
        this.result.serviceGeneratorType_ = CSharpServiceType.NONE;
        return this;
      }

      public bool HasGeneratedCodeAttributes
      {
        get
        {
          return this.result.hasGeneratedCodeAttributes;
        }
      }

      public bool GeneratedCodeAttributes
      {
        get
        {
          return this.result.GeneratedCodeAttributes;
        }
        set
        {
          this.SetGeneratedCodeAttributes(value);
        }
      }

      public CSharpFileOptions.Builder SetGeneratedCodeAttributes(bool value)
      {
        this.PrepareBuilder();
        this.result.hasGeneratedCodeAttributes = true;
        this.result.generatedCodeAttributes_ = value;
        return this;
      }

      public CSharpFileOptions.Builder ClearGeneratedCodeAttributes()
      {
        this.PrepareBuilder();
        this.result.hasGeneratedCodeAttributes = false;
        this.result.generatedCodeAttributes_ = false;
        return this;
      }
    }
  }
}
