﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.DescriptorProtos.ServiceOptions
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers.Collections;
using Google.ProtocolBuffers.Descriptors;
using Google.ProtocolBuffers.FieldAccess;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace Google.ProtocolBuffers.DescriptorProtos
{
  [DebuggerNonUserCode]
  public sealed class ServiceOptions : ExtendableMessage<ServiceOptions, ServiceOptions.Builder>
  {
    private static readonly ServiceOptions defaultInstance = new ServiceOptions().MakeReadOnly();
    private static readonly string[] _serviceOptionsFieldNames = new string[1]
    {
      "uninterpreted_option"
    };
    private static readonly uint[] _serviceOptionsFieldTags = new uint[1]
    {
      7994U
    };
    private PopsicleList<UninterpretedOption> uninterpretedOption_ = new PopsicleList<UninterpretedOption>();
    private int memoizedSerializedSize = -1;
    public const int UninterpretedOptionFieldNumber = 999;

    private ServiceOptions()
    {
    }

    public static ServiceOptions DefaultInstance
    {
      get
      {
        return ServiceOptions.defaultInstance;
      }
    }

    public override ServiceOptions DefaultInstanceForType
    {
      get
      {
        return ServiceOptions.DefaultInstance;
      }
    }

    protected override ServiceOptions ThisMessage
    {
      get
      {
        return this;
      }
    }

    public static MessageDescriptor Descriptor
    {
      get
      {
        return DescriptorProtoFile.internal__static_google_protobuf_ServiceOptions__Descriptor;
      }
    }

    protected override FieldAccessorTable<ServiceOptions, ServiceOptions.Builder> InternalFieldAccessors
    {
      get
      {
        return DescriptorProtoFile.internal__static_google_protobuf_ServiceOptions__FieldAccessorTable;
      }
    }

    public IList<UninterpretedOption> UninterpretedOptionList
    {
      get
      {
        return (IList<UninterpretedOption>) this.uninterpretedOption_;
      }
    }

    public int UninterpretedOptionCount
    {
      get
      {
        return this.uninterpretedOption_.Count;
      }
    }

    public UninterpretedOption GetUninterpretedOption(int index)
    {
      return this.uninterpretedOption_[index];
    }

    public override bool IsInitialized
    {
      get
      {
        foreach (AbstractMessageLite<UninterpretedOption, UninterpretedOption.Builder> uninterpretedOption in (IEnumerable<UninterpretedOption>) this.UninterpretedOptionList)
        {
          if (!uninterpretedOption.IsInitialized)
            return false;
        }
        return this.ExtensionsAreInitialized;
      }
    }

    public override void WriteTo(ICodedOutputStream output)
    {
      int serializedSize = this.SerializedSize;
      string[] optionsFieldNames = ServiceOptions._serviceOptionsFieldNames;
      ExtendableMessage<ServiceOptions, ServiceOptions.Builder>.ExtensionWriter extensionWriter = this.CreateExtensionWriter((ExtendableMessage<ServiceOptions, ServiceOptions.Builder>) this);
      if (this.uninterpretedOption_.Count > 0)
        output.WriteMessageArray<UninterpretedOption>(999, optionsFieldNames[0], (IEnumerable<UninterpretedOption>) this.uninterpretedOption_);
      extensionWriter.WriteUntil(536870912, output);
      this.UnknownFields.WriteTo(output);
    }

    public override int SerializedSize
    {
      get
      {
        int memoizedSerializedSize = this.memoizedSerializedSize;
        if (memoizedSerializedSize != -1)
          return memoizedSerializedSize;
        int num1 = 0;
        foreach (UninterpretedOption uninterpretedOption in (IEnumerable<UninterpretedOption>) this.UninterpretedOptionList)
          num1 += CodedOutputStream.ComputeMessageSize(999, (IMessageLite) uninterpretedOption);
        int num2 = num1 + this.ExtensionsSerializedSize + this.UnknownFields.SerializedSize;
        this.memoizedSerializedSize = num2;
        return num2;
      }
    }

    public static ServiceOptions ParseFrom(ByteString data)
    {
      return ServiceOptions.CreateBuilder().MergeFrom(data).BuildParsed();
    }

    public static ServiceOptions ParseFrom(ByteString data, ExtensionRegistry extensionRegistry)
    {
      return ServiceOptions.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
    }

    public static ServiceOptions ParseFrom(byte[] data)
    {
      return ServiceOptions.CreateBuilder().MergeFrom(data).BuildParsed();
    }

    public static ServiceOptions ParseFrom(byte[] data, ExtensionRegistry extensionRegistry)
    {
      return ServiceOptions.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
    }

    public static ServiceOptions ParseFrom(Stream input)
    {
      return ServiceOptions.CreateBuilder().MergeFrom(input).BuildParsed();
    }

    public static ServiceOptions ParseFrom(Stream input, ExtensionRegistry extensionRegistry)
    {
      return ServiceOptions.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
    }

    public static ServiceOptions ParseDelimitedFrom(Stream input)
    {
      return ServiceOptions.CreateBuilder().MergeDelimitedFrom(input).BuildParsed();
    }

    public static ServiceOptions ParseDelimitedFrom(Stream input, ExtensionRegistry extensionRegistry)
    {
      return ServiceOptions.CreateBuilder().MergeDelimitedFrom(input, extensionRegistry).BuildParsed();
    }

    public static ServiceOptions ParseFrom(ICodedInputStream input)
    {
      return ServiceOptions.CreateBuilder().MergeFrom(input).BuildParsed();
    }

    public static ServiceOptions ParseFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
    {
      return ServiceOptions.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
    }

    private ServiceOptions MakeReadOnly()
    {
      this.uninterpretedOption_.MakeReadOnly();
      return this;
    }

    public static ServiceOptions.Builder CreateBuilder()
    {
      return new ServiceOptions.Builder();
    }

    public override ServiceOptions.Builder ToBuilder()
    {
      return ServiceOptions.CreateBuilder(this);
    }

    public override ServiceOptions.Builder CreateBuilderForType()
    {
      return new ServiceOptions.Builder();
    }

    public static ServiceOptions.Builder CreateBuilder(ServiceOptions prototype)
    {
      return new ServiceOptions.Builder(prototype);
    }

    static ServiceOptions()
    {
      object.ReferenceEquals((object) DescriptorProtoFile.Descriptor, (object) null);
    }

    [DebuggerNonUserCode]
    public sealed class Builder : ExtendableBuilder<ServiceOptions, ServiceOptions.Builder>
    {
      private bool resultIsReadOnly;
      private ServiceOptions result;

      protected override ServiceOptions.Builder ThisBuilder
      {
        get
        {
          return this;
        }
      }

      public Builder()
      {
        this.result = ServiceOptions.DefaultInstance;
        this.resultIsReadOnly = true;
      }

      internal Builder(ServiceOptions cloneFrom)
      {
        this.result = cloneFrom;
        this.resultIsReadOnly = true;
      }

      private ServiceOptions PrepareBuilder()
      {
        if (this.resultIsReadOnly)
        {
          ServiceOptions result = this.result;
          this.result = new ServiceOptions();
          this.resultIsReadOnly = false;
          this.MergeFrom(result);
        }
        return this.result;
      }

      public override bool IsInitialized
      {
        get
        {
          return this.result.IsInitialized;
        }
      }

      protected override ServiceOptions MessageBeingBuilt
      {
        get
        {
          return this.PrepareBuilder();
        }
      }

      public override ServiceOptions.Builder Clear()
      {
        this.result = ServiceOptions.DefaultInstance;
        this.resultIsReadOnly = true;
        return this;
      }

      public override ServiceOptions.Builder Clone()
      {
        if (this.resultIsReadOnly)
          return new ServiceOptions.Builder(this.result);
        return new ServiceOptions.Builder().MergeFrom(this.result);
      }

      public override MessageDescriptor DescriptorForType
      {
        get
        {
          return ServiceOptions.Descriptor;
        }
      }

      public override ServiceOptions DefaultInstanceForType
      {
        get
        {
          return ServiceOptions.DefaultInstance;
        }
      }

      public override ServiceOptions BuildPartial()
      {
        if (this.resultIsReadOnly)
          return this.result;
        this.resultIsReadOnly = true;
        return this.result.MakeReadOnly();
      }

      public override ServiceOptions.Builder MergeFrom(IMessage other)
      {
        if (other is ServiceOptions)
          return this.MergeFrom((ServiceOptions) other);
        base.MergeFrom(other);
        return this;
      }

      public override ServiceOptions.Builder MergeFrom(ServiceOptions other)
      {
        if (other == ServiceOptions.DefaultInstance)
          return this;
        this.PrepareBuilder();
        if (other.uninterpretedOption_.Count != 0)
          this.result.uninterpretedOption_.Add((IEnumerable<UninterpretedOption>) other.uninterpretedOption_);
        this.MergeExtensionFields((ExtendableMessage<ServiceOptions, ServiceOptions.Builder>) other);
        this.MergeUnknownFields(other.UnknownFields);
        return this;
      }

      public override ServiceOptions.Builder MergeFrom(ICodedInputStream input)
      {
        return this.MergeFrom(input, ExtensionRegistry.Empty);
      }

      public override ServiceOptions.Builder MergeFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
      {
        this.PrepareBuilder();
        UnknownFieldSet.Builder unknownFields = (UnknownFieldSet.Builder) null;
        uint fieldTag;
        string fieldName;
        while (input.ReadTag(out fieldTag, out fieldName))
        {
          if ((int) fieldTag == 0 && fieldName != null)
          {
            int index = Array.BinarySearch<string>(ServiceOptions._serviceOptionsFieldNames, fieldName, (IComparer<string>) StringComparer.Ordinal);
            if (index >= 0)
            {
              fieldTag = ServiceOptions._serviceOptionsFieldTags[index];
            }
            else
            {
              if (unknownFields == null)
                unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
              this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
              continue;
            }
          }
          switch (fieldTag)
          {
            case 0:
              throw InvalidProtocolBufferException.InvalidTag();
            case 7994:
              input.ReadMessageArray<UninterpretedOption>(fieldTag, fieldName, (ICollection<UninterpretedOption>) this.result.uninterpretedOption_, UninterpretedOption.DefaultInstance, extensionRegistry);
              continue;
            default:
              if (WireFormat.IsEndGroupTag(fieldTag))
              {
                if (unknownFields != null)
                  this.UnknownFields = unknownFields.Build();
                return this;
              }
              if (unknownFields == null)
                unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
              this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
              continue;
          }
        }
        if (unknownFields != null)
          this.UnknownFields = unknownFields.Build();
        return this;
      }

      public IPopsicleList<UninterpretedOption> UninterpretedOptionList
      {
        get
        {
          return (IPopsicleList<UninterpretedOption>) this.PrepareBuilder().uninterpretedOption_;
        }
      }

      public int UninterpretedOptionCount
      {
        get
        {
          return this.result.UninterpretedOptionCount;
        }
      }

      public UninterpretedOption GetUninterpretedOption(int index)
      {
        return this.result.GetUninterpretedOption(index);
      }

      public ServiceOptions.Builder SetUninterpretedOption(int index, UninterpretedOption value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.uninterpretedOption_[index] = value;
        return this;
      }

      public ServiceOptions.Builder SetUninterpretedOption(int index, UninterpretedOption.Builder builderForValue)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
        this.PrepareBuilder();
        this.result.uninterpretedOption_[index] = builderForValue.Build();
        return this;
      }

      public ServiceOptions.Builder AddUninterpretedOption(UninterpretedOption value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.uninterpretedOption_.Add(value);
        return this;
      }

      public ServiceOptions.Builder AddUninterpretedOption(UninterpretedOption.Builder builderForValue)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
        this.PrepareBuilder();
        this.result.uninterpretedOption_.Add(builderForValue.Build());
        return this;
      }

      public ServiceOptions.Builder AddRangeUninterpretedOption(IEnumerable<UninterpretedOption> values)
      {
        this.PrepareBuilder();
        this.result.uninterpretedOption_.Add(values);
        return this;
      }

      public ServiceOptions.Builder ClearUninterpretedOption()
      {
        this.PrepareBuilder();
        this.result.uninterpretedOption_.Clear();
        return this;
      }
    }
  }
}
