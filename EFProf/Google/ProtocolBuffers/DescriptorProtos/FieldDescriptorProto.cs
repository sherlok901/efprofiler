﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.DescriptorProtos.FieldDescriptorProto
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers.Descriptors;
using Google.ProtocolBuffers.FieldAccess;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace Google.ProtocolBuffers.DescriptorProtos
{
  [DebuggerNonUserCode]
  public sealed class FieldDescriptorProto : GeneratedMessage<FieldDescriptorProto, FieldDescriptorProto.Builder>, IDescriptorProto<FieldOptions>
  {
    private static readonly FieldDescriptorProto defaultInstance = new FieldDescriptorProto().MakeReadOnly();
    private static readonly string[] _fieldDescriptorProtoFieldNames = new string[8]
    {
      "default_value",
      "extendee",
      "label",
      "name",
      "number",
      "options",
      "type",
      "type_name"
    };
    private static readonly uint[] _fieldDescriptorProtoFieldTags = new uint[8]
    {
      58U,
      18U,
      32U,
      10U,
      24U,
      66U,
      40U,
      50U
    };
    private string name_ = "";
    private FieldDescriptorProto.Types.Label label_ = FieldDescriptorProto.Types.Label.LABEL_OPTIONAL;
    private FieldDescriptorProto.Types.Type type_ = FieldDescriptorProto.Types.Type.TYPE_DOUBLE;
    private string typeName_ = "";
    private string extendee_ = "";
    private string defaultValue_ = "";
    private int memoizedSerializedSize = -1;
    public const int NameFieldNumber = 1;
    public const int NumberFieldNumber = 3;
    public const int LabelFieldNumber = 4;
    public const int TypeFieldNumber = 5;
    public const int TypeNameFieldNumber = 6;
    public const int ExtendeeFieldNumber = 2;
    public const int DefaultValueFieldNumber = 7;
    public const int OptionsFieldNumber = 8;
    private bool hasName;
    private bool hasNumber;
    private int number_;
    private bool hasLabel;
    private bool hasType;
    private bool hasTypeName;
    private bool hasExtendee;
    private bool hasDefaultValue;
    private bool hasOptions;
    private FieldOptions options_;

    private FieldDescriptorProto()
    {
    }

    public static FieldDescriptorProto DefaultInstance
    {
      get
      {
        return FieldDescriptorProto.defaultInstance;
      }
    }

    public override FieldDescriptorProto DefaultInstanceForType
    {
      get
      {
        return FieldDescriptorProto.DefaultInstance;
      }
    }

    protected override FieldDescriptorProto ThisMessage
    {
      get
      {
        return this;
      }
    }

    public static MessageDescriptor Descriptor
    {
      get
      {
        return DescriptorProtoFile.internal__static_google_protobuf_FieldDescriptorProto__Descriptor;
      }
    }

    protected override FieldAccessorTable<FieldDescriptorProto, FieldDescriptorProto.Builder> InternalFieldAccessors
    {
      get
      {
        return DescriptorProtoFile.internal__static_google_protobuf_FieldDescriptorProto__FieldAccessorTable;
      }
    }

    public bool HasName
    {
      get
      {
        return this.hasName;
      }
    }

    public string Name
    {
      get
      {
        return this.name_;
      }
    }

    public bool HasNumber
    {
      get
      {
        return this.hasNumber;
      }
    }

    public int Number
    {
      get
      {
        return this.number_;
      }
    }

    public bool HasLabel
    {
      get
      {
        return this.hasLabel;
      }
    }

    public FieldDescriptorProto.Types.Label Label
    {
      get
      {
        return this.label_;
      }
    }

    public bool HasType
    {
      get
      {
        return this.hasType;
      }
    }

    public FieldDescriptorProto.Types.Type Type
    {
      get
      {
        return this.type_;
      }
    }

    public bool HasTypeName
    {
      get
      {
        return this.hasTypeName;
      }
    }

    public string TypeName
    {
      get
      {
        return this.typeName_;
      }
    }

    public bool HasExtendee
    {
      get
      {
        return this.hasExtendee;
      }
    }

    public string Extendee
    {
      get
      {
        return this.extendee_;
      }
    }

    public bool HasDefaultValue
    {
      get
      {
        return this.hasDefaultValue;
      }
    }

    public string DefaultValue
    {
      get
      {
        return this.defaultValue_;
      }
    }

    public bool HasOptions
    {
      get
      {
        return this.hasOptions;
      }
    }

    public FieldOptions Options
    {
      get
      {
        return this.options_ ?? FieldOptions.DefaultInstance;
      }
    }

    public override bool IsInitialized
    {
      get
      {
        return !this.HasOptions || this.Options.IsInitialized;
      }
    }

    public override void WriteTo(ICodedOutputStream output)
    {
      int serializedSize = this.SerializedSize;
      string[] descriptorProtoFieldNames = FieldDescriptorProto._fieldDescriptorProtoFieldNames;
      if (this.hasName)
        output.WriteString(1, descriptorProtoFieldNames[3], this.Name);
      if (this.hasExtendee)
        output.WriteString(2, descriptorProtoFieldNames[1], this.Extendee);
      if (this.hasNumber)
        output.WriteInt32(3, descriptorProtoFieldNames[4], this.Number);
      if (this.hasLabel)
        output.WriteEnum(4, descriptorProtoFieldNames[2], (int) this.Label, (object) this.Label);
      if (this.hasType)
        output.WriteEnum(5, descriptorProtoFieldNames[6], (int) this.Type, (object) this.Type);
      if (this.hasTypeName)
        output.WriteString(6, descriptorProtoFieldNames[7], this.TypeName);
      if (this.hasDefaultValue)
        output.WriteString(7, descriptorProtoFieldNames[0], this.DefaultValue);
      if (this.hasOptions)
        output.WriteMessage(8, descriptorProtoFieldNames[5], (IMessageLite) this.Options);
      this.UnknownFields.WriteTo(output);
    }

    public override int SerializedSize
    {
      get
      {
        int memoizedSerializedSize = this.memoizedSerializedSize;
        if (memoizedSerializedSize != -1)
          return memoizedSerializedSize;
        int num1 = 0;
        if (this.hasName)
          num1 += CodedOutputStream.ComputeStringSize(1, this.Name);
        if (this.hasNumber)
          num1 += CodedOutputStream.ComputeInt32Size(3, this.Number);
        if (this.hasLabel)
          num1 += CodedOutputStream.ComputeEnumSize(4, (int) this.Label);
        if (this.hasType)
          num1 += CodedOutputStream.ComputeEnumSize(5, (int) this.Type);
        if (this.hasTypeName)
          num1 += CodedOutputStream.ComputeStringSize(6, this.TypeName);
        if (this.hasExtendee)
          num1 += CodedOutputStream.ComputeStringSize(2, this.Extendee);
        if (this.hasDefaultValue)
          num1 += CodedOutputStream.ComputeStringSize(7, this.DefaultValue);
        if (this.hasOptions)
          num1 += CodedOutputStream.ComputeMessageSize(8, (IMessageLite) this.Options);
        int num2 = num1 + this.UnknownFields.SerializedSize;
        this.memoizedSerializedSize = num2;
        return num2;
      }
    }

    public static FieldDescriptorProto ParseFrom(ByteString data)
    {
      return FieldDescriptorProto.CreateBuilder().MergeFrom(data).BuildParsed();
    }

    public static FieldDescriptorProto ParseFrom(ByteString data, ExtensionRegistry extensionRegistry)
    {
      return FieldDescriptorProto.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
    }

    public static FieldDescriptorProto ParseFrom(byte[] data)
    {
      return FieldDescriptorProto.CreateBuilder().MergeFrom(data).BuildParsed();
    }

    public static FieldDescriptorProto ParseFrom(byte[] data, ExtensionRegistry extensionRegistry)
    {
      return FieldDescriptorProto.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
    }

    public static FieldDescriptorProto ParseFrom(Stream input)
    {
      return FieldDescriptorProto.CreateBuilder().MergeFrom(input).BuildParsed();
    }

    public static FieldDescriptorProto ParseFrom(Stream input, ExtensionRegistry extensionRegistry)
    {
      return FieldDescriptorProto.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
    }

    public static FieldDescriptorProto ParseDelimitedFrom(Stream input)
    {
      return FieldDescriptorProto.CreateBuilder().MergeDelimitedFrom(input).BuildParsed();
    }

    public static FieldDescriptorProto ParseDelimitedFrom(Stream input, ExtensionRegistry extensionRegistry)
    {
      return FieldDescriptorProto.CreateBuilder().MergeDelimitedFrom(input, extensionRegistry).BuildParsed();
    }

    public static FieldDescriptorProto ParseFrom(ICodedInputStream input)
    {
      return FieldDescriptorProto.CreateBuilder().MergeFrom(input).BuildParsed();
    }

    public static FieldDescriptorProto ParseFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
    {
      return FieldDescriptorProto.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
    }

    private FieldDescriptorProto MakeReadOnly()
    {
      return this;
    }

    public static FieldDescriptorProto.Builder CreateBuilder()
    {
      return new FieldDescriptorProto.Builder();
    }

    public override FieldDescriptorProto.Builder ToBuilder()
    {
      return FieldDescriptorProto.CreateBuilder(this);
    }

    public override FieldDescriptorProto.Builder CreateBuilderForType()
    {
      return new FieldDescriptorProto.Builder();
    }

    public static FieldDescriptorProto.Builder CreateBuilder(FieldDescriptorProto prototype)
    {
      return new FieldDescriptorProto.Builder(prototype);
    }

    static FieldDescriptorProto()
    {
      object.ReferenceEquals((object) DescriptorProtoFile.Descriptor, (object) null);
    }

    [DebuggerNonUserCode]
    public static class Types
    {
      public enum Type
      {
        TYPE_DOUBLE = 1,
        TYPE_FLOAT = 2,
        TYPE_INT64 = 3,
        TYPE_UINT64 = 4,
        TYPE_INT32 = 5,
        TYPE_FIXED64 = 6,
        TYPE_FIXED32 = 7,
        TYPE_BOOL = 8,
        TYPE_STRING = 9,
        TYPE_GROUP = 10, // 0x0000000A
        TYPE_MESSAGE = 11, // 0x0000000B
        TYPE_BYTES = 12, // 0x0000000C
        TYPE_UINT32 = 13, // 0x0000000D
        TYPE_ENUM = 14, // 0x0000000E
        TYPE_SFIXED32 = 15, // 0x0000000F
        TYPE_SFIXED64 = 16, // 0x00000010
        TYPE_SINT32 = 17, // 0x00000011
        TYPE_SINT64 = 18, // 0x00000012
      }

      public enum Label
      {
        LABEL_OPTIONAL = 1,
        LABEL_REQUIRED = 2,
        LABEL_REPEATED = 3,
      }
    }

    [DebuggerNonUserCode]
    public sealed class Builder : GeneratedBuilder<FieldDescriptorProto, FieldDescriptorProto.Builder>
    {
      private bool resultIsReadOnly;
      private FieldDescriptorProto result;

      protected override FieldDescriptorProto.Builder ThisBuilder
      {
        get
        {
          return this;
        }
      }

      public Builder()
      {
        this.result = FieldDescriptorProto.DefaultInstance;
        this.resultIsReadOnly = true;
      }

      internal Builder(FieldDescriptorProto cloneFrom)
      {
        this.result = cloneFrom;
        this.resultIsReadOnly = true;
      }

      private FieldDescriptorProto PrepareBuilder()
      {
        if (this.resultIsReadOnly)
        {
          FieldDescriptorProto result = this.result;
          this.result = new FieldDescriptorProto();
          this.resultIsReadOnly = false;
          this.MergeFrom(result);
        }
        return this.result;
      }

      public override bool IsInitialized
      {
        get
        {
          return this.result.IsInitialized;
        }
      }

      protected override FieldDescriptorProto MessageBeingBuilt
      {
        get
        {
          return this.PrepareBuilder();
        }
      }

      public override FieldDescriptorProto.Builder Clear()
      {
        this.result = FieldDescriptorProto.DefaultInstance;
        this.resultIsReadOnly = true;
        return this;
      }

      public override FieldDescriptorProto.Builder Clone()
      {
        if (this.resultIsReadOnly)
          return new FieldDescriptorProto.Builder(this.result);
        return new FieldDescriptorProto.Builder().MergeFrom(this.result);
      }

      public override MessageDescriptor DescriptorForType
      {
        get
        {
          return FieldDescriptorProto.Descriptor;
        }
      }

      public override FieldDescriptorProto DefaultInstanceForType
      {
        get
        {
          return FieldDescriptorProto.DefaultInstance;
        }
      }

      public override FieldDescriptorProto BuildPartial()
      {
        if (this.resultIsReadOnly)
          return this.result;
        this.resultIsReadOnly = true;
        return this.result.MakeReadOnly();
      }

      public override FieldDescriptorProto.Builder MergeFrom(IMessage other)
      {
        if (other is FieldDescriptorProto)
          return this.MergeFrom((FieldDescriptorProto) other);
        base.MergeFrom(other);
        return this;
      }

      public override FieldDescriptorProto.Builder MergeFrom(FieldDescriptorProto other)
      {
        if (other == FieldDescriptorProto.DefaultInstance)
          return this;
        this.PrepareBuilder();
        if (other.HasName)
          this.Name = other.Name;
        if (other.HasNumber)
          this.Number = other.Number;
        if (other.HasLabel)
          this.Label = other.Label;
        if (other.HasType)
          this.Type = other.Type;
        if (other.HasTypeName)
          this.TypeName = other.TypeName;
        if (other.HasExtendee)
          this.Extendee = other.Extendee;
        if (other.HasDefaultValue)
          this.DefaultValue = other.DefaultValue;
        if (other.HasOptions)
          this.MergeOptions(other.Options);
        this.MergeUnknownFields(other.UnknownFields);
        return this;
      }

      public override FieldDescriptorProto.Builder MergeFrom(ICodedInputStream input)
      {
        return this.MergeFrom(input, ExtensionRegistry.Empty);
      }

      public override FieldDescriptorProto.Builder MergeFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
      {
        this.PrepareBuilder();
        UnknownFieldSet.Builder unknownFields = (UnknownFieldSet.Builder) null;
        uint fieldTag;
        string fieldName;
        while (input.ReadTag(out fieldTag, out fieldName))
        {
          if ((int) fieldTag == 0 && fieldName != null)
          {
            int index = Array.BinarySearch<string>(FieldDescriptorProto._fieldDescriptorProtoFieldNames, fieldName, (IComparer<string>) StringComparer.Ordinal);
            if (index >= 0)
            {
              fieldTag = FieldDescriptorProto._fieldDescriptorProtoFieldTags[index];
            }
            else
            {
              if (unknownFields == null)
                unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
              this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
              continue;
            }
          }
          switch (fieldTag)
          {
            case 0:
              throw InvalidProtocolBufferException.InvalidTag();
            case 10:
              this.result.hasName = input.ReadString(ref this.result.name_);
              continue;
            case 18:
              this.result.hasExtendee = input.ReadString(ref this.result.extendee_);
              continue;
            case 24:
              this.result.hasNumber = input.ReadInt32(ref this.result.number_);
              continue;
            case 32:
              object unknown1;
              if (input.ReadEnum<FieldDescriptorProto.Types.Label>(ref this.result.label_, out unknown1))
              {
                this.result.hasLabel = true;
                continue;
              }
              if (unknown1 is int)
              {
                if (unknownFields == null)
                  unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
                unknownFields.MergeVarintField(4, (ulong) (int) unknown1);
                continue;
              }
              continue;
            case 40:
              object unknown2;
              if (input.ReadEnum<FieldDescriptorProto.Types.Type>(ref this.result.type_, out unknown2))
              {
                this.result.hasType = true;
                continue;
              }
              if (unknown2 is int)
              {
                if (unknownFields == null)
                  unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
                unknownFields.MergeVarintField(5, (ulong) (int) unknown2);
                continue;
              }
              continue;
            case 50:
              this.result.hasTypeName = input.ReadString(ref this.result.typeName_);
              continue;
            case 58:
              this.result.hasDefaultValue = input.ReadString(ref this.result.defaultValue_);
              continue;
            case 66:
              FieldOptions.Builder builder = FieldOptions.CreateBuilder();
              if (this.result.hasOptions)
                builder.MergeFrom(this.Options);
              input.ReadMessage((IBuilderLite) builder, extensionRegistry);
              this.Options = builder.BuildPartial();
              continue;
            default:
              if (WireFormat.IsEndGroupTag(fieldTag))
              {
                if (unknownFields != null)
                  this.UnknownFields = unknownFields.Build();
                return this;
              }
              if (unknownFields == null)
                unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
              this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
              continue;
          }
        }
        if (unknownFields != null)
          this.UnknownFields = unknownFields.Build();
        return this;
      }

      public bool HasName
      {
        get
        {
          return this.result.hasName;
        }
      }

      public string Name
      {
        get
        {
          return this.result.Name;
        }
        set
        {
          this.SetName(value);
        }
      }

      public FieldDescriptorProto.Builder SetName(string value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.hasName = true;
        this.result.name_ = value;
        return this;
      }

      public FieldDescriptorProto.Builder ClearName()
      {
        this.PrepareBuilder();
        this.result.hasName = false;
        this.result.name_ = "";
        return this;
      }

      public bool HasNumber
      {
        get
        {
          return this.result.hasNumber;
        }
      }

      public int Number
      {
        get
        {
          return this.result.Number;
        }
        set
        {
          this.SetNumber(value);
        }
      }

      public FieldDescriptorProto.Builder SetNumber(int value)
      {
        this.PrepareBuilder();
        this.result.hasNumber = true;
        this.result.number_ = value;
        return this;
      }

      public FieldDescriptorProto.Builder ClearNumber()
      {
        this.PrepareBuilder();
        this.result.hasNumber = false;
        this.result.number_ = 0;
        return this;
      }

      public bool HasLabel
      {
        get
        {
          return this.result.hasLabel;
        }
      }

      public FieldDescriptorProto.Types.Label Label
      {
        get
        {
          return this.result.Label;
        }
        set
        {
          this.SetLabel(value);
        }
      }

      public FieldDescriptorProto.Builder SetLabel(FieldDescriptorProto.Types.Label value)
      {
        this.PrepareBuilder();
        this.result.hasLabel = true;
        this.result.label_ = value;
        return this;
      }

      public FieldDescriptorProto.Builder ClearLabel()
      {
        this.PrepareBuilder();
        this.result.hasLabel = false;
        this.result.label_ = FieldDescriptorProto.Types.Label.LABEL_OPTIONAL;
        return this;
      }

      public bool HasType
      {
        get
        {
          return this.result.hasType;
        }
      }

      public FieldDescriptorProto.Types.Type Type
      {
        get
        {
          return this.result.Type;
        }
        set
        {
          this.SetType(value);
        }
      }

      public FieldDescriptorProto.Builder SetType(FieldDescriptorProto.Types.Type value)
      {
        this.PrepareBuilder();
        this.result.hasType = true;
        this.result.type_ = value;
        return this;
      }

      public FieldDescriptorProto.Builder ClearType()
      {
        this.PrepareBuilder();
        this.result.hasType = false;
        this.result.type_ = FieldDescriptorProto.Types.Type.TYPE_DOUBLE;
        return this;
      }

      public bool HasTypeName
      {
        get
        {
          return this.result.hasTypeName;
        }
      }

      public string TypeName
      {
        get
        {
          return this.result.TypeName;
        }
        set
        {
          this.SetTypeName(value);
        }
      }

      public FieldDescriptorProto.Builder SetTypeName(string value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.hasTypeName = true;
        this.result.typeName_ = value;
        return this;
      }

      public FieldDescriptorProto.Builder ClearTypeName()
      {
        this.PrepareBuilder();
        this.result.hasTypeName = false;
        this.result.typeName_ = "";
        return this;
      }

      public bool HasExtendee
      {
        get
        {
          return this.result.hasExtendee;
        }
      }

      public string Extendee
      {
        get
        {
          return this.result.Extendee;
        }
        set
        {
          this.SetExtendee(value);
        }
      }

      public FieldDescriptorProto.Builder SetExtendee(string value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.hasExtendee = true;
        this.result.extendee_ = value;
        return this;
      }

      public FieldDescriptorProto.Builder ClearExtendee()
      {
        this.PrepareBuilder();
        this.result.hasExtendee = false;
        this.result.extendee_ = "";
        return this;
      }

      public bool HasDefaultValue
      {
        get
        {
          return this.result.hasDefaultValue;
        }
      }

      public string DefaultValue
      {
        get
        {
          return this.result.DefaultValue;
        }
        set
        {
          this.SetDefaultValue(value);
        }
      }

      public FieldDescriptorProto.Builder SetDefaultValue(string value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.hasDefaultValue = true;
        this.result.defaultValue_ = value;
        return this;
      }

      public FieldDescriptorProto.Builder ClearDefaultValue()
      {
        this.PrepareBuilder();
        this.result.hasDefaultValue = false;
        this.result.defaultValue_ = "";
        return this;
      }

      public bool HasOptions
      {
        get
        {
          return this.result.hasOptions;
        }
      }

      public FieldOptions Options
      {
        get
        {
          return this.result.Options;
        }
        set
        {
          this.SetOptions(value);
        }
      }

      public FieldDescriptorProto.Builder SetOptions(FieldOptions value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.hasOptions = true;
        this.result.options_ = value;
        return this;
      }

      public FieldDescriptorProto.Builder SetOptions(FieldOptions.Builder builderForValue)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
        this.PrepareBuilder();
        this.result.hasOptions = true;
        this.result.options_ = builderForValue.Build();
        return this;
      }

      public FieldDescriptorProto.Builder MergeOptions(FieldOptions value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.options_ = !this.result.hasOptions || this.result.options_ == FieldOptions.DefaultInstance ? value : FieldOptions.CreateBuilder(this.result.options_).MergeFrom(value).BuildPartial();
        this.result.hasOptions = true;
        return this;
      }

      public FieldDescriptorProto.Builder ClearOptions()
      {
        this.PrepareBuilder();
        this.result.hasOptions = false;
        this.result.options_ = (FieldOptions) null;
        return this;
      }
    }
  }
}
