﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.DescriptorProtos.SourceCodeInfo
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers.Collections;
using Google.ProtocolBuffers.Descriptors;
using Google.ProtocolBuffers.FieldAccess;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace Google.ProtocolBuffers.DescriptorProtos
{
  [DebuggerNonUserCode]
  public sealed class SourceCodeInfo : GeneratedMessage<SourceCodeInfo, SourceCodeInfo.Builder>
  {
    private static readonly SourceCodeInfo defaultInstance = new SourceCodeInfo().MakeReadOnly();
    private static readonly string[] _sourceCodeInfoFieldNames = new string[1]
    {
      "location"
    };
    private static readonly uint[] _sourceCodeInfoFieldTags = new uint[1]
    {
      10U
    };
    private PopsicleList<SourceCodeInfo.Types.Location> location_ = new PopsicleList<SourceCodeInfo.Types.Location>();
    private int memoizedSerializedSize = -1;
    public const int LocationFieldNumber = 1;

    private SourceCodeInfo()
    {
    }

    public static SourceCodeInfo DefaultInstance
    {
      get
      {
        return SourceCodeInfo.defaultInstance;
      }
    }

    public override SourceCodeInfo DefaultInstanceForType
    {
      get
      {
        return SourceCodeInfo.DefaultInstance;
      }
    }

    protected override SourceCodeInfo ThisMessage
    {
      get
      {
        return this;
      }
    }

    public static MessageDescriptor Descriptor
    {
      get
      {
        return DescriptorProtoFile.internal__static_google_protobuf_SourceCodeInfo__Descriptor;
      }
    }

    protected override FieldAccessorTable<SourceCodeInfo, SourceCodeInfo.Builder> InternalFieldAccessors
    {
      get
      {
        return DescriptorProtoFile.internal__static_google_protobuf_SourceCodeInfo__FieldAccessorTable;
      }
    }

    public IList<SourceCodeInfo.Types.Location> LocationList
    {
      get
      {
        return (IList<SourceCodeInfo.Types.Location>) this.location_;
      }
    }

    public int LocationCount
    {
      get
      {
        return this.location_.Count;
      }
    }

    public SourceCodeInfo.Types.Location GetLocation(int index)
    {
      return this.location_[index];
    }

    public override bool IsInitialized
    {
      get
      {
        return true;
      }
    }

    public override void WriteTo(ICodedOutputStream output)
    {
      int serializedSize = this.SerializedSize;
      string[] codeInfoFieldNames = SourceCodeInfo._sourceCodeInfoFieldNames;
      if (this.location_.Count > 0)
        output.WriteMessageArray<SourceCodeInfo.Types.Location>(1, codeInfoFieldNames[0], (IEnumerable<SourceCodeInfo.Types.Location>) this.location_);
      this.UnknownFields.WriteTo(output);
    }

    public override int SerializedSize
    {
      get
      {
        int memoizedSerializedSize = this.memoizedSerializedSize;
        if (memoizedSerializedSize != -1)
          return memoizedSerializedSize;
        int num1 = 0;
        foreach (SourceCodeInfo.Types.Location location in (IEnumerable<SourceCodeInfo.Types.Location>) this.LocationList)
          num1 += CodedOutputStream.ComputeMessageSize(1, (IMessageLite) location);
        int num2 = num1 + this.UnknownFields.SerializedSize;
        this.memoizedSerializedSize = num2;
        return num2;
      }
    }

    public static SourceCodeInfo ParseFrom(ByteString data)
    {
      return SourceCodeInfo.CreateBuilder().MergeFrom(data).BuildParsed();
    }

    public static SourceCodeInfo ParseFrom(ByteString data, ExtensionRegistry extensionRegistry)
    {
      return SourceCodeInfo.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
    }

    public static SourceCodeInfo ParseFrom(byte[] data)
    {
      return SourceCodeInfo.CreateBuilder().MergeFrom(data).BuildParsed();
    }

    public static SourceCodeInfo ParseFrom(byte[] data, ExtensionRegistry extensionRegistry)
    {
      return SourceCodeInfo.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
    }

    public static SourceCodeInfo ParseFrom(Stream input)
    {
      return SourceCodeInfo.CreateBuilder().MergeFrom(input).BuildParsed();
    }

    public static SourceCodeInfo ParseFrom(Stream input, ExtensionRegistry extensionRegistry)
    {
      return SourceCodeInfo.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
    }

    public static SourceCodeInfo ParseDelimitedFrom(Stream input)
    {
      return SourceCodeInfo.CreateBuilder().MergeDelimitedFrom(input).BuildParsed();
    }

    public static SourceCodeInfo ParseDelimitedFrom(Stream input, ExtensionRegistry extensionRegistry)
    {
      return SourceCodeInfo.CreateBuilder().MergeDelimitedFrom(input, extensionRegistry).BuildParsed();
    }

    public static SourceCodeInfo ParseFrom(ICodedInputStream input)
    {
      return SourceCodeInfo.CreateBuilder().MergeFrom(input).BuildParsed();
    }

    public static SourceCodeInfo ParseFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
    {
      return SourceCodeInfo.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
    }

    private SourceCodeInfo MakeReadOnly()
    {
      this.location_.MakeReadOnly();
      return this;
    }

    public static SourceCodeInfo.Builder CreateBuilder()
    {
      return new SourceCodeInfo.Builder();
    }

    public override SourceCodeInfo.Builder ToBuilder()
    {
      return SourceCodeInfo.CreateBuilder(this);
    }

    public override SourceCodeInfo.Builder CreateBuilderForType()
    {
      return new SourceCodeInfo.Builder();
    }

    public static SourceCodeInfo.Builder CreateBuilder(SourceCodeInfo prototype)
    {
      return new SourceCodeInfo.Builder(prototype);
    }

    static SourceCodeInfo()
    {
      object.ReferenceEquals((object) DescriptorProtoFile.Descriptor, (object) null);
    }

    [DebuggerNonUserCode]
    public static class Types
    {
      [DebuggerNonUserCode]
      public sealed class Location : GeneratedMessage<SourceCodeInfo.Types.Location, SourceCodeInfo.Types.Location.Builder>
      {
        private static readonly SourceCodeInfo.Types.Location defaultInstance = new SourceCodeInfo.Types.Location().MakeReadOnly();
        private static readonly string[] _locationFieldNames = new string[2]
        {
          "path",
          "span"
        };
        private static readonly uint[] _locationFieldTags = new uint[2]
        {
          10U,
          18U
        };
        private PopsicleList<int> path_ = new PopsicleList<int>();
        private PopsicleList<int> span_ = new PopsicleList<int>();
        private int memoizedSerializedSize = -1;
        public const int PathFieldNumber = 1;
        public const int SpanFieldNumber = 2;
        private int pathMemoizedSerializedSize;
        private int spanMemoizedSerializedSize;

        private Location()
        {
        }

        public static SourceCodeInfo.Types.Location DefaultInstance
        {
          get
          {
            return SourceCodeInfo.Types.Location.defaultInstance;
          }
        }

        public override SourceCodeInfo.Types.Location DefaultInstanceForType
        {
          get
          {
            return SourceCodeInfo.Types.Location.DefaultInstance;
          }
        }

        protected override SourceCodeInfo.Types.Location ThisMessage
        {
          get
          {
            return this;
          }
        }

        public static MessageDescriptor Descriptor
        {
          get
          {
            return DescriptorProtoFile.internal__static_google_protobuf_SourceCodeInfo_Location__Descriptor;
          }
        }

        protected override FieldAccessorTable<SourceCodeInfo.Types.Location, SourceCodeInfo.Types.Location.Builder> InternalFieldAccessors
        {
          get
          {
            return DescriptorProtoFile.internal__static_google_protobuf_SourceCodeInfo_Location__FieldAccessorTable;
          }
        }

        public IList<int> PathList
        {
          get
          {
            return Lists.AsReadOnly<int>((IList<int>) this.path_);
          }
        }

        public int PathCount
        {
          get
          {
            return this.path_.Count;
          }
        }

        public int GetPath(int index)
        {
          return this.path_[index];
        }

        public IList<int> SpanList
        {
          get
          {
            return Lists.AsReadOnly<int>((IList<int>) this.span_);
          }
        }

        public int SpanCount
        {
          get
          {
            return this.span_.Count;
          }
        }

        public int GetSpan(int index)
        {
          return this.span_[index];
        }

        public override bool IsInitialized
        {
          get
          {
            return true;
          }
        }

        public override void WriteTo(ICodedOutputStream output)
        {
          int serializedSize = this.SerializedSize;
          string[] locationFieldNames = SourceCodeInfo.Types.Location._locationFieldNames;
          if (this.path_.Count > 0)
            output.WritePackedInt32Array(1, locationFieldNames[0], this.pathMemoizedSerializedSize, (IEnumerable<int>) this.path_);
          if (this.span_.Count > 0)
            output.WritePackedInt32Array(2, locationFieldNames[1], this.spanMemoizedSerializedSize, (IEnumerable<int>) this.span_);
          this.UnknownFields.WriteTo(output);
        }

        public override int SerializedSize
        {
          get
          {
            int memoizedSerializedSize = this.memoizedSerializedSize;
            if (memoizedSerializedSize != -1)
              return memoizedSerializedSize;
            int num1 = 0;
            int num2 = 0;
            foreach (int path in (IEnumerable<int>) this.PathList)
              num2 += CodedOutputStream.ComputeInt32SizeNoTag(path);
            int num3 = num1 + num2;
            if (this.path_.Count != 0)
              num3 += 1 + CodedOutputStream.ComputeInt32SizeNoTag(num2);
            this.pathMemoizedSerializedSize = num2;
            int num4 = 0;
            foreach (int span in (IEnumerable<int>) this.SpanList)
              num4 += CodedOutputStream.ComputeInt32SizeNoTag(span);
            int num5 = num3 + num4;
            if (this.span_.Count != 0)
              num5 += 1 + CodedOutputStream.ComputeInt32SizeNoTag(num4);
            this.spanMemoizedSerializedSize = num4;
            int num6 = num5 + this.UnknownFields.SerializedSize;
            this.memoizedSerializedSize = num6;
            return num6;
          }
        }

        public static SourceCodeInfo.Types.Location ParseFrom(ByteString data)
        {
          return SourceCodeInfo.Types.Location.CreateBuilder().MergeFrom(data).BuildParsed();
        }

        public static SourceCodeInfo.Types.Location ParseFrom(ByteString data, ExtensionRegistry extensionRegistry)
        {
          return SourceCodeInfo.Types.Location.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
        }

        public static SourceCodeInfo.Types.Location ParseFrom(byte[] data)
        {
          return SourceCodeInfo.Types.Location.CreateBuilder().MergeFrom(data).BuildParsed();
        }

        public static SourceCodeInfo.Types.Location ParseFrom(byte[] data, ExtensionRegistry extensionRegistry)
        {
          return SourceCodeInfo.Types.Location.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
        }

        public static SourceCodeInfo.Types.Location ParseFrom(Stream input)
        {
          return SourceCodeInfo.Types.Location.CreateBuilder().MergeFrom(input).BuildParsed();
        }

        public static SourceCodeInfo.Types.Location ParseFrom(Stream input, ExtensionRegistry extensionRegistry)
        {
          return SourceCodeInfo.Types.Location.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
        }

        public static SourceCodeInfo.Types.Location ParseDelimitedFrom(Stream input)
        {
          return SourceCodeInfo.Types.Location.CreateBuilder().MergeDelimitedFrom(input).BuildParsed();
        }

        public static SourceCodeInfo.Types.Location ParseDelimitedFrom(Stream input, ExtensionRegistry extensionRegistry)
        {
          return SourceCodeInfo.Types.Location.CreateBuilder().MergeDelimitedFrom(input, extensionRegistry).BuildParsed();
        }

        public static SourceCodeInfo.Types.Location ParseFrom(ICodedInputStream input)
        {
          return SourceCodeInfo.Types.Location.CreateBuilder().MergeFrom(input).BuildParsed();
        }

        public static SourceCodeInfo.Types.Location ParseFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
        {
          return SourceCodeInfo.Types.Location.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
        }

        private SourceCodeInfo.Types.Location MakeReadOnly()
        {
          this.path_.MakeReadOnly();
          this.span_.MakeReadOnly();
          return this;
        }

        public static SourceCodeInfo.Types.Location.Builder CreateBuilder()
        {
          return new SourceCodeInfo.Types.Location.Builder();
        }

        public override SourceCodeInfo.Types.Location.Builder ToBuilder()
        {
          return SourceCodeInfo.Types.Location.CreateBuilder(this);
        }

        public override SourceCodeInfo.Types.Location.Builder CreateBuilderForType()
        {
          return new SourceCodeInfo.Types.Location.Builder();
        }

        public static SourceCodeInfo.Types.Location.Builder CreateBuilder(SourceCodeInfo.Types.Location prototype)
        {
          return new SourceCodeInfo.Types.Location.Builder(prototype);
        }

        static Location()
        {
          object.ReferenceEquals((object) DescriptorProtoFile.Descriptor, (object) null);
        }

        [DebuggerNonUserCode]
        public sealed class Builder : GeneratedBuilder<SourceCodeInfo.Types.Location, SourceCodeInfo.Types.Location.Builder>
        {
          private bool resultIsReadOnly;
          private SourceCodeInfo.Types.Location result;

          protected override SourceCodeInfo.Types.Location.Builder ThisBuilder
          {
            get
            {
              return this;
            }
          }

          public Builder()
          {
            this.result = SourceCodeInfo.Types.Location.DefaultInstance;
            this.resultIsReadOnly = true;
          }

          internal Builder(SourceCodeInfo.Types.Location cloneFrom)
          {
            this.result = cloneFrom;
            this.resultIsReadOnly = true;
          }

          private SourceCodeInfo.Types.Location PrepareBuilder()
          {
            if (this.resultIsReadOnly)
            {
              SourceCodeInfo.Types.Location result = this.result;
              this.result = new SourceCodeInfo.Types.Location();
              this.resultIsReadOnly = false;
              this.MergeFrom(result);
            }
            return this.result;
          }

          public override bool IsInitialized
          {
            get
            {
              return this.result.IsInitialized;
            }
          }

          protected override SourceCodeInfo.Types.Location MessageBeingBuilt
          {
            get
            {
              return this.PrepareBuilder();
            }
          }

          public override SourceCodeInfo.Types.Location.Builder Clear()
          {
            this.result = SourceCodeInfo.Types.Location.DefaultInstance;
            this.resultIsReadOnly = true;
            return this;
          }

          public override SourceCodeInfo.Types.Location.Builder Clone()
          {
            if (this.resultIsReadOnly)
              return new SourceCodeInfo.Types.Location.Builder(this.result);
            return new SourceCodeInfo.Types.Location.Builder().MergeFrom(this.result);
          }

          public override MessageDescriptor DescriptorForType
          {
            get
            {
              return SourceCodeInfo.Types.Location.Descriptor;
            }
          }

          public override SourceCodeInfo.Types.Location DefaultInstanceForType
          {
            get
            {
              return SourceCodeInfo.Types.Location.DefaultInstance;
            }
          }

          public override SourceCodeInfo.Types.Location BuildPartial()
          {
            if (this.resultIsReadOnly)
              return this.result;
            this.resultIsReadOnly = true;
            return this.result.MakeReadOnly();
          }

          public override SourceCodeInfo.Types.Location.Builder MergeFrom(IMessage other)
          {
            if (other is SourceCodeInfo.Types.Location)
              return this.MergeFrom((SourceCodeInfo.Types.Location) other);
            base.MergeFrom(other);
            return this;
          }

          public override SourceCodeInfo.Types.Location.Builder MergeFrom(SourceCodeInfo.Types.Location other)
          {
            if (other == SourceCodeInfo.Types.Location.DefaultInstance)
              return this;
            this.PrepareBuilder();
            if (other.path_.Count != 0)
              this.result.path_.Add((IEnumerable<int>) other.path_);
            if (other.span_.Count != 0)
              this.result.span_.Add((IEnumerable<int>) other.span_);
            this.MergeUnknownFields(other.UnknownFields);
            return this;
          }

          public override SourceCodeInfo.Types.Location.Builder MergeFrom(ICodedInputStream input)
          {
            return this.MergeFrom(input, ExtensionRegistry.Empty);
          }

          public override SourceCodeInfo.Types.Location.Builder MergeFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
          {
            this.PrepareBuilder();
            UnknownFieldSet.Builder unknownFields = (UnknownFieldSet.Builder) null;
            uint fieldTag;
            string fieldName;
            while (input.ReadTag(out fieldTag, out fieldName))
            {
              if ((int) fieldTag == 0 && fieldName != null)
              {
                int index = Array.BinarySearch<string>(SourceCodeInfo.Types.Location._locationFieldNames, fieldName, (IComparer<string>) StringComparer.Ordinal);
                if (index >= 0)
                {
                  fieldTag = SourceCodeInfo.Types.Location._locationFieldTags[index];
                }
                else
                {
                  if (unknownFields == null)
                    unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
                  this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
                  continue;
                }
              }
              switch (fieldTag)
              {
                case 0:
                  throw InvalidProtocolBufferException.InvalidTag();
                case 8:
                case 10:
                  input.ReadInt32Array(fieldTag, fieldName, (ICollection<int>) this.result.path_);
                  continue;
                case 16:
                case 18:
                  input.ReadInt32Array(fieldTag, fieldName, (ICollection<int>) this.result.span_);
                  continue;
                default:
                  if (WireFormat.IsEndGroupTag(fieldTag))
                  {
                    if (unknownFields != null)
                      this.UnknownFields = unknownFields.Build();
                    return this;
                  }
                  if (unknownFields == null)
                    unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
                  this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
                  continue;
              }
            }
            if (unknownFields != null)
              this.UnknownFields = unknownFields.Build();
            return this;
          }

          public IPopsicleList<int> PathList
          {
            get
            {
              return (IPopsicleList<int>) this.PrepareBuilder().path_;
            }
          }

          public int PathCount
          {
            get
            {
              return this.result.PathCount;
            }
          }

          public int GetPath(int index)
          {
            return this.result.GetPath(index);
          }

          public SourceCodeInfo.Types.Location.Builder SetPath(int index, int value)
          {
            this.PrepareBuilder();
            this.result.path_[index] = value;
            return this;
          }

          public SourceCodeInfo.Types.Location.Builder AddPath(int value)
          {
            this.PrepareBuilder();
            this.result.path_.Add(value);
            return this;
          }

          public SourceCodeInfo.Types.Location.Builder AddRangePath(IEnumerable<int> values)
          {
            this.PrepareBuilder();
            this.result.path_.Add(values);
            return this;
          }

          public SourceCodeInfo.Types.Location.Builder ClearPath()
          {
            this.PrepareBuilder();
            this.result.path_.Clear();
            return this;
          }

          public IPopsicleList<int> SpanList
          {
            get
            {
              return (IPopsicleList<int>) this.PrepareBuilder().span_;
            }
          }

          public int SpanCount
          {
            get
            {
              return this.result.SpanCount;
            }
          }

          public int GetSpan(int index)
          {
            return this.result.GetSpan(index);
          }

          public SourceCodeInfo.Types.Location.Builder SetSpan(int index, int value)
          {
            this.PrepareBuilder();
            this.result.span_[index] = value;
            return this;
          }

          public SourceCodeInfo.Types.Location.Builder AddSpan(int value)
          {
            this.PrepareBuilder();
            this.result.span_.Add(value);
            return this;
          }

          public SourceCodeInfo.Types.Location.Builder AddRangeSpan(IEnumerable<int> values)
          {
            this.PrepareBuilder();
            this.result.span_.Add(values);
            return this;
          }

          public SourceCodeInfo.Types.Location.Builder ClearSpan()
          {
            this.PrepareBuilder();
            this.result.span_.Clear();
            return this;
          }
        }
      }
    }

    [DebuggerNonUserCode]
    public sealed class Builder : GeneratedBuilder<SourceCodeInfo, SourceCodeInfo.Builder>
    {
      private bool resultIsReadOnly;
      private SourceCodeInfo result;

      protected override SourceCodeInfo.Builder ThisBuilder
      {
        get
        {
          return this;
        }
      }

      public Builder()
      {
        this.result = SourceCodeInfo.DefaultInstance;
        this.resultIsReadOnly = true;
      }

      internal Builder(SourceCodeInfo cloneFrom)
      {
        this.result = cloneFrom;
        this.resultIsReadOnly = true;
      }

      private SourceCodeInfo PrepareBuilder()
      {
        if (this.resultIsReadOnly)
        {
          SourceCodeInfo result = this.result;
          this.result = new SourceCodeInfo();
          this.resultIsReadOnly = false;
          this.MergeFrom(result);
        }
        return this.result;
      }

      public override bool IsInitialized
      {
        get
        {
          return this.result.IsInitialized;
        }
      }

      protected override SourceCodeInfo MessageBeingBuilt
      {
        get
        {
          return this.PrepareBuilder();
        }
      }

      public override SourceCodeInfo.Builder Clear()
      {
        this.result = SourceCodeInfo.DefaultInstance;
        this.resultIsReadOnly = true;
        return this;
      }

      public override SourceCodeInfo.Builder Clone()
      {
        if (this.resultIsReadOnly)
          return new SourceCodeInfo.Builder(this.result);
        return new SourceCodeInfo.Builder().MergeFrom(this.result);
      }

      public override MessageDescriptor DescriptorForType
      {
        get
        {
          return SourceCodeInfo.Descriptor;
        }
      }

      public override SourceCodeInfo DefaultInstanceForType
      {
        get
        {
          return SourceCodeInfo.DefaultInstance;
        }
      }

      public override SourceCodeInfo BuildPartial()
      {
        if (this.resultIsReadOnly)
          return this.result;
        this.resultIsReadOnly = true;
        return this.result.MakeReadOnly();
      }

      public override SourceCodeInfo.Builder MergeFrom(IMessage other)
      {
        if (other is SourceCodeInfo)
          return this.MergeFrom((SourceCodeInfo) other);
        base.MergeFrom(other);
        return this;
      }

      public override SourceCodeInfo.Builder MergeFrom(SourceCodeInfo other)
      {
        if (other == SourceCodeInfo.DefaultInstance)
          return this;
        this.PrepareBuilder();
        if (other.location_.Count != 0)
          this.result.location_.Add((IEnumerable<SourceCodeInfo.Types.Location>) other.location_);
        this.MergeUnknownFields(other.UnknownFields);
        return this;
      }

      public override SourceCodeInfo.Builder MergeFrom(ICodedInputStream input)
      {
        return this.MergeFrom(input, ExtensionRegistry.Empty);
      }

      public override SourceCodeInfo.Builder MergeFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
      {
        this.PrepareBuilder();
        UnknownFieldSet.Builder unknownFields = (UnknownFieldSet.Builder) null;
        uint fieldTag;
        string fieldName;
        while (input.ReadTag(out fieldTag, out fieldName))
        {
          if ((int) fieldTag == 0 && fieldName != null)
          {
            int index = Array.BinarySearch<string>(SourceCodeInfo._sourceCodeInfoFieldNames, fieldName, (IComparer<string>) StringComparer.Ordinal);
            if (index >= 0)
            {
              fieldTag = SourceCodeInfo._sourceCodeInfoFieldTags[index];
            }
            else
            {
              if (unknownFields == null)
                unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
              this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
              continue;
            }
          }
          switch (fieldTag)
          {
            case 0:
              throw InvalidProtocolBufferException.InvalidTag();
            case 10:
              input.ReadMessageArray<SourceCodeInfo.Types.Location>(fieldTag, fieldName, (ICollection<SourceCodeInfo.Types.Location>) this.result.location_, SourceCodeInfo.Types.Location.DefaultInstance, extensionRegistry);
              continue;
            default:
              if (WireFormat.IsEndGroupTag(fieldTag))
              {
                if (unknownFields != null)
                  this.UnknownFields = unknownFields.Build();
                return this;
              }
              if (unknownFields == null)
                unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
              this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
              continue;
          }
        }
        if (unknownFields != null)
          this.UnknownFields = unknownFields.Build();
        return this;
      }

      public IPopsicleList<SourceCodeInfo.Types.Location> LocationList
      {
        get
        {
          return (IPopsicleList<SourceCodeInfo.Types.Location>) this.PrepareBuilder().location_;
        }
      }

      public int LocationCount
      {
        get
        {
          return this.result.LocationCount;
        }
      }

      public SourceCodeInfo.Types.Location GetLocation(int index)
      {
        return this.result.GetLocation(index);
      }

      public SourceCodeInfo.Builder SetLocation(int index, SourceCodeInfo.Types.Location value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.location_[index] = value;
        return this;
      }

      public SourceCodeInfo.Builder SetLocation(int index, SourceCodeInfo.Types.Location.Builder builderForValue)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
        this.PrepareBuilder();
        this.result.location_[index] = builderForValue.Build();
        return this;
      }

      public SourceCodeInfo.Builder AddLocation(SourceCodeInfo.Types.Location value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.location_.Add(value);
        return this;
      }

      public SourceCodeInfo.Builder AddLocation(SourceCodeInfo.Types.Location.Builder builderForValue)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
        this.PrepareBuilder();
        this.result.location_.Add(builderForValue.Build());
        return this;
      }

      public SourceCodeInfo.Builder AddRangeLocation(IEnumerable<SourceCodeInfo.Types.Location> values)
      {
        this.PrepareBuilder();
        this.result.location_.Add(values);
        return this;
      }

      public SourceCodeInfo.Builder ClearLocation()
      {
        this.PrepareBuilder();
        this.result.location_.Clear();
        return this;
      }
    }
  }
}
