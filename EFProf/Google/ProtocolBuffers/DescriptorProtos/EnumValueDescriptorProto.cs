﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.DescriptorProtos.EnumValueDescriptorProto
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers.Descriptors;
using Google.ProtocolBuffers.FieldAccess;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace Google.ProtocolBuffers.DescriptorProtos
{
  [DebuggerNonUserCode]
  public sealed class EnumValueDescriptorProto : GeneratedMessage<EnumValueDescriptorProto, EnumValueDescriptorProto.Builder>, IDescriptorProto<EnumValueOptions>
  {
    private static readonly EnumValueDescriptorProto defaultInstance = new EnumValueDescriptorProto().MakeReadOnly();
    private static readonly string[] _enumValueDescriptorProtoFieldNames = new string[3]
    {
      "name",
      "number",
      "options"
    };
    private static readonly uint[] _enumValueDescriptorProtoFieldTags = new uint[3]
    {
      10U,
      16U,
      26U
    };
    private string name_ = "";
    private int memoizedSerializedSize = -1;
    public const int NameFieldNumber = 1;
    public const int NumberFieldNumber = 2;
    public const int OptionsFieldNumber = 3;
    private bool hasName;
    private bool hasNumber;
    private int number_;
    private bool hasOptions;
    private EnumValueOptions options_;

    private EnumValueDescriptorProto()
    {
    }

    public static EnumValueDescriptorProto DefaultInstance
    {
      get
      {
        return EnumValueDescriptorProto.defaultInstance;
      }
    }

    public override EnumValueDescriptorProto DefaultInstanceForType
    {
      get
      {
        return EnumValueDescriptorProto.DefaultInstance;
      }
    }

    protected override EnumValueDescriptorProto ThisMessage
    {
      get
      {
        return this;
      }
    }

    public static MessageDescriptor Descriptor
    {
      get
      {
        return DescriptorProtoFile.internal__static_google_protobuf_EnumValueDescriptorProto__Descriptor;
      }
    }

    protected override FieldAccessorTable<EnumValueDescriptorProto, EnumValueDescriptorProto.Builder> InternalFieldAccessors
    {
      get
      {
        return DescriptorProtoFile.internal__static_google_protobuf_EnumValueDescriptorProto__FieldAccessorTable;
      }
    }

    public bool HasName
    {
      get
      {
        return this.hasName;
      }
    }

    public string Name
    {
      get
      {
        return this.name_;
      }
    }

    public bool HasNumber
    {
      get
      {
        return this.hasNumber;
      }
    }

    public int Number
    {
      get
      {
        return this.number_;
      }
    }

    public bool HasOptions
    {
      get
      {
        return this.hasOptions;
      }
    }

    public EnumValueOptions Options
    {
      get
      {
        return this.options_ ?? EnumValueOptions.DefaultInstance;
      }
    }

    public override bool IsInitialized
    {
      get
      {
        return !this.HasOptions || this.Options.IsInitialized;
      }
    }

    public override void WriteTo(ICodedOutputStream output)
    {
      int serializedSize = this.SerializedSize;
      string[] descriptorProtoFieldNames = EnumValueDescriptorProto._enumValueDescriptorProtoFieldNames;
      if (this.hasName)
        output.WriteString(1, descriptorProtoFieldNames[0], this.Name);
      if (this.hasNumber)
        output.WriteInt32(2, descriptorProtoFieldNames[1], this.Number);
      if (this.hasOptions)
        output.WriteMessage(3, descriptorProtoFieldNames[2], (IMessageLite) this.Options);
      this.UnknownFields.WriteTo(output);
    }

    public override int SerializedSize
    {
      get
      {
        int memoizedSerializedSize = this.memoizedSerializedSize;
        if (memoizedSerializedSize != -1)
          return memoizedSerializedSize;
        int num1 = 0;
        if (this.hasName)
          num1 += CodedOutputStream.ComputeStringSize(1, this.Name);
        if (this.hasNumber)
          num1 += CodedOutputStream.ComputeInt32Size(2, this.Number);
        if (this.hasOptions)
          num1 += CodedOutputStream.ComputeMessageSize(3, (IMessageLite) this.Options);
        int num2 = num1 + this.UnknownFields.SerializedSize;
        this.memoizedSerializedSize = num2;
        return num2;
      }
    }

    public static EnumValueDescriptorProto ParseFrom(ByteString data)
    {
      return EnumValueDescriptorProto.CreateBuilder().MergeFrom(data).BuildParsed();
    }

    public static EnumValueDescriptorProto ParseFrom(ByteString data, ExtensionRegistry extensionRegistry)
    {
      return EnumValueDescriptorProto.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
    }

    public static EnumValueDescriptorProto ParseFrom(byte[] data)
    {
      return EnumValueDescriptorProto.CreateBuilder().MergeFrom(data).BuildParsed();
    }

    public static EnumValueDescriptorProto ParseFrom(byte[] data, ExtensionRegistry extensionRegistry)
    {
      return EnumValueDescriptorProto.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
    }

    public static EnumValueDescriptorProto ParseFrom(Stream input)
    {
      return EnumValueDescriptorProto.CreateBuilder().MergeFrom(input).BuildParsed();
    }

    public static EnumValueDescriptorProto ParseFrom(Stream input, ExtensionRegistry extensionRegistry)
    {
      return EnumValueDescriptorProto.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
    }

    public static EnumValueDescriptorProto ParseDelimitedFrom(Stream input)
    {
      return EnumValueDescriptorProto.CreateBuilder().MergeDelimitedFrom(input).BuildParsed();
    }

    public static EnumValueDescriptorProto ParseDelimitedFrom(Stream input, ExtensionRegistry extensionRegistry)
    {
      return EnumValueDescriptorProto.CreateBuilder().MergeDelimitedFrom(input, extensionRegistry).BuildParsed();
    }

    public static EnumValueDescriptorProto ParseFrom(ICodedInputStream input)
    {
      return EnumValueDescriptorProto.CreateBuilder().MergeFrom(input).BuildParsed();
    }

    public static EnumValueDescriptorProto ParseFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
    {
      return EnumValueDescriptorProto.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
    }

    private EnumValueDescriptorProto MakeReadOnly()
    {
      return this;
    }

    public static EnumValueDescriptorProto.Builder CreateBuilder()
    {
      return new EnumValueDescriptorProto.Builder();
    }

    public override EnumValueDescriptorProto.Builder ToBuilder()
    {
      return EnumValueDescriptorProto.CreateBuilder(this);
    }

    public override EnumValueDescriptorProto.Builder CreateBuilderForType()
    {
      return new EnumValueDescriptorProto.Builder();
    }

    public static EnumValueDescriptorProto.Builder CreateBuilder(EnumValueDescriptorProto prototype)
    {
      return new EnumValueDescriptorProto.Builder(prototype);
    }

    static EnumValueDescriptorProto()
    {
      object.ReferenceEquals((object) DescriptorProtoFile.Descriptor, (object) null);
    }

    [DebuggerNonUserCode]
    public sealed class Builder : GeneratedBuilder<EnumValueDescriptorProto, EnumValueDescriptorProto.Builder>
    {
      private bool resultIsReadOnly;
      private EnumValueDescriptorProto result;

      protected override EnumValueDescriptorProto.Builder ThisBuilder
      {
        get
        {
          return this;
        }
      }

      public Builder()
      {
        this.result = EnumValueDescriptorProto.DefaultInstance;
        this.resultIsReadOnly = true;
      }

      internal Builder(EnumValueDescriptorProto cloneFrom)
      {
        this.result = cloneFrom;
        this.resultIsReadOnly = true;
      }

      private EnumValueDescriptorProto PrepareBuilder()
      {
        if (this.resultIsReadOnly)
        {
          EnumValueDescriptorProto result = this.result;
          this.result = new EnumValueDescriptorProto();
          this.resultIsReadOnly = false;
          this.MergeFrom(result);
        }
        return this.result;
      }

      public override bool IsInitialized
      {
        get
        {
          return this.result.IsInitialized;
        }
      }

      protected override EnumValueDescriptorProto MessageBeingBuilt
      {
        get
        {
          return this.PrepareBuilder();
        }
      }

      public override EnumValueDescriptorProto.Builder Clear()
      {
        this.result = EnumValueDescriptorProto.DefaultInstance;
        this.resultIsReadOnly = true;
        return this;
      }

      public override EnumValueDescriptorProto.Builder Clone()
      {
        if (this.resultIsReadOnly)
          return new EnumValueDescriptorProto.Builder(this.result);
        return new EnumValueDescriptorProto.Builder().MergeFrom(this.result);
      }

      public override MessageDescriptor DescriptorForType
      {
        get
        {
          return EnumValueDescriptorProto.Descriptor;
        }
      }

      public override EnumValueDescriptorProto DefaultInstanceForType
      {
        get
        {
          return EnumValueDescriptorProto.DefaultInstance;
        }
      }

      public override EnumValueDescriptorProto BuildPartial()
      {
        if (this.resultIsReadOnly)
          return this.result;
        this.resultIsReadOnly = true;
        return this.result.MakeReadOnly();
      }

      public override EnumValueDescriptorProto.Builder MergeFrom(IMessage other)
      {
        if (other is EnumValueDescriptorProto)
          return this.MergeFrom((EnumValueDescriptorProto) other);
        base.MergeFrom(other);
        return this;
      }

      public override EnumValueDescriptorProto.Builder MergeFrom(EnumValueDescriptorProto other)
      {
        if (other == EnumValueDescriptorProto.DefaultInstance)
          return this;
        this.PrepareBuilder();
        if (other.HasName)
          this.Name = other.Name;
        if (other.HasNumber)
          this.Number = other.Number;
        if (other.HasOptions)
          this.MergeOptions(other.Options);
        this.MergeUnknownFields(other.UnknownFields);
        return this;
      }

      public override EnumValueDescriptorProto.Builder MergeFrom(ICodedInputStream input)
      {
        return this.MergeFrom(input, ExtensionRegistry.Empty);
      }

      public override EnumValueDescriptorProto.Builder MergeFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
      {
        this.PrepareBuilder();
        UnknownFieldSet.Builder unknownFields = (UnknownFieldSet.Builder) null;
        uint fieldTag;
        string fieldName;
        while (input.ReadTag(out fieldTag, out fieldName))
        {
          if ((int) fieldTag == 0 && fieldName != null)
          {
            int index = Array.BinarySearch<string>(EnumValueDescriptorProto._enumValueDescriptorProtoFieldNames, fieldName, (IComparer<string>) StringComparer.Ordinal);
            if (index >= 0)
            {
              fieldTag = EnumValueDescriptorProto._enumValueDescriptorProtoFieldTags[index];
            }
            else
            {
              if (unknownFields == null)
                unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
              this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
              continue;
            }
          }
          switch (fieldTag)
          {
            case 0:
              throw InvalidProtocolBufferException.InvalidTag();
            case 10:
              this.result.hasName = input.ReadString(ref this.result.name_);
              continue;
            case 16:
              this.result.hasNumber = input.ReadInt32(ref this.result.number_);
              continue;
            case 26:
              EnumValueOptions.Builder builder = EnumValueOptions.CreateBuilder();
              if (this.result.hasOptions)
                builder.MergeFrom(this.Options);
              input.ReadMessage((IBuilderLite) builder, extensionRegistry);
              this.Options = builder.BuildPartial();
              continue;
            default:
              if (WireFormat.IsEndGroupTag(fieldTag))
              {
                if (unknownFields != null)
                  this.UnknownFields = unknownFields.Build();
                return this;
              }
              if (unknownFields == null)
                unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
              this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
              continue;
          }
        }
        if (unknownFields != null)
          this.UnknownFields = unknownFields.Build();
        return this;
      }

      public bool HasName
      {
        get
        {
          return this.result.hasName;
        }
      }

      public string Name
      {
        get
        {
          return this.result.Name;
        }
        set
        {
          this.SetName(value);
        }
      }

      public EnumValueDescriptorProto.Builder SetName(string value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.hasName = true;
        this.result.name_ = value;
        return this;
      }

      public EnumValueDescriptorProto.Builder ClearName()
      {
        this.PrepareBuilder();
        this.result.hasName = false;
        this.result.name_ = "";
        return this;
      }

      public bool HasNumber
      {
        get
        {
          return this.result.hasNumber;
        }
      }

      public int Number
      {
        get
        {
          return this.result.Number;
        }
        set
        {
          this.SetNumber(value);
        }
      }

      public EnumValueDescriptorProto.Builder SetNumber(int value)
      {
        this.PrepareBuilder();
        this.result.hasNumber = true;
        this.result.number_ = value;
        return this;
      }

      public EnumValueDescriptorProto.Builder ClearNumber()
      {
        this.PrepareBuilder();
        this.result.hasNumber = false;
        this.result.number_ = 0;
        return this;
      }

      public bool HasOptions
      {
        get
        {
          return this.result.hasOptions;
        }
      }

      public EnumValueOptions Options
      {
        get
        {
          return this.result.Options;
        }
        set
        {
          this.SetOptions(value);
        }
      }

      public EnumValueDescriptorProto.Builder SetOptions(EnumValueOptions value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.hasOptions = true;
        this.result.options_ = value;
        return this;
      }

      public EnumValueDescriptorProto.Builder SetOptions(EnumValueOptions.Builder builderForValue)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
        this.PrepareBuilder();
        this.result.hasOptions = true;
        this.result.options_ = builderForValue.Build();
        return this;
      }

      public EnumValueDescriptorProto.Builder MergeOptions(EnumValueOptions value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.options_ = !this.result.hasOptions || this.result.options_ == EnumValueOptions.DefaultInstance ? value : EnumValueOptions.CreateBuilder(this.result.options_).MergeFrom(value).BuildPartial();
        this.result.hasOptions = true;
        return this;
      }

      public EnumValueDescriptorProto.Builder ClearOptions()
      {
        this.PrepareBuilder();
        this.result.hasOptions = false;
        this.result.options_ = (EnumValueOptions) null;
        return this;
      }
    }
  }
}
