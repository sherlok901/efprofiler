﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.DescriptorProtos.FileDescriptorSet
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers.Collections;
using Google.ProtocolBuffers.Descriptors;
using Google.ProtocolBuffers.FieldAccess;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace Google.ProtocolBuffers.DescriptorProtos
{
  [DebuggerNonUserCode]
  public sealed class FileDescriptorSet : GeneratedMessage<FileDescriptorSet, FileDescriptorSet.Builder>
  {
    private static readonly FileDescriptorSet defaultInstance = new FileDescriptorSet().MakeReadOnly();
    private static readonly string[] _fileDescriptorSetFieldNames = new string[1]
    {
      "file"
    };
    private static readonly uint[] _fileDescriptorSetFieldTags = new uint[1]
    {
      10U
    };
    private PopsicleList<FileDescriptorProto> file_ = new PopsicleList<FileDescriptorProto>();
    private int memoizedSerializedSize = -1;
    public const int FileFieldNumber = 1;

    private FileDescriptorSet()
    {
    }

    public static FileDescriptorSet DefaultInstance
    {
      get
      {
        return FileDescriptorSet.defaultInstance;
      }
    }

    public override FileDescriptorSet DefaultInstanceForType
    {
      get
      {
        return FileDescriptorSet.DefaultInstance;
      }
    }

    protected override FileDescriptorSet ThisMessage
    {
      get
      {
        return this;
      }
    }

    public static MessageDescriptor Descriptor
    {
      get
      {
        return DescriptorProtoFile.internal__static_google_protobuf_FileDescriptorSet__Descriptor;
      }
    }

    protected override FieldAccessorTable<FileDescriptorSet, FileDescriptorSet.Builder> InternalFieldAccessors
    {
      get
      {
        return DescriptorProtoFile.internal__static_google_protobuf_FileDescriptorSet__FieldAccessorTable;
      }
    }

    public IList<FileDescriptorProto> FileList
    {
      get
      {
        return (IList<FileDescriptorProto>) this.file_;
      }
    }

    public int FileCount
    {
      get
      {
        return this.file_.Count;
      }
    }

    public FileDescriptorProto GetFile(int index)
    {
      return this.file_[index];
    }

    public override bool IsInitialized
    {
      get
      {
        foreach (AbstractMessageLite<FileDescriptorProto, FileDescriptorProto.Builder> file in (IEnumerable<FileDescriptorProto>) this.FileList)
        {
          if (!file.IsInitialized)
            return false;
        }
        return true;
      }
    }

    public override void WriteTo(ICodedOutputStream output)
    {
      int serializedSize = this.SerializedSize;
      string[] descriptorSetFieldNames = FileDescriptorSet._fileDescriptorSetFieldNames;
      if (this.file_.Count > 0)
        output.WriteMessageArray<FileDescriptorProto>(1, descriptorSetFieldNames[0], (IEnumerable<FileDescriptorProto>) this.file_);
      this.UnknownFields.WriteTo(output);
    }

    public override int SerializedSize
    {
      get
      {
        int memoizedSerializedSize = this.memoizedSerializedSize;
        if (memoizedSerializedSize != -1)
          return memoizedSerializedSize;
        int num1 = 0;
        foreach (FileDescriptorProto file in (IEnumerable<FileDescriptorProto>) this.FileList)
          num1 += CodedOutputStream.ComputeMessageSize(1, (IMessageLite) file);
        int num2 = num1 + this.UnknownFields.SerializedSize;
        this.memoizedSerializedSize = num2;
        return num2;
      }
    }

    public static FileDescriptorSet ParseFrom(ByteString data)
    {
      return FileDescriptorSet.CreateBuilder().MergeFrom(data).BuildParsed();
    }

    public static FileDescriptorSet ParseFrom(ByteString data, ExtensionRegistry extensionRegistry)
    {
      return FileDescriptorSet.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
    }

    public static FileDescriptorSet ParseFrom(byte[] data)
    {
      return FileDescriptorSet.CreateBuilder().MergeFrom(data).BuildParsed();
    }

    public static FileDescriptorSet ParseFrom(byte[] data, ExtensionRegistry extensionRegistry)
    {
      return FileDescriptorSet.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
    }

    public static FileDescriptorSet ParseFrom(Stream input)
    {
      return FileDescriptorSet.CreateBuilder().MergeFrom(input).BuildParsed();
    }

    public static FileDescriptorSet ParseFrom(Stream input, ExtensionRegistry extensionRegistry)
    {
      return FileDescriptorSet.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
    }

    public static FileDescriptorSet ParseDelimitedFrom(Stream input)
    {
      return FileDescriptorSet.CreateBuilder().MergeDelimitedFrom(input).BuildParsed();
    }

    public static FileDescriptorSet ParseDelimitedFrom(Stream input, ExtensionRegistry extensionRegistry)
    {
      return FileDescriptorSet.CreateBuilder().MergeDelimitedFrom(input, extensionRegistry).BuildParsed();
    }

    public static FileDescriptorSet ParseFrom(ICodedInputStream input)
    {
      return FileDescriptorSet.CreateBuilder().MergeFrom(input).BuildParsed();
    }

    public static FileDescriptorSet ParseFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
    {
      return FileDescriptorSet.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
    }

    private FileDescriptorSet MakeReadOnly()
    {
      this.file_.MakeReadOnly();
      return this;
    }

    public static FileDescriptorSet.Builder CreateBuilder()
    {
      return new FileDescriptorSet.Builder();
    }

    public override FileDescriptorSet.Builder ToBuilder()
    {
      return FileDescriptorSet.CreateBuilder(this);
    }

    public override FileDescriptorSet.Builder CreateBuilderForType()
    {
      return new FileDescriptorSet.Builder();
    }

    public static FileDescriptorSet.Builder CreateBuilder(FileDescriptorSet prototype)
    {
      return new FileDescriptorSet.Builder(prototype);
    }

    static FileDescriptorSet()
    {
      object.ReferenceEquals((object) DescriptorProtoFile.Descriptor, (object) null);
    }

    [DebuggerNonUserCode]
    public sealed class Builder : GeneratedBuilder<FileDescriptorSet, FileDescriptorSet.Builder>
    {
      private bool resultIsReadOnly;
      private FileDescriptorSet result;

      protected override FileDescriptorSet.Builder ThisBuilder
      {
        get
        {
          return this;
        }
      }

      public Builder()
      {
        this.result = FileDescriptorSet.DefaultInstance;
        this.resultIsReadOnly = true;
      }

      internal Builder(FileDescriptorSet cloneFrom)
      {
        this.result = cloneFrom;
        this.resultIsReadOnly = true;
      }

      private FileDescriptorSet PrepareBuilder()
      {
        if (this.resultIsReadOnly)
        {
          FileDescriptorSet result = this.result;
          this.result = new FileDescriptorSet();
          this.resultIsReadOnly = false;
          this.MergeFrom(result);
        }
        return this.result;
      }

      public override bool IsInitialized
      {
        get
        {
          return this.result.IsInitialized;
        }
      }

      protected override FileDescriptorSet MessageBeingBuilt
      {
        get
        {
          return this.PrepareBuilder();
        }
      }

      public override FileDescriptorSet.Builder Clear()
      {
        this.result = FileDescriptorSet.DefaultInstance;
        this.resultIsReadOnly = true;
        return this;
      }

      public override FileDescriptorSet.Builder Clone()
      {
        if (this.resultIsReadOnly)
          return new FileDescriptorSet.Builder(this.result);
        return new FileDescriptorSet.Builder().MergeFrom(this.result);
      }

      public override MessageDescriptor DescriptorForType
      {
        get
        {
          return FileDescriptorSet.Descriptor;
        }
      }

      public override FileDescriptorSet DefaultInstanceForType
      {
        get
        {
          return FileDescriptorSet.DefaultInstance;
        }
      }

      public override FileDescriptorSet BuildPartial()
      {
        if (this.resultIsReadOnly)
          return this.result;
        this.resultIsReadOnly = true;
        return this.result.MakeReadOnly();
      }

      public override FileDescriptorSet.Builder MergeFrom(IMessage other)
      {
        if (other is FileDescriptorSet)
          return this.MergeFrom((FileDescriptorSet) other);
        base.MergeFrom(other);
        return this;
      }

      public override FileDescriptorSet.Builder MergeFrom(FileDescriptorSet other)
      {
        if (other == FileDescriptorSet.DefaultInstance)
          return this;
        this.PrepareBuilder();
        if (other.file_.Count != 0)
          this.result.file_.Add((IEnumerable<FileDescriptorProto>) other.file_);
        this.MergeUnknownFields(other.UnknownFields);
        return this;
      }

      public override FileDescriptorSet.Builder MergeFrom(ICodedInputStream input)
      {
        return this.MergeFrom(input, ExtensionRegistry.Empty);
      }

      public override FileDescriptorSet.Builder MergeFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
      {
        this.PrepareBuilder();
        UnknownFieldSet.Builder unknownFields = (UnknownFieldSet.Builder) null;
        uint fieldTag;
        string fieldName;
        while (input.ReadTag(out fieldTag, out fieldName))
        {
          if ((int) fieldTag == 0 && fieldName != null)
          {
            int index = Array.BinarySearch<string>(FileDescriptorSet._fileDescriptorSetFieldNames, fieldName, (IComparer<string>) StringComparer.Ordinal);
            if (index >= 0)
            {
              fieldTag = FileDescriptorSet._fileDescriptorSetFieldTags[index];
            }
            else
            {
              if (unknownFields == null)
                unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
              this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
              continue;
            }
          }
          switch (fieldTag)
          {
            case 0:
              throw InvalidProtocolBufferException.InvalidTag();
            case 10:
              input.ReadMessageArray<FileDescriptorProto>(fieldTag, fieldName, (ICollection<FileDescriptorProto>) this.result.file_, FileDescriptorProto.DefaultInstance, extensionRegistry);
              continue;
            default:
              if (WireFormat.IsEndGroupTag(fieldTag))
              {
                if (unknownFields != null)
                  this.UnknownFields = unknownFields.Build();
                return this;
              }
              if (unknownFields == null)
                unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
              this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
              continue;
          }
        }
        if (unknownFields != null)
          this.UnknownFields = unknownFields.Build();
        return this;
      }

      public IPopsicleList<FileDescriptorProto> FileList
      {
        get
        {
          return (IPopsicleList<FileDescriptorProto>) this.PrepareBuilder().file_;
        }
      }

      public int FileCount
      {
        get
        {
          return this.result.FileCount;
        }
      }

      public FileDescriptorProto GetFile(int index)
      {
        return this.result.GetFile(index);
      }

      public FileDescriptorSet.Builder SetFile(int index, FileDescriptorProto value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.file_[index] = value;
        return this;
      }

      public FileDescriptorSet.Builder SetFile(int index, FileDescriptorProto.Builder builderForValue)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
        this.PrepareBuilder();
        this.result.file_[index] = builderForValue.Build();
        return this;
      }

      public FileDescriptorSet.Builder AddFile(FileDescriptorProto value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.file_.Add(value);
        return this;
      }

      public FileDescriptorSet.Builder AddFile(FileDescriptorProto.Builder builderForValue)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
        this.PrepareBuilder();
        this.result.file_.Add(builderForValue.Build());
        return this;
      }

      public FileDescriptorSet.Builder AddRangeFile(IEnumerable<FileDescriptorProto> values)
      {
        this.PrepareBuilder();
        this.result.file_.Add(values);
        return this;
      }

      public FileDescriptorSet.Builder ClearFile()
      {
        this.PrepareBuilder();
        this.result.file_.Clear();
        return this;
      }
    }
  }
}
