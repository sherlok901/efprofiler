﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.DescriptorProtos.EnumDescriptorProto
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers.Collections;
using Google.ProtocolBuffers.Descriptors;
using Google.ProtocolBuffers.FieldAccess;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace Google.ProtocolBuffers.DescriptorProtos
{
  [DebuggerNonUserCode]
  public sealed class EnumDescriptorProto : GeneratedMessage<EnumDescriptorProto, EnumDescriptorProto.Builder>, IDescriptorProto<EnumOptions>
  {
    private static readonly EnumDescriptorProto defaultInstance = new EnumDescriptorProto().MakeReadOnly();
    private static readonly string[] _enumDescriptorProtoFieldNames = new string[3]
    {
      "name",
      "options",
      "value"
    };
    private static readonly uint[] _enumDescriptorProtoFieldTags = new uint[3]
    {
      10U,
      26U,
      18U
    };
    private string name_ = "";
    private PopsicleList<EnumValueDescriptorProto> value_ = new PopsicleList<EnumValueDescriptorProto>();
    private int memoizedSerializedSize = -1;
    public const int NameFieldNumber = 1;
    public const int ValueFieldNumber = 2;
    public const int OptionsFieldNumber = 3;
    private bool hasName;
    private bool hasOptions;
    private EnumOptions options_;

    private EnumDescriptorProto()
    {
    }

    public static EnumDescriptorProto DefaultInstance
    {
      get
      {
        return EnumDescriptorProto.defaultInstance;
      }
    }

    public override EnumDescriptorProto DefaultInstanceForType
    {
      get
      {
        return EnumDescriptorProto.DefaultInstance;
      }
    }

    protected override EnumDescriptorProto ThisMessage
    {
      get
      {
        return this;
      }
    }

    public static MessageDescriptor Descriptor
    {
      get
      {
        return DescriptorProtoFile.internal__static_google_protobuf_EnumDescriptorProto__Descriptor;
      }
    }

    protected override FieldAccessorTable<EnumDescriptorProto, EnumDescriptorProto.Builder> InternalFieldAccessors
    {
      get
      {
        return DescriptorProtoFile.internal__static_google_protobuf_EnumDescriptorProto__FieldAccessorTable;
      }
    }

    public bool HasName
    {
      get
      {
        return this.hasName;
      }
    }

    public string Name
    {
      get
      {
        return this.name_;
      }
    }

    public IList<EnumValueDescriptorProto> ValueList
    {
      get
      {
        return (IList<EnumValueDescriptorProto>) this.value_;
      }
    }

    public int ValueCount
    {
      get
      {
        return this.value_.Count;
      }
    }

    public EnumValueDescriptorProto GetValue(int index)
    {
      return this.value_[index];
    }

    public bool HasOptions
    {
      get
      {
        return this.hasOptions;
      }
    }

    public EnumOptions Options
    {
      get
      {
        return this.options_ ?? EnumOptions.DefaultInstance;
      }
    }

    public override bool IsInitialized
    {
      get
      {
        foreach (AbstractMessageLite<EnumValueDescriptorProto, EnumValueDescriptorProto.Builder> abstractMessageLite in (IEnumerable<EnumValueDescriptorProto>) this.ValueList)
        {
          if (!abstractMessageLite.IsInitialized)
            return false;
        }
        return !this.HasOptions || this.Options.IsInitialized;
      }
    }

    public override void WriteTo(ICodedOutputStream output)
    {
      int serializedSize = this.SerializedSize;
      string[] descriptorProtoFieldNames = EnumDescriptorProto._enumDescriptorProtoFieldNames;
      if (this.hasName)
        output.WriteString(1, descriptorProtoFieldNames[0], this.Name);
      if (this.value_.Count > 0)
        output.WriteMessageArray<EnumValueDescriptorProto>(2, descriptorProtoFieldNames[2], (IEnumerable<EnumValueDescriptorProto>) this.value_);
      if (this.hasOptions)
        output.WriteMessage(3, descriptorProtoFieldNames[1], (IMessageLite) this.Options);
      this.UnknownFields.WriteTo(output);
    }

    public override int SerializedSize
    {
      get
      {
        int memoizedSerializedSize = this.memoizedSerializedSize;
        if (memoizedSerializedSize != -1)
          return memoizedSerializedSize;
        int num1 = 0;
        if (this.hasName)
          num1 += CodedOutputStream.ComputeStringSize(1, this.Name);
        foreach (EnumValueDescriptorProto valueDescriptorProto in (IEnumerable<EnumValueDescriptorProto>) this.ValueList)
          num1 += CodedOutputStream.ComputeMessageSize(2, (IMessageLite) valueDescriptorProto);
        if (this.hasOptions)
          num1 += CodedOutputStream.ComputeMessageSize(3, (IMessageLite) this.Options);
        int num2 = num1 + this.UnknownFields.SerializedSize;
        this.memoizedSerializedSize = num2;
        return num2;
      }
    }

    public static EnumDescriptorProto ParseFrom(ByteString data)
    {
      return EnumDescriptorProto.CreateBuilder().MergeFrom(data).BuildParsed();
    }

    public static EnumDescriptorProto ParseFrom(ByteString data, ExtensionRegistry extensionRegistry)
    {
      return EnumDescriptorProto.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
    }

    public static EnumDescriptorProto ParseFrom(byte[] data)
    {
      return EnumDescriptorProto.CreateBuilder().MergeFrom(data).BuildParsed();
    }

    public static EnumDescriptorProto ParseFrom(byte[] data, ExtensionRegistry extensionRegistry)
    {
      return EnumDescriptorProto.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
    }

    public static EnumDescriptorProto ParseFrom(Stream input)
    {
      return EnumDescriptorProto.CreateBuilder().MergeFrom(input).BuildParsed();
    }

    public static EnumDescriptorProto ParseFrom(Stream input, ExtensionRegistry extensionRegistry)
    {
      return EnumDescriptorProto.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
    }

    public static EnumDescriptorProto ParseDelimitedFrom(Stream input)
    {
      return EnumDescriptorProto.CreateBuilder().MergeDelimitedFrom(input).BuildParsed();
    }

    public static EnumDescriptorProto ParseDelimitedFrom(Stream input, ExtensionRegistry extensionRegistry)
    {
      return EnumDescriptorProto.CreateBuilder().MergeDelimitedFrom(input, extensionRegistry).BuildParsed();
    }

    public static EnumDescriptorProto ParseFrom(ICodedInputStream input)
    {
      return EnumDescriptorProto.CreateBuilder().MergeFrom(input).BuildParsed();
    }

    public static EnumDescriptorProto ParseFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
    {
      return EnumDescriptorProto.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
    }

    private EnumDescriptorProto MakeReadOnly()
    {
      this.value_.MakeReadOnly();
      return this;
    }

    public static EnumDescriptorProto.Builder CreateBuilder()
    {
      return new EnumDescriptorProto.Builder();
    }

    public override EnumDescriptorProto.Builder ToBuilder()
    {
      return EnumDescriptorProto.CreateBuilder(this);
    }

    public override EnumDescriptorProto.Builder CreateBuilderForType()
    {
      return new EnumDescriptorProto.Builder();
    }

    public static EnumDescriptorProto.Builder CreateBuilder(EnumDescriptorProto prototype)
    {
      return new EnumDescriptorProto.Builder(prototype);
    }

    static EnumDescriptorProto()
    {
      object.ReferenceEquals((object) DescriptorProtoFile.Descriptor, (object) null);
    }

    [DebuggerNonUserCode]
    public sealed class Builder : GeneratedBuilder<EnumDescriptorProto, EnumDescriptorProto.Builder>
    {
      private bool resultIsReadOnly;
      private EnumDescriptorProto result;

      protected override EnumDescriptorProto.Builder ThisBuilder
      {
        get
        {
          return this;
        }
      }

      public Builder()
      {
        this.result = EnumDescriptorProto.DefaultInstance;
        this.resultIsReadOnly = true;
      }

      internal Builder(EnumDescriptorProto cloneFrom)
      {
        this.result = cloneFrom;
        this.resultIsReadOnly = true;
      }

      private EnumDescriptorProto PrepareBuilder()
      {
        if (this.resultIsReadOnly)
        {
          EnumDescriptorProto result = this.result;
          this.result = new EnumDescriptorProto();
          this.resultIsReadOnly = false;
          this.MergeFrom(result);
        }
        return this.result;
      }

      public override bool IsInitialized
      {
        get
        {
          return this.result.IsInitialized;
        }
      }

      protected override EnumDescriptorProto MessageBeingBuilt
      {
        get
        {
          return this.PrepareBuilder();
        }
      }

      public override EnumDescriptorProto.Builder Clear()
      {
        this.result = EnumDescriptorProto.DefaultInstance;
        this.resultIsReadOnly = true;
        return this;
      }

      public override EnumDescriptorProto.Builder Clone()
      {
        if (this.resultIsReadOnly)
          return new EnumDescriptorProto.Builder(this.result);
        return new EnumDescriptorProto.Builder().MergeFrom(this.result);
      }

      public override MessageDescriptor DescriptorForType
      {
        get
        {
          return EnumDescriptorProto.Descriptor;
        }
      }

      public override EnumDescriptorProto DefaultInstanceForType
      {
        get
        {
          return EnumDescriptorProto.DefaultInstance;
        }
      }

      public override EnumDescriptorProto BuildPartial()
      {
        if (this.resultIsReadOnly)
          return this.result;
        this.resultIsReadOnly = true;
        return this.result.MakeReadOnly();
      }

      public override EnumDescriptorProto.Builder MergeFrom(IMessage other)
      {
        if (other is EnumDescriptorProto)
          return this.MergeFrom((EnumDescriptorProto) other);
        base.MergeFrom(other);
        return this;
      }

      public override EnumDescriptorProto.Builder MergeFrom(EnumDescriptorProto other)
      {
        if (other == EnumDescriptorProto.DefaultInstance)
          return this;
        this.PrepareBuilder();
        if (other.HasName)
          this.Name = other.Name;
        if (other.value_.Count != 0)
          this.result.value_.Add((IEnumerable<EnumValueDescriptorProto>) other.value_);
        if (other.HasOptions)
          this.MergeOptions(other.Options);
        this.MergeUnknownFields(other.UnknownFields);
        return this;
      }

      public override EnumDescriptorProto.Builder MergeFrom(ICodedInputStream input)
      {
        return this.MergeFrom(input, ExtensionRegistry.Empty);
      }

      public override EnumDescriptorProto.Builder MergeFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
      {
        this.PrepareBuilder();
        UnknownFieldSet.Builder unknownFields = (UnknownFieldSet.Builder) null;
        uint fieldTag;
        string fieldName;
        while (input.ReadTag(out fieldTag, out fieldName))
        {
          if ((int) fieldTag == 0 && fieldName != null)
          {
            int index = Array.BinarySearch<string>(EnumDescriptorProto._enumDescriptorProtoFieldNames, fieldName, (IComparer<string>) StringComparer.Ordinal);
            if (index >= 0)
            {
              fieldTag = EnumDescriptorProto._enumDescriptorProtoFieldTags[index];
            }
            else
            {
              if (unknownFields == null)
                unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
              this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
              continue;
            }
          }
          switch (fieldTag)
          {
            case 0:
              throw InvalidProtocolBufferException.InvalidTag();
            case 10:
              this.result.hasName = input.ReadString(ref this.result.name_);
              continue;
            case 18:
              input.ReadMessageArray<EnumValueDescriptorProto>(fieldTag, fieldName, (ICollection<EnumValueDescriptorProto>) this.result.value_, EnumValueDescriptorProto.DefaultInstance, extensionRegistry);
              continue;
            case 26:
              EnumOptions.Builder builder = EnumOptions.CreateBuilder();
              if (this.result.hasOptions)
                builder.MergeFrom(this.Options);
              input.ReadMessage((IBuilderLite) builder, extensionRegistry);
              this.Options = builder.BuildPartial();
              continue;
            default:
              if (WireFormat.IsEndGroupTag(fieldTag))
              {
                if (unknownFields != null)
                  this.UnknownFields = unknownFields.Build();
                return this;
              }
              if (unknownFields == null)
                unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
              this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
              continue;
          }
        }
        if (unknownFields != null)
          this.UnknownFields = unknownFields.Build();
        return this;
      }

      public bool HasName
      {
        get
        {
          return this.result.hasName;
        }
      }

      public string Name
      {
        get
        {
          return this.result.Name;
        }
        set
        {
          this.SetName(value);
        }
      }

      public EnumDescriptorProto.Builder SetName(string value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.hasName = true;
        this.result.name_ = value;
        return this;
      }

      public EnumDescriptorProto.Builder ClearName()
      {
        this.PrepareBuilder();
        this.result.hasName = false;
        this.result.name_ = "";
        return this;
      }

      public IPopsicleList<EnumValueDescriptorProto> ValueList
      {
        get
        {
          return (IPopsicleList<EnumValueDescriptorProto>) this.PrepareBuilder().value_;
        }
      }

      public int ValueCount
      {
        get
        {
          return this.result.ValueCount;
        }
      }

      public EnumValueDescriptorProto GetValue(int index)
      {
        return this.result.GetValue(index);
      }

      public EnumDescriptorProto.Builder SetValue(int index, EnumValueDescriptorProto value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.value_[index] = value;
        return this;
      }

      public EnumDescriptorProto.Builder SetValue(int index, EnumValueDescriptorProto.Builder builderForValue)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
        this.PrepareBuilder();
        this.result.value_[index] = builderForValue.Build();
        return this;
      }

      public EnumDescriptorProto.Builder AddValue(EnumValueDescriptorProto value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.value_.Add(value);
        return this;
      }

      public EnumDescriptorProto.Builder AddValue(EnumValueDescriptorProto.Builder builderForValue)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
        this.PrepareBuilder();
        this.result.value_.Add(builderForValue.Build());
        return this;
      }

      public EnumDescriptorProto.Builder AddRangeValue(IEnumerable<EnumValueDescriptorProto> values)
      {
        this.PrepareBuilder();
        this.result.value_.Add(values);
        return this;
      }

      public EnumDescriptorProto.Builder ClearValue()
      {
        this.PrepareBuilder();
        this.result.value_.Clear();
        return this;
      }

      public bool HasOptions
      {
        get
        {
          return this.result.hasOptions;
        }
      }

      public EnumOptions Options
      {
        get
        {
          return this.result.Options;
        }
        set
        {
          this.SetOptions(value);
        }
      }

      public EnumDescriptorProto.Builder SetOptions(EnumOptions value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.hasOptions = true;
        this.result.options_ = value;
        return this;
      }

      public EnumDescriptorProto.Builder SetOptions(EnumOptions.Builder builderForValue)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
        this.PrepareBuilder();
        this.result.hasOptions = true;
        this.result.options_ = builderForValue.Build();
        return this;
      }

      public EnumDescriptorProto.Builder MergeOptions(EnumOptions value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.options_ = !this.result.hasOptions || this.result.options_ == EnumOptions.DefaultInstance ? value : EnumOptions.CreateBuilder(this.result.options_).MergeFrom(value).BuildPartial();
        this.result.hasOptions = true;
        return this;
      }

      public EnumDescriptorProto.Builder ClearOptions()
      {
        this.PrepareBuilder();
        this.result.hasOptions = false;
        this.result.options_ = (EnumOptions) null;
        return this;
      }
    }
  }
}
