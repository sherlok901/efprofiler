﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.DescriptorProtos.CSharpFieldOptions
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers.Descriptors;
using Google.ProtocolBuffers.FieldAccess;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace Google.ProtocolBuffers.DescriptorProtos
{
  [DebuggerNonUserCode]
  public sealed class CSharpFieldOptions : GeneratedMessage<CSharpFieldOptions, CSharpFieldOptions.Builder>
  {
    private static readonly CSharpFieldOptions defaultInstance = new CSharpFieldOptions().MakeReadOnly();
    private static readonly string[] _cSharpFieldOptionsFieldNames = new string[1]
    {
      "property_name"
    };
    private static readonly uint[] _cSharpFieldOptionsFieldTags = new uint[1]
    {
      10U
    };
    private string propertyName_ = "";
    private int memoizedSerializedSize = -1;
    public const int PropertyNameFieldNumber = 1;
    private bool hasPropertyName;

    private CSharpFieldOptions()
    {
    }

    public static CSharpFieldOptions DefaultInstance
    {
      get
      {
        return CSharpFieldOptions.defaultInstance;
      }
    }

    public override CSharpFieldOptions DefaultInstanceForType
    {
      get
      {
        return CSharpFieldOptions.DefaultInstance;
      }
    }

    protected override CSharpFieldOptions ThisMessage
    {
      get
      {
        return this;
      }
    }

    public static MessageDescriptor Descriptor
    {
      get
      {
        return CSharpOptions.internal__static_google_protobuf_CSharpFieldOptions__Descriptor;
      }
    }

    protected override FieldAccessorTable<CSharpFieldOptions, CSharpFieldOptions.Builder> InternalFieldAccessors
    {
      get
      {
        return CSharpOptions.internal__static_google_protobuf_CSharpFieldOptions__FieldAccessorTable;
      }
    }

    public bool HasPropertyName
    {
      get
      {
        return this.hasPropertyName;
      }
    }

    public string PropertyName
    {
      get
      {
        return this.propertyName_;
      }
    }

    public override bool IsInitialized
    {
      get
      {
        return true;
      }
    }

    public override void WriteTo(ICodedOutputStream output)
    {
      int serializedSize = this.SerializedSize;
      string[] optionsFieldNames = CSharpFieldOptions._cSharpFieldOptionsFieldNames;
      if (this.hasPropertyName)
        output.WriteString(1, optionsFieldNames[0], this.PropertyName);
      this.UnknownFields.WriteTo(output);
    }

    public override int SerializedSize
    {
      get
      {
        int memoizedSerializedSize = this.memoizedSerializedSize;
        if (memoizedSerializedSize != -1)
          return memoizedSerializedSize;
        int num1 = 0;
        if (this.hasPropertyName)
          num1 += CodedOutputStream.ComputeStringSize(1, this.PropertyName);
        int num2 = num1 + this.UnknownFields.SerializedSize;
        this.memoizedSerializedSize = num2;
        return num2;
      }
    }

    public static CSharpFieldOptions ParseFrom(ByteString data)
    {
      return CSharpFieldOptions.CreateBuilder().MergeFrom(data).BuildParsed();
    }

    public static CSharpFieldOptions ParseFrom(ByteString data, ExtensionRegistry extensionRegistry)
    {
      return CSharpFieldOptions.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
    }

    public static CSharpFieldOptions ParseFrom(byte[] data)
    {
      return CSharpFieldOptions.CreateBuilder().MergeFrom(data).BuildParsed();
    }

    public static CSharpFieldOptions ParseFrom(byte[] data, ExtensionRegistry extensionRegistry)
    {
      return CSharpFieldOptions.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
    }

    public static CSharpFieldOptions ParseFrom(Stream input)
    {
      return CSharpFieldOptions.CreateBuilder().MergeFrom(input).BuildParsed();
    }

    public static CSharpFieldOptions ParseFrom(Stream input, ExtensionRegistry extensionRegistry)
    {
      return CSharpFieldOptions.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
    }

    public static CSharpFieldOptions ParseDelimitedFrom(Stream input)
    {
      return CSharpFieldOptions.CreateBuilder().MergeDelimitedFrom(input).BuildParsed();
    }

    public static CSharpFieldOptions ParseDelimitedFrom(Stream input, ExtensionRegistry extensionRegistry)
    {
      return CSharpFieldOptions.CreateBuilder().MergeDelimitedFrom(input, extensionRegistry).BuildParsed();
    }

    public static CSharpFieldOptions ParseFrom(ICodedInputStream input)
    {
      return CSharpFieldOptions.CreateBuilder().MergeFrom(input).BuildParsed();
    }

    public static CSharpFieldOptions ParseFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
    {
      return CSharpFieldOptions.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
    }

    private CSharpFieldOptions MakeReadOnly()
    {
      return this;
    }

    public static CSharpFieldOptions.Builder CreateBuilder()
    {
      return new CSharpFieldOptions.Builder();
    }

    public override CSharpFieldOptions.Builder ToBuilder()
    {
      return CSharpFieldOptions.CreateBuilder(this);
    }

    public override CSharpFieldOptions.Builder CreateBuilderForType()
    {
      return new CSharpFieldOptions.Builder();
    }

    public static CSharpFieldOptions.Builder CreateBuilder(CSharpFieldOptions prototype)
    {
      return new CSharpFieldOptions.Builder(prototype);
    }

    static CSharpFieldOptions()
    {
      object.ReferenceEquals((object) CSharpOptions.Descriptor, (object) null);
    }

    [DebuggerNonUserCode]
    public sealed class Builder : GeneratedBuilder<CSharpFieldOptions, CSharpFieldOptions.Builder>
    {
      private bool resultIsReadOnly;
      private CSharpFieldOptions result;

      protected override CSharpFieldOptions.Builder ThisBuilder
      {
        get
        {
          return this;
        }
      }

      public Builder()
      {
        this.result = CSharpFieldOptions.DefaultInstance;
        this.resultIsReadOnly = true;
      }

      internal Builder(CSharpFieldOptions cloneFrom)
      {
        this.result = cloneFrom;
        this.resultIsReadOnly = true;
      }

      private CSharpFieldOptions PrepareBuilder()
      {
        if (this.resultIsReadOnly)
        {
          CSharpFieldOptions result = this.result;
          this.result = new CSharpFieldOptions();
          this.resultIsReadOnly = false;
          this.MergeFrom(result);
        }
        return this.result;
      }

      public override bool IsInitialized
      {
        get
        {
          return this.result.IsInitialized;
        }
      }

      protected override CSharpFieldOptions MessageBeingBuilt
      {
        get
        {
          return this.PrepareBuilder();
        }
      }

      public override CSharpFieldOptions.Builder Clear()
      {
        this.result = CSharpFieldOptions.DefaultInstance;
        this.resultIsReadOnly = true;
        return this;
      }

      public override CSharpFieldOptions.Builder Clone()
      {
        if (this.resultIsReadOnly)
          return new CSharpFieldOptions.Builder(this.result);
        return new CSharpFieldOptions.Builder().MergeFrom(this.result);
      }

      public override MessageDescriptor DescriptorForType
      {
        get
        {
          return CSharpFieldOptions.Descriptor;
        }
      }

      public override CSharpFieldOptions DefaultInstanceForType
      {
        get
        {
          return CSharpFieldOptions.DefaultInstance;
        }
      }

      public override CSharpFieldOptions BuildPartial()
      {
        if (this.resultIsReadOnly)
          return this.result;
        this.resultIsReadOnly = true;
        return this.result.MakeReadOnly();
      }

      public override CSharpFieldOptions.Builder MergeFrom(IMessage other)
      {
        if (other is CSharpFieldOptions)
          return this.MergeFrom((CSharpFieldOptions) other);
        base.MergeFrom(other);
        return this;
      }

      public override CSharpFieldOptions.Builder MergeFrom(CSharpFieldOptions other)
      {
        if (other == CSharpFieldOptions.DefaultInstance)
          return this;
        this.PrepareBuilder();
        if (other.HasPropertyName)
          this.PropertyName = other.PropertyName;
        this.MergeUnknownFields(other.UnknownFields);
        return this;
      }

      public override CSharpFieldOptions.Builder MergeFrom(ICodedInputStream input)
      {
        return this.MergeFrom(input, ExtensionRegistry.Empty);
      }

      public override CSharpFieldOptions.Builder MergeFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
      {
        this.PrepareBuilder();
        UnknownFieldSet.Builder unknownFields = (UnknownFieldSet.Builder) null;
        uint fieldTag;
        string fieldName;
        while (input.ReadTag(out fieldTag, out fieldName))
        {
          if ((int) fieldTag == 0 && fieldName != null)
          {
            int index = Array.BinarySearch<string>(CSharpFieldOptions._cSharpFieldOptionsFieldNames, fieldName, (IComparer<string>) StringComparer.Ordinal);
            if (index >= 0)
            {
              fieldTag = CSharpFieldOptions._cSharpFieldOptionsFieldTags[index];
            }
            else
            {
              if (unknownFields == null)
                unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
              this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
              continue;
            }
          }
          switch (fieldTag)
          {
            case 0:
              throw InvalidProtocolBufferException.InvalidTag();
            case 10:
              this.result.hasPropertyName = input.ReadString(ref this.result.propertyName_);
              continue;
            default:
              if (WireFormat.IsEndGroupTag(fieldTag))
              {
                if (unknownFields != null)
                  this.UnknownFields = unknownFields.Build();
                return this;
              }
              if (unknownFields == null)
                unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
              this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
              continue;
          }
        }
        if (unknownFields != null)
          this.UnknownFields = unknownFields.Build();
        return this;
      }

      public bool HasPropertyName
      {
        get
        {
          return this.result.hasPropertyName;
        }
      }

      public string PropertyName
      {
        get
        {
          return this.result.PropertyName;
        }
        set
        {
          this.SetPropertyName(value);
        }
      }

      public CSharpFieldOptions.Builder SetPropertyName(string value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.hasPropertyName = true;
        this.result.propertyName_ = value;
        return this;
      }

      public CSharpFieldOptions.Builder ClearPropertyName()
      {
        this.PrepareBuilder();
        this.result.hasPropertyName = false;
        this.result.propertyName_ = "";
        return this;
      }
    }
  }
}
