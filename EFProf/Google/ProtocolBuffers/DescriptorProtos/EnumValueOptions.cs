﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.DescriptorProtos.EnumValueOptions
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers.Collections;
using Google.ProtocolBuffers.Descriptors;
using Google.ProtocolBuffers.FieldAccess;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace Google.ProtocolBuffers.DescriptorProtos
{
  [DebuggerNonUserCode]
  public sealed class EnumValueOptions : ExtendableMessage<EnumValueOptions, EnumValueOptions.Builder>
  {
    private static readonly EnumValueOptions defaultInstance = new EnumValueOptions().MakeReadOnly();
    private static readonly string[] _enumValueOptionsFieldNames = new string[1]
    {
      "uninterpreted_option"
    };
    private static readonly uint[] _enumValueOptionsFieldTags = new uint[1]
    {
      7994U
    };
    private PopsicleList<UninterpretedOption> uninterpretedOption_ = new PopsicleList<UninterpretedOption>();
    private int memoizedSerializedSize = -1;
    public const int UninterpretedOptionFieldNumber = 999;

    private EnumValueOptions()
    {
    }

    public static EnumValueOptions DefaultInstance
    {
      get
      {
        return EnumValueOptions.defaultInstance;
      }
    }

    public override EnumValueOptions DefaultInstanceForType
    {
      get
      {
        return EnumValueOptions.DefaultInstance;
      }
    }

    protected override EnumValueOptions ThisMessage
    {
      get
      {
        return this;
      }
    }

    public static MessageDescriptor Descriptor
    {
      get
      {
        return DescriptorProtoFile.internal__static_google_protobuf_EnumValueOptions__Descriptor;
      }
    }

    protected override FieldAccessorTable<EnumValueOptions, EnumValueOptions.Builder> InternalFieldAccessors
    {
      get
      {
        return DescriptorProtoFile.internal__static_google_protobuf_EnumValueOptions__FieldAccessorTable;
      }
    }

    public IList<UninterpretedOption> UninterpretedOptionList
    {
      get
      {
        return (IList<UninterpretedOption>) this.uninterpretedOption_;
      }
    }

    public int UninterpretedOptionCount
    {
      get
      {
        return this.uninterpretedOption_.Count;
      }
    }

    public UninterpretedOption GetUninterpretedOption(int index)
    {
      return this.uninterpretedOption_[index];
    }

    public override bool IsInitialized
    {
      get
      {
        foreach (AbstractMessageLite<UninterpretedOption, UninterpretedOption.Builder> uninterpretedOption in (IEnumerable<UninterpretedOption>) this.UninterpretedOptionList)
        {
          if (!uninterpretedOption.IsInitialized)
            return false;
        }
        return this.ExtensionsAreInitialized;
      }
    }

    public override void WriteTo(ICodedOutputStream output)
    {
      int serializedSize = this.SerializedSize;
      string[] optionsFieldNames = EnumValueOptions._enumValueOptionsFieldNames;
      ExtendableMessage<EnumValueOptions, EnumValueOptions.Builder>.ExtensionWriter extensionWriter = this.CreateExtensionWriter((ExtendableMessage<EnumValueOptions, EnumValueOptions.Builder>) this);
      if (this.uninterpretedOption_.Count > 0)
        output.WriteMessageArray<UninterpretedOption>(999, optionsFieldNames[0], (IEnumerable<UninterpretedOption>) this.uninterpretedOption_);
      extensionWriter.WriteUntil(536870912, output);
      this.UnknownFields.WriteTo(output);
    }

    public override int SerializedSize
    {
      get
      {
        int memoizedSerializedSize = this.memoizedSerializedSize;
        if (memoizedSerializedSize != -1)
          return memoizedSerializedSize;
        int num1 = 0;
        foreach (UninterpretedOption uninterpretedOption in (IEnumerable<UninterpretedOption>) this.UninterpretedOptionList)
          num1 += CodedOutputStream.ComputeMessageSize(999, (IMessageLite) uninterpretedOption);
        int num2 = num1 + this.ExtensionsSerializedSize + this.UnknownFields.SerializedSize;
        this.memoizedSerializedSize = num2;
        return num2;
      }
    }

    public static EnumValueOptions ParseFrom(ByteString data)
    {
      return EnumValueOptions.CreateBuilder().MergeFrom(data).BuildParsed();
    }

    public static EnumValueOptions ParseFrom(ByteString data, ExtensionRegistry extensionRegistry)
    {
      return EnumValueOptions.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
    }

    public static EnumValueOptions ParseFrom(byte[] data)
    {
      return EnumValueOptions.CreateBuilder().MergeFrom(data).BuildParsed();
    }

    public static EnumValueOptions ParseFrom(byte[] data, ExtensionRegistry extensionRegistry)
    {
      return EnumValueOptions.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
    }

    public static EnumValueOptions ParseFrom(Stream input)
    {
      return EnumValueOptions.CreateBuilder().MergeFrom(input).BuildParsed();
    }

    public static EnumValueOptions ParseFrom(Stream input, ExtensionRegistry extensionRegistry)
    {
      return EnumValueOptions.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
    }

    public static EnumValueOptions ParseDelimitedFrom(Stream input)
    {
      return EnumValueOptions.CreateBuilder().MergeDelimitedFrom(input).BuildParsed();
    }

    public static EnumValueOptions ParseDelimitedFrom(Stream input, ExtensionRegistry extensionRegistry)
    {
      return EnumValueOptions.CreateBuilder().MergeDelimitedFrom(input, extensionRegistry).BuildParsed();
    }

    public static EnumValueOptions ParseFrom(ICodedInputStream input)
    {
      return EnumValueOptions.CreateBuilder().MergeFrom(input).BuildParsed();
    }

    public static EnumValueOptions ParseFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
    {
      return EnumValueOptions.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
    }

    private EnumValueOptions MakeReadOnly()
    {
      this.uninterpretedOption_.MakeReadOnly();
      return this;
    }

    public static EnumValueOptions.Builder CreateBuilder()
    {
      return new EnumValueOptions.Builder();
    }

    public override EnumValueOptions.Builder ToBuilder()
    {
      return EnumValueOptions.CreateBuilder(this);
    }

    public override EnumValueOptions.Builder CreateBuilderForType()
    {
      return new EnumValueOptions.Builder();
    }

    public static EnumValueOptions.Builder CreateBuilder(EnumValueOptions prototype)
    {
      return new EnumValueOptions.Builder(prototype);
    }

    static EnumValueOptions()
    {
      object.ReferenceEquals((object) DescriptorProtoFile.Descriptor, (object) null);
    }

    [DebuggerNonUserCode]
    public sealed class Builder : ExtendableBuilder<EnumValueOptions, EnumValueOptions.Builder>
    {
      private bool resultIsReadOnly;
      private EnumValueOptions result;

      protected override EnumValueOptions.Builder ThisBuilder
      {
        get
        {
          return this;
        }
      }

      public Builder()
      {
        this.result = EnumValueOptions.DefaultInstance;
        this.resultIsReadOnly = true;
      }

      internal Builder(EnumValueOptions cloneFrom)
      {
        this.result = cloneFrom;
        this.resultIsReadOnly = true;
      }

      private EnumValueOptions PrepareBuilder()
      {
        if (this.resultIsReadOnly)
        {
          EnumValueOptions result = this.result;
          this.result = new EnumValueOptions();
          this.resultIsReadOnly = false;
          this.MergeFrom(result);
        }
        return this.result;
      }

      public override bool IsInitialized
      {
        get
        {
          return this.result.IsInitialized;
        }
      }

      protected override EnumValueOptions MessageBeingBuilt
      {
        get
        {
          return this.PrepareBuilder();
        }
      }

      public override EnumValueOptions.Builder Clear()
      {
        this.result = EnumValueOptions.DefaultInstance;
        this.resultIsReadOnly = true;
        return this;
      }

      public override EnumValueOptions.Builder Clone()
      {
        if (this.resultIsReadOnly)
          return new EnumValueOptions.Builder(this.result);
        return new EnumValueOptions.Builder().MergeFrom(this.result);
      }

      public override MessageDescriptor DescriptorForType
      {
        get
        {
          return EnumValueOptions.Descriptor;
        }
      }

      public override EnumValueOptions DefaultInstanceForType
      {
        get
        {
          return EnumValueOptions.DefaultInstance;
        }
      }

      public override EnumValueOptions BuildPartial()
      {
        if (this.resultIsReadOnly)
          return this.result;
        this.resultIsReadOnly = true;
        return this.result.MakeReadOnly();
      }

      public override EnumValueOptions.Builder MergeFrom(IMessage other)
      {
        if (other is EnumValueOptions)
          return this.MergeFrom((EnumValueOptions) other);
        base.MergeFrom(other);
        return this;
      }

      public override EnumValueOptions.Builder MergeFrom(EnumValueOptions other)
      {
        if (other == EnumValueOptions.DefaultInstance)
          return this;
        this.PrepareBuilder();
        if (other.uninterpretedOption_.Count != 0)
          this.result.uninterpretedOption_.Add((IEnumerable<UninterpretedOption>) other.uninterpretedOption_);
        this.MergeExtensionFields((ExtendableMessage<EnumValueOptions, EnumValueOptions.Builder>) other);
        this.MergeUnknownFields(other.UnknownFields);
        return this;
      }

      public override EnumValueOptions.Builder MergeFrom(ICodedInputStream input)
      {
        return this.MergeFrom(input, ExtensionRegistry.Empty);
      }

      public override EnumValueOptions.Builder MergeFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
      {
        this.PrepareBuilder();
        UnknownFieldSet.Builder unknownFields = (UnknownFieldSet.Builder) null;
        uint fieldTag;
        string fieldName;
        while (input.ReadTag(out fieldTag, out fieldName))
        {
          if ((int) fieldTag == 0 && fieldName != null)
          {
            int index = Array.BinarySearch<string>(EnumValueOptions._enumValueOptionsFieldNames, fieldName, (IComparer<string>) StringComparer.Ordinal);
            if (index >= 0)
            {
              fieldTag = EnumValueOptions._enumValueOptionsFieldTags[index];
            }
            else
            {
              if (unknownFields == null)
                unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
              this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
              continue;
            }
          }
          switch (fieldTag)
          {
            case 0:
              throw InvalidProtocolBufferException.InvalidTag();
            case 7994:
              input.ReadMessageArray<UninterpretedOption>(fieldTag, fieldName, (ICollection<UninterpretedOption>) this.result.uninterpretedOption_, UninterpretedOption.DefaultInstance, extensionRegistry);
              continue;
            default:
              if (WireFormat.IsEndGroupTag(fieldTag))
              {
                if (unknownFields != null)
                  this.UnknownFields = unknownFields.Build();
                return this;
              }
              if (unknownFields == null)
                unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
              this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
              continue;
          }
        }
        if (unknownFields != null)
          this.UnknownFields = unknownFields.Build();
        return this;
      }

      public IPopsicleList<UninterpretedOption> UninterpretedOptionList
      {
        get
        {
          return (IPopsicleList<UninterpretedOption>) this.PrepareBuilder().uninterpretedOption_;
        }
      }

      public int UninterpretedOptionCount
      {
        get
        {
          return this.result.UninterpretedOptionCount;
        }
      }

      public UninterpretedOption GetUninterpretedOption(int index)
      {
        return this.result.GetUninterpretedOption(index);
      }

      public EnumValueOptions.Builder SetUninterpretedOption(int index, UninterpretedOption value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.uninterpretedOption_[index] = value;
        return this;
      }

      public EnumValueOptions.Builder SetUninterpretedOption(int index, UninterpretedOption.Builder builderForValue)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
        this.PrepareBuilder();
        this.result.uninterpretedOption_[index] = builderForValue.Build();
        return this;
      }

      public EnumValueOptions.Builder AddUninterpretedOption(UninterpretedOption value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.uninterpretedOption_.Add(value);
        return this;
      }

      public EnumValueOptions.Builder AddUninterpretedOption(UninterpretedOption.Builder builderForValue)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
        this.PrepareBuilder();
        this.result.uninterpretedOption_.Add(builderForValue.Build());
        return this;
      }

      public EnumValueOptions.Builder AddRangeUninterpretedOption(IEnumerable<UninterpretedOption> values)
      {
        this.PrepareBuilder();
        this.result.uninterpretedOption_.Add(values);
        return this;
      }

      public EnumValueOptions.Builder ClearUninterpretedOption()
      {
        this.PrepareBuilder();
        this.result.uninterpretedOption_.Clear();
        return this;
      }
    }
  }
}
