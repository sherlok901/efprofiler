﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.GeneratedMessage`2
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers.Collections;
using Google.ProtocolBuffers.Descriptors;
using Google.ProtocolBuffers.FieldAccess;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Google.ProtocolBuffers
{
  [Serializable]
  public abstract class GeneratedMessage<TMessage, TBuilder> : AbstractMessage<TMessage, TBuilder> where TMessage : GeneratedMessage<TMessage, TBuilder> where TBuilder : GeneratedBuilder<TMessage, TBuilder>, new()
  {
    private UnknownFieldSet unknownFields = UnknownFieldSet.DefaultInstance;

    protected abstract TMessage ThisMessage { get; }

    internal FieldAccessorTable<TMessage, TBuilder> FieldAccessorsFromBuilder
    {
      get
      {
        return this.InternalFieldAccessors;
      }
    }

    protected abstract FieldAccessorTable<TMessage, TBuilder> InternalFieldAccessors { get; }

    public override MessageDescriptor DescriptorForType
    {
      get
      {
        return this.InternalFieldAccessors.Descriptor;
      }
    }

    internal IDictionary<FieldDescriptor, object> GetMutableFieldMap()
    {
      SortedList<FieldDescriptor, object> sortedList = new SortedList<FieldDescriptor, object>();
      foreach (FieldDescriptor field in (IEnumerable<FieldDescriptor>) this.DescriptorForType.Fields)
      {
        IFieldAccessor<TMessage, TBuilder> internalFieldAccessor = this.InternalFieldAccessors[field];
        if (field.IsRepeated)
        {
          if (internalFieldAccessor.GetRepeatedCount(this.ThisMessage) != 0)
            sortedList[field] = internalFieldAccessor.GetValue(this.ThisMessage);
        }
        else if (this.HasField(field))
          sortedList[field] = internalFieldAccessor.GetValue(this.ThisMessage);
      }
      return (IDictionary<FieldDescriptor, object>) sortedList;
    }

    public override bool IsInitialized
    {
      get
      {
        foreach (FieldDescriptor field in (IEnumerable<FieldDescriptor>) this.DescriptorForType.Fields)
        {
          if (field.IsRequired && !this.HasField(field))
            return false;
          if (field.MappedType == MappedType.Message)
          {
            if (field.IsRepeated)
            {
              foreach (IMessageLite messageLite in (IEnumerable) this[field])
              {
                if (!messageLite.IsInitialized)
                  return false;
              }
            }
            else if (this.HasField(field) && !((IMessageLite) this[field]).IsInitialized)
              return false;
          }
        }
        return true;
      }
    }

    public override IDictionary<FieldDescriptor, object> AllFields
    {
      get
      {
        return Dictionaries.AsReadOnly<FieldDescriptor, object>(this.GetMutableFieldMap());
      }
    }

    public override bool HasField(FieldDescriptor field)
    {
      return this.InternalFieldAccessors[field].Has(this.ThisMessage);
    }

    public override int GetRepeatedFieldCount(FieldDescriptor field)
    {
      return this.InternalFieldAccessors[field].GetRepeatedCount(this.ThisMessage);
    }

    public override object this[FieldDescriptor field, int index]
    {
      get
      {
        return this.InternalFieldAccessors[field].GetRepeatedValue(this.ThisMessage, index);
      }
    }

    public override object this[FieldDescriptor field]
    {
      get
      {
        return this.InternalFieldAccessors[field].GetValue(this.ThisMessage);
      }
    }

    public override UnknownFieldSet UnknownFields
    {
      get
      {
        return this.unknownFields;
      }
    }

    internal void SetUnknownFields(UnknownFieldSet fieldSet)
    {
      this.unknownFields = fieldSet;
    }
  }
}
