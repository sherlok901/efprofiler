﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.GeneratedExtensionBase`1
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers.Descriptors;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

namespace Google.ProtocolBuffers
{
  public abstract class GeneratedExtensionBase<TExtension>
  {
    private readonly FieldDescriptor descriptor;
    private readonly IMessageLite messageDefaultInstance;

    protected GeneratedExtensionBase(FieldDescriptor descriptor, Type singularExtensionType)
    {
      if (!descriptor.IsExtension)
        throw new ArgumentException("GeneratedExtension given a regular (non-extension) field.");
      this.descriptor = descriptor;
      if (descriptor.MappedType != MappedType.Message)
        return;
      PropertyInfo property = singularExtensionType.GetProperty("DefaultInstance", BindingFlags.Static | BindingFlags.Public);
      if (property == null)
        throw new ArgumentException("No public static DefaultInstance property for type " + typeof (TExtension).Name);
      this.messageDefaultInstance = (IMessageLite) property.GetValue((object) null, (object[]) null);
    }

    public FieldDescriptor Descriptor
    {
      get
      {
        return this.descriptor;
      }
    }

    public int Number
    {
      get
      {
        return this.Descriptor.FieldNumber;
      }
    }

    public IMessageLite MessageDefaultInstance
    {
      get
      {
        return this.messageDefaultInstance;
      }
    }

    public object SingularFromReflectionType(object value)
    {
      switch (this.Descriptor.MappedType)
      {
        case MappedType.Message:
          if (value is TExtension)
            return value;
          return (object) this.MessageDefaultInstance.WeakCreateBuilderForType().WeakMergeFrom((IMessageLite) value).WeakBuild();
        case MappedType.Enum:
          return (object) ((EnumValueDescriptor) value).Number;
        default:
          return value;
      }
    }

    public object ToReflectionType(object value)
    {
      if (!this.descriptor.IsRepeated)
        return this.SingularToReflectionType(value);
      if (this.descriptor.MappedType != MappedType.Enum)
        return value;
      IList<object> objectList = (IList<object>) new List<object>();
      foreach (object obj in (IEnumerable) value)
        objectList.Add(this.SingularToReflectionType(obj));
      return (object) objectList;
    }

    internal object SingularToReflectionType(object value)
    {
      if (this.descriptor.MappedType != MappedType.Enum)
        return value;
      return (object) this.descriptor.EnumType.FindValueByNumber((int) value);
    }

    public abstract object FromReflectionType(object value);
  }
}
