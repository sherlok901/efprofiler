﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.IBuilderLite`2
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using System.IO;

namespace Google.ProtocolBuffers
{
  public interface IBuilderLite<TMessage, TBuilder> : IBuilderLite where TMessage : IMessageLite<TMessage, TBuilder> where TBuilder : IBuilderLite<TMessage, TBuilder>
  {
    TBuilder Clear();

    TBuilder MergeFrom(IMessageLite other);

    TMessage Build();

    TMessage BuildPartial();

    TBuilder Clone();

    TBuilder MergeFrom(ICodedInputStream input);

    TBuilder MergeFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry);

    TMessage DefaultInstanceForType { get; }

    TBuilder MergeDelimitedFrom(Stream input);

    TBuilder MergeDelimitedFrom(Stream input, ExtensionRegistry extensionRegistry);

    TBuilder MergeFrom(ByteString data);

    TBuilder MergeFrom(ByteString data, ExtensionRegistry extensionRegistry);

    TBuilder MergeFrom(byte[] data);

    TBuilder MergeFrom(byte[] data, ExtensionRegistry extensionRegistry);

    TBuilder MergeFrom(Stream input);

    TBuilder MergeFrom(Stream input, ExtensionRegistry extensionRegistry);
  }
}
