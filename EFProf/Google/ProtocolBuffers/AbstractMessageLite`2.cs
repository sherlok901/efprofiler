﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.AbstractMessageLite`2
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using System;
using System.IO;
using System.Runtime.Serialization;
using System.Security;

namespace Google.ProtocolBuffers
{
  [Serializable]
  public abstract class AbstractMessageLite<TMessage, TBuilder> : IMessageLite<TMessage, TBuilder>, IMessageLite<TMessage>, IMessageLite, ISerializable where TMessage : AbstractMessageLite<TMessage, TBuilder> where TBuilder : AbstractBuilderLite<TMessage, TBuilder>
  {
    public abstract TBuilder CreateBuilderForType();

    public abstract TBuilder ToBuilder();

    public abstract TMessage DefaultInstanceForType { get; }

    public abstract bool IsInitialized { get; }

    public abstract void WriteTo(ICodedOutputStream output);

    public abstract int SerializedSize { get; }

    public abstract void PrintTo(TextWriter writer);

    public ByteString ToByteString()
    {
      ByteString.CodedBuilder codedBuilder = new ByteString.CodedBuilder(this.SerializedSize);
      this.WriteTo((ICodedOutputStream) codedBuilder.CodedOutput);
      return codedBuilder.Build();
    }

    public byte[] ToByteArray()
    {
      byte[] flatArray = new byte[this.SerializedSize];
      CodedOutputStream instance = CodedOutputStream.CreateInstance(flatArray);
      this.WriteTo((ICodedOutputStream) instance);
      instance.CheckNoSpaceLeft();
      return flatArray;
    }

    public void WriteTo(Stream output)
    {
      CodedOutputStream instance = CodedOutputStream.CreateInstance(output);
      this.WriteTo((ICodedOutputStream) instance);
      instance.Flush();
    }

    public void WriteDelimitedTo(Stream output)
    {
      CodedOutputStream instance = CodedOutputStream.CreateInstance(output);
      instance.WriteRawVarint32((uint) this.SerializedSize);
      this.WriteTo((ICodedOutputStream) instance);
      instance.Flush();
    }

    IBuilderLite IMessageLite.WeakCreateBuilderForType()
    {
      return (IBuilderLite) this.CreateBuilderForType();
    }

    IBuilderLite IMessageLite.WeakToBuilder()
    {
      return (IBuilderLite) this.ToBuilder();
    }

    IMessageLite IMessageLite.WeakDefaultInstanceForType
    {
      get
      {
        return (IMessageLite) this.DefaultInstanceForType;
      }
    }

    [SecurityCritical]
    void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
    {
      info.SetType(typeof (AbstractMessageLite<TMessage, TBuilder>.SerializationSurrogate));
      info.AddValue("message", (object) this.ToByteArray());
      info.AddValue("initialized", this.IsInitialized);
    }

    [Serializable]
    private sealed class SerializationSurrogate : IObjectReference, ISerializable
    {
      private static readonly TBuilder TemplateInstance = (TBuilder) Activator.CreateInstance(typeof (TBuilder));
      private readonly byte[] _message;
      private readonly bool _initialized;

      private SerializationSurrogate(SerializationInfo info, StreamingContext context)
      {
        this._message = (byte[]) info.GetValue("message", typeof (byte[]));
        this._initialized = info.GetBoolean("initialized");
      }

      object IObjectReference.GetRealObject(StreamingContext context)
      {
        ExtensionRegistry context1 = context.Context as ExtensionRegistry;
        TBuilder builderForType = AbstractMessageLite<TMessage, TBuilder>.SerializationSurrogate.TemplateInstance.DefaultInstanceForType.CreateBuilderForType();
        builderForType.MergeFrom(this._message, context1 ?? ExtensionRegistry.Empty);
        IDeserializationCallback deserializationCallback1 = (object) builderForType as IDeserializationCallback;
        if (deserializationCallback1 != null)
          deserializationCallback1.OnDeserialization((object) context);
        TMessage message = this._initialized ? builderForType.Build() : builderForType.BuildPartial();
        IDeserializationCallback deserializationCallback2 = (object) message as IDeserializationCallback;
        if (deserializationCallback2 != null)
          deserializationCallback2.OnDeserialization((object) context);
        return (object) message;
      }

      [SecurityCritical]
      void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
      {
        info.AddValue("message", (object) this._message);
      }
    }
  }
}
