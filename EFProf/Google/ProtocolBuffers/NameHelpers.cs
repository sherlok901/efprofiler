﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.NameHelpers
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using System;
using System.Text.RegularExpressions;

namespace Google.ProtocolBuffers
{
  public class NameHelpers
  {
    private static readonly Regex NonAlphaNumericCharacters = new Regex("[^a-zA-Z0-9]+");
    private static readonly Regex UnderscoreOrNumberWithLowerCase = new Regex("[0-9_][a-z]");

    public static string UnderscoresToPascalCase(string input)
    {
      string upperCase = NameHelpers.UnderscoresToUpperCase(input);
      if (!char.IsLower(upperCase[0]))
        return upperCase;
      char[] charArray = upperCase.ToCharArray();
      charArray[0] = char.ToUpper(charArray[0]);
      return new string(charArray);
    }

    public static string UnderscoresToCamelCase(string input)
    {
      string upperCase = NameHelpers.UnderscoresToUpperCase(input);
      if (!char.IsUpper(upperCase[0]))
        return upperCase;
      char[] charArray = upperCase.ToCharArray();
      charArray[0] = char.ToLower(charArray[0]);
      return new string(charArray);
    }

    private static string UnderscoresToUpperCase(string input)
    {
      string input1 = NameHelpers.UnderscoreOrNumberWithLowerCase.Replace(input, (MatchEvaluator) (x => x.Value.ToUpper()));
      string str = NameHelpers.NonAlphaNumericCharacters.Replace(input1, string.Empty);
      if (str.Length == 0)
        throw new ArgumentException(string.Format("The field name '{0}' is invalid.", (object) input));
      if (char.IsNumber(str[0]))
        str = 95.ToString() + str;
      return str;
    }

    internal static string StripProto(string text)
    {
      if (!NameHelpers.StripSuffix(ref text, ".protodevel"))
        NameHelpers.StripSuffix(ref text, ".proto");
      return text;
    }

    public static bool StripSuffix(ref string text, string suffix)
    {
      if (!text.EndsWith(suffix))
        return false;
      text = text.Substring(0, text.Length - suffix.Length);
      return true;
    }
  }
}
