﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.Collections.Enumerables
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using System;
using System.Collections;

namespace Google.ProtocolBuffers.Collections
{
  public static class Enumerables
  {
    public static bool Equals(IEnumerable left, IEnumerable right)
    {
      IEnumerator enumerator = left.GetEnumerator();
      try
      {
        foreach (object objB in right)
        {
          if (!enumerator.MoveNext() || !object.Equals(enumerator.Current, objB))
            return false;
        }
        if (enumerator.MoveNext())
          return false;
      }
      finally
      {
        IDisposable disposable = enumerator as IDisposable;
        if (disposable != null)
          disposable.Dispose();
      }
      return true;
    }
  }
}
