﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.Collections.Lists
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using System.Collections.Generic;

namespace Google.ProtocolBuffers.Collections
{
  public static class Lists
  {
    public static IList<T> AsReadOnly<T>(IList<T> list)
    {
      return Lists<T>.AsReadOnly(list);
    }

    public static bool Equals<T>(IList<T> left, IList<T> right)
    {
      if (left == right)
        return true;
      if (left == null || right == null || left.Count != right.Count)
        return false;
      IEqualityComparer<T> equalityComparer = (IEqualityComparer<T>) EqualityComparer<T>.Default;
      for (int index = 0; index < left.Count; ++index)
      {
        if (!equalityComparer.Equals(left[index], right[index]))
          return false;
      }
      return true;
    }

    public static int GetHashCode<T>(IList<T> list)
    {
      int num = 31;
      foreach (T obj in (IEnumerable<T>) list)
        num = num * 29 + obj.GetHashCode();
      return num;
    }
  }
}
