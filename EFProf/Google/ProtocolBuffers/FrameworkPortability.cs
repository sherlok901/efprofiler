﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.FrameworkPortability
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace Google.ProtocolBuffers
{
  internal static class FrameworkPortability
  {
    internal static readonly string NewLine = Environment.NewLine;
    internal const RegexOptions CompiledRegexWhereAvailable = RegexOptions.None;

    internal static CultureInfo InvariantCulture
    {
      get
      {
        return CultureInfo.InvariantCulture;
      }
    }

    internal static double Int64ToDouble(long value)
    {
      double[] numArray = new double[1];
      Buffer.BlockCopy((Array) new long[1]
      {
        value
      }, 0, (Array) numArray, 0, 8);
      return numArray[0];
    }

    internal static long DoubleToInt64(double value)
    {
      long[] numArray = new long[1];
      Buffer.BlockCopy((Array) new double[1]
      {
        value
      }, 0, (Array) numArray, 0, 8);
      return numArray[0];
    }

    internal static bool TryParseInt32(string text, out int number)
    {
      return FrameworkPortability.TryParseInt32(text, NumberStyles.Any, (IFormatProvider) FrameworkPortability.InvariantCulture, out number);
    }

    internal static bool TryParseInt32(string text, NumberStyles style, IFormatProvider format, out int number)
    {
      return int.TryParse(text, style, format, out number);
    }
  }
}
