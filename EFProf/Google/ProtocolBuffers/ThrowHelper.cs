﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.ThrowHelper
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using System;
using System.Collections.Generic;

namespace Google.ProtocolBuffers
{
  public static class ThrowHelper
  {
    public static void ThrowIfNull(object value, string name)
    {
      if (value == null)
        throw new ArgumentNullException(name);
    }

    public static void ThrowIfNull(object value)
    {
      if (value == null)
        throw new ArgumentNullException();
    }

    public static void ThrowIfAnyNull<T>(IEnumerable<T> sequence)
    {
      foreach (T obj in sequence)
      {
        if ((object) obj == null)
          throw new ArgumentNullException();
      }
    }

    public static Exception CreateMissingMethod(Type type, string methodName)
    {
      return (Exception) new ArgumentException(string.Format("The method '{0}' was not found on type {1}.", (object) methodName, (object) type));
    }
  }
}
