﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.AbstractBuilderLite`2
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using System;
using System.IO;
using System.Runtime.Serialization;
using System.Security;

namespace Google.ProtocolBuffers
{
  [Serializable]
  public abstract class AbstractBuilderLite<TMessage, TBuilder> : IBuilderLite<TMessage, TBuilder>, IBuilderLite, ISerializable where TMessage : AbstractMessageLite<TMessage, TBuilder> where TBuilder : AbstractBuilderLite<TMessage, TBuilder>
  {
    protected abstract TBuilder ThisBuilder { get; }

    public abstract bool IsInitialized { get; }

    public abstract TBuilder Clear();

    public abstract TBuilder Clone();

    public abstract TMessage Build();

    public abstract TMessage BuildPartial();

    public abstract TBuilder MergeFrom(IMessageLite other);

    public abstract TBuilder MergeFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry);

    public abstract TMessage DefaultInstanceForType { get; }

    public virtual TBuilder MergeFrom(ICodedInputStream input)
    {
      return this.MergeFrom(input, ExtensionRegistry.CreateInstance());
    }

    public TBuilder MergeDelimitedFrom(Stream input)
    {
      return this.MergeDelimitedFrom(input, ExtensionRegistry.CreateInstance());
    }

    public TBuilder MergeDelimitedFrom(Stream input, ExtensionRegistry extensionRegistry)
    {
      int size = (int) CodedInputStream.ReadRawVarint32(input);
      return this.MergeFrom((Stream) new AbstractBuilderLite<TMessage, TBuilder>.LimitedInputStream(input, size), extensionRegistry);
    }

    public TBuilder MergeFrom(ByteString data)
    {
      return this.MergeFrom(data, ExtensionRegistry.CreateInstance());
    }

    public TBuilder MergeFrom(ByteString data, ExtensionRegistry extensionRegistry)
    {
      CodedInputStream codedInput = data.CreateCodedInput();
      this.MergeFrom((ICodedInputStream) codedInput, extensionRegistry);
      codedInput.CheckLastTagWas(0U);
      return this.ThisBuilder;
    }

    public TBuilder MergeFrom(byte[] data)
    {
      CodedInputStream instance = CodedInputStream.CreateInstance(data);
      this.MergeFrom((ICodedInputStream) instance);
      instance.CheckLastTagWas(0U);
      return this.ThisBuilder;
    }

    public TBuilder MergeFrom(byte[] data, ExtensionRegistry extensionRegistry)
    {
      CodedInputStream instance = CodedInputStream.CreateInstance(data);
      this.MergeFrom((ICodedInputStream) instance, extensionRegistry);
      instance.CheckLastTagWas(0U);
      return this.ThisBuilder;
    }

    public TBuilder MergeFrom(Stream input)
    {
      CodedInputStream instance = CodedInputStream.CreateInstance(input);
      this.MergeFrom((ICodedInputStream) instance);
      instance.CheckLastTagWas(0U);
      return this.ThisBuilder;
    }

    public TBuilder MergeFrom(Stream input, ExtensionRegistry extensionRegistry)
    {
      CodedInputStream instance = CodedInputStream.CreateInstance(input);
      this.MergeFrom((ICodedInputStream) instance, extensionRegistry);
      instance.CheckLastTagWas(0U);
      return this.ThisBuilder;
    }

    IBuilderLite IBuilderLite.WeakClear()
    {
      return (IBuilderLite) this.Clear();
    }

    IBuilderLite IBuilderLite.WeakMergeFrom(IMessageLite message)
    {
      return (IBuilderLite) this.MergeFrom(message);
    }

    IBuilderLite IBuilderLite.WeakMergeFrom(ByteString data)
    {
      return (IBuilderLite) this.MergeFrom(data);
    }

    IBuilderLite IBuilderLite.WeakMergeFrom(ByteString data, ExtensionRegistry registry)
    {
      return (IBuilderLite) this.MergeFrom(data, registry);
    }

    IBuilderLite IBuilderLite.WeakMergeFrom(ICodedInputStream input)
    {
      return (IBuilderLite) this.MergeFrom(input);
    }

    IBuilderLite IBuilderLite.WeakMergeFrom(ICodedInputStream input, ExtensionRegistry registry)
    {
      return (IBuilderLite) this.MergeFrom(input, registry);
    }

    IMessageLite IBuilderLite.WeakBuild()
    {
      return (IMessageLite) this.Build();
    }

    IMessageLite IBuilderLite.WeakBuildPartial()
    {
      return (IMessageLite) this.BuildPartial();
    }

    IBuilderLite IBuilderLite.WeakClone()
    {
      return (IBuilderLite) this.Clone();
    }

    IMessageLite IBuilderLite.WeakDefaultInstanceForType
    {
      get
      {
        return (IMessageLite) this.DefaultInstanceForType;
      }
    }

    [SecurityCritical]
    void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
    {
      info.SetType(typeof (AbstractBuilderLite<TMessage, TBuilder>.SerializationSurrogate));
      info.AddValue("message", (object) this.Clone().BuildPartial().ToByteArray());
    }

    private class LimitedInputStream : Stream
    {
      private readonly Stream proxied;
      private int bytesLeft;

      internal LimitedInputStream(Stream proxied, int size)
      {
        this.proxied = proxied;
        this.bytesLeft = size;
      }

      public override bool CanRead
      {
        get
        {
          return true;
        }
      }

      public override bool CanSeek
      {
        get
        {
          return false;
        }
      }

      public override bool CanWrite
      {
        get
        {
          return false;
        }
      }

      public override void Flush()
      {
      }

      public override long Length
      {
        get
        {
          throw new NotSupportedException();
        }
      }

      public override long Position
      {
        get
        {
          throw new NotSupportedException();
        }
        set
        {
          throw new NotSupportedException();
        }
      }

      public override int Read(byte[] buffer, int offset, int count)
      {
        if (this.bytesLeft <= 0)
          return 0;
        int num = this.proxied.Read(buffer, offset, Math.Min(this.bytesLeft, count));
        this.bytesLeft -= num;
        return num;
      }

      public override long Seek(long offset, SeekOrigin origin)
      {
        throw new NotSupportedException();
      }

      public override void SetLength(long value)
      {
        throw new NotSupportedException();
      }

      public override void Write(byte[] buffer, int offset, int count)
      {
        throw new NotSupportedException();
      }
    }

    [Serializable]
    private sealed class SerializationSurrogate : IObjectReference, ISerializable
    {
      private static readonly TBuilder TemplateInstance = (TBuilder) Activator.CreateInstance(typeof (TBuilder));
      private readonly byte[] _message;

      private SerializationSurrogate(SerializationInfo info, StreamingContext context)
      {
        this._message = (byte[]) info.GetValue("message", typeof (byte[]));
      }

      object IObjectReference.GetRealObject(StreamingContext context)
      {
        ExtensionRegistry context1 = context.Context as ExtensionRegistry;
        TBuilder builderForType = AbstractBuilderLite<TMessage, TBuilder>.SerializationSurrogate.TemplateInstance.DefaultInstanceForType.CreateBuilderForType();
        builderForType.MergeFrom(this._message, context1 ?? ExtensionRegistry.Empty);
        IDeserializationCallback deserializationCallback = (object) builderForType as IDeserializationCallback;
        if (deserializationCallback != null)
          deserializationCallback.OnDeserialization((object) context);
        return (object) builderForType;
      }

      [SecurityCritical]
      void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
      {
        info.AddValue("message", (object) this._message);
      }
    }
  }
}
