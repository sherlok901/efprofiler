﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.IService
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers.Descriptors;
using System;

namespace Google.ProtocolBuffers
{
  public interface IService
  {
    ServiceDescriptor DescriptorForType { get; }

    void CallMethod(MethodDescriptor method, IRpcController controller, IMessage request, Action<IMessage> done);

    IMessage GetRequestPrototype(MethodDescriptor method);

    IMessage GetResponsePrototype(MethodDescriptor method);
  }
}
