﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.MessageUtil
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using System;
using System.Reflection;

namespace Google.ProtocolBuffers
{
  public static class MessageUtil
  {
    public static IMessage GetDefaultMessage(Type type)
    {
      if (type == null)
        throw new ArgumentNullException(nameof (type), "No type specified.");
      if (type.IsAbstract || type.IsGenericTypeDefinition)
        throw new ArgumentException("Unable to get a default message for an abstract or generic type (" + type.FullName + ")");
      if (!typeof (IMessage).IsAssignableFrom(type))
        throw new ArgumentException("Unable to get a default message for non-message type (" + type.FullName + ")");
      PropertyInfo property = type.GetProperty("DefaultInstance", BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
      if (property == null)
        throw new ArgumentException(type.FullName + " doesn't have a static DefaultInstance property");
      if (property.PropertyType != type)
        throw new ArgumentException(type.FullName + ".DefaultInstance property is of the wrong type");
      return (IMessage) property.GetValue((object) null, (object[]) null);
    }

    public static IMessage GetDefaultMessage(string typeName)
    {
      if (typeName == null)
        throw new ArgumentNullException(nameof (typeName), "No type name specified.");
      Type type = Type.GetType(typeName);
      if (type == null)
        throw new ArgumentException("Unable to load type " + typeName);
      return MessageUtil.GetDefaultMessage(type);
    }
  }
}
