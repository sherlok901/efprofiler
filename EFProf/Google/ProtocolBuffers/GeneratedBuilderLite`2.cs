﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.GeneratedBuilderLite`2
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using System;

namespace Google.ProtocolBuffers
{
  [Serializable]
  public abstract class GeneratedBuilderLite<TMessage, TBuilder> : AbstractBuilderLite<TMessage, TBuilder> where TMessage : GeneratedMessageLite<TMessage, TBuilder> where TBuilder : GeneratedBuilderLite<TMessage, TBuilder>
  {
    protected abstract TMessage MessageBeingBuilt { get; }

    public override TBuilder MergeFrom(IMessageLite other)
    {
      return this.ThisBuilder;
    }

    public abstract TBuilder MergeFrom(TMessage other);

    [CLSCompliant(false)]
    protected virtual bool ParseUnknownField(ICodedInputStream input, ExtensionRegistry extensionRegistry, uint tag, string fieldName)
    {
      return input.SkipField();
    }

    public TMessage BuildParsed()
    {
      if (!this.IsInitialized)
        throw new UninitializedMessageException((IMessageLite) this.MessageBeingBuilt).AsInvalidProtocolBufferException();
      return this.BuildPartial();
    }

    public override TMessage Build()
    {
      if (!this.IsInitialized)
        throw new UninitializedMessageException((IMessageLite) this.MessageBeingBuilt);
      return this.BuildPartial();
    }
  }
}
