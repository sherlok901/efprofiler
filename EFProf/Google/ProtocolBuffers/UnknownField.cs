﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.UnknownField
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers.Collections;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Google.ProtocolBuffers
{
  public sealed class UnknownField
  {
    private static readonly UnknownField defaultInstance = UnknownField.CreateBuilder().Build();
    public const string UnknownFieldName = "unknown_field";
    private readonly ReadOnlyCollection<ulong> varintList;
    private readonly ReadOnlyCollection<uint> fixed32List;
    private readonly ReadOnlyCollection<ulong> fixed64List;
    private readonly ReadOnlyCollection<ByteString> lengthDelimitedList;
    private readonly ReadOnlyCollection<UnknownFieldSet> groupList;

    private UnknownField(ReadOnlyCollection<ulong> varintList, ReadOnlyCollection<uint> fixed32List, ReadOnlyCollection<ulong> fixed64List, ReadOnlyCollection<ByteString> lengthDelimitedList, ReadOnlyCollection<UnknownFieldSet> groupList)
    {
      this.varintList = varintList;
      this.fixed32List = fixed32List;
      this.fixed64List = fixed64List;
      this.lengthDelimitedList = lengthDelimitedList;
      this.groupList = groupList;
    }

    public static UnknownField DefaultInstance
    {
      get
      {
        return UnknownField.defaultInstance;
      }
    }

    public IList<ulong> VarintList
    {
      get
      {
        return (IList<ulong>) this.varintList;
      }
    }

    public IList<uint> Fixed32List
    {
      get
      {
        return (IList<uint>) this.fixed32List;
      }
    }

    public IList<ulong> Fixed64List
    {
      get
      {
        return (IList<ulong>) this.fixed64List;
      }
    }

    public IList<ByteString> LengthDelimitedList
    {
      get
      {
        return (IList<ByteString>) this.lengthDelimitedList;
      }
    }

    public IList<UnknownFieldSet> GroupList
    {
      get
      {
        return (IList<UnknownFieldSet>) this.groupList;
      }
    }

    public override bool Equals(object other)
    {
      if (object.ReferenceEquals((object) this, other))
        return true;
      UnknownField unknownField = other as UnknownField;
      if (unknownField != null && Lists.Equals<ulong>((IList<ulong>) this.varintList, (IList<ulong>) unknownField.varintList) && (Lists.Equals<uint>((IList<uint>) this.fixed32List, (IList<uint>) unknownField.fixed32List) && Lists.Equals<ulong>((IList<ulong>) this.fixed64List, (IList<ulong>) unknownField.fixed64List)) && Lists.Equals<ByteString>((IList<ByteString>) this.lengthDelimitedList, (IList<ByteString>) unknownField.lengthDelimitedList))
        return Lists.Equals<UnknownFieldSet>((IList<UnknownFieldSet>) this.groupList, (IList<UnknownFieldSet>) unknownField.groupList);
      return false;
    }

    public override int GetHashCode()
    {
      return ((((43 * 47 + Lists.GetHashCode<ulong>((IList<ulong>) this.varintList)) * 47 + Lists.GetHashCode<uint>((IList<uint>) this.fixed32List)) * 47 + Lists.GetHashCode<ulong>((IList<ulong>) this.fixed64List)) * 47 + Lists.GetHashCode<ByteString>((IList<ByteString>) this.lengthDelimitedList)) * 47 + Lists.GetHashCode<UnknownFieldSet>((IList<UnknownFieldSet>) this.groupList);
    }

    public static UnknownField.Builder CreateBuilder()
    {
      return new UnknownField.Builder();
    }

    public static UnknownField.Builder CreateBuilder(UnknownField copyFrom)
    {
      return new UnknownField.Builder().MergeFrom(copyFrom);
    }

    public void WriteTo(int fieldNumber, ICodedOutputStream output)
    {
      foreach (ulong varint in this.varintList)
        output.WriteUnknownField(fieldNumber, WireFormat.WireType.Varint, varint);
      foreach (uint fixed32 in this.fixed32List)
        output.WriteUnknownField(fieldNumber, WireFormat.WireType.Fixed32, (ulong) fixed32);
      foreach (ulong fixed64 in this.fixed64List)
        output.WriteUnknownField(fieldNumber, WireFormat.WireType.Fixed64, fixed64);
      foreach (ByteString lengthDelimited in this.lengthDelimitedList)
        output.WriteUnknownBytes(fieldNumber, lengthDelimited);
      foreach (UnknownFieldSet group in this.groupList)
        output.WriteUnknownGroup(fieldNumber, (IMessageLite) group);
    }

    public int GetSerializedSize(int fieldNumber)
    {
      int num = 0;
      foreach (ulong varint in this.varintList)
        num += CodedOutputStream.ComputeUInt64Size(fieldNumber, varint);
      foreach (uint fixed32 in this.fixed32List)
        num += CodedOutputStream.ComputeFixed32Size(fieldNumber, fixed32);
      foreach (ulong fixed64 in this.fixed64List)
        num += CodedOutputStream.ComputeFixed64Size(fieldNumber, fixed64);
      foreach (ByteString lengthDelimited in this.lengthDelimitedList)
        num += CodedOutputStream.ComputeBytesSize(fieldNumber, lengthDelimited);
      foreach (UnknownFieldSet group in this.groupList)
        num += CodedOutputStream.ComputeUnknownGroupSize(fieldNumber, (IMessageLite) group);
      return num;
    }

    public void WriteAsMessageSetExtensionTo(int fieldNumber, ICodedOutputStream output)
    {
      foreach (ByteString lengthDelimited in this.lengthDelimitedList)
        output.WriteMessageSetExtension(fieldNumber, "unknown_field", lengthDelimited);
    }

    public int GetSerializedSizeAsMessageSetExtension(int fieldNumber)
    {
      int num = 0;
      foreach (ByteString lengthDelimited in this.lengthDelimitedList)
        num += CodedOutputStream.ComputeRawMessageSetExtensionSize(fieldNumber, lengthDelimited);
      return num;
    }

    public sealed class Builder
    {
      private List<ulong> varintList;
      private List<uint> fixed32List;
      private List<ulong> fixed64List;
      private List<ByteString> lengthDelimitedList;
      private List<UnknownFieldSet> groupList;

      public UnknownField Build()
      {
        return new UnknownField(UnknownField.Builder.MakeReadOnly<ulong>(ref this.varintList), UnknownField.Builder.MakeReadOnly<uint>(ref this.fixed32List), UnknownField.Builder.MakeReadOnly<ulong>(ref this.fixed64List), UnknownField.Builder.MakeReadOnly<ByteString>(ref this.lengthDelimitedList), UnknownField.Builder.MakeReadOnly<UnknownFieldSet>(ref this.groupList));
      }

      public UnknownField.Builder MergeFrom(UnknownField other)
      {
        this.varintList = UnknownField.Builder.AddAll<ulong>(this.varintList, other.VarintList);
        this.fixed32List = UnknownField.Builder.AddAll<uint>(this.fixed32List, other.Fixed32List);
        this.fixed64List = UnknownField.Builder.AddAll<ulong>(this.fixed64List, other.Fixed64List);
        this.lengthDelimitedList = UnknownField.Builder.AddAll<ByteString>(this.lengthDelimitedList, other.LengthDelimitedList);
        this.groupList = UnknownField.Builder.AddAll<UnknownFieldSet>(this.groupList, other.GroupList);
        return this;
      }

      private static List<T> AddAll<T>(List<T> current, IList<T> extras)
      {
        if (extras.Count == 0)
          return current;
        if (current == null)
          current = new List<T>((IEnumerable<T>) extras);
        else
          current.AddRange((IEnumerable<T>) extras);
        return current;
      }

      public UnknownField.Builder Clear()
      {
        this.varintList = (List<ulong>) null;
        this.fixed32List = (List<uint>) null;
        this.fixed64List = (List<ulong>) null;
        this.lengthDelimitedList = (List<ByteString>) null;
        this.groupList = (List<UnknownFieldSet>) null;
        return this;
      }

      [CLSCompliant(false)]
      public UnknownField.Builder AddVarint(ulong value)
      {
        this.varintList = UnknownField.Builder.Add<ulong>(this.varintList, value);
        return this;
      }

      [CLSCompliant(false)]
      public UnknownField.Builder AddFixed32(uint value)
      {
        this.fixed32List = UnknownField.Builder.Add<uint>(this.fixed32List, value);
        return this;
      }

      [CLSCompliant(false)]
      public UnknownField.Builder AddFixed64(ulong value)
      {
        this.fixed64List = UnknownField.Builder.Add<ulong>(this.fixed64List, value);
        return this;
      }

      public UnknownField.Builder AddLengthDelimited(ByteString value)
      {
        this.lengthDelimitedList = UnknownField.Builder.Add<ByteString>(this.lengthDelimitedList, value);
        return this;
      }

      public UnknownField.Builder AddGroup(UnknownFieldSet value)
      {
        this.groupList = UnknownField.Builder.Add<UnknownFieldSet>(this.groupList, value);
        return this;
      }

      private static List<T> Add<T>(List<T> list, T value)
      {
        if (list == null)
          list = new List<T>();
        list.Add(value);
        return list;
      }

      private static ReadOnlyCollection<T> MakeReadOnly<T>(ref List<T> list)
      {
        ReadOnlyCollection<T> readOnlyCollection = list == null ? Lists<T>.Empty : new ReadOnlyCollection<T>((IList<T>) list);
        list = (List<T>) null;
        return readOnlyCollection;
      }
    }
  }
}
