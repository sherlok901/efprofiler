﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.ExtensionRegistry
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers.Descriptors;
using System;
using System.Collections.Generic;

namespace Google.ProtocolBuffers
{
  public sealed class ExtensionRegistry
  {
    private static readonly ExtensionRegistry empty = new ExtensionRegistry(new Dictionary<object, Dictionary<string, IGeneratedExtensionLite>>(), new Dictionary<ExtensionRegistry.ExtensionIntPair, IGeneratedExtensionLite>(), true);
    private readonly Dictionary<object, Dictionary<string, IGeneratedExtensionLite>> extensionsByName;
    private readonly Dictionary<ExtensionRegistry.ExtensionIntPair, IGeneratedExtensionLite> extensionsByNumber;
    private readonly bool readOnly;

    [Obsolete("Please use the FindByName method instead.", true)]
    public ExtensionInfo this[string fullName]
    {
      get
      {
        foreach (IGeneratedExtensionLite generatedExtensionLite in this.extensionsByNumber.Values)
        {
          if (StringComparer.Ordinal.Equals(generatedExtensionLite.Descriptor.FullName, fullName))
            return generatedExtensionLite as ExtensionInfo;
        }
        return (ExtensionInfo) null;
      }
    }

    public ExtensionInfo this[MessageDescriptor containingType, int fieldNumber]
    {
      get
      {
        IGeneratedExtensionLite generatedExtensionLite;
        this.extensionsByNumber.TryGetValue(new ExtensionRegistry.ExtensionIntPair((object) containingType, fieldNumber), out generatedExtensionLite);
        return generatedExtensionLite as ExtensionInfo;
      }
    }

    public ExtensionInfo FindByName(MessageDescriptor containingType, string fieldName)
    {
      return this.FindExtensionByName((object) containingType, fieldName) as ExtensionInfo;
    }

    public void Add<TExtension>(GeneratedExtensionBase<TExtension> extension)
    {
      if (extension.Descriptor.MappedType == MappedType.Message)
        this.Add(new ExtensionInfo(extension.Descriptor, extension.MessageDefaultInstance));
      else
        this.Add(new ExtensionInfo(extension.Descriptor, (IMessageLite) null));
    }

    public void Add(FieldDescriptor type)
    {
      if (type.MappedType == MappedType.Message)
        throw new ArgumentException("ExtensionRegistry.Add() must be provided a default instance when adding an embedded message extension.");
      this.Add(new ExtensionInfo(type, (IMessageLite) null));
    }

    public void Add(FieldDescriptor type, IMessage defaultInstance)
    {
      if (type.MappedType != MappedType.Message)
        throw new ArgumentException("ExtensionRegistry.Add() provided a default instance for a non-message extension.");
      this.Add(new ExtensionInfo(type, (IMessageLite) defaultInstance));
    }

    private void Add(ExtensionInfo extension)
    {
      if (this.readOnly)
        throw new InvalidOperationException("Cannot add entries to a read-only extension registry");
      if (!extension.Descriptor.IsExtension)
        throw new ArgumentException("ExtensionRegistry.add() was given a FieldDescriptor for a regular (non-extension) field.");
      IGeneratedExtensionLite extension1 = (IGeneratedExtensionLite) extension;
      this.Add(extension1);
      FieldDescriptor descriptor = extension.Descriptor;
      Dictionary<string, IGeneratedExtensionLite> dictionary;
      if (!descriptor.ContainingType.Options.MessageSetWireFormat || descriptor.FieldType != FieldType.Message || (!descriptor.IsOptional || descriptor.ExtensionScope != descriptor.MessageType) || !this.extensionsByName.TryGetValue(extension1.ContainingType, out dictionary))
        return;
      dictionary[descriptor.MessageType.FullName] = (IGeneratedExtensionLite) extension;
    }

    private ExtensionRegistry(Dictionary<object, Dictionary<string, IGeneratedExtensionLite>> byName, Dictionary<ExtensionRegistry.ExtensionIntPair, IGeneratedExtensionLite> byNumber, bool readOnly)
    {
      this.extensionsByName = byName;
      this.extensionsByNumber = byNumber;
      this.readOnly = readOnly;
    }

    public static ExtensionRegistry CreateInstance()
    {
      return new ExtensionRegistry(new Dictionary<object, Dictionary<string, IGeneratedExtensionLite>>(), new Dictionary<ExtensionRegistry.ExtensionIntPair, IGeneratedExtensionLite>(), false);
    }

    public ExtensionRegistry AsReadOnly()
    {
      return new ExtensionRegistry(this.extensionsByName, this.extensionsByNumber, true);
    }

    public static ExtensionRegistry Empty
    {
      get
      {
        return ExtensionRegistry.empty;
      }
    }

    public IGeneratedExtensionLite this[IMessageLite containingType, int fieldNumber]
    {
      get
      {
        IGeneratedExtensionLite generatedExtensionLite;
        this.extensionsByNumber.TryGetValue(new ExtensionRegistry.ExtensionIntPair((object) containingType, fieldNumber), out generatedExtensionLite);
        return generatedExtensionLite;
      }
    }

    public IGeneratedExtensionLite FindByName(IMessageLite defaultInstanceOfType, string fieldName)
    {
      return this.FindExtensionByName((object) defaultInstanceOfType, fieldName);
    }

    private IGeneratedExtensionLite FindExtensionByName(object forwhat, string fieldName)
    {
      IGeneratedExtensionLite generatedExtensionLite = (IGeneratedExtensionLite) null;
      Dictionary<string, IGeneratedExtensionLite> dictionary;
      if (this.extensionsByName.TryGetValue(forwhat, out dictionary) && dictionary.TryGetValue(fieldName, out generatedExtensionLite))
        return generatedExtensionLite;
      return (IGeneratedExtensionLite) null;
    }

    public void Add(IGeneratedExtensionLite extension)
    {
      if (this.readOnly)
        throw new InvalidOperationException("Cannot add entries to a read-only extension registry");
      this.extensionsByNumber.Add(new ExtensionRegistry.ExtensionIntPair(extension.ContainingType, extension.Number), extension);
      Dictionary<string, IGeneratedExtensionLite> dictionary;
      if (!this.extensionsByName.TryGetValue(extension.ContainingType, out dictionary))
        this.extensionsByName.Add(extension.ContainingType, dictionary = new Dictionary<string, IGeneratedExtensionLite>());
      dictionary[extension.Descriptor.Name] = extension;
      dictionary[extension.Descriptor.FullName] = extension;
    }

    internal struct ExtensionIntPair : IEquatable<ExtensionRegistry.ExtensionIntPair>
    {
      private readonly object msgType;
      private readonly int number;

      internal ExtensionIntPair(object msgType, int number)
      {
        this.msgType = msgType;
        this.number = number;
      }

      public override int GetHashCode()
      {
        return this.msgType.GetHashCode() * (int) ushort.MaxValue + this.number;
      }

      public override bool Equals(object obj)
      {
        if (!(obj is ExtensionRegistry.ExtensionIntPair))
          return false;
        return this.Equals((ExtensionRegistry.ExtensionIntPair) obj);
      }

      public bool Equals(ExtensionRegistry.ExtensionIntPair other)
      {
        if (this.msgType.Equals(other.msgType))
          return this.number == other.number;
        return false;
      }
    }
  }
}
