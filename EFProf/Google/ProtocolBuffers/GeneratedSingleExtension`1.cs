﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.GeneratedSingleExtension`1
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers.Descriptors;
using System;

namespace Google.ProtocolBuffers
{
  public sealed class GeneratedSingleExtension<TExtension> : GeneratedExtensionBase<TExtension>
  {
    internal GeneratedSingleExtension(FieldDescriptor descriptor)
      : base(descriptor, typeof (TExtension))
    {
    }

    public static GeneratedSingleExtension<TExtension> CreateInstance(FieldDescriptor descriptor)
    {
      if (descriptor.IsRepeated)
        throw new ArgumentException("Must call GeneratedRepeateExtension.CreateInstance() for repeated types.");
      return new GeneratedSingleExtension<TExtension>(descriptor);
    }

    public override object FromReflectionType(object value)
    {
      return this.SingularFromReflectionType(value);
    }
  }
}
