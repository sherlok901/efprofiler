﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.ByteArray
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using System;

namespace Google.ProtocolBuffers
{
  internal static class ByteArray
  {
    private const int CopyThreshold = 12;

    public static void Copy(byte[] src, int srcOffset, byte[] dst, int dstOffset, int count)
    {
      if (count > 12)
        Buffer.BlockCopy((Array) src, srcOffset, (Array) dst, dstOffset, count);
      else
        ByteArray.ByteCopy(src, srcOffset, dst, dstOffset, count);
    }

    public static void ByteCopy(byte[] src, int srcOffset, byte[] dst, int dstOffset, int count)
    {
      int num = srcOffset + count;
      for (int index = srcOffset; index < num; ++index)
        dst[dstOffset++] = src[index];
    }

    public static void Reverse(byte[] bytes)
    {
      int index1 = 0;
      for (int index2 = bytes.Length - 1; index1 < index2; --index2)
      {
        byte num = bytes[index1];
        bytes[index1] = bytes[index2];
        bytes[index2] = num;
        ++index1;
      }
    }
  }
}
