﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.AbstractBuilder`2
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers.Descriptors;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Google.ProtocolBuffers
{
  [Serializable]
  public abstract class AbstractBuilder<TMessage, TBuilder> : AbstractBuilderLite<TMessage, TBuilder>, IBuilder<TMessage, TBuilder>, IBuilder, IBuilderLite<TMessage, TBuilder>, IBuilderLite where TMessage : AbstractMessage<TMessage, TBuilder> where TBuilder : AbstractBuilder<TMessage, TBuilder>
  {
    public abstract UnknownFieldSet UnknownFields { get; set; }

    public abstract IDictionary<FieldDescriptor, object> AllFields { get; }

    public abstract object this[FieldDescriptor field] { get; set; }

    public abstract MessageDescriptor DescriptorForType { get; }

    public abstract int GetRepeatedFieldCount(FieldDescriptor field);

    public abstract object this[FieldDescriptor field, int index] { get; set; }

    public abstract bool HasField(FieldDescriptor field);

    public abstract IBuilder CreateBuilderForField(FieldDescriptor field);

    public abstract TBuilder ClearField(FieldDescriptor field);

    public abstract TBuilder AddRepeatedField(FieldDescriptor field, object value);

    public TBuilder SetUnknownFields(UnknownFieldSet fields)
    {
      this.UnknownFields = fields;
      return this.ThisBuilder;
    }

    public override TBuilder Clear()
    {
      foreach (FieldDescriptor key in (IEnumerable<FieldDescriptor>) this.AllFields.Keys)
        this.ClearField(key);
      return this.ThisBuilder;
    }

    public override sealed TBuilder MergeFrom(IMessageLite other)
    {
      if (other is IMessage)
        return this.MergeFrom((IMessage) other);
      throw new ArgumentException("MergeFrom(Message) can only merge messages of the same type.");
    }

    public abstract TBuilder MergeFrom(TMessage other);

    public virtual TBuilder MergeFrom(IMessage other)
    {
      if (other.DescriptorForType != this.DescriptorForType)
        throw new ArgumentException("MergeFrom(IMessage) can only merge messages of the same type.");
      foreach (KeyValuePair<FieldDescriptor, object> allField in (IEnumerable<KeyValuePair<FieldDescriptor, object>>) other.AllFields)
      {
        FieldDescriptor key = allField.Key;
        if (key.IsRepeated)
        {
          foreach (object obj in (IEnumerable) allField.Value)
            this.AddRepeatedField(key, obj);
        }
        else if (key.MappedType == MappedType.Message)
        {
          IMessageLite message = (IMessageLite) this[key];
          this[key] = message != message.WeakDefaultInstanceForType ? (object) message.WeakCreateBuilderForType().WeakMergeFrom(message).WeakMergeFrom((IMessageLite) allField.Value).WeakBuild() : allField.Value;
        }
        else
          this[key] = allField.Value;
      }
      this.MergeUnknownFields(other.UnknownFields);
      return this.ThisBuilder;
    }

    public override TBuilder MergeFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
    {
      UnknownFieldSet.Builder builder = UnknownFieldSet.CreateBuilder(this.UnknownFields);
      builder.MergeFrom(input, extensionRegistry, (IBuilder) this);
      this.UnknownFields = builder.Build();
      return this.ThisBuilder;
    }

    public virtual TBuilder MergeUnknownFields(UnknownFieldSet unknownFields)
    {
      this.UnknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields).MergeFrom(unknownFields).Build();
      return this.ThisBuilder;
    }

    public virtual IBuilder SetField(FieldDescriptor field, object value)
    {
      this[field] = value;
      return (IBuilder) this.ThisBuilder;
    }

    public virtual IBuilder SetRepeatedField(FieldDescriptor field, int index, object value)
    {
      this[field, index] = value;
      return (IBuilder) this.ThisBuilder;
    }

    IMessage IBuilder.WeakBuild()
    {
      return (IMessage) this.Build();
    }

    IBuilder IBuilder.WeakAddRepeatedField(FieldDescriptor field, object value)
    {
      return (IBuilder) this.AddRepeatedField(field, value);
    }

    IBuilder IBuilder.WeakClear()
    {
      return (IBuilder) this.Clear();
    }

    IBuilder IBuilder.WeakMergeFrom(IMessage message)
    {
      return (IBuilder) this.MergeFrom(message);
    }

    IBuilder IBuilder.WeakMergeFrom(ICodedInputStream input)
    {
      return (IBuilder) this.MergeFrom(input);
    }

    IBuilder IBuilder.WeakMergeFrom(ICodedInputStream input, ExtensionRegistry registry)
    {
      return (IBuilder) this.MergeFrom(input, registry);
    }

    IBuilder IBuilder.WeakMergeFrom(ByteString data)
    {
      return (IBuilder) this.MergeFrom(data);
    }

    IBuilder IBuilder.WeakMergeFrom(ByteString data, ExtensionRegistry registry)
    {
      return (IBuilder) this.MergeFrom(data, registry);
    }

    IMessage IBuilder.WeakBuildPartial()
    {
      return (IMessage) this.BuildPartial();
    }

    IBuilder IBuilder.WeakClone()
    {
      return (IBuilder) this.Clone();
    }

    IMessage IBuilder.WeakDefaultInstanceForType
    {
      get
      {
        return (IMessage) this.DefaultInstanceForType;
      }
    }

    IBuilder IBuilder.WeakClearField(FieldDescriptor field)
    {
      return (IBuilder) this.ClearField(field);
    }

    public override string ToString()
    {
      return TextFormat.PrintToString((IBuilder) this);
    }
  }
}
