﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.IBuilderLite
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

namespace Google.ProtocolBuffers
{
  public interface IBuilderLite
  {
    bool IsInitialized { get; }

    IBuilderLite WeakClear();

    IBuilderLite WeakMergeFrom(IMessageLite message);

    IBuilderLite WeakMergeFrom(ByteString data);

    IBuilderLite WeakMergeFrom(ByteString data, ExtensionRegistry registry);

    IBuilderLite WeakMergeFrom(ICodedInputStream input);

    IBuilderLite WeakMergeFrom(ICodedInputStream input, ExtensionRegistry registry);

    IMessageLite WeakBuild();

    IMessageLite WeakBuildPartial();

    IBuilderLite WeakClone();

    IMessageLite WeakDefaultInstanceForType { get; }
  }
}
