﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.ExtensionInfo
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers.Descriptors;

namespace Google.ProtocolBuffers
{
  public sealed class ExtensionInfo : IGeneratedExtensionLite
  {
    public FieldDescriptor Descriptor { get; private set; }

    IFieldDescriptorLite IGeneratedExtensionLite.Descriptor
    {
      get
      {
        return (IFieldDescriptorLite) this.Descriptor;
      }
    }

    public IMessageLite DefaultInstance { get; private set; }

    internal ExtensionInfo(FieldDescriptor descriptor)
      : this(descriptor, (IMessageLite) null)
    {
    }

    internal ExtensionInfo(FieldDescriptor descriptor, IMessageLite defaultInstance)
    {
      this.Descriptor = descriptor;
      this.DefaultInstance = defaultInstance;
    }

    int IGeneratedExtensionLite.Number
    {
      get
      {
        return this.Descriptor.FieldNumber;
      }
    }

    object IGeneratedExtensionLite.ContainingType
    {
      get
      {
        return (object) this.Descriptor.ContainingType;
      }
    }

    IMessageLite IGeneratedExtensionLite.MessageDefaultInstance
    {
      get
      {
        return this.DefaultInstance;
      }
    }
  }
}
