﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.TextGenerator
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using System;
using System.IO;
using System.Text;

namespace Google.ProtocolBuffers
{
  public sealed class TextGenerator
  {
    private bool atStartOfLine = true;
    private readonly StringBuilder indent = new StringBuilder();
    private readonly string lineBreak;
    private readonly TextWriter writer;

    public TextGenerator(TextWriter writer, string lineBreak)
    {
      this.writer = writer;
      this.lineBreak = lineBreak;
    }

    public void Indent()
    {
      this.indent.Append("  ");
    }

    public void Outdent()
    {
      if (this.indent.Length == 0)
        throw new InvalidOperationException("Too many calls to Outdent()");
      this.indent.Length -= 2;
    }

    public void WriteLine(string text)
    {
      this.Print(text);
      this.Print("\n");
    }

    public void WriteLine(string format, params object[] args)
    {
      this.WriteLine(string.Format(format, args));
    }

    public void WriteLine()
    {
      this.WriteLine("");
    }

    public void Print(string text)
    {
      int startIndex = 0;
      for (int index = 0; index < text.Length; ++index)
      {
        if ((int) text[index] == 10)
        {
          this.Write(text.Substring(startIndex, index - startIndex));
          this.Write(this.lineBreak);
          startIndex = index + 1;
          this.atStartOfLine = true;
        }
      }
      this.Write(text.Substring(startIndex));
    }

    public void Write(string format, params object[] args)
    {
      this.Write(string.Format(format, args));
    }

    private void Write(string data)
    {
      if (data.Length == 0)
        return;
      if (this.atStartOfLine)
      {
        this.atStartOfLine = false;
        this.writer.Write((object) this.indent);
      }
      this.writer.Write(data);
    }
  }
}
