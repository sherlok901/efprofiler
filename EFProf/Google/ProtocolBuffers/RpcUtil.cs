﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.RpcUtil
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using System;

namespace Google.ProtocolBuffers
{
  public static class RpcUtil
  {
    public static Action<T> SpecializeCallback<T>(Action<IMessage> action) where T : IMessage<T>
    {
      return (Action<T>) (message => action((IMessage) message));
    }

    public static Action<IMessage> GeneralizeCallback<TMessage, TBuilder>(Action<TMessage> action, TMessage defaultInstance) where TMessage : class, IMessage<TMessage, TBuilder> where TBuilder : IBuilder<TMessage, TBuilder>
    {
      return (Action<IMessage>) (message =>
      {
        TMessage message1 = message as TMessage;
        if ((object) message1 == null)
          message1 = defaultInstance.CreateBuilderForType().MergeFrom(message).Build();
        action(message1);
      });
    }
  }
}
