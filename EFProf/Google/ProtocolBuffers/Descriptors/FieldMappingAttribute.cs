﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.Descriptors.FieldMappingAttribute
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers.Collections;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace Google.ProtocolBuffers.Descriptors
{
  [CLSCompliant(false)]
  [AttributeUsage(AttributeTargets.Field)]
  public sealed class FieldMappingAttribute : Attribute
  {
    private static readonly IDictionary<FieldType, FieldMappingAttribute> FieldTypeToMappedTypeMap = FieldMappingAttribute.MapFieldTypes();

    public FieldMappingAttribute(MappedType mappedType, WireFormat.WireType wireType)
    {
      this.MappedType = mappedType;
      this.WireType = wireType;
    }

    public MappedType MappedType { get; private set; }

    public WireFormat.WireType WireType { get; private set; }

    private static IDictionary<FieldType, FieldMappingAttribute> MapFieldTypes()
    {
      Dictionary<FieldType, FieldMappingAttribute> dictionary = new Dictionary<FieldType, FieldMappingAttribute>();
      foreach (FieldInfo field in typeof (FieldType).GetFields(BindingFlags.Static | BindingFlags.Public))
      {
        FieldType index = (FieldType) field.GetValue((object) null);
        FieldMappingAttribute customAttribute = (FieldMappingAttribute) field.GetCustomAttributes(typeof (FieldMappingAttribute), false)[0];
        dictionary[index] = customAttribute;
      }
      return Dictionaries.AsReadOnly<FieldType, FieldMappingAttribute>((IDictionary<FieldType, FieldMappingAttribute>) dictionary);
    }

    internal static MappedType MappedTypeFromFieldType(FieldType type)
    {
      return FieldMappingAttribute.FieldTypeToMappedTypeMap[type].MappedType;
    }

    internal static WireFormat.WireType WireTypeFromFieldType(FieldType type, bool packed)
    {
      if (!packed)
        return FieldMappingAttribute.FieldTypeToMappedTypeMap[type].WireType;
      return WireFormat.WireType.LengthDelimited;
    }
  }
}
