﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.Descriptors.IndexedDescriptorBase`2
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers.DescriptorProtos;

namespace Google.ProtocolBuffers.Descriptors
{
  public abstract class IndexedDescriptorBase<TProto, TOptions> : DescriptorBase<TProto, TOptions> where TProto : IMessage<TProto>, IDescriptorProto<TOptions>
  {
    private readonly int index;

    protected IndexedDescriptorBase(TProto proto, FileDescriptor file, string fullName, int index)
      : base(proto, file, fullName)
    {
      this.index = index;
    }

    public int Index
    {
      get
      {
        return this.index;
      }
    }
  }
}
