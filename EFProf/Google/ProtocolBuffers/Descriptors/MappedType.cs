﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.Descriptors.MappedType
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

namespace Google.ProtocolBuffers.Descriptors
{
  public enum MappedType
  {
    Int32,
    Int64,
    UInt32,
    UInt64,
    Single,
    Double,
    Boolean,
    String,
    ByteString,
    Message,
    Enum,
  }
}
