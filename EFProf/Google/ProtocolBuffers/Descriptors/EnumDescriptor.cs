﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.Descriptors.EnumDescriptor
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers.DescriptorProtos;
using System.Collections.Generic;

namespace Google.ProtocolBuffers.Descriptors
{
  public sealed class EnumDescriptor : IndexedDescriptorBase<EnumDescriptorProto, EnumOptions>, IEnumLiteMap<EnumValueDescriptor>, IEnumLiteMap
  {
    private readonly MessageDescriptor containingType;
    private readonly IList<EnumValueDescriptor> values;

    internal EnumDescriptor(EnumDescriptorProto proto, FileDescriptor file, MessageDescriptor parent, int index)
      : base(proto, file, DescriptorBase<EnumDescriptorProto, EnumOptions>.ComputeFullName(file, parent, proto.Name), index)
    {
      EnumDescriptor parent1 = this;
      this.containingType = parent;
      if (proto.ValueCount == 0)
        throw new DescriptorValidationException((IDescriptor) this, "Enums must contain at least one value.");
      this.values = DescriptorUtil.ConvertAndMakeReadOnly<EnumValueDescriptorProto, EnumValueDescriptor>(proto.ValueList, (DescriptorUtil.IndexedConverter<EnumValueDescriptorProto, EnumValueDescriptor>) ((value, i) => new EnumValueDescriptor(value, file, parent1, i)));
      this.File.DescriptorPool.AddSymbol((IDescriptor) this);
    }

    public MessageDescriptor ContainingType
    {
      get
      {
        return this.containingType;
      }
    }

    public IList<EnumValueDescriptor> Values
    {
      get
      {
        return this.values;
      }
    }

    public bool IsValidValue(IEnumLite value)
    {
      if (value is EnumValueDescriptor)
        return ((EnumValueDescriptor) value).EnumDescriptor == this;
      return false;
    }

    public EnumValueDescriptor FindValueByNumber(int number)
    {
      return this.File.DescriptorPool.FindEnumValueByNumber(this, number);
    }

    IEnumLite IEnumLiteMap.FindValueByNumber(int number)
    {
      return (IEnumLite) this.FindValueByNumber(number);
    }

    IEnumLite IEnumLiteMap.FindValueByName(string name)
    {
      return (IEnumLite) this.FindValueByName(name);
    }

    public EnumValueDescriptor FindValueByName(string name)
    {
      return this.File.DescriptorPool.FindSymbol<EnumValueDescriptor>(this.FullName + "." + name);
    }

    internal override void ReplaceProto(EnumDescriptorProto newProto)
    {
      base.ReplaceProto(newProto);
      for (int index = 0; index < this.values.Count; ++index)
        this.values[index].ReplaceProto(newProto.GetValue(index));
    }
  }
}
