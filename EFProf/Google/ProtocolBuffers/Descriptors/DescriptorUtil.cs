﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.Descriptors.DescriptorUtil
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers.Collections;
using System.Collections.Generic;

namespace Google.ProtocolBuffers.Descriptors
{
  internal static class DescriptorUtil
  {
    internal static IList<TOutput> ConvertAndMakeReadOnly<TInput, TOutput>(IList<TInput> input, DescriptorUtil.IndexedConverter<TInput, TOutput> converter)
    {
      TOutput[] outputArray = new TOutput[input.Count];
      for (int index = 0; index < outputArray.Length; ++index)
        outputArray[index] = converter(input[index], index);
      return Lists<TOutput>.AsReadOnly((IList<TOutput>) outputArray);
    }

    internal delegate TOutput IndexedConverter<TInput, TOutput>(TInput element, int index);
  }
}
