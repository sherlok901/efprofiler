﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.Descriptors.PackageDescriptor
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

namespace Google.ProtocolBuffers.Descriptors
{
  internal sealed class PackageDescriptor : IDescriptor<IMessage>, IDescriptor
  {
    private readonly string name;
    private readonly string fullName;
    private readonly FileDescriptor file;

    internal PackageDescriptor(string name, string fullName, FileDescriptor file)
    {
      this.file = file;
      this.fullName = fullName;
      this.name = name;
    }

    public IMessage Proto
    {
      get
      {
        return (IMessage) this.file.Proto;
      }
    }

    public string Name
    {
      get
      {
        return this.name;
      }
    }

    public string FullName
    {
      get
      {
        return this.fullName;
      }
    }

    public FileDescriptor File
    {
      get
      {
        return this.file;
      }
    }
  }
}
