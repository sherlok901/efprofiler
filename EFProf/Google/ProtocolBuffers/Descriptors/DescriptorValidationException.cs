﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.Descriptors.DescriptorValidationException
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using System;

namespace Google.ProtocolBuffers.Descriptors
{
  public sealed class DescriptorValidationException : Exception
  {
    private readonly string name;
    private readonly IMessage proto;
    private readonly string description;

    public string ProblemSymbolName
    {
      get
      {
        return this.name;
      }
    }

    public IMessage ProblemProto
    {
      get
      {
        return this.proto;
      }
    }

    public string Description
    {
      get
      {
        return this.description;
      }
    }

    internal DescriptorValidationException(IDescriptor problemDescriptor, string description)
      : base(problemDescriptor.FullName + ": " + description)
    {
      this.name = problemDescriptor.FullName;
      this.proto = problemDescriptor.Proto;
      this.description = description;
    }

    internal DescriptorValidationException(IDescriptor problemDescriptor, string description, Exception cause)
      : base(problemDescriptor.FullName + ": " + description, cause)
    {
      this.name = problemDescriptor.FullName;
      this.proto = problemDescriptor.Proto;
      this.description = description;
    }
  }
}
