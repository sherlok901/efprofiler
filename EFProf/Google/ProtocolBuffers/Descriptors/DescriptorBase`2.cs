﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.Descriptors.DescriptorBase`2
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers.DescriptorProtos;

namespace Google.ProtocolBuffers.Descriptors
{
  public abstract class DescriptorBase<TProto, TOptions> : IDescriptor<TProto>, IDescriptor where TProto : IMessage, IDescriptorProto<TOptions>
  {
    private TProto proto;
    private readonly FileDescriptor file;
    private readonly string fullName;

    protected DescriptorBase(TProto proto, FileDescriptor file, string fullName)
    {
      this.proto = proto;
      this.file = file;
      this.fullName = fullName;
    }

    internal virtual void ReplaceProto(TProto newProto)
    {
      this.proto = newProto;
    }

    protected static string ComputeFullName(FileDescriptor file, MessageDescriptor parent, string name)
    {
      if (parent != null)
        return parent.FullName + "." + name;
      if (file.Package.Length > 0)
        return file.Package + "." + name;
      return name;
    }

    IMessage IDescriptor.Proto
    {
      get
      {
        return (IMessage) this.proto;
      }
    }

    public TProto Proto
    {
      get
      {
        return this.proto;
      }
    }

    public TOptions Options
    {
      get
      {
        return this.proto.Options;
      }
    }

    public string FullName
    {
      get
      {
        return this.fullName;
      }
    }

    public string Name
    {
      get
      {
        return this.proto.Name;
      }
    }

    public FileDescriptor File
    {
      get
      {
        return this.file;
      }
    }
  }
}
