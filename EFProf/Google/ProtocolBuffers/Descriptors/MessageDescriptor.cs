﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.Descriptors.MessageDescriptor
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers.DescriptorProtos;
using System.Collections.Generic;

namespace Google.ProtocolBuffers.Descriptors
{
  public sealed class MessageDescriptor : IndexedDescriptorBase<DescriptorProto, MessageOptions>
  {
    private readonly MessageDescriptor containingType;
    private readonly IList<MessageDescriptor> nestedTypes;
    private readonly IList<EnumDescriptor> enumTypes;
    private readonly IList<FieldDescriptor> fields;
    private readonly IList<FieldDescriptor> extensions;
    private bool hasRequiredFields;

    internal MessageDescriptor(DescriptorProto proto, FileDescriptor file, MessageDescriptor parent, int typeIndex)
      : base(proto, file, DescriptorBase<DescriptorProto, MessageOptions>.ComputeFullName(file, parent, proto.Name), typeIndex)
    {
      MessageDescriptor parent1 = this;
      this.containingType = parent;
      this.nestedTypes = DescriptorUtil.ConvertAndMakeReadOnly<DescriptorProto, MessageDescriptor>(proto.NestedTypeList, (DescriptorUtil.IndexedConverter<DescriptorProto, MessageDescriptor>) ((type, index) => new MessageDescriptor(type, file, parent1, index)));
      this.enumTypes = DescriptorUtil.ConvertAndMakeReadOnly<EnumDescriptorProto, EnumDescriptor>(proto.EnumTypeList, (DescriptorUtil.IndexedConverter<EnumDescriptorProto, EnumDescriptor>) ((type, index) => new EnumDescriptor(type, file, parent1, index)));
      this.fields = DescriptorUtil.ConvertAndMakeReadOnly<FieldDescriptorProto, FieldDescriptor>(proto.FieldList, (DescriptorUtil.IndexedConverter<FieldDescriptorProto, FieldDescriptor>) ((field, index) => new FieldDescriptor(field, file, parent1, index, false)));
      this.extensions = DescriptorUtil.ConvertAndMakeReadOnly<FieldDescriptorProto, FieldDescriptor>(proto.ExtensionList, (DescriptorUtil.IndexedConverter<FieldDescriptorProto, FieldDescriptor>) ((field, index) => new FieldDescriptor(field, file, parent1, index, true)));
      file.DescriptorPool.AddSymbol((IDescriptor) this);
    }

    public MessageDescriptor ContainingType
    {
      get
      {
        return this.containingType;
      }
    }

    public IList<FieldDescriptor> Fields
    {
      get
      {
        return this.fields;
      }
    }

    public IList<FieldDescriptor> Extensions
    {
      get
      {
        return this.extensions;
      }
    }

    public IList<MessageDescriptor> NestedTypes
    {
      get
      {
        return this.nestedTypes;
      }
    }

    public IList<EnumDescriptor> EnumTypes
    {
      get
      {
        return this.enumTypes;
      }
    }

    internal bool HasRequiredFields
    {
      get
      {
        return this.hasRequiredFields;
      }
    }

    public bool IsExtensionNumber(int number)
    {
      foreach (DescriptorProto.Types.ExtensionRange extensionRange in (IEnumerable<DescriptorProto.Types.ExtensionRange>) this.Proto.ExtensionRangeList)
      {
        if (extensionRange.Start <= number && number < extensionRange.End)
          return true;
      }
      return false;
    }

    public FieldDescriptor FindFieldByName(string name)
    {
      return this.File.DescriptorPool.FindSymbol<FieldDescriptor>(this.FullName + "." + name);
    }

    public FieldDescriptor FindFieldByNumber(int number)
    {
      return this.File.DescriptorPool.FindFieldByNumber(this, number);
    }

    public FieldDescriptor FindFieldByPropertyName(string propertyName)
    {
      foreach (FieldDescriptor field in (IEnumerable<FieldDescriptor>) this.Fields)
      {
        if (field.CSharpOptions.PropertyName == propertyName)
          return field;
      }
      return (FieldDescriptor) null;
    }

    public T FindDescriptor<T>(string name) where T : class, IDescriptor
    {
      return this.File.DescriptorPool.FindSymbol<T>(this.FullName + "." + name);
    }

    internal void CrossLink()
    {
      foreach (MessageDescriptor nestedType in (IEnumerable<MessageDescriptor>) this.nestedTypes)
        nestedType.CrossLink();
      foreach (FieldDescriptor field in (IEnumerable<FieldDescriptor>) this.fields)
        field.CrossLink();
      foreach (FieldDescriptor extension in (IEnumerable<FieldDescriptor>) this.extensions)
        extension.CrossLink();
    }

    internal void CheckRequiredFields()
    {
      this.hasRequiredFields = this.CheckRequiredFields((IDictionary<MessageDescriptor, byte>) new Dictionary<MessageDescriptor, byte>());
    }

    private bool CheckRequiredFields(IDictionary<MessageDescriptor, byte> alreadySeen)
    {
      if (alreadySeen.ContainsKey(this))
        return false;
      alreadySeen[this] = (byte) 0;
      if (this.Proto.ExtensionRangeCount != 0)
        return true;
      foreach (FieldDescriptor field in (IEnumerable<FieldDescriptor>) this.Fields)
      {
        if (field.IsRequired || field.MappedType == MappedType.Message && field.MessageType.CheckRequiredFields(alreadySeen))
          return true;
      }
      return false;
    }

    internal override void ReplaceProto(DescriptorProto newProto)
    {
      base.ReplaceProto(newProto);
      for (int index = 0; index < this.nestedTypes.Count; ++index)
        this.nestedTypes[index].ReplaceProto(newProto.GetNestedType(index));
      for (int index = 0; index < this.enumTypes.Count; ++index)
        this.enumTypes[index].ReplaceProto(newProto.GetEnumType(index));
      for (int index = 0; index < this.fields.Count; ++index)
        this.fields[index].ReplaceProto(newProto.GetField(index));
      for (int index = 0; index < this.extensions.Count; ++index)
        this.extensions[index].ReplaceProto(newProto.GetExtension(index));
    }
  }
}
