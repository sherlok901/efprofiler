﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.Descriptors.EnumValueDescriptor
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers.DescriptorProtos;

namespace Google.ProtocolBuffers.Descriptors
{
  public sealed class EnumValueDescriptor : IndexedDescriptorBase<EnumValueDescriptorProto, EnumValueOptions>, IEnumLite
  {
    private readonly EnumDescriptor enumDescriptor;

    internal EnumValueDescriptor(EnumValueDescriptorProto proto, FileDescriptor file, EnumDescriptor parent, int index)
      : base(proto, file, parent.FullName + "." + proto.Name, index)
    {
      this.enumDescriptor = parent;
      file.DescriptorPool.AddSymbol((IDescriptor) this);
      file.DescriptorPool.AddEnumValueByNumber(this);
    }

    public int Number
    {
      get
      {
        return this.Proto.Number;
      }
    }

    public EnumDescriptor EnumDescriptor
    {
      get
      {
        return this.enumDescriptor;
      }
    }
  }
}
