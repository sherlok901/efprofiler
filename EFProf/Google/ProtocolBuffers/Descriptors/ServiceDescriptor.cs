﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.Descriptors.ServiceDescriptor
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers.DescriptorProtos;
using System.Collections.Generic;

namespace Google.ProtocolBuffers.Descriptors
{
  public sealed class ServiceDescriptor : IndexedDescriptorBase<ServiceDescriptorProto, ServiceOptions>
  {
    private readonly IList<MethodDescriptor> methods;

    public ServiceDescriptor(ServiceDescriptorProto proto, FileDescriptor file, int index)
      : base(proto, file, DescriptorBase<ServiceDescriptorProto, ServiceOptions>.ComputeFullName(file, (MessageDescriptor) null, proto.Name), index)
    {
      ServiceDescriptor parent = this;
      this.methods = DescriptorUtil.ConvertAndMakeReadOnly<MethodDescriptorProto, MethodDescriptor>(proto.MethodList, (DescriptorUtil.IndexedConverter<MethodDescriptorProto, MethodDescriptor>) ((method, i) => new MethodDescriptor(method, file, parent, i)));
      file.DescriptorPool.AddSymbol((IDescriptor) this);
    }

    public IList<MethodDescriptor> Methods
    {
      get
      {
        return this.methods;
      }
    }

    public MethodDescriptor FindMethodByName(string name)
    {
      return this.File.DescriptorPool.FindSymbol<MethodDescriptor>(this.FullName + "." + name);
    }

    internal void CrossLink()
    {
      foreach (MethodDescriptor method in (IEnumerable<MethodDescriptor>) this.methods)
        method.CrossLink();
    }

    internal override void ReplaceProto(ServiceDescriptorProto newProto)
    {
      base.ReplaceProto(newProto);
      for (int index = 0; index < this.methods.Count; ++index)
        this.methods[index].ReplaceProto(newProto.GetMethod(index));
    }
  }
}
