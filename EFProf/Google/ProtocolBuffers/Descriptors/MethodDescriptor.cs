﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.Descriptors.MethodDescriptor
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers.DescriptorProtos;

namespace Google.ProtocolBuffers.Descriptors
{
  public sealed class MethodDescriptor : IndexedDescriptorBase<MethodDescriptorProto, MethodOptions>
  {
    private readonly ServiceDescriptor service;
    private MessageDescriptor inputType;
    private MessageDescriptor outputType;

    public ServiceDescriptor Service
    {
      get
      {
        return this.service;
      }
    }

    public MessageDescriptor InputType
    {
      get
      {
        return this.inputType;
      }
    }

    public MessageDescriptor OutputType
    {
      get
      {
        return this.outputType;
      }
    }

    internal MethodDescriptor(MethodDescriptorProto proto, FileDescriptor file, ServiceDescriptor parent, int index)
      : base(proto, file, parent.FullName + "." + proto.Name, index)
    {
      this.service = parent;
      file.DescriptorPool.AddSymbol((IDescriptor) this);
    }

    internal void CrossLink()
    {
      IDescriptor descriptor1 = this.File.DescriptorPool.LookupSymbol(this.Proto.InputType, (IDescriptor) this);
      if (!(descriptor1 is MessageDescriptor))
        throw new DescriptorValidationException((IDescriptor) this, "\"" + this.Proto.InputType + "\" is not a message type.");
      this.inputType = (MessageDescriptor) descriptor1;
      IDescriptor descriptor2 = this.File.DescriptorPool.LookupSymbol(this.Proto.OutputType, (IDescriptor) this);
      if (!(descriptor2 is MessageDescriptor))
        throw new DescriptorValidationException((IDescriptor) this, "\"" + this.Proto.OutputType + "\" is not a message type.");
      this.outputType = (MessageDescriptor) descriptor2;
    }
  }
}
