﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.GeneratedRepeatExtensionLite`2
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers.Descriptors;
using System.Collections;
using System.Collections.Generic;

namespace Google.ProtocolBuffers
{
  public class GeneratedRepeatExtensionLite<TContainingType, TExtensionType> : GeneratedExtensionLite<TContainingType, IList<TExtensionType>> where TContainingType : IMessageLite
  {
    public GeneratedRepeatExtensionLite(string fullName, TContainingType containingTypeDefaultInstance, IMessageLite messageDefaultInstance, IEnumLiteMap enumTypeMap, int number, FieldType type, bool isPacked)
      : base(fullName, containingTypeDefaultInstance, (IList<TExtensionType>) new List<TExtensionType>(), messageDefaultInstance, enumTypeMap, number, type, isPacked)
    {
    }

    public override object ToReflectionType(object value)
    {
      IList<object> objectList = (IList<object>) new List<object>();
      foreach (object obj in (IEnumerable) value)
        objectList.Add(this.SingularToReflectionType(obj));
      return (object) objectList;
    }

    public override object FromReflectionType(object value)
    {
      List<TExtensionType> extensionTypeList = new List<TExtensionType>();
      foreach (object obj in (IEnumerable) value)
        extensionTypeList.Add((TExtensionType) this.SingularFromReflectionType(obj));
      return (object) extensionTypeList;
    }
  }
}
