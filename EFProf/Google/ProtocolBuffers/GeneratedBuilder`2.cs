﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.GeneratedBuilder`2
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers.Descriptors;
using Google.ProtocolBuffers.FieldAccess;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Google.ProtocolBuffers
{
  [Serializable]
  public abstract class GeneratedBuilder<TMessage, TBuilder> : AbstractBuilder<TMessage, TBuilder> where TMessage : GeneratedMessage<TMessage, TBuilder> where TBuilder : GeneratedBuilder<TMessage, TBuilder>, new()
  {
    protected abstract TMessage MessageBeingBuilt { get; }

    protected internal FieldAccessorTable<TMessage, TBuilder> InternalFieldAccessors
    {
      get
      {
        return this.DefaultInstanceForType.FieldAccessorsFromBuilder;
      }
    }

    public override IDictionary<FieldDescriptor, object> AllFields
    {
      get
      {
        return this.MessageBeingBuilt.AllFields;
      }
    }

    public override object this[FieldDescriptor field]
    {
      get
      {
        if (!field.IsRepeated)
          return this.MessageBeingBuilt[field];
        return this.InternalFieldAccessors[field].GetRepeatedWrapper(this.ThisBuilder);
      }
      set
      {
        this.InternalFieldAccessors[field].SetValue(this.ThisBuilder, value);
      }
    }

    [CLSCompliant(false)]
    protected virtual bool ParseUnknownField(ICodedInputStream input, UnknownFieldSet.Builder unknownFields, ExtensionRegistry extensionRegistry, uint tag, string fieldName)
    {
      return unknownFields.MergeFieldFrom(tag, input);
    }

    public override MessageDescriptor DescriptorForType
    {
      get
      {
        return this.DefaultInstanceForType.DescriptorForType;
      }
    }

    public override int GetRepeatedFieldCount(FieldDescriptor field)
    {
      return this.MessageBeingBuilt.GetRepeatedFieldCount(field);
    }

    public override object this[FieldDescriptor field, int index]
    {
      get
      {
        return this.MessageBeingBuilt[field, index];
      }
      set
      {
        this.InternalFieldAccessors[field].SetRepeated(this.ThisBuilder, index, value);
      }
    }

    public override bool HasField(FieldDescriptor field)
    {
      return this.MessageBeingBuilt.HasField(field);
    }

    public override IBuilder CreateBuilderForField(FieldDescriptor field)
    {
      return this.InternalFieldAccessors[field].CreateBuilder();
    }

    public override TBuilder ClearField(FieldDescriptor field)
    {
      this.InternalFieldAccessors[field].Clear(this.ThisBuilder);
      return this.ThisBuilder;
    }

    public override TBuilder MergeFrom(TMessage other)
    {
      if (other.DescriptorForType != this.InternalFieldAccessors.Descriptor)
        throw new ArgumentException("Message type mismatch");
      foreach (KeyValuePair<FieldDescriptor, object> allField in (IEnumerable<KeyValuePair<FieldDescriptor, object>>) other.AllFields)
      {
        FieldDescriptor key = allField.Key;
        if (key.IsRepeated)
        {
          foreach (object obj in (IEnumerable) allField.Value)
            this.AddRepeatedField(key, obj);
        }
        else if (key.MappedType == MappedType.Message && this.HasField(key))
        {
          IMessageLite message = (IMessageLite) this[key];
          this[key] = (object) message.WeakCreateBuilderForType().WeakMergeFrom(message).WeakMergeFrom((IMessageLite) allField.Value).WeakBuildPartial();
        }
        else
          this[key] = allField.Value;
      }
      this.MergeUnknownFields(other.UnknownFields);
      return this.ThisBuilder;
    }

    public override TBuilder MergeUnknownFields(UnknownFieldSet unknownFields)
    {
      if (unknownFields != UnknownFieldSet.DefaultInstance)
      {
        TMessage messageBeingBuilt = this.MessageBeingBuilt;
        messageBeingBuilt.SetUnknownFields(UnknownFieldSet.CreateBuilder(messageBeingBuilt.UnknownFields).MergeFrom(unknownFields).Build());
      }
      return this.ThisBuilder;
    }

    public override TBuilder AddRepeatedField(FieldDescriptor field, object value)
    {
      this.InternalFieldAccessors[field].AddRepeated(this.ThisBuilder, value);
      return this.ThisBuilder;
    }

    public TMessage BuildParsed()
    {
      if (!this.IsInitialized)
        throw new UninitializedMessageException((IMessage) this.MessageBeingBuilt).AsInvalidProtocolBufferException();
      return this.BuildPartial();
    }

    public override TMessage Build()
    {
      if (!this.IsInitialized)
        throw new UninitializedMessageException((IMessage) this.MessageBeingBuilt);
      return this.BuildPartial();
    }

    public override UnknownFieldSet UnknownFields
    {
      get
      {
        return this.MessageBeingBuilt.UnknownFields;
      }
      set
      {
        this.MessageBeingBuilt.SetUnknownFields(value);
      }
    }
  }
}
