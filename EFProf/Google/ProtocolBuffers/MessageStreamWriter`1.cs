﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.MessageStreamWriter`1
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using System.IO;

namespace Google.ProtocolBuffers
{
  public sealed class MessageStreamWriter<T> where T : IMessage<T>
  {
    private readonly CodedOutputStream codedOutput;

    public MessageStreamWriter(Stream output)
    {
      this.codedOutput = CodedOutputStream.CreateInstance(output);
    }

    public void Write(T message)
    {
      this.codedOutput.WriteMessage(1, "item", (IMessageLite) message);
    }

    public void Flush()
    {
      this.codedOutput.Flush();
    }
  }
}
