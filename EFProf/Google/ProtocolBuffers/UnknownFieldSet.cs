﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.UnknownFieldSet
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers.Collections;
using Google.ProtocolBuffers.Descriptors;
using System;
using System.Collections.Generic;
using System.IO;

namespace Google.ProtocolBuffers
{
  public sealed class UnknownFieldSet : IMessageLite
  {
    private static readonly UnknownFieldSet defaultInstance = new UnknownFieldSet((IDictionary<int, UnknownField>) new Dictionary<int, UnknownField>());
    private readonly IDictionary<int, UnknownField> fields;

    private UnknownFieldSet(IDictionary<int, UnknownField> fields)
    {
      this.fields = fields;
    }

    public static UnknownFieldSet.Builder CreateBuilder()
    {
      return new UnknownFieldSet.Builder();
    }

    public static UnknownFieldSet.Builder CreateBuilder(UnknownFieldSet original)
    {
      return new UnknownFieldSet.Builder().MergeFrom(original);
    }

    public static UnknownFieldSet DefaultInstance
    {
      get
      {
        return UnknownFieldSet.defaultInstance;
      }
    }

    public IDictionary<int, UnknownField> FieldDictionary
    {
      get
      {
        return Dictionaries.AsReadOnly<int, UnknownField>(this.fields);
      }
    }

    public bool HasField(int field)
    {
      return this.fields.ContainsKey(field);
    }

    public UnknownField this[int number]
    {
      get
      {
        UnknownField defaultInstance;
        if (!this.fields.TryGetValue(number, out defaultInstance))
          defaultInstance = UnknownField.DefaultInstance;
        return defaultInstance;
      }
    }

    public void WriteTo(ICodedOutputStream output)
    {
      foreach (KeyValuePair<int, UnknownField> field in (IEnumerable<KeyValuePair<int, UnknownField>>) this.fields)
        field.Value.WriteTo(field.Key, output);
    }

    public int SerializedSize
    {
      get
      {
        int num = 0;
        foreach (KeyValuePair<int, UnknownField> field in (IEnumerable<KeyValuePair<int, UnknownField>>) this.fields)
          num += field.Value.GetSerializedSize(field.Key);
        return num;
      }
    }

    public override string ToString()
    {
      return TextFormat.PrintToString(this);
    }

    public void PrintTo(TextWriter writer)
    {
      TextFormat.Print(this, writer);
    }

    public ByteString ToByteString()
    {
      ByteString.CodedBuilder codedBuilder = new ByteString.CodedBuilder(this.SerializedSize);
      this.WriteTo((ICodedOutputStream) codedBuilder.CodedOutput);
      return codedBuilder.Build();
    }

    public byte[] ToByteArray()
    {
      byte[] flatArray = new byte[this.SerializedSize];
      CodedOutputStream instance = CodedOutputStream.CreateInstance(flatArray);
      this.WriteTo((ICodedOutputStream) instance);
      instance.CheckNoSpaceLeft();
      return flatArray;
    }

    public void WriteTo(Stream output)
    {
      CodedOutputStream instance = CodedOutputStream.CreateInstance(output);
      this.WriteTo((ICodedOutputStream) instance);
      instance.Flush();
    }

    public void WriteAsMessageSetTo(ICodedOutputStream output)
    {
      foreach (KeyValuePair<int, UnknownField> field in (IEnumerable<KeyValuePair<int, UnknownField>>) this.fields)
        field.Value.WriteAsMessageSetExtensionTo(field.Key, output);
    }

    public int SerializedSizeAsMessageSet
    {
      get
      {
        int num = 0;
        foreach (KeyValuePair<int, UnknownField> field in (IEnumerable<KeyValuePair<int, UnknownField>>) this.fields)
          num += field.Value.GetSerializedSizeAsMessageSetExtension(field.Key);
        return num;
      }
    }

    public override bool Equals(object other)
    {
      if (object.ReferenceEquals((object) this, other))
        return true;
      UnknownFieldSet unknownFieldSet = other as UnknownFieldSet;
      if (unknownFieldSet != null)
        return Dictionaries.Equals<int, UnknownField>(this.fields, unknownFieldSet.fields);
      return false;
    }

    public override int GetHashCode()
    {
      return Dictionaries.GetHashCode<int, UnknownField>(this.fields);
    }

    public static UnknownFieldSet ParseFrom(ICodedInputStream input)
    {
      return UnknownFieldSet.CreateBuilder().MergeFrom(input).Build();
    }

    public static UnknownFieldSet ParseFrom(ByteString data)
    {
      return UnknownFieldSet.CreateBuilder().MergeFrom(data).Build();
    }

    public static UnknownFieldSet ParseFrom(byte[] data)
    {
      return UnknownFieldSet.CreateBuilder().MergeFrom(data).Build();
    }

    public static UnknownFieldSet ParseFrom(Stream input)
    {
      return UnknownFieldSet.CreateBuilder().MergeFrom(input).Build();
    }

    public bool IsInitialized
    {
      get
      {
        return this.fields != null;
      }
    }

    public void WriteDelimitedTo(Stream output)
    {
      CodedOutputStream instance = CodedOutputStream.CreateInstance(output);
      instance.WriteRawVarint32((uint) this.SerializedSize);
      this.WriteTo((ICodedOutputStream) instance);
      instance.Flush();
    }

    public IBuilderLite WeakCreateBuilderForType()
    {
      return (IBuilderLite) new UnknownFieldSet.Builder();
    }

    public IBuilderLite WeakToBuilder()
    {
      return (IBuilderLite) new UnknownFieldSet.Builder(this.fields);
    }

    public IMessageLite WeakDefaultInstanceForType
    {
      get
      {
        return (IMessageLite) UnknownFieldSet.defaultInstance;
      }
    }

    public sealed class Builder : IBuilderLite
    {
      private IDictionary<int, UnknownField> fields;
      private int lastFieldNumber;
      private UnknownField.Builder lastField;

      internal Builder()
      {
        this.fields = (IDictionary<int, UnknownField>) new SortedList<int, UnknownField>();
      }

      internal Builder(IDictionary<int, UnknownField> dictionary)
      {
        this.fields = (IDictionary<int, UnknownField>) new SortedList<int, UnknownField>(dictionary);
      }

      private UnknownField.Builder GetFieldBuilder(int number)
      {
        if (this.lastField != null)
        {
          if (number == this.lastFieldNumber)
            return this.lastField;
          this.AddField(this.lastFieldNumber, this.lastField.Build());
        }
        if (number == 0)
          return (UnknownField.Builder) null;
        this.lastField = UnknownField.CreateBuilder();
        UnknownField other;
        if (this.fields.TryGetValue(number, out other))
          this.lastField.MergeFrom(other);
        this.lastFieldNumber = number;
        return this.lastField;
      }

      public UnknownFieldSet Build()
      {
        this.GetFieldBuilder(0);
        UnknownFieldSet unknownFieldSet = this.fields.Count == 0 ? UnknownFieldSet.DefaultInstance : new UnknownFieldSet(this.fields);
        this.fields = (IDictionary<int, UnknownField>) null;
        return unknownFieldSet;
      }

      public UnknownFieldSet.Builder AddField(int number, UnknownField field)
      {
        if (number == 0)
          throw new ArgumentOutOfRangeException(nameof (number), "Zero is not a valid field number.");
        if (this.lastField != null && this.lastFieldNumber == number)
        {
          this.lastField = (UnknownField.Builder) null;
          this.lastFieldNumber = 0;
        }
        this.fields[number] = field;
        return this;
      }

      public UnknownFieldSet.Builder Clear()
      {
        this.fields.Clear();
        this.lastFieldNumber = 0;
        this.lastField = (UnknownField.Builder) null;
        return this;
      }

      public UnknownFieldSet.Builder MergeFrom(ICodedInputStream input)
      {
        uint fieldTag;
        string fieldName;
        while (input.ReadTag(out fieldTag, out fieldName))
        {
          if ((int) fieldTag == 0)
          {
            if (!input.SkipField())
              break;
          }
          else if (!this.MergeFieldFrom(fieldTag, input))
            break;
        }
        return this;
      }

      [CLSCompliant(false)]
      public bool MergeFieldFrom(uint tag, ICodedInputStream input)
      {
        if ((int) tag == 0)
        {
          input.SkipField();
          return true;
        }
        int tagFieldNumber = WireFormat.GetTagFieldNumber(tag);
        switch (WireFormat.GetTagWireType(tag))
        {
          case WireFormat.WireType.Varint:
            ulong num1 = 0;
            if (input.ReadUInt64(ref num1))
              this.GetFieldBuilder(tagFieldNumber).AddVarint(num1);
            return true;
          case WireFormat.WireType.Fixed64:
            ulong num2 = 0;
            if (input.ReadFixed64(ref num2))
              this.GetFieldBuilder(tagFieldNumber).AddFixed64(num2);
            return true;
          case WireFormat.WireType.LengthDelimited:
            ByteString byteString = (ByteString) null;
            if (input.ReadBytes(ref byteString))
              this.GetFieldBuilder(tagFieldNumber).AddLengthDelimited(byteString);
            return true;
          case WireFormat.WireType.StartGroup:
            UnknownFieldSet.Builder builder = UnknownFieldSet.CreateBuilder();
            input.ReadUnknownGroup(tagFieldNumber, (IBuilderLite) builder);
            this.GetFieldBuilder(tagFieldNumber).AddGroup(builder.Build());
            return true;
          case WireFormat.WireType.EndGroup:
            return false;
          case WireFormat.WireType.Fixed32:
            uint num3 = 0;
            if (input.ReadFixed32(ref num3))
              this.GetFieldBuilder(tagFieldNumber).AddFixed32(num3);
            return true;
          default:
            throw InvalidProtocolBufferException.InvalidWireType();
        }
      }

      public UnknownFieldSet.Builder MergeFrom(Stream input)
      {
        CodedInputStream instance = CodedInputStream.CreateInstance(input);
        this.MergeFrom((ICodedInputStream) instance);
        instance.CheckLastTagWas(0U);
        return this;
      }

      public UnknownFieldSet.Builder MergeFrom(ByteString data)
      {
        CodedInputStream codedInput = data.CreateCodedInput();
        this.MergeFrom((ICodedInputStream) codedInput);
        codedInput.CheckLastTagWas(0U);
        return this;
      }

      public UnknownFieldSet.Builder MergeFrom(byte[] data)
      {
        CodedInputStream instance = CodedInputStream.CreateInstance(data);
        this.MergeFrom((ICodedInputStream) instance);
        instance.CheckLastTagWas(0U);
        return this;
      }

      [CLSCompliant(false)]
      public UnknownFieldSet.Builder MergeVarintField(int number, ulong value)
      {
        if (number == 0)
          throw new ArgumentOutOfRangeException(nameof (number), "Zero is not a valid field number.");
        this.GetFieldBuilder(number).AddVarint(value);
        return this;
      }

      public UnknownFieldSet.Builder MergeFrom(UnknownFieldSet other)
      {
        if (other != UnknownFieldSet.DefaultInstance)
        {
          foreach (KeyValuePair<int, UnknownField> field in (IEnumerable<KeyValuePair<int, UnknownField>>) other.fields)
            this.MergeField(field.Key, field.Value);
        }
        return this;
      }

      public bool HasField(int number)
      {
        if (number == 0)
          throw new ArgumentOutOfRangeException(nameof (number), "Zero is not a valid field number.");
        if (number != this.lastFieldNumber)
          return this.fields.ContainsKey(number);
        return true;
      }

      public UnknownFieldSet.Builder MergeField(int number, UnknownField field)
      {
        if (number == 0)
          throw new ArgumentOutOfRangeException(nameof (number), "Zero is not a valid field number.");
        if (this.HasField(number))
          this.GetFieldBuilder(number).MergeFrom(field);
        else
          this.AddField(number, field);
        return this;
      }

      internal void MergeFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry, IBuilder builder)
      {
        uint fieldTag;
        string fieldName;
        while (input.ReadTag(out fieldTag, out fieldName))
        {
          if ((int) fieldTag == 0 && fieldName != null)
          {
            FieldDescriptor fieldByName = builder.DescriptorForType.FindFieldByName(fieldName);
            if (fieldByName != null)
            {
              fieldTag = WireFormat.MakeTag(fieldByName);
            }
            else
            {
              ExtensionInfo byName = extensionRegistry.FindByName(builder.DescriptorForType, fieldName);
              if (byName != null)
                fieldTag = WireFormat.MakeTag(byName.Descriptor);
            }
          }
          if ((int) fieldTag == 0)
          {
            if (!input.SkipField())
              break;
          }
          else if (!this.MergeFieldFrom(input, extensionRegistry, builder, fieldTag, fieldName))
            break;
        }
      }

      internal bool MergeFieldFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry, IBuilder builder, uint tag, string fieldName)
      {
        if ((int) tag == 0 && fieldName != null)
        {
          FieldDescriptor fieldByName = builder.DescriptorForType.FindFieldByName(fieldName);
          if (fieldByName != null)
          {
            tag = WireFormat.MakeTag(fieldByName);
          }
          else
          {
            ExtensionInfo byName = extensionRegistry.FindByName(builder.DescriptorForType, fieldName);
            if (byName != null)
              tag = WireFormat.MakeTag(byName.Descriptor);
          }
        }
        MessageDescriptor descriptorForType = builder.DescriptorForType;
        if (descriptorForType.Options.MessageSetWireFormat && (int) tag == (int) WireFormat.MessageSetTag.ItemStart)
        {
          this.MergeMessageSetExtensionFromCodedStream(input, extensionRegistry, builder);
          return true;
        }
        WireFormat.WireType tagWireType = WireFormat.GetTagWireType(tag);
        int tagFieldNumber = WireFormat.GetTagFieldNumber(tag);
        IMessageLite messageLite1 = (IMessageLite) null;
        FieldDescriptor index;
        if (descriptorForType.IsExtensionNumber(tagFieldNumber))
        {
          ExtensionInfo extensionInfo = extensionRegistry[descriptorForType, tagFieldNumber];
          if (extensionInfo == null)
          {
            index = (FieldDescriptor) null;
          }
          else
          {
            index = extensionInfo.Descriptor;
            messageLite1 = extensionInfo.DefaultInstance;
          }
        }
        else
          index = descriptorForType.FindFieldByNumber(tagFieldNumber);
        if (index == null)
          return this.MergeFieldFrom(tag, input);
        if (tagWireType != WireFormat.GetWireType(index))
        {
          WireFormat.WireType wireType = WireFormat.GetWireType(index.FieldType);
          if (tagWireType != wireType && (!index.IsRepeated || tagWireType != WireFormat.WireType.LengthDelimited || wireType != WireFormat.WireType.Varint && wireType != WireFormat.WireType.Fixed32 && wireType != WireFormat.WireType.Fixed64))
            return this.MergeFieldFrom(tag, input);
        }
        switch (index.FieldType)
        {
          case FieldType.Group:
          case FieldType.Message:
            IBuilderLite builder1 = messageLite1 != null ? messageLite1.WeakCreateBuilderForType() : (IBuilderLite) builder.CreateBuilderForField(index);
            if (!index.IsRepeated)
            {
              builder1.WeakMergeFrom((IMessageLite) builder[index]);
              if (index.FieldType == FieldType.Group)
                input.ReadGroup(index.FieldNumber, builder1, extensionRegistry);
              else
                input.ReadMessage(builder1, extensionRegistry);
              builder[index] = (object) builder1.WeakBuild();
              break;
            }
            List<IMessageLite> messageLiteList = new List<IMessageLite>();
            if (index.FieldType == FieldType.Group)
              input.ReadGroupArray<IMessageLite>(tag, fieldName, (ICollection<IMessageLite>) messageLiteList, builder1.WeakDefaultInstanceForType, extensionRegistry);
            else
              input.ReadMessageArray<IMessageLite>(tag, fieldName, (ICollection<IMessageLite>) messageLiteList, builder1.WeakDefaultInstanceForType, extensionRegistry);
            foreach (IMessageLite messageLite2 in messageLiteList)
              builder.WeakAddRepeatedField(index, (object) messageLite2);
            return true;
          case FieldType.Enum:
            if (!index.IsRepeated)
            {
              IEnumLite enumLite = (IEnumLite) null;
              object unknown;
              if (input.ReadEnum(ref enumLite, out unknown, (IEnumLiteMap) index.EnumType))
              {
                builder[index] = (object) enumLite;
                break;
              }
              if (unknown is int)
              {
                this.MergeVarintField(tagFieldNumber, (ulong) (int) unknown);
                break;
              }
              break;
            }
            List<IEnumLite> enumLiteList = new List<IEnumLite>();
            ICollection<object> unknown1;
            input.ReadEnumArray(tag, fieldName, (ICollection<IEnumLite>) enumLiteList, out unknown1, (IEnumLiteMap) index.EnumType);
            foreach (IEnumLite enumLite in enumLiteList)
              builder.WeakAddRepeatedField(index, (object) enumLite);
            if (unknown1 != null)
            {
              using (IEnumerator<object> enumerator = unknown1.GetEnumerator())
              {
                while (enumerator.MoveNext())
                {
                  object current = enumerator.Current;
                  if (current is int)
                    this.MergeVarintField(tagFieldNumber, (ulong) (int) current);
                }
                break;
              }
            }
            else
              break;
          default:
            if (!index.IsRepeated)
            {
              object obj = (object) null;
              if (input.ReadPrimitiveField(index.FieldType, ref obj))
              {
                builder[index] = obj;
                break;
              }
              break;
            }
            List<object> objectList = new List<object>();
            input.ReadPrimitiveArray(index.FieldType, tag, fieldName, (ICollection<object>) objectList);
            using (List<object>.Enumerator enumerator = objectList.GetEnumerator())
            {
              while (enumerator.MoveNext())
              {
                object current = enumerator.Current;
                builder.WeakAddRepeatedField(index, current);
              }
              break;
            }
        }
        return true;
      }

      private void MergeMessageSetExtensionFromCodedStream(ICodedInputStream input, ExtensionRegistry extensionRegistry, IBuilder builder)
      {
        MessageDescriptor descriptorForType = builder.DescriptorForType;
        int number = 0;
        ByteString byteString = (ByteString) null;
        IBuilderLite builder1 = (IBuilderLite) null;
        FieldDescriptor index = (FieldDescriptor) null;
        uint num = WireFormat.MessageSetTag.ItemStart;
        uint fieldTag;
        string fieldName;
        while (input.ReadTag(out fieldTag, out fieldName))
        {
          if ((int) fieldTag == 0 && fieldName != null)
          {
            if (fieldName == "type_id")
              fieldTag = WireFormat.MessageSetTag.TypeID;
            else if (fieldName == "message")
              fieldTag = WireFormat.MessageSetTag.Message;
          }
          if ((int) fieldTag == 0)
          {
            if (!input.SkipField())
              break;
          }
          else
          {
            num = fieldTag;
            if ((int) fieldTag == (int) WireFormat.MessageSetTag.TypeID)
            {
              number = 0;
              if (input.ReadInt32(ref number) && number != 0)
              {
                ExtensionInfo extensionInfo = extensionRegistry[descriptorForType, number];
                if (extensionInfo != null)
                {
                  index = extensionInfo.Descriptor;
                  builder1 = extensionInfo.DefaultInstance.WeakCreateBuilderForType();
                  IMessageLite message = (IMessageLite) builder[index];
                  if (message != null)
                    builder1.WeakMergeFrom(message);
                  if (byteString != null)
                  {
                    builder1.WeakMergeFrom((ICodedInputStream) byteString.CreateCodedInput());
                    byteString = (ByteString) null;
                  }
                }
                else if (byteString != null)
                {
                  this.MergeField(number, UnknownField.CreateBuilder().AddLengthDelimited(byteString).Build());
                  byteString = (ByteString) null;
                }
              }
            }
            else if ((int) fieldTag == (int) WireFormat.MessageSetTag.Message)
            {
              if (builder1 != null)
                input.ReadMessage(builder1, extensionRegistry);
              else if (input.ReadBytes(ref byteString) && number != 0)
                this.MergeField(number, UnknownField.CreateBuilder().AddLengthDelimited(byteString).Build());
            }
            else if (!input.SkipField())
              break;
          }
        }
        if ((int) num != (int) WireFormat.MessageSetTag.ItemEnd)
          throw InvalidProtocolBufferException.InvalidEndTag();
        if (builder1 == null)
          return;
        builder[index] = (object) builder1.WeakBuild();
      }

      bool IBuilderLite.IsInitialized
      {
        get
        {
          return this.fields != null;
        }
      }

      IBuilderLite IBuilderLite.WeakClear()
      {
        return (IBuilderLite) this.Clear();
      }

      IBuilderLite IBuilderLite.WeakMergeFrom(IMessageLite message)
      {
        return (IBuilderLite) this.MergeFrom((UnknownFieldSet) message);
      }

      IBuilderLite IBuilderLite.WeakMergeFrom(ByteString data)
      {
        return (IBuilderLite) this.MergeFrom(data);
      }

      IBuilderLite IBuilderLite.WeakMergeFrom(ByteString data, ExtensionRegistry registry)
      {
        return (IBuilderLite) this.MergeFrom(data);
      }

      IBuilderLite IBuilderLite.WeakMergeFrom(ICodedInputStream input)
      {
        return (IBuilderLite) this.MergeFrom(input);
      }

      IBuilderLite IBuilderLite.WeakMergeFrom(ICodedInputStream input, ExtensionRegistry registry)
      {
        return (IBuilderLite) this.MergeFrom(input);
      }

      IMessageLite IBuilderLite.WeakBuild()
      {
        return (IMessageLite) this.Build();
      }

      IMessageLite IBuilderLite.WeakBuildPartial()
      {
        return (IMessageLite) this.Build();
      }

      IBuilderLite IBuilderLite.WeakClone()
      {
        return this.Build().WeakToBuilder();
      }

      IMessageLite IBuilderLite.WeakDefaultInstanceForType
      {
        get
        {
          return (IMessageLite) UnknownFieldSet.DefaultInstance;
        }
      }
    }
  }
}
