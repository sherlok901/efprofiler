﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.AbstractMessage`2
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers.Collections;
using Google.ProtocolBuffers.Descriptors;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace Google.ProtocolBuffers
{
  [Serializable]
  public abstract class AbstractMessage<TMessage, TBuilder> : AbstractMessageLite<TMessage, TBuilder>, IMessage<TMessage, TBuilder>, IMessage<TMessage>, IMessage, IMessageLite<TMessage, TBuilder>, IMessageLite<TMessage>, IMessageLite where TMessage : AbstractMessage<TMessage, TBuilder> where TBuilder : AbstractBuilder<TMessage, TBuilder>
  {
    private int? memoizedSize = new int?();

    public abstract MessageDescriptor DescriptorForType { get; }

    public abstract IDictionary<FieldDescriptor, object> AllFields { get; }

    public abstract bool HasField(FieldDescriptor field);

    public abstract object this[FieldDescriptor field] { get; }

    public abstract int GetRepeatedFieldCount(FieldDescriptor field);

    public abstract object this[FieldDescriptor field, int index] { get; }

    public abstract UnknownFieldSet UnknownFields { get; }

    public override bool IsInitialized
    {
      get
      {
        foreach (FieldDescriptor field in (IEnumerable<FieldDescriptor>) this.DescriptorForType.Fields)
        {
          if (field.IsRequired && !this.HasField(field))
            return false;
        }
        foreach (KeyValuePair<FieldDescriptor, object> allField in (IEnumerable<KeyValuePair<FieldDescriptor, object>>) this.AllFields)
        {
          FieldDescriptor key = allField.Key;
          if (key.MappedType == MappedType.Message)
          {
            if (key.IsRepeated)
            {
              foreach (IMessageLite messageLite in (IEnumerable) allField.Value)
              {
                if (!messageLite.IsInitialized)
                  return false;
              }
            }
            else if (!((IMessageLite) allField.Value).IsInitialized)
              return false;
          }
        }
        return true;
      }
    }

    public override sealed string ToString()
    {
      return TextFormat.PrintToString((IMessage) this);
    }

    public override sealed void PrintTo(TextWriter writer)
    {
      TextFormat.Print((IMessage) this, writer);
    }

    public override void WriteTo(ICodedOutputStream output)
    {
      foreach (KeyValuePair<FieldDescriptor, object> allField in (IEnumerable<KeyValuePair<FieldDescriptor, object>>) this.AllFields)
      {
        FieldDescriptor key = allField.Key;
        if (key.IsRepeated)
        {
          IEnumerable list = (IEnumerable) allField.Value;
          if (key.IsPacked)
            output.WritePackedArray(key.FieldType, key.FieldNumber, key.Name, list);
          else
            output.WriteArray(key.FieldType, key.FieldNumber, key.Name, list);
        }
        else
          output.WriteField(key.FieldType, key.FieldNumber, key.Name, allField.Value);
      }
      UnknownFieldSet unknownFields = this.UnknownFields;
      if (this.DescriptorForType.Options.MessageSetWireFormat)
        unknownFields.WriteAsMessageSetTo(output);
      else
        unknownFields.WriteTo(output);
    }

    public override int SerializedSize
    {
      get
      {
        if (this.memoizedSize.HasValue)
          return this.memoizedSize.Value;
        int num1 = 0;
        foreach (KeyValuePair<FieldDescriptor, object> allField in (IEnumerable<KeyValuePair<FieldDescriptor, object>>) this.AllFields)
        {
          FieldDescriptor key = allField.Key;
          if (key.IsRepeated)
          {
            IEnumerable enumerable = (IEnumerable) allField.Value;
            if (key.IsPacked)
            {
              int num2 = 0;
              foreach (object obj in enumerable)
                num2 += CodedOutputStream.ComputeFieldSizeNoTag(key.FieldType, obj);
              num1 += num2;
              num1 += CodedOutputStream.ComputeTagSize(key.FieldNumber);
              num1 += CodedOutputStream.ComputeRawVarint32Size((uint) num2);
            }
            else
            {
              foreach (object obj in enumerable)
                num1 += CodedOutputStream.ComputeFieldSize(key.FieldType, key.FieldNumber, obj);
            }
          }
          else
            num1 += CodedOutputStream.ComputeFieldSize(key.FieldType, key.FieldNumber, allField.Value);
        }
        UnknownFieldSet unknownFields = this.UnknownFields;
        int num3 = !this.DescriptorForType.Options.MessageSetWireFormat ? num1 + unknownFields.SerializedSize : num1 + unknownFields.SerializedSizeAsMessageSet;
        this.memoizedSize = new int?(num3);
        return num3;
      }
    }

    public override bool Equals(object other)
    {
      if (other == this)
        return true;
      IMessage message = other as IMessage;
      if (message == null || message.DescriptorForType != this.DescriptorForType || !Dictionaries.Equals<FieldDescriptor, object>(this.AllFields, message.AllFields))
        return false;
      return this.UnknownFields.Equals((object) message.UnknownFields);
    }

    public override int GetHashCode()
    {
      return 29 * (53 * (19 * 41 + this.DescriptorForType.GetHashCode()) + Dictionaries.GetHashCode<FieldDescriptor, object>(this.AllFields)) + this.UnknownFields.GetHashCode();
    }

    IBuilder IMessage.WeakCreateBuilderForType()
    {
      return (IBuilder) this.CreateBuilderForType();
    }

    IBuilder IMessage.WeakToBuilder()
    {
      return (IBuilder) this.ToBuilder();
    }

    IMessage IMessage.WeakDefaultInstanceForType
    {
      get
      {
        return (IMessage) this.DefaultInstanceForType;
      }
    }
  }
}
