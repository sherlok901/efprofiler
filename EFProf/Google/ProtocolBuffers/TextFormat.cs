﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.TextFormat
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers.DescriptorProtos;
using Google.ProtocolBuffers.Descriptors;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Google.ProtocolBuffers
{
  public static class TextFormat
  {
    public static void Print(IMessage message, TextWriter output)
    {
      TextGenerator generator = new TextGenerator(output, "\n");
      TextFormat.Print(message, generator);
    }

    public static void Print(IBuilder builder, TextWriter output)
    {
      TextGenerator generator = new TextGenerator(output, "\n");
      TextFormat.Print(builder, generator);
    }

    public static void Print(UnknownFieldSet fields, TextWriter output)
    {
      TextGenerator generator = new TextGenerator(output, "\n");
      TextFormat.PrintUnknownFields(fields, generator);
    }

    public static string PrintToString(IMessage message)
    {
      StringWriter stringWriter = new StringWriter();
      TextFormat.Print(message, (TextWriter) stringWriter);
      return stringWriter.ToString();
    }

    public static string PrintToString(IBuilder builder)
    {
      StringWriter stringWriter = new StringWriter();
      TextFormat.Print(builder, (TextWriter) stringWriter);
      return stringWriter.ToString();
    }

    public static string PrintToString(UnknownFieldSet fields)
    {
      StringWriter stringWriter = new StringWriter();
      TextFormat.Print(fields, (TextWriter) stringWriter);
      return stringWriter.ToString();
    }

    private static void Print(IMessage message, TextGenerator generator)
    {
      foreach (KeyValuePair<FieldDescriptor, object> allField in (IEnumerable<KeyValuePair<FieldDescriptor, object>>) message.AllFields)
        TextFormat.PrintField(allField.Key, allField.Value, generator);
      TextFormat.PrintUnknownFields(message.UnknownFields, generator);
    }

    private static void Print(IBuilder message, TextGenerator generator)
    {
      foreach (KeyValuePair<FieldDescriptor, object> allField in (IEnumerable<KeyValuePair<FieldDescriptor, object>>) message.AllFields)
        TextFormat.PrintField(allField.Key, allField.Value, generator);
      TextFormat.PrintUnknownFields(message.UnknownFields, generator);
    }

    internal static void PrintField(FieldDescriptor field, object value, TextGenerator generator)
    {
      if (field.IsRepeated)
      {
        foreach (object obj in (IEnumerable) value)
          TextFormat.PrintSingleField(field, obj, generator);
      }
      else
        TextFormat.PrintSingleField(field, value, generator);
    }

    private static void PrintSingleField(FieldDescriptor field, object value, TextGenerator generator)
    {
      if (field.IsExtension)
      {
        generator.Print("[");
        if (field.ContainingType.Options.MessageSetWireFormat && field.FieldType == FieldType.Message && (field.IsOptional && field.ExtensionScope == field.MessageType))
          generator.Print(field.MessageType.FullName);
        else
          generator.Print(field.FullName);
        generator.Print("]");
      }
      else if (field.FieldType == FieldType.Group)
        generator.Print(field.MessageType.Name);
      else
        generator.Print(field.Name);
      if (field.MappedType == MappedType.Message)
      {
        generator.Print(" {\n");
        generator.Indent();
      }
      else
        generator.Print(": ");
      TextFormat.PrintFieldValue(field, value, generator);
      if (field.MappedType == MappedType.Message)
      {
        generator.Outdent();
        generator.Print("}");
      }
      generator.Print("\n");
    }

    private static void PrintFieldValue(FieldDescriptor field, object value, TextGenerator generator)
    {
      switch (field.FieldType)
      {
        case FieldType.Double:
          generator.Print(((double) value).ToString("r", (IFormatProvider) FrameworkPortability.InvariantCulture));
          break;
        case FieldType.Float:
          generator.Print(((float) value).ToString("r", (IFormatProvider) FrameworkPortability.InvariantCulture));
          break;
        case FieldType.Int64:
        case FieldType.UInt64:
        case FieldType.Int32:
        case FieldType.Fixed64:
        case FieldType.Fixed32:
        case FieldType.UInt32:
        case FieldType.SFixed32:
        case FieldType.SFixed64:
        case FieldType.SInt32:
        case FieldType.SInt64:
          generator.Print(((IConvertible) value).ToString((IFormatProvider) FrameworkPortability.InvariantCulture));
          break;
        case FieldType.Bool:
          generator.Print((bool) value ? "true" : "false");
          break;
        case FieldType.String:
          generator.Print("\"");
          generator.Print(TextFormat.EscapeText((string) value));
          generator.Print("\"");
          break;
        case FieldType.Group:
        case FieldType.Message:
          if (value is IMessageLite && !(value is IMessage))
            throw new NotSupportedException("Lite messages are not supported.");
          TextFormat.Print((IMessage) value, generator);
          break;
        case FieldType.Bytes:
          generator.Print("\"");
          generator.Print(TextFormat.EscapeBytes((ByteString) value));
          generator.Print("\"");
          break;
        case FieldType.Enum:
          if (value is IEnumLite && !(value is EnumValueDescriptor))
            throw new NotSupportedException("Lite enumerations are not supported.");
          generator.Print(((DescriptorBase<EnumValueDescriptorProto, EnumValueOptions>) value).Name);
          break;
      }
    }

    private static void PrintUnknownFields(UnknownFieldSet unknownFields, TextGenerator generator)
    {
      foreach (KeyValuePair<int, UnknownField> field in (IEnumerable<KeyValuePair<int, UnknownField>>) unknownFields.FieldDictionary)
      {
        string text = field.Key.ToString() + ": ";
        UnknownField unknownField = field.Value;
        foreach (ulong varint in (IEnumerable<ulong>) unknownField.VarintList)
        {
          generator.Print(text);
          generator.Print(varint.ToString());
          generator.Print("\n");
        }
        foreach (uint fixed32 in (IEnumerable<uint>) unknownField.Fixed32List)
        {
          generator.Print(text);
          generator.Print(string.Format("0x{0:x8}", (object) fixed32));
          generator.Print("\n");
        }
        foreach (ulong fixed64 in (IEnumerable<ulong>) unknownField.Fixed64List)
        {
          generator.Print(text);
          generator.Print(string.Format("0x{0:x16}", (object) fixed64));
          generator.Print("\n");
        }
        foreach (ByteString lengthDelimited in (IEnumerable<ByteString>) unknownField.LengthDelimitedList)
        {
          generator.Print(field.Key.ToString());
          generator.Print(": \"");
          generator.Print(TextFormat.EscapeBytes(lengthDelimited));
          generator.Print("\"\n");
        }
        foreach (UnknownFieldSet group in (IEnumerable<UnknownFieldSet>) unknownField.GroupList)
        {
          generator.Print(field.Key.ToString());
          generator.Print(" {\n");
          generator.Indent();
          TextFormat.PrintUnknownFields(group, generator);
          generator.Outdent();
          generator.Print("}\n");
        }
      }
    }

    [CLSCompliant(false)]
    public static ulong ParseUInt64(string text)
    {
      return (ulong) TextFormat.ParseInteger(text, false, true);
    }

    public static long ParseInt64(string text)
    {
      return TextFormat.ParseInteger(text, true, true);
    }

    [CLSCompliant(false)]
    public static uint ParseUInt32(string text)
    {
      return (uint) TextFormat.ParseInteger(text, false, false);
    }

    public static int ParseInt32(string text)
    {
      return (int) TextFormat.ParseInteger(text, true, false);
    }

    public static float ParseFloat(string text)
    {
      switch (text)
      {
        case "-inf":
        case "-infinity":
        case "-inff":
        case "-infinityf":
          return float.NegativeInfinity;
        case "inf":
        case "infinity":
        case "inff":
        case "infinityf":
          return float.PositiveInfinity;
        case "nan":
        case "nanf":
          return float.NaN;
        default:
          return float.Parse(text, (IFormatProvider) FrameworkPortability.InvariantCulture);
      }
    }

    public static double ParseDouble(string text)
    {
      switch (text)
      {
        case "-inf":
        case "-infinity":
          return double.NegativeInfinity;
        case "inf":
        case "infinity":
          return double.PositiveInfinity;
        case "nan":
          return double.NaN;
        default:
          return double.Parse(text, (IFormatProvider) FrameworkPortability.InvariantCulture);
      }
    }

    private static long ParseInteger(string text, bool isSigned, bool isLong)
    {
      string str = text;
      bool flag = false;
      if (text.StartsWith("-"))
      {
        if (!isSigned)
          throw new FormatException("Number must be positive: " + str);
        flag = true;
        text = text.Substring(1);
      }
      int fromBase = 10;
      if (text.StartsWith("0x"))
      {
        fromBase = 16;
        text = text.Substring(2);
      }
      else if (text.StartsWith("0"))
        fromBase = 8;
      ulong num1;
      try
      {
        num1 = fromBase == 10 ? ulong.Parse(text) : Convert.ToUInt64(text, fromBase);
      }
      catch (OverflowException ex)
      {
        throw new FormatException("Number out of range for " + string.Format("{0}-bit {1}signed integer", (object) (isLong ? 64 : 32), isSigned ? (object) "" : (object) "un") + ": " + str);
      }
      if (flag)
      {
        ulong num2 = isLong ? 9223372036854775808UL : 2147483648UL;
        if (num1 > num2)
          throw new FormatException("Number out of range for " + string.Format("{0}-bit signed integer", (object) (isLong ? 64 : 32)) + ": " + str);
        return -(long) num1;
      }
      ulong num3 = isSigned ? (isLong ? (ulong) long.MaxValue : (ulong) int.MaxValue) : (isLong ? ulong.MaxValue : (ulong) uint.MaxValue);
      if (num1 > num3)
        throw new FormatException("Number out of range for " + string.Format("{0}-bit {1}signed integer", (object) (isLong ? 64 : 32), isSigned ? (object) "" : (object) "un") + ": " + str);
      return (long) num1;
    }

    private static bool IsOctal(char c)
    {
      if (48 <= (int) c)
        return (int) c <= 55;
      return false;
    }

    private static bool IsHex(char c)
    {
      if (48 <= (int) c && (int) c <= 57 || 97 <= (int) c && (int) c <= 102)
        return true;
      if (65 <= (int) c)
        return (int) c <= 70;
      return false;
    }

    private static int ParseDigit(char c)
    {
      if (48 <= (int) c && (int) c <= 57)
        return (int) c - 48;
      if (97 <= (int) c && (int) c <= 122)
        return (int) c - 97 + 10;
      return (int) c - 65 + 10;
    }

    public static string UnescapeText(string input)
    {
      return TextFormat.UnescapeBytes(input).ToStringUtf8();
    }

    public static string EscapeText(string input)
    {
      return TextFormat.EscapeBytes(ByteString.CopyFromUtf8(input));
    }

    public static string EscapeBytes(ByteString input)
    {
      StringBuilder stringBuilder = new StringBuilder(input.Length);
      foreach (byte num in input)
      {
        switch (num)
        {
          case 7:
            stringBuilder.Append("\\a");
            continue;
          case 8:
            stringBuilder.Append("\\b");
            continue;
          case 9:
            stringBuilder.Append("\\t");
            continue;
          case 10:
            stringBuilder.Append("\\n");
            continue;
          case 11:
            stringBuilder.Append("\\v");
            continue;
          case 12:
            stringBuilder.Append("\\f");
            continue;
          case 13:
            stringBuilder.Append("\\r");
            continue;
          case 34:
            stringBuilder.Append("\\\"");
            continue;
          case 39:
            stringBuilder.Append("\\'");
            continue;
          case 92:
            stringBuilder.Append("\\\\");
            continue;
          default:
            if ((int) num >= 32 && (int) num < 128)
            {
              stringBuilder.Append((char) num);
              continue;
            }
            stringBuilder.Append('\\');
            stringBuilder.Append((char) (48 + ((int) num >> 6 & 3)));
            stringBuilder.Append((char) (48 + ((int) num >> 3 & 7)));
            stringBuilder.Append((char) (48 + ((int) num & 7)));
            continue;
        }
      }
      return stringBuilder.ToString();
    }

    public static ByteString UnescapeBytes(string input)
    {
      byte[] bytes = new byte[input.Length];
      int count = 0;
      for (int index = 0; index < input.Length; ++index)
      {
        char ch = input[index];
        if ((int) ch > (int) sbyte.MaxValue || (int) ch < 32)
          throw new FormatException("Escaped string must only contain ASCII");
        if ((int) ch != 92)
        {
          bytes[count++] = (byte) ch;
        }
        else
        {
          if (index + 1 >= input.Length)
            throw new FormatException("Invalid escape sequence: '\\' at end of string.");
          ++index;
          char c = input[index];
          if ((int) c >= 48 && (int) c <= 55)
          {
            int num = TextFormat.ParseDigit(c);
            if (index + 1 < input.Length && TextFormat.IsOctal(input[index + 1]))
            {
              ++index;
              num = num * 8 + TextFormat.ParseDigit(input[index]);
            }
            if (index + 1 < input.Length && TextFormat.IsOctal(input[index + 1]))
            {
              ++index;
              num = num * 8 + TextFormat.ParseDigit(input[index]);
            }
            bytes[count++] = (byte) num;
          }
          else
          {
            switch (c)
            {
              case '"':
                bytes[count++] = (byte) 34;
                continue;
              case '\'':
                bytes[count++] = (byte) 39;
                continue;
              case '\\':
                bytes[count++] = (byte) 92;
                continue;
              case 'a':
                bytes[count++] = (byte) 7;
                continue;
              case 'b':
                bytes[count++] = (byte) 8;
                continue;
              case 'f':
                bytes[count++] = (byte) 12;
                continue;
              case 'n':
                bytes[count++] = (byte) 10;
                continue;
              case 'r':
                bytes[count++] = (byte) 13;
                continue;
              case 't':
                bytes[count++] = (byte) 9;
                continue;
              case 'v':
                bytes[count++] = (byte) 11;
                continue;
              case 'x':
                if (index + 1 >= input.Length || !TextFormat.IsHex(input[index + 1]))
                  throw new FormatException("Invalid escape sequence: '\\x' with no digits");
                ++index;
                int num1 = TextFormat.ParseDigit(input[index]);
                if (index + 1 < input.Length && TextFormat.IsHex(input[index + 1]))
                {
                  ++index;
                  num1 = num1 * 16 + TextFormat.ParseDigit(input[index]);
                }
                bytes[count++] = (byte) num1;
                continue;
              default:
                throw new FormatException("Invalid escape sequence: '\\" + (object) c + "'");
            }
          }
        }
      }
      return ByteString.CopyFrom(bytes, 0, count);
    }

    public static void Merge(string text, IBuilder builder)
    {
      TextFormat.Merge(text, ExtensionRegistry.Empty, builder);
    }

    public static void Merge(TextReader reader, IBuilder builder)
    {
      TextFormat.Merge(reader, ExtensionRegistry.Empty, builder);
    }

    public static void Merge(TextReader reader, ExtensionRegistry registry, IBuilder builder)
    {
      TextFormat.Merge(reader.ReadToEnd(), registry, builder);
    }

    public static void Merge(string text, ExtensionRegistry registry, IBuilder builder)
    {
      TextTokenizer tokenizer = new TextTokenizer(text);
      while (!tokenizer.AtEnd)
        TextFormat.MergeField(tokenizer, registry, builder);
    }

    private static void MergeField(TextTokenizer tokenizer, ExtensionRegistry extensionRegistry, IBuilder builder)
    {
      MessageDescriptor descriptorForType = builder.DescriptorForType;
      ExtensionInfo extensionInfo = (ExtensionInfo) null;
      FieldDescriptor field;
      if (tokenizer.TryConsume("["))
      {
        StringBuilder stringBuilder = new StringBuilder(tokenizer.ConsumeIdentifier());
        while (tokenizer.TryConsume("."))
        {
          stringBuilder.Append(".");
          stringBuilder.Append(tokenizer.ConsumeIdentifier());
        }
        extensionInfo = extensionRegistry.FindByName(descriptorForType, stringBuilder.ToString());
        if (extensionInfo == null)
          throw tokenizer.CreateFormatExceptionPreviousToken("Extension \"" + (object) stringBuilder + "\" not found in the ExtensionRegistry.");
        if (extensionInfo.Descriptor.ContainingType != descriptorForType)
          throw tokenizer.CreateFormatExceptionPreviousToken("Extension \"" + (object) stringBuilder + "\" does not extend message type \"" + descriptorForType.FullName + "\".");
        tokenizer.Consume("]");
        field = extensionInfo.Descriptor;
      }
      else
      {
        string name = tokenizer.ConsumeIdentifier();
        field = descriptorForType.FindDescriptor<FieldDescriptor>(name);
        if (field == null)
        {
          string lower = name.ToLower(FrameworkPortability.InvariantCulture);
          field = descriptorForType.FindDescriptor<FieldDescriptor>(lower);
          if (field != null && field.FieldType != FieldType.Group)
            field = (FieldDescriptor) null;
        }
        if (field != null && field.FieldType == FieldType.Group && field.MessageType.Name != name)
          field = (FieldDescriptor) null;
        if (field == null)
          throw tokenizer.CreateFormatExceptionPreviousToken("Message type \"" + descriptorForType.FullName + "\" has no field named \"" + name + "\".");
      }
      object obj = (object) null;
      if (field.MappedType == MappedType.Message)
      {
        tokenizer.TryConsume(":");
        string token;
        if (tokenizer.TryConsume("<"))
        {
          token = ">";
        }
        else
        {
          tokenizer.Consume("{");
          token = "}";
        }
        IBuilder builder1;
        if (extensionInfo == null)
        {
          builder1 = builder.CreateBuilderForField(field);
        }
        else
        {
          builder1 = extensionInfo.DefaultInstance.WeakCreateBuilderForType() as IBuilder;
          if (builder1 == null)
            throw new NotSupportedException("Lite messages are not supported.");
        }
        while (!tokenizer.TryConsume(token))
        {
          if (tokenizer.AtEnd)
            throw tokenizer.CreateFormatException("Expected \"" + token + "\".");
          TextFormat.MergeField(tokenizer, extensionRegistry, builder1);
        }
        obj = (object) builder1.WeakBuild();
      }
      else
      {
        tokenizer.Consume(":");
        switch (field.FieldType)
        {
          case FieldType.Double:
            obj = (object) tokenizer.ConsumeDouble();
            break;
          case FieldType.Float:
            obj = (object) tokenizer.ConsumeFloat();
            break;
          case FieldType.Int64:
          case FieldType.SFixed64:
          case FieldType.SInt64:
            obj = (object) tokenizer.ConsumeInt64();
            break;
          case FieldType.UInt64:
          case FieldType.Fixed64:
            obj = (object) tokenizer.ConsumeUInt64();
            break;
          case FieldType.Int32:
          case FieldType.SFixed32:
          case FieldType.SInt32:
            obj = (object) tokenizer.ConsumeInt32();
            break;
          case FieldType.Fixed32:
          case FieldType.UInt32:
            obj = (object) tokenizer.ConsumeUInt32();
            break;
          case FieldType.Bool:
            obj = (object) tokenizer.ConsumeBoolean();
            break;
          case FieldType.String:
            obj = (object) tokenizer.ConsumeString();
            break;
          case FieldType.Group:
          case FieldType.Message:
            throw new InvalidOperationException("Can't get here.");
          case FieldType.Bytes:
            obj = (object) tokenizer.ConsumeByteString();
            break;
          case FieldType.Enum:
            EnumDescriptor enumType = field.EnumType;
            if (tokenizer.LookingAtInteger())
            {
              int number = tokenizer.ConsumeInt32();
              obj = (object) enumType.FindValueByNumber(number);
              if (obj == null)
                throw tokenizer.CreateFormatExceptionPreviousToken("Enum type \"" + enumType.FullName + "\" has no value with number " + (object) number + ".");
              break;
            }
            string name = tokenizer.ConsumeIdentifier();
            obj = (object) enumType.FindValueByName(name);
            if (obj == null)
              throw tokenizer.CreateFormatExceptionPreviousToken("Enum type \"" + enumType.FullName + "\" has no value named \"" + name + "\".");
            break;
        }
      }
      if (field.IsRepeated)
        builder.WeakAddRepeatedField(field, obj);
      else
        builder.SetField(field, obj);
    }
  }
}
