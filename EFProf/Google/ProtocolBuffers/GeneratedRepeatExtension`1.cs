﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.GeneratedRepeatExtension`1
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers.Descriptors;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Google.ProtocolBuffers
{
  public sealed class GeneratedRepeatExtension<TExtensionElement> : GeneratedExtensionBase<IList<TExtensionElement>>
  {
    private GeneratedRepeatExtension(FieldDescriptor field)
      : base(field, typeof (TExtensionElement))
    {
    }

    public static GeneratedExtensionBase<IList<TExtensionElement>> CreateInstance(FieldDescriptor descriptor)
    {
      if (!descriptor.IsRepeated)
        throw new ArgumentException("Must call GeneratedRepeatExtension.CreateInstance() for repeated types.");
      return (GeneratedExtensionBase<IList<TExtensionElement>>) new GeneratedRepeatExtension<TExtensionElement>(descriptor);
    }

    public override object FromReflectionType(object value)
    {
      if (this.Descriptor.MappedType != MappedType.Message && this.Descriptor.MappedType != MappedType.Enum)
        return value;
      List<TExtensionElement> extensionElementList = new List<TExtensionElement>();
      foreach (object obj in (IEnumerable) value)
        extensionElementList.Add((TExtensionElement) this.SingularFromReflectionType(obj));
      return (object) extensionElementList;
    }
  }
}
