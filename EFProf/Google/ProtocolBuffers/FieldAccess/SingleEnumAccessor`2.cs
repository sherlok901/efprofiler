﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.FieldAccess.SingleEnumAccessor`2
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers.Descriptors;

namespace Google.ProtocolBuffers.FieldAccess
{
  internal sealed class SingleEnumAccessor<TMessage, TBuilder> : SinglePrimitiveAccessor<TMessage, TBuilder> where TMessage : IMessage<TMessage, TBuilder> where TBuilder : IBuilder<TMessage, TBuilder>
  {
    private readonly EnumDescriptor enumDescriptor;

    internal SingleEnumAccessor(FieldDescriptor field, string name)
      : base(name)
    {
      this.enumDescriptor = field.EnumType;
    }

    public override object GetValue(TMessage message)
    {
      return (object) this.enumDescriptor.FindValueByNumber((int) base.GetValue(message));
    }

    public override void SetValue(TBuilder builder, object value)
    {
      ThrowHelper.ThrowIfNull(value, nameof (value));
      EnumValueDescriptor enumValueDescriptor = (EnumValueDescriptor) value;
      base.SetValue(builder, (object) enumValueDescriptor.Number);
    }
  }
}
