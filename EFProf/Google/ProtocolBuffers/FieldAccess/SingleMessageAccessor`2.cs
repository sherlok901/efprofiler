﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.FieldAccess.SingleMessageAccessor`2
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using System;
using System.Reflection;

namespace Google.ProtocolBuffers.FieldAccess
{
  internal sealed class SingleMessageAccessor<TMessage, TBuilder> : SinglePrimitiveAccessor<TMessage, TBuilder> where TMessage : IMessage<TMessage, TBuilder> where TBuilder : IBuilder<TMessage, TBuilder>
  {
    private readonly Func<IBuilder> createBuilderDelegate;

    internal SingleMessageAccessor(string name)
      : base(name)
    {
      MethodInfo method = this.ClrType.GetMethod("CreateBuilder", ReflectionUtil.EmptyTypes);
      if (method == null)
        throw new ArgumentException("No public static CreateBuilder method declared in " + this.ClrType.Name);
      this.createBuilderDelegate = ReflectionUtil.CreateStaticUpcastDelegate(method);
    }

    private object CoerceType(object value)
    {
      Google.ProtocolBuffers.ThrowHelper.ThrowIfNull(value, nameof (value));
      if (this.ClrType.IsInstanceOfType(value))
        return value;
      return (object) this.CreateBuilder().WeakMergeFrom((IMessageLite) value).WeakBuild();
    }

    public override void SetValue(TBuilder builder, object value)
    {
      base.SetValue(builder, this.CoerceType(value));
    }

    public override IBuilder CreateBuilder()
    {
      return this.createBuilderDelegate();
    }
  }
}
