﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.FieldAccess.FieldAccessorTable`2
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers.Descriptors;
using System;

namespace Google.ProtocolBuffers.FieldAccess
{
  public sealed class FieldAccessorTable<TMessage, TBuilder> where TMessage : IMessage<TMessage, TBuilder> where TBuilder : IBuilder<TMessage, TBuilder>
  {
    private readonly IFieldAccessor<TMessage, TBuilder>[] accessors;
    private readonly MessageDescriptor descriptor;

    public MessageDescriptor Descriptor
    {
      get
      {
        return this.descriptor;
      }
    }

    public FieldAccessorTable(MessageDescriptor descriptor, string[] propertyNames)
    {
      this.descriptor = descriptor;
      this.accessors = new IFieldAccessor<TMessage, TBuilder>[descriptor.Fields.Count];
      for (int index = 0; index < this.accessors.Length; ++index)
        this.accessors[index] = FieldAccessorTable<TMessage, TBuilder>.CreateAccessor(descriptor.Fields[index], propertyNames[index]);
    }

    private static IFieldAccessor<TMessage, TBuilder> CreateAccessor(FieldDescriptor field, string name)
    {
      if (field.IsRepeated)
      {
        switch (field.MappedType)
        {
          case MappedType.Message:
            return (IFieldAccessor<TMessage, TBuilder>) new RepeatedMessageAccessor<TMessage, TBuilder>(name);
          case MappedType.Enum:
            return (IFieldAccessor<TMessage, TBuilder>) new RepeatedEnumAccessor<TMessage, TBuilder>(field, name);
          default:
            return (IFieldAccessor<TMessage, TBuilder>) new RepeatedPrimitiveAccessor<TMessage, TBuilder>(name);
        }
      }
      else
      {
        switch (field.MappedType)
        {
          case MappedType.Message:
            return (IFieldAccessor<TMessage, TBuilder>) new SingleMessageAccessor<TMessage, TBuilder>(name);
          case MappedType.Enum:
            return (IFieldAccessor<TMessage, TBuilder>) new SingleEnumAccessor<TMessage, TBuilder>(field, name);
          default:
            return (IFieldAccessor<TMessage, TBuilder>) new SinglePrimitiveAccessor<TMessage, TBuilder>(name);
        }
      }
    }

    internal IFieldAccessor<TMessage, TBuilder> this[FieldDescriptor field]
    {
      get
      {
        if (field.ContainingType != this.descriptor)
          throw new ArgumentException("FieldDescriptor does not match message type.");
        if (field.IsExtension)
          throw new ArgumentException("This type does not have extensions.");
        return this.accessors[field.Index];
      }
    }
  }
}
