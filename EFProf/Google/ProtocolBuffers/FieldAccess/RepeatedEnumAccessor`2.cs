﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.FieldAccess.RepeatedEnumAccessor`2
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers.Collections;
using Google.ProtocolBuffers.Descriptors;
using System.Collections;
using System.Collections.Generic;

namespace Google.ProtocolBuffers.FieldAccess
{
  internal sealed class RepeatedEnumAccessor<TMessage, TBuilder> : RepeatedPrimitiveAccessor<TMessage, TBuilder> where TMessage : IMessage<TMessage, TBuilder> where TBuilder : IBuilder<TMessage, TBuilder>
  {
    private readonly EnumDescriptor enumDescriptor;

    internal RepeatedEnumAccessor(FieldDescriptor field, string name)
      : base(name)
    {
      this.enumDescriptor = field.EnumType;
    }

    public override object GetValue(TMessage message)
    {
      List<EnumValueDescriptor> enumValueDescriptorList = new List<EnumValueDescriptor>();
      foreach (int number in (IEnumerable) base.GetValue(message))
        enumValueDescriptorList.Add(this.enumDescriptor.FindValueByNumber(number));
      return (object) Lists.AsReadOnly<EnumValueDescriptor>((IList<EnumValueDescriptor>) enumValueDescriptorList);
    }

    public override object GetRepeatedValue(TMessage message, int index)
    {
      return (object) this.enumDescriptor.FindValueByNumber((int) base.GetRepeatedValue(message, index));
    }

    public override void AddRepeated(TBuilder builder, object value)
    {
      ThrowHelper.ThrowIfNull(value, nameof (value));
      base.AddRepeated(builder, (object) ((EnumValueDescriptor) value).Number);
    }

    public override void SetRepeated(TBuilder builder, int index, object value)
    {
      ThrowHelper.ThrowIfNull(value, nameof (value));
      base.SetRepeated(builder, index, (object) ((EnumValueDescriptor) value).Number);
    }
  }
}
