﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.FieldAccess.SinglePrimitiveAccessor`2
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using System;
using System.Reflection;

namespace Google.ProtocolBuffers.FieldAccess
{
  internal class SinglePrimitiveAccessor<TMessage, TBuilder> : IFieldAccessor<TMessage, TBuilder> where TMessage : IMessage<TMessage, TBuilder> where TBuilder : IBuilder<TMessage, TBuilder>
  {
    private readonly Type clrType;
    private readonly Func<TMessage, object> getValueDelegate;
    private readonly Action<TBuilder, object> setValueDelegate;
    private readonly Func<TMessage, bool> hasDelegate;
    private readonly Func<TBuilder, IBuilder> clearDelegate;

    protected Type ClrType
    {
      get
      {
        return this.clrType;
      }
    }

    internal SinglePrimitiveAccessor(string name)
    {
      PropertyInfo property1 = typeof (TMessage).GetProperty(name, (Type) null, ReflectionUtil.EmptyTypes);
      PropertyInfo property2 = typeof (TBuilder).GetProperty(name, (Type) null, ReflectionUtil.EmptyTypes);
      PropertyInfo property3 = typeof (TMessage).GetProperty("Has" + name);
      MethodInfo method = typeof (TBuilder).GetMethod("Clear" + name);
      if (property1 == null || property2 == null || (property3 == null || method == null))
        throw new ArgumentException("Not all required properties/methods available");
      this.clrType = property1.PropertyType;
      this.hasDelegate = ReflectionUtil.CreateDelegateFunc<TMessage, bool>(property3.GetGetMethod());
      this.clearDelegate = ReflectionUtil.CreateDelegateFunc<TBuilder, IBuilder>(method);
      this.getValueDelegate = ReflectionUtil.CreateUpcastDelegate<TMessage>(property1.GetGetMethod());
      this.setValueDelegate = ReflectionUtil.CreateDowncastDelegate<TBuilder>(property2.GetSetMethod());
    }

    public bool Has(TMessage message)
    {
      return this.hasDelegate(message);
    }

    public void Clear(TBuilder builder)
    {
      IBuilder builder1 = this.clearDelegate(builder);
    }

    public virtual IBuilder CreateBuilder()
    {
      throw new InvalidOperationException();
    }

    public virtual object GetValue(TMessage message)
    {
      return this.getValueDelegate(message);
    }

    public virtual void SetValue(TBuilder builder, object value)
    {
      this.setValueDelegate(builder, value);
    }

    public int GetRepeatedCount(TMessage message)
    {
      throw new InvalidOperationException();
    }

    public object GetRepeatedValue(TMessage message, int index)
    {
      throw new InvalidOperationException();
    }

    public void SetRepeated(TBuilder builder, int index, object value)
    {
      throw new InvalidOperationException();
    }

    public void AddRepeated(TBuilder builder, object value)
    {
      throw new InvalidOperationException();
    }

    public object GetRepeatedWrapper(TBuilder builder)
    {
      throw new InvalidOperationException();
    }
  }
}
