﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.FieldAccess.ReflectionUtil
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using System;
using System.Reflection;

namespace Google.ProtocolBuffers.FieldAccess
{
  internal static class ReflectionUtil
  {
    internal static readonly Type[] EmptyTypes = new Type[0];

    public static Func<T, object> CreateUpcastDelegate<T>(MethodInfo method)
    {
      return (Func<T, object>) typeof (ReflectionUtil).GetMethod("CreateUpcastDelegateImpl").MakeGenericMethod(typeof (T), method.ReturnType).Invoke((object) null, new object[1]
      {
        (object) method
      });
    }

    public static Func<TSource, object> CreateUpcastDelegateImpl<TSource, TResult>(MethodInfo method)
    {
      Func<TSource, TResult> getter = ReflectionUtil.CreateDelegateFunc<TSource, TResult>(method);
      return (Func<TSource, object>) (source => (object) getter(source));
    }

    public static Action<T, object> CreateDowncastDelegate<T>(MethodInfo method)
    {
      return (Action<T, object>) typeof (ReflectionUtil).GetMethod("CreateDowncastDelegateImpl").MakeGenericMethod(typeof (T), method.GetParameters()[0].ParameterType).Invoke((object) null, new object[1]
      {
        (object) method
      });
    }

    public static Action<TSource, object> CreateDowncastDelegateImpl<TSource, TParam>(MethodInfo method)
    {
      Action<TSource, TParam> call = ReflectionUtil.CreateDelegateAction<TSource, TParam>(method);
      return (Action<TSource, object>) ((source, parameter) => call(source, (TParam) parameter));
    }

    public static Action<T, object> CreateDowncastDelegateIgnoringReturn<T>(MethodInfo method)
    {
      return (Action<T, object>) typeof (ReflectionUtil).GetMethod("CreateDowncastDelegateIgnoringReturnImpl").MakeGenericMethod(typeof (T), method.GetParameters()[0].ParameterType, method.ReturnType).Invoke((object) null, new object[1]
      {
        (object) method
      });
    }

    public static Action<TSource, object> CreateDowncastDelegateIgnoringReturnImpl<TSource, TParam, TReturn>(MethodInfo method)
    {
      Func<TSource, TParam, TReturn> call = ReflectionUtil.CreateDelegateFunc<TSource, TParam, TReturn>(method);
      TReturn @return;
      return (Action<TSource, object>) ((source, parameter) => @return = call(source, (TParam) parameter));
    }

    public static Func<IBuilder> CreateStaticUpcastDelegate(MethodInfo method)
    {
      return (Func<IBuilder>) typeof (ReflectionUtil).GetMethod("CreateStaticUpcastDelegateImpl").MakeGenericMethod(method.ReturnType).Invoke((object) null, new object[1]
      {
        (object) method
      });
    }

    public static Func<IBuilder> CreateStaticUpcastDelegateImpl<T>(MethodInfo method)
    {
      Func<T> call = ReflectionUtil.CreateDelegateFunc<T>(method);
      return (Func<IBuilder>) (() => (IBuilder) (object) call());
    }

    internal static Func<TResult> CreateDelegateFunc<TResult>(MethodInfo method)
    {
      return (Func<TResult>) (object) Delegate.CreateDelegate(typeof (Func<TResult>), (object) null, method);
    }

    internal static Func<T, TResult> CreateDelegateFunc<T, TResult>(MethodInfo method)
    {
      return (Func<T, TResult>) (object) Delegate.CreateDelegate(typeof (Func<T, TResult>), (object) null, method);
    }

    internal static Func<T1, T2, TResult> CreateDelegateFunc<T1, T2, TResult>(MethodInfo method)
    {
      return (Func<T1, T2, TResult>) (object) Delegate.CreateDelegate(typeof (Func<T1, T2, TResult>), (object) null, method);
    }

    internal static Action<T1, T2> CreateDelegateAction<T1, T2>(MethodInfo method)
    {
      return (Action<T1, T2>) (object) Delegate.CreateDelegate(typeof (Action<T1, T2>), (object) null, method);
    }
  }
}
