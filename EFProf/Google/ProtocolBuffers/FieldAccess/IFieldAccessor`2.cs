﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.FieldAccess.IFieldAccessor`2
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

namespace Google.ProtocolBuffers.FieldAccess
{
  internal interface IFieldAccessor<TMessage, TBuilder> where TMessage : IMessage<TMessage, TBuilder> where TBuilder : IBuilder<TMessage, TBuilder>
  {
    bool Has(TMessage message);

    int GetRepeatedCount(TMessage message);

    void Clear(TBuilder builder);

    IBuilder CreateBuilder();

    object GetValue(TMessage message);

    void SetValue(TBuilder builder, object value);

    object GetRepeatedValue(TMessage message, int index);

    void SetRepeated(TBuilder builder, int index, object value);

    void AddRepeated(TBuilder builder, object value);

    object GetRepeatedWrapper(TBuilder builder);
  }
}
