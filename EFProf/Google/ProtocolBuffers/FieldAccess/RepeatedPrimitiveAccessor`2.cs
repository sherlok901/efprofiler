﻿// Decompiled with JetBrains decompiler
// Type: Google.ProtocolBuffers.FieldAccess.RepeatedPrimitiveAccessor`2
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using System;
using System.Collections;
using System.Reflection;

namespace Google.ProtocolBuffers.FieldAccess
{
  internal class RepeatedPrimitiveAccessor<TMessage, TBuilder> : IFieldAccessor<TMessage, TBuilder> where TMessage : IMessage<TMessage, TBuilder> where TBuilder : IBuilder<TMessage, TBuilder>
  {
    internal static readonly Type[] EmptyTypes = new Type[0];
    private readonly Type clrType;
    private readonly Func<TMessage, object> getValueDelegate;
    private readonly Func<TBuilder, IBuilder> clearDelegate;
    private readonly Action<TBuilder, object> addValueDelegate;
    private readonly Func<TBuilder, object> getRepeatedWrapperDelegate;
    private readonly Func<TMessage, int> countDelegate;
    private readonly MethodInfo getElementMethod;
    private readonly MethodInfo setElementMethod;

    protected Type ClrType
    {
      get
      {
        return this.clrType;
      }
    }

    internal RepeatedPrimitiveAccessor(string name)
    {
      PropertyInfo property1 = typeof (TMessage).GetProperty(name + "List");
      PropertyInfo property2 = typeof (TBuilder).GetProperty(name + "List");
      PropertyInfo property3 = typeof (TMessage).GetProperty(name + "Count");
      MethodInfo method1 = typeof (TBuilder).GetMethod("Clear" + name, RepeatedPrimitiveAccessor<TMessage, TBuilder>.EmptyTypes);
      this.getElementMethod = typeof (TMessage).GetMethod("Get" + name, new Type[1]
      {
        typeof (int)
      });
      this.clrType = this.getElementMethod.ReturnType;
      MethodInfo method2 = typeof (TBuilder).GetMethod("Add" + name, new Type[1]
      {
        this.ClrType
      });
      this.setElementMethod = typeof (TBuilder).GetMethod("Set" + name, new Type[2]
      {
        typeof (int),
        this.ClrType
      });
      if (property1 == null || property2 == null || (property3 == null || method1 == null) || (method2 == null || this.getElementMethod == null || this.setElementMethod == null))
        throw new ArgumentException("Not all required properties/methods available");
      this.clearDelegate = ReflectionUtil.CreateDelegateFunc<TBuilder, IBuilder>(method1);
      this.countDelegate = ReflectionUtil.CreateDelegateFunc<TMessage, int>(property3.GetGetMethod());
      this.getValueDelegate = ReflectionUtil.CreateUpcastDelegate<TMessage>(property1.GetGetMethod());
      this.addValueDelegate = ReflectionUtil.CreateDowncastDelegateIgnoringReturn<TBuilder>(method2);
      this.getRepeatedWrapperDelegate = ReflectionUtil.CreateUpcastDelegate<TBuilder>(property2.GetGetMethod());
    }

    public bool Has(TMessage message)
    {
      throw new InvalidOperationException();
    }

    public virtual IBuilder CreateBuilder()
    {
      throw new InvalidOperationException();
    }

    public virtual object GetValue(TMessage message)
    {
      return this.getValueDelegate(message);
    }

    public void SetValue(TBuilder builder, object value)
    {
      this.Clear(builder);
      foreach (object obj in (IEnumerable) value)
        this.AddRepeated(builder, obj);
    }

    public void Clear(TBuilder builder)
    {
      IBuilder builder1 = this.clearDelegate(builder);
    }

    public int GetRepeatedCount(TMessage message)
    {
      return this.countDelegate(message);
    }

    public virtual object GetRepeatedValue(TMessage message, int index)
    {
      return this.getElementMethod.Invoke((object) message, new object[1]
      {
        (object) index
      });
    }

    public virtual void SetRepeated(TBuilder builder, int index, object value)
    {
      Google.ProtocolBuffers.ThrowHelper.ThrowIfNull(value, nameof (value));
      this.setElementMethod.Invoke((object) builder, new object[2]
      {
        (object) index,
        value
      });
    }

    public virtual void AddRepeated(TBuilder builder, object value)
    {
      Google.ProtocolBuffers.ThrowHelper.ThrowIfNull(value, nameof (value));
      this.addValueDelegate(builder, value);
    }

    public object GetRepeatedWrapper(TBuilder builder)
    {
      return this.getRepeatedWrapperDelegate(builder);
    }
  }
}
