<ResourceDictionary xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
                    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
                    xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
                    xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                    xmlns:i="http://schemas.microsoft.com/expression/2010/interactivity"
                    xmlns:ei="http://schemas.microsoft.com/expression/2010/interactions">
  <ControlTemplate x:Key="ButtonTemplate" TargetType="{x:Type Button}">
    <Border x:Name="border" Padding="1" CornerRadius="3" MinWidth="{TemplateBinding FrameworkElement.MinWidth}"
            MinHeight="{TemplateBinding FrameworkElement.MinHeight}"
            BorderBrush="{TemplateBinding Control.BorderBrush}" BorderThickness="1"
            Margin="{TemplateBinding FrameworkElement.Margin}"
            Background="{TemplateBinding Control.Background}" RenderTransformOrigin="0.5,0.5">
      <VisualStateManager.VisualStateGroups>
        <VisualStateGroup x:Name="CommonStates">
          <VisualState x:Name="Normal"/>
          <VisualState x:Name="MouseOver">
            <Storyboard>
              <ObjectAnimationUsingKeyFrames Storyboard.TargetProperty="(Border.BorderBrush)" Storyboard.TargetName="border">
                <DiscreteObjectKeyFrame KeyTime="0" Value="{StaticResource MouseOverBrush}"/>
              </ObjectAnimationUsingKeyFrames>
            </Storyboard>
          </VisualState>
          <VisualState x:Name="Pressed">
            <Storyboard>
              <DoubleAnimation Duration="0:0:0.05" To="0.95"
                               Storyboard.TargetProperty="(UIElement.RenderTransform).(ScaleTransform.ScaleX)"
                               Storyboard.TargetName="border"/>
              <DoubleAnimation Duration="0:0:0.05" To="0.95"
                               Storyboard.TargetProperty="(UIElement.RenderTransform).(ScaleTransform.ScaleY)"
                               Storyboard.TargetName="border"/>
              <ObjectAnimationUsingKeyFrames Storyboard.TargetProperty="(Border.BorderBrush)" Storyboard.TargetName="border">
                <DiscreteObjectKeyFrame KeyTime="0" Value="{StaticResource MouseOverBrush}"/>
              </ObjectAnimationUsingKeyFrames>
            </Storyboard>
          </VisualState>
          <VisualState x:Name="Disabled">
            <Storyboard>
              <ObjectAnimationUsingKeyFrames Storyboard.TargetProperty="(Border.Background)" Storyboard.TargetName="border">
                <DiscreteObjectKeyFrame KeyTime="0" Value="{StaticResource DisabledButtonBackground}"/>
              </ObjectAnimationUsingKeyFrames>
              <ObjectAnimationUsingKeyFrames Storyboard.TargetProperty="(Border.BorderBrush)" Storyboard.TargetName="border">
                <DiscreteObjectKeyFrame KeyTime="0" Value="{StaticResource DarkAccent}"/>
              </ObjectAnimationUsingKeyFrames>
            </Storyboard>
          </VisualState>
        </VisualStateGroup>
      </VisualStateManager.VisualStateGroups>
      <UIElement.RenderTransform>
        <ScaleTransform/>
      </UIElement.RenderTransform>
      <Grid>
        <Grid.RowDefinitions>
          <RowDefinition Height="*"/>
          <RowDefinition Height="*"/>
        </Grid.RowDefinitions>
        <ContentPresenter Grid.RowSpan="2" Margin="{TemplateBinding Control.Padding}" HorizontalAlignment="Center"
                          VerticalAlignment="Center"/>
      </Grid>
    </Border>
  </ControlTemplate>
  <Style x:Key="FocusVisual">
    <Setter Property="Control.Template">
      <Setter.Value>
        <ControlTemplate>
          <Rectangle Margin="2" SnapsToDevicePixels="true"
                     Stroke="{DynamicResource {x:Static SystemColors.ControlTextBrushKey}}" StrokeThickness="1"
                     StrokeDashArray="1 2"/>
        </ControlTemplate>
      </Setter.Value>
    </Setter>
  </Style>
  <SolidColorBrush x:Key="Button.Static.Background" Color="#FFDDDDDD"/>
  <SolidColorBrush x:Key="Button.Static.Border" Color="#FF707070"/>
  <SolidColorBrush x:Key="Button.MouseOver.Background" Color="#FFBEE6FD"/>
  <SolidColorBrush x:Key="Button.MouseOver.Border" Color="#FF3C7FB1"/>
  <SolidColorBrush x:Key="Button.Pressed.Background" Color="#FFC4E5F6"/>
  <SolidColorBrush x:Key="Button.Pressed.Border" Color="#FF2C628B"/>
  <SolidColorBrush x:Key="Button.Disabled.Background" Color="#FFF4F4F4"/>
  <SolidColorBrush x:Key="Button.Disabled.Border" Color="#FFADB2B5"/>
  <SolidColorBrush x:Key="Button.Disabled.Foreground" Color="#FF838383"/>
  <Style x:Key="ToggleButtonBarStyle" TargetType="{x:Type ToggleButton}">
    <Setter Property="FrameworkElement.FocusVisualStyle" Value="{StaticResource FocusVisual}"/>
    <Setter Property="Control.Background" Value="{StaticResource Button.Static.Background}"/>
    <Setter Property="Control.BorderBrush" Value="{StaticResource WindowBackground}"/>
    <Setter Property="Control.Foreground" Value="{DynamicResource {x:Static SystemColors.ControlTextBrushKey}}"/>
    <Setter Property="Control.BorderThickness" Value="1"/>
    <Setter Property="Control.HorizontalContentAlignment" Value="Center"/>
    <Setter Property="Control.VerticalContentAlignment" Value="Center"/>
    <Setter Property="Control.Padding" Value="1"/>
    <Setter Property="Control.Template">
      <Setter.Value>
        <ControlTemplate TargetType="{x:Type ToggleButton}">
          <Border x:Name="border" SnapsToDevicePixels="true" MinWidth="24">
            <ContentPresenter x:Name="contentPresenter" Focusable="false"
                              HorizontalAlignment="{TemplateBinding Control.HorizontalContentAlignment}"
                              Margin="{TemplateBinding Control.Padding}" RecognizesAccessKey="true"
                              SnapsToDevicePixels="{TemplateBinding UIElement.SnapsToDevicePixels}"
                              VerticalAlignment="{TemplateBinding Control.VerticalContentAlignment}"/>
          </Border>
          <ControlTemplate.Triggers>
            <Trigger Property="Button.IsDefaulted" Value="true">
              <Setter TargetName="border" Value="{DynamicResource {x:Static SystemColors.HighlightBrushKey}}"
                      Property="Border.Background"/>
              <Setter TargetName="border" Value="{StaticResource WindowBackground}" Property="Border.BorderBrush"/>
            </Trigger>
            <Trigger Property="UIElement.IsMouseOver" Value="true">
              <Setter TargetName="border" Value="{StaticResource BrushButtonBarBackgroundChecked}"
                      Property="Border.Background"/>
              <Setter TargetName="border" Value="{StaticResource WindowBackground}" Property="Border.BorderBrush"/>
            </Trigger>
            <Trigger Property="ButtonBase.IsPressed" Value="true">
              <Setter TargetName="border" Value="{StaticResource BrushButtonBarBackgroundChecked}"
                      Property="Border.Background"/>
              <Setter TargetName="border" Value="{StaticResource WindowBackground}" Property="Border.BorderBrush"/>
            </Trigger>
            <Trigger Property="ToggleButton.IsChecked" Value="true">
              <Setter TargetName="border" Value="{StaticResource BrushButtonBarBackgroundChecked}"
                      Property="Border.Background"/>
              <Setter TargetName="border" Value="{StaticResource WindowBackground}" Property="Border.BorderBrush"/>
            </Trigger>
            <Trigger Property="UIElement.IsEnabled" Value="false">
              <Setter TargetName="border" Value="{StaticResource Button.Disabled.Background}"
                      Property="Border.Background"/>
              <Setter TargetName="border" Value="{StaticResource Button.Disabled.Border}" Property="Border.BorderBrush"/>
              <Setter TargetName="contentPresenter" Value="{StaticResource Button.Disabled.Foreground}"
                      Property="TextElement.Foreground"/>
            </Trigger>
          </ControlTemplate.Triggers>
        </ControlTemplate>
      </Setter.Value>
    </Setter>
    <Setter Property="Control.FontFamily" Value="Segoe UI"/>
    <Setter Property="Control.FontWeight" Value="Bold"/>
  </Style>
  <Style x:Key="ButtonBarStyle" TargetType="{x:Type Button}">
    <Setter Property="FrameworkElement.FocusVisualStyle" Value="{StaticResource FocusVisual}"/>
    <Setter Property="Control.Background" Value="{StaticResource Button.Static.Background}"/>
    <Setter Property="Control.BorderBrush" Value="{StaticResource WindowBackground}"/>
    <Setter Property="Control.Foreground" Value="{DynamicResource {x:Static SystemColors.ControlTextBrushKey}}"/>
    <Setter Property="Control.BorderThickness" Value="1"/>
    <Setter Property="Control.HorizontalContentAlignment" Value="Center"/>
    <Setter Property="Control.VerticalContentAlignment" Value="Center"/>
    <Setter Property="Control.Padding" Value="1"/>
    <Setter Property="Control.Template">
      <Setter.Value>
        <ControlTemplate TargetType="{x:Type Button}">
          <Border x:Name="border" SnapsToDevicePixels="true" MinWidth="24">
            <ContentPresenter x:Name="contentPresenter" Focusable="false"
                              HorizontalAlignment="{TemplateBinding Control.HorizontalContentAlignment}"
                              Margin="{TemplateBinding Control.Padding}" RecognizesAccessKey="true"
                              SnapsToDevicePixels="{TemplateBinding UIElement.SnapsToDevicePixels}"
                              VerticalAlignment="{TemplateBinding Control.VerticalContentAlignment}"/>
          </Border>
          <ControlTemplate.Triggers>
            <Trigger Property="Button.IsDefaulted" Value="true">
              <Setter TargetName="border" Value="{DynamicResource {x:Static SystemColors.HighlightBrushKey}}"
                      Property="Border.Background"/>
              <Setter TargetName="border" Value="{StaticResource WindowBackground}" Property="Border.BorderBrush"/>
            </Trigger>
            <Trigger Property="UIElement.IsMouseOver" Value="true">
              <Setter TargetName="border" Value="{StaticResource BrushButtonBarBackgroundChecked}"
                      Property="Border.Background"/>
              <Setter TargetName="border" Value="{StaticResource WindowBackground}" Property="Border.BorderBrush"/>
            </Trigger>
            <Trigger Property="ButtonBase.IsPressed" Value="true">
              <Setter TargetName="border" Value="{StaticResource BrushButtonBarBackgroundChecked}"
                      Property="Border.Background"/>
              <Setter TargetName="border" Value="{StaticResource WindowBackground}" Property="Border.BorderBrush"/>
            </Trigger>
            <Trigger Property="UIElement.IsEnabled" Value="false">
              <Setter TargetName="border" Value="{StaticResource Button.Disabled.Background}"
                      Property="Border.Background"/>
              <Setter TargetName="border" Value="{StaticResource Button.Disabled.Border}" Property="Border.BorderBrush"/>
              <Setter TargetName="contentPresenter" Value="{StaticResource Button.Disabled.Foreground}"
                      Property="TextElement.Foreground"/>
            </Trigger>
          </ControlTemplate.Triggers>
        </ControlTemplate>
      </Setter.Value>
    </Setter>
    <Setter Property="Control.FontFamily" Value="Segoe UI"/>
    <Setter Property="Control.FontWeight" Value="Bold"/>
  </Style>
</ResourceDictionary>