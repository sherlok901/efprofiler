﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Appender.ProfiledDataAccess.ProfiledDbDataAdapter
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using System.Data;
using System.Data.Common;

namespace HibernatingRhinos.Profiler.Appender.ProfiledDataAccess
{
  public class ProfiledDbDataAdapter : DbDataAdapter
  {
    private readonly DbDataAdapter adapter;

    public ProfiledDbDataAdapter(DbDataAdapter adapter)
    {
      this.adapter = adapter;
    }

    public override bool ReturnProviderSpecificTypes
    {
      get
      {
        return this.adapter.ReturnProviderSpecificTypes;
      }
      set
      {
        this.adapter.ReturnProviderSpecificTypes = value;
      }
    }

    public override int UpdateBatchSize
    {
      get
      {
        return this.adapter.UpdateBatchSize;
      }
      set
      {
        this.adapter.UpdateBatchSize = value;
      }
    }

    protected override void Dispose(bool disposing)
    {
      this.adapter.Dispose();
    }

    public override int Fill(DataSet dataSet)
    {
      if (this.SelectCommand != null)
        this.adapter.SelectCommand = ((ProfiledCommand) this.SelectCommand).Inner;
      return this.adapter.Fill(dataSet);
    }

    public override DataTable[] FillSchema(DataSet dataSet, SchemaType schemaType)
    {
      if (this.SelectCommand != null)
        this.adapter.SelectCommand = ((ProfiledCommand) this.SelectCommand).Inner;
      return this.adapter.FillSchema(dataSet, schemaType);
    }

    public override IDataParameter[] GetFillParameters()
    {
      return this.adapter.GetFillParameters();
    }

    public override bool ShouldSerializeAcceptChangesDuringFill()
    {
      return this.adapter.ShouldSerializeAcceptChangesDuringFill();
    }

    public override bool ShouldSerializeFillLoadOption()
    {
      return this.adapter.ShouldSerializeFillLoadOption();
    }

    public override int Update(DataSet dataSet)
    {
      if (this.UpdateCommand != null)
        this.adapter.UpdateCommand = ((ProfiledCommand) this.UpdateCommand).Inner;
      if (this.InsertCommand != null)
        this.adapter.InsertCommand = ((ProfiledCommand) this.InsertCommand).Inner;
      if (this.DeleteCommand != null)
        this.adapter.DeleteCommand = ((ProfiledCommand) this.DeleteCommand).Inner;
      return this.adapter.Update(dataSet);
    }

    public override string ToString()
    {
      return this.adapter.ToString();
    }
  }
}
