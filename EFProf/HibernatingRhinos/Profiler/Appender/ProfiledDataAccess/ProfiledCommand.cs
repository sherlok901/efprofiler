﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Appender.ProfiledDataAccess.ProfiledCommand
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using System;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Reflection;
using System.Text;

namespace HibernatingRhinos.Profiler.Appender.ProfiledDataAccess
{
  public class ProfiledCommand : DbCommand
  {
    private readonly DbCommand inner;
    private readonly IDbAppender appender;
    private readonly DbReaderWrappingMode wrapReaders;
    private ProfiledConnection connection;

    public ProfiledCommand(DbCommand inner, ProfiledConnection connection, IDbAppender appender, DbReaderWrappingMode wrapReaders)
    {
      this.inner = inner;
      this.connection = connection;
      this.appender = appender;
      this.wrapReaders = wrapReaders;
    }

    public ProfiledCommand(DbCommand inner, IDbAppender appender, DbReaderWrappingMode wrapReaders)
    {
      this.inner = inner;
      this.appender = appender;
      this.wrapReaders = wrapReaders;
    }

    public DbCommand Inner
    {
      get
      {
        return this.inner;
      }
    }

    public override ISite Site
    {
      get
      {
        return this.inner.Site;
      }
      set
      {
        this.inner.Site = value;
      }
    }

    public override void Cancel()
    {
      this.inner.Cancel();
    }

    protected override DbParameter CreateDbParameter()
    {
      return this.inner.CreateParameter();
    }

    protected override DbDataReader ExecuteDbDataReader(CommandBehavior behavior)
    {
      Guid guid = Guid.NewGuid();
      this.LogCommand(guid);
      Stopwatch stopwatch = Stopwatch.StartNew();
      DbDataReader dbDataReader;
      try
      {
        dbDataReader = this.inner.ExecuteReader(behavior);
      }
      catch (Exception ex)
      {
        this.appender.StatementError(this.connection.ConnectionId, ex);
        throw;
      }
      this.appender.CommandDurationAndRowCount(this.connection.ConnectionId, stopwatch.ElapsedMilliseconds, new int?(dbDataReader.RecordsAffected));
      switch (this.wrapReaders)
      {
        case DbReaderWrappingMode.ProfiledDataReader:
          return (DbDataReader) new ProfiledDataReader(dbDataReader, this.inner, this.connection.ConnectionId, guid, this.appender);
        case DbReaderWrappingMode.ProxiedSqlDataReader:
          return this.CreateProxiedDataReader(guid, dbDataReader);
        default:
          throw new ArgumentOutOfRangeException();
      }
    }

    private DbDataReader CreateProxiedDataReader(Guid statementId, DbDataReader reader)
    {
      return (DbDataReader) new ProxiedDataReader(reader, this.connection.ConnectionId, statementId, this.appender).GetTransparentProxy();
    }

    private void LogCommand(Guid statementId)
    {
      string commandText = this.inner.CommandText;
      StringBuilder stringBuilder = new StringBuilder();
      if (this.inner.CommandType == CommandType.StoredProcedure && !commandText.StartsWith("EXEC ", StringComparison.OrdinalIgnoreCase) && !commandText.StartsWith("EXECUTE ", StringComparison.OrdinalIgnoreCase))
        stringBuilder.Append("EXEC ");
      stringBuilder.AppendLine(commandText).AppendLine("-- Parameters:");
      foreach (IDbDataParameter parameter in this.Parameters)
      {
        string str = parameter.ParameterName;
        if (!str.StartsWith("@") && !str.StartsWith(":") && !str.StartsWith("?"))
          str = "@" + str;
        stringBuilder.Append("-- ").Append(str).Append(" = [-[").Append(ProfiledCommand.GetParameterValue((IDataParameter) parameter)).Append("]-] [-[").Append((object) parameter.DbType).Append(" (").Append(parameter.Size).AppendLine(")]-]");
      }
      this.appender.StatementExecuted(this.connection.ConnectionId, statementId, stringBuilder.ToString());
    }

    private static object GetParameterValue(IDataParameter parameter)
    {
      if (parameter.Value == DBNull.Value)
        return (object) "NULL";
      if (!(parameter.Value is byte[]))
        return parameter.Value;
      StringBuilder stringBuilder = new StringBuilder("0x");
      foreach (byte num in (byte[]) parameter.Value)
        stringBuilder.Append(num.ToString("X2"));
      return (object) stringBuilder.ToString();
    }

    public override int ExecuteNonQuery()
    {
      this.LogCommand(Guid.NewGuid());
      Stopwatch stopwatch = Stopwatch.StartNew();
      int num;
      try
      {
        num = this.inner.ExecuteNonQuery();
      }
      catch (Exception ex)
      {
        this.appender.StatementError(this.connection.ConnectionId, ex);
        throw;
      }
      this.appender.CommandDurationAndRowCount(this.connection.ConnectionId, stopwatch.ElapsedMilliseconds, new int?(num));
      return num;
    }

    public override object ExecuteScalar()
    {
      this.LogCommand(Guid.NewGuid());
      Stopwatch stopwatch = Stopwatch.StartNew();
      object obj;
      try
      {
        obj = this.inner.ExecuteScalar();
      }
      catch (Exception ex)
      {
        this.appender.StatementError(this.connection.ConnectionId, ex);
        throw;
      }
      this.appender.CommandDurationAndRowCount(this.connection.ConnectionId, stopwatch.ElapsedMilliseconds, new int?());
      return obj;
    }

    public override void Prepare()
    {
      this.inner.Prepare();
    }

    public override string CommandText
    {
      get
      {
        return this.inner.CommandText;
      }
      set
      {
        this.inner.CommandText = value;
      }
    }

    public override int CommandTimeout
    {
      get
      {
        return this.inner.CommandTimeout;
      }
      set
      {
        this.inner.CommandTimeout = value;
      }
    }

    public override CommandType CommandType
    {
      get
      {
        return this.inner.CommandType;
      }
      set
      {
        this.inner.CommandType = value;
      }
    }

    protected override DbConnection DbConnection
    {
      get
      {
        return (DbConnection) this.connection;
      }
      set
      {
        this.connection = value as ProfiledConnection;
        if (this.connection == null && value != null)
        {
          if (value.GetType().Name == "LINQPadDbConnection")
            this.connection = new ProfiledConnection(value, this.appender, this.wrapReaders, Guid.NewGuid(), (DbProviderFactory) null);
          if (value.GetType().Name == "OracleConnection")
            this.connection = new ProfiledConnection(value, this.appender, this.wrapReaders, Guid.NewGuid(), (DbProviderFactory) null);
          if (this.connection == null)
            throw new InvalidOperationException("Got unexpected type of " + value.GetType().FullName + " instead of a ProfiledConnection");
        }
        if (this.inner == null)
          return;
        this.inner.Connection = this.connection != null ? this.connection.Inner : (DbConnection) null;
      }
    }

    protected override DbParameterCollection DbParameterCollection
    {
      get
      {
        return this.inner.Parameters;
      }
    }

    protected override DbTransaction DbTransaction
    {
      get
      {
        if (this.inner.Transaction == null)
          return (DbTransaction) null;
        return (DbTransaction) new ProfiledTransaction(this.inner.Transaction, this.appender, this.wrapReaders, this.connection);
      }
      set
      {
        ProfiledTransaction profiledTransaction = value as ProfiledTransaction;
        this.inner.Transaction = profiledTransaction != null ? profiledTransaction.Inner : (DbTransaction) null;
      }
    }

    public override bool DesignTimeVisible
    {
      get
      {
        return this.inner.DesignTimeVisible;
      }
      set
      {
        this.inner.DesignTimeVisible = value;
      }
    }

    public override UpdateRowSource UpdatedRowSource
    {
      get
      {
        return this.inner.UpdatedRowSource;
      }
      set
      {
        this.inner.UpdatedRowSource = value;
      }
    }

    public bool BindByName
    {
      get
      {
        PropertyInfo property = this.inner.GetType().GetProperty(nameof (BindByName));
        if (property == null)
          return false;
        return (bool) property.GetValue((object) this.inner, (object[]) null);
      }
      set
      {
        PropertyInfo property = this.inner.GetType().GetProperty(nameof (BindByName));
        if (property == null)
          return;
        property.SetValue((object) this.inner, (object) value, (object[]) null);
      }
    }

    protected override void Dispose(bool disposing)
    {
      this.inner.Dispose();
      base.Dispose(disposing);
    }
  }
}
