﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Appender.ProfiledDataAccess.ProxiedDataReader
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using System;
using System.Data.Common;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Messaging;
using System.Runtime.Remoting.Proxies;

namespace HibernatingRhinos.Profiler.Appender.ProfiledDataAccess
{
  public class ProxiedDataReader : RealProxy
  {
    private readonly DbDataReader dataReader;
    private readonly Guid connectionId;
    private readonly Guid statementId;
    private readonly IDbAppender appender;
    private int rowCount;

    public ProxiedDataReader(DbDataReader dataReader, Guid connectionId, Guid statementId, IDbAppender appender)
      : base(dataReader.GetType())
    {
      this.dataReader = dataReader;
      this.connectionId = connectionId;
      this.statementId = statementId;
      this.appender = appender;
    }

    public override IMessage Invoke(IMessage msg)
    {
      IMethodCallMessage reqMsg = (IMethodCallMessage) msg;
      switch (reqMsg.MethodName)
      {
        case "Read":
          IMethodReturnMessage methodReturnMessage = RemotingServices.ExecuteMessage((MarshalByRefObject) this.dataReader, reqMsg);
          if ((bool) methodReturnMessage.ReturnValue)
            ++this.rowCount;
          return (IMessage) methodReturnMessage;
        case "Dispose":
        case "Close":
          this.appender.StatementRowCount(this.connectionId, this.statementId, this.rowCount);
          break;
      }
      return (IMessage) RemotingServices.ExecuteMessage((MarshalByRefObject) this.dataReader, reqMsg);
    }
  }
}
