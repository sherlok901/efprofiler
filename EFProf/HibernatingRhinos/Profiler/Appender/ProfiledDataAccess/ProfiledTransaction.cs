﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Appender.ProfiledDataAccess.ProfiledTransaction
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using System;
using System.Data;
using System.Data.Common;
using System.Reflection;

namespace HibernatingRhinos.Profiler.Appender.ProfiledDataAccess
{
  public class ProfiledTransaction : DbTransaction
  {
    private readonly DbTransaction inner;
    private readonly IDbAppender appender;
    private readonly DbReaderWrappingMode wrapReaders;
    private readonly ProfiledConnection connection;

    public ProfiledTransaction(DbTransaction inner, IDbAppender appender, DbReaderWrappingMode wrapReaders, ProfiledConnection connection)
    {
      this.inner = inner;
      this.appender = appender;
      this.wrapReaders = wrapReaders;
      this.connection = connection;
      appender.TransactionBegan(connection.ConnectionId, inner.IsolationLevel);
    }

    public override void Commit()
    {
      this.inner.Commit();
      this.appender.TransactionCommit(this.connection.ConnectionId);
    }

    public override void Rollback()
    {
      this.inner.Rollback();
      this.appender.TransactionRolledBack(this.connection.ConnectionId);
    }

    protected override DbConnection DbConnection
    {
      get
      {
        return (DbConnection) this.connection;
      }
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing)
      {
        this.inner.Dispose();
        this.appender.TransactionDisposed(this.connection.ConnectionId);
      }
      base.Dispose(disposing);
    }

    public override IsolationLevel IsolationLevel
    {
      get
      {
        return this.inner.IsolationLevel;
      }
    }

    public DbTransaction Inner
    {
      get
      {
        return this.inner;
      }
    }

    public void Save(string name)
    {
      MethodInfo method = this.inner.GetType().GetMethod(nameof (Save));
      if (method == null)
        throw new NotSupportedException("Save(name) isn't supported by " + (object) this.inner);
      method.Invoke((object) this.inner, (object[]) new string[1]
      {
        name
      });
    }

    public void Rollback(string name)
    {
      MethodInfo method = this.inner.GetType().GetMethod(nameof (Rollback), new Type[1]
      {
        typeof (string)
      });
      if (method == null)
        throw new NotSupportedException("Rollback(name) isn't supported by " + (object) this.inner);
      method.Invoke((object) this.inner, (object[]) new string[1]
      {
        name
      });
    }
  }
}
