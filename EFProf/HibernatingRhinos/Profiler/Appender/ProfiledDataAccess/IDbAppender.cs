﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Appender.ProfiledDataAccess.IDbAppender
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using System;
using System.Transactions;

namespace HibernatingRhinos.Profiler.Appender.ProfiledDataAccess
{
  public interface IDbAppender
  {
    void ConnectionStarted(Guid connectionId);

    void ConnectionDisposed(Guid connectionId);

    void StatementExecuted(Guid connectionId, Guid statementId, string statement);

    void CommandDurationAndRowCount(Guid connectionId, long milliseconds, int? rowCount);

    void StatementError(Guid connectionId, Exception exception);

    void StatementRowCount(Guid connectionId, Guid statementId, int rowCount);

    void TransactionBegan(Guid connectionId, System.Data.IsolationLevel isolationLevel);

    void TransactionCommit(Guid connectionId);

    void TransactionRolledBack(Guid connectionId);

    void TransactionDisposed(Guid connectionId);

    void DtcTransactionEnlisted(Guid connectionId, System.Transactions.IsolationLevel isolationLevel);

    void DtcTransactionCompleted(Guid connectionId, TransactionStatus status);

    bool ReportConnectionDispose { get; }

    bool ReportConnectionOnOpen { get; }

    void SessionConnectionString(Guid connectionId, string connectionString);
  }
}
