﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Appender.ProfiledDataAccess.ProfiledConnection
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using System;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Transactions;

namespace HibernatingRhinos.Profiler.Appender.ProfiledDataAccess
{
  public class ProfiledConnection : DbConnection
  {
    private readonly Guid connectionId;
    private DbConnection inner;
    private readonly IDbAppender appender;
    private readonly DbReaderWrappingMode wrapReaders;
    private readonly DbProviderFactory providerFactory;
    private bool reportedStart;

    public static event Action<ProfiledConnection> OnProfiledConnectionCreated;

    public static event Action<ProfiledConnection> OnProfiledConnectionDisposed;

    public ProfiledConnection(DbConnection inner, IDbAppender appender, DbReaderWrappingMode wrapReaders, Guid connectionId, DbProviderFactory providerFactory)
    {
      this.SetInner(inner);
      this.providerFactory = providerFactory;
      this.connectionId = connectionId;
      this.appender = appender;
      this.wrapReaders = wrapReaders;
      if (!appender.ReportConnectionOnOpen)
      {
        appender.ConnectionStarted(connectionId);
        this.reportedStart = true;
      }
      Action<ProfiledConnection> connectionCreated = ProfiledConnection.OnProfiledConnectionCreated;
      if (connectionCreated == null)
        return;
      connectionCreated(this);
    }

    private void SetInner(DbConnection dbConnection)
    {
      for (ProfiledConnection profiledConnection = dbConnection as ProfiledConnection; profiledConnection != null; profiledConnection = dbConnection as ProfiledConnection)
        dbConnection = profiledConnection.inner;
      this.inner = dbConnection;
    }

    public override ISite Site
    {
      get
      {
        return this.inner.Site;
      }
      set
      {
        this.inner.Site = value;
      }
    }

    protected override DbTransaction BeginDbTransaction(System.Data.IsolationLevel isolationLevel)
    {
      return (DbTransaction) new ProfiledTransaction(this.inner.BeginTransaction(isolationLevel), this.appender, this.wrapReaders, this);
    }

    public override void Close()
    {
      Action<ProfiledConnection> connectionDisposed = ProfiledConnection.OnProfiledConnectionDisposed;
      if (connectionDisposed != null)
        connectionDisposed(this);
      this.inner.Close();
    }

    public override void ChangeDatabase(string databaseName)
    {
      this.inner.ChangeDatabase(databaseName);
    }

    protected override DbCommand CreateDbCommand()
    {
      return (DbCommand) new ProfiledCommand(this.inner.CreateCommand(), this, this.appender, this.wrapReaders);
    }

    public override void EnlistTransaction(Transaction transaction)
    {
      this.inner.EnlistTransaction(transaction);
      if (transaction == (Transaction) null)
        return;
      transaction.TransactionCompleted += new TransactionCompletedEventHandler(this.OnDtcTransactionCompleted);
      this.appender.DtcTransactionEnlisted(this.connectionId, transaction.IsolationLevel);
    }

    private void OnDtcTransactionCompleted(object sender, TransactionEventArgs args)
    {
      TransactionStatus status;
      try
      {
        status = args.Transaction.TransactionInformation.Status;
      }
      catch (ObjectDisposedException ex)
      {
        status = TransactionStatus.Aborted;
      }
      this.appender.DtcTransactionCompleted(this.connectionId, status);
    }

    public override DataTable GetSchema()
    {
      return this.inner.GetSchema();
    }

    public override DataTable GetSchema(string collectionName)
    {
      return this.inner.GetSchema(collectionName);
    }

    public override DataTable GetSchema(string collectionName, string[] restrictionValues)
    {
      return this.inner.GetSchema(collectionName, restrictionValues);
    }

    public override void Open()
    {
      if (this.appender.ReportConnectionOnOpen && !this.reportedStart)
      {
        this.appender.ConnectionStarted(this.connectionId);
        this.reportedStart = true;
      }
      if (!ProfilerInfrastructure.IgnoreConnectionStrings)
        this.appender.SessionConnectionString(this.connectionId, this.inner.ConnectionString + "|" + this.inner.GetType().FullName);
      this.inner.Open();
    }

    public override string ConnectionString
    {
      get
      {
        return this.inner.ConnectionString;
      }
      set
      {
        this.inner.ConnectionString = value;
      }
    }

    public override int ConnectionTimeout
    {
      get
      {
        return this.inner.ConnectionTimeout;
      }
    }

    public override string Database
    {
      get
      {
        return this.inner.Database;
      }
    }

    public override string DataSource
    {
      get
      {
        return this.inner.DataSource;
      }
    }

    public override string ServerVersion
    {
      get
      {
        return this.inner.ServerVersion;
      }
    }

    public override ConnectionState State
    {
      get
      {
        return this.inner.State;
      }
    }

    public DbConnection Inner
    {
      get
      {
        return this.inner;
      }
    }

    public Guid ConnectionId
    {
      get
      {
        return this.connectionId;
      }
    }

    public override event StateChangeEventHandler StateChange
    {
      add
      {
        this.inner.StateChange += value;
      }
      remove
      {
        this.inner.StateChange -= value;
      }
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing)
      {
        this.NotifyClosing();
        this.inner.Dispose();
      }
      base.Dispose(disposing);
    }

    private void NotifyClosing()
    {
      if (!this.reportedStart || !this.appender.ReportConnectionDispose)
        return;
      this.appender.ConnectionDisposed(this.connectionId);
    }

    protected override object GetService(Type service)
    {
      return ((IServiceProvider) this.inner).GetService(service);
    }

    protected override DbProviderFactory DbProviderFactory
    {
      get
      {
        return this.providerFactory;
      }
    }
  }
}
