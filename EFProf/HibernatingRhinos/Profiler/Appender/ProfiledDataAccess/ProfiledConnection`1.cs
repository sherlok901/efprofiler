﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Appender.ProfiledDataAccess.ProfiledConnection`1
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using System;
using System.Data.Common;

namespace HibernatingRhinos.Profiler.Appender.ProfiledDataAccess
{
  public class ProfiledConnection<TInnerProviderFactory> : ProfiledConnection where TInnerProviderFactory : DbProviderFactory
  {
    public ProfiledConnection(DbConnection inner, IDbAppender appender, DbReaderWrappingMode wrapReaders, Guid connectionId, DbProviderFactory providerFactory)
      : base(inner, appender, wrapReaders, connectionId, providerFactory)
    {
    }
  }
}
