﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Appender.UrlGetter
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using System;
using System.Reflection;

namespace HibernatingRhinos.Profiler.Appender
{
  public class UrlGetter
  {
    private readonly bool hasWcfAssemblies;
    private readonly MethodInfo wcfCurrent;
    private readonly MethodInfo wcfIncomingMessageHeaders;
    private readonly MethodInfo wcfAction;

    public UrlGetter()
    {
      try
      {
        Assembly assembly = Assembly.LoadWithPartialName("System.ServiceModel");
        if (assembly == null)
          return;
        Type type = assembly.GetType("System.ServiceModel.OperationContext");
        this.wcfCurrent = type.GetProperty("Current", BindingFlags.Static | BindingFlags.Public | BindingFlags.FlattenHierarchy).GetGetMethod();
        this.wcfIncomingMessageHeaders = type.GetProperty("IncomingMessageHeaders", BindingFlags.Instance | BindingFlags.Public | BindingFlags.FlattenHierarchy).GetGetMethod();
        this.wcfAction = assembly.GetType("System.ServiceModel.Channels.MessageHeaders").GetProperty("Action", BindingFlags.Instance | BindingFlags.Public | BindingFlags.FlattenHierarchy).GetGetMethod();
        if (this.wcfCurrent == null || this.wcfIncomingMessageHeaders == null || this.wcfAction == null)
          return;
        this.hasWcfAssemblies = true;
      }
      catch (Exception ex)
      {
      }
    }

    public string GetUrl()
    {
      try
      {
        if (!string.IsNullOrEmpty(ProfilerIntegration.CurrentSessionContext))
          return ProfilerIntegration.CurrentSessionContext;
        string currentUrl = ProfilerHttpContext.GetCurrentUrl();
        if (!string.IsNullOrEmpty(currentUrl))
          return currentUrl;
        if (!this.hasWcfAssemblies)
          return string.Empty;
        object obj1 = this.wcfCurrent.Invoke((object) null, (object[]) null);
        if (obj1 == null)
          return string.Empty;
        object obj2 = this.wcfIncomingMessageHeaders.Invoke(obj1, (object[]) null);
        if (obj2 == null)
          return string.Empty;
        return (string) this.wcfAction.Invoke(obj2, (object[]) null) ?? string.Empty;
      }
      catch
      {
        return string.Empty;
      }
    }
  }
}
