﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Appender.ProfilerInfrastructure
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using HibernatingRhinos.Profiler.Appender.Channels;
using HibernatingRhinos.Profiler.Appender.StackTraces;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading;

namespace HibernatingRhinos.Profiler.Appender
{
  public static class ProfilerInfrastructure
  {
    internal static List<IStatisticsSource> StatisticsSource = new List<IStatisticsSource>();
    private static readonly UrlGetter urlGetter = new UrlGetter();
    internal static IDatabaseDialect DatabaseDialect = (IDatabaseDialect) new EmptyDatabaseDialect();
    private const string ProfileNotInitializedMessage = "The profiler was not initialize properly, did you forgot to call Profiler.Initialize()?";
    private static ProfilerMessageDispatcher messageDispatcher;
    private static IProfilerChannel profilerChannel;
    private static AppenderConfiguration installedConfiguration;
    private static bool successfullyPushedStats;
    private static bool stopSendingMessages;
    private static DateTime lastSentStatistics;
    private static Thread backgroundThreadForNotifyingProfiler;
    private static StackTraceGenerator stackTraceGenerator;

    public static bool IsInitialized
    {
      get
      {
        return ProfilerInfrastructure.installedConfiguration != null;
      }
    }

    public static StackTraceGenerator StackTraceGenerator
    {
      get
      {
        if (ProfilerInfrastructure.stackTraceGenerator == null)
          throw new InvalidOperationException("The profiler was not initialize properly, did you forgot to call Profiler.Initialize()?");
        return ProfilerInfrastructure.stackTraceGenerator;
      }
    }

    [MethodImpl(MethodImplOptions.Synchronized)]
    public static void Shutdown()
    {
      if (ProfilerInfrastructure.stopSendingMessages)
        return;
      ProfilerInfrastructure.stopSendingMessages = true;
      if (ProfilerInfrastructure.backgroundThreadForNotifyingProfiler != null)
      {
        try
        {
          ProfilerInfrastructure.backgroundThreadForNotifyingProfiler.Join();
        }
        catch (Exception ex)
        {
        }
      }
      ProfilerInfrastructure.messageDispatcher.FlushAndClear();
      ProfilerInfrastructure.profilerChannel.Dispose();
      ProfilerInfrastructure.profilerChannel = (IProfilerChannel) null;
      ProfilerInfrastructure.StatisticsSource.Clear();
      ProfilerInfrastructure.backgroundThreadForNotifyingProfiler = (Thread) null;
      ProfilerInfrastructure.messageDispatcher = (ProfilerMessageDispatcher) null;
      ProfilerInfrastructure.stackTraceGenerator = (StackTraceGenerator) null;
      ProfilerInfrastructure.installedConfiguration = (AppenderConfiguration) null;
    }

    [MethodImpl(MethodImplOptions.Synchronized)]
    public static void Initialize(AppenderConfiguration configuration)
    {
      if (ProfilerInfrastructure.installedConfiguration != null && ProfilerInfrastructure.installedConfiguration.IsMatch(configuration))
      {
        ProfilerInfrastructure.MergeStackTraces(configuration);
      }
      else
      {
        ProfilerInfrastructure.IgnoreConnectionStrings = configuration.IgnoreConnectionStrings;
        ApplicationInformationProvider.Initialize();
        ProfilerInfrastructure.InitializeChannelToProfiler(configuration);
        ProfilerInfrastructure.messageDispatcher = new ProfilerMessageDispatcher(ProfilerInfrastructure.profilerChannel);
        ProfilerInfrastructure.stackTraceGenerator = new StackTraceGenerator(configuration.StackTraceFilters)
        {
          DotNotFixDynamicProxyStackTrace = configuration.DotNotFixDynamicProxyStackTrace
        };
        ProfilerInfrastructure.installedConfiguration = configuration;
        ProfilerInfrastructure.stopSendingMessages = false;
        ProfilerInfrastructure.backgroundThreadForNotifyingProfiler = new Thread(new ThreadStart(ProfilerInfrastructure.SendMessagesToProfiler))
        {
          IsBackground = true,
          Name = "Profiler Message Dispatcher Thread"
        };
        ProfilerInfrastructure.backgroundThreadForNotifyingProfiler.Start();
        AppDomain.CurrentDomain.ProcessExit += new EventHandler(ProfilerInfrastructure.TurnOffLogging);
        AppDomain.CurrentDomain.DomainUnload += new EventHandler(ProfilerInfrastructure.TurnOffLogging);
      }
    }

    private static void TurnOffLogging(object sender, EventArgs e)
    {
      if (ProfilerInfrastructure.messageDispatcher == null)
        return;
      ProfilerInfrastructure.FlushAllMessages();
    }

    private static void InitializeChannelToProfiler(AppenderConfiguration configuration)
    {
      if (configuration.Production)
      {
        ProfilerInfrastructure.profilerChannel = (IProfilerChannel) new TcpListenerProfilerChannel()
        {
          Port = configuration.Port,
          ProductionPassword = configuration.ProductionPassword
        };
        ProfilerInfrastructure.profilerChannel.InitializeChannelToProfiler();
      }
      else if (configuration.FileToLogTo == null)
      {
        ProfilerInfrastructure.profilerChannel = (IProfilerChannel) new TcpProfilerChannel()
        {
          Host = configuration.HostToSendProfilingInformationTo,
          Port = configuration.Port
        };
      }
      else
      {
        ProfilerInfrastructure.profilerChannel = (IProfilerChannel) new FileProfilerChannel(configuration.FileToLogTo);
        ProfilerInfrastructure.profilerChannel.InitializeChannelToProfiler();
      }
    }

    private static void MergeStackTraces(AppenderConfiguration configuration)
    {
      List<IStackTraceFilter> stackTraceFilterList = new List<IStackTraceFilter>((IEnumerable<IStackTraceFilter>) ProfilerInfrastructure.installedConfiguration.StackTraceFilters);
      foreach (IStackTraceFilter stackTraceFilter in configuration.StackTraceFilters)
      {
        if (!ProfilerInfrastructure.ContainsOfType(stackTraceFilter, (IEnumerable<IStackTraceFilter>) stackTraceFilterList))
          stackTraceFilterList.Add(stackTraceFilter);
      }
      ProfilerInfrastructure.installedConfiguration.StackTraceFilters = stackTraceFilterList.ToArray();
      ProfilerInfrastructure.stackTraceGenerator = new StackTraceGenerator(ProfilerInfrastructure.installedConfiguration.StackTraceFilters)
      {
        DotNotFixDynamicProxyStackTrace = configuration.DotNotFixDynamicProxyStackTrace
      };
    }

    private static bool ContainsOfType(IStackTraceFilter filter, IEnumerable<IStackTraceFilter> filters)
    {
      foreach (object filter1 in filters)
      {
        if (filter1.GetType() == filter.GetType())
          return true;
      }
      return false;
    }

    public static void FlushAllMessages()
    {
      ProfilerInfrastructure.MessageDispatcher.WaitForAllMessagesToBeFlushed();
    }

    public static ProfilerMessageDispatcher MessageDispatcher
    {
      get
      {
        if (ProfilerInfrastructure.messageDispatcher == null)
          throw new InvalidOperationException("The profiler was not initialize properly, did you forgot to call Profiler.Initialize()?");
        return ProfilerInfrastructure.messageDispatcher;
      }
    }

    internal static IProfilerChannel ProfilerChannel
    {
      get
      {
        if (ProfilerInfrastructure.profilerChannel == null)
          throw new InvalidOperationException("The profiler was not initialize properly, did you forgot to call Profiler.Initialize()?");
        return ProfilerInfrastructure.profilerChannel;
      }
    }

    private static void SendMessagesToProfiler()
    {
      while (!ProfilerInfrastructure.stopSendingMessages)
      {
        if ((DateTime.Now - ProfilerInfrastructure.lastSentStatistics).Milliseconds > 250)
        {
          ProfilerInfrastructure.SendStatisticInformation();
          ProfilerInfrastructure.lastSentStatistics = DateTime.Now;
        }
        ProfilerMessageDispatcher messageDispatcher = ProfilerInfrastructure.messageDispatcher;
        if (messageDispatcher != null && !messageDispatcher.FlushAllMessages())
          Thread.Sleep(250);
      }
    }

    private static void SendStatisticInformation()
    {
      try
      {
        IProfilerChannel profilerChannel = ProfilerInfrastructure.profilerChannel;
        if (profilerChannel == null || !profilerChannel.ShouldSendDataToProfiler)
          return;
        List<IStatisticsSource> statisticsSourceList;
        lock (ProfilerInfrastructure.StatisticsSource)
          statisticsSourceList = new List<IStatisticsSource>((IEnumerable<IStatisticsSource>) ProfilerInfrastructure.StatisticsSource);
        if (statisticsSourceList.Count == 0)
          return;
        List<SessionFactoryStats> sessionFactoryStatsList = new List<SessionFactoryStats>();
        foreach (IStatisticsSource statisticsSource in statisticsSourceList)
          sessionFactoryStatsList.AddRange((IEnumerable<SessionFactoryStats>) statisticsSource.GetStatistics());
        if (sessionFactoryStatsList.Count == 0)
          return;
        profilerChannel.Send(sessionFactoryStatsList.ToArray());
        ProfilerInfrastructure.SuccessfullyPushedStats = true;
      }
      catch
      {
      }
    }

    public static bool SuccessfullyPushedStats
    {
      get
      {
        return ProfilerInfrastructure.successfullyPushedStats;
      }
      set
      {
        ProfilerInfrastructure.successfullyPushedStats = value;
        if (!ProfilerInfrastructure.successfullyPushedStats)
          return;
        ProfilerAction statisticsPublished = ProfilerInfrastructure.StatisticsPublished;
        if (statisticsPublished == null)
          return;
        statisticsPublished();
      }
    }

    public static UrlGetter UrlGetter
    {
      get
      {
        return ProfilerInfrastructure.urlGetter;
      }
    }

    public static event ProfilerAction StatisticsPublished;

    public static void ResetStatisticPushed()
    {
      ProfilerInfrastructure.SuccessfullyPushedStats = false;
      ProfilerInfrastructure.lastSentStatistics = DateTime.MinValue;
    }

    public static void ClearNullMessageDispatcher()
    {
      ProfilerInfrastructure.messageDispatcher = (ProfilerMessageDispatcher) null;
      ProfilerInfrastructure.stackTraceGenerator = (StackTraceGenerator) null;
    }

    public static void InitializeNullMessageDispatcher(IStackTraceFilter[] filters)
    {
      ProfilerInfrastructure.messageDispatcher = new ProfilerMessageDispatcher((IProfilerChannel) new NullProfilerChannel());
      ProfilerInfrastructure.stackTraceGenerator = new StackTraceGenerator(filters)
      {
        DotNotFixDynamicProxyStackTrace = true
      };
    }

    public static bool ThrowOnConnectionErrors { get; set; }

    public static bool IgnoreConnectionStrings { get; set; }
  }
}
