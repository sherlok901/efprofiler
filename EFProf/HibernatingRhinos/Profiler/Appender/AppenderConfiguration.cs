﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Appender.AppenderConfiguration
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using HibernatingRhinos.Profiler.Appender.StackTraces;

namespace HibernatingRhinos.Profiler.Appender
{
  public abstract class AppenderConfiguration
  {
    protected AppenderConfiguration(IStackTraceFilter[] stackTraceFilters)
    {
      this.StackTraceFilters = stackTraceFilters;
      this.Port = this.DefaultPort;
      this.HostToSendProfilingInformationTo = "127.0.0.1";
    }

    public abstract int DefaultPort { get; }

    internal abstract void StartProfiling();

    internal abstract void StopProfiling();

    public IStackTraceFilter[] StackTraceFilters { get; set; }

    public string HostToSendProfilingInformationTo { get; set; }

    public bool DotNotFixDynamicProxyStackTrace { get; set; }

    public int Port { get; set; }

    public string FileToLogTo { get; set; }

    public bool Production { get; set; }

    public string ProductionPassword { get; set; }

    public bool IgnoreConnectionStrings { get; set; }

    public bool IsMatch(AppenderConfiguration other)
    {
      if (this.FileToLogTo != null && this.FileToLogTo == other.FileToLogTo)
        return true;
      if (this.HostToSendProfilingInformationTo == other.HostToSendProfilingInformationTo)
        return this.Port == other.Port;
      return false;
    }
  }
}
