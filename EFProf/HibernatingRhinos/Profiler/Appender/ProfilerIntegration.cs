﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Appender.ProfilerIntegration
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using HibernatingRhinos.Profiler.Appender.StackTraces;
using HibernatingRhinos.Profiler.Appender.Util;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Runtime.Remoting.Messaging;

namespace HibernatingRhinos.Profiler.Appender
{
  public static class ProfilerIntegration
  {
    private static readonly string[] AllowedStarColors = new string[6]
    {
      "Yellow",
      "Orange",
      "Red",
      "Green",
      "Blue",
      "Gray"
    };
    public static Func<string> GetCurrentScopeName = (Func<string>) (() =>
    {
      if (ProfilerIntegration.CurrentThreadScopeName != null)
        return ProfilerIntegration.CurrentThreadScopeName;
      if (ProfilerHttpContext.CurrentItems == null)
        return (string) null;
      string str = (string) ProfilerHttpContext.CurrentItems[(object) "HibernatingRhinos.Profiler.ProfilerIntegration.CurrentScopeName"];
      if (str == null)
      {
        str = "ASP.Net #" + (object) ProfilerIntegration.scopeCounter + " / Process " + (object) Process.GetCurrentProcess().Id;
        ProfilerHttpContext.CurrentItems[(object) "HibernatingRhinos.Profiler.ProfilerIntegration.CurrentScopeName"] = (object) str;
      }
      return str;
    });
    private static int scopeCounter = 0;
    private static int stackTraceErrorCount;

    private static string InternalCurrentThreadId
    {
      get
      {
        return (string) CallContext.GetData("internalCurrentThreadId");
      }
      set
      {
        CallContext.SetData("internalCurrentThreadId", (object) value);
      }
    }

    private static string CurrentThreadScopeName
    {
      get
      {
        return (string) CallContext.GetData("currentThreadScopeName");
      }
      set
      {
        CallContext.SetData("currentThreadScopeName", (object) value);
      }
    }

    public static string CurrentSessionContext
    {
      get
      {
        return (string) CallContext.GetData("currentSessionContext");
      }
      set
      {
        CallContext.SetData("currentSessionContext", (object) value);
      }
    }

    internal static bool AvoidPublishing { get; set; }

    internal static string StarStatementsColor { get; set; }

    public static IDisposable IgnoreAll()
    {
      bool oldValue = ProfilerIntegration.AvoidPublishing;
      ProfilerIntegration.AvoidPublishing = true;
      return (IDisposable) new DisposableAction((Action) (() => ProfilerIntegration.AvoidPublishing = oldValue));
    }

    public static void ResumeProfiling()
    {
      ProfilerIntegration.AvoidPublishing = false;
    }

    public static IDisposable StarStatements(string color = "Yellow")
    {
      if (!((IEnumerable<string>) ProfilerIntegration.AllowedStarColors).Contains<string>(color))
        throw new InvalidOperationException(string.Format("'{0}' is not allowed color. You can use the following colors: {1}", (object) color, (object) string.Join(", ", ProfilerIntegration.AllowedStarColors)));
      string oldValue = ProfilerIntegration.StarStatementsColor;
      ProfilerIntegration.StarStatementsColor = color;
      return (IDisposable) new DisposableAction((Action) (() => ProfilerIntegration.StarStatementsColor = oldValue));
    }

    public static void StarStatementsClear()
    {
      ProfilerIntegration.StarStatementsColor = (string) null;
    }

    public static string CurrentThreadId
    {
      get
      {
        if (ProfilerHttpContext.CurrentItems == null)
          return ProfilerIntegration.InternalCurrentThreadId ?? (ProfilerIntegration.InternalCurrentThreadId = Guid.NewGuid().ToString());
        string str = (string) ProfilerHttpContext.CurrentItems[(object) "HibernatingRhinos.Profiler.Appender.ProfilerIntegration.CurrentRequestId"];
        if (str == null)
        {
          str = "ASP.Net/" + (object) Guid.NewGuid();
          ProfilerHttpContext.CurrentItems[(object) "HibernatingRhinos.Profiler.Appender.ProfilerIntegration.CurrentRequestId"] = (object) str;
        }
        return str;
      }
    }

    public static void PublishProfilerEvent(string sessionId, string loggerName, string message)
    {
      ProfilerIntegration.PublishProfilerEvent(sessionId, loggerName, message, DateTime.Now, Level.Debug);
    }

    public static void PublishProfilerEvent(string sessionId, string loggerName, string message, DateTime timestamp, Level level)
    {
      if (ProfilerIntegration.AvoidPublishing)
        return;
      ProfilerInfrastructure.MessageDispatcher.Append(new LoggingEventMessage()
      {
        Date = timestamp,
        Logger = loggerName,
        SessionId = sessionId,
        Message = message,
        StackTrace = ProfilerIntegration.GetStackTrace(loggerName, message, level),
        ThreadId = ProfilerIntegration.CurrentThreadId,
        ScopeName = ProfilerIntegration.CurrentScopeName,
        Level = (int) level,
        Url = ProfilerInfrastructure.UrlGetter.GetUrl(),
        DbDialect = ProfilerInfrastructure.DatabaseDialect.GetDialect(),
        Culture = CultureInfo.CurrentCulture.Name,
        StarColor = ProfilerIntegration.StarStatementsColor
      });
    }

    internal static string CurrentScopeName
    {
      get
      {
        Func<string> currentScopeName = ProfilerIntegration.GetCurrentScopeName;
        if (currentScopeName == null)
          return ProfilerIntegration.CurrentThreadScopeName;
        return currentScopeName();
      }
    }

    private static StackTraceInfo GetStackTrace(string loggerName, string message, Level level)
    {
      try
      {
        return ProfilerInfrastructure.StackTraceGenerator.GetStackTrace(loggerName, message, level);
      }
      catch (Exception ex)
      {
        if (ProfilerIntegration.stackTraceErrorCount++ % 100 == 0)
          Trace.WriteLine("Error getting stack trace: " + (object) ex);
        return (StackTraceInfo) null;
      }
    }

    internal static void PublishProfilerWarning(string sessionId, string loggerName, string message)
    {
      ProfilerIntegration.PublishProfilerEvent(sessionId, loggerName, message, DateTime.Now, Level.Warn);
    }

    internal static void RegisterStatisticsSource(IStatisticsSource statsSource)
    {
      lock (ProfilerInfrastructure.StatisticsSource)
        ProfilerInfrastructure.StatisticsSource.Add(statsSource);
    }

    internal static void UnregisterStatisticsSource(IStatisticsSource statsSource)
    {
      lock (ProfilerInfrastructure.StatisticsSource)
        ProfilerInfrastructure.StatisticsSource.Remove(statsSource);
    }

    public static void SetDatabaseDialect(IDatabaseDialect databaseDialect)
    {
      ProfilerInfrastructure.DatabaseDialect = databaseDialect;
    }

    public static IDisposable StartScope(string name = null)
    {
      string old = ProfilerIntegration.CurrentThreadScopeName;
      ++ProfilerIntegration.scopeCounter;
      if (string.IsNullOrEmpty(name))
        name = "Scope #" + (object) ProfilerIntegration.scopeCounter + " / Process #" + (object) Process.GetCurrentProcess().Id;
      ProfilerIntegration.CurrentThreadScopeName = name;
      return (IDisposable) new DisposableAction((Action) (() => ProfilerIntegration.CurrentThreadScopeName = old));
    }
  }
}
