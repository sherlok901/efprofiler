﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Appender.ApplicationInformationProvider
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using System;
using System.Reflection;
using System.Threading;

namespace HibernatingRhinos.Profiler.Appender
{
  public class ApplicationInformationProvider
  {
    private static Guid id = Guid.NewGuid();

    public static Guid Id
    {
      get
      {
        return ApplicationInformationProvider.id;
      }
    }

    public static string Name { get; private set; }

    public static void Initialize()
    {
      ApplicationInformationProvider.Name = ApplicationInformationProvider.DetermineName();
    }

    private static string DetermineName()
    {
      string currentApplicationPath = ProfilerHttpContext.CurrentApplicationPath;
      if (currentApplicationPath != null)
        return "(ASP.Net) " + currentApplicationPath;
      Assembly entryAssembly = Assembly.GetEntryAssembly();
      if (entryAssembly != null)
        return entryAssembly.GetName().Name;
      return Thread.GetDomain().FriendlyName;
    }
  }
}
