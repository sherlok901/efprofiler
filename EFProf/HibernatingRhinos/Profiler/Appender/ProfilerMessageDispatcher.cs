﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Appender.ProfilerMessageDispatcher
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using HibernatingRhinos.Profiler.Appender.Channels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace HibernatingRhinos.Profiler.Appender
{
  public class ProfilerMessageDispatcher
  {
    private readonly object eventMessagesLock = new object();
    private readonly IProfilerChannel profilerChannel;

    public ProfilerMessageDispatcher(IProfilerChannel profilerChannel)
    {
      this.EventMessages = new LinkedList<LoggingEventMessage>();
      this.profilerChannel = profilerChannel;
    }

    public LinkedList<LoggingEventMessage> EventMessages { get; private set; }

    public void WaitForAllMessagesToBeFlushed()
    {
      while (true)
      {
        lock (this.eventMessagesLock)
        {
          if (this.EventMessages.Count == 0)
          {
            if (this.profilerChannel.FinishedToFlush)
              break;
          }
        }
        Thread.Sleep(100);
      }
    }

    public void FlushAndClear()
    {
      lock (this.eventMessagesLock)
      {
        this.FlushMessagesToProfiler((ICollection<LoggingEventMessage>) this.EventMessages);
        this.EventMessages.Clear();
      }
    }

    public void Clear()
    {
      lock (this.eventMessagesLock)
        this.EventMessages = new LinkedList<LoggingEventMessage>();
    }

    public bool FlushAllMessages()
    {
      if (!this.profilerChannel.ShouldSendDataToProfiler)
      {
        lock (this.eventMessagesLock)
          this.EventMessages = new LinkedList<LoggingEventMessage>();
        return false;
      }
      LinkedList<LoggingEventMessage> linkedList = (LinkedList<LoggingEventMessage>) null;
      bool flag;
      lock (this.eventMessagesLock)
      {
        flag = this.EventMessages.Count != 0;
        if (flag)
        {
          linkedList = this.EventMessages;
          this.EventMessages = new LinkedList<LoggingEventMessage>();
        }
      }
      if (!flag)
        return false;
      this.FlushMessagesToProfiler((ICollection<LoggingEventMessage>) linkedList);
      return true;
    }

    private void FlushMessagesToProfiler(ICollection<LoggingEventMessage> copy)
    {
      try
      {
        this.profilerChannel.Send(copy);
      }
      catch (Exception ex)
      {
      }
    }

    public void Append(LoggingEventMessage message)
    {
      lock (this.eventMessagesLock)
        this.EventMessages.AddLast(message);
      if (!Debugger.IsAttached)
        return;
      this.FlushAllMessages();
    }
  }
}
