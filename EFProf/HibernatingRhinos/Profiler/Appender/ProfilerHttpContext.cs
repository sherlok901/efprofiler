﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Appender.ProfilerHttpContext
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using System;
using System.Collections;
using System.Reflection;

namespace HibernatingRhinos.Profiler.Appender
{
  public static class ProfilerHttpContext
  {
    private static bool isRequestStartAvailable = true;
    private static readonly PropertyInfo currentProp;
    private static readonly PropertyInfo itemsProp;
    private static readonly PropertyInfo requestProp;
    private static readonly PropertyInfo urlProp;
    private static readonly PropertyInfo applicationPathProp;
    private static DateTime lastTimeRequestWasNotAvailable;
    private static int targetInvocationExceptionCount;

    static ProfilerHttpContext()
    {
      try
      {
        Assembly assembly = Assembly.LoadWithPartialName("System.Web");
        Type type1 = assembly.GetType("System.Web.HttpContext");
        Type type2 = assembly.GetType("System.Web.HttpRequest");
        ProfilerHttpContext.currentProp = type1.GetProperty("Current", BindingFlags.Static | BindingFlags.Public);
        ProfilerHttpContext.itemsProp = type1.GetProperty("Items", BindingFlags.Instance | BindingFlags.Public);
        ProfilerHttpContext.requestProp = type1.GetProperty("Request", BindingFlags.Instance | BindingFlags.Public);
        ProfilerHttpContext.urlProp = type2.GetProperty("Url", BindingFlags.Instance | BindingFlags.Public);
        ProfilerHttpContext.applicationPathProp = type2.GetProperty("ApplicationPath", BindingFlags.Instance | BindingFlags.Public);
      }
      catch
      {
      }
    }

    public static IDictionary CurrentItems
    {
      get
      {
        if (ProfilerHttpContext.currentProp == null)
          return (IDictionary) null;
        object obj = ProfilerHttpContext.currentProp.GetValue((object) null, (object[]) null);
        if (obj == null)
          return (IDictionary) null;
        return (IDictionary) ProfilerHttpContext.itemsProp.GetValue(obj, (object[]) null);
      }
    }

    public static string GetCurrentUrl()
    {
      if (!ProfilerHttpContext.isRequestStartAvailable)
      {
        int num = 250 * Math.Max(ProfilerHttpContext.targetInvocationExceptionCount, 12);
        if (DateTime.Now - ProfilerHttpContext.lastTimeRequestWasNotAvailable < TimeSpan.FromMilliseconds((double) num))
          return string.Empty;
      }
      ProfilerHttpContext.isRequestStartAvailable = true;
      try
      {
        if (ProfilerHttpContext.currentProp == null)
          return (string) null;
        object obj1 = ProfilerHttpContext.currentProp.GetValue((object) null, (object[]) null);
        if (obj1 == null)
          return (string) null;
        object obj2 = ProfilerHttpContext.requestProp.GetValue(obj1, (object[]) null);
        if (obj2 == null)
          return (string) null;
        object obj3 = ProfilerHttpContext.urlProp.GetValue(obj2, (object[]) null);
        if (obj3 == null)
          return (string) null;
        return obj3.ToString();
      }
      catch (TargetInvocationException ex)
      {
        ++ProfilerHttpContext.targetInvocationExceptionCount;
        ProfilerHttpContext.isRequestStartAvailable = false;
        ProfilerHttpContext.lastTimeRequestWasNotAvailable = DateTime.Now;
        return string.Empty;
      }
    }

    public static string CurrentApplicationPath
    {
      get
      {
        try
        {
          if (ProfilerHttpContext.currentProp == null)
            return (string) null;
          object current = ProfilerHttpContext.currentProp.GetValue((object) null, (object[]) null);
          if (current == null)
            return (string) null;
          return ProfilerHttpContext.CallActionWithApplicationPath(current);
        }
        catch
        {
          return (string) null;
        }
      }
    }

    private static string CallActionWithApplicationPath(object current)
    {
      object obj1 = ProfilerHttpContext.requestProp.GetValue(current, (object[]) null);
      if (obj1 == null)
        return (string) null;
      object obj2 = ProfilerHttpContext.applicationPathProp.GetValue(obj1, (object[]) null);
      if (obj2 == null)
        return (string) null;
      return obj2.ToString();
    }
  }
}
