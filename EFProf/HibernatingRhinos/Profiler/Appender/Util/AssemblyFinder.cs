﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Appender.Util.AssemblyFinder
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using System;
using System.IO;
using System.Reflection;

namespace HibernatingRhinos.Profiler.Appender.Util
{
  public static class AssemblyFinder
  {
    public static Assembly FindAssemblyPath(string assemblyName)
    {
      assemblyName += ".dll";
      string str;
      if (GenerateAssembly.IsNet45OrNewer())
      {
        str = AssemblyFinder.GetAssemblyPath(assemblyName, "4.5");
        if (!File.Exists(str))
          str = AssemblyFinder.GetAssemblyPath(assemblyName, "4.5.1");
        if (!File.Exists(str))
          str = AssemblyFinder.GetAssemblyPath(assemblyName, "4.0");
      }
      else
      {
        str = AssemblyFinder.GetAssemblyPath(assemblyName, "4.0");
        if (!File.Exists(str))
          str = AssemblyFinder.GetAssemblyPath(assemblyName, "3.5\\Profile\\Client");
      }
      if (!File.Exists(str))
        str = !EnvironmentExt.Is64BitProcess() ? Path.Combine(Environment.GetEnvironmentVariable("windir"), "Microsoft.NET\\Framework\\v4.0.30319\\" + assemblyName) : Path.Combine(Environment.GetEnvironmentVariable("windir"), "Microsoft.NET\\Framework64\\v4.0.30319\\" + assemblyName);
      if (string.IsNullOrEmpty(str))
        throw new InvalidOperationException("Cannot find the path to the following dll: " + assemblyName);
      Assembly assembly;
      try
      {
        assembly = Assembly.LoadFrom(str);
      }
      catch (FileNotFoundException ex1)
      {
        try
        {
          assembly = Assembly.Load(assemblyName);
        }
        catch (Exception ex2)
        {
          throw ex1;
        }
      }
      if (assembly == null)
        assembly = Assembly.Load(assemblyName);
      return assembly;
    }

    private static string GetAssemblyPath(string assemblyName, string versionPath)
    {
      return Path.Combine(AssemblyFinder.GetProgramFilesX86(), "Reference Assemblies\\Microsoft\\Framework\\.NETFramework\\v") + versionPath + "\\" + assemblyName;
    }

    public static string GetProgramFilesX86()
    {
      if (8 == IntPtr.Size || !string.IsNullOrEmpty(Environment.GetEnvironmentVariable("PROCESSOR_ARCHITEW6432")))
        return Environment.GetEnvironmentVariable("ProgramFiles(x86)");
      return Environment.GetEnvironmentVariable("ProgramFiles");
    }
  }
}
