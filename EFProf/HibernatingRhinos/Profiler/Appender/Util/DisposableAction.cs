﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Appender.Util.DisposableAction
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using System;

namespace HibernatingRhinos.Profiler.Appender.Util
{
  public class DisposableAction : IDisposable
  {
    private readonly Action action;

    public DisposableAction(Action action)
    {
      if (action == null)
        throw new ArgumentNullException(nameof (action));
      this.action = action;
    }

    public void Dispose()
    {
      this.action();
    }
  }
}
