﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Appender.Util.GenerateAssembly
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Microsoft.CSharp;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace HibernatingRhinos.Profiler.Appender.Util
{
  public class GenerateAssembly
  {
    private static readonly Dictionary<GenerateAssembly.CacheKey, Assembly> cache = new Dictionary<GenerateAssembly.CacheKey, Assembly>();

    public static bool IsNet45OrNewer()
    {
      return Type.GetType("System.Reflection.ReflectionContext", false) != null;
    }

    public static Assembly CompileAssembly(List<string> sources, HashSet<string> assemblies, HashSet<string> defineSymbols, string assemblyFileName, string outputFolder = null)
    {
      GenerateAssembly.CacheKey key = new GenerateAssembly.CacheKey(assemblyFileName, sources, assemblies);
      Assembly assembly1;
      if (GenerateAssembly.cache.TryGetValue(key, out assembly1))
        return assembly1;
      lock (GenerateAssembly.cache)
      {
        if (GenerateAssembly.cache.TryGetValue(key, out assembly1))
          return assembly1;
        Assembly assembly2;
        try
        {
          if (typeof (object).Assembly.ImageRuntimeVersion == "v2.0.50727")
            assembly2 = GenerateAssembly.CompileInternally(assemblyFileName, sources, assemblies, defineSymbols, outputFolder, "v3.5", 1);
          else if (GenerateAssembly.IsNet45OrNewer())
          {
            try
            {
              assembly2 = GenerateAssembly.CompileInternally(assemblyFileName, sources, assemblies, defineSymbols, outputFolder, "v4.5", 1);
            }
            catch (UnauthorizedAccessException ex)
            {
              throw new InvalidOperationException("You need write access to:\r\n" + ex.Message, (Exception) ex);
            }
            catch (Exception ex1)
            {
              try
              {
                assembly2 = GenerateAssembly.CompileInternally(assemblyFileName, sources, assemblies, defineSymbols, outputFolder, "v4.0", 1);
              }
              catch (Exception ex2)
              {
                throw new InvalidOperationException(string.Format("Failed to compile both on .NET 4.5 and on .NET 4.0. 4.5 Exception: {0}. 4.0 Exception: {1}", (object) ex1, (object) ex2));
              }
            }
          }
          else
            assembly2 = GenerateAssembly.CompileInternally(assemblyFileName, sources, assemblies, defineSymbols, outputFolder, "v4.0", 1);
        }
        catch (Exception ex)
        {
          throw new InvalidOperationException(ex.ToString() + Environment.NewLine + "ImageRuntimeVersion: " + typeof (object).Assembly.ImageRuntimeVersion, ex);
        }
        GenerateAssembly.AddAssemblyResolve(assembly2);
        GenerateAssembly.cache[key] = assembly2;
        return assembly2;
      }
    }

    public static HashSet<T> CloneHashSet<T>(IEnumerable<T> source)
    {
      if (source == null)
        throw new ArgumentNullException(nameof (source));
      return new HashSet<T>(source);
    }

    private static Assembly CompileInternally(string fileName, List<string> sources, HashSet<string> assemblies, HashSet<string> defineSymbols, string outputFolder, string compilerVersion, int tryCount = 1)
    {
      defineSymbols = GenerateAssembly.CloneHashSet<string>((IEnumerable<string>) defineSymbols);
      Dictionary<string, string> dictionary = new Dictionary<string, string>();
      string path = "C:\\Windows\\Microsoft.NET\\Framework\\v4.0.30319\\";
      switch (compilerVersion)
      {
        case "v3.5":
          path = "C:\\Windows\\Microsoft.NET\\Framework\\v3.5\\";
          break;
      }
      if (Directory.Exists(path) && tryCount == 2)
        dictionary.Add("CompilerDirectoryPath", path);
      else
        dictionary.Add("CompilerVersion", compilerVersion == "v4.5" ? "v4.0" : compilerVersion);
      CSharpCodeProvider csharpCodeProvider = new CSharpCodeProvider((IDictionary<string, string>) dictionary);
      assemblies.Add(typeof (NameValueCollection).Assembly.Location);
      string tempPath = Path.GetTempPath();
      string str1;
      try
      {
        str1 = outputFolder == null ? Path.GetTempFileName() + ".dll" : Path.Combine(outputFolder, fileName);
      }
      catch (UnauthorizedAccessException ex)
      {
        throw new UnauthorizedAccessException(tempPath + ".\r\n" + ex.Message);
      }
      CompilerParameters options = new CompilerParameters()
      {
        OutputAssembly = str1,
        GenerateExecutable = false,
        GenerateInMemory = false,
        IncludeDebugInformation = Debugger.IsAttached,
        TempFiles = new TempFileCollection(tempPath, false)
      };
      if (compilerVersion == "v4.0")
        defineSymbols.Add("NET40");
      else if (compilerVersion == "v4.5")
      {
        defineSymbols.Add("NET40");
        defineSymbols.Add("NET45");
      }
      else if (compilerVersion == "v3.5")
        defineSymbols.Add("NET35");
      string str2 = string.Join(";", defineSymbols.ToArray<string>());
      options.CompilerOptions = "/define:" + str2;
      foreach (string assembly in assemblies)
        options.ReferencedAssemblies.Add(assembly);
      CompilerResults compilerResults = csharpCodeProvider.CompileAssemblyFromSource(options, sources.Select<string, string>(new Func<string, string>(GenerateAssembly.GetResource)).ToArray<string>());
      if (!compilerResults.Errors.HasErrors)
        return compilerResults.CompiledAssembly;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.AppendLine(string.Format("Failed to compile the following symbols: '{0}'", (object) str2));
      foreach (CompilerError compilerError in compilerResults.Errors.Cast<CompilerError>().Take<CompilerError>(24))
        stringBuilder.AppendLine(compilerError.ErrorText);
      string message = stringBuilder.ToString();
      if (message.Contains("Compiler executable file csc.exe cannot be found") && tryCount == 1)
        return GenerateAssembly.CompileInternally(fileName, sources, assemblies, defineSymbols, outputFolder, compilerVersion, ++tryCount);
      throw new InvalidOperationException(message);
    }

    public static string GetResource(string sourcesResource)
    {
      using (Stream manifestResourceStream = typeof (GenerateAssembly).Assembly.GetManifestResourceStream(sourcesResource))
      {
        if (manifestResourceStream == null)
          throw new InvalidOperationException(string.Format("Source resource is missing: {0}", (object) sourcesResource));
        return new StreamReader(manifestResourceStream).ReadToEnd();
      }
    }

    public static void AddAssemblyResolve(Assembly assembly)
    {
      AppDomain.CurrentDomain.AssemblyResolve += (ResolveEventHandler) ((o, args) =>
      {
        if (args.Name == assembly.FullName)
          return assembly;
        return (Assembly) null;
      });
    }

    public class CacheKey
    {
      private readonly string fileName;
      private readonly List<string> sources;
      private readonly HashSet<string> assemblies;

      public CacheKey(string fileName, List<string> sources, HashSet<string> assemblies)
      {
        this.fileName = fileName;
        this.sources = sources;
        this.assemblies = assemblies;
      }

      private bool Equals(GenerateAssembly.CacheKey other)
      {
        if (other == null || !string.Equals(this.fileName, other.fileName) || !this.sources.SequenceEqual<string>((IEnumerable<string>) other.sources))
          return false;
        return this.assemblies.SequenceEqual<string>((IEnumerable<string>) other.assemblies);
      }

      public override bool Equals(object obj)
      {
        if (object.ReferenceEquals((object) null, obj))
          return false;
        if (object.ReferenceEquals((object) this, obj))
          return true;
        return this.Equals(obj as GenerateAssembly.CacheKey);
      }

      public override int GetHashCode()
      {
        return this.assemblies.Aggregate<string, int>(this.sources.Aggregate<string, int>(this.fileName.GetHashCode(), (Func<int, string, int>) ((a, s) => a * 397 ^ s.GetHashCode())), (Func<int, string, int>) ((a, s) => a * 397 ^ s.GetHashCode()));
      }
    }
  }
}
