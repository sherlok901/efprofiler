﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Appender.Util.AssemblyExtensions
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace HibernatingRhinos.Profiler.Appender.Util
{
  public static class AssemblyExtensions
  {
    public static IEnumerable<Type> GetLoadableTypes(this Assembly assembly)
    {
      if (assembly == null)
        throw new ArgumentNullException(nameof (assembly));
      try
      {
        return (IEnumerable<Type>) assembly.GetTypes();
      }
      catch (ReflectionTypeLoadException ex)
      {
        return ((IEnumerable<Type>) ex.Types).Where<Type>((Func<Type, bool>) (t => t != null));
      }
    }
  }
}
