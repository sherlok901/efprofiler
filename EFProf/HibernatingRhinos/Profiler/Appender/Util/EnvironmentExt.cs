﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Appender.Util.EnvironmentExt
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace HibernatingRhinos.Profiler.Appender.Util
{
  public static class EnvironmentExt
  {
    private static readonly bool is64BitProcess = IntPtr.Size == 8;
    private static bool is64BitOperatingSystem = EnvironmentExt.is64BitProcess || EnvironmentExt.InternalCheckIsWow64();

    [DllImport("kernel32.dll", SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    private static extern bool IsWow64Process([In] IntPtr hProcess, out bool wow64Process);

    public static bool Is64BitProcess()
    {
      return EnvironmentExt.is64BitOperatingSystem;
    }

    private static bool InternalCheckIsWow64()
    {
      if ((Environment.OSVersion.Version.Major != 5 || Environment.OSVersion.Version.Minor < 1) && Environment.OSVersion.Version.Major < 6)
        return false;
      using (Process currentProcess = Process.GetCurrentProcess())
      {
        bool wow64Process;
        if (!EnvironmentExt.IsWow64Process(currentProcess.Handle, out wow64Process))
          return false;
        return wow64Process;
      }
    }
  }
}
