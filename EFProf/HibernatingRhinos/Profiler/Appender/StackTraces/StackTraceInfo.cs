﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Appender.StackTraces.StackTraceInfo
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using System;

namespace HibernatingRhinos.Profiler.Appender.StackTraces
{
  [Serializable]
  public class StackTraceInfo : IEquatable<StackTraceInfo>
  {
    public StackTraceFrame[] Frames { get; set; }

    public bool Equals(StackTraceInfo other)
    {
      if (object.ReferenceEquals((object) null, (object) other))
        return false;
      if (object.ReferenceEquals((object) this, (object) other))
        return true;
      if (this.Frames.Length != other.Frames.Length)
        return false;
      for (int index = 0; index < this.Frames.Length; ++index)
      {
        if (!object.Equals((object) this.Frames[index], (object) other.Frames[index]))
          return false;
      }
      return true;
    }

    public override bool Equals(object obj)
    {
      if (object.ReferenceEquals((object) null, obj))
        return false;
      if (object.ReferenceEquals((object) this, obj))
        return true;
      if (obj.GetType() != typeof (StackTraceInfo))
        return false;
      return this.Equals((StackTraceInfo) obj);
    }

    public override int GetHashCode()
    {
      if (this.Frames == null)
        return 0;
      return this.Frames.GetHashCode();
    }
  }
}
