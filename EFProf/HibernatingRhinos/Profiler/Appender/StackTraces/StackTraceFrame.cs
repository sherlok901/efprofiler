﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Appender.StackTraces.StackTraceFrame
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using System;
using System.Diagnostics;
using System.IO;

namespace HibernatingRhinos.Profiler.Appender.StackTraces
{
  [DebuggerDisplay("{Namespace} {Type} {Method} {Line}:{Column}")]
  [Serializable]
  public class StackTraceFrame : IEquatable<StackTraceFrame>
  {
    private string fullFilename;
    private bool? fileExists;

    public string Type { get; set; }

    public int Line { get; set; }

    public int Column { get; set; }

    public string Filename { get; set; }

    public string Method { get; set; }

    public string Namespace { get; set; }

    public string FullTypeName
    {
      get
      {
        return this.Namespace + "." + this.Type;
      }
    }

    public bool FileExists
    {
      get
      {
        if (this.fileExists.HasValue)
          return this.fileExists.Value;
        if (this.fullFilename == null)
          return false;
        return File.Exists(this.FullFilename);
      }
      set
      {
        this.fileExists = new bool?(value);
      }
    }

    public string FullFilename
    {
      get
      {
        return this.fullFilename;
      }
      set
      {
        this.fullFilename = value;
        try
        {
          this.Filename = Path.GetFileName(value);
        }
        catch (ArgumentException ex)
        {
          this.Filename = value;
        }
      }
    }

    public bool Equals(StackTraceFrame other)
    {
      if (object.ReferenceEquals((object) null, (object) other))
        return false;
      if (object.ReferenceEquals((object) this, (object) other))
        return true;
      if (object.Equals((object) other.fullFilename, (object) this.fullFilename) && object.Equals((object) other.Type, (object) this.Type) && (other.Line == this.Line && other.Column == this.Column) && (object.Equals((object) other.Filename, (object) this.Filename) && object.Equals((object) other.Method, (object) this.Method)))
        return object.Equals((object) other.Namespace, (object) this.Namespace);
      return false;
    }

    public override bool Equals(object obj)
    {
      if (object.ReferenceEquals((object) null, obj))
        return false;
      if (object.ReferenceEquals((object) this, obj))
        return true;
      StackTraceFrame other = obj as StackTraceFrame;
      if (other == null)
        return false;
      return this.Equals(other);
    }

    public override int GetHashCode()
    {
      return ((((((this.fullFilename != null ? this.fullFilename.GetHashCode() : 0) * 397 ^ (this.Type != null ? this.Type.GetHashCode() : 0)) * 397 ^ this.Line) * 397 ^ this.Column) * 397 ^ (this.Filename != null ? this.Filename.GetHashCode() : 0)) * 397 ^ (this.Method != null ? this.Method.GetHashCode() : 0)) * 397 ^ (this.Namespace != null ? this.Namespace.GetHashCode() : 0);
    }
  }
}
