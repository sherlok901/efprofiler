﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Appender.StackTraces.StackTraceGenerator
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Reflection.Emit;
using System.Threading;

namespace HibernatingRhinos.Profiler.Appender.StackTraces
{
  public class StackTraceGenerator
  {
    private StackTraceGenerator.IStackTraceCache stackTraceCache;
    private readonly IStackTraceFilter[] stackTraceFilters;

    public StackTraceGenerator(IStackTraceFilter[] stackTraceFilters)
    {
      this.stackTraceFilters = stackTraceFilters;
      try
      {
        this.CheatClrAndUseDynamicMethodsToGetStackTraceFast();
        this.stackTraceCache.GetStackTace();
      }
      catch (Exception ex)
      {
        Trace.WriteLine("Could not create fast stack trace cache, falling back to old supported way, failure because: " + (object) ex);
        this.SlowAndSafeApproachToGetStackTrace();
      }
    }

    public bool DotNotFixDynamicProxyStackTrace { get; set; }

    private bool RequiresStackTrace(string logger, string message, Level level)
    {
      if (level >= Level.Warn)
        return true;
      foreach (IStackTraceFilter stackTraceFilter in this.stackTraceFilters)
      {
        if (stackTraceFilter.Applies(logger, message))
          return true;
      }
      return false;
    }

    public StackTraceInfo GetStackTrace(string logger, string message, Level level)
    {
      if (!this.RequiresStackTrace(logger, message, level))
        return (StackTraceInfo) null;
      List<StackTraceFrame> stackTraceFrameList = new List<StackTraceFrame>();
      StackFrame[] frames = this.GetStackTrace().GetFrames();
      if (frames == null)
        return (StackTraceInfo) null;
      foreach (StackFrame stackFrame in frames)
      {
        Type declaringType = stackFrame.GetMethod().DeclaringType;
        if (declaringType != null)
          stackTraceFrameList.Add(new StackTraceFrame()
          {
            Column = stackFrame.GetFileColumnNumber(),
            Line = stackFrame.GetFileLineNumber(),
            FullFilename = stackFrame.GetFileName(),
            Method = stackFrame.GetMethod().Name,
            Type = declaringType.FullName,
            Namespace = declaringType.Namespace ?? ""
          });
      }
      return new StackTraceInfo()
      {
        Frames = stackTraceFrameList.ToArray()
      };
    }

    private void SlowAndSafeApproachToGetStackTrace()
    {
      this.stackTraceCache = (StackTraceGenerator.IStackTraceCache) new StackTraceGenerator.StackTraceCache((StackTraceGenerator.GetKey) (() =>
      {
        StackFrame[] frames = new StackTrace(false).GetFrames();
        if (frames == null)
          return new StackTraceGenerator.Key(new StackTraceGenerator.MethodHandleAndILOffset[0]);
        StackTraceGenerator.MethodHandleAndILOffset[] items = new StackTraceGenerator.MethodHandleAndILOffset[frames.Length];
        for (int index = 0; index < items.Length; ++index)
          items[index] = new StackTraceGenerator.MethodHandleAndILOffset(frames[index].GetMethod().MethodHandle.Value, frames[index].GetILOffset());
        return new StackTraceGenerator.Key(items);
      }));
    }

    public StackTrace GetStackTrace()
    {
      return this.stackTraceCache.GetStackTace();
    }

    private void CheatClrAndUseDynamicMethodsToGetStackTraceFast()
    {
      Type type = typeof (object).Assembly.GetType("System.Diagnostics.StackFrameHelper");
      FieldInfo field1 = type.GetField("rgMethodHandle", BindingFlags.Instance | BindingFlags.NonPublic);
      FieldInfo field2 = type.GetField("rgiILOffset", BindingFlags.Instance | BindingFlags.NonPublic);
      MethodInfo method = Type.GetType("System.Diagnostics.StackTrace, mscorlib").GetMethod("GetStackFramesInternal", BindingFlags.Static | BindingFlags.NonPublic);
      DynamicMethod dynamicMethod = new DynamicMethod("GetStackTraceFast", typeof (StackTraceGenerator.MethodHandleAndILOffset[]), new Type[0], type, true);
      ILGenerator ilGenerator = dynamicMethod.GetILGenerator();
      ilGenerator.DeclareLocal(type);
      ilGenerator.Emit(OpCodes.Ldc_I4_0);
      ilGenerator.Emit(OpCodes.Ldnull);
      ilGenerator.Emit(OpCodes.Newobj, type.GetConstructor(new Type[2]
      {
        typeof (bool),
        typeof (Thread)
      }));
      ilGenerator.Emit(OpCodes.Stloc_0);
      ilGenerator.Emit(OpCodes.Ldloc_0);
      ilGenerator.Emit(OpCodes.Ldc_I4_0);
      ilGenerator.Emit(OpCodes.Ldnull);
      ilGenerator.Emit(OpCodes.Call, method);
      ilGenerator.Emit(OpCodes.Ldloc_0);
      ilGenerator.Emit(OpCodes.Ldfld, field1);
      ilGenerator.Emit(OpCodes.Ldloc_0);
      ilGenerator.Emit(OpCodes.Ldfld, field2);
      ilGenerator.Emit(OpCodes.Call, typeof (StackTraceGenerator.MethodHandleAndILOffset).GetMethod("Create"));
      ilGenerator.Emit(OpCodes.Ret);
      StackTraceGenerator.GetMethodRuntimeHandles getTheStackTrace = (StackTraceGenerator.GetMethodRuntimeHandles) dynamicMethod.CreateDelegate(typeof (StackTraceGenerator.GetMethodRuntimeHandles));
      this.stackTraceCache = (StackTraceGenerator.IStackTraceCache) new StackTraceGenerator.StackTraceCache((StackTraceGenerator.GetKey) (() => new StackTraceGenerator.Key(getTheStackTrace())));
    }

    private delegate StackTraceGenerator.MethodHandleAndILOffset[] GetMethodRuntimeHandles();

    private interface IStackTraceCache
    {
      StackTrace GetStackTace();
    }

    private delegate StackTraceGenerator.Key GetKey();

    private class StackTraceCache : StackTraceGenerator.IStackTraceCache
    {
      private readonly ReaderWriterLock rwLock = new ReaderWriterLock();
      private readonly IDictionary<StackTraceGenerator.Key, StackTrace> cachedTraces = (IDictionary<StackTraceGenerator.Key, StackTrace>) new Dictionary<StackTraceGenerator.Key, StackTrace>();
      private readonly StackTraceGenerator.GetKey createKey;

      public StackTraceCache(StackTraceGenerator.GetKey createKey)
      {
        this.createKey = createKey;
      }

      public StackTrace GetStackTace()
      {
        StackTraceGenerator.Key key = this.createKey();
        this.rwLock.AcquireReaderLock(-1);
        try
        {
          StackTrace stackTrace;
          if (this.cachedTraces.TryGetValue(key, out stackTrace))
            return stackTrace;
        }
        finally
        {
          this.rwLock.ReleaseReaderLock();
        }
        this.rwLock.AcquireWriterLock(-1);
        try
        {
          return this.cachedTraces[key] = new StackTrace(true);
        }
        finally
        {
          this.rwLock.ReleaseWriterLock();
        }
      }
    }

    private class MethodHandleAndILOffset
    {
      private readonly IntPtr methodHandle;
      private readonly int offset;

      public static StackTraceGenerator.MethodHandleAndILOffset[] Create(IntPtr[] methods, int[] offsets)
      {
        StackTraceGenerator.MethodHandleAndILOffset[] handleAndIlOffsetArray = new StackTraceGenerator.MethodHandleAndILOffset[methods.Length];
        for (int index = 0; index < methods.Length; ++index)
          handleAndIlOffsetArray[index] = new StackTraceGenerator.MethodHandleAndILOffset(methods[index], offsets[index]);
        return handleAndIlOffsetArray;
      }

      public MethodHandleAndILOffset(IntPtr methodHandle, int offset)
      {
        this.methodHandle = methodHandle;
        this.offset = offset;
      }

      public bool Equals(StackTraceGenerator.MethodHandleAndILOffset other)
      {
        if (object.ReferenceEquals((object) null, (object) other))
          return false;
        if (object.ReferenceEquals((object) this, (object) other))
          return true;
        if (other.methodHandle.Equals((object) this.methodHandle))
          return other.offset == this.offset;
        return false;
      }

      public override bool Equals(object obj)
      {
        if (object.ReferenceEquals((object) null, obj))
          return false;
        if (object.ReferenceEquals((object) this, obj))
          return true;
        return this.Equals(obj as StackTraceGenerator.MethodHandleAndILOffset);
      }

      public override int GetHashCode()
      {
        return this.methodHandle.GetHashCode() * 397 ^ this.offset;
      }
    }

    private class Key
    {
      private readonly StackTraceGenerator.MethodHandleAndILOffset[] Items;

      public Key(StackTraceGenerator.MethodHandleAndILOffset[] items)
      {
        this.Items = items;
      }

      private bool Equals(StackTraceGenerator.Key other)
      {
        if (object.ReferenceEquals((object) null, (object) other))
          return false;
        if (object.ReferenceEquals((object) this, (object) other))
          return true;
        if (other.Items.Length != this.Items.Length)
          return false;
        for (int index = 0; index < this.Items.Length; ++index)
        {
          if (!other.Items[index].Equals(this.Items[index]))
            return false;
        }
        return true;
      }

      public override bool Equals(object obj)
      {
        if (object.ReferenceEquals((object) null, obj))
          return false;
        if (object.ReferenceEquals((object) this, obj))
          return true;
        return this.Equals(obj as StackTraceGenerator.Key);
      }

      public override int GetHashCode()
      {
        int num = 0;
        for (int index = 0; index < this.Items.Length; ++index)
          num = this.Items[index].GetHashCode() * 397 ^ num;
        return num;
      }
    }
  }
}
