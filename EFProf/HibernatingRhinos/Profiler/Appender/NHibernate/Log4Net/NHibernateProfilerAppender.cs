//-----------------------------------------------------------------------
// <copyright file="NHibernateProfilerAppender.cs" company="Hibernating Rhinos LTD">
//     Copyright (c) Hibernating Rhinos LTD. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using log4net;
using log4net.Appender;
using log4net.Core;

namespace HibernatingRhinos.Profiler.Appender.NHibernate
{
    public class NHibernateProfilerAppender : AppenderSkeleton
    {
        private readonly SessionIdLoggingContext sessionIdLoggingContext = new SessionIdLoggingContext();

        private readonly IDictionary<string, log4net.Core.Level> levels = new Dictionary<string, log4net.Core.Level>();

        public string SessionId
        {
            get
            {
                if (sessionIdLoggingContext.GetSessionId != null)
                {
                    var sessionId = sessionIdLoggingContext.GetSessionId();
                    if (sessionId != null)
                        return sessionId.Value.ToString();
                }
                else
                {
                    var sessionId = ThreadContext.Properties["sessionId"];
                    if (sessionId != null) //nh 2.1 Beta - which will report which session we use
                    {
                        return sessionId.ToString();
                    }
                }

                return ProfilerIntegration.CurrentThreadId;
            }
        }

        protected override bool RequiresLayout
        {
            get
            {
                return false;
            }
        }

        protected override void Append(LoggingEvent loggingEvent)
        {
            try
            {
               ProfilerIntegration.PublishProfilerEvent(
                    SessionId,
                    loggingEvent.LoggerName,
                    loggingEvent.RenderedMessage,
                    loggingEvent.TimeStamp,
                    GetLevel(loggingEvent)
                    );
            }
            catch (Exception e)
            {
                ErrorHandler.Error("Could not create message to send", e);
            }
        }

        private static Level GetLevel(LoggingEvent loggingEvent)
        {
            if (loggingEvent.Level == null)
                return Level.Debug;
            if (loggingEvent.Level == log4net.Core.Level.Warn)
                return Level.Warn;
            if (loggingEvent.Level == log4net.Core.Level.Error)
                return Level.Error;
            return Level.Debug;
        }

        protected override void OnClose()
        {
            base.OnClose();
            if (WrapLog4NetLogger.DoNotRestartNHProfLogging) 
                return;
            var copy = OnLoggerClosed;
            if (copy != null)
                copy(this);
        }

        public event Action<object> OnLoggerClosed = delegate { };

        public log4net.Core.Level GetPerviousLevel(string loggerName)
        {
            log4net.Core.Level value;
            if(levels.TryGetValue(loggerName, out value))
                return value;
            return log4net.Core.Level.Warn;
        }

        public void RegisterLoggerLevel(string name, log4net.Core.Level level)
        {
            levels[name] = level;
        }

        public void ClearCloseEventListeners()
        {
            OnLoggerClosed = null;
        }
    }
}