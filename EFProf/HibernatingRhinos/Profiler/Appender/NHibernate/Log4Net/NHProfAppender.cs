//-----------------------------------------------------------------------
// <copyright file="NHProfAppender.cs" company="Hibernating Rhinos LTD">
//     Copyright (c) Hibernating Rhinos LTD. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
using System;
using HibernatingRhinos.Profiler.Appender.StackTraces;
using log4net.Appender;
using log4net.Core;

namespace HibernatingRhinos.Profiler.Appender.NHibernate
{
	public class NHProfAppender : AppenderSkeleton
	{
		public string Sink { get; set; }

		public bool DotNotFixDynamicProxyStackTrace { get; set; }

		public bool SkipCapturingStackTraces { get; set; }

		public override void ActivateOptions()
		{
			base.ActivateOptions();

			var configuration = new NHibernateAppenderConfiguration
			{
				HostToSendProfilingInformationTo = new Uri(Sink).Host,
				Port = new Uri(Sink).Port,
				DotNotFixDynamicProxyStackTrace = DotNotFixDynamicProxyStackTrace,
			};
			if (SkipCapturingStackTraces)
				configuration.StackTraceFilters = new IStackTraceFilter[0];

			NHibernateProfiler.Initialize(configuration);

		}

		protected override void Append(LoggingEvent loggingEvent)
		{
			// do nothing
		}
	}
}