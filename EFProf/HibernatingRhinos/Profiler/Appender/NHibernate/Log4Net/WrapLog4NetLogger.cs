using System;
using System.Threading;
using log4net;
using log4net.Appender;
using log4net.Core;
using log4net.Repository.Hierarchy;

namespace HibernatingRhinos.Profiler.Appender.NHibernate
{
    public static class WrapLog4NetLogger
    {
        internal static bool DoNotRestartNHProfLogging;

        public static void Initialize()
        {
            DoNotRestartNHProfLogging = false;
            RegisterAppender();
        }

        public static void Shutdown()
        {
            DoNotRestartNHProfLogging = true;

            var repository = (Hierarchy)LogManager.GetRepository();
            Logger logger;
            foreach (var name in NHibernateProfiler.LoggerNames)
            {
                logger = (Logger)repository.GetLogger(name);
                RemoveAppender(name, logger);
            }
            logger = (Logger)repository.GetLogger("NHibernate");
            RemoveAppender("NHibernate", logger);
        }

        private static void RegisterAppender()
        {
            var repository = (Hierarchy)LogManager.GetRepository();
            var profilerAppender = new NHibernateProfilerAppender
            {
                Name = "NHibernate.Profiler",
            };

            profilerAppender.OnLoggerClosed += sender =>
            {
                if (DoNotRestartNHProfLogging)
                    return;
                ProfilerAppenderOnOnLoggerClose();
            };

            Logger logger;
            foreach (var name in NHibernateProfiler.LoggerNames)
            {
                logger = (Logger)repository.GetLogger(name);
                profilerAppender.RegisterLoggerLevel(name, logger.Level);
                logger.Level = log4net.Core.Level.Debug;
                logger.Additivity = false;
                AddAppenderOnlyOnce(profilerAppender, logger);
            }
            logger = (Logger)repository.GetLogger("NHibernate");
            profilerAppender.RegisterLoggerLevel("NHibernate", logger.Level);
            if (logger.Level == null || logger.Level.Value > log4net.Core.Level.Warn.Value)
                logger.Level = log4net.Core.Level.Warn;
            AddAppenderOnlyOnce(profilerAppender, logger);
            profilerAppender.ActivateOptions();
            repository.Configured = true;
        }

        private static void AddAppenderOnlyOnce(IAppender profilerAppender, IAppenderAttachable logger)
        {
            IAppender appender;
            do
            {
                appender = logger.GetAppender(profilerAppender.Name);
                if (appender != null)
                {
                    try
                    {
                        logger.RemoveAppender(appender);
                    }
                    catch (Exception)
                    {
                        //multi threading ? Funky log traversal?   
                    }
                }
            } while (appender != null);
            logger.AddAppender(profilerAppender);
        }

        private static void RemoveAppender(string name, Logger logger)
        {
            NHibernateProfilerAppender appender;
            while (true)
            {
                appender = (NHibernateProfilerAppender)logger.GetAppender("NHibernate.Profiler");
                if (appender == null)
                    return;
                appender.ClearCloseEventListeners();
                logger.Level = appender.GetPerviousLevel(name);
                logger.RemoveAppender(appender);
            }
        }

        private static void ProfilerAppenderOnOnLoggerClose()
        {
            new Thread(() =>
            {
                Thread.Sleep(250);
                RegisterAppender();
            })
            {
                IsBackground = true,
                Name = "Restarting NH Prof logging"
            }.Start();
        }
    }
}