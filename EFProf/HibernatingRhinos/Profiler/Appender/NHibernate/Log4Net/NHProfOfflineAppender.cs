//-----------------------------------------------------------------------
// <copyright file="NHProfOfflineAppender.cs" company="Hibernating Rhinos LTD">
//     Copyright (c) Hibernating Rhinos LTD. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
using log4net.Appender;
using log4net.Core;

namespace HibernatingRhinos.Profiler.Appender.NHibernate
{
    public class NHProfOfflineAppender : AppenderSkeleton
    {
        public string File { get; set; }

        public override void ActivateOptions()
        {
            base.ActivateOptions();

            NHibernateProfiler.Initialize(new NHibernateAppenderConfiguration
            {
                FileToLogTo = File
            });

        }

        protected override void Append(LoggingEvent loggingEvent)
        {
            // do nothing
        }
    }
}