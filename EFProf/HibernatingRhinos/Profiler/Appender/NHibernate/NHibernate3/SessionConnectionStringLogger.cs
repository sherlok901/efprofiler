using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Reflection;

namespace HibernatingRhinos.Profiler.Appender.NHibernate3Logger
{
	public class SessionConnectionStringLogger
	{
		private readonly ProfilerLogger logger;
		private readonly IDictionary factoryInstances;
		private readonly PropertyInfo connectionProviderProperty;
        private static readonly Dictionary<string, object> NotedSessionDictionary = new Dictionary<string, object>();
         

		public SessionConnectionStringLogger()
		{
			logger =  new ProfilerLogger(null, "NHibernate.SessionConnectionString", this);

			try
			{
				var nhibernate = Assembly.Load("NHibernate");
				Type sessionFactoryObjectFactory = nhibernate.GetType("NHibernate.Impl.SessionFactoryObjectFactory");
				FieldInfo instancesField = sessionFactoryObjectFactory.GetField("Instances", BindingFlags.Static | BindingFlags.NonPublic);
				factoryInstances = (IDictionary) instancesField.GetValue(null);

				var sessionFactoryImplType = nhibernate.GetType("NHibernate.Impl.SessionFactoryImpl");
				connectionProviderProperty = sessionFactoryImplType.GetProperty("ConnectionProvider", BindingFlags.Instance | BindingFlags.Public);
			}
			catch (Exception)
			{
				// could not load it for some reason, might be permissions or versioning issues after all
				// we will just set it to no instances
			}
		}

		public void PullConnectionStrings()
		{
			if (ProfilerInfrastructure.IgnoreConnectionStrings)
				return;
			
			if (factoryInstances == null || connectionProviderProperty == null)
				return;

            
			
			foreach (var sessionFactory in factoryInstances.Values)
			{
			    var sessionFactoryType = sessionFactory.GetType();
                // call only new ones
			    var uuidProperty = sessionFactoryType.GetProperty("Uuid");
                string uuid = (string)uuidProperty.GetValue(sessionFactory, null);
			    if (NotedSessionDictionary.ContainsKey(uuid))
			        continue;

			    NotedSessionDictionary[uuid] = sessionFactory;
                
				var connectionProvider = connectionProviderProperty.GetValue(sessionFactory, new object[0]);
				if (connectionProvider == null)
					continue;

                var connectionProviderType = connectionProvider.GetType();

			    
                var connectionStringProperty = connectionProviderType.GetProperty("ConnectionString", BindingFlags.Instance | BindingFlags.NonPublic);
				if (connectionStringProperty == null)
					continue;
                var connectionString = connectionStringProperty.GetValue(connectionProvider, new object[0]);


                var getConnection = connectionProviderType.GetMethod("GetConnection");
			    if (getConnection == null)
			        continue;

			    string connectionTypeName;

			    IDbConnection conn = (IDbConnection) getConnection.Invoke(connectionProvider, null);
			    try
			    {
                    if (conn == null)
                        continue;

                    connectionTypeName = conn.GetType().FullName;
                }
			    finally
			    {
			        var coloseConnection = connectionProviderType.GetMethod("CloseConnection");
			        if (coloseConnection != null)
			        {
			            coloseConnection.Invoke(connectionProvider, new object[] {conn});

			        }

			    }

			    if (string.IsNullOrEmpty(connectionTypeName))
			        continue;
                //notedSessionDictionary
                logger.Info(connectionString + "|" + connectionTypeName);
			}

		}
	}
}