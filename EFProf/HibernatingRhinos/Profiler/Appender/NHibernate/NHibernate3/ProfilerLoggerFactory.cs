using System;
using HibernatingRhinos.Profiler.Appender.NHibernate;
using NHibernate;

namespace HibernatingRhinos.Profiler.Appender.NHibernate3Logger
{
	public class ProfilerLoggerFactory : ILoggerFactory
	{
		private readonly SessionConnectionStringLogger connectionStringLogger;
		public static ILoggerFactory OriginalLoggerFactory = new NoLoggingLoggerFactory();

		public ProfilerLoggerFactory(SessionConnectionStringLogger connectionStringLogger)
		{
			this.connectionStringLogger = connectionStringLogger;
		}

		public IInternalLogger LoggerFor(string keyName)
		{
		    bool logErrorsOnlyMode = Array.IndexOf(NHibernateProfiler.LoggerNames, keyName) == -1;
			return new ProfilerLogger(OriginalLoggerFactory.LoggerFor(keyName), keyName, logErrorsOnlyMode, connectionStringLogger);
		}

		public IInternalLogger LoggerFor(Type type)
		{
			return LoggerFor(type.FullName);
		}
	}
}