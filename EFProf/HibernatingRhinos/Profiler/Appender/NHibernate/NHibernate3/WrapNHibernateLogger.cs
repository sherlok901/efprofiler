using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using NHibernate;

namespace HibernatingRhinos.Profiler.Appender.NHibernate3Logger
{
	public static class WrapNHibernateLogger
	{
		public static void Initialize()
		{
			var originalLoggerFactory = GetOriginalLoggerFactory();
			if (originalLoggerFactory.GetType() == typeof (ProfilerLoggerFactory))
			{
				// If we got here, this is probably a bug, probably caused by a user calling the Initialize twice without call Shutdown in between.
				originalLoggerFactory = null;
#if DEBUG
				Debugger.Launch();
				Debugger.Break();
#endif
			}
			ProfilerLoggerFactory.OriginalLoggerFactory = originalLoggerFactory;
			LoggerProvider.SetLoggersFactory(new ProfilerLoggerFactory(new SessionConnectionStringLogger()));

			SetLoggerOnStaticFields();
		}

		private static ILoggerFactory GetOriginalLoggerFactory()
		{
			// Get original logger factory from: LoggerProvider.instance.loggerFactory
			var loggerProvider = typeof (LoggerProvider);
			var instanceField = loggerProvider.GetField("instance", BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.GetField);
			var instance = (LoggerProvider)instanceField.GetValue(null);
			var loggerFactoryField = loggerProvider.GetField("loggerFactory", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.GetField);
			var loggerFactory = (ILoggerFactory)loggerFactoryField.GetValue(instance);
			return loggerFactory ?? new NoLoggingLoggerFactory();
		}

		private static void SetLoggerOnStaticFields()
		{
			var nhibernate = typeof(ISessionFactory).Assembly;

			var loggers = new[]
								  {
									  
new {LoggerType = "NHibernate.SQL", ClassName="NHibernate.AdoNet.Util.SqlStatementLogger", FieldName = "log"},
new {LoggerType = "NHibernate.AdoNet.AbstractBatcher", ClassName="NHibernate.AdoNet.AbstractBatcher", FieldName = "log"},
new {LoggerType = "NHibernate.AdoNet.ConnectionManager", ClassName="NHibernate.AdoNet.ConnectionManager", FieldName = "log"},
new {LoggerType = "NHibernate.Bytecode.CodeDom.BytecodeProviderImpl", ClassName="NHibernate.Bytecode.CodeDom.BytecodeProviderImpl", FieldName = "log"},
new {LoggerType = "NHibernate.Cache.ReadWriteCache", ClassName="NHibernate.Cache.ReadWriteCache", FieldName = "log"},
new {LoggerType = "NHibernate.Cache.CacheFactory", ClassName="NHibernate.Cache.CacheFactory", FieldName = "log"},
new {LoggerType = "NHibernate.Cache.NoCacheProvider", ClassName="NHibernate.Cache.NoCacheProvider", FieldName = "log"},
new {LoggerType = "NHibernate.Cache.NonstrictReadWriteCache", ClassName="NHibernate.Cache.NonstrictReadWriteCache", FieldName = "log"},
new {LoggerType = "NHibernate.Cache.ReadOnlyCache", ClassName="NHibernate.Cache.ReadOnlyCache", FieldName = "log"},
new {LoggerType = "NHibernate.Cache.StandardQueryCache", ClassName="NHibernate.Cache.StandardQueryCache", FieldName = "log"},
new {LoggerType = "NHibernate.Cache.UpdateTimestampsCache", ClassName="NHibernate.Cache.UpdateTimestampsCache", FieldName = "log"},
new {LoggerType = "NHibernate.Cfg.ConfigurationSchema.HibernateConfiguration", ClassName="NHibernate.Cfg.ConfigurationSchema.HibernateConfiguration", FieldName = "log"},
new {LoggerType = "NHibernate.Cfg.XmlHbmBinding.Binder", ClassName="NHibernate.Cfg.XmlHbmBinding.Binder", FieldName = "log"},
new {LoggerType = "NHibernate.Cfg.XmlHbmBinding.FilterDefinitionFactory", ClassName="NHibernate.Cfg.XmlHbmBinding.FilterDefinitionFactory", FieldName = "log"},
new {LoggerType = "NHibernate.Cfg.Configuration", ClassName="NHibernate.Cfg.Configuration", FieldName = "log"},
new {LoggerType = "NHibernate.Cfg.Environment", ClassName="NHibernate.Cfg.Environment", FieldName = "log"},
new {LoggerType = "NHibernate.Cfg.Mappings", ClassName="NHibernate.Cfg.Mappings", FieldName = "log"},
new {LoggerType = "NHibernate.Cfg.SettingsFactory", ClassName="NHibernate.Cfg.SettingsFactory", FieldName = "log"},
new {LoggerType = "NHibernate.Collection.PersistentArrayHolder", ClassName="NHibernate.Collection.PersistentArrayHolder", FieldName = "log"},
new {LoggerType = "NHibernate.Connection.ConnectionProvider", ClassName="NHibernate.Connection.ConnectionProvider", FieldName = "log"},
new {LoggerType = "NHibernate.Connection.ConnectionProviderFactory", ClassName="NHibernate.Connection.ConnectionProviderFactory", FieldName = "log"},
new {LoggerType = "NHibernate.Connection.DriverConnectionProvider", ClassName="NHibernate.Connection.DriverConnectionProvider", FieldName = "log"},
new {LoggerType = "NHibernate.Connection.UserSuppliedConnectionProvider", ClassName="NHibernate.Connection.UserSuppliedConnectionProvider", FieldName = "log"},
new {LoggerType = "NHibernate.Context.ThreadLocalSessionContext", ClassName="NHibernate.Context.ThreadLocalSessionContext", FieldName = "log"},
new {LoggerType = "NHibernate.Dialect.Lock.UpdateLockingStrategy", ClassName="NHibernate.Dialect.Lock.UpdateLockingStrategy", FieldName = "log"},
new {LoggerType = "NHibernate.Dialect.Schema.ITableMetadata", ClassName="NHibernate.Dialect.Schema.AbstractTableMetadata", FieldName = "log"},
new {LoggerType = "NHibernate.Dialect.Dialect", ClassName="NHibernate.Dialect.Dialect", FieldName = "log"},
new {LoggerType = "NHibernate.Driver.DriverBase", ClassName="NHibernate.Driver.DriverBase", FieldName = "log"},
new {LoggerType = "NHibernate.Engine.Loading.CollectionLoadContext", ClassName="NHibernate.Engine.Loading.CollectionLoadContext", FieldName = "log"},
new {LoggerType = "NHibernate.Engine.Loading.LoadContexts", ClassName="NHibernate.Engine.Loading.LoadContexts", FieldName = "log"},
new {LoggerType = "NHibernate.Engine.Query.HQLQueryPlan", ClassName="NHibernate.Engine.Query.HQLQueryPlan", FieldName = "Log"},
new {LoggerType = "NHibernate.Engine.Query.NativeSQLQueryPlan", ClassName="NHibernate.Engine.Query.NativeSQLQueryPlan", FieldName = "log"},
new {LoggerType = "NHibernate.Engine.Query.QueryPlanCache", ClassName="NHibernate.Engine.Query.QueryPlanCache", FieldName = "log"},
new {LoggerType = "NHibernate.Engine.Transaction.Isolater", ClassName="NHibernate.Engine.Transaction.Isolater", FieldName = "log"},
new {LoggerType = "NHibernate.Engine.ActionQueue", ClassName="NHibernate.Engine.ActionQueue", FieldName = "log"},
new {LoggerType = "NHibernate.Engine.Cascade", ClassName="NHibernate.Engine.Cascade", FieldName = "log"},
new {LoggerType = "NHibernate.Engine.CascadingAction", ClassName="NHibernate.Engine.CascadingAction", FieldName = "log"},
new {LoggerType = "NHibernate.Engine.CollectionEntry", ClassName="NHibernate.Engine.CollectionEntry", FieldName = "log"},
new {LoggerType = "NHibernate.Engine.Collections", ClassName="NHibernate.Engine.Collections", FieldName = "log"},
new {LoggerType = "NHibernate.Engine.ForeignKeys", ClassName="NHibernate.Engine.ForeignKeys", FieldName = "log"},
new {LoggerType = "NHibernate.Engine.IdentifierValue", ClassName="NHibernate.Engine.IdentifierValue", FieldName = "log"},
new {LoggerType = "NHibernate.Engine.QueryParameters", ClassName="NHibernate.Engine.QueryParameters", FieldName = "log"},
new {LoggerType = "NHibernate.Engine.StatefulPersistenceContext", ClassName="NHibernate.Engine.StatefulPersistenceContext", FieldName = "log"},
new {LoggerType = "NHibernate.Engine.StatefulPersistenceContext.ProxyWarnLog", ClassName="NHibernate.Engine.StatefulPersistenceContext", FieldName = "ProxyWarnLog"},
new {LoggerType = "NHibernate.Engine.TwoPhaseLoad", ClassName="NHibernate.Engine.TwoPhaseLoad", FieldName = "log"},
new {LoggerType = "NHibernate.Engine.Versioning", ClassName="NHibernate.Engine.Versioning", FieldName = "log"},
new {LoggerType = "NHibernate.Engine.VersionValue", ClassName="NHibernate.Engine.VersionValue", FieldName = "log"},
new {LoggerType = "NHibernate.Event.Default.AbstractFlushingEventListener", ClassName="NHibernate.Event.Default.AbstractFlushingEventListener", FieldName = "log"},
new {LoggerType = "NHibernate.Event.Default.AbstractReassociateEventListener", ClassName="NHibernate.Event.Default.AbstractReassociateEventListener", FieldName = "log"},
new {LoggerType = "NHibernate.Event.Default.AbstractLockUpgradeEventListener", ClassName="NHibernate.Event.Default.AbstractLockUpgradeEventListener", FieldName = "log"},
new {LoggerType = "NHibernate.Event.Default.AbstractSaveEventListener", ClassName="NHibernate.Event.Default.AbstractSaveEventListener", FieldName = "log"},
new {LoggerType = "NHibernate.Event.Default.DefaultAutoFlushEventListener", ClassName="NHibernate.Event.Default.DefaultAutoFlushEventListener", FieldName = "log"},
new {LoggerType = "NHibernate.Event.Default.DefaultDeleteEventListener", ClassName="NHibernate.Event.Default.DefaultDeleteEventListener", FieldName = "log"},
new {LoggerType = "NHibernate.Event.Default.DefaultDirtyCheckEventListener", ClassName="NHibernate.Event.Default.DefaultDirtyCheckEventListener", FieldName = "log"},
new {LoggerType = "NHibernate.Event.Default.DefaultEvictEventListener", ClassName="NHibernate.Event.Default.DefaultEvictEventListener", FieldName = "log"},
new {LoggerType = "NHibernate.Event.Default.DefaultFlushEntityEventListener", ClassName="NHibernate.Event.Default.DefaultFlushEntityEventListener", FieldName = "log"},
new {LoggerType = "NHibernate.Event.Default.DefaultInitializeCollectionEventListener", ClassName="NHibernate.Event.Default.DefaultInitializeCollectionEventListener", FieldName = "log"},
new {LoggerType = "NHibernate.Event.Default.DefaultLoadEventListener", ClassName="NHibernate.Event.Default.DefaultLoadEventListener", FieldName = "log"},
new {LoggerType = "NHibernate.Event.Default.DefaultMergeEventListener", ClassName="NHibernate.Event.Default.DefaultMergeEventListener", FieldName = "log"},
new {LoggerType = "NHibernate.Event.Default.DefaultPersistEventListener", ClassName="NHibernate.Event.Default.DefaultPersistEventListener", FieldName = "log"},
new {LoggerType = "NHibernate.Event.Default.DefaultRefreshEventListener", ClassName="NHibernate.Event.Default.DefaultRefreshEventListener", FieldName = "log"},
new {LoggerType = "NHibernate.Event.Default.DefaultReplicateEventListener", ClassName="NHibernate.Event.Default.DefaultReplicateEventListener", FieldName = "log"},
new {LoggerType = "NHibernate.Event.Default.DefaultSaveOrUpdateEventListener", ClassName="NHibernate.Event.Default.DefaultSaveOrUpdateEventListener", FieldName = "log"},
new {LoggerType = "NHibernate.Event.Default.EvictVisitor", ClassName="NHibernate.Event.Default.EvictVisitor", FieldName = "log"},
new {LoggerType = "NHibernate.Event.Default.AbstractFlushingEventListener", ClassName="NHibernate.Event.Default.ReattachVisitor", FieldName = "log"},
new {LoggerType = "NHibernate.Event.Default.WrapVisitor", ClassName="NHibernate.Event.Default.WrapVisitor", FieldName = "log"},
new {LoggerType = "NHibernate.Exceptions.SQLExceptionConverterFactory", ClassName="NHibernate.Exceptions.SQLExceptionConverterFactory", FieldName = "log"},
new {LoggerType = "NHibernate.Hql.Ast.ANTLR.Exec.BasicExecutor", ClassName="NHibernate.Hql.Ast.ANTLR.Exec.BasicExecutor", FieldName = "log"},
new {LoggerType = "NHibernate.Hql.Ast.ANTLR.Exec.MultiTableDeleteExecutor", ClassName="NHibernate.Hql.Ast.ANTLR.Exec.MultiTableDeleteExecutor", FieldName = "log"},
new {LoggerType = "NHibernate.Hql.Ast.ANTLR.Exec.MultiTableDeleteExecutor", ClassName="NHibernate.Hql.Ast.ANTLR.Exec.MultiTableUpdateExecutor", FieldName = "log"},
new {LoggerType = "NHibernate.Hql.Ast.ANTLR.HqlParser", ClassName="NHibernate.Hql.Ast.ANTLR.HqlParser", FieldName = "log"},
new {LoggerType = "NHibernate.Hql.Ast.ANTLR.HqlSqlWalker", ClassName="NHibernate.Hql.Ast.ANTLR.HqlSqlWalker", FieldName = "log"},
new {LoggerType = "NHibernate.Loader.Loader", ClassName="NHibernate.Loader.Loader", FieldName = "log"},
// new {LoggerType = "NHibernate.Loader.Loader", ClassName="NHibernate.Loader.Loader", FieldName = "Log"}, // Version:  3.3.3.4000
new {LoggerType = "NHibernate.Hql.Ast.ANTLR.Tree.MethodNode", ClassName="NHibernate.Hql.Ast.ANTLR.Tree.MethodNode", FieldName = "Log"},
new {LoggerType = "NHibernate.Hql.Ast.ANTLR.Tree.DeleteStatement", ClassName="NHibernate.Hql.Ast.ANTLR.Tree.DeleteStatement", FieldName = "Log"},
new {LoggerType = "NHibernate.Hql.Ast.ANTLR.Tree.FromReferenceNode", ClassName="NHibernate.Hql.Ast.ANTLR.Tree.FromReferenceNode", FieldName = "Log"},
new {LoggerType = "NHibernate.Hql.Ast.ANTLR.Tree.DotNode", ClassName="NHibernate.Hql.Ast.ANTLR.Tree.DotNode", FieldName = "Log"},
new {LoggerType = "NHibernate.Hql.Ast.ANTLR.Tree.FromClause", ClassName="NHibernate.Hql.Ast.ANTLR.Tree.FromClause", FieldName = "Log"},
new {LoggerType = "NHibernate.Hql.Ast.ANTLR.Tree.FromElement", ClassName="NHibernate.Hql.Ast.ANTLR.Tree.FromElement", FieldName = "Log"},
new {LoggerType = "NHibernate.Hql.Ast.ANTLR.Tree.FromElementFactory", ClassName="NHibernate.Hql.Ast.ANTLR.Tree.FromElementFactory", FieldName = "Log"},
new {LoggerType = "NHibernate.Hql.Ast.ANTLR.Tree.FromElementType", ClassName="NHibernate.Hql.Ast.ANTLR.Tree.FromElementType", FieldName = "Log"},
new {LoggerType = "NHibernate.Hql.Ast.ANTLR.Tree.IndexNode", ClassName="NHibernate.Hql.Ast.ANTLR.Tree.IndexNode", FieldName = "Log"},
new {LoggerType = "NHibernate.Hql.Ast.ANTLR.Tree.QueryNode", ClassName="NHibernate.Hql.Ast.ANTLR.Tree.QueryNode", FieldName = "Log"},
new {LoggerType = "NHibernate.Hql.Ast.ANTLR.Tree.UpdateStatement", ClassName="NHibernate.Hql.Ast.ANTLR.Tree.UpdateStatement", FieldName = "Log"},
new {LoggerType = "NHibernate.Hql.Ast.ANTLR.Util.JoinProcessor", ClassName="NHibernate.Hql.Ast.ANTLR.Util.JoinProcessor", FieldName = "log"},
new {LoggerType = "NHibernate.Hql.Ast.ANTLR.Util.LiteralProcessor", ClassName="NHibernate.Hql.Ast.ANTLR.Util.LiteralProcessor", FieldName = "log"},
new {LoggerType = "NHibernate.Hql.Ast.ANTLR.Util.PathHelper", ClassName="NHibernate.Hql.Ast.ANTLR.Util.PathHelper", FieldName = "log"},
new {LoggerType = "NHibernate.Hql.Ast.ANTLR.Util.SyntheticAndFactory", ClassName="NHibernate.Hql.Ast.ANTLR.Util.SyntheticAndFactory", FieldName = "log"},
new {LoggerType = "NHibernate.Hql.Ast.ANTLR.ErrorCounter", ClassName="NHibernate.Hql.Ast.ANTLR.ErrorCounter", FieldName = "log"},
new {LoggerType = "NHibernate.Hql.Parser", ClassName="NHibernate.Hql.Ast.ANTLR.ErrorCounter", FieldName = "hqlLog"},
new {LoggerType = "NHibernate.Hql.Ast.ANTLR.QueryTranslatorImpl", ClassName="NHibernate.Hql.Ast.ANTLR.QueryTranslatorImpl", FieldName = "log"},
new {LoggerType = "NHibernate.Hql.Ast.ANTLR.HqlParseEngine", ClassName="NHibernate.Hql.Ast.ANTLR.HqlParseEngine", FieldName = "log"},
new {LoggerType = "NHibernate.Hql.Ast.ANTLR.HqlSqlGenerator", ClassName="NHibernate.Hql.Ast.ANTLR.HqlSqlGenerator", FieldName = "log"},
new {LoggerType = "NHibernate.Hql.Classic.QueryTranslator", ClassName="NHibernate.Hql.Classic.QueryTranslator", FieldName = "log"},
new {LoggerType = "NHibernate.Hql.QuerySplitter", ClassName="NHibernate.Hql.QuerySplitter", FieldName = "log"},
new {LoggerType = "NHibernate.Id.Enhanced.OptimizerFactory", ClassName="NHibernate.Id.Enhanced.OptimizerFactory", FieldName = "log"},
new {LoggerType = "NHibernate.Id.Enhanced.SequenceStructure", ClassName="NHibernate.Id.Enhanced.SequenceStructure", FieldName = "log"},
new {LoggerType = "NHibernate.Id.Enhanced.SequenceStyleGenerator", ClassName="NHibernate.Id.Enhanced.SequenceStyleGenerator", FieldName = "log"},
new {LoggerType = "NHibernate.Id.Enhanced.IDatabaseStructure", ClassName="NHibernate.Id.Enhanced.TableStructure", FieldName = "log"},
new {LoggerType = "NHibernate.SQL", ClassName="NHibernate.Id.Enhanced.TableStructure", FieldName = "SqlLog"},
new {LoggerType = "NHibernate.SqlCommand.SqlInsertBuilder", ClassName="NHibernate.SqlCommand.SqlInsertBuilder", FieldName = "log"},
new {LoggerType = "NHibernate.Id.IdentifierGeneratorFactory", ClassName="NHibernate.Id.IdentifierGeneratorFactory", FieldName = "log"},
new {LoggerType = "NHibernate.Id.IncrementGenerator", ClassName="NHibernate.Id.IncrementGenerator", FieldName = "log"},
new {LoggerType = "NHibernate.Id.NativeGuidGenerator", ClassName="NHibernate.Id.NativeGuidGenerator", FieldName = "log"},
new {LoggerType = "NHibernate.Id.SequenceGenerator", ClassName="NHibernate.Id.SequenceGenerator", FieldName = "log"},
new {LoggerType = "NHibernate.Id.SequenceHiLoGenerator", ClassName="NHibernate.Id.SequenceHiLoGenerator", FieldName = "log"},
new {LoggerType = "NHibernate.Id.TableGenerator", ClassName="NHibernate.Id.TableGenerator", FieldName = "log"},
new {LoggerType = "NHibernate.Id.TableHiLoGenerator", ClassName="NHibernate.Id.TableHiLoGenerator", FieldName = "log"},
new {LoggerType = "NHibernate.Impl.AbstractSessionImpl", ClassName="NHibernate.Impl.AbstractSessionImpl", FieldName = "logger"},
new {LoggerType = "NHibernate.Impl.EnumerableImpl", ClassName="NHibernate.Impl.EnumerableImpl", FieldName = "log"},
new {LoggerType = "NHibernate.Impl.MultiCriteriaImpl", ClassName="NHibernate.Impl.MultiCriteriaImpl", FieldName = "log"},
new {LoggerType = "NHibernate.Impl.MultiQueryImpl", ClassName="NHibernate.Impl.MultiQueryImpl", FieldName = "log"},
new {LoggerType = "NHibernate.Impl.Printer", ClassName="NHibernate.Impl.Printer", FieldName = "log"},
new {LoggerType = "NHibernate.Impl.SessionFactoryImpl", ClassName="NHibernate.Impl.SessionFactoryImpl", FieldName = "log"},
new {LoggerType = "NHibernate.Impl.SessionFactoryObjectFactory", ClassName="NHibernate.Impl.SessionFactoryObjectFactory", FieldName = "log"},
new {LoggerType = "NHibernate.Impl.SessionImpl", ClassName="NHibernate.Impl.SessionImpl", FieldName = "log"},
new {LoggerType = "NHibernate.Impl.StatelessSessionImpl", ClassName="NHibernate.Impl.StatelessSessionImpl", FieldName = "log"},
new {LoggerType = "NHibernate.Linq.Functions.LinqToHqlGeneratorsRegistryFactory", ClassName="NHibernate.Linq.Functions.LinqToHqlGeneratorsRegistryFactory", FieldName = "log"},
new {LoggerType = "NHibernate.Loader.Collection.BasicCollectionLoader", ClassName="NHibernate.Loader.Collection.BasicCollectionLoader", FieldName = "log"},
new {LoggerType = "NHibernate.Loader.Collection.OneToManyLoader", ClassName="NHibernate.Loader.Collection.OneToManyLoader", FieldName = "log"},
new {LoggerType = "NHibernate.Loader.Criteria.CriteriaJoinWalker", ClassName="NHibernate.Loader.Criteria.CriteriaJoinWalker", FieldName = "logger"},
new {LoggerType = "NHibernate.Loader.Criteria.CriteriaQueryTranslator", ClassName="NHibernate.Loader.Criteria.CriteriaQueryTranslator", FieldName = "logger"},
new {LoggerType = "NHibernate.Loader.Custom.Sql.SQLCustomQuery", ClassName="NHibernate.Loader.Custom.Sql.SQLCustomQuery", FieldName = "log"},
new {LoggerType = "NHibernate.Loader.Custom.Sql.SQLQueryReturnProcessor", ClassName="NHibernate.Loader.Custom.Sql.SQLQueryReturnProcessor", FieldName = "log"},
new {LoggerType = "NHibernate.Loader.Entity.AbstractEntityLoader", ClassName="NHibernate.Loader.Entity.AbstractEntityLoader", FieldName = "log"},
new {LoggerType = "NHibernate.Loader.Entity.CollectionElementLoader", ClassName="NHibernate.Loader.Entity.CollectionElementLoader", FieldName = "log"},
new {LoggerType = "NHibernate.Mapping.RootClass", ClassName="NHibernate.Mapping.RootClass", FieldName = "log"},
new {LoggerType = "NHibernate.Persister.Collection.ICollectionPersister", ClassName="NHibernate.Persister.Collection.AbstractCollectionPersister", FieldName = "log"},
new {LoggerType = "NHibernate.Persister.Collection.NamedQueryCollectionInitializer", ClassName="NHibernate.Persister.Collection.NamedQueryCollectionInitializer", FieldName = "log"},
new {LoggerType = "NHibernate.Persister.Entity.AbstractEntityPersister", ClassName="NHibernate.Persister.Entity.AbstractEntityPersister", FieldName = "log"},
new {LoggerType = "NHibernate.Persister.Entity.NamedQueryLoader", ClassName="NHibernate.Persister.Entity.NamedQueryLoader", FieldName = "log"},
new {LoggerType = "NHibernate.SqlCommand.InsertSelect", ClassName="NHibernate.SqlCommand.InsertSelect", FieldName = "log"},
new {LoggerType = "NHibernate.SqlCommand.SqlDeleteBuilder", ClassName="NHibernate.SqlCommand.SqlDeleteBuilder", FieldName = "log"},
new {LoggerType = "NHibernate.SqlCommand.SqlSelectBuilder", ClassName="NHibernate.SqlCommand.SqlSelectBuilder", FieldName = "log"},
new {LoggerType = "NHibernate.SqlCommand.SqlUpdateBuilder", ClassName="NHibernate.SqlCommand.SqlUpdateBuilder", FieldName = "log"},
new {LoggerType = "NHibernate.Stat.StatisticsImpl", ClassName="NHibernate.Stat.StatisticsImpl", FieldName = "log"},
new {LoggerType = "NHibernate.Tool.hbm2ddl.DatabaseMetadata", ClassName="NHibernate.Tool.hbm2ddl.DatabaseMetadata", FieldName = "log"},
new {LoggerType = "NHibernate.Tool.hbm2ddl.SchemaExport", ClassName="NHibernate.Tool.hbm2ddl.SchemaExport", FieldName = "log"},
new {LoggerType = "NHibernate.Tool.hbm2ddl.SchemaUpdate", ClassName="NHibernate.Tool.hbm2ddl.SchemaUpdate", FieldName = "log"},
new {LoggerType = "NHibernate.Tool.hbm2ddl.SchemaValidator", ClassName="NHibernate.Tool.hbm2ddl.SchemaValidator", FieldName = "log"},
new {LoggerType = "NHibernate.Impl.AbstractSessionImpl", ClassName="NHibernate.Transaction.AdoNetWithDistributedTransactionFactory", FieldName = "logger"},  // This is valid only for NHibernate before 7/29/2011
// new {LoggerType = "NHibernate.Transaction.ITransactionFactory", ClassName="NHibernate.Transaction.AdoNetWithDistributedTransactionFactory", FieldName = "logger"},  // Version:  3.3.3.4000 
new {LoggerType = "NHibernate.Transaction.ITransactionFactory", ClassName="NHibernate.Transaction.AdoNetTransactionFactory", FieldName = "isolaterLog"},  // Version:  3.3.3.4000 
new {LoggerType = "NHibernate.Transaction.AdoTransaction", ClassName="NHibernate.Transaction.AdoTransaction", FieldName = "log"},
new {LoggerType = "NHibernate.Transform.DistinctRootEntityResultTransformer", ClassName="NHibernate.Transform.DistinctRootEntityResultTransformer", FieldName = "log"},
new {LoggerType = "NHibernate.Tuple.Component.AbstractComponentTuplizer", ClassName="NHibernate.Tuple.Component.AbstractComponentTuplizer", FieldName = "log"},
new {LoggerType = "NHibernate.Tuple.Entity.AbstractEntityTuplizer", ClassName="NHibernate.Tuple.Entity.AbstractEntityTuplizer", FieldName = "log"},
new {LoggerType = "NHibernate.Tuple.Entity.PocoEntityTuplizer", ClassName="NHibernate.Tuple.Entity.DynamicMapEntityTuplizer", FieldName = "log"},
new {LoggerType = "NHibernate.Tuple.Entity.EntityMetamodel", ClassName="NHibernate.Tuple.Entity.EntityMetamodel", FieldName = "log"},
new {LoggerType = "NHibernate.Tuple.Entity.PocoEntityTuplizer", ClassName="NHibernate.Tuple.Entity.PocoEntityTuplizer", FieldName = "log"},
new {LoggerType = "NHibernate.Tuple.PocoInstantiator", ClassName="NHibernate.Tuple.PocoInstantiator", FieldName = "log"},
new {LoggerType = "NHibernate.Type.DbTimestampType", ClassName="NHibernate.Type.DbTimestampType", FieldName = "log"},
new {LoggerType = "NHibernate.Util.ADOExceptionReporter", ClassName="NHibernate.Util.ADOExceptionReporter", FieldName = "log"},
new {LoggerType = "NHibernate.Util.IdentityMap", ClassName="NHibernate.Util.IdentityMap", FieldName = "log"},
new {LoggerType = "NHibernate.Util.JoinedEnumerable", ClassName="NHibernate.Util.JoinedEnumerable", FieldName = "log"},
new {LoggerType = "NHibernate.Util.ReflectHelper", ClassName="NHibernate.Util.ReflectHelper", FieldName = "log"},

								  };

			foreach (var logger in loggers)
			{
				var typeHoldingLogger = nhibernate.GetType(logger.ClassName);
				if (typeHoldingLogger != null)
				{
					var field = typeHoldingLogger.GetField(logger.FieldName, BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.SetField);

					if (field == null)
					{
						var fieldNamesToTry = new List<string> { "log", "Log", "logger", "Logger" };
						fieldNamesToTry.Remove(logger.FieldName);

						foreach (var fieldName in fieldNamesToTry)
						{
							field = typeHoldingLogger.GetField(fieldName, BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.SetField);
							if (field != null)
								break;
						}
					}

					if (field != null)
						field.SetValue(null, LoggerProvider.LoggerFor(logger.LoggerType));
				}
			}
		}

		public static void Shutdown()
		{
			LoggerProvider.SetLoggersFactory(ProfilerLoggerFactory.OriginalLoggerFactory);

			SetLoggerOnStaticFields();
		}
	}
}