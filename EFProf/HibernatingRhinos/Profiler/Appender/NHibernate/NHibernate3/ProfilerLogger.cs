using System;
using HibernatingRhinos.Profiler.Appender.NHibernate;
using NHibernate;

namespace HibernatingRhinos.Profiler.Appender.NHibernate3Logger
{
    public class ProfilerLogger : IInternalLogger
    {
        private readonly bool logErrorsOnlyMode;
        private readonly IInternalLogger internalLogger;
        private readonly string logger;

        private static readonly SessionIdLoggingContext sessionIdLoggingContext = new SessionIdLoggingContext();
	    private readonly SessionConnectionStringLogger connectionStringLogger;

	    public static string SessionId
        {
            get
            {
                if (sessionIdLoggingContext.GetSessionId != null)
                {
                    var sessionId = sessionIdLoggingContext.GetSessionId();
                    if (sessionId != null)
                        return sessionId.Value.ToString();
                }
                return ProfilerIntegration.CurrentThreadId;
            }
        }

        public ProfilerLogger(IInternalLogger internalLogger, string logger, SessionConnectionStringLogger connectionStringLogger)
        {
            this.internalLogger = internalLogger ?? new NoLoggingInternalLogger();
            this.logger = logger;
	        this.connectionStringLogger = connectionStringLogger;
        }

        public ProfilerLogger(IInternalLogger internalLogger, string logger, bool logErrorsOnlyMode, SessionConnectionStringLogger connectionStringLogger)
            : this(internalLogger, logger, connectionStringLogger)
        {
            this.logErrorsOnlyMode = logErrorsOnlyMode;
        }

        #region Logger Implementation
        
        public void Error(object message)
        {
            internalLogger.Error(message);
            LogEvent(Level.Error, message);
        }

        public void Error(object message, Exception exception)
        {
            internalLogger.Error(message, exception);
            LogEvent(Level.Error, message, exception);
        }

        public void ErrorFormat(string format, params object[] args)
        {
            internalLogger.ErrorFormat(format, args);
            LogEventFormat(Level.Error, format, args);
        }

        public void Fatal(object message)
        {
            internalLogger.Fatal(message);
            LogEvent(Level.Error, message);
        }

        public void Fatal(object message, Exception exception)
        {
            internalLogger.Fatal(message, exception);
            LogEvent(Level.Error, message, exception);
        }

	    public void Debug(object message)
	    {
		    internalLogger.Debug(message);
		    LogEvent(Level.Debug, message);

			// Get connection string
		    if (logger == "NHibernate.Impl.SessionFactoryImpl")
		    {
			    var msg = message as string;
			    if (msg != null && msg == "Instantiated session factory")
			    {
					connectionStringLogger.PullConnectionStrings();
			    }
		    }
	    }

	    public void Debug(object message, Exception exception)
        {
            internalLogger.Debug(message, exception);
            LogEvent(Level.Debug, message, exception);
        }

        public void DebugFormat(string format, params object[] args)
        {
            internalLogger.DebugFormat(format, args);
            LogEventFormat(Level.Debug, format, args);
        }

        public void Info(object message)
        {
            internalLogger.Info(message);
            LogEvent(Level.Debug, message);
        }

        public void Info(object message, Exception exception)
        {
            internalLogger.Info(message, exception);
            LogEvent(Level.Debug, message, exception);
        }

        public void InfoFormat(string format, params object[] args)
        {
            internalLogger.InfoFormat(format, args);
            LogEventFormat(Level.Debug, format, args);
        }

        private void LogEventFormat(Level level, string format, object[] args)
        {
            string s;
            try
            {
                s = string.Format(format, args);
            }
            catch
            {
                return;
            }
            LogEvent(level, s);

        }

        public void Warn(object message)
        {
            internalLogger.Warn(message);
            LogEvent(Level.Warn, message);
        }

        public void Warn(object message, Exception exception)
        {
            internalLogger.Warn(message, exception);
            LogEvent(Level.Warn, message, exception);
        }

        public void WarnFormat(string format, params object[] args)
        {
            internalLogger.WarnFormat(format, args);
            LogEventFormat(Level.Warn, format, args);
        }

        public bool IsErrorEnabled
        {
            get { return true; }
        }

        public bool IsFatalEnabled
        {
            get { return true; }
        }

        public bool IsDebugEnabled
        {
            get { return logErrorsOnlyMode ? internalLogger.IsDebugEnabled : true; }
        }

        public bool IsInfoEnabled
        {
            get { return logErrorsOnlyMode ? internalLogger.IsInfoEnabled : true; }
        }

        public bool IsWarnEnabled
        {
            get { return true; }
        }

#endregion

        private void LogEvent(Level level, object message)
        {
            LogEvent(level, message, null);
        }

        private void LogEvent(Level level, object message, Exception exception)
        {
            if (ProfilerInfrastructure.IsInitialized == false)
                return;
            
            if (logErrorsOnlyMode && level == Level.Debug)
                return;

            string actualMessage = (exception != null ? exception.Message : null) +
                                   (message != null ? message.ToString() : string.Empty);

            ProfilerIntegration.PublishProfilerEvent(
                SessionId,
                logger,
                actualMessage,
                DateTime.Now,
                level
                );
        }
    }
}