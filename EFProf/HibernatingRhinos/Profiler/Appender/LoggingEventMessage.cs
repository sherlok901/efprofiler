﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Appender.LoggingEventMessage
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using HibernatingRhinos.Profiler.Appender.StackTraces;
using System;

namespace HibernatingRhinos.Profiler.Appender
{
  [Serializable]
  public class LoggingEventMessage
  {
    public Exception Exception { get; set; }

    public string ExceptionString
    {
      get
      {
        if (this.Exception == null)
          return (string) null;
        return this.Exception.ToString();
      }
    }

    public string Message { get; set; }

    public string Logger { get; set; }

    public int Level { get; set; }

    public DateTime Date { get; set; }

    public string ThreadId { get; set; }

    public string ScopeName { get; set; }

    public string SessionId { get; set; }

    public string Url { get; set; }

    public string ThreadContext { get; set; }

    public StackTraceInfo StackTrace { get; set; }

    public string DbDialect { get; set; }

    public string Culture { get; set; }

    public string StarColor { get; set; }

    public override string ToString()
    {
      return this.Logger + ": " + this.Message;
    }
  }
}
