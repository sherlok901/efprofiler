﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Appender.Channels.StreamProfilerChannel
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers;
using HibernatingRhinos.Profiler.Appender.Messages;
using System;
using System.Collections.Generic;
using System.IO;

namespace HibernatingRhinos.Profiler.Appender.Channels
{
  public abstract class StreamProfilerChannel : IProfilerChannel, IDisposable
  {
    protected readonly object locker = new object();
    protected Stream stream;
    protected MessageStreamWriter<MessageWrapper> writer;

    public void InitializeChannelToProfiler()
    {
      lock (this.locker)
      {
        if (this.stream != null)
        {
          this.stream.Dispose();
          this.stream = (Stream) null;
        }
        this.CloseChannel();
        this.OpenChannel();
        if (this.stream == null)
          return;
        this.writer = new MessageStreamWriter<MessageWrapper>(this.stream);
        this.Send(ApplicationInformationProvider.Id, ApplicationInformationProvider.Name);
      }
    }

    public abstract bool ShouldSendDataToProfiler { get; }

    public bool FinishedToFlush
    {
      get
      {
        return true;
      }
    }

    protected abstract void OpenChannel();

    protected abstract void CloseChannel();

    public void Send(ICollection<HibernatingRhinos.Profiler.Appender.LoggingEventMessage> msgs)
    {
      List<MessageWrapper> messageWrapperList = new List<MessageWrapper>(msgs.Count);
      foreach (HibernatingRhinos.Profiler.Appender.LoggingEventMessage msg in (IEnumerable<HibernatingRhinos.Profiler.Appender.LoggingEventMessage>) msgs)
        messageWrapperList.Add(ProfilerChannelHelpers.Parse(msg));
      this.WriteToProfilerWithRetries((IEnumerable<MessageWrapper>) messageWrapperList, 3);
    }

    public void Send(Guid applicationId, string applicationName)
    {
      this.WriteToProfilerWithRetries((IEnumerable<MessageWrapper>) new List<MessageWrapper>()
      {
        ProfilerChannelHelpers.Parse(applicationId, applicationName)
      }, 3);
    }

    public void Send(SessionFactoryStats[] statistics)
    {
      List<MessageWrapper> messageWrapperList = new List<MessageWrapper>();
      foreach (SessionFactoryStats statistic in statistics)
        messageWrapperList.Add(ProfilerChannelHelpers.Parse(statistic));
      this.WriteToProfilerWithRetries((IEnumerable<MessageWrapper>) messageWrapperList, 3);
    }

    private void WriteToProfilerWithRetries(IEnumerable<MessageWrapper> wrappers, int retryCount)
    {
      try
      {
        lock (this.locker)
        {
          if (this.writer == null || this.stream == null)
            return;
          foreach (MessageWrapper wrapper in wrappers)
            this.writer.Write(wrapper);
          this.writer.Flush();
          this.stream.Flush();
        }
      }
      catch (Exception ex)
      {
        if (retryCount == 0)
        {
          throw;
        }
        else
        {
          this.InitializeChannelToProfiler();
          this.WriteToProfilerWithRetries(wrappers, retryCount - 1);
        }
      }
    }

    public void Dispose()
    {
      if (this.stream != null)
        this.stream.Dispose();
      this.CloseChannel();
    }
  }
}
