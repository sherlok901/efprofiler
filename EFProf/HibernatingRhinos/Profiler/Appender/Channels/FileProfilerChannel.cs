﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Appender.Channels.FileProfilerChannel
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using System.IO;

namespace HibernatingRhinos.Profiler.Appender.Channels
{
  public class FileProfilerChannel : StreamProfilerChannel
  {
    private readonly string filename;

    public FileProfilerChannel(string filename)
    {
      this.filename = filename;
    }

    public override bool ShouldSendDataToProfiler
    {
      get
      {
        return true;
      }
    }

    protected override void OpenChannel()
    {
      this.stream = (Stream) File.OpenWrite(this.filename);
    }

    protected override void CloseChannel()
    {
    }
  }
}
