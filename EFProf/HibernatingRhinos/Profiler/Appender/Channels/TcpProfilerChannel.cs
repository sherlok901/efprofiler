﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Appender.Channels.TcpProfilerChannel
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using System;
using System.IO;
using System.Net.Sockets;

namespace HibernatingRhinos.Profiler.Appender.Channels
{
  public class TcpProfilerChannel : StreamProfilerChannel
  {
    protected DateTime lastConnectionAttempt = DateTime.MinValue;
    protected TcpClient client;
    private DateTime? firstTimeFailedListenOnPortAt;

    public string Host { get; set; }

    public int Port { get; set; }

    public override bool ShouldSendDataToProfiler
    {
      get
      {
        if ((this.client == null || !this.client.Connected) && (DateTime.Now - this.lastConnectionAttempt).TotalSeconds > 15.0)
        {
          this.lastConnectionAttempt = DateTime.Now;
          this.InitializeChannelToProfiler();
        }
        if (this.client != null)
          return this.client.Connected;
        return false;
      }
    }

    protected override void OpenChannel()
    {
      try
      {
        this.client = new TcpClient();
        this.client.Connect(this.Host, this.Port);
        this.stream = (Stream) this.client.GetStream();
      }
      catch (SocketException ex)
      {
        if (!ProfilerInfrastructure.ThrowOnConnectionErrors)
          return;
        if (!this.firstTimeFailedListenOnPortAt.HasValue)
        {
          this.firstTimeFailedListenOnPortAt = new DateTime?(DateTime.Now);
        }
        else
        {
          DateTime now = DateTime.Now;
          DateTime? failedListenOnPortAt = this.firstTimeFailedListenOnPortAt;
          TimeSpan? nullable = failedListenOnPortAt.HasValue ? new TimeSpan?(now - failedListenOnPortAt.GetValueOrDefault()) : new TimeSpan?();
          TimeSpan timeSpan = TimeSpan.FromSeconds(30.0);
          if ((nullable.HasValue ? (nullable.GetValueOrDefault() > timeSpan ? 1 : 0) : 0) != 0)
            throw new InvalidOperationException(string.Format("Could not connect the profiler backend. Please make sure that the profiler is running and listening to the '{0}' port.", (object) this.Port), (Exception) ex);
        }
      }
      catch (Exception ex)
      {
        if (!ProfilerInfrastructure.ThrowOnConnectionErrors)
          return;
        throw;
      }
    }

    protected override void CloseChannel()
    {
      if (this.client == null)
        return;
      if (this.client.Client != null)
        this.client.Client.Close();
      this.client.Close();
      this.client = (TcpClient) null;
    }
  }
}
