﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Appender.Channels.CompositeStream
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using System;
using System.Collections.Generic;
using System.IO;

namespace HibernatingRhinos.Profiler.Appender.Channels
{
  public class CompositeStream : Stream
  {
    private List<CompositeStream.StreamAndDisposer> streams = new List<CompositeStream.StreamAndDisposer>();

    public event ProfilerAction OnRemoval = () => {};

    public void Add(Stream stream, ProfilerAction disposer)
    {
      this.streams = new List<CompositeStream.StreamAndDisposer>((IEnumerable<CompositeStream.StreamAndDisposer>) this.streams)
      {
        new CompositeStream.StreamAndDisposer()
        {
          Disposer = disposer,
          Stream = stream
        }
      };
    }

    private void Remove(CompositeStream.StreamAndDisposer stream)
    {
      this.streams = new List<CompositeStream.StreamAndDisposer>((IEnumerable<CompositeStream.StreamAndDisposer>) this.streams.FindAll((Predicate<CompositeStream.StreamAndDisposer>) (s => s != stream)));
      stream.Disposer();
      this.OnRemoval();
    }

    public override void Flush()
    {
      this.streams.ForEach((Action<CompositeStream.StreamAndDisposer>) (x =>
      {
        try
        {
          x.Stream.Flush();
        }
        catch (Exception ex)
        {
          this.Remove(x);
          throw;
        }
      }));
    }

    public override long Seek(long offset, SeekOrigin origin)
    {
      throw new NotSupportedException();
    }

    public override void SetLength(long value)
    {
      throw new NotSupportedException();
    }

    public override int Read(byte[] buffer, int offset, int count)
    {
      throw new NotSupportedException();
    }

    public override void Write(byte[] buffer, int offset, int count)
    {
      this.streams.ForEach((Action<CompositeStream.StreamAndDisposer>) (x =>
      {
        try
        {
          x.Stream.Write(buffer, offset, count);
        }
        catch (Exception ex)
        {
          this.Remove(x);
          throw;
        }
      }));
    }

    public override bool CanRead
    {
      get
      {
        return false;
      }
    }

    public override bool CanSeek
    {
      get
      {
        return false;
      }
    }

    public override bool CanWrite
    {
      get
      {
        return true;
      }
    }

    public override long Length
    {
      get
      {
        throw new NotSupportedException();
      }
    }

    public override long Position
    {
      get
      {
        throw new NotSupportedException();
      }
      set
      {
        throw new NotSupportedException();
      }
    }

    public bool HasStreams
    {
      get
      {
        return this.streams.Count > 0;
      }
    }

    public class StreamAndDisposer
    {
      public Stream Stream;
      public ProfilerAction Disposer;
    }
  }
}
