﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Appender.Channels.TcpListenerProfilerChannel
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers;
using HibernatingRhinos.Profiler.Appender.Channels.SSL;
using HibernatingRhinos.Profiler.Appender.Messages;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;

namespace HibernatingRhinos.Profiler.Appender.Channels
{
  public class TcpListenerProfilerChannel : IProfilerChannel, IDisposable
  {
    private bool doWork = true;
    private SslStream[] connectedClients = new SslStream[0];
    private readonly Queue<MessageWrapper> sendQueue = new Queue<MessageWrapper>();
    private TcpListener tcpListener;
    private readonly Thread sendToClients;
    private int queuedCount;

    public int Port { get; set; }

    public string ProductionPassword { get; set; }

    public int Capacity { get; set; }

    public bool WaitForFirstClientToConnect { get; set; }

    public bool FinishedToFlush
    {
      get
      {
        if (this.sendQueue.Count != 0 || this.queuedCount != 0)
          return !this.doWork;
        return true;
      }
    }

    public TcpListenerProfilerChannel()
    {
      this.sendToClients = new Thread(new ThreadStart(this.SendToClientWork))
      {
        IsBackground = true,
        Name = "Send profiling data to clients",
        Priority = ThreadPriority.BelowNormal
      };
      this.sendToClients.Start();
      this.Capacity = 32768;
    }

    private void SendToClientWork()
    {
      while (this.doWork)
      {
        List<MessageWrapper> wrappers = new List<MessageWrapper>();
        lock (this.sendQueue)
        {
          while (this.sendQueue.Count == 0 && this.doWork)
            Monitor.Wait((object) this.sendQueue);
          Interlocked.Increment(ref this.queuedCount);
          while (this.sendQueue.Count > 0)
          {
            if (this.doWork)
              wrappers.Add(this.sendQueue.Dequeue());
            else
              break;
          }
        }
        if (!this.doWork)
        {
          Interlocked.Decrement(ref this.queuedCount);
          break;
        }
        foreach (Stream connectedClient in this.connectedClients)
          this.WriteToProfiler(connectedClient, wrappers);
        Interlocked.Decrement(ref this.queuedCount);
      }
    }

    public void Dispose()
    {
      lock (this.sendQueue)
      {
        this.doWork = false;
        Monitor.Pulse((object) this.sendQueue);
      }
      this.sendToClients.Join();
      foreach (SslStream connectedClient in this.connectedClients)
      {
        try
        {
          connectedClient.Dispose();
        }
        catch (Exception ex)
        {
        }
      }
      this.tcpListener.Server.Close();
      this.tcpListener.Stop();
    }

    public void InitializeChannelToProfiler()
    {
      this.tcpListener = new TcpListener(IPAddress.Any, this.Port);
      this.tcpListener.Start();
      this.ListenForIncomingConnection();
    }

    private void ListenForIncomingConnection()
    {
      try
      {
        this.tcpListener.BeginAcceptTcpClient(new AsyncCallback(this.ConnectionEstablished), (object) null);
      }
      catch (Exception ex)
      {
        if (!ProfilerInfrastructure.ThrowOnConnectionErrors)
          return;
        throw;
      }
    }

    private void ConnectionEstablished(IAsyncResult ar)
    {
      if (!this.doWork)
        return;
      TcpClient tcpClient;
      try
      {
        tcpClient = this.tcpListener.EndAcceptTcpClient(ar);
      }
      catch (ObjectDisposedException ex)
      {
        this.ListenForIncomingConnection();
        return;
      }
      catch (Exception ex)
      {
        if (ProfilerInfrastructure.ThrowOnConnectionErrors)
        {
          throw;
        }
        else
        {
          this.ListenForIncomingConnection();
          return;
        }
      }
      NetworkStream stream = tcpClient.GetStream();
      SslStream sslStream = new SslStream((Stream) stream, false, new RemoteCertificateValidationCallback(TcpListenerProfilerChannel.UserCertificateValidationCallback));
      try
      {
        sslStream.AuthenticateAsServer(CertificateHolder.Certificate, true, SslProtocols.Default, true);
      }
      catch (Exception ex)
      {
        if (ProfilerInfrastructure.ThrowOnConnectionErrors)
        {
          throw;
        }
        else
        {
          this.ListenForIncomingConnection();
          return;
        }
      }
      string str = new BinaryReader((Stream) sslStream, Encoding.UTF8).ReadString();
      BinaryWriter binaryWriter = new BinaryWriter((Stream) sslStream, Encoding.UTF8);
      if (str == this.ProductionPassword)
      {
        binaryWriter.Write("Connection: OK.");
        this.connectedClients = ((IEnumerable<SslStream>) this.connectedClients).Concat<SslStream>((IEnumerable<SslStream>) new SslStream[1]
        {
          sslStream
        }).ToArray<SslStream>();
        this.Send(ApplicationInformationProvider.Id, ApplicationInformationProvider.Name);
        this.ListenForIncomingConnection();
      }
      else
      {
        binaryWriter.Write("Connection: Error, the password is not correct.");
        sslStream.Dispose();
        stream.Dispose();
        this.ListenForIncomingConnection();
      }
    }

    public static bool UserCertificateValidationCallback(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
    {
      if (ProfilerInfrastructure.ThrowOnConnectionErrors)
        Console.WriteLine("Profiler logger: validation of policy error: " + (object) sslPolicyErrors);
      switch (sslPolicyErrors)
      {
        case SslPolicyErrors.None:
        case SslPolicyErrors.RemoteCertificateNotAvailable:
        case SslPolicyErrors.RemoteCertificateChainErrors:
          return true;
        default:
          if (ProfilerInfrastructure.ThrowOnConnectionErrors)
            Console.WriteLine("Profiler logger: validation failed because of policy error: " + (object) sslPolicyErrors);
          return false;
      }
    }

    public bool ShouldSendDataToProfiler
    {
      get
      {
        if (!this.WaitForFirstClientToConnect)
          return ((IEnumerable<SslStream>) this.connectedClients).Any<SslStream>();
        return true;
      }
    }

    public void Send(ICollection<HibernatingRhinos.Profiler.Appender.LoggingEventMessage> msgs)
    {
      this.AddToQueue(msgs.Select<HibernatingRhinos.Profiler.Appender.LoggingEventMessage, MessageWrapper>(new System.Func<LoggingEventMessage, MessageWrapper>(ProfilerChannelHelpers.Parse)));
    }

    private void AddToQueue(IEnumerable<MessageWrapper> msgs)
    {
      lock (this.sendQueue)
      {
        foreach (MessageWrapper msg in msgs)
          this.sendQueue.Enqueue(msg);
        while (this.sendQueue.Count > this.Capacity)
          this.sendQueue.Dequeue();
        Monitor.Pulse((object) this.sendQueue);
      }
    }

    public void Send(Guid applicationId, string applicationName)
    {
      this.AddToQueue((IEnumerable<MessageWrapper>) new MessageWrapper[1]
      {
        ProfilerChannelHelpers.Parse(applicationId, applicationName)
      });
    }

    public void Send(SessionFactoryStats[] statistics)
    {
      this.AddToQueue(((IEnumerable<SessionFactoryStats>) statistics).Select<SessionFactoryStats, MessageWrapper>(new System.Func<SessionFactoryStats, MessageWrapper>(ProfilerChannelHelpers.Parse)));
    }

    private void WriteToProfiler(Stream stream, List<MessageWrapper> wrappers)
    {
      try
      {
        MessageStreamWriter<MessageWrapper> messageStreamWriter = new MessageStreamWriter<MessageWrapper>(stream);
        foreach (MessageWrapper wrapper in wrappers)
          messageStreamWriter.Write(wrapper);
        messageStreamWriter.Flush();
        stream.Flush();
      }
      catch (Exception ex)
      {
        this.connectedClients = ((IEnumerable<SslStream>) this.connectedClients).Where<SslStream>((System.Func<SslStream, bool>) (x => x != stream)).ToArray<SslStream>();
        stream.Dispose();
      }
    }
  }
}
