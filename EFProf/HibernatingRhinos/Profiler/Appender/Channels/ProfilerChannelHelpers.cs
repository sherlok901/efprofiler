﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Appender.Channels.ProfilerChannelHelpers
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using HibernatingRhinos.Profiler.Appender.Messages;
using System;
using System.Collections;

namespace HibernatingRhinos.Profiler.Appender.Channels
{
  public abstract class ProfilerChannelHelpers
  {
    public static readonly DateTime UnixEpoch = new DateTime(1970, 1, 1);

    public static HibernatingRhinos.Profiler.Appender.Messages.LoggingEventMessage.Types.StackTraceInfo ConvertStackTrace(HibernatingRhinos.Profiler.Appender.LoggingEventMessage msg)
    {
      if (msg.StackTrace == null)
        return HibernatingRhinos.Profiler.Appender.Messages.LoggingEventMessage.Types.StackTraceInfo.DefaultInstance;
      HibernatingRhinos.Profiler.Appender.StackTraces.StackTraceFrame[] frames = msg.StackTrace.Frames;
      HibernatingRhinos.Profiler.Appender.Messages.LoggingEventMessage.Types.StackTraceInfo.Builder builder1 = new HibernatingRhinos.Profiler.Appender.Messages.LoggingEventMessage.Types.StackTraceInfo.Builder();
      for (int index = 0; index < frames.Length; ++index)
      {
        HibernatingRhinos.Profiler.Appender.Messages.LoggingEventMessage.Types.StackTraceFrame.Builder builder2 = new HibernatingRhinos.Profiler.Appender.Messages.LoggingEventMessage.Types.StackTraceFrame.Builder()
        {
          Column = frames[index].Column,
          Line = frames[index].Line,
          Method = frames[index].Method,
          Namespace = frames[index].Namespace,
          Type = frames[index].Type
        };
        if (frames[index].FullFilename != null)
          builder2.FullFilename = frames[index].FullFilename;
        builder1.FramesList.Add(builder2.Build());
      }
      return builder1.Build();
    }

    public static MessageWrapper Parse(HibernatingRhinos.Profiler.Appender.LoggingEventMessage msg)
    {
      return new MessageWrapper.Builder()
      {
        MessageId = Guid.NewGuid().ToString(),
        Type = MessageWrapper.Types.MessageType.LogEvent,
        Message = new HibernatingRhinos.Profiler.Appender.Messages.LoggingEventMessage.Builder()
        {
          DateAsMillisecondsSinceUnixEpoch = Convert.ToInt64((msg.Date - ProfilerChannelHelpers.UnixEpoch).TotalMilliseconds),
          Logger = msg.Logger,
          Message = msg.Message,
          Level = msg.Level,
          ThreadId = msg.ThreadId,
          SessionId = msg.SessionId,
          StackTrace = ProfilerChannelHelpers.ConvertStackTrace(msg),
          ThreadContext = (msg.ThreadContext ?? ""),
          Url = (msg.Url ?? string.Empty),
          Exception = (msg.ExceptionString ?? string.Empty),
          DbDialect = (msg.DbDialect ?? string.Empty),
          Culture = (msg.Culture ?? string.Empty),
          StarColor = (msg.StarColor ?? string.Empty),
          ScopeName = (msg.ScopeName ?? string.Empty)
        }.Build()
      }.Build();
    }

    public static MessageWrapper Parse(Guid applicationId, string applicationName)
    {
      return new MessageWrapper.Builder()
      {
        MessageId = Guid.NewGuid().ToString(),
        Type = MessageWrapper.Types.MessageType.ApplicationAttached,
        Application = new ApplicationInformation.Builder()
        {
          Id = applicationId.ToString(),
          Name = applicationName
        }.Build()
      }.Build();
    }

    public static MessageWrapper Parse(SessionFactoryStats stats)
    {
      StatisticsInformation.Builder information = new StatisticsInformation.Builder()
      {
        Name = stats.Name
      };
      ProfilerChannelHelpers.AddToDictionary(information, (IDictionary) stats.Statistics);
      return new MessageWrapper.Builder()
      {
        MessageId = Guid.NewGuid().ToString(),
        Type = MessageWrapper.Types.MessageType.Statistics,
        Stats = information.Build()
      }.Build();
    }

    private static void AddToDictionary(StatisticsInformation.Builder information, IDictionary dictionary)
    {
      foreach (DictionaryEntry dictionaryEntry in dictionary)
      {
        if (dictionaryEntry.Value != null)
        {
          StatisticsInformation.Types.KeyValue.Builder builder = new StatisticsInformation.Types.KeyValue.Builder()
          {
            Key = dictionaryEntry.Key.ToString()
          };
          if (!(dictionaryEntry.Value is string) && dictionaryEntry.Value is IEnumerable)
          {
            foreach (object obj in (IEnumerable) dictionaryEntry.Value)
              builder.ValueList.Add(obj.ToString());
          }
          else
            builder.ValueList.Add(dictionaryEntry.Value.ToString());
          information.ItemsList.Add(builder.Build());
        }
      }
    }
  }
}
