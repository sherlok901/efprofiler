﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Appender.Channels.SSL.CertificateHolder
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using System.IO;
using System.Security.Cryptography.X509Certificates;

namespace HibernatingRhinos.Profiler.Appender.Channels.SSL
{
  public class CertificateHolder
  {
    private static X509Certificate certificate;

    public static X509Certificate Certificate
    {
      get
      {
        return CertificateHolder.certificate ?? (CertificateHolder.certificate = CertificateHolder.Load());
      }
      set
      {
        CertificateHolder.certificate = value;
      }
    }

    private static X509Certificate Load()
    {
      using (Stream manifestResourceStream = typeof (CertificateHolder).Assembly.GetManifestResourceStream(typeof (CertificateHolder), "PFXFileContainingPrivateKey.pfx"))
      {
        using (MemoryStream memoryStream = new MemoryStream())
        {
          using (BinaryReader binaryReader = new BinaryReader(manifestResourceStream))
          {
            byte[] buffer = new byte[4096];
            int count;
            while ((count = binaryReader.Read(buffer, 0, buffer.Length)) != 0)
              memoryStream.Write(buffer, 0, count);
            return (X509Certificate) new X509Certificate2(memoryStream.ToArray(), "123456");
          }
        }
      }
    }
  }
}
