﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Appender.Channels.NullProfilerChannel
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using System;
using System.Collections.Generic;

namespace HibernatingRhinos.Profiler.Appender.Channels
{
  public class NullProfilerChannel : IProfilerChannel, IDisposable
  {
    public void Dispose()
    {
    }

    public void InitializeChannelToProfiler()
    {
    }

    public bool ShouldSendDataToProfiler
    {
      get
      {
        return false;
      }
    }

    public bool FinishedToFlush
    {
      get
      {
        return true;
      }
    }

    public void Send(ICollection<LoggingEventMessage> msgs)
    {
    }

    public void Send(SessionFactoryStats[] statistics)
    {
    }
  }
}
