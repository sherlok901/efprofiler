﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Appender.CustomQueryReporting
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;

namespace HibernatingRhinos.Profiler.Appender
{
  public static class CustomQueryReporting
  {
    public static void ReportSessionOpened(string sessionId, string url = null)
    {
      if (url != null)
        ProfilerIntegration.CurrentSessionContext = url;
      ProfilerIntegration.PublishProfilerEvent(sessionId, "HibernatingRhinos.Profiler.Appender.CustomReporting.Session", "Session opened");
    }

    public static void ReportSessionClosed(string sessionId)
    {
      ProfilerIntegration.PublishProfilerEvent(sessionId, "HibernatingRhinos.Profiler.Appender.CustomReporting.Session", "Session closed");
      ProfilerIntegration.CurrentSessionContext = (string) null;
    }

    public static void ReportTransactionBeginning(string sessionId)
    {
      CustomQueryReporting.ReportTransactionBeginning(sessionId, IsolationLevel.Unspecified);
    }

    public static void ReportTransactionBeginning(string sessionId, IsolationLevel isolationLevel)
    {
      ProfilerIntegration.PublishProfilerEvent(sessionId, "HibernatingRhinos.Profiler.Appender.CustomReporting.Transactions", "Transaction began: " + (object) isolationLevel);
    }

    public static void ReportTransactionCommitted(string sessionId)
    {
      ProfilerIntegration.PublishProfilerEvent(sessionId, "HibernatingRhinos.Profiler.Appender.CustomReporting.Transactions", "Transaction committed");
    }

    public static void ReportTransactionRolledBack(string sessionId)
    {
      ProfilerIntegration.PublishProfilerEvent(sessionId, "HibernatingRhinos.Profiler.Appender.CustomReporting.Transactions", "Transaction rollbacked");
    }

    public static void ReportError(string sessionId, string error)
    {
      CustomQueryReporting.ReportError(sessionId, error, Level.Error);
    }

    public static void ReportError(string sessionId, Exception error)
    {
      CustomQueryReporting.ReportError(sessionId, error.ToString(), Level.Error);
    }

    public static void ReportError(string sessionId, string error, Level level)
    {
      ProfilerIntegration.PublishProfilerEvent(sessionId, "HibernatingRhinos.Profiler.Appender.CustomReporting.Errors", error, DateTime.Now, level);
    }

    public static void ReportWriteQuery(string sessionId, string queryText, IEnumerable<IDataParameter> parameters, int totalTime)
    {
      StringBuilder stringBuilder = new StringBuilder(queryText).AppendLine();
      if (parameters != null)
      {
        stringBuilder.AppendLine("-- Parameters:");
        foreach (IDbDataParameter parameter in parameters)
        {
          string str = parameter.ParameterName;
          if (!str.StartsWith("@") && !str.StartsWith(":") && !str.StartsWith("?"))
            str = "@" + str;
          stringBuilder.Append("-- ").Append(str).Append(" = [-[").Append(CustomQueryReporting.GetParameterValue((IDataParameter) parameter)).Append("]-] [-[").Append(CustomQueryReporting.GetDbType(parameter)).Append(" (").Append((object) parameter.Size).AppendLine(")]-]");
        }
      }
      stringBuilder.Append("-- total time: ").AppendLine(totalTime.ToString());
      ProfilerIntegration.PublishProfilerEvent(sessionId, "HibernatingRhinos.Profiler.Appender.CustomReporting", stringBuilder.ToString());
    }

    public static void ReportQuery(string sessionId, string queryText, IEnumerable<IDataParameter> parameters, int databaseTime, int totalTime, IEnumerable<int> countOfRows)
    {
      StringBuilder stringBuilder = new StringBuilder(queryText).AppendLine();
      if (parameters != null)
      {
        stringBuilder.AppendLine("-- Parameters:");
        foreach (IDbDataParameter parameter in parameters)
        {
          string str = parameter.ParameterName;
          if (!str.StartsWith("@") && !str.StartsWith(":") && !str.StartsWith("?"))
            str = "@" + str;
          stringBuilder.Append("-- ").Append(str).Append(" = [-[").Append(CustomQueryReporting.GetParameterValue((IDataParameter) parameter)).Append("]-] [-[").Append(CustomQueryReporting.GetDbType(parameter)).Append(" (").Append((object) parameter.Size).AppendLine(")]-]");
        }
      }
      stringBuilder.Append("-- db time: ").AppendLine(databaseTime.ToString()).Append("-- total time: ").AppendLine(totalTime.ToString());
      if (countOfRows != null)
      {
        foreach (int countOfRow in countOfRows)
          stringBuilder.Append("-- row count: ").AppendLine(countOfRow.ToString());
      }
      ProfilerIntegration.PublishProfilerEvent(sessionId, "HibernatingRhinos.Profiler.Appender.CustomReporting", stringBuilder.ToString());
    }

    public static void ReportQuery(string sessionId, string queryText, IEnumerable<IDataParameter> parameters, int databaseTime, int totalTime, int rowCount)
    {
      CustomQueryReporting.ReportQuery(sessionId, queryText, parameters, databaseTime, totalTime, (IEnumerable<int>) new int[1]
      {
        rowCount
      });
    }

    private static string GetDbType(IDbDataParameter parameter)
    {
      SqlParameter sqlParameter = parameter as SqlParameter;
      if (sqlParameter != null)
        return sqlParameter.SqlDbType.ToString();
      return parameter.DbType.ToString();
    }

    private static object GetParameterValue(IDataParameter parameter)
    {
      if (parameter.Value == DBNull.Value)
        return (object) "NULL";
      byte[] numArray = parameter.Value as byte[];
      if (numArray != null)
      {
        StringBuilder stringBuilder = new StringBuilder("0x");
        foreach (byte num in numArray)
          stringBuilder.Append(num.ToString("X2"));
        return (object) stringBuilder.ToString();
      }
      DataTable dataTable = parameter.Value as DataTable;
      if (dataTable == null)
        return parameter.Value;
      string tableName = dataTable.TableName;
      if (string.IsNullOrEmpty(tableName))
        dataTable.TableName = "TableName";
      StringWriter stringWriter = new StringWriter();
      dataTable.WriteXml((TextWriter) stringWriter, XmlWriteMode.WriteSchema);
      dataTable.TableName = tableName;
      return (object) stringWriter.GetStringBuilder();
    }
  }
}
