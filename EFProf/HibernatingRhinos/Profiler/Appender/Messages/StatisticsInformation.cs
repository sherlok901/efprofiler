﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Appender.Messages.StatisticsInformation
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers;
using Google.ProtocolBuffers.Collections;
using Google.ProtocolBuffers.Descriptors;
using Google.ProtocolBuffers.FieldAccess;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace HibernatingRhinos.Profiler.Appender.Messages
{
  [DebuggerNonUserCode]
  public sealed class StatisticsInformation : GeneratedMessage<StatisticsInformation, StatisticsInformation.Builder>
  {
    private static readonly StatisticsInformation defaultInstance = new StatisticsInformation().MakeReadOnly();
    private static readonly string[] _statisticsInformationFieldNames = new string[2]
    {
      "Items",
      nameof (Name)
    };
    private static readonly uint[] _statisticsInformationFieldTags = new uint[2]
    {
      18U,
      10U
    };
    private string name_ = "";
    private PopsicleList<StatisticsInformation.Types.KeyValue> items_ = new PopsicleList<StatisticsInformation.Types.KeyValue>();
    private int memoizedSerializedSize = -1;
    public const int NameFieldNumber = 1;
    public const int ItemsFieldNumber = 2;
    private bool hasName;

    private StatisticsInformation()
    {
    }

    public static StatisticsInformation DefaultInstance
    {
      get
      {
        return StatisticsInformation.defaultInstance;
      }
    }

    public override StatisticsInformation DefaultInstanceForType
    {
      get
      {
        return StatisticsInformation.DefaultInstance;
      }
    }

    protected override StatisticsInformation ThisMessage
    {
      get
      {
        return this;
      }
    }

    public static MessageDescriptor Descriptor
    {
      get
      {
        return HibernatingRhinosProfilerMessages.internal__static_HibernatingRhinos_Profiler_Appender_Messages_StatisticsInformation__Descriptor;
      }
    }

    protected override FieldAccessorTable<StatisticsInformation, StatisticsInformation.Builder> InternalFieldAccessors
    {
      get
      {
        return HibernatingRhinosProfilerMessages.internal__static_HibernatingRhinos_Profiler_Appender_Messages_StatisticsInformation__FieldAccessorTable;
      }
    }

    public bool HasName
    {
      get
      {
        return this.hasName;
      }
    }

    public string Name
    {
      get
      {
        return this.name_;
      }
    }

    public IList<StatisticsInformation.Types.KeyValue> ItemsList
    {
      get
      {
        return (IList<StatisticsInformation.Types.KeyValue>) this.items_;
      }
    }

    public int ItemsCount
    {
      get
      {
        return this.items_.Count;
      }
    }

    public StatisticsInformation.Types.KeyValue GetItems(int index)
    {
      return this.items_[index];
    }

    public override bool IsInitialized
    {
      get
      {
        if (!this.hasName)
          return false;
        foreach (AbstractMessageLite<StatisticsInformation.Types.KeyValue, StatisticsInformation.Types.KeyValue.Builder> items in (IEnumerable<StatisticsInformation.Types.KeyValue>) this.ItemsList)
        {
          if (!items.IsInitialized)
            return false;
        }
        return true;
      }
    }

    public override void WriteTo(ICodedOutputStream output)
    {
      int serializedSize = this.SerializedSize;
      string[] informationFieldNames = StatisticsInformation._statisticsInformationFieldNames;
      if (this.hasName)
        output.WriteString(1, informationFieldNames[1], this.Name);
      if (this.items_.Count > 0)
        output.WriteMessageArray<StatisticsInformation.Types.KeyValue>(2, informationFieldNames[0], (IEnumerable<StatisticsInformation.Types.KeyValue>) this.items_);
      this.UnknownFields.WriteTo(output);
    }

    public override int SerializedSize
    {
      get
      {
        int memoizedSerializedSize = this.memoizedSerializedSize;
        if (memoizedSerializedSize != -1)
          return memoizedSerializedSize;
        int num1 = 0;
        if (this.hasName)
          num1 += CodedOutputStream.ComputeStringSize(1, this.Name);
        foreach (StatisticsInformation.Types.KeyValue items in (IEnumerable<StatisticsInformation.Types.KeyValue>) this.ItemsList)
          num1 += CodedOutputStream.ComputeMessageSize(2, (IMessageLite) items);
        int num2 = num1 + this.UnknownFields.SerializedSize;
        this.memoizedSerializedSize = num2;
        return num2;
      }
    }

    public static StatisticsInformation ParseFrom(ByteString data)
    {
      return StatisticsInformation.CreateBuilder().MergeFrom(data).BuildParsed();
    }

    public static StatisticsInformation ParseFrom(ByteString data, ExtensionRegistry extensionRegistry)
    {
      return StatisticsInformation.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
    }

    public static StatisticsInformation ParseFrom(byte[] data)
    {
      return StatisticsInformation.CreateBuilder().MergeFrom(data).BuildParsed();
    }

    public static StatisticsInformation ParseFrom(byte[] data, ExtensionRegistry extensionRegistry)
    {
      return StatisticsInformation.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
    }

    public static StatisticsInformation ParseFrom(Stream input)
    {
      return StatisticsInformation.CreateBuilder().MergeFrom(input).BuildParsed();
    }

    public static StatisticsInformation ParseFrom(Stream input, ExtensionRegistry extensionRegistry)
    {
      return StatisticsInformation.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
    }

    public static StatisticsInformation ParseDelimitedFrom(Stream input)
    {
      return StatisticsInformation.CreateBuilder().MergeDelimitedFrom(input).BuildParsed();
    }

    public static StatisticsInformation ParseDelimitedFrom(Stream input, ExtensionRegistry extensionRegistry)
    {
      return StatisticsInformation.CreateBuilder().MergeDelimitedFrom(input, extensionRegistry).BuildParsed();
    }

    public static StatisticsInformation ParseFrom(ICodedInputStream input)
    {
      return StatisticsInformation.CreateBuilder().MergeFrom(input).BuildParsed();
    }

    public static StatisticsInformation ParseFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
    {
      return StatisticsInformation.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
    }

    private StatisticsInformation MakeReadOnly()
    {
      this.items_.MakeReadOnly();
      return this;
    }

    public static StatisticsInformation.Builder CreateBuilder()
    {
      return new StatisticsInformation.Builder();
    }

    public override StatisticsInformation.Builder ToBuilder()
    {
      return StatisticsInformation.CreateBuilder(this);
    }

    public override StatisticsInformation.Builder CreateBuilderForType()
    {
      return new StatisticsInformation.Builder();
    }

    public static StatisticsInformation.Builder CreateBuilder(StatisticsInformation prototype)
    {
      return new StatisticsInformation.Builder(prototype);
    }

    static StatisticsInformation()
    {
      object.ReferenceEquals((object) HibernatingRhinosProfilerMessages.Descriptor, (object) null);
    }

    [DebuggerNonUserCode]
    public static class Types
    {
      [DebuggerNonUserCode]
      public sealed class KeyValue : GeneratedMessage<StatisticsInformation.Types.KeyValue, StatisticsInformation.Types.KeyValue.Builder>
      {
        private static readonly StatisticsInformation.Types.KeyValue defaultInstance = new StatisticsInformation.Types.KeyValue().MakeReadOnly();
        private static readonly string[] _keyValueFieldNames = new string[2]
        {
          nameof (Key),
          "Value"
        };
        private static readonly uint[] _keyValueFieldTags = new uint[2]
        {
          10U,
          18U
        };
        private string key_ = "";
        private PopsicleList<string> value_ = new PopsicleList<string>();
        private int memoizedSerializedSize = -1;
        public const int KeyFieldNumber = 1;
        public const int ValueFieldNumber = 2;
        private bool hasKey;

        private KeyValue()
        {
        }

        public static StatisticsInformation.Types.KeyValue DefaultInstance
        {
          get
          {
            return StatisticsInformation.Types.KeyValue.defaultInstance;
          }
        }

        public override StatisticsInformation.Types.KeyValue DefaultInstanceForType
        {
          get
          {
            return StatisticsInformation.Types.KeyValue.DefaultInstance;
          }
        }

        protected override StatisticsInformation.Types.KeyValue ThisMessage
        {
          get
          {
            return this;
          }
        }

        public static MessageDescriptor Descriptor
        {
          get
          {
            return HibernatingRhinosProfilerMessages.internal__static_HibernatingRhinos_Profiler_Appender_Messages_StatisticsInformation_KeyValue__Descriptor;
          }
        }

        protected override FieldAccessorTable<StatisticsInformation.Types.KeyValue, StatisticsInformation.Types.KeyValue.Builder> InternalFieldAccessors
        {
          get
          {
            return HibernatingRhinosProfilerMessages.internal__static_HibernatingRhinos_Profiler_Appender_Messages_StatisticsInformation_KeyValue__FieldAccessorTable;
          }
        }

        public bool HasKey
        {
          get
          {
            return this.hasKey;
          }
        }

        public string Key
        {
          get
          {
            return this.key_;
          }
        }

        public IList<string> ValueList
        {
          get
          {
            return Lists.AsReadOnly<string>((IList<string>) this.value_);
          }
        }

        public int ValueCount
        {
          get
          {
            return this.value_.Count;
          }
        }

        public string GetValue(int index)
        {
          return this.value_[index];
        }

        public override bool IsInitialized
        {
          get
          {
            return this.hasKey;
          }
        }

        public override void WriteTo(ICodedOutputStream output)
        {
          int serializedSize = this.SerializedSize;
          string[] keyValueFieldNames = StatisticsInformation.Types.KeyValue._keyValueFieldNames;
          if (this.hasKey)
            output.WriteString(1, keyValueFieldNames[0], this.Key);
          if (this.value_.Count > 0)
            output.WriteStringArray(2, keyValueFieldNames[1], (IEnumerable<string>) this.value_);
          this.UnknownFields.WriteTo(output);
        }

        public override int SerializedSize
        {
          get
          {
            int memoizedSerializedSize = this.memoizedSerializedSize;
            if (memoizedSerializedSize != -1)
              return memoizedSerializedSize;
            int num1 = 0;
            if (this.hasKey)
              num1 += CodedOutputStream.ComputeStringSize(1, this.Key);
            int num2 = 0;
            foreach (string str in (IEnumerable<string>) this.ValueList)
              num2 += CodedOutputStream.ComputeStringSizeNoTag(str);
            int num3 = num1 + num2 + this.value_.Count + this.UnknownFields.SerializedSize;
            this.memoizedSerializedSize = num3;
            return num3;
          }
        }

        public static StatisticsInformation.Types.KeyValue ParseFrom(ByteString data)
        {
          return StatisticsInformation.Types.KeyValue.CreateBuilder().MergeFrom(data).BuildParsed();
        }

        public static StatisticsInformation.Types.KeyValue ParseFrom(ByteString data, ExtensionRegistry extensionRegistry)
        {
          return StatisticsInformation.Types.KeyValue.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
        }

        public static StatisticsInformation.Types.KeyValue ParseFrom(byte[] data)
        {
          return StatisticsInformation.Types.KeyValue.CreateBuilder().MergeFrom(data).BuildParsed();
        }

        public static StatisticsInformation.Types.KeyValue ParseFrom(byte[] data, ExtensionRegistry extensionRegistry)
        {
          return StatisticsInformation.Types.KeyValue.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
        }

        public static StatisticsInformation.Types.KeyValue ParseFrom(Stream input)
        {
          return StatisticsInformation.Types.KeyValue.CreateBuilder().MergeFrom(input).BuildParsed();
        }

        public static StatisticsInformation.Types.KeyValue ParseFrom(Stream input, ExtensionRegistry extensionRegistry)
        {
          return StatisticsInformation.Types.KeyValue.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
        }

        public static StatisticsInformation.Types.KeyValue ParseDelimitedFrom(Stream input)
        {
          return StatisticsInformation.Types.KeyValue.CreateBuilder().MergeDelimitedFrom(input).BuildParsed();
        }

        public static StatisticsInformation.Types.KeyValue ParseDelimitedFrom(Stream input, ExtensionRegistry extensionRegistry)
        {
          return StatisticsInformation.Types.KeyValue.CreateBuilder().MergeDelimitedFrom(input, extensionRegistry).BuildParsed();
        }

        public static StatisticsInformation.Types.KeyValue ParseFrom(ICodedInputStream input)
        {
          return StatisticsInformation.Types.KeyValue.CreateBuilder().MergeFrom(input).BuildParsed();
        }

        public static StatisticsInformation.Types.KeyValue ParseFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
        {
          return StatisticsInformation.Types.KeyValue.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
        }

        private StatisticsInformation.Types.KeyValue MakeReadOnly()
        {
          this.value_.MakeReadOnly();
          return this;
        }

        public static StatisticsInformation.Types.KeyValue.Builder CreateBuilder()
        {
          return new StatisticsInformation.Types.KeyValue.Builder();
        }

        public override StatisticsInformation.Types.KeyValue.Builder ToBuilder()
        {
          return StatisticsInformation.Types.KeyValue.CreateBuilder(this);
        }

        public override StatisticsInformation.Types.KeyValue.Builder CreateBuilderForType()
        {
          return new StatisticsInformation.Types.KeyValue.Builder();
        }

        public static StatisticsInformation.Types.KeyValue.Builder CreateBuilder(StatisticsInformation.Types.KeyValue prototype)
        {
          return new StatisticsInformation.Types.KeyValue.Builder(prototype);
        }

        static KeyValue()
        {
          object.ReferenceEquals((object) HibernatingRhinosProfilerMessages.Descriptor, (object) null);
        }

        [DebuggerNonUserCode]
        public sealed class Builder : GeneratedBuilder<StatisticsInformation.Types.KeyValue, StatisticsInformation.Types.KeyValue.Builder>
        {
          private bool resultIsReadOnly;
          private StatisticsInformation.Types.KeyValue result;

          protected override StatisticsInformation.Types.KeyValue.Builder ThisBuilder
          {
            get
            {
              return this;
            }
          }

          public Builder()
          {
            this.result = StatisticsInformation.Types.KeyValue.DefaultInstance;
            this.resultIsReadOnly = true;
          }

          internal Builder(StatisticsInformation.Types.KeyValue cloneFrom)
          {
            this.result = cloneFrom;
            this.resultIsReadOnly = true;
          }

          private StatisticsInformation.Types.KeyValue PrepareBuilder()
          {
            if (this.resultIsReadOnly)
            {
              StatisticsInformation.Types.KeyValue result = this.result;
              this.result = new StatisticsInformation.Types.KeyValue();
              this.resultIsReadOnly = false;
              this.MergeFrom(result);
            }
            return this.result;
          }

          public override bool IsInitialized
          {
            get
            {
              return this.result.IsInitialized;
            }
          }

          protected override StatisticsInformation.Types.KeyValue MessageBeingBuilt
          {
            get
            {
              return this.PrepareBuilder();
            }
          }

          public override StatisticsInformation.Types.KeyValue.Builder Clear()
          {
            this.result = StatisticsInformation.Types.KeyValue.DefaultInstance;
            this.resultIsReadOnly = true;
            return this;
          }

          public override StatisticsInformation.Types.KeyValue.Builder Clone()
          {
            if (this.resultIsReadOnly)
              return new StatisticsInformation.Types.KeyValue.Builder(this.result);
            return new StatisticsInformation.Types.KeyValue.Builder().MergeFrom(this.result);
          }

          public override MessageDescriptor DescriptorForType
          {
            get
            {
              return StatisticsInformation.Types.KeyValue.Descriptor;
            }
          }

          public override StatisticsInformation.Types.KeyValue DefaultInstanceForType
          {
            get
            {
              return StatisticsInformation.Types.KeyValue.DefaultInstance;
            }
          }

          public override StatisticsInformation.Types.KeyValue BuildPartial()
          {
            if (this.resultIsReadOnly)
              return this.result;
            this.resultIsReadOnly = true;
            return this.result.MakeReadOnly();
          }

          public override StatisticsInformation.Types.KeyValue.Builder MergeFrom(IMessage other)
          {
            if (other is StatisticsInformation.Types.KeyValue)
              return this.MergeFrom((StatisticsInformation.Types.KeyValue) other);
            base.MergeFrom(other);
            return this;
          }

          public override StatisticsInformation.Types.KeyValue.Builder MergeFrom(StatisticsInformation.Types.KeyValue other)
          {
            if (other == StatisticsInformation.Types.KeyValue.DefaultInstance)
              return this;
            this.PrepareBuilder();
            if (other.HasKey)
              this.Key = other.Key;
            if (other.value_.Count != 0)
              this.result.value_.Add((IEnumerable<string>) other.value_);
            this.MergeUnknownFields(other.UnknownFields);
            return this;
          }

          public override StatisticsInformation.Types.KeyValue.Builder MergeFrom(ICodedInputStream input)
          {
            return this.MergeFrom(input, ExtensionRegistry.Empty);
          }

          public override StatisticsInformation.Types.KeyValue.Builder MergeFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
          {
            this.PrepareBuilder();
            UnknownFieldSet.Builder unknownFields = (UnknownFieldSet.Builder) null;
            uint fieldTag;
            string fieldName;
            while (input.ReadTag(out fieldTag, out fieldName))
            {
              if ((int) fieldTag == 0 && fieldName != null)
              {
                int index = Array.BinarySearch<string>(StatisticsInformation.Types.KeyValue._keyValueFieldNames, fieldName, (IComparer<string>) StringComparer.Ordinal);
                if (index >= 0)
                {
                  fieldTag = StatisticsInformation.Types.KeyValue._keyValueFieldTags[index];
                }
                else
                {
                  if (unknownFields == null)
                    unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
                  this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
                  continue;
                }
              }
              switch (fieldTag)
              {
                case 0:
                  throw InvalidProtocolBufferException.InvalidTag();
                case 10:
                  this.result.hasKey = input.ReadString(ref this.result.key_);
                  continue;
                case 18:
                  input.ReadStringArray(fieldTag, fieldName, (ICollection<string>) this.result.value_);
                  continue;
                default:
                  if (WireFormat.IsEndGroupTag(fieldTag))
                  {
                    if (unknownFields != null)
                      this.UnknownFields = unknownFields.Build();
                    return this;
                  }
                  if (unknownFields == null)
                    unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
                  this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
                  continue;
              }
            }
            if (unknownFields != null)
              this.UnknownFields = unknownFields.Build();
            return this;
          }

          public bool HasKey
          {
            get
            {
              return this.result.hasKey;
            }
          }

          public string Key
          {
            get
            {
              return this.result.Key;
            }
            set
            {
              this.SetKey(value);
            }
          }

          public StatisticsInformation.Types.KeyValue.Builder SetKey(string value)
          {
            Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
            this.PrepareBuilder();
            this.result.hasKey = true;
            this.result.key_ = value;
            return this;
          }

          public StatisticsInformation.Types.KeyValue.Builder ClearKey()
          {
            this.PrepareBuilder();
            this.result.hasKey = false;
            this.result.key_ = "";
            return this;
          }

          public IPopsicleList<string> ValueList
          {
            get
            {
              return (IPopsicleList<string>) this.PrepareBuilder().value_;
            }
          }

          public int ValueCount
          {
            get
            {
              return this.result.ValueCount;
            }
          }

          public string GetValue(int index)
          {
            return this.result.GetValue(index);
          }

          public StatisticsInformation.Types.KeyValue.Builder SetValue(int index, string value)
          {
            Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
            this.PrepareBuilder();
            this.result.value_[index] = value;
            return this;
          }

          public StatisticsInformation.Types.KeyValue.Builder AddValue(string value)
          {
            Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
            this.PrepareBuilder();
            this.result.value_.Add(value);
            return this;
          }

          public StatisticsInformation.Types.KeyValue.Builder AddRangeValue(IEnumerable<string> values)
          {
            this.PrepareBuilder();
            this.result.value_.Add(values);
            return this;
          }

          public StatisticsInformation.Types.KeyValue.Builder ClearValue()
          {
            this.PrepareBuilder();
            this.result.value_.Clear();
            return this;
          }
        }
      }
    }

    [DebuggerNonUserCode]
    public sealed class Builder : GeneratedBuilder<StatisticsInformation, StatisticsInformation.Builder>
    {
      private bool resultIsReadOnly;
      private StatisticsInformation result;

      protected override StatisticsInformation.Builder ThisBuilder
      {
        get
        {
          return this;
        }
      }

      public Builder()
      {
        this.result = StatisticsInformation.DefaultInstance;
        this.resultIsReadOnly = true;
      }

      internal Builder(StatisticsInformation cloneFrom)
      {
        this.result = cloneFrom;
        this.resultIsReadOnly = true;
      }

      private StatisticsInformation PrepareBuilder()
      {
        if (this.resultIsReadOnly)
        {
          StatisticsInformation result = this.result;
          this.result = new StatisticsInformation();
          this.resultIsReadOnly = false;
          this.MergeFrom(result);
        }
        return this.result;
      }

      public override bool IsInitialized
      {
        get
        {
          return this.result.IsInitialized;
        }
      }

      protected override StatisticsInformation MessageBeingBuilt
      {
        get
        {
          return this.PrepareBuilder();
        }
      }

      public override StatisticsInformation.Builder Clear()
      {
        this.result = StatisticsInformation.DefaultInstance;
        this.resultIsReadOnly = true;
        return this;
      }

      public override StatisticsInformation.Builder Clone()
      {
        if (this.resultIsReadOnly)
          return new StatisticsInformation.Builder(this.result);
        return new StatisticsInformation.Builder().MergeFrom(this.result);
      }

      public override MessageDescriptor DescriptorForType
      {
        get
        {
          return StatisticsInformation.Descriptor;
        }
      }

      public override StatisticsInformation DefaultInstanceForType
      {
        get
        {
          return StatisticsInformation.DefaultInstance;
        }
      }

      public override StatisticsInformation BuildPartial()
      {
        if (this.resultIsReadOnly)
          return this.result;
        this.resultIsReadOnly = true;
        return this.result.MakeReadOnly();
      }

      public override StatisticsInformation.Builder MergeFrom(IMessage other)
      {
        if (other is StatisticsInformation)
          return this.MergeFrom((StatisticsInformation) other);
        base.MergeFrom(other);
        return this;
      }

      public override StatisticsInformation.Builder MergeFrom(StatisticsInformation other)
      {
        if (other == StatisticsInformation.DefaultInstance)
          return this;
        this.PrepareBuilder();
        if (other.HasName)
          this.Name = other.Name;
        if (other.items_.Count != 0)
          this.result.items_.Add((IEnumerable<StatisticsInformation.Types.KeyValue>) other.items_);
        this.MergeUnknownFields(other.UnknownFields);
        return this;
      }

      public override StatisticsInformation.Builder MergeFrom(ICodedInputStream input)
      {
        return this.MergeFrom(input, ExtensionRegistry.Empty);
      }

      public override StatisticsInformation.Builder MergeFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
      {
        this.PrepareBuilder();
        UnknownFieldSet.Builder unknownFields = (UnknownFieldSet.Builder) null;
        uint fieldTag;
        string fieldName;
        while (input.ReadTag(out fieldTag, out fieldName))
        {
          if ((int) fieldTag == 0 && fieldName != null)
          {
            int index = Array.BinarySearch<string>(StatisticsInformation._statisticsInformationFieldNames, fieldName, (IComparer<string>) StringComparer.Ordinal);
            if (index >= 0)
            {
              fieldTag = StatisticsInformation._statisticsInformationFieldTags[index];
            }
            else
            {
              if (unknownFields == null)
                unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
              this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
              continue;
            }
          }
          switch (fieldTag)
          {
            case 0:
              throw InvalidProtocolBufferException.InvalidTag();
            case 10:
              this.result.hasName = input.ReadString(ref this.result.name_);
              continue;
            case 18:
              input.ReadMessageArray<StatisticsInformation.Types.KeyValue>(fieldTag, fieldName, (ICollection<StatisticsInformation.Types.KeyValue>) this.result.items_, StatisticsInformation.Types.KeyValue.DefaultInstance, extensionRegistry);
              continue;
            default:
              if (WireFormat.IsEndGroupTag(fieldTag))
              {
                if (unknownFields != null)
                  this.UnknownFields = unknownFields.Build();
                return this;
              }
              if (unknownFields == null)
                unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
              this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
              continue;
          }
        }
        if (unknownFields != null)
          this.UnknownFields = unknownFields.Build();
        return this;
      }

      public bool HasName
      {
        get
        {
          return this.result.hasName;
        }
      }

      public string Name
      {
        get
        {
          return this.result.Name;
        }
        set
        {
          this.SetName(value);
        }
      }

      public StatisticsInformation.Builder SetName(string value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.hasName = true;
        this.result.name_ = value;
        return this;
      }

      public StatisticsInformation.Builder ClearName()
      {
        this.PrepareBuilder();
        this.result.hasName = false;
        this.result.name_ = "";
        return this;
      }

      public IPopsicleList<StatisticsInformation.Types.KeyValue> ItemsList
      {
        get
        {
          return (IPopsicleList<StatisticsInformation.Types.KeyValue>) this.PrepareBuilder().items_;
        }
      }

      public int ItemsCount
      {
        get
        {
          return this.result.ItemsCount;
        }
      }

      public StatisticsInformation.Types.KeyValue GetItems(int index)
      {
        return this.result.GetItems(index);
      }

      public StatisticsInformation.Builder SetItems(int index, StatisticsInformation.Types.KeyValue value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.items_[index] = value;
        return this;
      }

      public StatisticsInformation.Builder SetItems(int index, StatisticsInformation.Types.KeyValue.Builder builderForValue)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
        this.PrepareBuilder();
        this.result.items_[index] = builderForValue.Build();
        return this;
      }

      public StatisticsInformation.Builder AddItems(StatisticsInformation.Types.KeyValue value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.items_.Add(value);
        return this;
      }

      public StatisticsInformation.Builder AddItems(StatisticsInformation.Types.KeyValue.Builder builderForValue)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
        this.PrepareBuilder();
        this.result.items_.Add(builderForValue.Build());
        return this;
      }

      public StatisticsInformation.Builder AddRangeItems(IEnumerable<StatisticsInformation.Types.KeyValue> values)
      {
        this.PrepareBuilder();
        this.result.items_.Add(values);
        return this;
      }

      public StatisticsInformation.Builder ClearItems()
      {
        this.PrepareBuilder();
        this.result.items_.Clear();
        return this;
      }
    }
  }
}
