﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Appender.Messages.LoggingEventMessage
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers;
using Google.ProtocolBuffers.Collections;
using Google.ProtocolBuffers.Descriptors;
using Google.ProtocolBuffers.FieldAccess;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace HibernatingRhinos.Profiler.Appender.Messages
{
  [DebuggerNonUserCode]
  public sealed class LoggingEventMessage : GeneratedMessage<LoggingEventMessage, LoggingEventMessage.Builder>
  {
    private static readonly LoggingEventMessage defaultInstance = new LoggingEventMessage().MakeReadOnly();
    private static readonly string[] _loggingEventMessageFieldNames = new string[14]
    {
      nameof (Culture),
      nameof (DateAsMillisecondsSinceUnixEpoch),
      nameof (DbDialect),
      nameof (Exception),
      nameof (Level),
      nameof (Logger),
      nameof (Message),
      nameof (ScopeName),
      nameof (SessionId),
      nameof (StackTrace),
      nameof (StarColor),
      nameof (ThreadContext),
      nameof (ThreadId),
      nameof (Url)
    };
    private static readonly uint[] _loggingEventMessageFieldTags = new uint[14]
    {
      98U,
      24U,
      90U,
      74U,
      56U,
      18U,
      10U,
      114U,
      34U,
      50U,
      106U,
      66U,
      82U,
      42U
    };
    private string message_ = "";
    private string logger_ = "";
    private string sessionId_ = "";
    private string url_ = "";
    private string threadContext_ = "";
    private string threadId_ = "";
    private string exception_ = "";
    private string dbDialect_ = "";
    private string culture_ = "";
    private string starColor_ = "";
    private string scopeName_ = "";
    private int memoizedSerializedSize = -1;
    public const int MessageFieldNumber = 1;
    public const int LoggerFieldNumber = 2;
    public const int DateAsMillisecondsSinceUnixEpochFieldNumber = 3;
    public const int SessionIdFieldNumber = 4;
    public const int UrlFieldNumber = 5;
    public const int LevelFieldNumber = 7;
    public const int ThreadContextFieldNumber = 8;
    public const int ThreadIdFieldNumber = 10;
    public const int StackTraceFieldNumber = 6;
    public const int ExceptionFieldNumber = 9;
    public const int DbDialectFieldNumber = 11;
    public const int CultureFieldNumber = 12;
    public const int StarColorFieldNumber = 13;
    public const int ScopeNameFieldNumber = 14;
    private bool hasMessage;
    private bool hasLogger;
    private bool hasDateAsMillisecondsSinceUnixEpoch;
    private long dateAsMillisecondsSinceUnixEpoch_;
    private bool hasSessionId;
    private bool hasUrl;
    private bool hasLevel;
    private int level_;
    private bool hasThreadContext;
    private bool hasThreadId;
    private bool hasStackTrace;
    private LoggingEventMessage.Types.StackTraceInfo stackTrace_;
    private bool hasException;
    private bool hasDbDialect;
    private bool hasCulture;
    private bool hasStarColor;
    private bool hasScopeName;

    private LoggingEventMessage()
    {
    }

    public static LoggingEventMessage DefaultInstance
    {
      get
      {
        return LoggingEventMessage.defaultInstance;
      }
    }

    public override LoggingEventMessage DefaultInstanceForType
    {
      get
      {
        return LoggingEventMessage.DefaultInstance;
      }
    }

    protected override LoggingEventMessage ThisMessage
    {
      get
      {
        return this;
      }
    }

    public static MessageDescriptor Descriptor
    {
      get
      {
        return HibernatingRhinosProfilerMessages.internal__static_HibernatingRhinos_Profiler_Appender_Messages_LoggingEventMessage__Descriptor;
      }
    }

    protected override FieldAccessorTable<LoggingEventMessage, LoggingEventMessage.Builder> InternalFieldAccessors
    {
      get
      {
        return HibernatingRhinosProfilerMessages.internal__static_HibernatingRhinos_Profiler_Appender_Messages_LoggingEventMessage__FieldAccessorTable;
      }
    }

    public bool HasMessage
    {
      get
      {
        return this.hasMessage;
      }
    }

    public string Message
    {
      get
      {
        return this.message_;
      }
    }

    public bool HasLogger
    {
      get
      {
        return this.hasLogger;
      }
    }

    public string Logger
    {
      get
      {
        return this.logger_;
      }
    }

    public bool HasDateAsMillisecondsSinceUnixEpoch
    {
      get
      {
        return this.hasDateAsMillisecondsSinceUnixEpoch;
      }
    }

    public long DateAsMillisecondsSinceUnixEpoch
    {
      get
      {
        return this.dateAsMillisecondsSinceUnixEpoch_;
      }
    }

    public bool HasSessionId
    {
      get
      {
        return this.hasSessionId;
      }
    }

    public string SessionId
    {
      get
      {
        return this.sessionId_;
      }
    }

    public bool HasUrl
    {
      get
      {
        return this.hasUrl;
      }
    }

    public string Url
    {
      get
      {
        return this.url_;
      }
    }

    public bool HasLevel
    {
      get
      {
        return this.hasLevel;
      }
    }

    public int Level
    {
      get
      {
        return this.level_;
      }
    }

    public bool HasThreadContext
    {
      get
      {
        return this.hasThreadContext;
      }
    }

    public string ThreadContext
    {
      get
      {
        return this.threadContext_;
      }
    }

    public bool HasThreadId
    {
      get
      {
        return this.hasThreadId;
      }
    }

    public string ThreadId
    {
      get
      {
        return this.threadId_;
      }
    }

    public bool HasStackTrace
    {
      get
      {
        return this.hasStackTrace;
      }
    }

    public LoggingEventMessage.Types.StackTraceInfo StackTrace
    {
      get
      {
        return this.stackTrace_ ?? LoggingEventMessage.Types.StackTraceInfo.DefaultInstance;
      }
    }

    public bool HasException
    {
      get
      {
        return this.hasException;
      }
    }

    public string Exception
    {
      get
      {
        return this.exception_;
      }
    }

    public bool HasDbDialect
    {
      get
      {
        return this.hasDbDialect;
      }
    }

    public string DbDialect
    {
      get
      {
        return this.dbDialect_;
      }
    }

    public bool HasCulture
    {
      get
      {
        return this.hasCulture;
      }
    }

    public string Culture
    {
      get
      {
        return this.culture_;
      }
    }

    public bool HasStarColor
    {
      get
      {
        return this.hasStarColor;
      }
    }

    public string StarColor
    {
      get
      {
        return this.starColor_;
      }
    }

    public bool HasScopeName
    {
      get
      {
        return this.hasScopeName;
      }
    }

    public string ScopeName
    {
      get
      {
        return this.scopeName_;
      }
    }

    public override bool IsInitialized
    {
      get
      {
        return this.hasMessage && this.hasLogger && (this.hasDateAsMillisecondsSinceUnixEpoch && this.hasSessionId) && (this.hasUrl && this.hasLevel && this.hasThreadContext) && (!this.HasStackTrace || this.StackTrace.IsInitialized);
      }
    }

    public override void WriteTo(ICodedOutputStream output)
    {
      int serializedSize = this.SerializedSize;
      string[] messageFieldNames = LoggingEventMessage._loggingEventMessageFieldNames;
      if (this.hasMessage)
        output.WriteString(1, messageFieldNames[6], this.Message);
      if (this.hasLogger)
        output.WriteString(2, messageFieldNames[5], this.Logger);
      if (this.hasDateAsMillisecondsSinceUnixEpoch)
        output.WriteInt64(3, messageFieldNames[1], this.DateAsMillisecondsSinceUnixEpoch);
      if (this.hasSessionId)
        output.WriteString(4, messageFieldNames[8], this.SessionId);
      if (this.hasUrl)
        output.WriteString(5, messageFieldNames[13], this.Url);
      if (this.hasStackTrace)
        output.WriteMessage(6, messageFieldNames[9], (IMessageLite) this.StackTrace);
      if (this.hasLevel)
        output.WriteInt32(7, messageFieldNames[4], this.Level);
      if (this.hasThreadContext)
        output.WriteString(8, messageFieldNames[11], this.ThreadContext);
      if (this.hasException)
        output.WriteString(9, messageFieldNames[3], this.Exception);
      if (this.hasThreadId)
        output.WriteString(10, messageFieldNames[12], this.ThreadId);
      if (this.hasDbDialect)
        output.WriteString(11, messageFieldNames[2], this.DbDialect);
      if (this.hasCulture)
        output.WriteString(12, messageFieldNames[0], this.Culture);
      if (this.hasStarColor)
        output.WriteString(13, messageFieldNames[10], this.StarColor);
      if (this.hasScopeName)
        output.WriteString(14, messageFieldNames[7], this.ScopeName);
      this.UnknownFields.WriteTo(output);
    }

    public override int SerializedSize
    {
      get
      {
        int memoizedSerializedSize = this.memoizedSerializedSize;
        if (memoizedSerializedSize != -1)
          return memoizedSerializedSize;
        int num1 = 0;
        if (this.hasMessage)
          num1 += CodedOutputStream.ComputeStringSize(1, this.Message);
        if (this.hasLogger)
          num1 += CodedOutputStream.ComputeStringSize(2, this.Logger);
        if (this.hasDateAsMillisecondsSinceUnixEpoch)
          num1 += CodedOutputStream.ComputeInt64Size(3, this.DateAsMillisecondsSinceUnixEpoch);
        if (this.hasSessionId)
          num1 += CodedOutputStream.ComputeStringSize(4, this.SessionId);
        if (this.hasUrl)
          num1 += CodedOutputStream.ComputeStringSize(5, this.Url);
        if (this.hasLevel)
          num1 += CodedOutputStream.ComputeInt32Size(7, this.Level);
        if (this.hasThreadContext)
          num1 += CodedOutputStream.ComputeStringSize(8, this.ThreadContext);
        if (this.hasThreadId)
          num1 += CodedOutputStream.ComputeStringSize(10, this.ThreadId);
        if (this.hasStackTrace)
          num1 += CodedOutputStream.ComputeMessageSize(6, (IMessageLite) this.StackTrace);
        if (this.hasException)
          num1 += CodedOutputStream.ComputeStringSize(9, this.Exception);
        if (this.hasDbDialect)
          num1 += CodedOutputStream.ComputeStringSize(11, this.DbDialect);
        if (this.hasCulture)
          num1 += CodedOutputStream.ComputeStringSize(12, this.Culture);
        if (this.hasStarColor)
          num1 += CodedOutputStream.ComputeStringSize(13, this.StarColor);
        if (this.hasScopeName)
          num1 += CodedOutputStream.ComputeStringSize(14, this.ScopeName);
        int num2 = num1 + this.UnknownFields.SerializedSize;
        this.memoizedSerializedSize = num2;
        return num2;
      }
    }

    public static LoggingEventMessage ParseFrom(ByteString data)
    {
      return LoggingEventMessage.CreateBuilder().MergeFrom(data).BuildParsed();
    }

    public static LoggingEventMessage ParseFrom(ByteString data, ExtensionRegistry extensionRegistry)
    {
      return LoggingEventMessage.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
    }

    public static LoggingEventMessage ParseFrom(byte[] data)
    {
      return LoggingEventMessage.CreateBuilder().MergeFrom(data).BuildParsed();
    }

    public static LoggingEventMessage ParseFrom(byte[] data, ExtensionRegistry extensionRegistry)
    {
      return LoggingEventMessage.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
    }

    public static LoggingEventMessage ParseFrom(Stream input)
    {
      return LoggingEventMessage.CreateBuilder().MergeFrom(input).BuildParsed();
    }

    public static LoggingEventMessage ParseFrom(Stream input, ExtensionRegistry extensionRegistry)
    {
      return LoggingEventMessage.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
    }

    public static LoggingEventMessage ParseDelimitedFrom(Stream input)
    {
      return LoggingEventMessage.CreateBuilder().MergeDelimitedFrom(input).BuildParsed();
    }

    public static LoggingEventMessage ParseDelimitedFrom(Stream input, ExtensionRegistry extensionRegistry)
    {
      return LoggingEventMessage.CreateBuilder().MergeDelimitedFrom(input, extensionRegistry).BuildParsed();
    }

    public static LoggingEventMessage ParseFrom(ICodedInputStream input)
    {
      return LoggingEventMessage.CreateBuilder().MergeFrom(input).BuildParsed();
    }

    public static LoggingEventMessage ParseFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
    {
      return LoggingEventMessage.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
    }

    private LoggingEventMessage MakeReadOnly()
    {
      return this;
    }

    public static LoggingEventMessage.Builder CreateBuilder()
    {
      return new LoggingEventMessage.Builder();
    }

    public override LoggingEventMessage.Builder ToBuilder()
    {
      return LoggingEventMessage.CreateBuilder(this);
    }

    public override LoggingEventMessage.Builder CreateBuilderForType()
    {
      return new LoggingEventMessage.Builder();
    }

    public static LoggingEventMessage.Builder CreateBuilder(LoggingEventMessage prototype)
    {
      return new LoggingEventMessage.Builder(prototype);
    }

    static LoggingEventMessage()
    {
      object.ReferenceEquals((object) HibernatingRhinosProfilerMessages.Descriptor, (object) null);
    }

    [DebuggerNonUserCode]
    public static class Types
    {
      [DebuggerNonUserCode]
      public sealed class StackTraceFrame : GeneratedMessage<LoggingEventMessage.Types.StackTraceFrame, LoggingEventMessage.Types.StackTraceFrame.Builder>
      {
        private static readonly LoggingEventMessage.Types.StackTraceFrame defaultInstance = new LoggingEventMessage.Types.StackTraceFrame().MakeReadOnly();
        private static readonly string[] _stackTraceFrameFieldNames = new string[6]
        {
          nameof (Column),
          nameof (FullFilename),
          nameof (Line),
          nameof (Method),
          nameof (Namespace),
          nameof (Type)
        };
        private static readonly uint[] _stackTraceFrameFieldTags = new uint[6]
        {
          24U,
          50U,
          16U,
          34U,
          42U,
          10U
        };
        private string type_ = "";
        private string method_ = "";
        private string namespace_ = "";
        private string fullFilename_ = "";
        private int memoizedSerializedSize = -1;
        public const int TypeFieldNumber = 1;
        public const int LineFieldNumber = 2;
        public const int ColumnFieldNumber = 3;
        public const int MethodFieldNumber = 4;
        public const int NamespaceFieldNumber = 5;
        public const int FullFilenameFieldNumber = 6;
        private bool hasType;
        private bool hasLine;
        private int line_;
        private bool hasColumn;
        private int column_;
        private bool hasMethod;
        private bool hasNamespace;
        private bool hasFullFilename;

        private StackTraceFrame()
        {
        }

        public static LoggingEventMessage.Types.StackTraceFrame DefaultInstance
        {
          get
          {
            return LoggingEventMessage.Types.StackTraceFrame.defaultInstance;
          }
        }

        public override LoggingEventMessage.Types.StackTraceFrame DefaultInstanceForType
        {
          get
          {
            return LoggingEventMessage.Types.StackTraceFrame.DefaultInstance;
          }
        }

        protected override LoggingEventMessage.Types.StackTraceFrame ThisMessage
        {
          get
          {
            return this;
          }
        }

        public static MessageDescriptor Descriptor
        {
          get
          {
            return HibernatingRhinosProfilerMessages.internal__static_HibernatingRhinos_Profiler_Appender_Messages_LoggingEventMessage_StackTraceFrame__Descriptor;
          }
        }

        protected override FieldAccessorTable<LoggingEventMessage.Types.StackTraceFrame, LoggingEventMessage.Types.StackTraceFrame.Builder> InternalFieldAccessors
        {
          get
          {
            return HibernatingRhinosProfilerMessages.internal__static_HibernatingRhinos_Profiler_Appender_Messages_LoggingEventMessage_StackTraceFrame__FieldAccessorTable;
          }
        }

        public bool HasType
        {
          get
          {
            return this.hasType;
          }
        }

        public string Type
        {
          get
          {
            return this.type_;
          }
        }

        public bool HasLine
        {
          get
          {
            return this.hasLine;
          }
        }

        public int Line
        {
          get
          {
            return this.line_;
          }
        }

        public bool HasColumn
        {
          get
          {
            return this.hasColumn;
          }
        }

        public int Column
        {
          get
          {
            return this.column_;
          }
        }

        public bool HasMethod
        {
          get
          {
            return this.hasMethod;
          }
        }

        public string Method
        {
          get
          {
            return this.method_;
          }
        }

        public bool HasNamespace
        {
          get
          {
            return this.hasNamespace;
          }
        }

        public string Namespace
        {
          get
          {
            return this.namespace_;
          }
        }

        public bool HasFullFilename
        {
          get
          {
            return this.hasFullFilename;
          }
        }

        public string FullFilename
        {
          get
          {
            return this.fullFilename_;
          }
        }

        public override bool IsInitialized
        {
          get
          {
            return this.hasType && this.hasLine && (this.hasColumn && this.hasMethod) && this.hasNamespace;
          }
        }

        public override void WriteTo(ICodedOutputStream output)
        {
          int serializedSize = this.SerializedSize;
          string[] traceFrameFieldNames = LoggingEventMessage.Types.StackTraceFrame._stackTraceFrameFieldNames;
          if (this.hasType)
            output.WriteString(1, traceFrameFieldNames[5], this.Type);
          if (this.hasLine)
            output.WriteInt32(2, traceFrameFieldNames[2], this.Line);
          if (this.hasColumn)
            output.WriteInt32(3, traceFrameFieldNames[0], this.Column);
          if (this.hasMethod)
            output.WriteString(4, traceFrameFieldNames[3], this.Method);
          if (this.hasNamespace)
            output.WriteString(5, traceFrameFieldNames[4], this.Namespace);
          if (this.hasFullFilename)
            output.WriteString(6, traceFrameFieldNames[1], this.FullFilename);
          this.UnknownFields.WriteTo(output);
        }

        public override int SerializedSize
        {
          get
          {
            int memoizedSerializedSize = this.memoizedSerializedSize;
            if (memoizedSerializedSize != -1)
              return memoizedSerializedSize;
            int num1 = 0;
            if (this.hasType)
              num1 += CodedOutputStream.ComputeStringSize(1, this.Type);
            if (this.hasLine)
              num1 += CodedOutputStream.ComputeInt32Size(2, this.Line);
            if (this.hasColumn)
              num1 += CodedOutputStream.ComputeInt32Size(3, this.Column);
            if (this.hasMethod)
              num1 += CodedOutputStream.ComputeStringSize(4, this.Method);
            if (this.hasNamespace)
              num1 += CodedOutputStream.ComputeStringSize(5, this.Namespace);
            if (this.hasFullFilename)
              num1 += CodedOutputStream.ComputeStringSize(6, this.FullFilename);
            int num2 = num1 + this.UnknownFields.SerializedSize;
            this.memoizedSerializedSize = num2;
            return num2;
          }
        }

        public static LoggingEventMessage.Types.StackTraceFrame ParseFrom(ByteString data)
        {
          return LoggingEventMessage.Types.StackTraceFrame.CreateBuilder().MergeFrom(data).BuildParsed();
        }

        public static LoggingEventMessage.Types.StackTraceFrame ParseFrom(ByteString data, ExtensionRegistry extensionRegistry)
        {
          return LoggingEventMessage.Types.StackTraceFrame.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
        }

        public static LoggingEventMessage.Types.StackTraceFrame ParseFrom(byte[] data)
        {
          return LoggingEventMessage.Types.StackTraceFrame.CreateBuilder().MergeFrom(data).BuildParsed();
        }

        public static LoggingEventMessage.Types.StackTraceFrame ParseFrom(byte[] data, ExtensionRegistry extensionRegistry)
        {
          return LoggingEventMessage.Types.StackTraceFrame.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
        }

        public static LoggingEventMessage.Types.StackTraceFrame ParseFrom(Stream input)
        {
          return LoggingEventMessage.Types.StackTraceFrame.CreateBuilder().MergeFrom(input).BuildParsed();
        }

        public static LoggingEventMessage.Types.StackTraceFrame ParseFrom(Stream input, ExtensionRegistry extensionRegistry)
        {
          return LoggingEventMessage.Types.StackTraceFrame.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
        }

        public static LoggingEventMessage.Types.StackTraceFrame ParseDelimitedFrom(Stream input)
        {
          return LoggingEventMessage.Types.StackTraceFrame.CreateBuilder().MergeDelimitedFrom(input).BuildParsed();
        }

        public static LoggingEventMessage.Types.StackTraceFrame ParseDelimitedFrom(Stream input, ExtensionRegistry extensionRegistry)
        {
          return LoggingEventMessage.Types.StackTraceFrame.CreateBuilder().MergeDelimitedFrom(input, extensionRegistry).BuildParsed();
        }

        public static LoggingEventMessage.Types.StackTraceFrame ParseFrom(ICodedInputStream input)
        {
          return LoggingEventMessage.Types.StackTraceFrame.CreateBuilder().MergeFrom(input).BuildParsed();
        }

        public static LoggingEventMessage.Types.StackTraceFrame ParseFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
        {
          return LoggingEventMessage.Types.StackTraceFrame.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
        }

        private LoggingEventMessage.Types.StackTraceFrame MakeReadOnly()
        {
          return this;
        }

        public static LoggingEventMessage.Types.StackTraceFrame.Builder CreateBuilder()
        {
          return new LoggingEventMessage.Types.StackTraceFrame.Builder();
        }

        public override LoggingEventMessage.Types.StackTraceFrame.Builder ToBuilder()
        {
          return LoggingEventMessage.Types.StackTraceFrame.CreateBuilder(this);
        }

        public override LoggingEventMessage.Types.StackTraceFrame.Builder CreateBuilderForType()
        {
          return new LoggingEventMessage.Types.StackTraceFrame.Builder();
        }

        public static LoggingEventMessage.Types.StackTraceFrame.Builder CreateBuilder(LoggingEventMessage.Types.StackTraceFrame prototype)
        {
          return new LoggingEventMessage.Types.StackTraceFrame.Builder(prototype);
        }

        static StackTraceFrame()
        {
          object.ReferenceEquals((object) HibernatingRhinosProfilerMessages.Descriptor, (object) null);
        }

        [DebuggerNonUserCode]
        public sealed class Builder : GeneratedBuilder<LoggingEventMessage.Types.StackTraceFrame, LoggingEventMessage.Types.StackTraceFrame.Builder>
        {
          private bool resultIsReadOnly;
          private LoggingEventMessage.Types.StackTraceFrame result;

          protected override LoggingEventMessage.Types.StackTraceFrame.Builder ThisBuilder
          {
            get
            {
              return this;
            }
          }

          public Builder()
          {
            this.result = LoggingEventMessage.Types.StackTraceFrame.DefaultInstance;
            this.resultIsReadOnly = true;
          }

          internal Builder(LoggingEventMessage.Types.StackTraceFrame cloneFrom)
          {
            this.result = cloneFrom;
            this.resultIsReadOnly = true;
          }

          private LoggingEventMessage.Types.StackTraceFrame PrepareBuilder()
          {
            if (this.resultIsReadOnly)
            {
              LoggingEventMessage.Types.StackTraceFrame result = this.result;
              this.result = new LoggingEventMessage.Types.StackTraceFrame();
              this.resultIsReadOnly = false;
              this.MergeFrom(result);
            }
            return this.result;
          }

          public override bool IsInitialized
          {
            get
            {
              return this.result.IsInitialized;
            }
          }

          protected override LoggingEventMessage.Types.StackTraceFrame MessageBeingBuilt
          {
            get
            {
              return this.PrepareBuilder();
            }
          }

          public override LoggingEventMessage.Types.StackTraceFrame.Builder Clear()
          {
            this.result = LoggingEventMessage.Types.StackTraceFrame.DefaultInstance;
            this.resultIsReadOnly = true;
            return this;
          }

          public override LoggingEventMessage.Types.StackTraceFrame.Builder Clone()
          {
            if (this.resultIsReadOnly)
              return new LoggingEventMessage.Types.StackTraceFrame.Builder(this.result);
            return new LoggingEventMessage.Types.StackTraceFrame.Builder().MergeFrom(this.result);
          }

          public override MessageDescriptor DescriptorForType
          {
            get
            {
              return LoggingEventMessage.Types.StackTraceFrame.Descriptor;
            }
          }

          public override LoggingEventMessage.Types.StackTraceFrame DefaultInstanceForType
          {
            get
            {
              return LoggingEventMessage.Types.StackTraceFrame.DefaultInstance;
            }
          }

          public override LoggingEventMessage.Types.StackTraceFrame BuildPartial()
          {
            if (this.resultIsReadOnly)
              return this.result;
            this.resultIsReadOnly = true;
            return this.result.MakeReadOnly();
          }

          public override LoggingEventMessage.Types.StackTraceFrame.Builder MergeFrom(IMessage other)
          {
            if (other is LoggingEventMessage.Types.StackTraceFrame)
              return this.MergeFrom((LoggingEventMessage.Types.StackTraceFrame) other);
            base.MergeFrom(other);
            return this;
          }

          public override LoggingEventMessage.Types.StackTraceFrame.Builder MergeFrom(LoggingEventMessage.Types.StackTraceFrame other)
          {
            if (other == LoggingEventMessage.Types.StackTraceFrame.DefaultInstance)
              return this;
            this.PrepareBuilder();
            if (other.HasType)
              this.Type = other.Type;
            if (other.HasLine)
              this.Line = other.Line;
            if (other.HasColumn)
              this.Column = other.Column;
            if (other.HasMethod)
              this.Method = other.Method;
            if (other.HasNamespace)
              this.Namespace = other.Namespace;
            if (other.HasFullFilename)
              this.FullFilename = other.FullFilename;
            this.MergeUnknownFields(other.UnknownFields);
            return this;
          }

          public override LoggingEventMessage.Types.StackTraceFrame.Builder MergeFrom(ICodedInputStream input)
          {
            return this.MergeFrom(input, ExtensionRegistry.Empty);
          }

          public override LoggingEventMessage.Types.StackTraceFrame.Builder MergeFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
          {
            this.PrepareBuilder();
            UnknownFieldSet.Builder unknownFields = (UnknownFieldSet.Builder) null;
            uint fieldTag;
            string fieldName;
            while (input.ReadTag(out fieldTag, out fieldName))
            {
              if ((int) fieldTag == 0 && fieldName != null)
              {
                int index = Array.BinarySearch<string>(LoggingEventMessage.Types.StackTraceFrame._stackTraceFrameFieldNames, fieldName, (IComparer<string>) StringComparer.Ordinal);
                if (index >= 0)
                {
                  fieldTag = LoggingEventMessage.Types.StackTraceFrame._stackTraceFrameFieldTags[index];
                }
                else
                {
                  if (unknownFields == null)
                    unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
                  this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
                  continue;
                }
              }
              switch (fieldTag)
              {
                case 0:
                  throw InvalidProtocolBufferException.InvalidTag();
                case 10:
                  this.result.hasType = input.ReadString(ref this.result.type_);
                  continue;
                case 16:
                  this.result.hasLine = input.ReadInt32(ref this.result.line_);
                  continue;
                case 24:
                  this.result.hasColumn = input.ReadInt32(ref this.result.column_);
                  continue;
                case 34:
                  this.result.hasMethod = input.ReadString(ref this.result.method_);
                  continue;
                case 42:
                  this.result.hasNamespace = input.ReadString(ref this.result.namespace_);
                  continue;
                case 50:
                  this.result.hasFullFilename = input.ReadString(ref this.result.fullFilename_);
                  continue;
                default:
                  if (WireFormat.IsEndGroupTag(fieldTag))
                  {
                    if (unknownFields != null)
                      this.UnknownFields = unknownFields.Build();
                    return this;
                  }
                  if (unknownFields == null)
                    unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
                  this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
                  continue;
              }
            }
            if (unknownFields != null)
              this.UnknownFields = unknownFields.Build();
            return this;
          }

          public bool HasType
          {
            get
            {
              return this.result.hasType;
            }
          }

          public string Type
          {
            get
            {
              return this.result.Type;
            }
            set
            {
              this.SetType(value);
            }
          }

          public LoggingEventMessage.Types.StackTraceFrame.Builder SetType(string value)
          {
            Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
            this.PrepareBuilder();
            this.result.hasType = true;
            this.result.type_ = value;
            return this;
          }

          public LoggingEventMessage.Types.StackTraceFrame.Builder ClearType()
          {
            this.PrepareBuilder();
            this.result.hasType = false;
            this.result.type_ = "";
            return this;
          }

          public bool HasLine
          {
            get
            {
              return this.result.hasLine;
            }
          }

          public int Line
          {
            get
            {
              return this.result.Line;
            }
            set
            {
              this.SetLine(value);
            }
          }

          public LoggingEventMessage.Types.StackTraceFrame.Builder SetLine(int value)
          {
            this.PrepareBuilder();
            this.result.hasLine = true;
            this.result.line_ = value;
            return this;
          }

          public LoggingEventMessage.Types.StackTraceFrame.Builder ClearLine()
          {
            this.PrepareBuilder();
            this.result.hasLine = false;
            this.result.line_ = 0;
            return this;
          }

          public bool HasColumn
          {
            get
            {
              return this.result.hasColumn;
            }
          }

          public int Column
          {
            get
            {
              return this.result.Column;
            }
            set
            {
              this.SetColumn(value);
            }
          }

          public LoggingEventMessage.Types.StackTraceFrame.Builder SetColumn(int value)
          {
            this.PrepareBuilder();
            this.result.hasColumn = true;
            this.result.column_ = value;
            return this;
          }

          public LoggingEventMessage.Types.StackTraceFrame.Builder ClearColumn()
          {
            this.PrepareBuilder();
            this.result.hasColumn = false;
            this.result.column_ = 0;
            return this;
          }

          public bool HasMethod
          {
            get
            {
              return this.result.hasMethod;
            }
          }

          public string Method
          {
            get
            {
              return this.result.Method;
            }
            set
            {
              this.SetMethod(value);
            }
          }

          public LoggingEventMessage.Types.StackTraceFrame.Builder SetMethod(string value)
          {
            Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
            this.PrepareBuilder();
            this.result.hasMethod = true;
            this.result.method_ = value;
            return this;
          }

          public LoggingEventMessage.Types.StackTraceFrame.Builder ClearMethod()
          {
            this.PrepareBuilder();
            this.result.hasMethod = false;
            this.result.method_ = "";
            return this;
          }

          public bool HasNamespace
          {
            get
            {
              return this.result.hasNamespace;
            }
          }

          public string Namespace
          {
            get
            {
              return this.result.Namespace;
            }
            set
            {
              this.SetNamespace(value);
            }
          }

          public LoggingEventMessage.Types.StackTraceFrame.Builder SetNamespace(string value)
          {
            Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
            this.PrepareBuilder();
            this.result.hasNamespace = true;
            this.result.namespace_ = value;
            return this;
          }

          public LoggingEventMessage.Types.StackTraceFrame.Builder ClearNamespace()
          {
            this.PrepareBuilder();
            this.result.hasNamespace = false;
            this.result.namespace_ = "";
            return this;
          }

          public bool HasFullFilename
          {
            get
            {
              return this.result.hasFullFilename;
            }
          }

          public string FullFilename
          {
            get
            {
              return this.result.FullFilename;
            }
            set
            {
              this.SetFullFilename(value);
            }
          }

          public LoggingEventMessage.Types.StackTraceFrame.Builder SetFullFilename(string value)
          {
            Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
            this.PrepareBuilder();
            this.result.hasFullFilename = true;
            this.result.fullFilename_ = value;
            return this;
          }

          public LoggingEventMessage.Types.StackTraceFrame.Builder ClearFullFilename()
          {
            this.PrepareBuilder();
            this.result.hasFullFilename = false;
            this.result.fullFilename_ = "";
            return this;
          }
        }
      }

      [DebuggerNonUserCode]
      public sealed class StackTraceInfo : GeneratedMessage<LoggingEventMessage.Types.StackTraceInfo, LoggingEventMessage.Types.StackTraceInfo.Builder>
      {
        private static readonly LoggingEventMessage.Types.StackTraceInfo defaultInstance = new LoggingEventMessage.Types.StackTraceInfo().MakeReadOnly();
        private static readonly string[] _stackTraceInfoFieldNames = new string[1]
        {
          "Frames"
        };
        private static readonly uint[] _stackTraceInfoFieldTags = new uint[1]
        {
          10U
        };
        private PopsicleList<LoggingEventMessage.Types.StackTraceFrame> frames_ = new PopsicleList<LoggingEventMessage.Types.StackTraceFrame>();
        private int memoizedSerializedSize = -1;
        public const int FramesFieldNumber = 1;

        private StackTraceInfo()
        {
        }

        public static LoggingEventMessage.Types.StackTraceInfo DefaultInstance
        {
          get
          {
            return LoggingEventMessage.Types.StackTraceInfo.defaultInstance;
          }
        }

        public override LoggingEventMessage.Types.StackTraceInfo DefaultInstanceForType
        {
          get
          {
            return LoggingEventMessage.Types.StackTraceInfo.DefaultInstance;
          }
        }

        protected override LoggingEventMessage.Types.StackTraceInfo ThisMessage
        {
          get
          {
            return this;
          }
        }

        public static MessageDescriptor Descriptor
        {
          get
          {
            return HibernatingRhinosProfilerMessages.internal__static_HibernatingRhinos_Profiler_Appender_Messages_LoggingEventMessage_StackTraceInfo__Descriptor;
          }
        }

        protected override FieldAccessorTable<LoggingEventMessage.Types.StackTraceInfo, LoggingEventMessage.Types.StackTraceInfo.Builder> InternalFieldAccessors
        {
          get
          {
            return HibernatingRhinosProfilerMessages.internal__static_HibernatingRhinos_Profiler_Appender_Messages_LoggingEventMessage_StackTraceInfo__FieldAccessorTable;
          }
        }

        public IList<LoggingEventMessage.Types.StackTraceFrame> FramesList
        {
          get
          {
            return (IList<LoggingEventMessage.Types.StackTraceFrame>) this.frames_;
          }
        }

        public int FramesCount
        {
          get
          {
            return this.frames_.Count;
          }
        }

        public LoggingEventMessage.Types.StackTraceFrame GetFrames(int index)
        {
          return this.frames_[index];
        }

        public override bool IsInitialized
        {
          get
          {
            foreach (AbstractMessageLite<LoggingEventMessage.Types.StackTraceFrame, LoggingEventMessage.Types.StackTraceFrame.Builder> frames in (IEnumerable<LoggingEventMessage.Types.StackTraceFrame>) this.FramesList)
            {
              if (!frames.IsInitialized)
                return false;
            }
            return true;
          }
        }

        public override void WriteTo(ICodedOutputStream output)
        {
          int serializedSize = this.SerializedSize;
          string[] traceInfoFieldNames = LoggingEventMessage.Types.StackTraceInfo._stackTraceInfoFieldNames;
          if (this.frames_.Count > 0)
            output.WriteMessageArray<LoggingEventMessage.Types.StackTraceFrame>(1, traceInfoFieldNames[0], (IEnumerable<LoggingEventMessage.Types.StackTraceFrame>) this.frames_);
          this.UnknownFields.WriteTo(output);
        }

        public override int SerializedSize
        {
          get
          {
            int memoizedSerializedSize = this.memoizedSerializedSize;
            if (memoizedSerializedSize != -1)
              return memoizedSerializedSize;
            int num1 = 0;
            foreach (LoggingEventMessage.Types.StackTraceFrame frames in (IEnumerable<LoggingEventMessage.Types.StackTraceFrame>) this.FramesList)
              num1 += CodedOutputStream.ComputeMessageSize(1, (IMessageLite) frames);
            int num2 = num1 + this.UnknownFields.SerializedSize;
            this.memoizedSerializedSize = num2;
            return num2;
          }
        }

        public static LoggingEventMessage.Types.StackTraceInfo ParseFrom(ByteString data)
        {
          return LoggingEventMessage.Types.StackTraceInfo.CreateBuilder().MergeFrom(data).BuildParsed();
        }

        public static LoggingEventMessage.Types.StackTraceInfo ParseFrom(ByteString data, ExtensionRegistry extensionRegistry)
        {
          return LoggingEventMessage.Types.StackTraceInfo.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
        }

        public static LoggingEventMessage.Types.StackTraceInfo ParseFrom(byte[] data)
        {
          return LoggingEventMessage.Types.StackTraceInfo.CreateBuilder().MergeFrom(data).BuildParsed();
        }

        public static LoggingEventMessage.Types.StackTraceInfo ParseFrom(byte[] data, ExtensionRegistry extensionRegistry)
        {
          return LoggingEventMessage.Types.StackTraceInfo.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
        }

        public static LoggingEventMessage.Types.StackTraceInfo ParseFrom(Stream input)
        {
          return LoggingEventMessage.Types.StackTraceInfo.CreateBuilder().MergeFrom(input).BuildParsed();
        }

        public static LoggingEventMessage.Types.StackTraceInfo ParseFrom(Stream input, ExtensionRegistry extensionRegistry)
        {
          return LoggingEventMessage.Types.StackTraceInfo.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
        }

        public static LoggingEventMessage.Types.StackTraceInfo ParseDelimitedFrom(Stream input)
        {
          return LoggingEventMessage.Types.StackTraceInfo.CreateBuilder().MergeDelimitedFrom(input).BuildParsed();
        }

        public static LoggingEventMessage.Types.StackTraceInfo ParseDelimitedFrom(Stream input, ExtensionRegistry extensionRegistry)
        {
          return LoggingEventMessage.Types.StackTraceInfo.CreateBuilder().MergeDelimitedFrom(input, extensionRegistry).BuildParsed();
        }

        public static LoggingEventMessage.Types.StackTraceInfo ParseFrom(ICodedInputStream input)
        {
          return LoggingEventMessage.Types.StackTraceInfo.CreateBuilder().MergeFrom(input).BuildParsed();
        }

        public static LoggingEventMessage.Types.StackTraceInfo ParseFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
        {
          return LoggingEventMessage.Types.StackTraceInfo.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
        }

        private LoggingEventMessage.Types.StackTraceInfo MakeReadOnly()
        {
          this.frames_.MakeReadOnly();
          return this;
        }

        public static LoggingEventMessage.Types.StackTraceInfo.Builder CreateBuilder()
        {
          return new LoggingEventMessage.Types.StackTraceInfo.Builder();
        }

        public override LoggingEventMessage.Types.StackTraceInfo.Builder ToBuilder()
        {
          return LoggingEventMessage.Types.StackTraceInfo.CreateBuilder(this);
        }

        public override LoggingEventMessage.Types.StackTraceInfo.Builder CreateBuilderForType()
        {
          return new LoggingEventMessage.Types.StackTraceInfo.Builder();
        }

        public static LoggingEventMessage.Types.StackTraceInfo.Builder CreateBuilder(LoggingEventMessage.Types.StackTraceInfo prototype)
        {
          return new LoggingEventMessage.Types.StackTraceInfo.Builder(prototype);
        }

        static StackTraceInfo()
        {
          object.ReferenceEquals((object) HibernatingRhinosProfilerMessages.Descriptor, (object) null);
        }

        [DebuggerNonUserCode]
        public sealed class Builder : GeneratedBuilder<LoggingEventMessage.Types.StackTraceInfo, LoggingEventMessage.Types.StackTraceInfo.Builder>
        {
          private bool resultIsReadOnly;
          private LoggingEventMessage.Types.StackTraceInfo result;

          protected override LoggingEventMessage.Types.StackTraceInfo.Builder ThisBuilder
          {
            get
            {
              return this;
            }
          }

          public Builder()
          {
            this.result = LoggingEventMessage.Types.StackTraceInfo.DefaultInstance;
            this.resultIsReadOnly = true;
          }

          internal Builder(LoggingEventMessage.Types.StackTraceInfo cloneFrom)
          {
            this.result = cloneFrom;
            this.resultIsReadOnly = true;
          }

          private LoggingEventMessage.Types.StackTraceInfo PrepareBuilder()
          {
            if (this.resultIsReadOnly)
            {
              LoggingEventMessage.Types.StackTraceInfo result = this.result;
              this.result = new LoggingEventMessage.Types.StackTraceInfo();
              this.resultIsReadOnly = false;
              this.MergeFrom(result);
            }
            return this.result;
          }

          public override bool IsInitialized
          {
            get
            {
              return this.result.IsInitialized;
            }
          }

          protected override LoggingEventMessage.Types.StackTraceInfo MessageBeingBuilt
          {
            get
            {
              return this.PrepareBuilder();
            }
          }

          public override LoggingEventMessage.Types.StackTraceInfo.Builder Clear()
          {
            this.result = LoggingEventMessage.Types.StackTraceInfo.DefaultInstance;
            this.resultIsReadOnly = true;
            return this;
          }

          public override LoggingEventMessage.Types.StackTraceInfo.Builder Clone()
          {
            if (this.resultIsReadOnly)
              return new LoggingEventMessage.Types.StackTraceInfo.Builder(this.result);
            return new LoggingEventMessage.Types.StackTraceInfo.Builder().MergeFrom(this.result);
          }

          public override MessageDescriptor DescriptorForType
          {
            get
            {
              return LoggingEventMessage.Types.StackTraceInfo.Descriptor;
            }
          }

          public override LoggingEventMessage.Types.StackTraceInfo DefaultInstanceForType
          {
            get
            {
              return LoggingEventMessage.Types.StackTraceInfo.DefaultInstance;
            }
          }

          public override LoggingEventMessage.Types.StackTraceInfo BuildPartial()
          {
            if (this.resultIsReadOnly)
              return this.result;
            this.resultIsReadOnly = true;
            return this.result.MakeReadOnly();
          }

          public override LoggingEventMessage.Types.StackTraceInfo.Builder MergeFrom(IMessage other)
          {
            if (other is LoggingEventMessage.Types.StackTraceInfo)
              return this.MergeFrom((LoggingEventMessage.Types.StackTraceInfo) other);
            base.MergeFrom(other);
            return this;
          }

          public override LoggingEventMessage.Types.StackTraceInfo.Builder MergeFrom(LoggingEventMessage.Types.StackTraceInfo other)
          {
            if (other == LoggingEventMessage.Types.StackTraceInfo.DefaultInstance)
              return this;
            this.PrepareBuilder();
            if (other.frames_.Count != 0)
              this.result.frames_.Add((IEnumerable<LoggingEventMessage.Types.StackTraceFrame>) other.frames_);
            this.MergeUnknownFields(other.UnknownFields);
            return this;
          }

          public override LoggingEventMessage.Types.StackTraceInfo.Builder MergeFrom(ICodedInputStream input)
          {
            return this.MergeFrom(input, ExtensionRegistry.Empty);
          }

          public override LoggingEventMessage.Types.StackTraceInfo.Builder MergeFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
          {
            this.PrepareBuilder();
            UnknownFieldSet.Builder unknownFields = (UnknownFieldSet.Builder) null;
            uint fieldTag;
            string fieldName;
            while (input.ReadTag(out fieldTag, out fieldName))
            {
              if ((int) fieldTag == 0 && fieldName != null)
              {
                int index = Array.BinarySearch<string>(LoggingEventMessage.Types.StackTraceInfo._stackTraceInfoFieldNames, fieldName, (IComparer<string>) StringComparer.Ordinal);
                if (index >= 0)
                {
                  fieldTag = LoggingEventMessage.Types.StackTraceInfo._stackTraceInfoFieldTags[index];
                }
                else
                {
                  if (unknownFields == null)
                    unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
                  this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
                  continue;
                }
              }
              switch (fieldTag)
              {
                case 0:
                  throw InvalidProtocolBufferException.InvalidTag();
                case 10:
                  input.ReadMessageArray<LoggingEventMessage.Types.StackTraceFrame>(fieldTag, fieldName, (ICollection<LoggingEventMessage.Types.StackTraceFrame>) this.result.frames_, LoggingEventMessage.Types.StackTraceFrame.DefaultInstance, extensionRegistry);
                  continue;
                default:
                  if (WireFormat.IsEndGroupTag(fieldTag))
                  {
                    if (unknownFields != null)
                      this.UnknownFields = unknownFields.Build();
                    return this;
                  }
                  if (unknownFields == null)
                    unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
                  this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
                  continue;
              }
            }
            if (unknownFields != null)
              this.UnknownFields = unknownFields.Build();
            return this;
          }

          public IPopsicleList<LoggingEventMessage.Types.StackTraceFrame> FramesList
          {
            get
            {
              return (IPopsicleList<LoggingEventMessage.Types.StackTraceFrame>) this.PrepareBuilder().frames_;
            }
          }

          public int FramesCount
          {
            get
            {
              return this.result.FramesCount;
            }
          }

          public LoggingEventMessage.Types.StackTraceFrame GetFrames(int index)
          {
            return this.result.GetFrames(index);
          }

          public LoggingEventMessage.Types.StackTraceInfo.Builder SetFrames(int index, LoggingEventMessage.Types.StackTraceFrame value)
          {
            Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
            this.PrepareBuilder();
            this.result.frames_[index] = value;
            return this;
          }

          public LoggingEventMessage.Types.StackTraceInfo.Builder SetFrames(int index, LoggingEventMessage.Types.StackTraceFrame.Builder builderForValue)
          {
            Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
            this.PrepareBuilder();
            this.result.frames_[index] = builderForValue.Build();
            return this;
          }

          public LoggingEventMessage.Types.StackTraceInfo.Builder AddFrames(LoggingEventMessage.Types.StackTraceFrame value)
          {
            Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
            this.PrepareBuilder();
            this.result.frames_.Add(value);
            return this;
          }

          public LoggingEventMessage.Types.StackTraceInfo.Builder AddFrames(LoggingEventMessage.Types.StackTraceFrame.Builder builderForValue)
          {
            Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
            this.PrepareBuilder();
            this.result.frames_.Add(builderForValue.Build());
            return this;
          }

          public LoggingEventMessage.Types.StackTraceInfo.Builder AddRangeFrames(IEnumerable<LoggingEventMessage.Types.StackTraceFrame> values)
          {
            this.PrepareBuilder();
            this.result.frames_.Add(values);
            return this;
          }

          public LoggingEventMessage.Types.StackTraceInfo.Builder ClearFrames()
          {
            this.PrepareBuilder();
            this.result.frames_.Clear();
            return this;
          }
        }
      }
    }

    [DebuggerNonUserCode]
    public sealed class Builder : GeneratedBuilder<LoggingEventMessage, LoggingEventMessage.Builder>
    {
      private bool resultIsReadOnly;
      private LoggingEventMessage result;

      protected override LoggingEventMessage.Builder ThisBuilder
      {
        get
        {
          return this;
        }
      }

      public Builder()
      {
        this.result = LoggingEventMessage.DefaultInstance;
        this.resultIsReadOnly = true;
      }

      internal Builder(LoggingEventMessage cloneFrom)
      {
        this.result = cloneFrom;
        this.resultIsReadOnly = true;
      }

      private LoggingEventMessage PrepareBuilder()
      {
        if (this.resultIsReadOnly)
        {
          LoggingEventMessage result = this.result;
          this.result = new LoggingEventMessage();
          this.resultIsReadOnly = false;
          this.MergeFrom(result);
        }
        return this.result;
      }

      public override bool IsInitialized
      {
        get
        {
          return this.result.IsInitialized;
        }
      }

      protected override LoggingEventMessage MessageBeingBuilt
      {
        get
        {
          return this.PrepareBuilder();
        }
      }

      public override LoggingEventMessage.Builder Clear()
      {
        this.result = LoggingEventMessage.DefaultInstance;
        this.resultIsReadOnly = true;
        return this;
      }

      public override LoggingEventMessage.Builder Clone()
      {
        if (this.resultIsReadOnly)
          return new LoggingEventMessage.Builder(this.result);
        return new LoggingEventMessage.Builder().MergeFrom(this.result);
      }

      public override MessageDescriptor DescriptorForType
      {
        get
        {
          return LoggingEventMessage.Descriptor;
        }
      }

      public override LoggingEventMessage DefaultInstanceForType
      {
        get
        {
          return LoggingEventMessage.DefaultInstance;
        }
      }

      public override LoggingEventMessage BuildPartial()
      {
        if (this.resultIsReadOnly)
          return this.result;
        this.resultIsReadOnly = true;
        return this.result.MakeReadOnly();
      }

      public override LoggingEventMessage.Builder MergeFrom(IMessage other)
      {
        if (other is LoggingEventMessage)
          return this.MergeFrom((LoggingEventMessage) other);
        base.MergeFrom(other);
        return this;
      }

      public override LoggingEventMessage.Builder MergeFrom(LoggingEventMessage other)
      {
        if (other == LoggingEventMessage.DefaultInstance)
          return this;
        this.PrepareBuilder();
        if (other.HasMessage)
          this.Message = other.Message;
        if (other.HasLogger)
          this.Logger = other.Logger;
        if (other.HasDateAsMillisecondsSinceUnixEpoch)
          this.DateAsMillisecondsSinceUnixEpoch = other.DateAsMillisecondsSinceUnixEpoch;
        if (other.HasSessionId)
          this.SessionId = other.SessionId;
        if (other.HasUrl)
          this.Url = other.Url;
        if (other.HasLevel)
          this.Level = other.Level;
        if (other.HasThreadContext)
          this.ThreadContext = other.ThreadContext;
        if (other.HasThreadId)
          this.ThreadId = other.ThreadId;
        if (other.HasStackTrace)
          this.MergeStackTrace(other.StackTrace);
        if (other.HasException)
          this.Exception = other.Exception;
        if (other.HasDbDialect)
          this.DbDialect = other.DbDialect;
        if (other.HasCulture)
          this.Culture = other.Culture;
        if (other.HasStarColor)
          this.StarColor = other.StarColor;
        if (other.HasScopeName)
          this.ScopeName = other.ScopeName;
        this.MergeUnknownFields(other.UnknownFields);
        return this;
      }

      public override LoggingEventMessage.Builder MergeFrom(ICodedInputStream input)
      {
        return this.MergeFrom(input, ExtensionRegistry.Empty);
      }

      public override LoggingEventMessage.Builder MergeFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
      {
        this.PrepareBuilder();
        UnknownFieldSet.Builder unknownFields = (UnknownFieldSet.Builder) null;
        uint fieldTag;
        string fieldName;
        while (input.ReadTag(out fieldTag, out fieldName))
        {
          if ((int) fieldTag == 0 && fieldName != null)
          {
            int index = Array.BinarySearch<string>(LoggingEventMessage._loggingEventMessageFieldNames, fieldName, (IComparer<string>) StringComparer.Ordinal);
            if (index >= 0)
            {
              fieldTag = LoggingEventMessage._loggingEventMessageFieldTags[index];
            }
            else
            {
              if (unknownFields == null)
                unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
              this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
              continue;
            }
          }
          switch (fieldTag)
          {
            case 0:
              throw InvalidProtocolBufferException.InvalidTag();
            case 10:
              this.result.hasMessage = input.ReadString(ref this.result.message_);
              continue;
            case 18:
              this.result.hasLogger = input.ReadString(ref this.result.logger_);
              continue;
            case 24:
              this.result.hasDateAsMillisecondsSinceUnixEpoch = input.ReadInt64(ref this.result.dateAsMillisecondsSinceUnixEpoch_);
              continue;
            case 34:
              this.result.hasSessionId = input.ReadString(ref this.result.sessionId_);
              continue;
            case 42:
              this.result.hasUrl = input.ReadString(ref this.result.url_);
              continue;
            case 50:
              LoggingEventMessage.Types.StackTraceInfo.Builder builder = LoggingEventMessage.Types.StackTraceInfo.CreateBuilder();
              if (this.result.hasStackTrace)
                builder.MergeFrom(this.StackTrace);
              input.ReadMessage((IBuilderLite) builder, extensionRegistry);
              this.StackTrace = builder.BuildPartial();
              continue;
            case 56:
              this.result.hasLevel = input.ReadInt32(ref this.result.level_);
              continue;
            case 66:
              this.result.hasThreadContext = input.ReadString(ref this.result.threadContext_);
              continue;
            case 74:
              this.result.hasException = input.ReadString(ref this.result.exception_);
              continue;
            case 82:
              this.result.hasThreadId = input.ReadString(ref this.result.threadId_);
              continue;
            case 90:
              this.result.hasDbDialect = input.ReadString(ref this.result.dbDialect_);
              continue;
            case 98:
              this.result.hasCulture = input.ReadString(ref this.result.culture_);
              continue;
            case 106:
              this.result.hasStarColor = input.ReadString(ref this.result.starColor_);
              continue;
            case 114:
              this.result.hasScopeName = input.ReadString(ref this.result.scopeName_);
              continue;
            default:
              if (WireFormat.IsEndGroupTag(fieldTag))
              {
                if (unknownFields != null)
                  this.UnknownFields = unknownFields.Build();
                return this;
              }
              if (unknownFields == null)
                unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
              this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
              continue;
          }
        }
        if (unknownFields != null)
          this.UnknownFields = unknownFields.Build();
        return this;
      }

      public bool HasMessage
      {
        get
        {
          return this.result.hasMessage;
        }
      }

      public string Message
      {
        get
        {
          return this.result.Message;
        }
        set
        {
          this.SetMessage(value);
        }
      }

      public LoggingEventMessage.Builder SetMessage(string value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.hasMessage = true;
        this.result.message_ = value;
        return this;
      }

      public LoggingEventMessage.Builder ClearMessage()
      {
        this.PrepareBuilder();
        this.result.hasMessage = false;
        this.result.message_ = "";
        return this;
      }

      public bool HasLogger
      {
        get
        {
          return this.result.hasLogger;
        }
      }

      public string Logger
      {
        get
        {
          return this.result.Logger;
        }
        set
        {
          this.SetLogger(value);
        }
      }

      public LoggingEventMessage.Builder SetLogger(string value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.hasLogger = true;
        this.result.logger_ = value;
        return this;
      }

      public LoggingEventMessage.Builder ClearLogger()
      {
        this.PrepareBuilder();
        this.result.hasLogger = false;
        this.result.logger_ = "";
        return this;
      }

      public bool HasDateAsMillisecondsSinceUnixEpoch
      {
        get
        {
          return this.result.hasDateAsMillisecondsSinceUnixEpoch;
        }
      }

      public long DateAsMillisecondsSinceUnixEpoch
      {
        get
        {
          return this.result.DateAsMillisecondsSinceUnixEpoch;
        }
        set
        {
          this.SetDateAsMillisecondsSinceUnixEpoch(value);
        }
      }

      public LoggingEventMessage.Builder SetDateAsMillisecondsSinceUnixEpoch(long value)
      {
        this.PrepareBuilder();
        this.result.hasDateAsMillisecondsSinceUnixEpoch = true;
        this.result.dateAsMillisecondsSinceUnixEpoch_ = value;
        return this;
      }

      public LoggingEventMessage.Builder ClearDateAsMillisecondsSinceUnixEpoch()
      {
        this.PrepareBuilder();
        this.result.hasDateAsMillisecondsSinceUnixEpoch = false;
        this.result.dateAsMillisecondsSinceUnixEpoch_ = 0L;
        return this;
      }

      public bool HasSessionId
      {
        get
        {
          return this.result.hasSessionId;
        }
      }

      public string SessionId
      {
        get
        {
          return this.result.SessionId;
        }
        set
        {
          this.SetSessionId(value);
        }
      }

      public LoggingEventMessage.Builder SetSessionId(string value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.hasSessionId = true;
        this.result.sessionId_ = value;
        return this;
      }

      public LoggingEventMessage.Builder ClearSessionId()
      {
        this.PrepareBuilder();
        this.result.hasSessionId = false;
        this.result.sessionId_ = "";
        return this;
      }

      public bool HasUrl
      {
        get
        {
          return this.result.hasUrl;
        }
      }

      public string Url
      {
        get
        {
          return this.result.Url;
        }
        set
        {
          this.SetUrl(value);
        }
      }

      public LoggingEventMessage.Builder SetUrl(string value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.hasUrl = true;
        this.result.url_ = value;
        return this;
      }

      public LoggingEventMessage.Builder ClearUrl()
      {
        this.PrepareBuilder();
        this.result.hasUrl = false;
        this.result.url_ = "";
        return this;
      }

      public bool HasLevel
      {
        get
        {
          return this.result.hasLevel;
        }
      }

      public int Level
      {
        get
        {
          return this.result.Level;
        }
        set
        {
          this.SetLevel(value);
        }
      }

      public LoggingEventMessage.Builder SetLevel(int value)
      {
        this.PrepareBuilder();
        this.result.hasLevel = true;
        this.result.level_ = value;
        return this;
      }

      public LoggingEventMessage.Builder ClearLevel()
      {
        this.PrepareBuilder();
        this.result.hasLevel = false;
        this.result.level_ = 0;
        return this;
      }

      public bool HasThreadContext
      {
        get
        {
          return this.result.hasThreadContext;
        }
      }

      public string ThreadContext
      {
        get
        {
          return this.result.ThreadContext;
        }
        set
        {
          this.SetThreadContext(value);
        }
      }

      public LoggingEventMessage.Builder SetThreadContext(string value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.hasThreadContext = true;
        this.result.threadContext_ = value;
        return this;
      }

      public LoggingEventMessage.Builder ClearThreadContext()
      {
        this.PrepareBuilder();
        this.result.hasThreadContext = false;
        this.result.threadContext_ = "";
        return this;
      }

      public bool HasThreadId
      {
        get
        {
          return this.result.hasThreadId;
        }
      }

      public string ThreadId
      {
        get
        {
          return this.result.ThreadId;
        }
        set
        {
          this.SetThreadId(value);
        }
      }

      public LoggingEventMessage.Builder SetThreadId(string value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.hasThreadId = true;
        this.result.threadId_ = value;
        return this;
      }

      public LoggingEventMessage.Builder ClearThreadId()
      {
        this.PrepareBuilder();
        this.result.hasThreadId = false;
        this.result.threadId_ = "";
        return this;
      }

      public bool HasStackTrace
      {
        get
        {
          return this.result.hasStackTrace;
        }
      }

      public LoggingEventMessage.Types.StackTraceInfo StackTrace
      {
        get
        {
          return this.result.StackTrace;
        }
        set
        {
          this.SetStackTrace(value);
        }
      }

      public LoggingEventMessage.Builder SetStackTrace(LoggingEventMessage.Types.StackTraceInfo value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.hasStackTrace = true;
        this.result.stackTrace_ = value;
        return this;
      }

      public LoggingEventMessage.Builder SetStackTrace(LoggingEventMessage.Types.StackTraceInfo.Builder builderForValue)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
        this.PrepareBuilder();
        this.result.hasStackTrace = true;
        this.result.stackTrace_ = builderForValue.Build();
        return this;
      }

      public LoggingEventMessage.Builder MergeStackTrace(LoggingEventMessage.Types.StackTraceInfo value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.stackTrace_ = !this.result.hasStackTrace || this.result.stackTrace_ == LoggingEventMessage.Types.StackTraceInfo.DefaultInstance ? value : LoggingEventMessage.Types.StackTraceInfo.CreateBuilder(this.result.stackTrace_).MergeFrom(value).BuildPartial();
        this.result.hasStackTrace = true;
        return this;
      }

      public LoggingEventMessage.Builder ClearStackTrace()
      {
        this.PrepareBuilder();
        this.result.hasStackTrace = false;
        this.result.stackTrace_ = (LoggingEventMessage.Types.StackTraceInfo) null;
        return this;
      }

      public bool HasException
      {
        get
        {
          return this.result.hasException;
        }
      }

      public string Exception
      {
        get
        {
          return this.result.Exception;
        }
        set
        {
          this.SetException(value);
        }
      }

      public LoggingEventMessage.Builder SetException(string value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.hasException = true;
        this.result.exception_ = value;
        return this;
      }

      public LoggingEventMessage.Builder ClearException()
      {
        this.PrepareBuilder();
        this.result.hasException = false;
        this.result.exception_ = "";
        return this;
      }

      public bool HasDbDialect
      {
        get
        {
          return this.result.hasDbDialect;
        }
      }

      public string DbDialect
      {
        get
        {
          return this.result.DbDialect;
        }
        set
        {
          this.SetDbDialect(value);
        }
      }

      public LoggingEventMessage.Builder SetDbDialect(string value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.hasDbDialect = true;
        this.result.dbDialect_ = value;
        return this;
      }

      public LoggingEventMessage.Builder ClearDbDialect()
      {
        this.PrepareBuilder();
        this.result.hasDbDialect = false;
        this.result.dbDialect_ = "";
        return this;
      }

      public bool HasCulture
      {
        get
        {
          return this.result.hasCulture;
        }
      }

      public string Culture
      {
        get
        {
          return this.result.Culture;
        }
        set
        {
          this.SetCulture(value);
        }
      }

      public LoggingEventMessage.Builder SetCulture(string value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.hasCulture = true;
        this.result.culture_ = value;
        return this;
      }

      public LoggingEventMessage.Builder ClearCulture()
      {
        this.PrepareBuilder();
        this.result.hasCulture = false;
        this.result.culture_ = "";
        return this;
      }

      public bool HasStarColor
      {
        get
        {
          return this.result.hasStarColor;
        }
      }

      public string StarColor
      {
        get
        {
          return this.result.StarColor;
        }
        set
        {
          this.SetStarColor(value);
        }
      }

      public LoggingEventMessage.Builder SetStarColor(string value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.hasStarColor = true;
        this.result.starColor_ = value;
        return this;
      }

      public LoggingEventMessage.Builder ClearStarColor()
      {
        this.PrepareBuilder();
        this.result.hasStarColor = false;
        this.result.starColor_ = "";
        return this;
      }

      public bool HasScopeName
      {
        get
        {
          return this.result.hasScopeName;
        }
      }

      public string ScopeName
      {
        get
        {
          return this.result.ScopeName;
        }
        set
        {
          this.SetScopeName(value);
        }
      }

      public LoggingEventMessage.Builder SetScopeName(string value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.hasScopeName = true;
        this.result.scopeName_ = value;
        return this;
      }

      public LoggingEventMessage.Builder ClearScopeName()
      {
        this.PrepareBuilder();
        this.result.hasScopeName = false;
        this.result.scopeName_ = "";
        return this;
      }
    }
  }
}
