﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Appender.Messages.ApplicationInformation
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers;
using Google.ProtocolBuffers.Descriptors;
using Google.ProtocolBuffers.FieldAccess;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace HibernatingRhinos.Profiler.Appender.Messages
{
  [DebuggerNonUserCode]
  public sealed class ApplicationInformation : GeneratedMessage<ApplicationInformation, ApplicationInformation.Builder>
  {
    private static readonly ApplicationInformation defaultInstance = new ApplicationInformation().MakeReadOnly();
    private static readonly string[] _applicationInformationFieldNames = new string[2]
    {
      nameof (Id),
      nameof (Name)
    };
    private static readonly uint[] _applicationInformationFieldTags = new uint[2]
    {
      10U,
      18U
    };
    private string id_ = "";
    private string name_ = "";
    private int memoizedSerializedSize = -1;
    public const int IdFieldNumber = 1;
    public const int NameFieldNumber = 2;
    private bool hasId;
    private bool hasName;

    private ApplicationInformation()
    {
    }

    public static ApplicationInformation DefaultInstance
    {
      get
      {
        return ApplicationInformation.defaultInstance;
      }
    }

    public override ApplicationInformation DefaultInstanceForType
    {
      get
      {
        return ApplicationInformation.DefaultInstance;
      }
    }

    protected override ApplicationInformation ThisMessage
    {
      get
      {
        return this;
      }
    }

    public static MessageDescriptor Descriptor
    {
      get
      {
        return HibernatingRhinosProfilerMessages.internal__static_HibernatingRhinos_Profiler_Appender_Messages_ApplicationInformation__Descriptor;
      }
    }

    protected override FieldAccessorTable<ApplicationInformation, ApplicationInformation.Builder> InternalFieldAccessors
    {
      get
      {
        return HibernatingRhinosProfilerMessages.internal__static_HibernatingRhinos_Profiler_Appender_Messages_ApplicationInformation__FieldAccessorTable;
      }
    }

    public bool HasId
    {
      get
      {
        return this.hasId;
      }
    }

    public string Id
    {
      get
      {
        return this.id_;
      }
    }

    public bool HasName
    {
      get
      {
        return this.hasName;
      }
    }

    public string Name
    {
      get
      {
        return this.name_;
      }
    }

    public override bool IsInitialized
    {
      get
      {
        return this.hasId && this.hasName;
      }
    }

    public override void WriteTo(ICodedOutputStream output)
    {
      int serializedSize = this.SerializedSize;
      string[] informationFieldNames = ApplicationInformation._applicationInformationFieldNames;
      if (this.hasId)
        output.WriteString(1, informationFieldNames[0], this.Id);
      if (this.hasName)
        output.WriteString(2, informationFieldNames[1], this.Name);
      this.UnknownFields.WriteTo(output);
    }

    public override int SerializedSize
    {
      get
      {
        int memoizedSerializedSize = this.memoizedSerializedSize;
        if (memoizedSerializedSize != -1)
          return memoizedSerializedSize;
        int num1 = 0;
        if (this.hasId)
          num1 += CodedOutputStream.ComputeStringSize(1, this.Id);
        if (this.hasName)
          num1 += CodedOutputStream.ComputeStringSize(2, this.Name);
        int num2 = num1 + this.UnknownFields.SerializedSize;
        this.memoizedSerializedSize = num2;
        return num2;
      }
    }

    public static ApplicationInformation ParseFrom(ByteString data)
    {
      return ApplicationInformation.CreateBuilder().MergeFrom(data).BuildParsed();
    }

    public static ApplicationInformation ParseFrom(ByteString data, ExtensionRegistry extensionRegistry)
    {
      return ApplicationInformation.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
    }

    public static ApplicationInformation ParseFrom(byte[] data)
    {
      return ApplicationInformation.CreateBuilder().MergeFrom(data).BuildParsed();
    }

    public static ApplicationInformation ParseFrom(byte[] data, ExtensionRegistry extensionRegistry)
    {
      return ApplicationInformation.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
    }

    public static ApplicationInformation ParseFrom(Stream input)
    {
      return ApplicationInformation.CreateBuilder().MergeFrom(input).BuildParsed();
    }

    public static ApplicationInformation ParseFrom(Stream input, ExtensionRegistry extensionRegistry)
    {
      return ApplicationInformation.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
    }

    public static ApplicationInformation ParseDelimitedFrom(Stream input)
    {
      return ApplicationInformation.CreateBuilder().MergeDelimitedFrom(input).BuildParsed();
    }

    public static ApplicationInformation ParseDelimitedFrom(Stream input, ExtensionRegistry extensionRegistry)
    {
      return ApplicationInformation.CreateBuilder().MergeDelimitedFrom(input, extensionRegistry).BuildParsed();
    }

    public static ApplicationInformation ParseFrom(ICodedInputStream input)
    {
      return ApplicationInformation.CreateBuilder().MergeFrom(input).BuildParsed();
    }

    public static ApplicationInformation ParseFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
    {
      return ApplicationInformation.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
    }

    private ApplicationInformation MakeReadOnly()
    {
      return this;
    }

    public static ApplicationInformation.Builder CreateBuilder()
    {
      return new ApplicationInformation.Builder();
    }

    public override ApplicationInformation.Builder ToBuilder()
    {
      return ApplicationInformation.CreateBuilder(this);
    }

    public override ApplicationInformation.Builder CreateBuilderForType()
    {
      return new ApplicationInformation.Builder();
    }

    public static ApplicationInformation.Builder CreateBuilder(ApplicationInformation prototype)
    {
      return new ApplicationInformation.Builder(prototype);
    }

    static ApplicationInformation()
    {
      object.ReferenceEquals((object) HibernatingRhinosProfilerMessages.Descriptor, (object) null);
    }

    [DebuggerNonUserCode]
    public sealed class Builder : GeneratedBuilder<ApplicationInformation, ApplicationInformation.Builder>
    {
      private bool resultIsReadOnly;
      private ApplicationInformation result;

      protected override ApplicationInformation.Builder ThisBuilder
      {
        get
        {
          return this;
        }
      }

      public Builder()
      {
        this.result = ApplicationInformation.DefaultInstance;
        this.resultIsReadOnly = true;
      }

      internal Builder(ApplicationInformation cloneFrom)
      {
        this.result = cloneFrom;
        this.resultIsReadOnly = true;
      }

      private ApplicationInformation PrepareBuilder()
      {
        if (this.resultIsReadOnly)
        {
          ApplicationInformation result = this.result;
          this.result = new ApplicationInformation();
          this.resultIsReadOnly = false;
          this.MergeFrom(result);
        }
        return this.result;
      }

      public override bool IsInitialized
      {
        get
        {
          return this.result.IsInitialized;
        }
      }

      protected override ApplicationInformation MessageBeingBuilt
      {
        get
        {
          return this.PrepareBuilder();
        }
      }

      public override ApplicationInformation.Builder Clear()
      {
        this.result = ApplicationInformation.DefaultInstance;
        this.resultIsReadOnly = true;
        return this;
      }

      public override ApplicationInformation.Builder Clone()
      {
        if (this.resultIsReadOnly)
          return new ApplicationInformation.Builder(this.result);
        return new ApplicationInformation.Builder().MergeFrom(this.result);
      }

      public override MessageDescriptor DescriptorForType
      {
        get
        {
          return ApplicationInformation.Descriptor;
        }
      }

      public override ApplicationInformation DefaultInstanceForType
      {
        get
        {
          return ApplicationInformation.DefaultInstance;
        }
      }

      public override ApplicationInformation BuildPartial()
      {
        if (this.resultIsReadOnly)
          return this.result;
        this.resultIsReadOnly = true;
        return this.result.MakeReadOnly();
      }

      public override ApplicationInformation.Builder MergeFrom(IMessage other)
      {
        if (other is ApplicationInformation)
          return this.MergeFrom((ApplicationInformation) other);
        base.MergeFrom(other);
        return this;
      }

      public override ApplicationInformation.Builder MergeFrom(ApplicationInformation other)
      {
        if (other == ApplicationInformation.DefaultInstance)
          return this;
        this.PrepareBuilder();
        if (other.HasId)
          this.Id = other.Id;
        if (other.HasName)
          this.Name = other.Name;
        this.MergeUnknownFields(other.UnknownFields);
        return this;
      }

      public override ApplicationInformation.Builder MergeFrom(ICodedInputStream input)
      {
        return this.MergeFrom(input, ExtensionRegistry.Empty);
      }

      public override ApplicationInformation.Builder MergeFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
      {
        this.PrepareBuilder();
        UnknownFieldSet.Builder unknownFields = (UnknownFieldSet.Builder) null;
        uint fieldTag;
        string fieldName;
        while (input.ReadTag(out fieldTag, out fieldName))
        {
          if ((int) fieldTag == 0 && fieldName != null)
          {
            int index = Array.BinarySearch<string>(ApplicationInformation._applicationInformationFieldNames, fieldName, (IComparer<string>) StringComparer.Ordinal);
            if (index >= 0)
            {
              fieldTag = ApplicationInformation._applicationInformationFieldTags[index];
            }
            else
            {
              if (unknownFields == null)
                unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
              this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
              continue;
            }
          }
          switch (fieldTag)
          {
            case 0:
              throw InvalidProtocolBufferException.InvalidTag();
            case 10:
              this.result.hasId = input.ReadString(ref this.result.id_);
              continue;
            case 18:
              this.result.hasName = input.ReadString(ref this.result.name_);
              continue;
            default:
              if (WireFormat.IsEndGroupTag(fieldTag))
              {
                if (unknownFields != null)
                  this.UnknownFields = unknownFields.Build();
                return this;
              }
              if (unknownFields == null)
                unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
              this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
              continue;
          }
        }
        if (unknownFields != null)
          this.UnknownFields = unknownFields.Build();
        return this;
      }

      public bool HasId
      {
        get
        {
          return this.result.hasId;
        }
      }

      public string Id
      {
        get
        {
          return this.result.Id;
        }
        set
        {
          this.SetId(value);
        }
      }

      public ApplicationInformation.Builder SetId(string value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.hasId = true;
        this.result.id_ = value;
        return this;
      }

      public ApplicationInformation.Builder ClearId()
      {
        this.PrepareBuilder();
        this.result.hasId = false;
        this.result.id_ = "";
        return this;
      }

      public bool HasName
      {
        get
        {
          return this.result.hasName;
        }
      }

      public string Name
      {
        get
        {
          return this.result.Name;
        }
        set
        {
          this.SetName(value);
        }
      }

      public ApplicationInformation.Builder SetName(string value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.hasName = true;
        this.result.name_ = value;
        return this;
      }

      public ApplicationInformation.Builder ClearName()
      {
        this.PrepareBuilder();
        this.result.hasName = false;
        this.result.name_ = "";
        return this;
      }
    }
  }
}
