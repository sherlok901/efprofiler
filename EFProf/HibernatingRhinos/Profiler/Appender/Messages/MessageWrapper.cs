﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Appender.Messages.MessageWrapper
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers;
using Google.ProtocolBuffers.Descriptors;
using Google.ProtocolBuffers.FieldAccess;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace HibernatingRhinos.Profiler.Appender.Messages
{
  [DebuggerNonUserCode]
  public sealed class MessageWrapper : GeneratedMessage<MessageWrapper, MessageWrapper.Builder>
  {
    private static readonly MessageWrapper defaultInstance = new MessageWrapper().MakeReadOnly();
    private static readonly string[] _messageWrapperFieldNames = new string[6]
    {
      nameof (Application),
      nameof (ContextFile),
      nameof (Message),
      nameof (MessageId),
      nameof (Stats),
      nameof (Type)
    };
    private static readonly uint[] _messageWrapperFieldTags = new uint[6]
    {
      42U,
      50U,
      18U,
      34U,
      26U,
      8U
    };
    private MessageWrapper.Types.MessageType type_ = MessageWrapper.Types.MessageType.LogEvent;
    private string messageId_ = "";
    private string contextFile_ = "";
    private int memoizedSerializedSize = -1;
    public const int TypeFieldNumber = 1;
    public const int MessageIdFieldNumber = 4;
    public const int MessageFieldNumber = 2;
    public const int StatsFieldNumber = 3;
    public const int ApplicationFieldNumber = 5;
    public const int ContextFileFieldNumber = 6;
    private bool hasType;
    private bool hasMessageId;
    private bool hasMessage;
    private LoggingEventMessage message_;
    private bool hasStats;
    private StatisticsInformation stats_;
    private bool hasApplication;
    private ApplicationInformation application_;
    private bool hasContextFile;

    private MessageWrapper()
    {
    }

    public static MessageWrapper DefaultInstance
    {
      get
      {
        return MessageWrapper.defaultInstance;
      }
    }

    public override MessageWrapper DefaultInstanceForType
    {
      get
      {
        return MessageWrapper.DefaultInstance;
      }
    }

    protected override MessageWrapper ThisMessage
    {
      get
      {
        return this;
      }
    }

    public static MessageDescriptor Descriptor
    {
      get
      {
        return HibernatingRhinosProfilerMessages.internal__static_HibernatingRhinos_Profiler_Appender_Messages_MessageWrapper__Descriptor;
      }
    }

    protected override FieldAccessorTable<MessageWrapper, MessageWrapper.Builder> InternalFieldAccessors
    {
      get
      {
        return HibernatingRhinosProfilerMessages.internal__static_HibernatingRhinos_Profiler_Appender_Messages_MessageWrapper__FieldAccessorTable;
      }
    }

    public bool HasType
    {
      get
      {
        return this.hasType;
      }
    }

    public MessageWrapper.Types.MessageType Type
    {
      get
      {
        return this.type_;
      }
    }

    public bool HasMessageId
    {
      get
      {
        return this.hasMessageId;
      }
    }

    public string MessageId
    {
      get
      {
        return this.messageId_;
      }
    }

    public bool HasMessage
    {
      get
      {
        return this.hasMessage;
      }
    }

    public LoggingEventMessage Message
    {
      get
      {
        return this.message_ ?? LoggingEventMessage.DefaultInstance;
      }
    }

    public bool HasStats
    {
      get
      {
        return this.hasStats;
      }
    }

    public StatisticsInformation Stats
    {
      get
      {
        return this.stats_ ?? StatisticsInformation.DefaultInstance;
      }
    }

    public bool HasApplication
    {
      get
      {
        return this.hasApplication;
      }
    }

    public ApplicationInformation Application
    {
      get
      {
        return this.application_ ?? ApplicationInformation.DefaultInstance;
      }
    }

    public bool HasContextFile
    {
      get
      {
        return this.hasContextFile;
      }
    }

    public string ContextFile
    {
      get
      {
        return this.contextFile_;
      }
    }

    public override bool IsInitialized
    {
      get
      {
        return this.hasType && this.hasMessageId && (!this.HasMessage || this.Message.IsInitialized) && ((!this.HasStats || this.Stats.IsInitialized) && (!this.HasApplication || this.Application.IsInitialized));
      }
    }

    public override void WriteTo(ICodedOutputStream output)
    {
      int serializedSize = this.SerializedSize;
      string[] wrapperFieldNames = MessageWrapper._messageWrapperFieldNames;
      if (this.hasType)
        output.WriteEnum(1, wrapperFieldNames[5], (int) this.Type, (object) this.Type);
      if (this.hasMessage)
        output.WriteMessage(2, wrapperFieldNames[2], (IMessageLite) this.Message);
      if (this.hasStats)
        output.WriteMessage(3, wrapperFieldNames[4], (IMessageLite) this.Stats);
      if (this.hasMessageId)
        output.WriteString(4, wrapperFieldNames[3], this.MessageId);
      if (this.hasApplication)
        output.WriteMessage(5, wrapperFieldNames[0], (IMessageLite) this.Application);
      if (this.hasContextFile)
        output.WriteString(6, wrapperFieldNames[1], this.ContextFile);
      this.UnknownFields.WriteTo(output);
    }

    public override int SerializedSize
    {
      get
      {
        int memoizedSerializedSize = this.memoizedSerializedSize;
        if (memoizedSerializedSize != -1)
          return memoizedSerializedSize;
        int num1 = 0;
        if (this.hasType)
          num1 += CodedOutputStream.ComputeEnumSize(1, (int) this.Type);
        if (this.hasMessageId)
          num1 += CodedOutputStream.ComputeStringSize(4, this.MessageId);
        if (this.hasMessage)
          num1 += CodedOutputStream.ComputeMessageSize(2, (IMessageLite) this.Message);
        if (this.hasStats)
          num1 += CodedOutputStream.ComputeMessageSize(3, (IMessageLite) this.Stats);
        if (this.hasApplication)
          num1 += CodedOutputStream.ComputeMessageSize(5, (IMessageLite) this.Application);
        if (this.hasContextFile)
          num1 += CodedOutputStream.ComputeStringSize(6, this.ContextFile);
        int num2 = num1 + this.UnknownFields.SerializedSize;
        this.memoizedSerializedSize = num2;
        return num2;
      }
    }

    public static MessageWrapper ParseFrom(ByteString data)
    {
      return MessageWrapper.CreateBuilder().MergeFrom(data).BuildParsed();
    }

    public static MessageWrapper ParseFrom(ByteString data, ExtensionRegistry extensionRegistry)
    {
      return MessageWrapper.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
    }

    public static MessageWrapper ParseFrom(byte[] data)
    {
      return MessageWrapper.CreateBuilder().MergeFrom(data).BuildParsed();
    }

    public static MessageWrapper ParseFrom(byte[] data, ExtensionRegistry extensionRegistry)
    {
      return MessageWrapper.CreateBuilder().MergeFrom(data, extensionRegistry).BuildParsed();
    }

    public static MessageWrapper ParseFrom(Stream input)
    {
      return MessageWrapper.CreateBuilder().MergeFrom(input).BuildParsed();
    }

    public static MessageWrapper ParseFrom(Stream input, ExtensionRegistry extensionRegistry)
    {
      return MessageWrapper.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
    }

    public static MessageWrapper ParseDelimitedFrom(Stream input)
    {
      return MessageWrapper.CreateBuilder().MergeDelimitedFrom(input).BuildParsed();
    }

    public static MessageWrapper ParseDelimitedFrom(Stream input, ExtensionRegistry extensionRegistry)
    {
      return MessageWrapper.CreateBuilder().MergeDelimitedFrom(input, extensionRegistry).BuildParsed();
    }

    public static MessageWrapper ParseFrom(ICodedInputStream input)
    {
      return MessageWrapper.CreateBuilder().MergeFrom(input).BuildParsed();
    }

    public static MessageWrapper ParseFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
    {
      return MessageWrapper.CreateBuilder().MergeFrom(input, extensionRegistry).BuildParsed();
    }

    private MessageWrapper MakeReadOnly()
    {
      return this;
    }

    public static MessageWrapper.Builder CreateBuilder()
    {
      return new MessageWrapper.Builder();
    }

    public override MessageWrapper.Builder ToBuilder()
    {
      return MessageWrapper.CreateBuilder(this);
    }

    public override MessageWrapper.Builder CreateBuilderForType()
    {
      return new MessageWrapper.Builder();
    }

    public static MessageWrapper.Builder CreateBuilder(MessageWrapper prototype)
    {
      return new MessageWrapper.Builder(prototype);
    }

    static MessageWrapper()
    {
      object.ReferenceEquals((object) HibernatingRhinosProfilerMessages.Descriptor, (object) null);
    }

    [DebuggerNonUserCode]
    public static class Types
    {
      public enum MessageType
      {
        LogEvent = 1,
        Statistics = 2,
        BringApplicationToFront = 3,
        ShutdownAndOutputReport = 4,
        ApplicationAttached = 5,
      }
    }

    [DebuggerNonUserCode]
    public sealed class Builder : GeneratedBuilder<MessageWrapper, MessageWrapper.Builder>
    {
      private bool resultIsReadOnly;
      private MessageWrapper result;

      protected override MessageWrapper.Builder ThisBuilder
      {
        get
        {
          return this;
        }
      }

      public Builder()
      {
        this.result = MessageWrapper.DefaultInstance;
        this.resultIsReadOnly = true;
      }

      internal Builder(MessageWrapper cloneFrom)
      {
        this.result = cloneFrom;
        this.resultIsReadOnly = true;
      }

      private MessageWrapper PrepareBuilder()
      {
        if (this.resultIsReadOnly)
        {
          MessageWrapper result = this.result;
          this.result = new MessageWrapper();
          this.resultIsReadOnly = false;
          this.MergeFrom(result);
        }
        return this.result;
      }

      public override bool IsInitialized
      {
        get
        {
          return this.result.IsInitialized;
        }
      }

      protected override MessageWrapper MessageBeingBuilt
      {
        get
        {
          return this.PrepareBuilder();
        }
      }

      public override MessageWrapper.Builder Clear()
      {
        this.result = MessageWrapper.DefaultInstance;
        this.resultIsReadOnly = true;
        return this;
      }

      public override MessageWrapper.Builder Clone()
      {
        if (this.resultIsReadOnly)
          return new MessageWrapper.Builder(this.result);
        return new MessageWrapper.Builder().MergeFrom(this.result);
      }

      public override MessageDescriptor DescriptorForType
      {
        get
        {
          return MessageWrapper.Descriptor;
        }
      }

      public override MessageWrapper DefaultInstanceForType
      {
        get
        {
          return MessageWrapper.DefaultInstance;
        }
      }

      public override MessageWrapper BuildPartial()
      {
        if (this.resultIsReadOnly)
          return this.result;
        this.resultIsReadOnly = true;
        return this.result.MakeReadOnly();
      }

      public override MessageWrapper.Builder MergeFrom(IMessage other)
      {
        if (other is MessageWrapper)
          return this.MergeFrom((MessageWrapper) other);
        base.MergeFrom(other);
        return this;
      }

      public override MessageWrapper.Builder MergeFrom(MessageWrapper other)
      {
        if (other == MessageWrapper.DefaultInstance)
          return this;
        this.PrepareBuilder();
        if (other.HasType)
          this.Type = other.Type;
        if (other.HasMessageId)
          this.MessageId = other.MessageId;
        if (other.HasMessage)
          this.MergeMessage(other.Message);
        if (other.HasStats)
          this.MergeStats(other.Stats);
        if (other.HasApplication)
          this.MergeApplication(other.Application);
        if (other.HasContextFile)
          this.ContextFile = other.ContextFile;
        this.MergeUnknownFields(other.UnknownFields);
        return this;
      }

      public override MessageWrapper.Builder MergeFrom(ICodedInputStream input)
      {
        return this.MergeFrom(input, ExtensionRegistry.Empty);
      }

      public override MessageWrapper.Builder MergeFrom(ICodedInputStream input, ExtensionRegistry extensionRegistry)
      {
        this.PrepareBuilder();
        UnknownFieldSet.Builder unknownFields = (UnknownFieldSet.Builder) null;
        uint fieldTag;
        string fieldName;
        while (input.ReadTag(out fieldTag, out fieldName))
        {
          if ((int) fieldTag == 0 && fieldName != null)
          {
            int index = Array.BinarySearch<string>(MessageWrapper._messageWrapperFieldNames, fieldName, (IComparer<string>) StringComparer.Ordinal);
            if (index >= 0)
            {
              fieldTag = MessageWrapper._messageWrapperFieldTags[index];
            }
            else
            {
              if (unknownFields == null)
                unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
              this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
              continue;
            }
          }
          switch (fieldTag)
          {
            case 0:
              throw InvalidProtocolBufferException.InvalidTag();
            case 8:
              object unknown;
              if (input.ReadEnum<MessageWrapper.Types.MessageType>(ref this.result.type_, out unknown))
              {
                this.result.hasType = true;
                continue;
              }
              if (unknown is int)
              {
                if (unknownFields == null)
                  unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
                unknownFields.MergeVarintField(1, (ulong) (int) unknown);
                continue;
              }
              continue;
            case 18:
              LoggingEventMessage.Builder builder1 = LoggingEventMessage.CreateBuilder();
              if (this.result.hasMessage)
                builder1.MergeFrom(this.Message);
              input.ReadMessage((IBuilderLite) builder1, extensionRegistry);
              this.Message = builder1.BuildPartial();
              continue;
            case 26:
              StatisticsInformation.Builder builder2 = StatisticsInformation.CreateBuilder();
              if (this.result.hasStats)
                builder2.MergeFrom(this.Stats);
              input.ReadMessage((IBuilderLite) builder2, extensionRegistry);
              this.Stats = builder2.BuildPartial();
              continue;
            case 34:
              this.result.hasMessageId = input.ReadString(ref this.result.messageId_);
              continue;
            case 42:
              ApplicationInformation.Builder builder3 = ApplicationInformation.CreateBuilder();
              if (this.result.hasApplication)
                builder3.MergeFrom(this.Application);
              input.ReadMessage((IBuilderLite) builder3, extensionRegistry);
              this.Application = builder3.BuildPartial();
              continue;
            case 50:
              this.result.hasContextFile = input.ReadString(ref this.result.contextFile_);
              continue;
            default:
              if (WireFormat.IsEndGroupTag(fieldTag))
              {
                if (unknownFields != null)
                  this.UnknownFields = unknownFields.Build();
                return this;
              }
              if (unknownFields == null)
                unknownFields = UnknownFieldSet.CreateBuilder(this.UnknownFields);
              this.ParseUnknownField(input, unknownFields, extensionRegistry, fieldTag, fieldName);
              continue;
          }
        }
        if (unknownFields != null)
          this.UnknownFields = unknownFields.Build();
        return this;
      }

      public bool HasType
      {
        get
        {
          return this.result.hasType;
        }
      }

      public MessageWrapper.Types.MessageType Type
      {
        get
        {
          return this.result.Type;
        }
        set
        {
          this.SetType(value);
        }
      }

      public MessageWrapper.Builder SetType(MessageWrapper.Types.MessageType value)
      {
        this.PrepareBuilder();
        this.result.hasType = true;
        this.result.type_ = value;
        return this;
      }

      public MessageWrapper.Builder ClearType()
      {
        this.PrepareBuilder();
        this.result.hasType = false;
        this.result.type_ = MessageWrapper.Types.MessageType.LogEvent;
        return this;
      }

      public bool HasMessageId
      {
        get
        {
          return this.result.hasMessageId;
        }
      }

      public string MessageId
      {
        get
        {
          return this.result.MessageId;
        }
        set
        {
          this.SetMessageId(value);
        }
      }

      public MessageWrapper.Builder SetMessageId(string value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.hasMessageId = true;
        this.result.messageId_ = value;
        return this;
      }

      public MessageWrapper.Builder ClearMessageId()
      {
        this.PrepareBuilder();
        this.result.hasMessageId = false;
        this.result.messageId_ = "";
        return this;
      }

      public bool HasMessage
      {
        get
        {
          return this.result.hasMessage;
        }
      }

      public LoggingEventMessage Message
      {
        get
        {
          return this.result.Message;
        }
        set
        {
          this.SetMessage(value);
        }
      }

      public MessageWrapper.Builder SetMessage(LoggingEventMessage value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.hasMessage = true;
        this.result.message_ = value;
        return this;
      }

      public MessageWrapper.Builder SetMessage(LoggingEventMessage.Builder builderForValue)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
        this.PrepareBuilder();
        this.result.hasMessage = true;
        this.result.message_ = builderForValue.Build();
        return this;
      }

      public MessageWrapper.Builder MergeMessage(LoggingEventMessage value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.message_ = !this.result.hasMessage || this.result.message_ == LoggingEventMessage.DefaultInstance ? value : LoggingEventMessage.CreateBuilder(this.result.message_).MergeFrom(value).BuildPartial();
        this.result.hasMessage = true;
        return this;
      }

      public MessageWrapper.Builder ClearMessage()
      {
        this.PrepareBuilder();
        this.result.hasMessage = false;
        this.result.message_ = (LoggingEventMessage) null;
        return this;
      }

      public bool HasStats
      {
        get
        {
          return this.result.hasStats;
        }
      }

      public StatisticsInformation Stats
      {
        get
        {
          return this.result.Stats;
        }
        set
        {
          this.SetStats(value);
        }
      }

      public MessageWrapper.Builder SetStats(StatisticsInformation value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.hasStats = true;
        this.result.stats_ = value;
        return this;
      }

      public MessageWrapper.Builder SetStats(StatisticsInformation.Builder builderForValue)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
        this.PrepareBuilder();
        this.result.hasStats = true;
        this.result.stats_ = builderForValue.Build();
        return this;
      }

      public MessageWrapper.Builder MergeStats(StatisticsInformation value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.stats_ = !this.result.hasStats || this.result.stats_ == StatisticsInformation.DefaultInstance ? value : StatisticsInformation.CreateBuilder(this.result.stats_).MergeFrom(value).BuildPartial();
        this.result.hasStats = true;
        return this;
      }

      public MessageWrapper.Builder ClearStats()
      {
        this.PrepareBuilder();
        this.result.hasStats = false;
        this.result.stats_ = (StatisticsInformation) null;
        return this;
      }

      public bool HasApplication
      {
        get
        {
          return this.result.hasApplication;
        }
      }

      public ApplicationInformation Application
      {
        get
        {
          return this.result.Application;
        }
        set
        {
          this.SetApplication(value);
        }
      }

      public MessageWrapper.Builder SetApplication(ApplicationInformation value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.hasApplication = true;
        this.result.application_ = value;
        return this;
      }

      public MessageWrapper.Builder SetApplication(ApplicationInformation.Builder builderForValue)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) builderForValue, nameof (builderForValue));
        this.PrepareBuilder();
        this.result.hasApplication = true;
        this.result.application_ = builderForValue.Build();
        return this;
      }

      public MessageWrapper.Builder MergeApplication(ApplicationInformation value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.application_ = !this.result.hasApplication || this.result.application_ == ApplicationInformation.DefaultInstance ? value : ApplicationInformation.CreateBuilder(this.result.application_).MergeFrom(value).BuildPartial();
        this.result.hasApplication = true;
        return this;
      }

      public MessageWrapper.Builder ClearApplication()
      {
        this.PrepareBuilder();
        this.result.hasApplication = false;
        this.result.application_ = (ApplicationInformation) null;
        return this;
      }

      public bool HasContextFile
      {
        get
        {
          return this.result.hasContextFile;
        }
      }

      public string ContextFile
      {
        get
        {
          return this.result.ContextFile;
        }
        set
        {
          this.SetContextFile(value);
        }
      }

      public MessageWrapper.Builder SetContextFile(string value)
      {
        Google.ProtocolBuffers.ThrowHelper.ThrowIfNull((object) value, nameof (value));
        this.PrepareBuilder();
        this.result.hasContextFile = true;
        this.result.contextFile_ = value;
        return this;
      }

      public MessageWrapper.Builder ClearContextFile()
      {
        this.PrepareBuilder();
        this.result.hasContextFile = false;
        this.result.contextFile_ = "";
        return this;
      }
    }
  }
}
