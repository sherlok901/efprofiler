﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Appender.Messages.HibernatingRhinosProfilerMessages
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using Google.ProtocolBuffers;
using Google.ProtocolBuffers.Descriptors;
using Google.ProtocolBuffers.FieldAccess;
using System;
using System.Diagnostics;

namespace HibernatingRhinos.Profiler.Appender.Messages
{
  [DebuggerNonUserCode]
  public static class HibernatingRhinosProfilerMessages
  {
    internal static MessageDescriptor internal__static_HibernatingRhinos_Profiler_Appender_Messages_LoggingEventMessage__Descriptor;
    internal static FieldAccessorTable<LoggingEventMessage, LoggingEventMessage.Builder> internal__static_HibernatingRhinos_Profiler_Appender_Messages_LoggingEventMessage__FieldAccessorTable;
    internal static MessageDescriptor internal__static_HibernatingRhinos_Profiler_Appender_Messages_LoggingEventMessage_StackTraceFrame__Descriptor;
    internal static FieldAccessorTable<LoggingEventMessage.Types.StackTraceFrame, LoggingEventMessage.Types.StackTraceFrame.Builder> internal__static_HibernatingRhinos_Profiler_Appender_Messages_LoggingEventMessage_StackTraceFrame__FieldAccessorTable;
    internal static MessageDescriptor internal__static_HibernatingRhinos_Profiler_Appender_Messages_LoggingEventMessage_StackTraceInfo__Descriptor;
    internal static FieldAccessorTable<LoggingEventMessage.Types.StackTraceInfo, LoggingEventMessage.Types.StackTraceInfo.Builder> internal__static_HibernatingRhinos_Profiler_Appender_Messages_LoggingEventMessage_StackTraceInfo__FieldAccessorTable;
    internal static MessageDescriptor internal__static_HibernatingRhinos_Profiler_Appender_Messages_StatisticsInformation__Descriptor;
    internal static FieldAccessorTable<StatisticsInformation, StatisticsInformation.Builder> internal__static_HibernatingRhinos_Profiler_Appender_Messages_StatisticsInformation__FieldAccessorTable;
    internal static MessageDescriptor internal__static_HibernatingRhinos_Profiler_Appender_Messages_StatisticsInformation_KeyValue__Descriptor;
    internal static FieldAccessorTable<StatisticsInformation.Types.KeyValue, StatisticsInformation.Types.KeyValue.Builder> internal__static_HibernatingRhinos_Profiler_Appender_Messages_StatisticsInformation_KeyValue__FieldAccessorTable;
    internal static MessageDescriptor internal__static_HibernatingRhinos_Profiler_Appender_Messages_ApplicationInformation__Descriptor;
    internal static FieldAccessorTable<ApplicationInformation, ApplicationInformation.Builder> internal__static_HibernatingRhinos_Profiler_Appender_Messages_ApplicationInformation__FieldAccessorTable;
    internal static MessageDescriptor internal__static_HibernatingRhinos_Profiler_Appender_Messages_MessageWrapper__Descriptor;
    internal static FieldAccessorTable<MessageWrapper, MessageWrapper.Builder> internal__static_HibernatingRhinos_Profiler_Appender_Messages_MessageWrapper__FieldAccessorTable;
    private static FileDescriptor descriptor;

    public static void RegisterAllExtensions(ExtensionRegistry registry)
    {
    }

    public static FileDescriptor Descriptor
    {
      get
      {
        return HibernatingRhinosProfilerMessages.descriptor;
      }
    }

    static HibernatingRhinosProfilerMessages()
    {
      FileDescriptor.InternalBuildGeneratedFileFrom(Convert.FromBase64String("CilIaWJlcm5hdGluZ1JoaW5vcy5Qcm9maWxlci5NZXNzYWdlcy5wcm90bxIs" + "SGliZXJuYXRpbmdSaGlub3MuUHJvZmlsZXIuQXBwZW5kZXIuTWVzc2FnZXMi" + "6AQKE0xvZ2dpbmdFdmVudE1lc3NhZ2USDwoHTWVzc2FnZRgBIAIoCRIOCgZM" + "b2dnZXIYAiACKAkSKAogRGF0ZUFzTWlsbGlzZWNvbmRzU2luY2VVbml4RXBv" + "Y2gYAyACKAMSEQoJU2Vzc2lvbklkGAQgAigJEgsKA1VybBgFIAIoCRINCgVM" + "ZXZlbBgHIAIoBRIVCg1UaHJlYWRDb250ZXh0GAggAigJEhAKCFRocmVhZElk" + "GAogASgJEmQKClN0YWNrVHJhY2UYBiABKAsyUC5IaWJlcm5hdGluZ1JoaW5v" + "cy5Qcm9maWxlci5BcHBlbmRlci5NZXNzYWdlcy5Mb2dnaW5nRXZlbnRNZXNz" + "YWdlLlN0YWNrVHJhY2VJbmZvEhEKCUV4Y2VwdGlvbhgJIAEoCRIRCglEYkRp" + "YWxlY3QYCyABKAkSDwoHQ3VsdHVyZRgMIAEoCRIRCglTdGFyQ29sb3IYDSAB" + "KAkSEQoJU2NvcGVOYW1lGA4gASgJGnYKD1N0YWNrVHJhY2VGcmFtZRIMCgRU" + "eXBlGAEgAigJEgwKBExpbmUYAiACKAUSDgoGQ29sdW1uGAMgAigFEg4KBk1l" + "dGhvZBgEIAIoCRIRCglOYW1lc3BhY2UYBSACKAkSFAoMRnVsbEZpbGVuYW1l" + "GAYgASgJGnMKDlN0YWNrVHJhY2VJbmZvEmEKBkZyYW1lcxgBIAMoCzJRLkhp" + "YmVybmF0aW5nUmhpbm9zLlByb2ZpbGVyLkFwcGVuZGVyLk1lc3NhZ2VzLkxv" + "Z2dpbmdFdmVudE1lc3NhZ2UuU3RhY2tUcmFjZUZyYW1lIqoBChVTdGF0aXN0" + "aWNzSW5mb3JtYXRpb24SDAoETmFtZRgBIAIoCRJbCgVJdGVtcxgCIAMoCzJM" + "LkhpYmVybmF0aW5nUmhpbm9zLlByb2ZpbGVyLkFwcGVuZGVyLk1lc3NhZ2Vz" + "LlN0YXRpc3RpY3NJbmZvcm1hdGlvbi5LZXlWYWx1ZRomCghLZXlWYWx1ZRIL" + "CgNLZXkYASACKAkSDQoFVmFsdWUYAiADKAkiMgoWQXBwbGljYXRpb25JbmZv" + "cm1hdGlvbhIKCgJJZBgBIAIoCRIMCgROYW1lGAIgAigJIpMECg5NZXNzYWdl" + "V3JhcHBlchJWCgRUeXBlGAEgAigOMkguSGliZXJuYXRpbmdSaGlub3MuUHJv" + "ZmlsZXIuQXBwZW5kZXIuTWVzc2FnZXMuTWVzc2FnZVdyYXBwZXIuTWVzc2Fn" + "ZVR5cGUSEQoJTWVzc2FnZUlkGAQgAigJElIKB01lc3NhZ2UYAiABKAsyQS5I" + "aWJlcm5hdGluZ1JoaW5vcy5Qcm9maWxlci5BcHBlbmRlci5NZXNzYWdlcy5M" + "b2dnaW5nRXZlbnRNZXNzYWdlElIKBVN0YXRzGAMgASgLMkMuSGliZXJuYXRp" + "bmdSaGlub3MuUHJvZmlsZXIuQXBwZW5kZXIuTWVzc2FnZXMuU3RhdGlzdGlj" + "c0luZm9ybWF0aW9uElkKC0FwcGxpY2F0aW9uGAUgASgLMkQuSGliZXJuYXRp" + "bmdSaGlub3MuUHJvZmlsZXIuQXBwZW5kZXIuTWVzc2FnZXMuQXBwbGljYXRp" + "b25JbmZvcm1hdGlvbhITCgtDb250ZXh0RmlsZRgGIAEoCSJ+CgtNZXNzYWdl" + "VHlwZRIMCghMb2dFdmVudBABEg4KClN0YXRpc3RpY3MQAhIbChdCcmluZ0Fw" + "cGxpY2F0aW9uVG9Gcm9udBADEhsKF1NodXRkb3duQW5kT3V0cHV0UmVwb3J0" + "EAQSFwoTQXBwbGljYXRpb25BdHRhY2hlZBAFQkgKM2hpYmVybmF0aW5ncmhp" + "bm9zLmhpYmVybmF0ZS5wcm9maWxlci5tZXNzYWdlcy5wcm90b0IPUHJvZmls" + "ZXJNZXNzYWdlSAE="), new FileDescriptor[0], (FileDescriptor.InternalDescriptorAssigner) (root =>
      {
        HibernatingRhinosProfilerMessages.descriptor = root;
        HibernatingRhinosProfilerMessages.internal__static_HibernatingRhinos_Profiler_Appender_Messages_LoggingEventMessage__Descriptor = HibernatingRhinosProfilerMessages.Descriptor.MessageTypes[0];
        HibernatingRhinosProfilerMessages.internal__static_HibernatingRhinos_Profiler_Appender_Messages_LoggingEventMessage__FieldAccessorTable = new FieldAccessorTable<LoggingEventMessage, LoggingEventMessage.Builder>(HibernatingRhinosProfilerMessages.internal__static_HibernatingRhinos_Profiler_Appender_Messages_LoggingEventMessage__Descriptor, new string[14]
        {
          "Message",
          "Logger",
          "DateAsMillisecondsSinceUnixEpoch",
          "SessionId",
          "Url",
          "Level",
          "ThreadContext",
          "ThreadId",
          "StackTrace",
          "Exception",
          "DbDialect",
          "Culture",
          "StarColor",
          "ScopeName"
        });
        HibernatingRhinosProfilerMessages.internal__static_HibernatingRhinos_Profiler_Appender_Messages_LoggingEventMessage_StackTraceFrame__Descriptor = HibernatingRhinosProfilerMessages.internal__static_HibernatingRhinos_Profiler_Appender_Messages_LoggingEventMessage__Descriptor.NestedTypes[0];
        HibernatingRhinosProfilerMessages.internal__static_HibernatingRhinos_Profiler_Appender_Messages_LoggingEventMessage_StackTraceFrame__FieldAccessorTable = new FieldAccessorTable<LoggingEventMessage.Types.StackTraceFrame, LoggingEventMessage.Types.StackTraceFrame.Builder>(HibernatingRhinosProfilerMessages.internal__static_HibernatingRhinos_Profiler_Appender_Messages_LoggingEventMessage_StackTraceFrame__Descriptor, new string[6]
        {
          "Type",
          "Line",
          "Column",
          "Method",
          "Namespace",
          "FullFilename"
        });
        HibernatingRhinosProfilerMessages.internal__static_HibernatingRhinos_Profiler_Appender_Messages_LoggingEventMessage_StackTraceInfo__Descriptor = HibernatingRhinosProfilerMessages.internal__static_HibernatingRhinos_Profiler_Appender_Messages_LoggingEventMessage__Descriptor.NestedTypes[1];
        HibernatingRhinosProfilerMessages.internal__static_HibernatingRhinos_Profiler_Appender_Messages_LoggingEventMessage_StackTraceInfo__FieldAccessorTable = new FieldAccessorTable<LoggingEventMessage.Types.StackTraceInfo, LoggingEventMessage.Types.StackTraceInfo.Builder>(HibernatingRhinosProfilerMessages.internal__static_HibernatingRhinos_Profiler_Appender_Messages_LoggingEventMessage_StackTraceInfo__Descriptor, new string[1]
        {
          "Frames"
        });
        HibernatingRhinosProfilerMessages.internal__static_HibernatingRhinos_Profiler_Appender_Messages_StatisticsInformation__Descriptor = HibernatingRhinosProfilerMessages.Descriptor.MessageTypes[1];
        HibernatingRhinosProfilerMessages.internal__static_HibernatingRhinos_Profiler_Appender_Messages_StatisticsInformation__FieldAccessorTable = new FieldAccessorTable<StatisticsInformation, StatisticsInformation.Builder>(HibernatingRhinosProfilerMessages.internal__static_HibernatingRhinos_Profiler_Appender_Messages_StatisticsInformation__Descriptor, new string[2]
        {
          "Name",
          "Items"
        });
        HibernatingRhinosProfilerMessages.internal__static_HibernatingRhinos_Profiler_Appender_Messages_StatisticsInformation_KeyValue__Descriptor = HibernatingRhinosProfilerMessages.internal__static_HibernatingRhinos_Profiler_Appender_Messages_StatisticsInformation__Descriptor.NestedTypes[0];
        HibernatingRhinosProfilerMessages.internal__static_HibernatingRhinos_Profiler_Appender_Messages_StatisticsInformation_KeyValue__FieldAccessorTable = new FieldAccessorTable<StatisticsInformation.Types.KeyValue, StatisticsInformation.Types.KeyValue.Builder>(HibernatingRhinosProfilerMessages.internal__static_HibernatingRhinos_Profiler_Appender_Messages_StatisticsInformation_KeyValue__Descriptor, new string[2]
        {
          "Key",
          "Value"
        });
        HibernatingRhinosProfilerMessages.internal__static_HibernatingRhinos_Profiler_Appender_Messages_ApplicationInformation__Descriptor = HibernatingRhinosProfilerMessages.Descriptor.MessageTypes[2];
        HibernatingRhinosProfilerMessages.internal__static_HibernatingRhinos_Profiler_Appender_Messages_ApplicationInformation__FieldAccessorTable = new FieldAccessorTable<ApplicationInformation, ApplicationInformation.Builder>(HibernatingRhinosProfilerMessages.internal__static_HibernatingRhinos_Profiler_Appender_Messages_ApplicationInformation__Descriptor, new string[2]
        {
          "Id",
          "Name"
        });
        HibernatingRhinosProfilerMessages.internal__static_HibernatingRhinos_Profiler_Appender_Messages_MessageWrapper__Descriptor = HibernatingRhinosProfilerMessages.Descriptor.MessageTypes[3];
        HibernatingRhinosProfilerMessages.internal__static_HibernatingRhinos_Profiler_Appender_Messages_MessageWrapper__FieldAccessorTable = new FieldAccessorTable<MessageWrapper, MessageWrapper.Builder>(HibernatingRhinosProfilerMessages.internal__static_HibernatingRhinos_Profiler_Appender_Messages_MessageWrapper__Descriptor, new string[6]
        {
          "Type",
          "MessageId",
          "Message",
          "Stats",
          "Application",
          "ContextFile"
        });
        return (ExtensionRegistry) null;
      }));
    }
  }
}
