//-----------------------------------------------------------------------
// <copyright file="ProfiledDbProviderServices.cs" company="Hibernating Rhinos LTD">
//     Copyright (c) Hibernating Rhinos LTD. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
using System;
using System.Data.Common;
using System.Diagnostics;
using System.Reflection;
using HibernatingRhinos.Profiler.Appender.ProfiledDataAccess;

#if !EF6
using System.Data.Metadata.Edm;
using System.Data.Common.CommandTrees;
#else
using System.Collections.Generic;
using System.Data.Entity.Core.Common;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Core.Common.CommandTrees;
#endif

#if NET45
#if !EF6
using System.Data.Spatial;
#else
using System.Data.Entity.Spatial;
#endif
#endif

namespace HibernatingRhinos.Profiler.Appender.EntityFramework
{
#if DEBUG
	[CLSCompliant(false)]
#endif
	public class ProfiledDbProviderServices : DbProviderServices
    {
        private readonly DbProviderServices inner;
        private readonly IDbAppender appender;

		public ProfiledDbProviderServices(DbProviderServices inner, IDbAppender appender)
        {
            this.inner = inner;
            this.appender = appender;
        }

#if EF6
		public override void RegisterInfoMessageHandler(DbConnection connection, Action<string> handler)
		{
			inner.RegisterInfoMessageHandler(connection, handler);
		}

		public override IEnumerable<object> GetServices(Type type, object key)
		{
			return inner.GetServices(type, key);
		}
#endif

#if NET40
		protected override bool DbDatabaseExists(DbConnection connection, int? commandTimeout, StoreItemCollection storeItemCollection)
		{
			return inner.DatabaseExists(((ProfiledConnection)connection).Inner, commandTimeout, storeItemCollection);
		}

		protected override void DbCreateDatabase(DbConnection connection, int? commandTimeout, StoreItemCollection storeItemCollection)
		{
			inner.CreateDatabase(((ProfiledConnection)connection).Inner, commandTimeout, storeItemCollection);
		}

		protected override string DbCreateDatabaseScript(string providerManifestToken, StoreItemCollection storeItemCollection)
		{
			return inner.CreateDatabaseScript(providerManifestToken, storeItemCollection);
		}

		protected override void DbDeleteDatabase(DbConnection connection, int? commandTimeout, StoreItemCollection storeItemCollection)
		{
			inner.DeleteDatabase(((ProfiledConnection)connection).Inner, commandTimeout, storeItemCollection);
		}
#endif

#if NET45
		protected override DbSpatialDataReader GetDbSpatialDataReader(DbDataReader fromReader, string manifestToken)
		{
			var type = inner.GetType();
			var methodInfo = type.GetMethod("GetDbSpatialDataReader", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic, System.Type.DefaultBinder, new[] {typeof (DbDataReader), typeof (string)}, null);
			try
			{
				var profiledDataReader = fromReader as ProfiledDataReader;
				if (profiledDataReader != null)
					fromReader = profiledDataReader.Inner;
				var reader = (DbSpatialDataReader)methodInfo.Invoke(inner, new object[] { fromReader, manifestToken });
				return reader;
			}
			catch (System.Exception e)
			{
				System.Diagnostics.Trace.WriteLine(e);
				throw;
			}
		}

#if EF6
		[Obsolete]
#endif
		protected override DbSpatialServices DbGetSpatialServices(string manifestToken)
	    {
			return inner.GetSpatialServices(manifestToken);
	    }

	    protected override void SetDbParameterValue(DbParameter parameter, TypeUsage parameterType, object value)
	    {
			var type = inner.GetType();
			var methodInfo = type.GetMethod("SetDbParameterValue", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic, System.Type.DefaultBinder, new[] { typeof(DbParameter), typeof(TypeUsage), typeof(object) }, null);
			try
			{
				methodInfo.Invoke(inner, new[] {parameter, parameterType, value});
			}
			catch (System.Exception e)
			{
				System.Diagnostics.Trace.WriteLine(e);
				throw;
			}
	    }
#endif
		
		protected override DbCommandDefinition CreateDbCommandDefinition(DbProviderManifest providerManifest, DbCommandTree commandTree)
        {
#if NET40 || NET45
            var definition = inner.CreateCommandDefinition(providerManifest, commandTree);
#else
            var definition = inner.CreateCommandDefinition(commandTree);
#endif
            return new ProfiledDbCommandDefinition(definition, appender);
        }

        protected override string GetDbProviderManifestToken(DbConnection connection)
        {
#if EF6
	        try
	        {
				return inner.GetProviderManifestToken(connection);
	        }
	        catch (Exception ef6Exception)
	        {
		        try
		        {
					return inner.GetProviderManifestToken(((ProfiledConnection)connection).Inner);
		        }
		        catch (Exception e)
		        {
			        throw new InvalidOperationException(string.Format("Got exception. See exception 1: {0}. \n\rSee exception 2: {1}", ef6Exception, e));
		        }
	        }
#else
			return inner.GetProviderManifestToken(((ProfiledConnection)connection).Inner);
#endif
		}

        protected override DbProviderManifest GetDbProviderManifest(string manifestToken)
        {
            return inner.GetProviderManifest(manifestToken);
        }

        public override DbCommandDefinition CreateCommandDefinition(DbCommand prototype)
        {
            var definition = inner.CreateCommandDefinition(prototype);
            return new ProfiledDbCommandDefinition(definition, appender);
        }

#if EF6
		public override object GetService(Type type, object key)
		{
			return inner.GetService(type, key);
		}
#endif
	}
}