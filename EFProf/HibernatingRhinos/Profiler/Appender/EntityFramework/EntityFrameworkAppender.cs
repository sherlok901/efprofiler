﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Appender.EntityFramework.EntityFrameworkAppender
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using HibernatingRhinos.Profiler.Appender.ProfiledDataAccess;
using System;
using System.Collections;
using System.Threading;
using System.Transactions;

namespace HibernatingRhinos.Profiler.Appender.EntityFramework
{
  public class EntityFrameworkAppender : IStatisticsSource, IDbAppender
  {
    private readonly string name;
    private int connectionOpened;
    private int connectionClosed;
    private int queriesExecuted;
    private int transactionsBegan;
    private int transactionsCommitted;
    private int transactionsRollbacked;
    private int transactionsDisposed;
    [ThreadStatic]
    private static string localDtcTransactionId;

    public string Name
    {
      get
      {
        return this.name;
      }
    }

    public EntityFrameworkAppender(string name)
    {
      this.name = name;
    }

    public void ResetStatistics()
    {
      this.connectionOpened = 0;
      this.connectionClosed = 0;
      this.queriesExecuted = 0;
      this.transactionsBegan = 0;
      this.transactionsRollbacked = 0;
      this.transactionsDisposed = 0;
      this.transactionsCommitted = 0;
    }

    public void ConnectionStarted(Guid connectionId)
    {
      Interlocked.Increment(ref this.connectionOpened);
      ProfilerIntegration.PublishProfilerEvent(connectionId.ToString(), "EntityFramework.Connection", "Connection opened");
      this.EnlistInDtcTransactionIfNeeded(connectionId);
    }

    public void ConnectionDisposed(Guid connectionId)
    {
      Interlocked.Increment(ref this.connectionClosed);
      ProfilerIntegration.PublishProfilerEvent(connectionId.ToString(), "EntityFramework.Connection", "Connection closed");
    }

    public void SessionConnectionString(Guid connectionId, string connectionString)
    {
      ProfilerIntegration.PublishProfilerEvent(connectionId.ToString(), "EntityFramework.SessionConnectionString", connectionString);
    }

    public void StatementExecuted(Guid connectionId, Guid statementId, string statement)
    {
      this.EnlistInDtcTransactionIfNeeded(connectionId);
      Interlocked.Increment(ref this.queriesExecuted);
      ProfilerIntegration.PublishProfilerEvent(connectionId.ToString(), "EntityFramework.Sql", "-- Statement " + (object) statementId + Environment.NewLine + statement);
    }

    public SessionFactoryStats[] GetStatistics()
    {
      return new SessionFactoryStats[1]
      {
        new SessionFactoryStats()
        {
          Name = this.name,
          Statistics = new Hashtable()
          {
            {
              (object) "Object Contexts Opened",
              (object) this.connectionOpened
            },
            {
              (object) "Object Contexts Closed",
              (object) this.connectionClosed
            },
            {
              (object) "Queries Executed",
              (object) this.queriesExecuted
            },
            {
              (object) "Transactions began",
              (object) this.transactionsBegan
            },
            {
              (object) "Transactions committed",
              (object) this.transactionsCommitted
            },
            {
              (object) "Transactions rollbacked",
              (object) this.transactionsRollbacked
            },
            {
              (object) "Transactions disposed",
              (object) this.transactionsDisposed
            }
          }
        }
      };
    }

    public void CommandDurationAndRowCount(Guid connectionId, long milliseconds, int? rowCount)
    {
      ProfilerIntegration.PublishProfilerEvent(connectionId.ToString(), "EntityFramework.StatementStats", string.Format("Executed in {0} ms with {1} rows", (object) milliseconds, (object) rowCount));
    }

    public void StatementError(Guid connectionId, Exception exception)
    {
      ProfilerIntegration.PublishProfilerWarning(connectionId.ToString(), "EntityFramework.Error", exception.ToString());
    }

    public void StatementRowCount(Guid connectionId, Guid statementId, int rowCount)
    {
      ProfilerIntegration.PublishProfilerEvent(connectionId.ToString(), "EntityFramework.StatementRowCount", string.Format("Read {0} rows for {1}", (object) rowCount, (object) statementId));
    }

    public void TransactionBegan(Guid connectionId, System.Data.IsolationLevel isolationLevel)
    {
      Interlocked.Increment(ref this.transactionsBegan);
      ProfilerIntegration.PublishProfilerEvent(connectionId.ToString(), "EntityFramework.Transactions", "Transaction began: " + (object) isolationLevel);
    }

    public void TransactionCommit(Guid connectionId)
    {
      Interlocked.Increment(ref this.transactionsCommitted);
      ProfilerIntegration.PublishProfilerEvent(connectionId.ToString(), "EntityFramework.Transactions", "Transaction committed");
    }

    public void TransactionRolledBack(Guid connectionId)
    {
      Interlocked.Increment(ref this.transactionsRollbacked);
      ProfilerIntegration.PublishProfilerEvent(connectionId.ToString(), "EntityFramework.Transactions", "Transaction rollbacked");
    }

    public void TransactionDisposed(Guid connectionId)
    {
      Interlocked.Increment(ref this.transactionsDisposed);
      ProfilerIntegration.PublishProfilerEvent(connectionId.ToString(), "EntityFramework.Transactions", "Transaction disposed");
    }

    public void DtcTransactionEnlisted(Guid connectionId, System.Transactions.IsolationLevel isolationLevel)
    {
      Interlocked.Increment(ref this.transactionsBegan);
      ProfilerIntegration.PublishProfilerEvent(connectionId.ToString(), "EntityFramework.Dtc.Transactions", "Transaction enlisted: " + (object) isolationLevel);
    }

    public void DtcTransactionCompleted(Guid connectionId, TransactionStatus status)
    {
      switch (status)
      {
        case TransactionStatus.Committed:
          Interlocked.Increment(ref this.transactionsCommitted);
          break;
        case TransactionStatus.Aborted:
          Interlocked.Increment(ref this.transactionsRollbacked);
          break;
        case TransactionStatus.InDoubt:
          Interlocked.Increment(ref this.transactionsRollbacked);
          break;
      }
      ProfilerIntegration.PublishProfilerEvent(connectionId.ToString(), "EntityFramework.Dtc.Transactions", "Transaction completed: " + (object) status);
    }

    public bool ReportConnectionOnOpen
    {
      get
      {
        return true;
      }
    }

    public bool ReportConnectionDispose
    {
      get
      {
        return true;
      }
    }

    private void EnlistInDtcTransactionIfNeeded(Guid connectionId)
    {
      Transaction current = Transaction.Current;
      if (current == (Transaction) null)
        return;
      string localIdentifier = current.TransactionInformation.LocalIdentifier;
      if (localIdentifier == EntityFrameworkAppender.localDtcTransactionId)
        return;
      EntityFrameworkAppender.localDtcTransactionId = localIdentifier;
      this.DtcTransactionEnlisted(connectionId, current.IsolationLevel);
      current.TransactionCompleted += (TransactionCompletedEventHandler) ((sender, args) => this.DtcTransactionCompleted(connectionId, args.Transaction.TransactionInformation.Status));
    }
  }
}
