﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Appender.EntityFramework.EntityFrameworkProfilerInternal
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using System;
using System.Reflection;

namespace HibernatingRhinos.Profiler.Appender.EntityFramework
{
  public static class EntityFrameworkProfilerInternal
  {
    private static Assembly compiledAssembly;

    public static Assembly CompiledAssembly
    {
      get
      {
        if (EntityFrameworkProfilerInternal.compiledAssembly == null)
          throw new InvalidOperationException("CompiledAssembly wasn't initialized yet. Please call EntityFrameworkProfiler.Initialize() first.");
        return EntityFrameworkProfilerInternal.compiledAssembly;
      }
      internal set
      {
        EntityFrameworkProfilerInternal.compiledAssembly = value;
      }
    }
  }
}
