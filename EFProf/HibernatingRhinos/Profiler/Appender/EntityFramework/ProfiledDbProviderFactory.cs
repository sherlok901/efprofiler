//-----------------------------------------------------------------------
// <copyright file="ProfiledDbProviderFactory.cs" company="Hibernating Rhinos LTD">
//     Copyright (c) Hibernating Rhinos LTD. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data.Common;
#if EF6
using System.Data.Entity.Core.Common;
#endif
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Security;
using System.Security.Permissions;
using HibernatingRhinos.Profiler.Appender.ProfiledDataAccess;
using HibernatingRhinos.Profiler.Appender.Util;

namespace HibernatingRhinos.Profiler.Appender.EntityFramework
{
	public class ProfiledDbProviderFactory<TInnerProviderFactory> : DbProviderFactory, IServiceProvider
		where TInnerProviderFactory : DbProviderFactory
	{
		private readonly TInnerProviderFactory inner;

		public static readonly IDbAppender Appender =
			new EntityFrameworkAppender(typeof (TInnerProviderFactory).Name);

		public ProfiledDbProviderFactory()
		{
			var field = typeof (TInnerProviderFactory).GetField("Instance", BindingFlags.Public | BindingFlags.Static);
			inner = (TInnerProviderFactory) field.GetValue(null);
		}

		public static readonly ProfiledDbProviderFactory<TInnerProviderFactory> Instance = new ProfiledDbProviderFactory<TInnerProviderFactory>();

		public override bool CanCreateDataSourceEnumerator
		{
			get { return inner.CanCreateDataSourceEnumerator; }
		}

		public override DbCommandBuilder CreateCommandBuilder()
		{
			return inner.CreateCommandBuilder();
		}

		public override DbCommand CreateCommand()
		{
			return new ProfiledCommand(inner.CreateCommand(), Appender, DbReaderWrappingMode.ProfiledDataReader);
		}

		public override DbConnectionStringBuilder CreateConnectionStringBuilder()
		{
			return inner.CreateConnectionStringBuilder();
		}

		public override DbDataAdapter CreateDataAdapter()
		{
			return new ProfiledDbDataAdapter(inner.CreateDataAdapter());
		}

		public override DbDataSourceEnumerator CreateDataSourceEnumerator()
		{
			return inner.CreateDataSourceEnumerator();
		}

		public override DbParameter CreateParameter()
		{
			return inner.CreateParameter();
		}

		public override DbConnection CreateConnection()
		{
			return new ProfiledConnection<TInnerProviderFactory>(inner.CreateConnection(), Appender, DbReaderWrappingMode.ProfiledDataReader, Guid.NewGuid(), this);
		}

		public object GetService(Type serviceType)
		{
			if (serviceType == GetType())
				return inner;

			var provider = (IServiceProvider) inner;
			var service = provider.GetService(serviceType);
			var dbProviderServices = service as DbProviderServices;

			if (dbProviderServices == null && inner is SqlClientFactory)
			{
				var sqlProviderServices = Type.GetType("System.Data.SqlClient.SqlProviderServices, System.Data.Entity");
				if (sqlProviderServices != null)
				{
					var fieldInfo = sqlProviderServices.GetField("Instance", BindingFlags.Static | BindingFlags.NonPublic);
					if (fieldInfo != null)
					{
						dbProviderServices = fieldInfo.GetValue(null) as DbProviderServices;
					}
				}
			}

			if (dbProviderServices != null)
				return new ProfiledDbProviderServices(dbProviderServices, Appender);

			// Thr "Please use assembly redirect to the right EF version"
			return service;
		}

		public override CodeAccessPermission CreatePermission(PermissionState state)
		{
			return inner.CreatePermission(state);
		}
	}
}