﻿#if EF6
using System;
using System.Collections.Concurrent;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Core.Common;
using System.Data.Entity.Core.EntityClient;
using System.Data.Entity.Infrastructure;
using System.Diagnostics;
using System.Reflection;
using HibernatingRhinos.Profiler.Appender.ProfiledDataAccess;

namespace HibernatingRhinos.Profiler.Appender.EntityFramework
{
	public static class ProfiledDbConfiguration
	{
		private static bool isLoadedRegistered;

		public static void Initialize()
		{
			ProfiledConnectionMapping.Initialize();

			if (isLoadedRegistered == false)
			{
				isLoadedRegistered = true;

				DbConfiguration.Loaded += (sender, args) =>
				{
					args.ReplaceService<DbProviderServices>((inner, o) =>
					{
						var appender = new EntityFrameworkAppender(inner.GetType().Name);
						return new ProfiledDbProviderServices(inner, appender);
					});

					//			args.ReplaceService<IDbProviderFactoryService>((inner, o) => new ProfiledDbProviderFactoryService(inner));

					args.ReplaceService<IDbConnectionFactory>((inner, o) =>
					{
					    Type providerType = null;
					    if (inner.GetType().FullName == "Oracle.ManagedDataAccess.EntityFramework.OracleConnectionFactory")
					    {
                            // ORACLE requires a valid connection string here
					        var conn = inner.CreateConnection("USER ID=system;PASSWORD=dummy;DATA SOURCE=\"(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=dummy-pc)(PORT=1521)))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=XE)));\"");
					        PropertyInfo prop = conn.GetType().GetProperty("DbProviderFactory", BindingFlags.NonPublic | BindingFlags.Instance);
                            var provider = prop.GetValue(conn, null);
                            providerType = provider.GetType();
					    }
					    else
					    {
                            var dbConnection = inner.CreateConnection("fake connection string which will never be used");

                            var provider = dbConnection.GetType().GetProperty("ProviderFactory", BindingFlags.NonPublic | BindingFlags.Instance)
                                                       .GetValue(dbConnection, null);

					        providerType = provider.GetType();
					    }
						if (providerType.Assembly != typeof (ProfiledDbProviderFactory<>).Assembly)
						{
							providerType = typeof (ProfiledDbProviderFactory<>).MakeGenericType(providerType);
						}

						var connectionFactoryType = typeof (ProfiledConnectionFactory<>).MakeGenericType(providerType.GetGenericArguments()[0]);
						return (IDbConnectionFactory) Activator.CreateInstance(connectionFactoryType, new object[] {inner, (IDbAppender) providerType.GetField("Appender").GetValue(null), DbReaderWrappingMode.ProfiledDataReader, (DbProviderFactory) providerType.GetField("Instance").GetValue(null)});
					});


					// NOTE: This is needed to avoid a something that smells like a bug in EF.
					// The EF team doesn't believe that this is exists, and they asked for a mall repro project to prove this.
					args.ReplaceService<DbProviderFactory>((inner, o) =>
					{
						var invariantName = (string) o;
						return DbProviderFactories.GetFactory(invariantName);
					});
				};
			}
		}
	}

	public class ProfiledConnectionMapping
	{
		public static readonly ConcurrentDictionary<DbConnection, ProfiledConnection> Mappings = new ConcurrentDictionary<DbConnection, ProfiledConnection>();

		public static void Initialize()
		{
			ProfiledConnection.OnProfiledConnectionCreated += connection => Mappings.TryAdd(connection.Inner, connection);
			ProfiledConnection.OnProfiledConnectionDisposed += connection =>
			{
				ProfiledConnection _;
				Mappings.TryRemove(connection.Inner, out _);
			};
		}
	}

	/*public class ProfiledDbProviderFactoryService : IDbProviderFactoryService
	{
		private readonly IDbProviderFactoryService innerFactoryService;

		public ProfiledDbProviderFactoryService(IDbProviderFactoryService innerFactoryService)
		{
			this.innerFactoryService = innerFactoryService;
		}

		public DbProviderFactory GetProviderFactory(DbConnection connection)
		{
			if (connection is ProfiledConnection)
			{
				var connectionType = connection.GetType();
				if (connectionType.IsGenericType)
				{
					var innerProviderFactory = connectionType.GetGenericArguments()[0];
					var profiledDbProviderFactory = typeof(ProfiledDbProviderFactory<>).MakeGenericType(innerProviderFactory);
					return (DbProviderFactory) Activator.CreateInstance(profiledDbProviderFactory);
				}
			}

			if (connection is EntityConnection)
				return innerFactoryService.GetProviderFactory(connection);

			ProfiledConnection profiledConnection;
			if (ProfiledConnectionMapping.Mappings.TryGetValue(connection, out profiledConnection))
			{
				var connectionType = profiledConnection.GetType();
				if (connectionType.IsGenericType)
				{
					var innerProviderFactory = connectionType.GetGenericArguments()[0];
					var profiledDbProviderFactory = typeof(ProfiledDbProviderFactory<>).MakeGenericType(innerProviderFactory);
					return (DbProviderFactory)Activator.CreateInstance(profiledDbProviderFactory);
				}
			}

			throw new InvalidOperationException("Should have ProfiledConnection but got " + connection.GetType().FullName + ". If you're using EF6 pre-release version, please make sure to use the latest official pre-release of it, and not a nightly build.");
		}
	}*/
}
#endif