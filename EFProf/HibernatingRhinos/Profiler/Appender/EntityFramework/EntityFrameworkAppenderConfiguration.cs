﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Appender.EntityFramework.EntityFrameworkAppenderConfiguration
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using HibernatingRhinos.Profiler.Appender.StackFilters;
using HibernatingRhinos.Profiler.Appender.StackTraces;
using System;

namespace HibernatingRhinos.Profiler.Appender.EntityFramework
{
  public class EntityFrameworkAppenderConfiguration : AppenderConfiguration
  {
    public EntityFrameworkAppenderConfiguration(IStackTraceFilter[] stackTraceFilters)
      : base(stackTraceFilters)
    {
      this.EntityFrameworkVersion = EntityFrameworkVersion.Detect;
    }

    public EntityFrameworkAppenderConfiguration()
      : base(new IStackTraceFilter[2]
      {
        (IStackTraceFilter) new EntityFrameworkSqlStackTraceFilter(),
        (IStackTraceFilter) new CustomReportingFilter()
      })
    {
      this.EntityFrameworkVersion = EntityFrameworkVersion.Detect;
    }

    public override int DefaultPort
    {
      get
      {
        return 22898;
      }
    }

    internal override void StartProfiling()
    {
      throw new NotImplementedException();
    }

    internal override void StopProfiling()
    {
      throw new NotImplementedException();
    }

    public EntityFrameworkVersion EntityFrameworkVersion { get; set; }
  }
}
