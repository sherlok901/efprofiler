﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Appender.EntityFramework.EntityFrameworkProfiler
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using HibernatingRhinos.Profiler.Appender.ProfiledDataAccess;
using HibernatingRhinos.Profiler.Appender.Util;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.Appender.EntityFramework
{
  public static class EntityFrameworkProfiler
  {
    private static bool initialized;
    private static Assembly callingAssembly;
    private static EntityFrameworkVersion entityFrameworkVersion;

    public static void Initialize()
    {
      EntityFrameworkProfiler.callingAssembly = Assembly.GetCallingAssembly();
      EntityFrameworkProfiler.Initialize(new EntityFrameworkAppenderConfiguration());
    }

    public static void Initialize(EntityFrameworkVersion version)
    {
      EntityFrameworkProfiler.callingAssembly = Assembly.GetCallingAssembly();
      EntityFrameworkProfiler.Initialize(new EntityFrameworkAppenderConfiguration()
      {
        EntityFrameworkVersion = version
      });
    }

    [MethodImpl(MethodImplOptions.Synchronized)]
    public static void Initialize(EntityFrameworkAppenderConfiguration configuration)
    {
      string appSetting = ConfigurationManager.AppSettings["HibernatingRhinos.Profiler.Appender.EntityFramework"];
      if (!string.IsNullOrEmpty(appSetting) && appSetting.Equals("Disabled", StringComparison.OrdinalIgnoreCase))
        return;
      if (EntityFrameworkProfiler.callingAssembly == null)
        EntityFrameworkProfiler.callingAssembly = Assembly.GetCallingAssembly();
      EntityFrameworkProfiler.entityFrameworkVersion = configuration.EntityFrameworkVersion;
      ProfilerInfrastructure.Initialize((AppenderConfiguration) configuration);
      EntityFrameworkProfiler.SetupEntityFrameworkIntegration();
    }

    public static void InitializeForProduction(int port, string password)
    {
      EntityFrameworkAppenderConfiguration appenderConfiguration = new EntityFrameworkAppenderConfiguration();
      appenderConfiguration.Production = true;
      appenderConfiguration.Port = port;
      appenderConfiguration.ProductionPassword = password;
      EntityFrameworkAppenderConfiguration configuration = appenderConfiguration;
      if (port == configuration.DefaultPort)
        throw new InvalidOperationException(string.Format("The '{0}' port is reserved for local profiling.", (object) configuration.DefaultPort));
      EntityFrameworkProfiler.Initialize(configuration);
    }

    [MethodImpl(MethodImplOptions.Synchronized)]
    public static void Shutdown()
    {
      ProfilerInfrastructure.Shutdown();
      EntityFrameworkProfiler.initialized = false;
    }

    private static void SetupEntityFrameworkIntegration()
    {
      if (EntityFrameworkProfiler.initialized)
        return;
      EntityFrameworkProfiler.SetupDatabaseDefaultConnectionFactoryIfNeeded();
      EntityFrameworkProfiler.RewriteProvidersDefinition();
      EntityFrameworkProfiler.initialized = true;
    }

    private static void SetupDatabaseDefaultConnectionFactoryIfNeeded()
    {
      HashSet<string> defineSymbols = new HashSet<string>();
      bool flag1 = false;
      HashSet<string> assemblies1 = new HashSet<string>()
      {
        typeof (EntityFrameworkProfiler).Assembly.Location
      };
      Assembly assembly1 = (Assembly) null;
      Assembly assembly2 = (Assembly) null;
      if (EntityFrameworkProfiler.entityFrameworkVersion == EntityFrameworkVersion.Detect)
        assembly1 = EntityFrameworkProfiler.TryLoadAssembly(new AssemblyName()
        {
          Name = "EntityFramework"
        });
      else if (EntityFrameworkProfiler.entityFrameworkVersion == EntityFrameworkVersion.EntityFramework6)
        assembly1 = EntityFrameworkProfiler.TryLoadAssembly(new AssemblyName()
        {
          Name = "EntityFramework",
          Version = new Version("6.0.0.0")
        });
      else if (EntityFrameworkProfiler.entityFrameworkVersion == EntityFrameworkVersion.EntityFramework5)
      {
        Assembly assembly3 = EntityFrameworkProfiler.TryLoadAssembly(new AssemblyName()
        {
          Name = "EntityFramework",
          Version = new Version("5.0.0.0")
        });
        if (assembly3 == null)
          assembly3 = EntityFrameworkProfiler.TryLoadAssembly(new AssemblyName()
          {
            Name = "EntityFramework",
            Version = new Version("4.4.0.0")
          });
        assembly1 = assembly3;
      }
      else if (EntityFrameworkProfiler.entityFrameworkVersion == EntityFrameworkVersion.EntityFramework4)
      {
        Assembly assembly3 = EntityFrameworkProfiler.TryLoadAssembly(new AssemblyName()
        {
          Name = "EntityFramework",
          Version = new Version("4.3.1.0")
        });
        if (assembly3 == null)
        {
          Assembly assembly4 = EntityFrameworkProfiler.TryLoadAssembly(new AssemblyName()
          {
            Name = "EntityFramework",
            Version = new Version("4.3.0.0")
          });
          if (assembly4 == null)
          {
            Assembly assembly5 = EntityFrameworkProfiler.TryLoadAssembly(new AssemblyName()
            {
              Name = "EntityFramework",
              Version = new Version("4.2.0.0")
            });
            if (assembly5 == null)
              assembly3 = EntityFrameworkProfiler.TryLoadAssembly(new AssemblyName()
              {
                Name = "EntityFramework",
                Version = new Version("4.1.0.0")
              });
            else
              assembly3 = assembly5;
          }
          else
            assembly3 = assembly4;
        }
        assembly1 = assembly3;
      }
      if (EntityFrameworkProfiler.entityFrameworkVersion == EntityFrameworkVersion.Detect)
        assembly2 = EntityFrameworkProfiler.TryLoadAssembly(typeof (object).Assembly.ImageRuntimeVersion == "v2.0.50727" ? "System.Data.Entity, Version=3.5.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" : "System.Data.Entity, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089");
      else if (EntityFrameworkProfiler.entityFrameworkVersion == EntityFrameworkVersion.EntityFramework5 || EntityFrameworkProfiler.entityFrameworkVersion == EntityFrameworkVersion.EntityFramework4)
        assembly2 = EntityFrameworkProfiler.TryLoadAssembly("System.Data.Entity, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089");
      else if (EntityFrameworkProfiler.entityFrameworkVersion == EntityFrameworkVersion.EntityFramework35)
        assembly2 = EntityFrameworkProfiler.TryLoadAssembly("System.Data.Entity, Version=3.5.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089");
      if (assembly2 == null)
      {
        if (EntityFrameworkProfiler.entityFrameworkVersion != EntityFrameworkVersion.EntityFramework6)
        {
          try
          {
            Type type = Type.GetType("System.Data.Common.DbCommandDefinition, System.Data.Entity, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089", false);
            if (type != null)
              assembly2 = type.Assembly;
          }
          catch
          {
            try
            {
              assembly2 = AssemblyFinder.FindAssemblyPath("System.Data.Entity");
            }
            catch
            {
            }
          }
        }
      }
      if (assembly1 == null && assembly2 == null)
      {
        Assembly[] assemblies2 = AppDomain.CurrentDomain.GetAssemblies();
        Assembly assembly3 = (((IEnumerable<Assembly>) assemblies2).FirstOrDefault<Assembly>((Func<Assembly, bool>) (name => name.GetName().Name == "EntityFramework")) ?? ((IEnumerable<Assembly>) assemblies2).FirstOrDefault<Assembly>((Func<Assembly, bool>) (name => name.GetName().Name == "System.Data.Entity"))) ?? ((IEnumerable<Assembly>) assemblies2).FirstOrDefault<Assembly>((Func<Assembly, bool>) (assembly =>
        {
          if (assembly.GetType("System.Data.Entity.Core.Common.DbCommandDefinition") == null)
            return assembly.GetType("System.Data.Common.DbCommandDefinition") != null;
          return true;
        }));
        if (assembly3 == null && EntityFrameworkProfiler.callingAssembly != null)
        {
          AssemblyName[] referencedAssemblies = EntityFrameworkProfiler.callingAssembly.GetReferencedAssemblies();
          AssemblyName assemblyRef = ((IEnumerable<AssemblyName>) referencedAssemblies).FirstOrDefault<AssemblyName>((Func<AssemblyName, bool>) (name => name.Name == "EntityFramework")) ?? ((IEnumerable<AssemblyName>) referencedAssemblies).FirstOrDefault<AssemblyName>((Func<AssemblyName, bool>) (name => name.Name == "System.Data.Entity"));
          if (assemblyRef != null)
            assembly3 = Assembly.Load(assemblyRef);
        }
        if ((EntityFrameworkProfiler.entityFrameworkVersion == EntityFrameworkVersion.Detect || EntityFrameworkProfiler.entityFrameworkVersion == EntityFrameworkVersion.EntityFramework5 || (EntityFrameworkProfiler.entityFrameworkVersion == EntityFrameworkVersion.EntityFramework4 || EntityFrameworkProfiler.entityFrameworkVersion == EntityFrameworkVersion.EntityFramework35)) && assembly3 == null)
          assembly3 = AssemblyFinder.FindAssemblyPath("System.Data.Entity");
        if (assembly3 == null)
          throw new InvalidOperationException(string.Format("We did not recognize the EntityFramework version that you're using. Tried to find the type DbCommandDefinition in {0} and in {1}. Please send this error message to support@hibernatingrhinos.com", (object) "System.Data.Common", (object) "System.Data.Entity.Core.Common"));
        if (assembly3.GetName().Name == "EntityFramework")
          assembly1 = assembly3;
        else
          assembly2 = assembly3;
      }
      if (assembly1 != null)
      {
        assemblies1.Add(assembly1.Location);
        AssemblyFileVersionAttribute versionAttribute = ((IEnumerable<object>) assembly1.GetCustomAttributes(typeof (AssemblyFileVersionAttribute), true)).FirstOrDefault<object>() as AssemblyFileVersionAttribute;
        if (versionAttribute != null && versionAttribute.Version == "4.1.10715.0")
          throw new NotSupportedException("You're using Entity Framework 4.1 Update 1 version, which we do not support. Please use a different version of Entity Framework.");
        if (assembly1.GetName().Version.Major >= 6)
        {
          flag1 = true;
          defineSymbols.Add("EF6");
        }
      }
      if (assembly2 != null)
      {
        if (!flag1)
          assemblies1.Add(assembly2.Location);
      }
      try
      {
        List<string> sources = new List<string>()
        {
          "HibernatingRhinos.Profiler.Appender.EntityFramework.ProfiledConnectionFactory.cs",
          "HibernatingRhinos.Profiler.Appender.EntityFramework.ProfiledConnectionFactoryHelper.cs",
          "HibernatingRhinos.Profiler.Appender.EntityFramework.ProfiledDbCommandDefinition.cs",
          "HibernatingRhinos.Profiler.Appender.EntityFramework.ProfiledDbConfiguration.cs",
          "HibernatingRhinos.Profiler.Appender.EntityFramework.ProfiledDbProviderFactory.cs",
          "HibernatingRhinos.Profiler.Appender.EntityFramework.ProfiledDbProviderServices.cs"
        };
        Assembly assembly3 = ((IEnumerable<Assembly>) AppDomain.CurrentDomain.GetAssemblies()).FirstOrDefault<Assembly>((Func<Assembly, bool>) (assembly => assembly.GetType("System.Data.Common.DbConnection") != null)) ?? AssemblyFinder.FindAssemblyPath("System.Data");
        if (assembly3 != null)
          assemblies1.Add(assembly3.Location);
        bool flag2 = false;
        if (EntityFrameworkProfiler.entityFrameworkVersion == EntityFrameworkVersion.Detect || EntityFrameworkProfiler.entityFrameworkVersion == EntityFrameworkVersion.EntityFramework5 || (EntityFrameworkProfiler.entityFrameworkVersion == EntityFrameworkVersion.EntityFramework4 || EntityFrameworkProfiler.entityFrameworkVersion == EntityFrameworkVersion.EntityFramework35))
        {
          Type type1 = ((IEnumerable<Assembly>) AppDomain.CurrentDomain.GetAssemblies()).Select<Assembly, Type>((Func<Assembly, Type>) (assembly => assembly.GetType("System.Data.Entity.Database"))).FirstOrDefault<Type>((Func<Type, bool>) (type =>
          {
            if (type != null)
              return type.GetProperty("DefaultConnectionFactory") != null;
            return false;
          }));
          if (type1 != null)
          {
            assemblies1.Add(type1.Assembly.Location);
            defineSymbols.Add("EF4");
            flag2 = true;
          }
        }
        EntityFrameworkProfilerInternal.CompiledAssembly = GenerateAssembly.CompileAssembly(sources, assemblies1, defineSymbols, "HibernatingRhinos.Profiler.Appender.EntityFramework.ProfiledConnectionFactory.dll", (string) null);
        if (flag1)
        {
          EntityFrameworkProfilerInternal.CompiledAssembly.GetType("HibernatingRhinos.Profiler.Appender.EntityFramework.ProfiledDbConfiguration").GetMethod("Initialize").Invoke((object) null, (object[]) null);
        }
        else
        {
          if (!flag2)
            return;
          EntityFrameworkProfilerInternal.CompiledAssembly.GetType("HibernatingRhinos.Profiler.Appender.EntityFramework.ProfiledConnectionFactoryHelper").GetMethod("Initialize").Invoke((object) null, (object[]) null);
        }
      }
      catch (Exception ex)
      {
        Trace.TraceError(ex.ToString());
        throw;
      }
    }

    private static Assembly TryLoadAssembly(string assemblyName)
    {
      try
      {
        return Assembly.Load(assemblyName);
      }
      catch
      {
      }
      return (Assembly) null;
    }

    private static Assembly TryLoadAssembly(AssemblyName assemblyName)
    {
      try
      {
        return Assembly.Load(assemblyName);
      }
      catch
      {
      }
      return (Assembly) null;
    }

    public static void InitializeOfflineProfiling(string filename)
    {
      EntityFrameworkAppenderConfiguration configuration = new EntityFrameworkAppenderConfiguration();
      configuration.FileToLogTo = filename;
      EntityFrameworkProfiler.Initialize(configuration);
    }

    private static void RewriteProvidersDefinition()
    {
      DataTable factoriesDataTable = EntityFrameworkProfiler.GetDbProvidersFactoriesDataTable();
      if (factoriesDataTable == null)
        return;
      foreach (string providerInvariantName in factoriesDataTable.Rows.Cast<DataRow>().Select<DataRow, string>((Func<DataRow, string>) (dt => (string) dt["InvariantName"])).ToList<string>())
      {
        DbProviderFactory factory;
        try
        {
          factory = DbProviderFactories.GetFactory(providerInvariantName);
        }
        catch (Exception ex)
        {
          continue;
        }
        if (factory.GetType().Namespace.StartsWith("HibernatingRhinos.Profiler.Appender."))
        {
          EntityFrameworkProfiler.RegisterStatisticsSource((IReflect) factory.GetType());
        }
        else
        {
          Type type = EntityFrameworkProfilerInternal.CompiledAssembly.GetType("HibernatingRhinos.Profiler.Appender.EntityFramework.ProfiledDbProviderFactory`1").MakeGenericType(factory.GetType());
          EntityFrameworkProfiler.RegisterStatisticsSource((IReflect) type);
          string cp = providerInvariantName;
          DataRow row1 = factoriesDataTable.Rows.Cast<DataRow>().First<DataRow>((Func<DataRow, bool>) (dt => (string) dt["InvariantName"] == cp));
          DataRow row2 = factoriesDataTable.NewRow();
          row2["Name"] = row1["Name"];
          row2["Description"] = row1["Description"];
          row2["InvariantName"] = row1["InvariantName"];
          row2["AssemblyQualifiedName"] = (object) type.AssemblyQualifiedName;
          factoriesDataTable.Rows.Remove(row1);
          factoriesDataTable.Rows.Add(row2);
        }
      }
    }

    private static void RegisterStatisticsSource(IReflect factoryType)
    {
      ProfilerIntegration.RegisterStatisticsSource((IStatisticsSource) factoryType.GetField("Appender", BindingFlags.Static | BindingFlags.Public | BindingFlags.FlattenHierarchy).GetValue((object) null));
    }

    private static DataTable GetDbProvidersFactoriesDataTable()
    {
      EntityFrameworkProfiler.ForceDbProviderFactoriesInitialization();
      Type type = typeof (DbProviderFactories);
      object obj = (type.GetField("_providerTable", BindingFlags.Static | BindingFlags.NonPublic) ?? type.GetField("_configTable", BindingFlags.Static | BindingFlags.NonPublic)).GetValue((object) null);
      if (obj is DataSet)
        return ((DataSet) obj).Tables["DbProviderFactories"];
      return (DataTable) obj;
    }

    private static void ForceDbProviderFactoriesInitialization()
    {
      try
      {
        DbProviderFactories.GetFactory("does not exists");
      }
      catch (ArgumentException ex)
      {
      }
    }

    public static void RenameObjectContextInProfiler(DbConnection connection, string newName)
    {
      ProfiledConnection profiledConnection = connection as ProfiledConnection ?? connection.GetType().GetProperty("StoreConnection", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic).GetValue((object) connection, (object[]) null) as ProfiledConnection;
      if (profiledConnection == null)
        throw new InvalidOperationException("Your connection should be of type EntityConnection or ProfiledConnection.");
      ProfilerIntegration.PublishProfilerEvent(profiledConnection.ConnectionId.ToString(), "HibernatingRhinos.Profiler.Session.Rename", newName, DateTime.Now, Level.Debug);
    }

    public static void ResetStatistics()
    {
      DataTable factoriesDataTable = EntityFrameworkProfiler.GetDbProvidersFactoriesDataTable();
      List<string> stringList = new List<string>();
      foreach (DataRow row in (InternalDataCollectionBase) factoriesDataTable.Rows)
        stringList.Add((string) row["InvariantName"]);
      foreach (string providerInvariantName in stringList)
      {
        DbProviderFactory factory;
        try
        {
          factory = DbProviderFactories.GetFactory(providerInvariantName);
        }
        catch (Exception ex)
        {
          continue;
        }
        FieldInfo field = factory.GetType().GetField("Appender", BindingFlags.Static | BindingFlags.Public | BindingFlags.FlattenHierarchy);
        if (field != null)
          ((EntityFrameworkAppender) field.GetValue((object) null)).ResetStatistics();
      }
    }
  }
}
