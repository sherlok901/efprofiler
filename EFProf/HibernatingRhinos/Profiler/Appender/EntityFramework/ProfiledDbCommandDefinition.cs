//-----------------------------------------------------------------------
// <copyright file="ProfiledDbCommandDefinition.cs" company="Hibernating Rhinos LTD">
//     Copyright (c) Hibernating Rhinos LTD. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
using System.Data.Common;
#if EF6
using System.Data.Entity.Core.Common;
#endif
using HibernatingRhinos.Profiler.Appender.ProfiledDataAccess;

namespace HibernatingRhinos.Profiler.Appender.EntityFramework
{
    public class ProfiledDbCommandDefinition : DbCommandDefinition
    {
        private readonly DbCommandDefinition inner;
		private readonly IDbAppender appender;

        public ProfiledDbCommandDefinition(DbCommandDefinition inner, IDbAppender appender)
        {
            this.inner = inner;
            this.appender = appender;
        }

        public override DbCommand CreateCommand()
        {
            var command = inner.CreateCommand();
            return new ProfiledCommand(command, appender, DbReaderWrappingMode.ProfiledDataReader);
        }
    }
}