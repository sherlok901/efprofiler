// Should be used in EF4 and up but bellow EF 6 alpha 3.
#if EF4 && !EF6
using System;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Reflection;
using HibernatingRhinos.Profiler.Appender.ProfiledDataAccess;

namespace HibernatingRhinos.Profiler.Appender.EntityFramework
{
	public class ProfiledConnectionFactoryHelper
	{
		public static void Initialize()
		{
			var old = Database.DefaultConnectionFactory;

			var dbConnection = old.CreateConnection("fake connection string which will never be used");

			var provider = dbConnection.GetType().GetProperty("ProviderFactory", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(
			                                                                                                                              dbConnection, null);

			Type providerType = provider.GetType();
			if (providerType.Assembly != typeof (ProfiledDbProviderFactory<>).Assembly)
			{
				providerType = typeof (ProfiledDbProviderFactory<>).MakeGenericType(providerType);
			}

			var connectionFactoryType = typeof (ProfiledConnectionFactory<>).MakeGenericType(providerType.GetGenericArguments()[0]);
			Database.DefaultConnectionFactory = (IDbConnectionFactory) Activator.CreateInstance(connectionFactoryType, new object[] {old, (IDbAppender) providerType.GetField("Appender").GetValue(null), DbReaderWrappingMode.ProfiledDataReader, (DbProviderFactory) providerType.GetField("Instance").GetValue(null)});
		}
	}
}
#endif