// Should be used in EF4 and up.
#if EF4 || EF6
using System;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Reflection;
using HibernatingRhinos.Profiler.Appender.ProfiledDataAccess;

namespace HibernatingRhinos.Profiler.Appender.EntityFramework
{
	public class ProfiledConnectionFactory<TConnectionFactory> : IDbConnectionFactory
		where TConnectionFactory : DbProviderFactory
	{
		private readonly IDbConnectionFactory inner;
		private readonly IDbAppender appender;
		private readonly DbReaderWrappingMode wrappingMode;
		private readonly DbProviderFactory factory;

		public ProfiledConnectionFactory(IDbConnectionFactory inner, IDbAppender appender, DbReaderWrappingMode wrappingMode, DbProviderFactory factory)
		{
			this.inner = inner;
			this.factory = factory;
			this.appender = appender;
			this.wrappingMode = wrappingMode;
		}

		public DbConnection CreateConnection(string nameOrConnectionString)
		{
			return new ProfiledConnection<TConnectionFactory>(inner.CreateConnection(nameOrConnectionString), appender, wrappingMode, Guid.NewGuid(), factory);
		}
	}
}
#endif