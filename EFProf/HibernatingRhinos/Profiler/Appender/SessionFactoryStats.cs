﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Appender.SessionFactoryStats
// Assembly: HibernatingRhinos.Profiler.Appender, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: FD4E0CB6-EE85-4333-9499-D61C822F8DB7
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\HibernatingRhinos.Profiler.Appender.dll

using System.Collections;

namespace HibernatingRhinos.Profiler.Appender
{
  public class SessionFactoryStats
  {
    public string Name { get; set; }

    public Hashtable Statistics { get; set; }

    public bool Equals(SessionFactoryStats other)
    {
      if (object.ReferenceEquals((object) null, (object) other))
        return false;
      if (object.ReferenceEquals((object) this, (object) other))
        return true;
      if (object.Equals((object) other.Name, (object) this.Name))
        return object.Equals((object) other.Statistics, (object) this.Statistics);
      return false;
    }

    public override bool Equals(object obj)
    {
      if (object.ReferenceEquals((object) null, obj))
        return false;
      if (object.ReferenceEquals((object) this, obj))
        return true;
      return this.Equals((SessionFactoryStats) obj);
    }

    public override int GetHashCode()
    {
      int num = this.Name.GetHashCode();
      foreach (DictionaryEntry statistic in this.Statistics)
      {
        num = num * 397 ^ statistic.Key.GetHashCode();
        object obj1 = statistic.Value;
        IEnumerable enumerable = obj1 as IEnumerable;
        if (enumerable != null)
        {
          foreach (object obj2 in enumerable)
          {
            if (obj2 != null)
              num = num * 397 ^ obj2.GetHashCode();
          }
        }
        else if (obj1 != null)
          num = num * 397 ^ obj1.GetHashCode();
      }
      return num;
    }
  }
}
