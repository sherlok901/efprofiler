﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.ProfilerBootstrapper
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using Castle.Core.Internal;
using Castle.Facilities.TypedFactory;
using Castle.MicroKernel;
using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.BackEnd.Exceptions;
using HibernatingRhinos.Profiler.BackEnd.Profiles;
using HibernatingRhinos.Profiler.Client.CaliburnInfrastructure;
using HibernatingRhinos.Profiler.Client.Controls;
using HibernatingRhinos.Profiler.Client.Infrastructure;
using HibernatingRhinos.Profiler.Client.Infrastructure.AutoUpdate;
using HibernatingRhinos.Profiler.Client.Licensing;
using HibernatingRhinos.Profiler.Client.Services;
using HibernatingRhinos.Profiler.Client.Settings;
using HibernatingRhinos.Profiler.Client.Shell;
using HibernatingRhinos.Profiler.Client.Startup;
using NAppUpdate.Framework;
using Rhino.Licensing;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace HibernatingRhinos.Profiler.Client
{
  public class ProfilerBootstrapper : HandleUnhandledExceptions
  {
    protected static readonly ILog Log = LogManager.GetLog(typeof (ProfilerBootstrapper));
    public static readonly Func<DependencyObject, string, System.Windows.Interactivity.TriggerBase> CreateTrigger = (Func<DependencyObject, string, System.Windows.Interactivity.TriggerBase>) ((target, triggerText) =>
    {
      if (triggerText == null)
        return ConventionManager.GetElementConvention(target.GetType()).CreateTrigger();
      string[] strArray1 = triggerText.Replace("[", string.Empty).Replace("]", string.Empty).Split((char[]) null, StringSplitOptions.RemoveEmptyEntries);
      switch (strArray1[0])
      {
        case "Event":
          return (System.Windows.Interactivity.TriggerBase) new System.Windows.Interactivity.EventTrigger()
          {
            EventName = strArray1[1]
          };
        case "Key":
          if (!strArray1[1].Contains("+"))
          {
            Key key = (Key) Enum.Parse(typeof (Key), strArray1[1], true);
            return (System.Windows.Interactivity.TriggerBase) new KeyTrigger()
            {
              Key = key
            };
          }
          string[] strArray2 = strArray1[1].Split('+');
          ModifierKeys modifierKeys = (ModifierKeys) Enum.Parse(typeof (ModifierKeys), string.Join(",", ((IEnumerable<string>) strArray2).Take<string>(strArray2.Length - 1)), true);
          Key key1 = (Key) Enum.Parse(typeof (Key), ((IEnumerable<string>) strArray2).Last<string>(), true);
          return (System.Windows.Interactivity.TriggerBase) new KeyTrigger()
          {
            Key = key1,
            Modifiers = modifierKeys
          };
        default:
          throw new InvalidOperationException("Trigger type not supported");
      }
    });
    public static readonly Func<Type, DependencyObject, object, UIElement> MyLocateForModelType = (Func<Type, DependencyObject, object, UIElement>) ((modelType, displayLocation, context) =>
    {
      IViewStrategy viewStrategy = modelType.GetAttributes<IViewStrategy>(true).FirstOrDefault<IViewStrategy>((Func<IViewStrategy, bool>) (x => x.Matches(context)));
      if (viewStrategy != null)
        return viewStrategy.Locate(modelType, displayLocation, context);
      string viewTypeName = modelType.FullName.Replace("Model", string.Empty);
      viewTypeName += "View";
      if (context != null)
      {
        viewTypeName = viewTypeName.Remove(viewTypeName.Length - 4, 4);
        viewTypeName += "Views";
        viewTypeName = viewTypeName + "." + context;
      }
      Type type1 = AssemblySource.Instance.SelectMany((Func<Assembly, IEnumerable<Type>>) (assmebly => (IEnumerable<Type>) assmebly.GetExportedTypes()), (assmebly, type) => new
      {
        assmebly = assmebly,
        type = type
      }).Where(_param1 => _param1.type.FullName == viewTypeName).Select(_param0 => _param0.type).FirstOrDefault<Type>();
      if (!(type1 == (Type) null))
        return ViewLocator.GetOrCreateViewType(type1);
      return (UIElement) new TextBlock()
      {
        Text = string.Format("{0} not found.", (object) viewTypeName)
      };
    });
    private Window mainWindow;
    private ApplicationContainer container;

    static ProfilerBootstrapper()
    {
      LogManager.GetLog = (Func<Type, ILog>) (type => (ILog) new ProfilerLogger(type));
    }

    protected override void Configure()
    {
      this.container = new ApplicationContainer();
      this.container.AddFacility<TypedFactoryFacility>();
      ViewLocator.LocateForModelType = ProfilerBootstrapper.MyLocateForModelType;
      Caliburn.Micro.Parser.CreateTrigger = ProfilerBootstrapper.CreateTrigger;
      this.CreateCustomConventions();
    }

    public void CreateCustomConventions()
    {
      ConventionManager.AddElementConvention<MenuItem>(ItemsControl.ItemsSourceProperty, "DataContext", "Click");
    }

    protected override object GetInstance(Type service, string key)
    {
      if (string.IsNullOrWhiteSpace(key))
        return this.container.Resolve(service);
      if (service != (Type) null)
        return this.container.Resolve(key, service);
      return ProfilerBootstrapper.FindComponentByTypeName(this.container, key);
    }

    private static object FindComponentByTypeName(ApplicationContainer container, string typeName)
    {
      foreach (Type service in AssemblySource.Instance.SelectMany((Func<Assembly, IEnumerable<Type>>) (assembly => (IEnumerable<Type>) assembly.GetExportedTypes()), (assembly, type) => new
      {
        assembly = assembly,
        type = type
      }).Where(_param1 => _param1.type.Name == typeName).Select(_param0 => _param0.type))
      {
        try
        {
          object obj = container.Resolve(service);
          if (obj != null)
            return obj;
        }
        catch (ComponentNotFoundException ex)
        {
        }
      }
      return (object) null;
    }

    protected override IEnumerable<object> GetAllInstances(Type service)
    {
      return (IEnumerable<object>) this.container.ResolveAll(service);
    }

    protected override void BuildUp(object instance)
    {
      ((IEnumerable<PropertyInfo>) instance.GetType().GetProperties()).Where<PropertyInfo>((Func<PropertyInfo, bool>) (property =>
      {
        if (property.CanWrite)
          return property.PropertyType.IsPublic;
        return false;
      })).Where<PropertyInfo>((Func<PropertyInfo, bool>) (property => this.container.Kernel.HasComponent(property.PropertyType))).ForEach<PropertyInfo>((System.Action<PropertyInfo>) (property => property.SetValue(instance, this.container.Resolve(property.PropertyType), (object[]) null)));
    }

    protected override void OnStartup(object sender, StartupEventArgs e)
    {
      base.OnStartup(sender, e);
      BackendBridge backendBridge = IoC.Get<BackendBridge>((string) null);
      backendBridge.Configuration = UserPreferencesHolder.UserSettings.Configuration;
      backendBridge.Configuration.EnsureIsValid();
      backendBridge.ConnectionInfo = UserPreferencesHolder.UserSettings.ConnectionInfo;
      string contextFile = this.InitializeBackendBridge(backendBridge);
      this.DisplayMainWindow();
      this.ProfilerStartup(backendBridge, contextFile);
    }

    private void DisplayMainWindow()
    {
      ApplicationModel applicationModel = (ApplicationModel) IoC.GetInstance(typeof (ApplicationModel), (string) null);
            //igor
            //applicationModel.ActivateItem(applicationModel.ActiveItem);
            //=igor

            ((IActivate)applicationModel).Activate();


            //applicationModel.Activate();

            MainWindow mainWindow = new MainWindow();
      ViewModelBinder.Bind((object) applicationModel, (DependencyObject) mainWindow, (object) null);
      mainWindow.Show();
        mainWindow.Activate();
      this.mainWindow = this.Application.MainWindow;
      this.mainWindow.Closing += new CancelEventHandler(this.MainWindowClosing);
    }

    private string InitializeBackendBridge(BackendBridge backendBridge)
    {
      //Thread thread = new Thread(new ThreadStart(ProfilerBootstrapper.SelfVerification))
      //{
      //  IsBackground = true
      //};
      //thread.SetApartmentState(ApartmentState.STA);
      //thread.Start();

      string contextFile = "";
      if (StartupParser.CurrentArguments != null)
        contextFile = StartupParser.CurrentArguments.ContextFile;
      this.AttemptToStartBackEnd((IBackendBridge) backendBridge, contextFile);
      IoC.Get<ProfilerAutoUpdate>((string) null).Configure();
      FileAssociation.SetupAssociation();
      return contextFile;
    }

    private bool AttemptToStartBackEnd(IBackendBridge backend, string contextFile)
    {
      try
      {
        backend.Start(UserPreferencesHolder.UserSettings.ListenPort, contextFile);
        PollingService.Start();
        return true;
      }
      catch (CouldNotActivateAnotherApplicationInstanceException ex)
      {
        int num = (int) MessageBox.Show("Could not start listening to notifications from profiled applications." + Environment.NewLine + "This error may be caused by another application listening to port " + (object) UserPreferencesHolder.UserSettings.ListenPort, Profile.CurrentProfileDisplayName + " - Could not open socket for listening", MessageBoxButton.OK, MessageBoxImage.Hand);
        this.CloseAfterAppInit();
      }
      catch (AnotherApplicationIsAlreadyRunningAndControlWasMovedToItException ex)
      {
        this.CloseAfterAppInit();
      }
      catch (Exception ex)
      {
        int num = (int) MessageBox.Show("Could not start listening to notifications from profiled applications." + Environment.NewLine + "This error may be caused by a firewall or a virus scanner that blocks " + Profile.CurrentProfileDisplayName + " from listening to a socket." + Environment.NewLine + Environment.NewLine + (object) ex, Profile.CurrentProfileDisplayName + " - Could not open socket for listening", MessageBoxButton.OK, MessageBoxImage.Hand);
        this.CloseAfterAppInit();
      }
      return false;
    }

    private void CloseAfterAppInit()
    {
      Environment.Exit(1);
    }

    private static void SelfVerification()
    {
      Thread.Sleep(TimeSpan.FromSeconds((double) new Random().Next(10, 25)));
      Assembly assembly = typeof (BuildInfo).Assembly;
      byte[] numArray = Convert.FromBase64String("ACQAAASAAACUAAAABgIAAAAkAABSU0ExAAQAAAEAAQDBLTP+od21ZEnX3WXgZVRX1adQQHusYawlMrB4Mnz5vH3GcynLb4CvG7zwjJiYsit/xgN28VmeeQJ5PSdcXH1aB54Qnm4TMa+HcRFxcnGLQQfzwKa/rQIufkQ+Du1hERNZRdERss/wpbnyF2mcNsmGDa09Y+x0264LLK/sK4SIog==");
      byte[] publicKey = assembly.GetName().GetPublicKey();
      for (int index = 0; index < publicKey.Length; ++index)
      {
        if ((int) publicKey[index] != (int) numArray[index])
          throw new InvalidOleVariantTypeException("pk");
      }
      bool pfWasVerified = false;
      if (!HostProgram.StrongNameSignatureVerificationEx(assembly.Location, true, ref pfWasVerified))
        throw new InvalidOleVariantTypeException("sn");
    }

    private void ProfilerStartup(BackendBridge backendBridge, string contextFile)
    {
      IoC.Get<IEventAggregator>((string) null).Subscribe((object) this);
      ApplicationModel applicationModel = IoC.Get<ApplicationModel>((string) null);

      //if (!applicationModel.LicenseInfo.ConfirmLicense(true) || !LicenseAgreementModel.HasAgreementBeenAccepted() && !Dialog.Show<LicenseAgreementModel>((object) null, true))
      //  return;

      IoC.Get<ProfilerAutoUpdate>((string) null).CheckForUpdates(false);
      if (!string.IsNullOrEmpty(contextFile))
      {
        Guid guid = backendBridge.StartLoadFromFile(contextFile);
        OpenFileInfo openFileInfo = new OpenFileInfo()
        {
          FileName = contextFile,
          Token = guid
        };
        backendBridge.Notifications.RaiseBringApplicationToFront(openFileInfo);
      }
      applicationModel.Start();
    }

    private void MainWindowClosing(object sender, CancelEventArgs e)
    {
      try
      {
        ProfilerAutoUpdate.ApplyUpdates(false);
      }
      catch (LicenseUpgradeRequiredException ex)
      {
        ProfilerBootstrapper.Log.Info("you need to upgrade your license.", (object) ex);
      }
      UpdateManager.Instance.CleanUp();
    }

    public void StartForTesting()
    {
      PlatformProvider.Current = (IPlatformProvider) new DefaultPlatformProvider();
      this.StartDesignTime();
    }
  }
}
