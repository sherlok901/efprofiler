﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Statistics.StatisticsModel
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.Client.Infrastructure;
using HibernatingRhinos.Profiler.Client.Sessions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text.RegularExpressions;

namespace HibernatingRhinos.Profiler.Client.Statistics
{
  public class StatisticsModel : PollingScreen
  {
    private static readonly Regex PascalCaseToWords = new Regex("([a-z](?=[A-Z])|[A-Z](?=[A-Z][a-z]))");
    private readonly Dictionary<string, string> keysToWordsCache = new Dictionary<string, string>();
    private readonly Dictionary<string, StatisticEntry> statisticLookup = new Dictionary<string, StatisticEntry>();
    private Dictionary<string, object> currentStatisticsSet = new Dictionary<string, object>();
    private readonly SessionListModel sessionList;
    private readonly BackendBridge backendBridge;
    private int currentIndex;
    private DateTime lastUpdate;

    public StatisticsModel(BackendBridge backendBridge, SessionListModel sessionList)
    {
      this.backendBridge = backendBridge;
      this.sessionList = sessionList;
      this.DisplayName = string.Empty;
      this.SortedStatistics = new BindableCollection<StatisticEntry>();
    }

    public bool ShouldRemindToEnableStatistics
    {
      get
      {
        if (this.sessionList.Sessions.Count<SessionModel>() <= 1)
          return false;
        if (this.currentStatisticsSet != null)
          return this.currentStatisticsSet.Count == 0;
        return true;
      }
    }

    public BindableCollection<StatisticEntry> SortedStatistics { get; private set; }

    public bool CanNavigate
    {
      get
      {
        if (this.AllStatistics == null)
          return false;
        return this.AllStatistics.Length > 0;
      }
    }

    public StatisticsSnapshot[] AllStatistics { get; set; }

    private string KeyToWordUsingCache(string key)
    {
      string str1;
      if (this.keysToWordsCache.TryGetValue(key, out str1))
        return str1;
      string str2 = StatisticsModel.PascalCaseToWords.Replace(key, "$1 ");
      this.keysToWordsCache[key] = str2;
      return str2;
    }

    public override void TimerTicked(object sender, EventArgs e)
    {
      if (DateTime.Now - this.lastUpdate < TimeSpan.FromSeconds(5.0))
        return;
      this.lastUpdate = DateTime.Now;
      this.AllStatistics = this.backendBridge.Notifications.GetStatisticsSnapshots();
      this.SetCurrentStatistics();
      this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.ShouldRemindToEnableStatistics));
      this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.CanNavigate));
    }

    public void Next()
    {
      if (this.AllStatistics.Length == 0)
        return;
      ++this.currentIndex;
      if (this.currentIndex >= this.AllStatistics.Length)
        this.currentIndex = 0;
      this.SetCurrentStatistics();
    }

    private void SetCurrentStatistics()
    {
      if (this.AllStatistics == null || this.AllStatistics.Length == 0 || this.AllStatistics[this.currentIndex] == null)
        return;
      this.currentStatisticsSet = this.AllStatistics[this.currentIndex].Statistics;
      this.DisplayName = this.AllStatistics[this.currentIndex].Name;
      if (this.SortedStatistics.Count != this.currentStatisticsSet.Count)
        this.InitializeStatistics();
      else
        this.UpdateExistingStatistics();
      this.NotifyOfPropertyChange<string>((Expression<Func<string>>) (() => this.DisplayName));
    }

    private void InitializeStatistics()
    {
      this.SortedStatistics.Clear();
      this.statisticLookup.Clear();
      foreach (KeyValuePair<string, object> keyValuePair in (IEnumerable<KeyValuePair<string, object>>) this.currentStatisticsSet.OrderBy<KeyValuePair<string, object>, string>((Func<KeyValuePair<string, object>, string>) (x => x.Key)))
      {
        StatisticEntry statisticEntry = new StatisticEntry(this.KeyToWordUsingCache(keyValuePair.Key))
        {
          Value = keyValuePair.Value
        };
        this.SortedStatistics.Add(statisticEntry);
        this.statisticLookup.Add(keyValuePair.Key, statisticEntry);
      }
    }

    private void UpdateExistingStatistics()
    {
      foreach (KeyValuePair<string, object> currentStatistics in this.currentStatisticsSet)
        this.statisticLookup[currentStatistics.Key].Value = currentStatistics.Value;
    }

    public void Previous()
    {
      if (this.AllStatistics.Length == 0)
        return;
      --this.currentIndex;
      if (this.currentIndex < 0)
        this.currentIndex = this.AllStatistics.Length - 1;
      this.SetCurrentStatistics();
    }

    public override void ForceUpdate()
    {
      throw new NotImplementedException();
    }
  }
}
