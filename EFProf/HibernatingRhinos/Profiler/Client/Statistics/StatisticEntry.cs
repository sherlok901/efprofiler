﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Statistics.StatisticEntry
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using System;
using System.Linq.Expressions;

namespace HibernatingRhinos.Profiler.Client.Statistics
{
  public class StatisticEntry : PropertyChangedBase
  {
    private object value;

    public StatisticEntry(string label)
    {
      this.Label = label;
    }

    public string Label { get; private set; }

    public object Value
    {
      get
      {
        return this.value;
      }
      set
      {
        this.value = value;
        this.NotifyOfPropertyChange<object>((Expression<Func<object>>) (() => this.Value));
      }
    }
  }
}
