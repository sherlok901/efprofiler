﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Queries.QueryExecuter
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.BackEnd.Infrastructure;
using HibernatingRhinos.Profiler.BackEnd.Messages;
using log4net;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;

namespace HibernatingRhinos.Profiler.Client.Queries
{
  public class QueryExecuter
  {
    private static readonly ILog Logger = LogManager.GetLogger(typeof (QueryExecuter));

    public event Action<string, Exception> Error = (param0, param1) => {};

    public DataSet Data { get; private set; }

    public StatementSnapshot Statement { get; private set; }

    public QueryExecuter(StatementSnapshot statement)
    {
      this.Statement = statement;
    }

    public void Execute(ConnectionInfo connectionInfo)
    {
      this.Data = new DataSet();
      if (!this.Statement.CanExecuteQuery)
      {
        this.GotError("Only select statements can be executed.", (Exception) null);
      }
      else
      {
        IDbConnection dbConnection = (IDbConnection) null;
        try
        {
          if (string.IsNullOrEmpty(connectionInfo.ConnectionString))
            this.GotError("Can not execute query when connection string is not valid.", (Exception) null);
          else if (string.IsNullOrEmpty(connectionInfo.ConnectionTypeName))
          {
            this.GotError("Can not execute query when the connection type is not specified.", (Exception) null);
          }
          else
          {
            Type type = (Type) null;
            try
            {
              type = QueryExecuter.GetTypeForConnection(connectionInfo);
            }
            catch (Exception ex)
            {
              this.GotError(string.Format("Could not load connection type: {0}. {1}", (object) connectionInfo.ConnectionTypeName, (object) ex), (Exception) null);
            }
            if (type == (Type) null)
            {
              this.GotError("Could not find connection type: " + connectionInfo.ConnectionTypeName, (Exception) null);
            }
            else
            {
              try
              {
                dbConnection = (IDbConnection) Activator.CreateInstance(type);
              }
              catch (Exception ex)
              {
                this.GotError("Could not create an instance of " + connectionInfo.ConnectionTypeName, ex);
                return;
              }
              Assembly assembly = dbConnection.GetType().Assembly;
              ResolveEventHandler resolveEventHandler = (ResolveEventHandler) ((sender, args) =>
              {
                if (args.Name == assembly.FullName)
                  return assembly;
                return (Assembly) null;
              });
              AppDomain.CurrentDomain.AssemblyResolve += resolveEventHandler;
              try
              {
                dbConnection.ConnectionString = connectionInfo.ConnectionString;
                dbConnection.Open();
              }
              catch (Exception ex)
              {
                this.GotError("Could not open connection", ex);
                return;
              }
              finally
              {
                AppDomain.CurrentDomain.AssemblyResolve -= resolveEventHandler;
              }
              using (IDbTransaction dbTransaction = dbConnection.BeginTransaction())
              {
                using (IDbCommand command = dbConnection.CreateCommand())
                {
                  command.Transaction = dbTransaction;
                  command.CommandText = this.Statement.RawSql;
                  if (this.Statement.Parameters != null)
                  {
                    foreach (Parameter parameter1 in this.Statement.Parameters)
                    {
                      IDbDataParameter parameter2 = command.CreateParameter();
                      parameter2.ParameterName = parameter1.Name;
                      parameter2.Value = parameter1.GetParameterValue();
                      SqlParameter sqlParameter = parameter2 as SqlParameter;
                      if (sqlParameter != null && sqlParameter.SqlDbType == SqlDbType.Structured && string.IsNullOrEmpty(sqlParameter.TypeName))
                        sqlParameter.TypeName = "IntegerListTableType";
                      command.Parameters.Add((object) parameter2);
                    }
                  }
                  IDataReader dataReader;
                  try
                  {
                    dataReader = command.ExecuteReader();
                  }
                  catch (Exception ex1)
                  {
                    QueryExecuter.Logger.Info((object) ("Failed to execute query: " + this.Statement.RawSql), ex1);
                    try
                    {
                      command.CommandText = this.Statement.FormattedSql;
                      dataReader = command.ExecuteReader();
                    }
                    catch (Exception ex2)
                    {
                      QueryExecuter.Logger.Info((object) ("Failed to execute formatted query: " + this.Statement.FormattedSql), ex1);
                      this.GotError("Error when executing statement", ex1);
                      return;
                    }
                  }
                  try
                  {
                    do
                    {
                      DataTable dataTable = this.Data.Tables.Add();
                      foreach (DataRow row in (InternalDataCollectionBase) dataReader.GetSchemaTable().Rows)
                      {
                        string str = (string) row["ColumnName"];
                        while (dataTable.Columns.Contains(str))
                          str += " ";
                        dataTable.Columns.Add(str);
                      }
                      while (dataReader.Read() && dataTable.Rows.Count < 500)
                      {
                        DataRow row = dataTable.NewRow();
                        for (int index = 0; index < dataTable.Columns.Count; ++index)
                          row[index] = QueryExecuter.FormatForDisplay(dataReader[index]);
                        dataTable.Rows.Add(row);
                      }
                    }
                    while (dataReader.NextResult());
                  }
                  catch (Exception ex)
                  {
                    this.GotError("Could not read data from statement", ex);
                    return;
                  }
                  finally
                  {
                    if (dataReader != null)
                      dataReader.Dispose();
                  }
                  dbTransaction.Rollback();
                }
              }
            }
          }
        }
        catch (Exception ex)
        {
          this.GotError("Error when executing statement", ex);
        }
        finally
        {
          if (dbConnection != null)
            dbConnection.Dispose();
        }
      }
    }

    private void GotError(string message, Exception e)
    {
      string str = "Cannot execute a query. " + message + Environment.NewLine + "Win32 Error: " + (object) Marshal.GetLastWin32Error();
      if (e != null)
        str = str + Environment.NewLine + (object) e;
      QueryExecuter.Logger.Debug((object) str);
      this.Error(message, e);
    }

    private static object FormatForDisplay(object o)
    {
      if (o == DBNull.Value)
        return (object) "NULL";
      byte[] numArray = o as byte[];
      if (numArray == null)
        return o;
      StringBuilder stringBuilder = new StringBuilder("0x");
      foreach (byte num in numArray)
        stringBuilder.Append(num.ToString("X2"));
      return (object) stringBuilder.ToString();
    }

    private static Type GetTypeForConnection(ConnectionInfo connectionInfo)
    {
      if (string.IsNullOrEmpty(connectionInfo.FullPathToAssembly))
        return Type.GetType(connectionInfo.ConnectionTypeName);
      return Assembly.LoadFrom(connectionInfo.FullPathToAssembly).GetType(connectionInfo.ConnectionTypeName);
    }
  }
}
