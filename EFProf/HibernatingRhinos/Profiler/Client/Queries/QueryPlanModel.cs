﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Queries.QueryPlanModel
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using HibernatingRhinos.Profiler.BackEnd.Infrastructure;
using HibernatingRhinos.Profiler.BackEnd.Infrastructure.Connections;
using HibernatingRhinos.Profiler.BackEnd.Profiles;
using HibernatingRhinos.Profiler.Client.Connections;
using HibernatingRhinos.Profiler.Client.Controls;
using HibernatingRhinos.Profiler.Client.Infrastructure;
using HibernatingRhinos.Profiler.Client.Queries.QueryPlan;
using HibernatingRhinos.Profiler.Client.Services;
using HibernatingRhinos.Profiler.Client.Settings;
using HibernatingRhinos.Profiler.Client.Statements;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Windows;

namespace HibernatingRhinos.Profiler.Client.Queries
{
  public class QueryPlanModel : ProfilerScreen
  {
    private readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof (QueryPlanModel));
    private const double PercentOfCostlyItems = 0.05;
    private QueryPlanStepModel selectedStep;
    private bool displayZoomPanel;
    private IList<QueryPlanStepModel> steps;

    public QueryPlanModel(IStatementModel statement)
    {
      this.Statement = statement;
      this.DisplayName = "Query Plan";
      this.Steps = (IList<QueryPlanStepModel>) new List<QueryPlanStepModel>();
    }

    private IStatementModel Statement { get; set; }

    public bool DisplayZoomPanel
    {
      get
      {
        return this.displayZoomPanel;
      }
      set
      {
        this.displayZoomPanel = value;
        this.NotifyOfPropertyChange<bool>((System.Linq.Expressions.Expression<Func<bool>>) (() => this.DisplayZoomPanel));
      }
    }

    public bool UserHasHiddenZoomPanel { get; private set; }

    public event System.Action StepsChanged = () => {};

    public void Query()
    {
      ConnectionInfo connectionInfo = this.EnsureConnection();
      if (connectionInfo == null || !this.UpdateQueryPlan(this.Statement.Text, connectionInfo))
        return;
      Dialog.Show((object) this, (object) null, false);
    }

    private void AnalyzeStepCost()
    {
      List<QueryPlanStepModel> list = this.Steps.ToList<QueryPlanStepModel>();
      list.Sort(new Comparison<QueryPlanStepModel>(QueryPlanModel.QueryStepsComparer));
      list.Reverse();
      int count = (int) Math.Ceiling((double) list.Count * 0.05);
      foreach (QueryPlanStepModel queryPlanStepModel in list.Take<QueryPlanStepModel>(count))
        queryPlanStepModel.IsCostly = true;
    }

    private void InitializeStepsModel(QueryPlanStep rootStep)
    {
      QueryPlanStepModel root = rootStep.Children.Count<QueryPlanStep>() == 1 ? new QueryPlanStepModel(rootStep.Children.First<QueryPlanStep>()) : new QueryPlanStepModel(rootStep);
      List<QueryPlanStepModel> allStepsFromRoot = QueryPlanModel.GetAllStepsFromRoot(root);
      allStepsFromRoot.Insert(0, root);
      this.Steps = (IList<QueryPlanStepModel>) allStepsFromRoot;
    }

    private static int QueryStepsComparer(QueryPlanStepModel x, QueryPlanStepModel y)
    {
      double result1;
      if (!double.TryParse(x.Cost, out result1))
        result1 = 0.0;
      double result2;
      if (!double.TryParse(y.Cost, out result2))
        result2 = 0.0;
      if (result1 == result2)
        return 0;
      return result1 <= result2 ? -1 : 1;
    }

    private static List<QueryPlanStepModel> GetAllStepsFromRoot(QueryPlanStepModel root)
    {
      List<QueryPlanStepModel> queryPlanStepModelList = new List<QueryPlanStepModel>();
      queryPlanStepModelList.AddRange((IEnumerable<QueryPlanStepModel>) root.Children);
      foreach (QueryPlanStepModel child in root.Children)
        queryPlanStepModelList.AddRange((IEnumerable<QueryPlanStepModel>) QueryPlanModel.GetAllStepsFromRoot(child));
      return queryPlanStepModelList;
    }

    public IList<QueryPlanStepModel> Steps
    {
      get
      {
        return this.steps;
      }
      set
      {
        this.steps = value;
        this.NotifyOfPropertyChange<IList<QueryPlanStepModel>>((System.Linq.Expressions.Expression<Func<IList<QueryPlanStepModel>>>) (() => this.Steps));
      }
    }

    public void Close()
    {
      this.TryClose();
    }

    public void EditConnection()
    {
      if (!QueryPlanModel.ShowEditConnectionDialog())
        return;
      ConnectionInfo currentConnection = UserPreferencesHolder.UserSettings.GetCurrentConnection();
      if (currentConnection == null)
        return;
      this.UpdateQueryPlan(this.Statement.Text, currentConnection);
    }

    public void SelectedStepChanged(QueryPlanStepModel step)
    {
      this.SelectedStep = step;
    }

    public QueryPlanStepModel SelectedStep
    {
      get
      {
        return this.selectedStep;
      }
      set
      {
        this.selectedStep = value;
        this.NotifyOfPropertyChange<QueryPlanStepModel>((System.Linq.Expressions.Expression<Func<QueryPlanStepModel>>) (() => this.SelectedStep));
      }
    }

    public void ToggleZoomPanel()
    {
      this.UserHasHiddenZoomPanel = true;
      this.DisplayZoomPanel = !this.DisplayZoomPanel;
    }

    private ConnectionInfo EnsureConnection()
    {
      ConnectionInfo currentConnection = UserPreferencesHolder.UserSettings.GetCurrentConnection();
      if (currentConnection == null && QueryPlanModel.ShowEditConnectionDialog())
        currentConnection = UserPreferencesHolder.UserSettings.GetCurrentConnection();
      return currentConnection;
    }

    private static bool ShowEditConnectionDialog()
    {
      EditConnectionModel editConnectionModel = IoC.Get<EditConnectionModel>((string) null);
      editConnectionModel.RequireConnection = true;
      return Dialog.Show((object) editConnectionModel, (object) null, false);
    }

    private bool UpdateQueryPlan(string sql, ConnectionInfo connectionInfo)
    {
      Type type = QueryPlanModel.InferServiceTypeFromConnection(connectionInfo);
      ConnectionBuilder.Result result = ConnectionBuilder.BuildConnectionFromInfo(connectionInfo);
      Assembly assembly = result.Connection.GetType().Assembly;
      ResolveEventHandler resolveEventHandler = (ResolveEventHandler) ((sender, args) =>
      {
        if (args.Name == assembly.FullName)
          return assembly;
        return (Assembly) null;
      });
      AppDomain.CurrentDomain.AssemblyResolve += resolveEventHandler;
      if (!result.WasSuccessful)
        return false;
      using (IDbConnection connection = result.Connection)
      {
        try
        {
          connection.ConnectionString = connectionInfo.ConnectionString;
          IShowPlanService instance = (IShowPlanService) Activator.CreateInstance(type, new object[2]
          {
            (object) sql,
            (object) connection
          });
          if (connection.State != ConnectionState.Open)
            connection.Open();
          this.LoadPlanFrom(instance.GetPlan());
          try
          {
            this.StepsChanged();
          }
          catch
          {
          }
          return true;
        }
        catch (Exception ex)
        {
          this.log.Error((object) "Could not get query plan", ex);
          int num = (int) MessageBox.Show(ex.Message, "Could not get query plan", MessageBoxButton.OK, MessageBoxImage.Hand);
          return false;
        }
        finally
        {
          AppDomain.CurrentDomain.AssemblyResolve -= resolveEventHandler;
        }
      }
    }

    private static Type InferServiceTypeFromConnection(ConnectionInfo connection)
    {
      string name = Profile.Current.GetAvailableConnectionTypes().First<IConnectionDefinition>((Func<IConnectionDefinition, bool>) (x => x.ClassName == connection.ConnectionTypeName)).GetType().Name;
      string serviceTypeName = name.Replace("ConnectionDefinition", "ShowPlanService");
      Type type = ((IEnumerable<Type>) Assembly.GetExecutingAssembly().GetTypes()).FirstOrDefault<Type>((Func<Type, bool>) (x => x.Name == serviceTypeName));
      if (type == (Type) null)
        throw new NotSupportedException(string.Format("Unable to locate an implementation of {2} that supports {0}. Expected to find a type named {1}. ", (object) name, (object) serviceTypeName, (object) typeof (IShowPlanService).Name));
      return type;
    }

    public void LoadPlanFrom(QueryPlanStep rootStep)
    {
      this.InitializeStepsModel(rootStep);
      this.AnalyzeStepCost();
    }
  }
}
