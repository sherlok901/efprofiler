﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Queries.QueryPlanStepModel
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.Client.Queries.QueryPlan;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HibernatingRhinos.Profiler.Client.Queries
{
  public class QueryPlanStepModel
  {
    private readonly QueryPlanStep step;
    private readonly QueryPlanStepModel parent;

    public QueryPlanStepModel(QueryPlanStep step)
      : this(step, (QueryPlanStepModel) null)
    {
    }

    public QueryPlanStepModel(QueryPlanStep step, QueryPlanStepModel parent)
    {
      this.step = step;
      this.parent = parent;
      this.NestedLevel = parent == null ? 0 : parent.NestedLevel + 1;
      this.Children = this.step.Children.Select<QueryPlanStep, QueryPlanStepModel>((Func<QueryPlanStep, QueryPlanStepModel>) (child => new QueryPlanStepModel(child, this))).ToList<QueryPlanStepModel>();
    }

    public int NestedLevel { get; private set; }

    public List<QueryPlanStepModel> Children { get; private set; }

    public string Name
    {
      get
      {
        return this.step.Name;
      }
    }

    public string Cost
    {
      get
      {
        string str;
        if (this.step.VisibleProperties.TryGetValue(nameof (Cost), out str))
          return str;
        return "";
      }
    }

    public string Statement
    {
      get
      {
        return this.step.VisibleProperties[nameof (Statement)];
      }
    }

    public IDictionary<string, string> VisibleProperties
    {
      get
      {
        return this.step.VisibleProperties;
      }
    }

    public IDictionary<string, string> AllProperties
    {
      get
      {
        return this.step.AllProperties;
      }
    }

    public bool IsCostly { get; set; }

    public QueryPlanStepModel Parent
    {
      get
      {
        return this.parent;
      }
    }
  }
}
