﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Queries.QueryPlanStepView
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace HibernatingRhinos.Profiler.Client.Queries
{
  public partial class QueryPlanStepView : UserControl, IComponentConnector
  {
    //internal RadioButton Step;
    //private bool _contentLoaded;

    public QueryPlanStepView()
    {
      this.InitializeComponent();
    }

    public QueryPlanStepModel Model
    {
      get
      {
        return (QueryPlanStepModel) this.DataContext;
      }
    }

    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //public void InitializeComponent()
    //{
    //  if (this._contentLoaded)
    //    return;
    //  this._contentLoaded = true;
    //  Application.LoadComponent((object) this, new Uri("/EFProf;component/queries/queryplanstepview.xaml", UriKind.Relative));
    //}

    //[EditorBrowsable(EditorBrowsableState.Never)]
    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //void IComponentConnector.Connect(int connectionId, object target)
    //{
    //  if (connectionId == 1)
    //    this.Step = (RadioButton) target;
    //  else
    //    this._contentLoaded = true;
    //}
  }
}
