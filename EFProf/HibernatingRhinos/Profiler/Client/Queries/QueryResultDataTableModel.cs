﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Queries.QueryResultDataTableModel
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.Client.Infrastructure;
using System;
using System.Data;
using System.Linq.Expressions;

namespace HibernatingRhinos.Profiler.Client.Queries
{
  public class QueryResultDataTableModel : SelectionBase
  {
    private DataTable _innerTable;

    public QueryResultDataTableModel(DataTable table)
    {
      this.InnerTable = table;
    }

    public DataTable InnerTable
    {
      get
      {
        return this._innerTable;
      }
      set
      {
        this._innerTable = value;
        this.NotifyOfPropertyChange<DataTable>((Expression<Func<DataTable>>) (() => this.InnerTable));
      }
    }
  }
}
