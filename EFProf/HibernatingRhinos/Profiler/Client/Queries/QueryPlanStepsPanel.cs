﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Queries.QueryPlanStepsPanel
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace HibernatingRhinos.Profiler.Client.Queries
{
  public class QueryPlanStepsPanel : Panel
  {
    public static readonly DependencyProperty StepPaddingProperty = DependencyProperty.Register(nameof (StepPadding), typeof (double), typeof (QueryPlanStepsPanel), new PropertyMetadata((object) 48.0));
    private readonly List<QueryPlanStepsPanel.LayoutData> layoutdata = new List<QueryPlanStepsPanel.LayoutData>();

    public double StepPadding
    {
      get
      {
        return Convert.ToDouble(this.GetValue(QueryPlanStepsPanel.StepPaddingProperty));
      }
      set
      {
        this.SetValue(QueryPlanStepsPanel.StepPaddingProperty, (object) value);
      }
    }

    protected override Size MeasureOverride(Size constraint)
    {
      this.layoutdata.Clear();
      Size availableSize = new Size(double.PositiveInfinity, double.PositiveInfinity);
      foreach (UIElement child in this.Children)
      {
        child.Measure(availableSize);
        ContentPresenter contentPresenter = child as ContentPresenter;
        if (contentPresenter != null)
        {
          QueryPlanStepModel content = contentPresenter.Content as QueryPlanStepModel;
          if (content != null)
            this.layoutdata.Add(new QueryPlanStepsPanel.LayoutData(child, content));
        }
      }
      Size size = new Size(0.0, 0.0);
      foreach (QueryPlanStepsPanel.LayoutData layoutData in this.layoutdata)
      {
        QueryPlanStepsPanel.LayoutData parent = this.layoutdata.FromQueryPlanStepModel(layoutData.Step.Parent);
        double x = parent == null ? 0.0 : parent.X + parent.DesiredSize.Width + this.StepPadding;
        double y = parent == null ? 0.0 : this.DetermineYWithRespectToSiblings(parent, layoutData);
        Rect rect = layoutData.InitializeDestination(x, y);
        if (rect.X + rect.Width > size.Width)
          size.Width = rect.X + rect.Width;
        if (rect.Y + rect.Height > size.Height)
          size.Height = rect.Y + rect.Height;
      }
      double num = Math.Max(size.Width, size.Height);
      return new Size(num, num);
    }

    protected override Size ArrangeOverride(Size finalSize)
    {
      foreach (QueryPlanStepsPanel.LayoutData layoutData in this.layoutdata)
        layoutData.Element.Arrange(layoutData.Destination);
      return finalSize;
    }

    private double DetermineYWithRespectToSiblings(QueryPlanStepsPanel.LayoutData parent, QueryPlanStepsPanel.LayoutData item)
    {
      return parent.Step.Children.TakeWhile<QueryPlanStepModel>((Func<QueryPlanStepModel, bool>) (x => x != item.Step)).Select<QueryPlanStepModel, QueryPlanStepsPanel.LayoutData>((Func<QueryPlanStepModel, QueryPlanStepsPanel.LayoutData>) (model => this.layoutdata.FromQueryPlanStepModel(model))).Select<QueryPlanStepsPanel.LayoutData, double>((Func<QueryPlanStepsPanel.LayoutData, double>) (layout => layout.DesiredHeightIncludingChildren((IList<QueryPlanStepsPanel.LayoutData>) this.layoutdata))).Sum() + parent.Y;
    }

    public class LayoutData
    {
      private readonly UIElement element;
      private readonly QueryPlanStepModel step;
      private Rect? destination;
      private double? desiredHeightIncludingChildren;

      public LayoutData(UIElement element, QueryPlanStepModel step)
      {
        this.element = element;
        this.step = step;
      }

      public Size DesiredSize
      {
        get
        {
          return this.Element.DesiredSize;
        }
      }

      public UIElement Element
      {
        get
        {
          return this.element;
        }
      }

      public QueryPlanStepModel Step
      {
        get
        {
          return this.step;
        }
      }

      public double X { get; set; }

      public double Y { get; set; }

      public Rect Destination
      {
        get
        {
          if (this.destination.HasValue)
            return this.destination.Value;
          throw new Exception("The destination rectangle has not been initialized. Call InitializeDestination(x,y) first.");
        }
      }

      public Rect InitializeDestination(double x, double y)
      {
        this.X = x;
        this.Y = y;
        this.destination = new Rect?(new Rect(x, y, this.DesiredSize.Width, this.DesiredSize.Height));
        return this.destination.Value;
      }

      public double DesiredHeightIncludingChildren(IList<QueryPlanStepsPanel.LayoutData> layouts)
      {
        if (this.desiredHeightIncludingChildren.HasValue)
          return this.desiredHeightIncludingChildren.Value;
        this.desiredHeightIncludingChildren = new double?(Math.Max(this.step.Children.Select<QueryPlanStepModel, QueryPlanStepsPanel.LayoutData>((Func<QueryPlanStepModel, QueryPlanStepsPanel.LayoutData>) (child => layouts.FromQueryPlanStepModel(child))).Select<QueryPlanStepsPanel.LayoutData, double>((Func<QueryPlanStepsPanel.LayoutData, double>) (layout => layout.DesiredHeightIncludingChildren(layouts))).Sum(), this.DesiredSize.Height));
        return this.desiredHeightIncludingChildren.Value;
      }
    }
  }
}
