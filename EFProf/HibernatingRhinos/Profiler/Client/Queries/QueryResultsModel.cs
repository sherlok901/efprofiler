﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Queries.QueryResultsModel
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.BackEnd.Infrastructure;
using HibernatingRhinos.Profiler.Client.Connections;
using HibernatingRhinos.Profiler.Client.Controls;
using HibernatingRhinos.Profiler.Client.Infrastructure;
using HibernatingRhinos.Profiler.Client.Services;
using HibernatingRhinos.Profiler.Client.Settings;
using HibernatingRhinos.Profiler.Client.Tracking;
using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq.Expressions;

namespace HibernatingRhinos.Profiler.Client.Queries
{
  public class QueryResultsModel : ProfilerScreen
  {
    private readonly ITrackingService trackingService;
    private ObservableCollection<QueryResultDataTableModel> tables;
    private QueryResultDataTableModel selectedTable;
    private StatementSnapshot snapshot;
    private Exception exception;

    public QueryResultsModel(StatementSnapshot snapshot, ITrackingService trackingService)
    {
      this.trackingService = trackingService;
      this.snapshot = snapshot;
      this.Tables = new ObservableCollection<QueryResultDataTableModel>();
      this.DisplayName = "Query Results";
    }

    public bool GetQueryResults()
    {
      ConnectionInfo currentConnection = UserPreferencesHolder.UserSettings.GetCurrentConnection();
      if (currentConnection == null)
      {
        if (!Dialog.Show<EditConnectionModel>((object) null, false))
          return false;
        return this.GetQueryResults();
      }
      if (!ConnectionBuilder.BuildConnectionFromInfo(currentConnection).WasSuccessful)
        return false;
      this.Exception = (Exception) null;
      this.Tables.Clear();
      QueryExecuter queryExecuter = new QueryExecuter(this.snapshot);
      queryExecuter.Error += new Action<string, Exception>(this.DisplayError);
      queryExecuter.Execute(currentConnection);
      foreach (DataTable table in (InternalDataCollectionBase) queryExecuter.Data.Tables)
        this.Tables.Add(new QueryResultDataTableModel(table));
      if (this.Tables.Count > 0)
        this.SelectedTable = this.Tables[0];
      return true;
    }

    public void Close()
    {
      this.TryClose();
    }

    public void EditConnection()
    {
      if (!Dialog.Show((object) new EditConnectionModel(this.trackingService), (object) null, false))
        return;
      this.GetQueryResults();
    }

    public void DisplayError(string error, Exception exception)
    {
      this.Exception = new Exception(error, exception);
    }

    public ObservableCollection<QueryResultDataTableModel> Tables
    {
      get
      {
        return this.tables;
      }
      set
      {
        this.tables = value;
        this.NotifyOfPropertyChange<ObservableCollection<QueryResultDataTableModel>>((Expression<Func<ObservableCollection<QueryResultDataTableModel>>>) (() => this.Tables));
      }
    }

    public QueryResultDataTableModel SelectedTable
    {
      get
      {
        return this.selectedTable;
      }
      set
      {
        if (this.selectedTable != null)
          this.selectedTable.IsSelected = false;
        this.selectedTable = value;
        if (this.selectedTable != null)
          this.selectedTable.IsSelected = true;
        this.NotifyOfPropertyChange<QueryResultDataTableModel>((Expression<Func<QueryResultDataTableModel>>) (() => this.SelectedTable));
      }
    }

    public StatementSnapshot Snapshot
    {
      get
      {
        return this.snapshot;
      }
      set
      {
        this.snapshot = value;
        this.NotifyOfPropertyChange<StatementSnapshot>((Expression<Func<StatementSnapshot>>) (() => this.Snapshot));
      }
    }

    public Exception Exception
    {
      get
      {
        return this.exception;
      }
      set
      {
        this.exception = value;
        this.NotifyOfPropertyChange<Exception>((Expression<Func<Exception>>) (() => this.Exception));
      }
    }
  }
}
