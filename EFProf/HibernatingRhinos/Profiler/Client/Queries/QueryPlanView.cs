﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Queries.QueryPlanView
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HibernatingRhinos.Profiler.Client.Queries
{
  public partial class QueryPlanView : UserControl, IComponentConnector
  {
    private const float VerticalPadding = 4f;
    private const double ZoomFactor = 0.05;
    private Point start;
    private Point origin;
    private Point vaClickPoint;
    private Point vaPosition;
    private bool draggingMiniMap;
    private bool rootMouseCaptured;
    private bool visibleAreaMouseCaptured;
    //internal ScrollViewer rootContainer;
    //internal Grid gridContainer;
    //internal ScaleTransform rootScale;
    //internal Canvas connectionCanvas;
    //internal ItemsControl root;
    //internal Grid ZoomPanel;
    //internal Button button;
    //internal Border MiniMapContainer;
    //internal Canvas MiniMap;
    //internal ImageBrush MiniMapBackground;
    //internal Rectangle VisibleArea;
    //internal Slider zoomSlider;
    //private bool _contentLoaded;

    public QueryPlanView()
    {
      this.InitializeComponent();
      this.DataContextChanged += (DependencyPropertyChangedEventHandler) ((s, e) => this.Model.StepsChanged += new Action(this.SetupStepsView));
      this.gridContainer.SizeChanged += (SizeChangedEventHandler) ((param0, param1) => this.SetupStepsView());
      this.rootContainer.ScrollChanged += new ScrollChangedEventHandler(this.ScrollViewerChanged);
    }

    private QueryPlanModel Model
    {
      get
      {
        return this.DataContext as QueryPlanModel;
      }
    }

    private void SetupStepsView()
    {
      if (this.gridContainer.ActualWidth <= 0.0 || this.gridContainer.ActualHeight <= 0.0)
        return;
      this.rootContainer.UpdateLayout();
      this.DrawConnectingLines(this.connectionCanvas);
      RenderTargetBitmap renderTargetBitmap = new RenderTargetBitmap((int) this.gridContainer.ActualWidth, (int) this.gridContainer.ActualHeight, 96.0, 96.0, PixelFormats.Pbgra32);
      renderTargetBitmap.Render((Visual) this.gridContainer);
      this.MiniMapBackground.ImageSource = (ImageSource) renderTargetBitmap;
    }

    private void DisplayMiniMap()
    {
      if (this.Model.UserHasHiddenZoomPanel)
        return;
      this.Model.DisplayZoomPanel = true;
    }

    private void HandleMouseWheel(object sender, MouseWheelEventArgs e)
    {
      if (!Keyboard.Modifiers.HasFlag((Enum) ModifierKeys.Control))
        return;
      double num = e.Delta > 0 ? 0.05 : -0.05;
      this.rootScale.ScaleX += num;
      this.rootScale.ScaleY += num;
      if (this.rootScale.ScaleX < this.zoomSlider.Minimum)
        this.rootScale.ScaleX = this.rootScale.ScaleY = this.zoomSlider.Minimum;
      if (this.rootScale.ScaleX > this.zoomSlider.Maximum)
        this.rootScale.ScaleX = this.rootScale.ScaleY = this.zoomSlider.Maximum;
      if (this.rootScale.ScaleX != 1.0)
        this.DisplayMiniMap();
      this.UpdateMiniMap();
      e.Handled = true;
    }

    private void UpdateMiniMap()
    {
      if (this.root == null)
        return;
      double num1 = this.gridContainer.ActualWidth * this.rootScale.ScaleX;
      double num2 = this.gridContainer.ActualHeight * this.rootScale.ScaleY;
      double num3 = this.rootContainer.ViewportWidth / num1;
      double num4 = this.rootContainer.ViewportHeight / num2;
      this.VisibleArea.Width = Math.Min(this.MiniMapContainer.Width * num3, this.MiniMapContainer.Width);
      this.VisibleArea.Height = Math.Min(this.MiniMapContainer.Height * num4, this.MiniMapContainer.Height);
      double length1 = this.MiniMapContainer.Width * (this.rootContainer.HorizontalOffset / this.rootContainer.ExtentWidth);
      double length2 = this.MiniMapContainer.Height * (this.rootContainer.VerticalOffset / this.rootContainer.ExtentHeight);
      Canvas.SetLeft((UIElement) this.VisibleArea, length1);
      Canvas.SetTop((UIElement) this.VisibleArea, length2);
    }

    private void SliderValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
    {
      this.UpdateMiniMap();
    }

    private void HandleMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
    {
      this.start = e.GetPosition((IInputElement) this.rootContainer);
      this.origin = new Point(this.rootContainer.HorizontalOffset, this.rootContainer.VerticalOffset);
      this.root.CaptureMouse();
      this.rootMouseCaptured = true;
    }

    private void HandleMouseMove(object sender, MouseEventArgs e)
    {
      if (!this.rootMouseCaptured)
        return;
      Point point = this.start.Subtract(e.GetPosition((IInputElement) this.rootContainer));
      this.rootContainer.ScrollToHorizontalOffset(this.origin.X + point.X);
      this.rootContainer.ScrollToVerticalOffset(this.origin.Y + point.Y);
      this.UpdateMiniMap();
    }

    private void HandleMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
    {
      this.root.ReleaseMouseCapture();
      this.rootMouseCaptured = false;
      this.UpdateMiniMap();
    }

    private void HandleMiniMapClick(object sender, MouseButtonEventArgs e)
    {
      if (this.visibleAreaMouseCaptured)
        return;
      Point position = e.GetPosition((IInputElement) this.MiniMap);
      Canvas.SetLeft((UIElement) this.VisibleArea, position.X);
      Canvas.SetTop((UIElement) this.VisibleArea, position.Y);
      double num1 = this.VisibleArea.ActualWidth / 2.0;
      double num2 = this.VisibleArea.ActualHeight / 2.0;
      double num3 = (position.X - num1) / this.MiniMap.ActualWidth;
      double num4 = (position.Y - num2) / this.MiniMap.ActualHeight;
      double offset1 = this.rootContainer.ExtentWidth * num3;
      double offset2 = this.rootContainer.ExtentHeight * num4;
      this.rootContainer.ScrollToHorizontalOffset(offset1);
      this.rootContainer.ScrollToVerticalOffset(offset2);
      e.Handled = true;
    }

    private void VisibleAreaMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
    {
      this.draggingMiniMap = true;
      this.vaClickPoint = e.GetPosition((IInputElement) this.MiniMap);
      this.vaPosition = new Point(Canvas.GetLeft((UIElement) this.VisibleArea), Canvas.GetTop((UIElement) this.VisibleArea));
      this.VisibleArea.CaptureMouse();
      this.visibleAreaMouseCaptured = true;
      e.Handled = true;
    }

    private void VisibleAreaMouseMove(object sender, MouseEventArgs e)
    {
      if (!this.visibleAreaMouseCaptured)
        return;
      Point point = this.vaPosition.Subtract(this.vaClickPoint.Subtract(e.GetPosition((IInputElement) this.MiniMap)));
      if (point.X >= 0.0 && this.VisibleArea.Width + point.X <= this.MiniMapContainer.Width)
      {
        Canvas.SetLeft((UIElement) this.VisibleArea, point.X);
        double num = this.VisibleArea.ActualWidth / 2.0;
        this.rootContainer.ScrollToHorizontalOffset(this.rootContainer.ExtentWidth * ((point.X - num) / this.MiniMap.ActualWidth));
      }
      if (point.Y < 0.0 || this.VisibleArea.Height + point.Y > this.MiniMapContainer.Height)
        return;
      Canvas.SetTop((UIElement) this.VisibleArea, point.Y);
      double num1 = this.VisibleArea.ActualHeight / 2.0;
      this.rootContainer.ScrollToVerticalOffset(this.rootContainer.ExtentHeight * ((point.Y - num1) / this.MiniMap.ActualHeight));
    }

    private void VisibleAreaMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
    {
      this.draggingMiniMap = false;
      this.VisibleArea.ReleaseMouseCapture();
      this.visibleAreaMouseCaptured = false;
    }

    private void ScrollViewerChanged(object sender, ScrollChangedEventArgs e)
    {
      if (this.draggingMiniMap)
        return;
      this.UpdateMiniMap();
    }

    private void HandleResetClick(object sender, RoutedEventArgs e)
    {
      this.rootScale.ScaleX = this.rootScale.ScaleY = 1.0;
      this.rootContainer.ScrollToHorizontalOffset(0.0);
      this.rootContainer.ScrollToVerticalOffset(0.0);
      this.UpdateMiniMap();
    }

    public void DrawConnectingLines(Canvas canvas)
    {
      foreach (Line line in QueryPlanView.FindChildrenByType<Line>((DependencyObject) canvas).ToList<Line>())
        canvas.Children.Remove((UIElement) line);
      IEnumerable<QueryPlanStepView> childrenByType = QueryPlanView.FindChildrenByType<QueryPlanStepView>((DependencyObject) this.root);
      foreach (QueryPlanStepView step1 in childrenByType)
      {
        Point origin = this.OriginForParent(step1);
        double num1 = QueryPlanView.LengthForHorizontalSegment(step1) + 10.0;
        int num2 = 0;
        foreach (QueryPlanStepModel child in step1.Model.Children)
        {
          QueryPlanStepModel childModel = child;
          QueryPlanStepView step2 = childrenByType.First<QueryPlanStepView>((Func<QueryPlanStepView, bool>) (x => x.Model == childModel));
          Point start = origin.Add(0.0, (double) num2 * 4.0);
          Point point1 = new Point(start.X + num1, start.Y);
          Point point2 = this.TerminusForChild(step2).Add(-10.0, 0.0);
          Point end = new Point(point2.X + 10.0, point2.Y);
          SolidColorBrush solidColorBrush = new SolidColorBrush(new Color()
          {
            A = (byte) 102,
            R = (byte) 0,
            G = (byte) 0,
            B = (byte) 0
          });
          canvas.DrawLine(start, point1, (Brush) solidColorBrush, 1.0);
          canvas.DrawLine(point1, point2, (Brush) solidColorBrush, 1.0);
          canvas.DrawLine(point2, end, (Brush) solidColorBrush, 1.0);
          ++num2;
        }
      }
    }

    private Point OriginForParent(QueryPlanStepView step)
    {
      RadioButton step1 = step.Step;
      Point point = step1.TransformToVisual((Visual) this.root).Transform(new Point(0.0, 0.0));
      return new Point(point.X + step1.ActualWidth, point.Y + step1.ActualHeight / 2.0 - QueryPlanView.OffsetToAllowForChildren(step));
    }

    private static double LengthForHorizontalSegment(QueryPlanStepView step)
    {
      RadioButton step1 = step.Step;
      return step.ActualWidth - step1.ActualWidth;
    }

    private static double OffsetToAllowForChildren(QueryPlanStepView step)
    {
      return (double) step.Model.Children.Count * 4.0 / 2.0;
    }

    private Point TerminusForChild(QueryPlanStepView step)
    {
      RadioButton step1 = step.Step;
      return step1.TransformToVisual((Visual) this.root).Transform(new Point(0.0, 0.0)).Add(0.0, step1.ActualHeight / 2.0);
    }

    private static IEnumerable<T> FindChildrenByType<T>(DependencyObject parent) where T : DependencyObject
    {
      for (int i = 0; i < VisualTreeHelper.GetChildrenCount(parent); ++i)
      {
        DependencyObject child = VisualTreeHelper.GetChild(parent, i);
        if (child is T)
        {
          yield return child as T;
        }
        else
        {
          foreach (T obj in QueryPlanView.FindChildrenByType<T>(child))
            yield return obj;
        }
      }
    }

    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //public void InitializeComponent()
    //{
    //  if (this._contentLoaded)
    //    return;
    //  this._contentLoaded = true;
    //  Application.LoadComponent((object) this, new Uri("/EFProf;component/queries/queryplanview.xaml", UriKind.Relative));
    //}

    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //internal Delegate _CreateDelegate(Type delegateType, string handler)
    //{
    //  return Delegate.CreateDelegate(delegateType, (object) this, handler);
    //}

    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[EditorBrowsable(EditorBrowsableState.Never)]
    //void IComponentConnector.Connect(int connectionId, object target)
    //{
    //  switch (connectionId)
    //  {
    //    case 1:
    //      this.rootContainer = (ScrollViewer) target;
    //      break;
    //    case 2:
    //      this.gridContainer = (Grid) target;
    //      this.gridContainer.MouseLeftButtonDown += new MouseButtonEventHandler(this.HandleMouseLeftButtonDown);
    //      this.gridContainer.MouseMove += new MouseEventHandler(this.HandleMouseMove);
    //      this.gridContainer.MouseLeftButtonUp += new MouseButtonEventHandler(this.HandleMouseLeftButtonUp);
    //      this.gridContainer.MouseWheel += new MouseWheelEventHandler(this.HandleMouseWheel);
    //      break;
    //    case 3:
    //      this.rootScale = (ScaleTransform) target;
    //      break;
    //    case 4:
    //      this.connectionCanvas = (Canvas) target;
    //      break;
    //    case 5:
    //      this.root = (ItemsControl) target;
    //      break;
    //    case 6:
    //      this.ZoomPanel = (Grid) target;
    //      break;
    //    case 7:
    //      this.button = (Button) target;
    //      break;
    //    case 8:
    //      this.MiniMapContainer = (Border) target;
    //      break;
    //    case 9:
    //      this.MiniMap = (Canvas) target;
    //      this.MiniMap.MouseLeftButtonDown += new MouseButtonEventHandler(this.HandleMiniMapClick);
    //      break;
    //    case 10:
    //      this.MiniMapBackground = (ImageBrush) target;
    //      break;
    //    case 11:
    //      this.VisibleArea = (Rectangle) target;
    //      this.VisibleArea.MouseLeftButtonDown += new MouseButtonEventHandler(this.VisibleAreaMouseLeftButtonDown);
    //      this.VisibleArea.MouseMove += new MouseEventHandler(this.VisibleAreaMouseMove);
    //      this.VisibleArea.MouseLeftButtonUp += new MouseButtonEventHandler(this.VisibleAreaMouseLeftButtonUp);
    //      break;
    //    case 12:
    //      ((ButtonBase) target).Click += new RoutedEventHandler(this.HandleResetClick);
    //      break;
    //    case 13:
    //      this.zoomSlider = (Slider) target;
    //      this.zoomSlider.ValueChanged += new RoutedPropertyChangedEventHandler<double>(this.SliderValueChanged);
    //      break;
    //    default:
    //      this._contentLoaded = true;
    //      break;
    //  }
    //}
  }
}
