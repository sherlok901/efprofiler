﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Queries.QueryPlan.QueryPlanStep
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Newtonsoft.Json;
using System.Collections.Generic;

namespace HibernatingRhinos.Profiler.Client.Queries.QueryPlan
{
  public class QueryPlanStep
  {
    private readonly HashSet<string> elementsToMerge = new HashSet<string>()
    {
      "Update"
    };
    private readonly IList<QueryPlanStep> children;
    public int Level;
    [JsonIgnore]
    public QueryPlanStep Parent;

    public QueryPlanStep()
    {
      this.VisibleProperties = (IDictionary<string, string>) new Dictionary<string, string>();
      this.AllProperties = (IDictionary<string, string>) new Dictionary<string, string>();
      this.children = (IList<QueryPlanStep>) new List<QueryPlanStep>();
    }

    public string Name { get; set; }

    public IEnumerable<QueryPlanStep> Children
    {
      get
      {
        return (IEnumerable<QueryPlanStep>) this.children;
      }
    }

    public IDictionary<string, string> VisibleProperties { get; set; }

    public IDictionary<string, string> AllProperties { get; set; }

    public override string ToString()
    {
      return this.Name;
    }

    public void Add(QueryPlanStep step)
    {
      if (step == null)
        return;
      if (this.elementsToMerge.Contains(step.Name))
      {
        foreach (QueryPlanStep child in step.Children)
        {
          this.children.Add(child);
          child.Parent = this;
        }
      }
      else
      {
        this.children.Add(step);
        step.Parent = this;
      }
    }

    public void MergeWithChild(int index)
    {
      QueryPlanStep child1 = this.children[index];
      this.children.RemoveAt(0);
      foreach (KeyValuePair<string, string> visibleProperty in (IEnumerable<KeyValuePair<string, string>>) child1.VisibleProperties)
        this.VisibleProperties[visibleProperty.Key] = visibleProperty.Value;
      foreach (KeyValuePair<string, string> allProperty in (IEnumerable<KeyValuePair<string, string>>) child1.AllProperties)
        this.AllProperties[allProperty.Key] = allProperty.Value;
      foreach (QueryPlanStep child2 in child1.Children)
        this.Add(child2);
      this.Name = child1.Name;
    }

    public QueryPlanStep FindMatchingLevel(int nestingLevel)
    {
      QueryPlanStep queryPlanStep = this;
      while (queryPlanStep.Level + 1 > nestingLevel)
        queryPlanStep = queryPlanStep.Parent;
      return queryPlanStep;
    }
  }
}
