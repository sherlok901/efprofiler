﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Queries.QueryPlan.SqlServer.ElementOperationService
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.Client.Queries.QueryPlan.SqlServer.Operators;
using System;
using System.Collections.Generic;
using System.Xml.Linq;

namespace HibernatingRhinos.Profiler.Client.Queries.QueryPlan.SqlServer
{
  public class ElementOperationService : AbstractElementOperation, IElementOperationService
  {
    private readonly HashSet<string> elementsToIgnore = new HashSet<string>()
    {
      "OutputList",
      "MemoryFractions",
      "RunTimeInformation",
      "RunTimePartitionSummary",
      "InternalInfo",
      "Warnings",
      "DefinedValues",
      "StmtUseDb"
    };

    public QueryPlanStep Generate(XElement element)
    {
      if (this.elementsToIgnore.Contains(element.Name.LocalName))
        return (QueryPlanStep) null;
      Type type = Type.GetType(this.GetType().Namespace + ".Operators." + element.Name.LocalName);
      if (type == (Type) null)
        throw new InvalidOperationException("No processor for " + element.Name.LocalName);
      IElementOperation instance = (IElementOperation) Activator.CreateInstance(type);
      QueryPlanStep queryPlanStep = new QueryPlanStep()
      {
        Name = AbstractElementOperation.PascalCaseToWords.Replace(element.Name.LocalName, "$1 ")
      };
      this.AddAttributes(element, queryPlanStep);
      instance.Generate(element, queryPlanStep, (IElementOperationService) this);
      return queryPlanStep;
    }

    public override void Generate(XElement element, QueryPlanStep step, IElementOperationService service)
    {
      throw new NotImplementedException();
    }
  }
}
