﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Queries.QueryPlan.SqlServer.Operators.Top
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System.Xml.Linq;

namespace HibernatingRhinos.Profiler.Client.Queries.QueryPlan.SqlServer.Operators
{
  public class Top : WithRelOp
  {
    public override void Generate(XElement element, QueryPlanStep step, IElementOperationService service)
    {
      base.Generate(element, step, service);
      string str;
      if (step.AllProperties.TryGetValue("Rows", out str))
        step.VisibleProperties["Rows"] = str;
      if (!step.AllProperties.TryGetValue("IsPercent", out str))
        return;
      step.VisibleProperties["IsPercent"] = str;
    }
  }
}
