﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Queries.QueryPlan.SqlServer.Operators.StmtSimple
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System.Xml.Linq;

namespace HibernatingRhinos.Profiler.Client.Queries.QueryPlan.SqlServer.Operators
{
  public class StmtSimple : AbstractElementOperation
  {
    public override void Generate(XElement element, QueryPlanStep step, IElementOperationService service)
    {
      step.Name = step.AllProperties["StatementType"];
      step.VisibleProperties["Statement"] = step.AllProperties["StatementText"];
      step.VisibleProperties["Cost"] = step.AllProperties["StatementSubTreeCost"];
      XElement element1 = element.Element(this.ns + "QueryPlan");
      this.AddAttributes(element1, step);
      XElement xelement = element1.Element(this.ns + "RelOp");
      this.AddAttributes(xelement, step);
      this.HandleWarning(step, xelement);
      this.HandleElements(step, xelement, service);
    }
  }
}
