﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Queries.QueryPlan.SqlServer.Operators.IndexScan
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace HibernatingRhinos.Profiler.Client.Queries.QueryPlan.SqlServer.Operators
{
  public class IndexScan : AbstractElementOperation
  {
    public override void Generate(XElement element, QueryPlanStep step, IElementOperationService service)
    {
      XElement objElement = element.Element(this.ns + "Object");
      XAttribute xattribute = objElement.Attribute((XName) "IndexKind");
      if (xattribute != null)
        step.Name = xattribute.Value + " " + step.Name;
      step.VisibleProperties["Index"] = string.Join(".", ((IEnumerable<string>) new string[4]
      {
        AbstractElementOperation.GetAttributeValue(objElement, "Database"),
        AbstractElementOperation.GetAttributeValue(objElement, "Schema"),
        AbstractElementOperation.GetAttributeValue(objElement, "Table"),
        AbstractElementOperation.GetAttributeValue(objElement, "Index")
      }).Where<string>((Func<string, bool>) (x => x != null)).ToArray<string>());
      XElement element1 = element.Element(this.ns + "Predicate");
      if (element1 == null)
        return;
      QueryPlanStep queryPlanStep = service.Generate(element1);
      step.VisibleProperties["Condition"] = queryPlanStep.Name;
    }
  }
}
