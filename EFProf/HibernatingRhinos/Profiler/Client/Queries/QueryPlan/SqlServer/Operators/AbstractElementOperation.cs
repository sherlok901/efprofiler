﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Queries.QueryPlan.SqlServer.Operators.AbstractElementOperation
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace HibernatingRhinos.Profiler.Client.Queries.QueryPlan.SqlServer.Operators
{
  public abstract class AbstractElementOperation : IElementOperation
  {
    protected static readonly Regex PascalCaseToWords = new Regex("([a-z](?=[A-Z])|[A-Z](?=[A-Z][a-z]))", RegexOptions.Compiled);
    protected readonly XNamespace ns = XNamespace.Get("http://schemas.microsoft.com/sqlserver/2004/07/showplan");

    protected void AddAttributes(XElement element, QueryPlanStep queryPlanStep)
    {
      foreach (XAttribute attribute in element.Attributes())
        queryPlanStep.AllProperties.Add(attribute.Name.LocalName, attribute.Value);
      string str;
      if (!queryPlanStep.AllProperties.TryGetValue("EstimatedTotalSubtreeCost", out str))
        return;
      queryPlanStep.VisibleProperties["Cost"] = str;
    }

    protected void HandleElements(QueryPlanStep step, XElement relOp, IElementOperationService service)
    {
      foreach (XElement element in relOp.Elements())
      {
        QueryPlanStep step1 = service.Generate(element);
        step.Add(step1);
      }
    }

    protected void HandleWarning(QueryPlanStep step, XElement relOp)
    {
      XElement xelement = relOp.Element(this.ns + "Warnings");
      if (xelement == null)
        return;
      XAttribute xattribute = xelement.Attribute(this.ns + "NoJoinPredicate");
      if (xattribute == null)
        return;
      step.VisibleProperties["No Join Predicate"] = xattribute.Value;
    }

    protected static string GetAttributeValue(XElement objElement, string name)
    {
      XAttribute xattribute = objElement.Attribute((XName) name);
      if (xattribute == null)
        return (string) null;
      return xattribute.Value;
    }

    public abstract void Generate(XElement element, QueryPlanStep step, IElementOperationService service);
  }
}
