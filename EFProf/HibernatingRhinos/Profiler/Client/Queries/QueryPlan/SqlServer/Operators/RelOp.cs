﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Queries.QueryPlan.SqlServer.Operators.RelOp
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System;
using System.Xml.Linq;

namespace HibernatingRhinos.Profiler.Client.Queries.QueryPlan.SqlServer.Operators
{
  public class RelOp : AbstractElementOperation
  {
    public override void Generate(XElement element, QueryPlanStep step, IElementOperationService service)
    {
      this.HandleWarning(step, element);
      this.HandleElements(step, element, service);
      step.MergeWithChild(0);
      if (step.AllProperties["PhysicalOp"] != step.AllProperties["LogicalOp"])
        step.Name = step.AllProperties["PhysicalOp"] + Environment.NewLine + "(" + step.AllProperties["LogicalOp"] + ")";
      else
        step.Name = step.AllProperties["PhysicalOp"];
    }
  }
}
