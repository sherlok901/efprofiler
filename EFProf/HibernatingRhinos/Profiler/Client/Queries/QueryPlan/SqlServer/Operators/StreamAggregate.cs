﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Queries.QueryPlan.SqlServer.Operators.StreamAggregate
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System.Xml.Linq;

namespace HibernatingRhinos.Profiler.Client.Queries.QueryPlan.SqlServer.Operators
{
  public class StreamAggregate : AbstractElementOperation
  {
    public override void Generate(XElement element, QueryPlanStep step, IElementOperationService service)
    {
      XElement element1 = element.Element(this.ns + "RelOp");
      step.Add(service.Generate(element1));
    }
  }
}
