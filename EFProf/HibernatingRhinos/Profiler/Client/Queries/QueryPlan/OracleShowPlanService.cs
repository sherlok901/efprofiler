﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Queries.QueryPlan.OracleShowPlanService
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System;
using System.Collections.Generic;
using System.Data;

namespace HibernatingRhinos.Profiler.Client.Queries.QueryPlan
{
  public class OracleShowPlanService : IShowPlanService
  {
    private readonly string statement;
    private readonly IDbConnection connection;

    public OracleShowPlanService(string statement, IDbConnection connection)
    {
      this.statement = statement;
      this.connection = connection;
    }

    public QueryPlanStep GetPlan()
    {
      QueryPlanStep queryPlanStep1 = new QueryPlanStep();
      using (IDbTransaction dbTransaction = this.connection.BeginTransaction())
      {
        using (IDbCommand command = this.connection.CreateCommand())
        {
          command.Transaction = dbTransaction;
          command.CommandText = "DELETE PLAN_TABLE";
          command.ExecuteNonQuery();
          command.CommandText = "EXPLAIN PLAN FOR " + this.statement;
          command.ExecuteNonQuery();
          Dictionary<int, QueryPlanStep> dictionary = new Dictionary<int, QueryPlanStep>();
          using (IDataReader executeReader = this.GetExecuteReader(command))
          {
            while (executeReader.Read())
            {
              QueryPlanStep step = new QueryPlanStep()
              {
                Name = string.Format("{0} {1} {2}.{3}", (object) this.TryGetValue(executeReader, "options"), (object) this.TryGetValue(executeReader, "operation"), (object) this.TryGetValue(executeReader, "object_owner"), (object) this.TryGetValue(executeReader, "object_name")),
                VisibleProperties = {
                  {
                    "Cost",
                    this.TryGetValue(executeReader, "cost")
                  }
                },
                AllProperties = {
                  {
                    "Time",
                    this.TryGetValue(executeReader, "time")
                  },
                  {
                    "IO Cost",
                    this.TryGetValue(executeReader, "io_cost")
                  },
                  {
                    "CPU Cost",
                    this.TryGetValue(executeReader, "cpu_cost")
                  }
                }
              };
              string str1 = this.TryGetValue(executeReader, "access_predicates");
              if (str1 != "")
                step.VisibleProperties["Access predicates"] = str1;
              string str2 = this.TryGetValue(executeReader, "filter_predicates");
              if (str2 != "")
                step.VisibleProperties["Filter predicates"] = str2;
              dictionary[(int) ((Decimal) executeReader["id"])] = step;
              if (executeReader["parent_id"] != DBNull.Value)
              {
                int key = (int) ((Decimal) executeReader["parent_id"]);
                QueryPlanStep queryPlanStep2;
                if (dictionary.TryGetValue(key, out queryPlanStep2))
                  queryPlanStep2.Add(step);
              }
              else
                queryPlanStep1.Add(step);
            }
          }
          dbTransaction.Rollback();
        }
      }
      return queryPlanStep1;
    }

    private string TryGetValue(IDataReader reader, string name)
    {
      try
      {
        return reader[name].ToString();
      }
      catch (IndexOutOfRangeException ex)
      {
        return "";
      }
    }

    private IDataReader GetExecuteReader(IDbCommand oracleCommand)
    {
      oracleCommand.CommandText = "SELECT  * FROM   plan_table ORDER BY id";
      return oracleCommand.ExecuteReader();
    }
  }
}
