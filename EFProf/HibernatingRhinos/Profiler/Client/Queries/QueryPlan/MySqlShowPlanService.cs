﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Queries.QueryPlan.MySqlShowPlanService
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System;
using System.Collections.Generic;
using System.Data;

namespace HibernatingRhinos.Profiler.Client.Queries.QueryPlan
{
  public class MySqlShowPlanService : IShowPlanService
  {
    private readonly string statement;
    private readonly IDbConnection connection;

    public MySqlShowPlanService(string statement, IDbConnection connection)
    {
      this.statement = statement;
      this.connection = connection;
    }

    public QueryPlanStep GetPlan()
    {
      QueryPlanStep queryPlanStep1 = new QueryPlanStep();
      Dictionary<string, QueryPlanStep> dictionary = new Dictionary<string, QueryPlanStep>();
      List<QueryPlanStep> queryPlanStepList = new List<QueryPlanStep>();
      using (IDbTransaction dbTransaction = this.connection.BeginTransaction())
      {
        using (IDbCommand command = this.connection.CreateCommand())
        {
          command.Transaction = dbTransaction;
          command.CommandText = "EXPLAIN " + this.statement;
          using (IDataReader dataReader = command.ExecuteReader())
          {
            while (dataReader.Read())
            {
              QueryPlanStep queryPlanStep2 = new QueryPlanStep()
              {
                AllProperties = {
                  {
                    "Id",
                    dataReader["id"].ToString()
                  },
                  {
                    "SelectType",
                    (string) dataReader["select_type"]
                  },
                  {
                    "Table",
                    (string) dataReader["table"]
                  },
                  {
                    "Type",
                    (string) dataReader["Type"]
                  },
                  {
                    "PossibleKeys",
                    dataReader["possible_keys"] == DBNull.Value ? (string) null : (string) dataReader["possible_keys"]
                  },
                  {
                    "Key",
                    dataReader["key"] == DBNull.Value ? (string) null : (string) dataReader["key"]
                  },
                  {
                    "Ref",
                    dataReader["ref"] == DBNull.Value ? (string) null : (string) dataReader["ref"]
                  },
                  {
                    "Extra",
                    (string) dataReader["extra"]
                  }
                }
              };
              queryPlanStep2.Name = queryPlanStep2.AllProperties["SelectType"] + " " + queryPlanStep2.AllProperties["Table"];
              queryPlanStep2.VisibleProperties["Key"] = queryPlanStep2.AllProperties["Table"] + "." + (queryPlanStep2.AllProperties["Key"] ?? queryPlanStep2.AllProperties["PossibleKeys"]);
              string allProperty = queryPlanStep2.AllProperties["Id"];
              QueryPlanStep queryPlanStep3;
              if (dictionary.TryGetValue(allProperty, out queryPlanStep3))
              {
                QueryPlanStep queryPlanStep4 = new QueryPlanStep()
                {
                  Name = "JOIN",
                  VisibleProperties = {
                    {
                      "Ref",
                      this.GetValue(queryPlanStep2, queryPlanStep3)
                    }
                  }
                };
                queryPlanStep4.Add(queryPlanStep3);
                queryPlanStep4.Add(queryPlanStep2);
                dictionary[allProperty] = queryPlanStep4;
                queryPlanStepList.Remove(queryPlanStep3);
                queryPlanStepList.Add(queryPlanStep4);
              }
              else
              {
                dictionary[allProperty] = queryPlanStep2;
                queryPlanStepList.Add(queryPlanStep2);
              }
            }
          }
        }
        dbTransaction.Rollback();
      }
      if (queryPlanStepList.Count == 1)
        queryPlanStep1.Add(queryPlanStepList[0]);
      else if (queryPlanStepList.Count == 2)
      {
        QueryPlanStep step = new QueryPlanStep()
        {
          Name = "ROOT JOIN"
        };
        step.Add(queryPlanStepList[0]);
        step.Add(queryPlanStepList[1]);
        queryPlanStep1.Add(step);
      }
      return queryPlanStep1;
    }

    private string GetValue(QueryPlanStep cur, QueryPlanStep prev)
    {
      string str;
      if (prev.AllProperties.TryGetValue("Ref", out str) || cur.AllProperties.TryGetValue("Ref", out str))
        return str;
      return (string) null;
    }
  }
}
