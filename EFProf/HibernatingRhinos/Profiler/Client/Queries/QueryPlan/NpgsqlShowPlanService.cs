﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Queries.QueryPlan.NpgsqlShowPlanService
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System;
using System.Data;
using System.IO;
using System.Text;

namespace HibernatingRhinos.Profiler.Client.Queries.QueryPlan
{
  public class NpgsqlShowPlanService : IShowPlanService
  {
    private readonly string statement;
    private readonly IDbConnection connection;

    public NpgsqlShowPlanService(string statement, IDbConnection connection)
    {
      this.statement = statement;
      this.connection = connection;
    }

    public QueryPlanStep GetPlan()
    {
      QueryPlanStep step1 = new QueryPlanStep();
      QueryPlanStep step2 = step1;
      StringReader stringReader = new StringReader(this.GetPlanInternal());
      string str1;
      while ((str1 = stringReader.ReadLine()) != null)
      {
        if (str1.Contains(": ("))
        {
          string[] strArray = str1.Trim().Split(new string[1]
          {
            ": ("
          }, 2, StringSplitOptions.RemoveEmptyEntries);
          string str2 = strArray[1].Trim();
          if (str2.EndsWith(")"))
            str2 = str2.Substring(0, str2.Length - 1);
          step2.VisibleProperties[strArray[0]] = str2;
        }
        else
        {
          if (str1.Contains("->"))
          {
            int num = str1.IndexOf("->");
            QueryPlanStep matchingLevel = step2.FindMatchingLevel(num);
            step2 = new QueryPlanStep() { Level = num };
            matchingLevel.Add(step2);
            str1 = str1.Substring(num);
          }
          string[] strArray1 = str1.Split(new char[1]{ '(' }, 2, StringSplitOptions.RemoveEmptyEntries);
          step2.Name = strArray1[0].Trim();
          if (strArray1.Length > 1)
          {
            string str2 = strArray1[1];
            char[] chArray1 = new char[1]{ ' ' };
            foreach (string str3 in str2.Split(chArray1))
            {
              char[] chArray2 = new char[1]{ '=' };
              string[] strArray2 = str3.Split(chArray2);
              string str4 = strArray2[1].Trim();
              if (str4.EndsWith(")"))
                str4 = str4.Substring(0, str4.Length - 1);
              step2.VisibleProperties[strArray2[0]] = str4;
            }
          }
        }
      }
      QueryPlanStep queryPlanStep = new QueryPlanStep();
      queryPlanStep.Add(step1);
      return queryPlanStep;
    }

    public string GetPlanInternal()
    {
      using (IDbCommand command = this.connection.CreateCommand())
      {
        StringBuilder stringBuilder = new StringBuilder();
        using (IDbTransaction dbTransaction = this.connection.BeginTransaction())
        {
          command.Transaction = dbTransaction;
          command.CommandText = "explain " + this.statement;
          using (IDataReader dataReader = command.ExecuteReader())
          {
            while (dataReader.Read())
            {
              string str = dataReader.GetString(0);
              stringBuilder.AppendLine(str);
            }
          }
          dbTransaction.Rollback();
          return stringBuilder.ToString();
        }
      }
    }
  }
}
