﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Queries.QueryPlan.SqlServerShowPlanService
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.Client.Queries.QueryPlan.SqlServer;
using System.Data;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace HibernatingRhinos.Profiler.Client.Queries.QueryPlan
{
  public class SqlServerShowPlanService : IShowPlanService
  {
    private readonly XNamespace ns = XNamespace.Get("http://schemas.microsoft.com/sqlserver/2004/07/showplan");
    private readonly string statement;
    private readonly IDbConnection connection;

    public SqlServerShowPlanService(string statement, IDbConnection connection)
    {
      this.statement = statement;
      this.connection = connection;
    }

    public QueryPlanStep GetPlan()
    {
      return new ElementOperationService().Generate(this.GetPlanAsXml().Descendants(this.ns + "Statements").First<XElement>());
    }

    private XDocument GetPlanAsXml()
    {
      using (IDbCommand command = this.connection.CreateCommand())
      {
        using (IDbTransaction dbTransaction = this.connection.BeginTransaction())
        {
          command.Transaction = dbTransaction;
          command.CommandText = "SET SHOWPLAN_XML ON";
          command.ExecuteNonQuery();
          command.CommandText = this.statement;
          StringBuilder stringBuilder = new StringBuilder();
          using (IDataReader dataReader = command.ExecuteReader())
          {
            while (dataReader.Read())
              stringBuilder.Append(dataReader.GetString(0));
          }
          dbTransaction.Rollback();
          return XDocument.Parse(stringBuilder.ToString());
        }
      }
    }

    public QueryPlanStep LoadPlanFrom(XDocument doc)
    {
      return new ElementOperationService().Generate(doc.Descendants(this.ns + "Statements").First<XElement>());
    }
  }
}
