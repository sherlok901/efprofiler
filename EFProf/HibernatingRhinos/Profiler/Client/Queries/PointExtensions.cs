﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Queries.PointExtensions
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System.Windows;

namespace HibernatingRhinos.Profiler.Client.Queries
{
  public static class PointExtensions
  {
    public static Point Add(this Point origin, double x, double y)
    {
      return new Point(origin.X + x, origin.Y + y);
    }

    public static Point Subtract(this Point origin, Point second)
    {
      return new Point(origin.X - second.X, origin.Y - second.Y);
    }
  }
}
