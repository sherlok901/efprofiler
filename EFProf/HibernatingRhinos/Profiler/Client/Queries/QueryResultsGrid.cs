﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Queries.QueryResultsGrid
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;

namespace HibernatingRhinos.Profiler.Client.Queries
{
  public partial class QueryResultsGrid : UserControl, IComponentConnector
  {
    //internal Grid LayoutRoot;
    //internal DataGrid ResultsGrid;
    //private bool _contentLoaded;

    public QueryResultsGrid()
    {
      this.InitializeComponent();
    }

    private void SelectAll(object sender, RoutedEventArgs e)
    {
      this.ResultsGrid.SelectAll();
    }

    private void Copy(object sender, RoutedEventArgs e)
    {
      this.Copy(DataGridClipboardCopyMode.ExcludeHeader);
    }

    private void CopyWithHeaders(object sender, RoutedEventArgs e)
    {
      this.Copy(DataGridClipboardCopyMode.IncludeHeader);
    }

    private void Copy(DataGridClipboardCopyMode headersMode)
    {
      DataGridClipboardCopyMode clipboardCopyMode = this.ResultsGrid.ClipboardCopyMode;
      this.ResultsGrid.ClipboardCopyMode = headersMode;
      ApplicationCommands.Copy.Execute((object) null, (IInputElement) this.ResultsGrid);
      this.ResultsGrid.ClipboardCopyMode = clipboardCopyMode;
    }

    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //public void InitializeComponent()
    //{
    //  if (this._contentLoaded)
    //    return;
    //  this._contentLoaded = true;
    //  Application.LoadComponent((object) this, new Uri("/EFProf;component/queries/queryresultsgrid.xaml", UriKind.Relative));
    //}

    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //internal Delegate _CreateDelegate(Type delegateType, string handler)
    //{
    //  return Delegate.CreateDelegate(delegateType, (object) this, handler);
    //}

    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[EditorBrowsable(EditorBrowsableState.Never)]
    //void IComponentConnector.Connect(int connectionId, object target)
    //{
    //  switch (connectionId)
    //  {
    //    case 1:
    //      this.LayoutRoot = (Grid) target;
    //      break;
    //    case 2:
    //      this.ResultsGrid = (DataGrid) target;
    //      break;
    //    case 3:
    //      ((MenuItem) target).Click += new RoutedEventHandler(this.SelectAll);
    //      break;
    //    case 4:
    //      ((MenuItem) target).Click += new RoutedEventHandler(this.Copy);
    //      break;
    //    case 5:
    //      ((MenuItem) target).Click += new RoutedEventHandler(this.CopyWithHeaders);
    //      break;
    //    default:
    //      this._contentLoaded = true;
    //      break;
    //  }
    //}
  }
}
