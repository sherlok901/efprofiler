﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Queries.DrawingExtensions
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace HibernatingRhinos.Profiler.Client.Queries
{
  public static class DrawingExtensions
  {
    public static Canvas DrawLine(this Canvas canvas, Point start, Point end, Brush stroke, double strokeThickness)
    {
      Line line1 = new Line();
      line1.X1 = start.X;
      line1.Y1 = start.Y;
      line1.X2 = end.X;
      line1.Y2 = end.Y;
      line1.Stroke = stroke;
      line1.StrokeThickness = strokeThickness;
      Line line2 = line1;
      canvas.Children.Add((UIElement) line2);
      return canvas;
    }
  }
}
