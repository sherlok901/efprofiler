﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.UserWindowSettings
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Newtonsoft.Json;
using System.Runtime.Serialization;
using System.Windows;

namespace HibernatingRhinos.Profiler.Client
{
  [DataContract]
  public class UserWindowSettings
  {
    private double? left;
    private WindowState? state;
    private double? top;

    [DataMember]
    public WindowState WindowState
    {
      get
      {
        return this.state ?? WindowState.Normal;
      }
      set
      {
        this.state = new WindowState?(value);
      }
    }

    [DataMember]
    public double Width { get; set; }

    [DataMember]
    public double Height { get; set; }

    [DataMember]
    public double? Top
    {
      get
      {
        return this.top;
      }
      set
      {
        this.top = value;
      }
    }

    [DataMember]
    public double? Left
    {
      get
      {
        return this.left;
      }
      set
      {
        this.left = value;
      }
    }

    public double MinWidth { get; set; }

    public double MinHeight { get; set; }

    public static UserWindowSettings Load(string serializedSettings, int defaultWidth, int defaultHeight, int minWidth, int minHeight)
    {
      UserWindowSettings userWindowSettings = string.IsNullOrEmpty(serializedSettings) ? new UserWindowSettings() : JsonConvert.DeserializeObject<UserWindowSettings>(serializedSettings);
      if (userWindowSettings.Width == 0.0)
        userWindowSettings.Width = (double) defaultWidth;
      if (userWindowSettings.Height == 0.0)
        userWindowSettings.Height = (double) defaultHeight;
      userWindowSettings.MinWidth = (double) minWidth;
      userWindowSettings.MinHeight = (double) minHeight;
      return userWindowSettings;
    }
  }
}
