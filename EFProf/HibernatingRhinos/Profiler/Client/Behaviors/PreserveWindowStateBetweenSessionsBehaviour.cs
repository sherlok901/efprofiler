﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Behaviors.PreserveWindowStateBetweenSessionsBehaviour
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.Client.Infrastructure.Monitors;
using HibernatingRhinos.Profiler.Client.Properties;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Interactivity;

namespace HibernatingRhinos.Profiler.Client.Behaviors
{
  public class PreserveWindowStateBetweenSessionsBehaviour : Behavior<Window>
  {
    public static readonly DependencyProperty WindowStyleProperty = DependencyProperty.Register(nameof (WindowStyle), typeof (string), typeof (PreserveWindowStateBetweenSessionsBehaviour), new PropertyMetadata((object) "", new PropertyChangedCallback(PreserveWindowStateBetweenSessionsBehaviour.HandleWindowStyleChanged)));

    public string WindowStyle
    {
      get
      {
        return (string) this.GetValue(PreserveWindowStateBetweenSessionsBehaviour.WindowStyleProperty);
      }
      set
      {
        this.SetValue(PreserveWindowStateBetweenSessionsBehaviour.WindowStyleProperty, (object) value);
      }
    }

    private static void HandleWindowStyleChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      (d as PreserveWindowStateBetweenSessionsBehaviour).HandleWindowStyleChanged((string) e.OldValue, (string) e.NewValue);
    }

    private void HandleWindowStyleChanged(string oldWindowStyle, string newWindowStyle)
    {
      this.SaveWindowSettings(oldWindowStyle);
      this.ApplyWindowSettings(newWindowStyle);
    }

    protected override void OnAttached()
    {
      base.OnAttached();
      if (this.AssociatedObject.Visibility == Visibility.Visible)
        this.ApplyWindowSettings(this.WindowStyle);
      else
        this.AssociatedObject.Loaded += (RoutedEventHandler) ((param0, param1) => this.ApplyWindowSettings(this.WindowStyle));
      this.AssociatedObject.Closing += new CancelEventHandler(this.HandleClosing);
    }

    private void HandleClosing(object sender, CancelEventArgs e)
    {
      this.SaveWindowSettings(this.WindowStyle);
    }

    private void SaveWindowSettings(string windowStyle)
    {
      if (windowStyle == "")
        return;
      //if (windowStyle == "Full")
      //  this.SaveWindowSettings((Action<string>) (s => Settings.Default.WindowSettings = s));
      //else
      //  this.SaveWindowSettings((Action<string>) (s => Settings.Default.CompactWindowSettings = s));
    }

    private void SaveWindowSettings(Action<string> saver)
    {
      UserWindowSettings userWindowSettings = new UserWindowSettings()
      {
        Top = new double?(this.AssociatedObject.Top),
        Left = new double?(this.AssociatedObject.Left),
        WindowState = this.AssociatedObject.WindowState,
        Height = this.AssociatedObject.Height,
        Width = this.AssociatedObject.Width
      };
      saver(JsonConvert.SerializeObject((object) userWindowSettings));
      //Settings.Default.Save();
    }

    private void ApplyWindowSettings(string windowStyle)
    {
      UserWindowSettings settings;
      //if (windowStyle == "Full" || string.IsNullOrEmpty(windowStyle))
      //{
      //  settings = UserWindowSettings.Load(Settings.Default.WindowSettings, 1024, 768, 730, 480);
      //}
      //else
      //{
      //  settings = UserWindowSettings.Load(Settings.Default.CompactWindowSettings, 640, 300, 500, 300);
      //  settings.WindowState = WindowState.Normal;
      //}
      //this.ApplyWindowSettings(settings);
    }

    private void ApplyWindowSettings(UserWindowSettings settings)
    {
      double? nullable1 = settings.Top;
      DisplayInfo displayInfo1 = DisplayManager.Displays.FirstOrDefault<DisplayInfo>((Func<DisplayInfo, bool>) (info => info.IsPrimary));
      if (displayInfo1 != null)
      {
        double? nullable2 = nullable1;
        double num = (double) (displayInfo1.Height / 2);
        if ((nullable2.GetValueOrDefault() <= num ? 0 : (nullable2.HasValue ? 1 : 0)) != 0)
          nullable1 = new double?((double) (displayInfo1.Height / 4));
      }
      this.AssociatedObject.Top = nullable1.HasValue ? nullable1.Value : this.AssociatedObject.Top;
      this.AssociatedObject.Left = settings.Left.HasValue ? settings.Left.Value : this.AssociatedObject.Left;
      this.AssociatedObject.MinHeight = settings.MinHeight;
      this.AssociatedObject.MinWidth = settings.MinWidth;
      this.AssociatedObject.WindowState = settings.WindowState;
      try
      {
        this.AssociatedObject.Height = settings.Height;
        this.AssociatedObject.Width = settings.Width;
      }
      catch
      {
        this.AssociatedObject.Height = 768.0;
        this.AssociatedObject.Width = 1024.0;
      }
      bool flag = false;
      Rect rect1 = new Rect(this.AssociatedObject.Left, this.AssociatedObject.Top, this.AssociatedObject.Width, this.AssociatedObject.Height);
      foreach (DisplayInfo displayInfo2 in (IEnumerable<DisplayInfo>) DisplayManager.Displays.OrderByDescending<DisplayInfo, bool>((Func<DisplayInfo, bool>) (info => info.IsPrimary)))
      {
        Rect rect2 = rect1;
        rect2.Intersect(new Rect((double) displayInfo2.WorkArea.Left, (double) displayInfo2.WorkArea.Top, (double) displayInfo2.Width, (double) displayInfo2.Height));
        if (rect2.Height > 0.0 && rect2.Width > 0.0)
        {
          flag = true;
          break;
        }
      }
      if (flag || displayInfo1 == null)
        return;
      this.AssociatedObject.Left = (double) displayInfo1.WorkArea.Left;
      this.AssociatedObject.Top = (double) displayInfo1.WorkArea.Top;
      this.AssociatedObject.WindowState = WindowState.Maximized;
    }
  }
}
