﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Behaviors.BindGridToDataTableColumnsBehaviour
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System.Data;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Interactivity;

namespace HibernatingRhinos.Profiler.Client.Behaviors
{
  public class BindGridToDataTableColumnsBehaviour : Behavior<DataGrid>
  {
    public static readonly DependencyProperty DataTableProperty = DependencyProperty.Register(nameof (DataTable), typeof (DataTable), typeof (BindGridToDataTableColumnsBehaviour), new PropertyMetadata((object) null, new PropertyChangedCallback(BindGridToDataTableColumnsBehaviour.HandlePropertyChanged)));

    public DataTable DataTable
    {
      get
      {
        return (DataTable) this.GetValue(BindGridToDataTableColumnsBehaviour.DataTableProperty);
      }
      set
      {
        this.SetValue(BindGridToDataTableColumnsBehaviour.DataTableProperty, (object) value);
      }
    }

    private static void HandlePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      ((BindGridToDataTableColumnsBehaviour) d).UpdateColumns();
    }

    private void UpdateColumns()
    {
      if (this.AssociatedObject == null || this.DataTable == null)
        return;
      this.AssociatedObject.Columns.Clear();
      for (int index = 0; index < this.DataTable.Columns.Count; ++index)
      {
        DataGridTextColumn dataGridTextColumn = new DataGridTextColumn();
        dataGridTextColumn.Header = (object) this.DataTable.Columns[index].ColumnName;
        dataGridTextColumn.Binding = (BindingBase) new Binding(string.Format("ItemArray[{0}]", (object) index));
        this.AssociatedObject.Columns.Add((DataGridColumn) dataGridTextColumn);
      }
    }

    protected override void OnAttached()
    {
      this.UpdateColumns();
    }
  }
}
