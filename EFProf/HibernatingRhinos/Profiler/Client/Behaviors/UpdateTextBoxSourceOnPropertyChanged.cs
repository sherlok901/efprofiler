﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Behaviors.UpdateTextBoxSourceOnPropertyChanged
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Interactivity;

namespace HibernatingRhinos.Profiler.Client.Behaviors
{
  public class UpdateTextBoxSourceOnPropertyChanged : Behavior<TextBox>
  {
    protected override void OnAttached()
    {
      base.OnAttached();
      this.AssociatedObject.TextChanged += new TextChangedEventHandler(this.TextBoxTextChanged);
    }

    protected override void OnDetaching()
    {
      base.OnDetaching();
      this.AssociatedObject.TextChanged -= new TextChangedEventHandler(this.TextBoxTextChanged);
    }

    private void TextBoxTextChanged(object sender, TextChangedEventArgs e)
    {
      BindingExpression bindingExpression = this.AssociatedObject.GetBindingExpression(TextBox.TextProperty);
      if (bindingExpression == null)
        return;
      bindingExpression.UpdateSource();
    }
  }
}
