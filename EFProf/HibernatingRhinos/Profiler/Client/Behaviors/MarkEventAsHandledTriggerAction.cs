﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Behaviors.MarkEventAsHandledTriggerAction
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System.Windows;

namespace HibernatingRhinos.Profiler.Client.Behaviors
{
  internal class MarkEventAsHandledTriggerAction : System.Windows.Interactivity.TriggerAction<FrameworkElement>
  {
    protected override void Invoke(object parameter)
    {
      RoutedEventArgs routedEventArgs = parameter as RoutedEventArgs;
      if (routedEventArgs == null)
        return;
      routedEventArgs.Handled = true;
    }
  }
}
