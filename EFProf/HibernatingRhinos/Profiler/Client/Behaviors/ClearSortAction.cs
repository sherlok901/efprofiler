﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Behaviors.ClearSortAction
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.Client.Infrastructure;
using System.ComponentModel;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Interactivity;

namespace HibernatingRhinos.Profiler.Client.Behaviors
{
  public class ClearSortAction : TargetedTriggerAction<DataGridColumnHeader>
  {
    protected override void Invoke(object parameter)
    {
      this.ClearSortDescriptions();
      this.Target.Column.SortDirection = new ListSortDirection?();
    }

    private void ClearSortDescriptions()
    {
      DataGrid ancestor = this.Target.GetAncestor<DataGrid>();
      if (ancestor == null)
        return;
      ICollectionView collectionView = ancestor.ItemsSource is ICollectionView ? ancestor.ItemsSource as ICollectionView : CollectionViewSource.GetDefaultView((object) ancestor.ItemsSource);
      if (collectionView == null)
        return;
      collectionView.SortDescriptions.Clear();
    }
  }
}
