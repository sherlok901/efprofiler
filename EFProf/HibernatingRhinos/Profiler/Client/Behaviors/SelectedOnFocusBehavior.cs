﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Behaviors.SelectedOnFocusBehavior
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.Client.Sessions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;

namespace HibernatingRhinos.Profiler.Client.Behaviors
{
  public class SelectedOnFocusBehavior : Behavior<DataGrid>
  {
    public static readonly DependencyProperty ValueProperty = DependencyProperty.Register(nameof (Value), typeof (bool), typeof (SelectedOnFocusBehavior), new PropertyMetadata((PropertyChangedCallback) null));

    public bool Value
    {
      get
      {
        return (bool) this.GetValue(SelectedOnFocusBehavior.ValueProperty);
      }
      set
      {
        this.SetValue(SelectedOnFocusBehavior.ValueProperty, (object) value);
      }
    }

    protected override void OnAttached()
    {
      base.OnAttached();
      this.AssociatedObject.GotFocus += new RoutedEventHandler(this.AssociatedObjectOnGotFocus);
    }

    private void AssociatedObjectOnGotFocus(object sender, RoutedEventArgs routedEventArgs)
    {
      ((SessionDiffModel) this.AssociatedObject.DataContext).SelectedSessionIsRight = this.Value;
    }

    protected override void OnDetaching()
    {
      base.OnDetaching();
      this.AssociatedObject.GotFocus -= new RoutedEventHandler(this.AssociatedObjectOnGotFocus);
    }
  }
}
