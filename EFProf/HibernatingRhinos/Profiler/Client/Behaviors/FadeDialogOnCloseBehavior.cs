﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Behaviors.FadeDialogOnCloseBehavior
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Interactivity;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace HibernatingRhinos.Profiler.Client.Behaviors
{
  public class FadeDialogOnCloseBehavior : Behavior<FrameworkElement>
  {
    public static readonly DependencyProperty WindowProperty = DependencyProperty.Register(nameof (Window), typeof (Window), typeof (FadeDialogOnCloseBehavior), new PropertyMetadata(new PropertyChangedCallback(FadeDialogOnCloseBehavior.WindowChanged)));
    private bool finalClose;
    private bool? dialogResult;

    public Window Window
    {
      get
      {
        return (Window) this.GetValue(FadeDialogOnCloseBehavior.WindowProperty);
      }
      set
      {
        this.SetValue(FadeDialogOnCloseBehavior.WindowProperty, (object) value);
      }
    }

    private static void WindowChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      FadeDialogOnCloseBehavior dialogOnCloseBehavior = (FadeDialogOnCloseBehavior) d;
      dialogOnCloseBehavior.DettachFromWindow((Window) e.OldValue);
      dialogOnCloseBehavior.AttachToWindow((Window) e.NewValue);
    }

    private void AttachToWindow(Window window)
    {
      if (window == null)
        return;
      window.Closing += new CancelEventHandler(this.HandleClosing);
    }

    private void DettachFromWindow(Window window)
    {
      if (window == null)
        return;
      window.Closing -= new CancelEventHandler(this.HandleClosing);
    }

    private void HandleClosing(object sender, CancelEventArgs e)
    {
      if (e.Cancel || this.finalClose)
        return;
      this.dialogResult = this.Window.DialogResult;
      this.FadeOut();
      e.Cancel = true;
    }

    private void FadeOut()
    {
      DoubleAnimation doubleAnimation1 = new DoubleAnimation();
      doubleAnimation1.To = new double?(0.0);
      doubleAnimation1.Duration = new Duration(TimeSpan.FromMilliseconds(200.0));
      DoubleAnimation doubleAnimation2 = doubleAnimation1;
      Storyboard.SetTargetProperty((DependencyObject) doubleAnimation2, new PropertyPath((object) UIElement.OpacityProperty));
      Storyboard.SetTarget((DependencyObject) doubleAnimation2, (DependencyObject) this.Window);
      Storyboard storyboard1 = new Storyboard();
      storyboard1.Children.Add((Timeline) doubleAnimation2);
      Storyboard storyboard2 = storyboard1;
      storyboard2.Completed += (EventHandler) ((param0, param1) =>
      {
        this.finalClose = true;
        if (this.dialogResult.HasValue && PresentationSource.FromVisual((Visual) this.Window) != null)
          this.Window.DialogResult = this.dialogResult;
        else
          this.Window.Close();
      });
      storyboard2.Begin();
    }
  }
}
