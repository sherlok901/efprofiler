﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Behaviors.UnselectableDataGridBehavior
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Interactivity;

namespace HibernatingRhinos.Profiler.Client.Behaviors
{
  public class UnselectableDataGridBehavior : Behavior<DataGrid>
  {
    protected override void OnAttached()
    {
      base.OnAttached();
      this.AssociatedObject.SelectionChanged += new SelectionChangedEventHandler(UnselectableDataGridBehavior.IgnoreSelectionChanged);
    }

    protected override void OnDetaching()
    {
      base.OnDetaching();
      this.AssociatedObject.SelectionChanged -= new SelectionChangedEventHandler(UnselectableDataGridBehavior.IgnoreSelectionChanged);
    }

    private static void IgnoreSelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (!(sender is DataGrid))
        return;
      ((Selector) sender).SelectedItem = (object) null;
    }
  }
}
