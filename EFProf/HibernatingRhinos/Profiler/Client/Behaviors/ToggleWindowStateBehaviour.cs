﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Behaviors.ToggleWindowStateBehaviour
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.Client.Controls;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Interactivity;

namespace HibernatingRhinos.Profiler.Client.Behaviors
{
  public class ToggleWindowStateBehaviour : Behavior<Button>
  {
    public static readonly DependencyProperty WindowProperty = DependencyProperty.Register(nameof (Window), typeof (ChromelessWindow), typeof (ToggleWindowStateBehaviour), new PropertyMetadata((PropertyChangedCallback) null));

    public ChromelessWindow Window
    {
      get
      {
        return (ChromelessWindow) this.GetValue(ToggleWindowStateBehaviour.WindowProperty);
      }
      set
      {
        this.SetValue(ToggleWindowStateBehaviour.WindowProperty, (object) value);
      }
    }

    protected override void OnAttached()
    {
      base.OnAttached();
      this.AssociatedObject.SetBinding(UIElement.VisibilityProperty, (BindingBase) new Binding("Window.CanMaximise")
      {
        Source = (object) this,
        Converter = (IValueConverter) new BooleanToVisibilityConverter()
      });
      this.AssociatedObject.Click += new RoutedEventHandler(this.HandleClick);
    }

    protected override void OnDetaching()
    {
      base.OnDetaching();
      BindingOperations.ClearBinding((DependencyObject) this.AssociatedObject, UIElement.VisibilityProperty);
      this.AssociatedObject.Click -= new RoutedEventHandler(this.HandleClick);
    }

    private void HandleClick(object sender, RoutedEventArgs e)
    {
      if (this.Window == null)
        return;
      this.Window.ToggleWindowState();
    }
  }
}
