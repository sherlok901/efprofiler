﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Behaviors.FadeTrimming
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;

namespace HibernatingRhinos.Profiler.Client.Behaviors
{
  public static class FadeTrimming
  {
    public static readonly DependencyProperty IsEnabledProperty = DependencyProperty.RegisterAttached("IsEnabled", typeof (bool), typeof (FadeTrimming), new PropertyMetadata((object) false, new PropertyChangedCallback(FadeTrimming.HandleIsEnabledChanged)));
    public static readonly DependencyProperty ForegroundColorProperty = DependencyProperty.RegisterAttached("ForegroundColor", typeof (Color), typeof (FadeTrimming), new PropertyMetadata((object) Colors.Transparent));
    public static readonly DependencyProperty ShowTextInToolTipWhenTrimmedProperty = DependencyProperty.RegisterAttached("ShowTextInToolTipWhenTrimmed", typeof (bool), typeof (FadeTrimming), new PropertyMetadata((object) false));
    private static readonly DependencyProperty FaderProperty = DependencyProperty.RegisterAttached("Fader", typeof (FadeTrimming.Fader), typeof (FadeTrimming), new PropertyMetadata((PropertyChangedCallback) null));
    public static readonly DependencyProperty ToolTipStyleProperty = DependencyProperty.RegisterAttached("ToolTipStyle", typeof (Style), typeof (FadeTrimming), new PropertyMetadata((PropertyChangedCallback) null));
    private const double Epsilon = 1E-05;
    private const double FadeWidth = 10.0;
    private const double FadeHeight = 20.0;

    public static Style GetToolTipStyle(DependencyObject obj)
    {
      return (Style) obj.GetValue(FadeTrimming.ToolTipStyleProperty);
    }

    public static void SetToolTipStyle(DependencyObject obj, Style value)
    {
      obj.SetValue(FadeTrimming.ToolTipStyleProperty, (object) value);
    }

    public static bool GetIsEnabled(DependencyObject obj)
    {
      return (bool) obj.GetValue(FadeTrimming.IsEnabledProperty);
    }

    public static void SetIsEnabled(DependencyObject obj, bool value)
    {
      obj.SetValue(FadeTrimming.IsEnabledProperty, (object) value);
    }

    public static bool GetShowTextInToolTipWhenTrimmed(DependencyObject obj)
    {
      return (bool) obj.GetValue(FadeTrimming.ShowTextInToolTipWhenTrimmedProperty);
    }

    public static void SetShowTextInToolTipWhenTrimmed(DependencyObject obj, bool value)
    {
      obj.SetValue(FadeTrimming.ShowTextInToolTipWhenTrimmedProperty, (object) value);
    }

    public static Color GetForegroundColor(DependencyObject obj)
    {
      return (Color) obj.GetValue(FadeTrimming.ForegroundColorProperty);
    }

    public static void SetForegroundColor(DependencyObject obj, Color value)
    {
      obj.SetValue(FadeTrimming.ForegroundColorProperty, (object) value);
    }

    private static FadeTrimming.Fader GetFader(DependencyObject obj)
    {
      return (FadeTrimming.Fader) obj.GetValue(FadeTrimming.FaderProperty);
    }

    private static void SetFader(DependencyObject obj, FadeTrimming.Fader value)
    {
      obj.SetValue(FadeTrimming.FaderProperty, (object) value);
    }

    private static void HandleIsEnabledChanged(DependencyObject source, DependencyPropertyChangedEventArgs e)
    {
      TextBlock textBlock = source as TextBlock;
      if (textBlock == null)
        return;
      if ((bool) e.OldValue)
      {
        FadeTrimming.Fader fader = FadeTrimming.GetFader((DependencyObject) textBlock);
        if (fader != null)
        {
          fader.Detach();
          FadeTrimming.SetFader((DependencyObject) textBlock, (FadeTrimming.Fader) null);
        }
        textBlock.Loaded -= new RoutedEventHandler(FadeTrimming.HandleTextBlockLoaded);
        textBlock.Unloaded -= new RoutedEventHandler(FadeTrimming.HandleTextBlockUnloaded);
      }
      if (!(bool) e.NewValue)
        return;
      textBlock.Loaded += new RoutedEventHandler(FadeTrimming.HandleTextBlockLoaded);
      textBlock.Unloaded += new RoutedEventHandler(FadeTrimming.HandleTextBlockUnloaded);
      FadeTrimming.Fader fader1 = new FadeTrimming.Fader(textBlock);
      FadeTrimming.SetFader((DependencyObject) textBlock, fader1);
      fader1.Attach();
    }

    private static void HandleTextBlockUnloaded(object sender, RoutedEventArgs e)
    {
      FadeTrimming.GetFader(sender as DependencyObject).Detach();
    }

    private static void HandleTextBlockLoaded(object sender, RoutedEventArgs e)
    {
      FadeTrimming.GetFader(sender as DependencyObject).Attach();
    }

    private static bool HorizontalBrushNeedsUpdating(LinearGradientBrush brush, double visibleWidth)
    {
      if (brush.EndPoint.X >= visibleWidth - 1E-05)
        return brush.EndPoint.X > visibleWidth + 1E-05;
      return true;
    }

    private static bool VerticalBrushNeedsUpdating(LinearGradientBrush brush, double visibleHeight)
    {
      if (brush.EndPoint.Y >= visibleHeight - 1E-05)
        return brush.EndPoint.Y > visibleHeight + 1E-05;
      return true;
    }

    private class Fader
    {
      private readonly TextBlock _textBlock;
      private bool _isAttached;
      private LinearGradientBrush _brush;
      private Color _foregroundColor;
      private bool _isClipped;

      public Fader(TextBlock textBlock)
      {
        this._textBlock = textBlock;
      }

      public void Attach()
      {
        FrameworkElement parent = VisualTreeHelper.GetParent((DependencyObject) this._textBlock) as FrameworkElement;
        if (parent == null || this._isAttached)
          return;
        parent.SizeChanged += new SizeChangedEventHandler(this.UpdateForegroundBrush);
        this._textBlock.SizeChanged += new SizeChangedEventHandler(this.UpdateForegroundBrush);
        this._foregroundColor = this.DetermineForegroundColor(this._textBlock);
        this.UpdateForegroundBrush((object) this._textBlock, EventArgs.Empty);
        this._textBlock.TextTrimming = TextTrimming.None;
        this._isAttached = true;
      }

      public void Detach()
      {
        this._textBlock.SizeChanged -= new SizeChangedEventHandler(this.UpdateForegroundBrush);
        FrameworkElement parent = VisualTreeHelper.GetParent((DependencyObject) this._textBlock) as FrameworkElement;
        if (parent != null)
          parent.SizeChanged -= new SizeChangedEventHandler(this.UpdateForegroundBrush);
        this._textBlock.ClearValue(TextBlock.ForegroundProperty);
        this._isAttached = false;
      }

      private Color DetermineForegroundColor(TextBlock textBlock)
      {
        if (FadeTrimming.GetForegroundColor((DependencyObject) textBlock) != Colors.Transparent)
          return FadeTrimming.GetForegroundColor((DependencyObject) textBlock);
        if (textBlock.Foreground is SolidColorBrush)
          return (textBlock.Foreground as SolidColorBrush).Color;
        return Colors.Black;
      }

      private void UpdateForegroundBrush(object sender, EventArgs e)
      {
        Geometry layoutClip = LayoutInformation.GetLayoutClip((FrameworkElement) this._textBlock);
        bool flag1 = layoutClip != null && (this._textBlock.TextWrapping == TextWrapping.NoWrap && layoutClip.Bounds.Width > 0.0 && layoutClip.Bounds.Width < this._textBlock.ActualWidth || this._textBlock.TextWrapping == TextWrapping.Wrap && layoutClip.Bounds.Height > 0.0 && layoutClip.Bounds.Height < this._textBlock.ActualHeight);
        if (this._isClipped && !flag1)
        {
          if (FadeTrimming.GetShowTextInToolTipWhenTrimmed((DependencyObject) this._textBlock))
            this._textBlock.ClearValue(ToolTipService.ToolTipProperty);
          this._textBlock.Foreground = (Brush) new SolidColorBrush()
          {
            Color = this._foregroundColor
          };
          this._brush = (LinearGradientBrush) null;
          this._isClipped = false;
        }
        if (flag1 && FadeTrimming.GetShowTextInToolTipWhenTrimmed((DependencyObject) this._textBlock))
        {
          ToolTip toolTip = ToolTipService.GetToolTip((DependencyObject) this._textBlock) as ToolTip;
          if (toolTip == null)
          {
            toolTip = new ToolTip();
            ToolTipService.SetToolTip((DependencyObject) this._textBlock, (object) toolTip);
            toolTip.Style = this._textBlock.GetValue(FadeTrimming.ToolTipStyleProperty) as Style;
          }
          toolTip.Content = (object) this._textBlock.Text;
        }
        if (!flag1)
          return;
        double width = layoutClip.Bounds.Width;
        double height = layoutClip.Bounds.Height;
        bool flag2 = this._textBlock.TextWrapping == TextWrapping.Wrap;
        if (this._brush == null)
        {
          this._brush = flag2 ? this.GetVerticalClipBrush(height) : this.GetHorizontalClipBrush(width);
          this._textBlock.Foreground = (Brush) this._brush;
        }
        else if (flag2 && FadeTrimming.VerticalBrushNeedsUpdating(this._brush, height))
        {
          this._brush.EndPoint = new Point(0.0, height);
          this._brush.GradientStops[1].Offset = (height - 20.0) / height;
        }
        else if (!flag2 && FadeTrimming.HorizontalBrushNeedsUpdating(this._brush, width))
        {
          this._brush.EndPoint = new Point(width, 0.0);
          this._brush.GradientStops[1].Offset = (width - 10.0) / width;
        }
        this._isClipped = true;
      }

      private LinearGradientBrush GetHorizontalClipBrush(double visibleWidth)
      {
        LinearGradientBrush linearGradientBrush = new LinearGradientBrush();
        linearGradientBrush.MappingMode = BrushMappingMode.Absolute;
        linearGradientBrush.StartPoint = new Point(0.0, 0.0);
        linearGradientBrush.EndPoint = new Point(visibleWidth, 0.0);
        linearGradientBrush.GradientStops.Add(new GradientStop()
        {
          Color = this._foregroundColor,
          Offset = 0.0
        });
        linearGradientBrush.GradientStops.Add(new GradientStop()
        {
          Color = this._foregroundColor,
          Offset = (visibleWidth - 10.0) / visibleWidth
        });
        linearGradientBrush.GradientStops.Add(new GradientStop()
        {
          Color = Color.FromArgb((byte) 0, this._foregroundColor.R, this._foregroundColor.G, this._foregroundColor.B),
          Offset = 1.0
        });
        return linearGradientBrush;
      }

      private LinearGradientBrush GetVerticalClipBrush(double visibleHeight)
      {
        LinearGradientBrush linearGradientBrush = new LinearGradientBrush();
        linearGradientBrush.MappingMode = BrushMappingMode.Absolute;
        linearGradientBrush.StartPoint = new Point(0.0, 0.0);
        linearGradientBrush.EndPoint = new Point(0.0, visibleHeight);
        linearGradientBrush.GradientStops.Add(new GradientStop()
        {
          Color = this._foregroundColor,
          Offset = 0.0
        });
        linearGradientBrush.GradientStops.Add(new GradientStop()
        {
          Color = this._foregroundColor,
          Offset = (visibleHeight - 20.0) / visibleHeight
        });
        linearGradientBrush.GradientStops.Add(new GradientStop()
        {
          Color = Color.FromArgb((byte) 0, this._foregroundColor.R, this._foregroundColor.G, this._foregroundColor.B),
          Offset = 1.0
        });
        return linearGradientBrush;
      }
    }
  }
}
