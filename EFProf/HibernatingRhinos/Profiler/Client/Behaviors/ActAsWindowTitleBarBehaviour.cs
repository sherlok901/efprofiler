﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Behaviors.ActAsWindowTitleBarBehaviour
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.Client.Controls;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interactivity;

namespace HibernatingRhinos.Profiler.Client.Behaviors
{
  public class ActAsWindowTitleBarBehaviour : Behavior<FrameworkElement>
  {
    public static readonly DependencyProperty WindowProperty = DependencyProperty.Register(nameof (Window), typeof (ChromelessWindow), typeof (ActAsWindowTitleBarBehaviour), new PropertyMetadata((PropertyChangedCallback) null));
    public static readonly DependencyProperty EnableMaximiseProperty = DependencyProperty.Register(nameof (EnableMaximise), typeof (bool), typeof (ActAsWindowTitleBarBehaviour), new PropertyMetadata((object) false));

    public ChromelessWindow Window
    {
      get
      {
        return (ChromelessWindow) this.GetValue(ActAsWindowTitleBarBehaviour.WindowProperty);
      }
      set
      {
        this.SetValue(ActAsWindowTitleBarBehaviour.WindowProperty, (object) value);
      }
    }

    public bool EnableMaximise
    {
      get
      {
        return (bool) this.GetValue(ActAsWindowTitleBarBehaviour.EnableMaximiseProperty);
      }
      set
      {
        this.SetValue(ActAsWindowTitleBarBehaviour.EnableMaximiseProperty, (object) value);
      }
    }

    protected override void OnAttached()
    {
      base.OnAttached();
      this.AssociatedObject.MouseLeftButtonDown += new MouseButtonEventHandler(this.HandleLeftButtonDown);
    }

    protected override void OnDetaching()
    {
      this.AssociatedObject.MouseLeftButtonDown -= new MouseButtonEventHandler(this.HandleLeftButtonDown);
    }

    private void HandleLeftButtonDown(object sender, MouseButtonEventArgs e)
    {
      if (this.Window == null)
        return;
      this.Window.DragMove();
      if (e.ClickCount <= 1 || !this.EnableMaximise)
        return;
      this.Window.ToggleWindowState();
    }
  }
}
