﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.ProfilerLogger
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System;

namespace HibernatingRhinos.Profiler.Client
{
  public class ProfilerLogger : Caliburn.Micro.ILog
  {
    private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(typeof (ProfilerLogger));
    private readonly Type type;

    public ProfilerLogger(Type type)
    {
      this.type = type;
    }

    public void Info(string format, params object[] args)
    {
    }

    public void Warn(string format, params object[] args)
    {
      string str = this.type.ToString() + ": " + string.Format(format, args);
      ProfilerLogger.Logger.Warn((object) str);
    }

    public void Error(Exception exception)
    {
      string str = this.type.ToString() + ": Error: " + (object) exception;
      ProfilerLogger.Logger.Error((object) str, exception);
    }
  }
}
