﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Startup.DefaultArgumentAttribute
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System;

namespace HibernatingRhinos.Profiler.Client.Startup
{
  [AttributeUsage(AttributeTargets.Field)]
  public class DefaultArgumentAttribute : ArgumentAttribute
  {
    public DefaultArgumentAttribute(ArgumentType type)
      : base(type)
    {
    }
  }
}
