﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Startup.IoC2
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using Castle.Facilities.TypedFactory;
using System;

namespace HibernatingRhinos.Profiler.Client.Startup
{
  public class IoC2 : IDisposable
  {
    private readonly ApplicationContainer container;

    public IoC2()
    {
      this.container = new ApplicationContainer();
      this.container.AddFacility<TypedFactoryFacility>();
      ViewLocator.LocateForModelType = ProfilerBootstrapper.MyLocateForModelType;
      Caliburn.Micro.Parser.CreateTrigger = ProfilerBootstrapper.CreateTrigger;
      IoC.GetInstance = new Func<Type, string, object>(this.GetInstance);
    }

    private object GetInstance(Type type, string arg2)
    {
      return this.container.Resolve(type);
    }

    public T Get<T>()
    {
      return (T) this.container.Resolve(typeof (T));
    }

    public void Dispose()
    {
      if (this.container == null)
        return;
      this.container.Dispose();
    }
  }
}
