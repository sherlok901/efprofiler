﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Startup.CommandLineCommand
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.Client.Services;
using HibernatingRhinos.Profiler.Client.Settings;
using HibernatingRhinos.Profiler.Client.Shell;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace HibernatingRhinos.Profiler.Client.Startup
{
  public class CommandLineCommand : IStartupCommand
  {
    private readonly int port;
    private readonly string file;
    private readonly string inputFile;
    private readonly string licensePath;
    private readonly IEnumerable<ReportFormat> formats;
    private BackendBridge bridge;
    private ManualResetEvent waitUntilToldToShutDown;
    private Guid stopListeningId;

    public CommandLineCommand(int port, string file, string inputFile, string licensePath, IEnumerable<ReportFormat> formats)
    {
      if (file == null)
        throw new ArgumentNullException(nameof (file));
      this.port = port;
      this.file = file;
      this.inputFile = inputFile;
      this.licensePath = licensePath;
      this.formats = formats;
    }

    public int Execute()
    {
      using (new IoC2())
      {
        this.bridge = IoC.Get<BackendBridge>((string) null);
        this.bridge.Configuration = UserPreferencesHolder.UserSettings.Configuration;
        using (this.bridge)
        {
          this.bridge.Initialize();
          ApplicationModel applicationModel = IoC.Get<ApplicationModel>((string) null);
          applicationModel.Start();
          ExportModel exportModel = new ExportModel(applicationModel, applicationModel.TrackingService);
          if (!string.IsNullOrWhiteSpace(this.licensePath))
            LicensingDescription.CustomLicensePath = this.licensePath;
          //if (!applicationModel.LicenseInfo.ConfirmLicense(false))
          //{
          //  StartupParser.WriteLineToConsole("Invalid license, cannot start profiling");
          //  StartupParser.FreeConsoleIfNeeded();
          //  return -5;
          //}
          this.waitUntilToldToShutDown = new ManualResetEvent(false);
          if (!string.IsNullOrEmpty(this.inputFile))
          {
            StartupParser.WriteLineToConsole("Started to read: " + this.inputFile);
            Guid loadFileId = this.bridge.StartLoadFromFile(this.inputFile);
            LoadFileStatus loadFileStatus;
            while (true)
            {
              loadFileStatus = this.bridge.GetLoadFileStatus(loadFileId);
              if (!loadFileStatus.Complete)
              {
                StartupParser.WriteToConsole(string.Format("\rProgress: {0}", (object) loadFileStatus.NumberOfLoadedMessages));
                Thread.Sleep(150);
              }
              else
                break;
            }
            if (loadFileStatus.ExceptionMessage != null)
              StartupParser.WriteLineToConsole(loadFileStatus.ExceptionMessage);
          }
          else
          {
            this.stopListeningId = this.bridge.Start(this.port, StartupParser.CurrentArguments.ContextFile);
            this.bridge.Notifications.ShutdownAndOutputReport += new System.Action(this.HandleShutdownAndOutputReport);
            StartupParser.WriteLineToConsole("Started to listen to profiling events");
            StartupParser.FreeConsoleIfNeeded();
            this.waitUntilToldToShutDown.WaitOne();
          }
          this.bridge.EventsObserver.WaitForAllMessages((HibernatingRhinos.Profiler.BackEnd.Bridge.CancellationToken) null);
          string str = this.file + ".~tmp";
          foreach (ReportFormat format in this.formats)
          {
            switch (format)
            {
              case ReportFormat.Xml:
                exportModel.ExportModelAsXml(str);
                continue;
              case ReportFormat.Html:
                exportModel.ExportModelAsHtml(str);
                continue;
              default:
                throw new ArgumentOutOfRangeException(format.ToString());
            }
          }
          File.Delete(this.file);
          File.Move(str, this.file);
        }
        return 0;
      }
    }

    private void HandleShutdownAndOutputReport()
    {
      this.bridge.DisposeOf(this.stopListeningId);
      this.waitUntilToldToShutDown.Set();
    }
  }
}
