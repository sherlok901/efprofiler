﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Startup.ShutdownCommand
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Google.ProtocolBuffers;
using HibernatingRhinos.Profiler.Appender.Messages;
using System;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace HibernatingRhinos.Profiler.Client.Startup
{
  public class ShutdownCommand : IStartupCommand
  {
    private readonly int port;

    public ShutdownCommand(int port)
    {
      this.port = port;
    }

    public int Execute()
    {
      try
      {
        using (TcpClient tcpClient = new TcpClient())
        {
          tcpClient.Connect(IPAddress.Loopback, this.port);
          using (NetworkStream stream = tcpClient.GetStream())
          {
            MessageStreamWriter<MessageWrapper> messageStreamWriter = new MessageStreamWriter<MessageWrapper>((Stream) stream);
            messageStreamWriter.Write(new MessageWrapper.Builder()
            {
              MessageId = Guid.NewGuid().ToString(),
              Type = MessageWrapper.Types.MessageType.ShutdownAndOutputReport
            }.Build());
            messageStreamWriter.Flush();
            stream.Flush();
          }
        }
        return 0;
      }
      catch (Exception ex)
      {
        StartupParser.WriteLineToConsole("Could not shut down profiling!");
        StartupParser.WriteLineToConsole("The profiler is probably not running in command line mode or was shutdown");
        StartupParser.WriteLineToConsole(ex.ToString());
        return -2;
      }
      finally
      {
        StartupParser.FreeConsoleIfNeeded();
      }
    }
  }
}
