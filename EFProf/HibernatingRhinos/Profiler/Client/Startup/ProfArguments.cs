﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Startup.ProfArguments
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.BackEnd.Profiles;
using HibernatingRhinos.Profiler.Client.Settings;

namespace HibernatingRhinos.Profiler.Client.Startup
{
  public class ProfArguments
  {
    public string File = Profile.CurrentProfile + " Report.xml";
    public HibernatingRhinos.Profiler.Client.Startup.ReportFormat[] ReportFormat = new HibernatingRhinos.Profiler.Client.Startup.ReportFormat[1];
    public bool CmdLineMode;
    public int Port;
    public string InputFile;
    public string LicensePath;
    public bool Shutdown;
    public string ContextFile;

    public int SpecifiedPortOrDefaultPort
    {
      get
      {
        if (this.Port != 0)
          return this.Port;
        return UserPreferencesHolder.UserSettings.ListenPort;
      }
    }

    public bool GuiMode
    {
      get
      {
        if (!this.CmdLineMode)
          return !this.Shutdown;
        return false;
      }
    }
  }
}
