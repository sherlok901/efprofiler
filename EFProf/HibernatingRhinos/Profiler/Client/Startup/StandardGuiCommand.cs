﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Startup.StandardGuiCommand
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using HibernatingRhinos.Profiler.BackEnd.Profiles;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using System.Windows;

namespace HibernatingRhinos.Profiler.Client.Startup
{
  internal class StandardGuiCommand : IStartupCommand
  {
    public int Execute()
    {
      StandardGuiCommand.EnsureThatTheCultureInfoIsValidForXamlParsing();
      App app = new App();
      app.InitializeComponent();
      StandardGuiCommand.AddProfileSpecificResources(app);
      app.Run();
      return 0;
    }

    private static void AddProfileSpecificResources(App app)
    {
      string root = "Resources/" + Profile.CurrentProfile + "/";
      ((IEnumerable<string>) new string[1]
      {
        "ColorsAndBrushes.xaml"
      }).Apply<string>((System.Action<string>) (dictionary => app.Resources.MergedDictionaries.Add(new ResourceDictionary()
      {
        Source = new Uri(root + dictionary, UriKind.Relative)
      })));
    }

    private static void EnsureThatTheCultureInfoIsValidForXamlParsing()
    {
      if (CultureInfo.CurrentCulture.IetfLanguageTag != "en-US")
        return;
      NumberFormatInfo numberFormat = CultureInfo.CurrentCulture.NumberFormat;
      if (numberFormat.NumberDecimalSeparator == "." && numberFormat.NumberGroupSeparator == ",")
        return;
      CultureInfo cultureInfo = (CultureInfo) CultureInfo.GetCultureInfo("en-US").Clone();
      cultureInfo.NumberFormat.NumberDecimalSeparator = ".";
      cultureInfo.NumberFormat.NumberGroupSeparator = ",";
      Thread.CurrentThread.CurrentCulture = cultureInfo;
      Thread.CurrentThread.CurrentUICulture = cultureInfo;
    }
  }
}
