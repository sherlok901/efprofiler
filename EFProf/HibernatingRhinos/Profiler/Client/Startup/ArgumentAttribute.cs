﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Startup.ArgumentAttribute
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System;

namespace HibernatingRhinos.Profiler.Client.Startup
{
  [AttributeUsage(AttributeTargets.Field)]
  public class ArgumentAttribute : Attribute
  {
    private string shortName;
    private string longName;
    private string helpText;
    private object defaultValue;
    private ArgumentType type;

    public ArgumentAttribute(ArgumentType type)
    {
      this.type = type;
    }

    public ArgumentType Type
    {
      get
      {
        return this.type;
      }
    }

    public bool DefaultShortName
    {
      get
      {
        return null == this.shortName;
      }
    }

    public string ShortName
    {
      get
      {
        return this.shortName;
      }
      set
      {
        this.shortName = value;
      }
    }

    public bool DefaultLongName
    {
      get
      {
        return null == this.longName;
      }
    }

    public string LongName
    {
      get
      {
        return this.longName;
      }
      set
      {
        this.longName = value;
      }
    }

    public object DefaultValue
    {
      get
      {
        return this.defaultValue;
      }
      set
      {
        this.defaultValue = value;
      }
    }

    public bool HasDefaultValue
    {
      get
      {
        return null != this.defaultValue;
      }
    }

    public bool HasHelpText
    {
      get
      {
        return null != this.helpText;
      }
    }

    public string HelpText
    {
      get
      {
        return this.helpText;
      }
      set
      {
        this.helpText = value;
      }
    }
  }
}
