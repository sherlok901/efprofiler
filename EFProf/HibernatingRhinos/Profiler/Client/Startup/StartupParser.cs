﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Startup.StartupParser
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace HibernatingRhinos.Profiler.Client.Startup
{
  public class StartupParser
  {
    private static bool wasAttachedToConsole;
    public static ProfArguments CurrentArguments;

    [DllImport("kernel32.dll", SetLastError = true)]
    private static extern bool AttachConsole(int dwProcessId);

    [DllImport("kernel32.dll", SetLastError = true)]
    private static extern bool AllocConsole();

    [DllImport("kernel32.dll", SetLastError = true)]
    private static extern bool FreeConsole();

    public static IStartupCommand ParseCommandLineArguments(string[] args)
    {
      try
      {
        StartupParser.CurrentArguments = new ProfArguments();
        if (!Parser.ParseArguments(args, (object) StartupParser.CurrentArguments, new ErrorReporter(StartupParser.WriteLineToConsole)))
        {
          StartupParser.WriteLineToConsole(Parser.ArgumentsUsage(typeof (ProfArguments)));
          return (IStartupCommand) new ExitCommand();
        }
        if (StartupParser.CurrentArguments.GuiMode)
          return (IStartupCommand) new StandardGuiCommand();
        if (StartupParser.CurrentArguments.CmdLineMode)
          return (IStartupCommand) new CommandLineCommand(StartupParser.CurrentArguments.SpecifiedPortOrDefaultPort, StartupParser.CurrentArguments.File, StartupParser.CurrentArguments.InputFile, StartupParser.CurrentArguments.LicensePath, (IEnumerable<ReportFormat>) StartupParser.CurrentArguments.ReportFormat);
        if (StartupParser.CurrentArguments.Shutdown)
          return (IStartupCommand) new ShutdownCommand(StartupParser.CurrentArguments.SpecifiedPortOrDefaultPort);
        return (IStartupCommand) new StandardGuiCommand();
      }
      finally
      {
        StartupParser.FreeConsoleIfNeeded();
      }
    }

    public static void FreeConsoleIfNeeded()
    {
      if (!StartupParser.wasAttachedToConsole)
        return;
      StartupParser.FreeConsole();
      StartupParser.wasAttachedToConsole = false;
    }

    public static void WriteLineToConsole(string message)
    {
      if (!StartupParser.wasAttachedToConsole)
      {
        if (!StartupParser.AttachConsole(-1))
          StartupParser.AllocConsole();
        StartupParser.wasAttachedToConsole = true;
        Console.WriteLine();
      }
      Console.WriteLine(message);
    }

    public static void WriteToConsole(string message)
    {
      if (!StartupParser.wasAttachedToConsole)
      {
        if (!StartupParser.AttachConsole(-1))
          StartupParser.AllocConsole();
        StartupParser.wasAttachedToConsole = true;
        Console.WriteLine();
      }
      Console.Write(message);
    }
  }
}
