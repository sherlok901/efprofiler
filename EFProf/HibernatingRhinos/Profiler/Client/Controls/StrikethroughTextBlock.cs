﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Controls.StrikethroughTextBlock
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Shapes;

namespace HibernatingRhinos.Profiler.Client.Controls
{
  public class StrikethroughTextBlock : Grid
  {
    public static readonly DependencyProperty TextProperty = DependencyProperty.Register(nameof (Text), typeof (string), typeof (StrikethroughTextBlock), new PropertyMetadata(new PropertyChangedCallback(StrikethroughTextBlock.TextChanged)));
    public static readonly DependencyProperty ForegroundProperty = DependencyProperty.Register(nameof (Foreground), typeof (Brush), typeof (StrikethroughTextBlock), new PropertyMetadata(new PropertyChangedCallback(StrikethroughTextBlock.ForegroundChanged)));
    public static readonly DependencyProperty HasStrikethroughProperty = DependencyProperty.Register(nameof (HasStrikethrough), typeof (bool), typeof (StrikethroughTextBlock), new PropertyMetadata(new PropertyChangedCallback(StrikethroughTextBlock.HasStrikethroughChanged)));

    public string Text
    {
      get
      {
        return this.GetValue(StrikethroughTextBlock.TextProperty) as string;
      }
      set
      {
        this.SetValue(StrikethroughTextBlock.TextProperty, (object) value);
      }
    }

    public Brush Foreground
    {
      get
      {
        return this.GetValue(StrikethroughTextBlock.ForegroundProperty) as Brush;
      }
      set
      {
        this.SetValue(StrikethroughTextBlock.ForegroundProperty, (object) value);
      }
    }

    public bool HasStrikethrough
    {
      get
      {
        return (bool) this.GetValue(StrikethroughTextBlock.HasStrikethroughProperty);
      }
      set
      {
        this.SetValue(StrikethroughTextBlock.HasStrikethroughProperty, (object) value);
      }
    }

    private static void TextChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      if (e.NewValue == e.OldValue)
        return;
      StrikethroughTextBlock block = (StrikethroughTextBlock) d;
      block.RowDefinitions.Clear();
      if (e.NewValue == null)
        return;
      string[] strArray = e.NewValue.ToString().Split(new string[1]
      {
        "\n"
      }, StringSplitOptions.RemoveEmptyEntries);
      for (int row = 0; row < strArray.Length; ++row)
      {
        block.RowDefinitions.Add(new RowDefinition());
        TextBlock actualBlock = new TextBlock()
        {
          Text = strArray[row],
          Foreground = block.Foreground
        };
        Grid.SetRow((UIElement) actualBlock, row);
        block.Children.Add((UIElement) actualBlock);
        if (block.HasStrikethrough)
          StrikethroughTextBlock.AddLine(block, actualBlock, row);
      }
    }

    private static void ForegroundChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      if (e.NewValue == e.OldValue)
        return;
      StrikethroughTextBlock strikethroughTextBlock = (StrikethroughTextBlock) d;
      Brush newValue = e.NewValue as Brush;
      foreach (object child in strikethroughTextBlock.Children)
      {
        TextBlock textBlock = child as TextBlock;
        if (textBlock != null)
        {
          textBlock.Foreground = newValue;
        }
        else
        {
          Line line = child as Line;
          if (line != null)
            line.Stroke = newValue;
        }
      }
    }

    private static void HasStrikethroughChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      if (e.NewValue == e.OldValue)
        return;
      StrikethroughTextBlock block = (StrikethroughTextBlock) d;
      if (!(bool) e.NewValue)
      {
        foreach (Line line in block.Children.OfType<Line>().ToArray<Line>())
          block.Children.Remove((UIElement) line);
      }
      else
      {
        TextBlock[] array = block.Children.OfType<TextBlock>().ToArray<TextBlock>();
        for (int row = 0; row < array.Length; ++row)
          StrikethroughTextBlock.AddLine(block, array[row], row);
      }
    }

    private static void AddLine(StrikethroughTextBlock block, TextBlock actualBlock, int row)
    {
      Line line1 = new Line();
      line1.Stroke = block.Foreground;
      line1.StrokeThickness = 1.0;
      line1.VerticalAlignment = VerticalAlignment.Center;
      line1.X1 = 0.0;
      Line line2 = line1;
      Binding binding = new Binding()
      {
        Source = (object) actualBlock,
        Path = new PropertyPath("ActualWidth", new object[0]),
        Mode = BindingMode.Default
      };
      BindingOperations.SetBinding((DependencyObject) line2, Line.X2Property, (BindingBase) binding);
      Grid.SetRow((UIElement) line2, row);
      block.Children.Add((UIElement) line2);
    }
  }
}
