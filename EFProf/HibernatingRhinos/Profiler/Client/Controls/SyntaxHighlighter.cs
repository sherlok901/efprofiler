﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Controls.SyntaxHighlighter
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.Client.Infrastructure;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace HibernatingRhinos.Profiler.Client.Controls
{
  public partial class SyntaxHighlighter : UserControl, IComponentConnector
  {
    public static readonly DependencyProperty TextProperty = DependencyProperty.Register(nameof (Text), typeof (string), typeof (SyntaxHighlighter), new PropertyMetadata(new PropertyChangedCallback(SyntaxHighlighter.TextChanged)));
    //internal AqiStar.TextBox _textBox;
    //private bool _contentLoaded;

    public SyntaxHighlighter()
    {
      this.InitializeComponent();
    }

    public string Text
    {
      get
      {
        return this.GetValue(SyntaxHighlighter.TextProperty) as string;
      }
      set
      {
        this.SetValue(SyntaxHighlighter.TextProperty, (object) value);
      }
    }

    private static void TextChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      if (e.NewValue == e.OldValue)
        return;
      SyntaxHighlighter syntaxHighlighter = (SyntaxHighlighter) d;
      syntaxHighlighter._textBox.IsReadOnly = false;
      syntaxHighlighter._textBox.Text = e.NewValue != null ? e.NewValue.ToString() : string.Empty;
      syntaxHighlighter._textBox.IsReadOnly = true;
    }

    private void CopyClick(object sender, RoutedEventArgs e)
    {
      if (this._textBox.Selection.TextLength == 0)
      {
        if (string.IsNullOrEmpty(this._textBox.Text))
          return;
        SafeClipboard.SetText(this._textBox.Text);
      }
      else
        SafeClipboard.SetText(this._textBox.Selection.Text);
    }

    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //public void InitializeComponent()
    //{
    //  if (this._contentLoaded)
    //    return;
    //  this._contentLoaded = true;
    //  Application.LoadComponent((object) this, new Uri("/EFProf;component/controls/syntaxhighlighter.xaml", UriKind.Relative));
    //}

    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[EditorBrowsable(EditorBrowsableState.Never)]
    //void IComponentConnector.Connect(int connectionId, object target)
    //{
    //  switch (connectionId)
    //  {
    //    case 1:
    //      this._textBox = (AqiStar.TextBox) target;
    //      break;
    //    case 2:
    //      ((MenuItem) target).Click += new RoutedEventHandler(this.CopyClick);
    //      break;
    //    default:
    //      this._contentLoaded = true;
    //      break;
    //  }
    //}
  }
}
