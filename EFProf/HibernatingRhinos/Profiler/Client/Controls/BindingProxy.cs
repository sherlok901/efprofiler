﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Controls.BindingProxy
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System.Windows;

namespace HibernatingRhinos.Profiler.Client.Controls
{
  public class BindingProxy : Freezable
  {
    public static readonly DependencyProperty DataProperty = DependencyProperty.Register(nameof (Data), typeof (object), typeof (BindingProxy));

    protected override Freezable CreateInstanceCore()
    {
      return (Freezable) new BindingProxy();
    }

    public object Data
    {
      get
      {
        return this.GetValue(BindingProxy.DataProperty);
      }
      set
      {
        this.SetValue(BindingProxy.DataProperty, value);
      }
    }
  }
}
