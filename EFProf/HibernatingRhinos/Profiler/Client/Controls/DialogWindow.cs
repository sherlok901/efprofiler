﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Controls.DialogWindow
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System.Windows;
using System.Windows.Input;

namespace HibernatingRhinos.Profiler.Client.Controls
{
  public class DialogWindow : ChromelessWindow
  {
    public static readonly DependencyProperty CanMaximiseProperty = DependencyProperty.Register(nameof (CanMaximise), typeof (bool), typeof (DialogWindow), new PropertyMetadata((object) false));

    static DialogWindow()
    {
      FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof (DialogWindow), (PropertyMetadata) new FrameworkPropertyMetadata((object) typeof (DialogWindow)));
    }

    public bool CanMaximise
    {
      get
      {
        return (bool) this.GetValue(DialogWindow.CanMaximiseProperty);
      }
      set
      {
        this.SetValue(DialogWindow.CanMaximiseProperty, (object) value);
      }
    }

    public DialogWindow()
    {
      this.ShowInTaskbar = false;
      this.WindowStartupLocation = WindowStartupLocation.CenterOwner;
      this.KeyDown += (KeyEventHandler) ((sender, e) =>
      {
        if (e.Key != Key.Escape)
          return;
        this.DialogResult = new bool?(false);
      });
    }
  }
}
