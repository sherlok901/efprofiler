﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Controls.GridColumnHeader
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System.Windows;
using System.Windows.Controls;

namespace HibernatingRhinos.Profiler.Client.Controls
{
  public class GridColumnHeader : Button
  {
    public static readonly DependencyProperty IsSortedAscendingProperty = DependencyProperty.Register(nameof (IsSortedAscending), typeof (bool?), typeof (GridColumnHeader), (PropertyMetadata) null);
    public static readonly DependencyProperty IsSortedDescendingProperty = DependencyProperty.Register(nameof (IsSortedDescending), typeof (bool?), typeof (GridColumnHeader), (PropertyMetadata) null);

    public bool? IsSortedAscending
    {
      get
      {
        return (bool?) this.GetValue(GridColumnHeader.IsSortedAscendingProperty);
      }
      set
      {
        this.SetValue(GridColumnHeader.IsSortedAscendingProperty, (object) value);
      }
    }

    public bool? IsSortedDescending
    {
      get
      {
        return (bool?) this.GetValue(GridColumnHeader.IsSortedDescendingProperty);
      }
      set
      {
        this.SetValue(GridColumnHeader.IsSortedDescendingProperty, (object) value);
      }
    }
  }
}
