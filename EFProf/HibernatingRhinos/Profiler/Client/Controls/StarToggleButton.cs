﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Controls.StarToggleButton
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Media;

namespace HibernatingRhinos.Profiler.Client.Controls
{
  public class StarToggleButton : ToggleButton
  {
    public static readonly DependencyProperty ColorProperty = DependencyProperty.RegisterAttached(nameof (Color), typeof (Brush), typeof (StarToggleButton), (PropertyMetadata) null);
    public static readonly string DefaultStarColor = Application.Current != null ? ((SolidColorBrush) Application.Current.Resources[(object) "SecondaryBackground"]).Color.ToString() : "#FFEAC0";

    static StarToggleButton()
    {
      StarToggleButton.StarColors = new List<string>()
      {
        "Yellow",
        "Orange",
        "Red",
        "Green",
        "Blue",
        "Gray"
      };
    }

    public static List<string> StarColors { get; private set; }

    public Brush Color
    {
      get
      {
        return (Brush) this.GetValue(StarToggleButton.ColorProperty);
      }
      set
      {
        this.SetValue(StarToggleButton.ColorProperty, (object) value);
      }
    }
  }
}
