﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Controls.ShortcutMenuHeader
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System.Windows;
using System.Windows.Controls;

namespace HibernatingRhinos.Profiler.Client.Controls
{
  [TemplatePart(Name = "Shortcut", Type = typeof (TextBlock))]
  [TemplatePart(Name = "Text", Type = typeof (TextBlock))]
  public class ShortcutMenuHeader : Control
  {
    public static readonly DependencyProperty TextProperty = DependencyProperty.Register(nameof (Text), typeof (string), typeof (ShortcutMenuHeader), new PropertyMetadata((object) string.Empty));
    public static readonly DependencyProperty ShortcutProperty = DependencyProperty.Register(nameof (Shortcut), typeof (string), typeof (ShortcutMenuHeader), new PropertyMetadata((object) string.Empty));

    public ShortcutMenuHeader()
    {
      this.DefaultStyleKey = (object) typeof (ShortcutMenuHeader);
    }

    public string Text
    {
      get
      {
        return (string) this.GetValue(ShortcutMenuHeader.TextProperty);
      }
      set
      {
        this.SetValue(ShortcutMenuHeader.TextProperty, (object) value);
      }
    }

    public string Shortcut
    {
      get
      {
        return (string) this.GetValue(ShortcutMenuHeader.ShortcutProperty);
      }
      set
      {
        this.SetValue(ShortcutMenuHeader.ShortcutProperty, (object) value);
      }
    }
  }
}
