﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Controls.Dialog
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using HibernatingRhinos.Profiler.Client.CaliburnInfrastructure;
using HibernatingRhinos.Profiler.Client.Shell;
using System.Collections.Generic;
using System.Windows;

namespace HibernatingRhinos.Profiler.Client.Controls
{
  public static class Dialog
  {
    public static readonly DependencyProperty EnableMaximiseProperty = DependencyProperty.RegisterAttached("EnableMaximise", typeof (bool), typeof (Dialog), new PropertyMetadata((object) false));
    public static readonly DependencyProperty MinHeightProperty = DependencyProperty.RegisterAttached("MinHeight", typeof (double), typeof (Dialog), new PropertyMetadata((object) 0.0));
    public static readonly DependencyProperty MinWidthProperty = DependencyProperty.RegisterAttached("MinWidth", typeof (double), typeof (Dialog), new PropertyMetadata((object) 0.0));
    public static readonly DependencyProperty HeightProperty = DependencyProperty.RegisterAttached("Height", typeof (double), typeof (Dialog), new PropertyMetadata((object) double.NaN));
    public static readonly DependencyProperty WidthProperty = DependencyProperty.RegisterAttached("Width", typeof (double), typeof (Dialog), new PropertyMetadata((object) double.NaN));

    public static bool Show<TModel>(object context = null, bool windowWithFullLogo = false)
    {
      return Dialog.Show((object) IoC.Get<TModel>((string) null), context, windowWithFullLogo);
    }

    public static bool Show(object model, object context = null, bool windowWithFullLogo = false)
    {
      ProfilerWindowManager profilerWindowManager = IoC.Get<ProfilerWindowManager>((string) null);
      Dictionary<string, object> dictionary = new Dictionary<string, object>();
      if (windowWithFullLogo)
        dictionary.Add("Template", Application.Current.Resources[(object) "LicenseDialogTemplate"]);
      return profilerWindowManager.ShowDialog(model, context, (IDictionary<string, object>) dictionary) ?? false;
    }

    public static void ShowMessage(string title, string message)
    {
      Dialog.Show((object) new MessageBoxModel(title, message), (object) null, false);
    }

    public static bool GetEnableMaximise(DependencyObject obj)
    {
      return (bool) obj.GetValue(Dialog.EnableMaximiseProperty);
    }

    public static void SetEnableMaximise(DependencyObject obj, bool value)
    {
      obj.SetValue(Dialog.EnableMaximiseProperty, (object) value);
    }

    public static double GetHeight(DependencyObject obj)
    {
      return (double) obj.GetValue(Dialog.HeightProperty);
    }

    public static void SetHeight(DependencyObject obj, double value)
    {
      obj.SetValue(Dialog.HeightProperty, (object) value);
    }

    public static double GetWidth(DependencyObject obj)
    {
      return (double) obj.GetValue(Dialog.WidthProperty);
    }

    public static void SetWidth(DependencyObject obj, double value)
    {
      obj.SetValue(Dialog.WidthProperty, (object) value);
    }

    public static double GetMinHeight(DependencyObject obj)
    {
      return (double) obj.GetValue(Dialog.MinHeightProperty);
    }

    public static void SetMinHeight(DependencyObject obj, double value)
    {
      obj.SetValue(Dialog.MinHeightProperty, (object) value);
    }

    public static double GetMinWidth(DependencyObject obj)
    {
      return (double) obj.GetValue(Dialog.MinWidthProperty);
    }

    public static void SetMinWidth(DependencyObject obj, double value)
    {
      obj.SetValue(Dialog.MinWidthProperty, (object) value);
    }
  }
}
