﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Controls.WatermarkTextBox
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace HibernatingRhinos.Profiler.Client.Controls
{
  [TemplateVisualState(GroupName = "FocusStates", Name = "Focused")]
  [TemplateVisualState(GroupName = "WatermarkStates", Name = "Unwatermarked")]
  [TemplateVisualState(GroupName = "WatermarkStates", Name = "Watermarked")]
  [TemplateVisualState(GroupName = "WatermarkStates", Name = "SemiUnwatermarked")]
  [TemplatePart(Name = "Watermark", Type = typeof (ContentControl))]
  [TemplateVisualState(GroupName = "CommonStates", Name = "Normal")]
  [TemplateVisualState(GroupName = "CommonStates", Name = "MouseOver")]
  [TemplateVisualState(GroupName = "CommonStates", Name = "Disabled")]
  [TemplateVisualState(GroupName = "FocusStates", Name = "Unfocused")]
  public class WatermarkTextBox : TextBox
  {
    public static readonly DependencyProperty WatermarkTextProperty = DependencyProperty.Register(nameof (WatermarkText), typeof (object), typeof (WatermarkTextBox), new PropertyMetadata(new PropertyChangedCallback(WatermarkTextBox.OnWatermarkPropertyChanged)));
    private const string ElementContentName = "Watermark";
    private ContentControl elementContent;
    private bool hasFocus;
    private bool isHovered;

    public WatermarkTextBox()
    {
      this.DefaultStyleKey = (object) typeof (WatermarkTextBox);
      this.MouseEnter += new MouseEventHandler(this.OnMouseEnter);
      this.MouseLeave += new MouseEventHandler(this.OnMouseLeave);
      this.Loaded += new RoutedEventHandler(this.OnLoaded);
      this.LostFocus += new RoutedEventHandler(this.OnLostFocus);
      this.GotFocus += new RoutedEventHandler(this.OnGotFocus);
      this.TextChanged += new TextChangedEventHandler(this.OnTextChanged);
      this.IsEnabledChanged += (DependencyPropertyChangedEventHandler) ((param0, param1) => this.ChangeVisualState(true));
    }

    public object WatermarkText
    {
      get
      {
        return this.GetValue(WatermarkTextBox.WatermarkTextProperty);
      }
      set
      {
        this.SetValue(WatermarkTextBox.WatermarkTextProperty, value);
      }
    }

    protected virtual void OnLoaded(object sender, RoutedEventArgs e)
    {
      this.ApplyTemplate();
      this.ChangeVisualState(false);
    }

    private void ChangeVisualState()
    {
      this.ChangeVisualState(true);
    }

    private new void ChangeVisualState(bool useTransitions)
    {
      if (!this.IsEnabled)
        VisualStateHelper.GoToState((Control) this, (useTransitions ? 1 : 0) != 0, "Disabled", "Normal");
      else if (this.isHovered)
        VisualStateHelper.GoToState((Control) this, (useTransitions ? 1 : 0) != 0, "MouseOver", "Normal");
      else
        VisualStateHelper.GoToState((Control) this, (useTransitions ? 1 : 0) != 0, "Normal");
      if (this.hasFocus && this.IsEnabled)
        VisualStateHelper.GoToState((Control) this, (useTransitions ? 1 : 0) != 0, "Focused", "Unfocused");
      else
        VisualStateHelper.GoToState((Control) this, (useTransitions ? 1 : 0) != 0, "Unfocused");
      if (!this.hasFocus && this.WatermarkText != null && string.IsNullOrEmpty(this.Text))
        VisualStateHelper.GoToState((Control) this, (useTransitions ? 1 : 0) != 0, "Watermarked", "Unwatermarked");
      else if (this.hasFocus && string.IsNullOrEmpty(this.Text))
        VisualStateHelper.GoToState((Control) this, true, "SemiUnwatermarked");
      else
        VisualStateHelper.GoToState((Control) this, (useTransitions ? 1 : 0) != 0, "Unwatermarked");
    }

    public override void OnApplyTemplate()
    {
      base.OnApplyTemplate();
      this.elementContent = this.ExtractTemplatePart<ContentControl>("Watermark");
      this.OnWatermarkChanged();
      this.ChangeVisualState(false);
    }

    private T ExtractTemplatePart<T>(string partName) where T : DependencyObject
    {
      DependencyObject templateChild = this.GetTemplateChild(partName);
      return WatermarkTextBox.EnsureTemplatePart<T>(partName, templateChild);
    }

    private static T EnsureTemplatePart<T>(string partName, DependencyObject obj) where T : DependencyObject
    {
      return obj as T;
    }

    private void OnGotFocus(object sender, RoutedEventArgs e)
    {
      if (!this.IsEnabled)
        return;
      this.hasFocus = true;
      if (!string.IsNullOrEmpty(this.Text))
        this.Select(0, this.Text.Length);
      this.ChangeVisualState();
    }

    private void OnLostFocus(object sender, RoutedEventArgs e)
    {
      this.hasFocus = false;
      this.ChangeVisualState();
    }

    private void OnMouseEnter(object sender, MouseEventArgs e)
    {
      this.isHovered = true;
      if (this.hasFocus)
        return;
      this.ChangeVisualState();
    }

    private void OnMouseLeave(object sender, MouseEventArgs e)
    {
      this.isHovered = false;
      this.ChangeVisualState();
    }

    private void OnTextChanged(object sender, TextChangedEventArgs e)
    {
      this.ChangeVisualState();
    }

    private void OnWatermarkChanged()
    {
      if (this.elementContent == null)
        return;
      Control watermarkText = this.WatermarkText as Control;
      if (watermarkText == null)
        return;
      watermarkText.IsTabStop = false;
      watermarkText.IsHitTestVisible = false;
    }

    private static void OnWatermarkPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
    {
      WatermarkTextBox watermarkTextBox = sender as WatermarkTextBox;
      watermarkTextBox.OnWatermarkChanged();
      watermarkTextBox.ChangeVisualState();
    }
  }
}
