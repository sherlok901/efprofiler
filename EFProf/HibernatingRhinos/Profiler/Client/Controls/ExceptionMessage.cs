﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Controls.ExceptionMessage
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System;
using System.Windows;
using System.Windows.Controls;

namespace HibernatingRhinos.Profiler.Client.Controls
{
  [TemplateVisualState(GroupName = "VisibilityStates", Name = "Hidden")]
  [TemplateVisualState(GroupName = "VisibilityStates", Name = "Visible")]
  public class ExceptionMessage : ContentControl
  {
    public static DependencyProperty ExceptionProperty = DependencyProperty.Register(nameof (Exception), typeof (Exception), typeof (ExceptionMessage), new PropertyMetadata(new PropertyChangedCallback(ExceptionMessage.ExceptionPropertyChangedCallback)));

    private static void ExceptionPropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      VisualStateHelper.GoToState((Control) (d as ExceptionMessage), true, e.NewValue != null ? "Visible" : "Hidden");
    }

    public Exception Exception
    {
      get
      {
        return (Exception) this.GetValue(ExceptionMessage.ExceptionProperty);
      }
      set
      {
        this.SetValue(ExceptionMessage.ExceptionProperty, (object) value);
      }
    }
  }
}
