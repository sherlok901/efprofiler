﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Controls.VisualStateHelper
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System;
using System.Windows;
using System.Windows.Controls;

namespace HibernatingRhinos.Profiler.Client.Controls
{
  internal static class VisualStateHelper
  {
    public const string GroupCommon = "CommonStates";
    public const string GroupFocus = "FocusStates";
    public const string GroupSelection = "SelectionStates";
    public const string GroupWatermark = "WatermarkStates";
    public const string GroupVisibility = "VisibilityStates";
    public const string StateDisabled = "Disabled";
    public const string StateFocused = "Focused";
    public const string StateMouseOver = "MouseOver";
    public const string StateNormal = "Normal";
    public const string StateUnfocused = "Unfocused";
    public const string StateUnwatermarked = "Unwatermarked";
    public const string StateWatermarked = "Watermarked";
    public const string StateSemiUnwatermarked = "SemiUnwatermarked";
    public const string StateHidden = "Hidden";
    public const string StateVisible = "Visible";

    public static void GoToState(Control control, bool useTransitions, params string[] stateNames)
    {
      if (control == null)
        throw new ArgumentNullException(nameof (control));
      if (stateNames == null)
        return;
      foreach (string stateName in stateNames)
      {
        if (VisualStateManager.GoToState((FrameworkElement) control, stateName, useTransitions))
          break;
      }
    }
  }
}
