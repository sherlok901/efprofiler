﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Controls.WatermarkComboBox
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System.Windows;
using System.Windows.Controls;

namespace HibernatingRhinos.Profiler.Client.Controls
{
  [TemplatePart(Name = "Watermark", Type = typeof (ContentControl))]
  [TemplateVisualState(GroupName = "WatermarkStates", Name = "Unwatermarked")]
  [TemplateVisualState(GroupName = "WatermarkStates", Name = "Watermarked")]
  [TemplateVisualState(GroupName = "WatermarkStates", Name = "SemiUnwatermarked")]
  public class WatermarkComboBox : ComboBox
  {
    public static readonly DependencyProperty WatermarkTextProperty = DependencyProperty.Register(nameof (WatermarkText), typeof (object), typeof (WatermarkComboBox), new PropertyMetadata(new PropertyChangedCallback(WatermarkComboBox.OnWatermarkPropertyChanged)));
    private const string WatermarkContentName = "Watermark";
    internal ContentControl watermarkContent;

    static WatermarkComboBox()
    {
      FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof (WatermarkComboBox), (PropertyMetadata) new FrameworkPropertyMetadata((object) typeof (WatermarkComboBox)));
    }

    public WatermarkComboBox()
    {
      this.Loaded += new RoutedEventHandler(this.InitializeDefaults);
      this.GotFocus += new RoutedEventHandler(this.OnGotFocus);
      this.LostFocus += new RoutedEventHandler(this.OnLostFocus);
      this.SelectionChanged += new SelectionChangedEventHandler(this.OnSelectionChanged);
    }

    private void OnSelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (this.SelectedItem == null)
        return;
      VisualStateHelper.GoToState((Control) this, true, "Unwatermarked");
    }

    private void OnLostFocus(object sender, RoutedEventArgs e)
    {
      if (this.SelectedItem != null)
        return;
      VisualStateHelper.GoToState((Control) this, true, "Watermarked");
    }

    private void OnGotFocus(object sender, RoutedEventArgs e)
    {
      if (this.SelectedItem != null)
        return;
      VisualStateHelper.GoToState((Control) this, true, "SemiUnwatermarked");
    }

    public override void OnApplyTemplate()
    {
      base.OnApplyTemplate();
      this.watermarkContent = (ContentControl) this.GetTemplateChild("Watermark");
    }

    private void InitializeDefaults(object sender, RoutedEventArgs e)
    {
      VisualStateHelper.GoToState((Control) this, true, "Watermarked");
    }

    public object WatermarkText
    {
      get
      {
        return this.GetValue(WatermarkComboBox.WatermarkTextProperty);
      }
      set
      {
        this.SetValue(WatermarkComboBox.WatermarkTextProperty, value);
      }
    }

    private static void OnWatermarkPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
    {
    }
  }
}
