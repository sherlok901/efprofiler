﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Controls.DataContextPropagationGrid
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace HibernatingRhinos.Profiler.Client.Controls
{
  public class DataContextPropagationGrid : Grid
  {
    public static readonly DependencyProperty InheritedDataContextProperty = DependencyProperty.Register("InheritedDataContext", typeof (object), typeof (DataContextPropagationGrid), new PropertyMetadata((object) null, new PropertyChangedCallback(DataContextPropagationGrid.OnInheritedDataContextChanged)));

    public DataContextPropagationGrid()
    {
      this.SetBinding(DataContextPropagationGrid.InheritedDataContextProperty, (BindingBase) new Binding());
    }

    private static void OnInheritedDataContextChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      DataContextPropagationGrid contextPropagationGrid = (DataContextPropagationGrid) d;
      contextPropagationGrid.DataContext = e.NewValue;
      contextPropagationGrid.ClearValue(FrameworkElement.DataContextProperty);
    }
  }
}
