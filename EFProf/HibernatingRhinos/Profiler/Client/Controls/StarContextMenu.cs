﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Controls.StarContextMenu
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System.Windows;
using System.Windows.Controls;

namespace HibernatingRhinos.Profiler.Client.Controls
{
  public class StarContextMenu : ContextMenu
  {
    public static readonly DependencyProperty CurrentItemProperty = DependencyProperty.RegisterAttached(nameof (CurrentItem), typeof (object), typeof (StarContextMenu), (PropertyMetadata) null);

    public object CurrentItem
    {
      get
      {
        return this.GetValue(StarContextMenu.CurrentItemProperty);
      }
      set
      {
        this.SetValue(StarContextMenu.CurrentItemProperty, value);
      }
    }
  }
}
