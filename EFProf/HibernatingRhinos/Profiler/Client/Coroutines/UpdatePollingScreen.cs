﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Coroutines.UpdatePollingScreen
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using HibernatingRhinos.Profiler.Client.Infrastructure;
using System;

namespace HibernatingRhinos.Profiler.Client.Coroutines
{
  public class UpdatePollingScreen : IResult
  {
    private readonly PollingScreen screen;

    public UpdatePollingScreen(PollingScreen screen)
    {
      this.screen = screen;
    }

    public void Execute(CoroutineExecutionContext context)
    {
      this.screen.NotifyOnUpdate += new System.Action(this.UpdateCompleted);
      this.screen.ForceUpdate();
    }

    public void UpdateCompleted()
    {
      if (this.Completed != null)
        this.Completed((object) this, new ResultCompletionEventArgs());
      this.screen.NotifyOnUpdate -= new System.Action(this.UpdateCompleted);
    }

    public event EventHandler<ResultCompletionEventArgs> Completed;
  }
}
