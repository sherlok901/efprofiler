﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Coroutines.ExportToHtml
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using HibernatingRhinos.Profiler.BackEnd.Profiles;
using HibernatingRhinos.Profiler.Client.Reports;
using HibernatingRhinos.Profiler.Client.Sessions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace HibernatingRhinos.Profiler.Client.Coroutines
{
  public class ExportToHtml : IResult
  {
    private readonly Stream fileStream;
    private readonly IEnumerable<IReport> reports;
    private readonly SessionListModel sessions;

    public ExportToHtml(Stream fileStream, IEnumerable<IReport> reports, SessionListModel sessions)
    {
      this.fileStream = fileStream;
      this.reports = reports;
      this.sessions = sessions;
    }

    public void Execute(CoroutineExecutionContext context)
    {
      string reportTemplate = this.GetReportTemplate();
      StringBuilder stringBuilder = new StringBuilder();
      foreach (IReport report in this.reports)
      {
        string jsonString = report.ExportReportToJsonString();
        stringBuilder.Append("var ").Append(report.JsonReportName).Append(" = ").Append(jsonString).AppendLine(";").AppendLine();
      }
      stringBuilder.Append("var queriesBySession = ").Append(this.sessions.ExportToJsonString()).AppendLine(";");
      stringBuilder.AppendLine("var supportedFeatures = ");
      stringBuilder.AppendLine("{");
      foreach (SupportedFeatures feature in Enum.GetValues(typeof (SupportedFeatures)))
        stringBuilder.Append("\t").Append((object) feature).Append(": ").Append(Profile.Current.Supports(feature).ToString().ToLower()).AppendLine(",");
      stringBuilder.Append("\t").Append("profileName: '").Append(Profile.Current.ProfileName).AppendLine("'");
      stringBuilder.AppendLine("};");
      string str = reportTemplate.Replace("/*{{report-css}}*/", this.GetExportCss()).Replace("{{report-name}}", Profile.CurrentProfile).Replace("{{report-title}}", Profile.CurrentProfileDisplayName).Replace("{{report-date}}", DateTime.Now.ToString()).Replace("{{report-root-url}}", Profile.Current.HomeUrl).Replace("{{reports-as-json}}", stringBuilder.ToString());
      using (StreamWriter streamWriter = new StreamWriter(this.fileStream))
        streamWriter.Write(str);
      this.fileStream.Dispose();
      this.Completed((object) this, new ResultCompletionEventArgs());
    }

    public event EventHandler<ResultCompletionEventArgs> Completed;

    private string GetExportCss()
    {
      using (Stream manifestResourceStream = this.GetType().Assembly.GetManifestResourceStream("HibernatingRhinos.Profiler.Client.Exports." + Profile.CurrentProfile + ".css"))
      {
        using (StreamReader streamReader = new StreamReader(manifestResourceStream))
          return streamReader.ReadToEnd();
      }
    }

    private string GetReportTemplate()
    {
      using (Stream manifestResourceStream = this.GetType().Assembly.GetManifestResourceStream("HibernatingRhinos.Profiler.Client.Exports.ReportTemplate.html"))
      {
        using (StreamReader streamReader = new StreamReader(manifestResourceStream))
          return streamReader.ReadToEnd();
      }
    }
  }
}
