﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Coroutines.ExportToXml
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using HibernatingRhinos.Profiler.BackEnd.Profiles;
using HibernatingRhinos.Profiler.Client.Reports;
using HibernatingRhinos.Profiler.Client.Sessions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;

namespace HibernatingRhinos.Profiler.Client.Coroutines
{
  public class ExportToXml : IResult
  {
    private static readonly ILog logger = LogManager.GetLog(typeof (ExportToXml));
    private readonly Stream fileStream;
    private readonly ReportListModel reports;
    private readonly SessionListModel sessions;

    public ExportToXml(Stream fileStream, ReportListModel reports, SessionListModel sessions)
    {
      this.fileStream = fileStream;
      this.reports = reports;
      this.sessions = sessions;
    }

    public static XNamespace Namespace
    {
      get
      {
        return XNamespace.Get("http://reports.hibernatingrhinos.com/" + Profile.CurrentProfile + ".Profiler/2009/10");
      }
    }

    public void Execute(CoroutineExecutionContext context)
    {
      XElement xelement = new XElement(ExportToXml.Namespace + "report");
      xelement.Add((object) this.sessions.Export());
      foreach (IReport report in (IEnumerable<IReport>) this.reports.Items)
      {
        report.TimerTicked((object) this, EventArgs.Empty);
        xelement.Add((object) report.ExportReport());
      }
      try
      {
        using (this.fileStream)
          new XDocument(new object[1]{ (object) xelement }).Save(this.fileStream);
      }
      catch (Exception ex)
      {
        ExportToXml.logger.Error(ex);
      }
      this.Completed((object) this, new ResultCompletionEventArgs());
    }

    public event EventHandler<ResultCompletionEventArgs> Completed;
  }
}
