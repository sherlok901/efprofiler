﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Util.AlternativeDataStreamFileUtil
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Microsoft.Win32.SafeHandles;
using System;
using System.IO;
using System.Runtime.InteropServices;

namespace HibernatingRhinos.Profiler.Client.Util
{
  internal static class AlternativeDataStreamFileUtil
  {
    private const uint GENERIC_WRITE = 1073741824;
    private const uint GENERIC_READ = 2147483648;
    private const uint FILE_SHARE_READ = 1;
    private const uint FILE_SHARE_WRITE = 2;
    private const uint CREATE_NEW = 1;
    private const uint CREATE_ALWAYS = 2;
    private const uint OPEN_EXISTING = 3;
    private const uint OPEN_ALWAYS = 4;

    [DllImport("kernel32", SetLastError = true)]
    private static extern IntPtr CreateFile(string filename, uint desiredAccess, uint shareMode, IntPtr attributes, uint creationDisposition, uint flagsAndAttributes, IntPtr templateFile);

    public static byte[] ReadBytes(string file, string stream)
    {
      using (SafeFileHandle handle = new SafeFileHandle(AlternativeDataStreamFileUtil.CreateFile(file + ":" + stream, 2147483648U, 1U, IntPtr.Zero, 3U, 0U, IntPtr.Zero), true))
      {
        using (FileStream fileStream = new FileStream(handle, FileAccess.Read))
          return new BinaryReader((Stream) fileStream).ReadBytes((int) fileStream.Length);
      }
    }

    public static void Write(byte[] buffer, string file, string stream)
    {
      using (SafeFileHandle handle = new SafeFileHandle(AlternativeDataStreamFileUtil.CreateFile(file + ":" + stream, 1073741824U, 2U, IntPtr.Zero, 2U, 0U, IntPtr.Zero), true))
      {
        using (FileStream fileStream = new FileStream(handle, FileAccess.Write))
          fileStream.Write(buffer, 0, buffer.Length);
      }
    }
  }
}
