﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Resources.AttachedProperties.GroupingHelper
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System.Windows;
using System.Windows.Controls;

namespace HibernatingRhinos.Profiler.Client.Resources.AttachedProperties
{
  public static class GroupingHelper
  {
    public static readonly DependencyProperty IsGroupCollapsedProperty = DependencyProperty.RegisterAttached("IsGroupCollapsed", typeof (bool?), typeof (GroupingHelper), new PropertyMetadata((object) null, new PropertyChangedCallback(GroupingHelper.IsGroupCollapsedPropertyChanged)));

    public static void SetIsGroupCollapsed(UIElement element, bool? value)
    {
      element.SetValue(GroupingHelper.IsGroupCollapsedProperty, (object) value);
    }

    public static bool? GetIsGroupCollapsed(UIElement element)
    {
      return (bool?) element.GetValue(GroupingHelper.IsGroupCollapsedProperty);
    }

    private static void IsGroupCollapsedPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
    {
      if (!(sender is Expander) || e.NewValue == null)
        return;
      ((Expander) sender).IsExpanded = !(bool) e.NewValue;
    }
  }
}
