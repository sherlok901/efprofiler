﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Connections.EditConnectionModel
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using HibernatingRhinos.Profiler.BackEnd.Infrastructure;
using HibernatingRhinos.Profiler.BackEnd.Infrastructure.Connections;
using HibernatingRhinos.Profiler.BackEnd.Profiles;
using HibernatingRhinos.Profiler.Client.Extensions;
using HibernatingRhinos.Profiler.Client.Infrastructure;
using HibernatingRhinos.Profiler.Client.Services;
using HibernatingRhinos.Profiler.Client.Settings;
using HibernatingRhinos.Profiler.Client.Tracking;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Input;

namespace HibernatingRhinos.Profiler.Client.Connections
{
  public class EditConnectionModel : ProfilerScreen
  {
    private IConnectionDefinition currentDefinition;
    private Exception exception;
    private readonly ObservableCollection<ConnectionModel> connections;
    private readonly ITrackingService trackingService;

    public EditConnectionModel(ITrackingService trackingService)
    {
      this.trackingService = trackingService;
      this.AvailableConnectionTypes = Profile.Current.GetAvailableConnectionTypes();
      this.CurrentDefinition = this.AvailableConnectionTypes.FirstOrDefault<IConnectionDefinition>();
      this.connections = new ObservableCollection<ConnectionModel>();
      this.connections.CollectionChanged += (NotifyCollectionChangedEventHandler) ((s, e) =>
      {
        this.NotifyOfPropertyChange<bool>((System.Linq.Expressions.Expression<Func<bool>>) (() => this.HasConnections));
        this.NotifyOfPropertyChange<bool>((System.Linq.Expressions.Expression<Func<bool>>) (() => this.HasNoConnections));
        this.NotifyOfPropertyChange<bool>((System.Linq.Expressions.Expression<Func<bool>>) (() => this.CanOK));
      });
      this.DisplayName = "Edit Default Connection";
      UserPreferencesHolder.UserSettings.ConnectionInfo.Apply<ConnectionInfo>((System.Action<ConnectionInfo>) (x => this.connections.Add(this.CreateViewModel(x))));
    }

    public IList<IConnectionDefinition> AvailableConnectionTypes { get; private set; }

    public IConnectionDefinition CurrentDefinition
    {
      get
      {
        return this.currentDefinition;
      }
      set
      {
        this.currentDefinition = value;
        this.NotifyOfPropertyChange<IConnectionDefinition>((System.Linq.Expressions.Expression<Func<IConnectionDefinition>>) (() => this.CurrentDefinition));
      }
    }

    public ObservableCollection<ConnectionModel> Connections
    {
      get
      {
        return this.connections;
      }
    }

    public bool HasNoConnections
    {
      get
      {
        return !this.HasConnections;
      }
    }

    public bool HasConnections
    {
      get
      {
        return this.Connections.Count > 0;
      }
    }

    public Exception Exception
    {
      get
      {
        return this.exception;
      }
      set
      {
        this.exception = value;
        this.NotifyOfPropertyChange<Exception>((System.Linq.Expressions.Expression<Func<Exception>>) (() => this.Exception));
      }
    }

    public bool CanOK
    {
      get
      {
        if (this.RequireConnection)
          return this.HasConnections;
        return true;
      }
    }

    public bool Cancelled { get; private set; }

    public bool RequireConnection { get; set; }

    public void AddConnection()
    {
      ConnectionModel connectionModel1 = new ConnectionModel();
      connectionModel1.Definition = this.CurrentDefinition;
      connectionModel1.IsSelected = true;
      ConnectionModel connectionModel2 = connectionModel1;
      this.Connections.Apply<ConnectionModel>((System.Action<ConnectionModel>) (x => x.IsSelected = false));
      this.Connections.Insert(0, connectionModel2);
    }

    public void RemoveConnection(ConnectionModel model)
    {
      int index = this.Connections.IndexOf(model);
      this.Connections.RemoveAt(index);
      ConnectionModel connectionModel = this.Connections.Count > index ? this.Connections[index] : (ConnectionModel) null;
      if (connectionModel == null)
        return;
      connectionModel.IsSelected = true;
    }

    public void Browse(ConnectionModel model)
    {
      IExternalConnectionDefinition definition = (IExternalConnectionDefinition) model.Definition;
      string str = string.Format("{0} ({1})|{1}", (object) definition.DisplayName, (object) (definition.AssemblyName + ".dll"));
      OpenFileDialog openFileDialog1 = new OpenFileDialog();
      openFileDialog1.Filter = str + "|Assemblies (*.dll)|*.dll|All files (*.*)|*.*";
      openFileDialog1.Title = "Load Assembly";
      openFileDialog1.RestoreDirectory = true;
      OpenFileDialog openFileDialog2 = openFileDialog1;
      bool? nullable = openFileDialog2.ShowDialog();
      if ((nullable.GetValueOrDefault() ? 0 : (nullable.HasValue ? 1 : 0)) != 0)
        return;
      model.FullPathToAssembly = openFileDialog2.FileName;
      LoadAssemblyUtil.VerifyThatAssemblyCanBeLoaded(model.FullPathToAssembly, (System.Action<Exception>) (e => this.DisplayError("Could not load assembly", e)));
    }

    public void DisplayError(string error, Exception e)
    {
      this.Exception = new Exception(error, e);
    }

    public void Ok()
    {
      if (!this.ValidateConnection())
        return;
      UserPreferencesHolder.UserSettings.ConnectionInfo.Clear();
      this.connections.Where<ConnectionModel>((Func<ConnectionModel, bool>) (x =>
      {
        if (x != null)
          return x.Definition != null;
        return false;
      })).Apply<ConnectionModel>((System.Action<ConnectionModel>) (x => UserPreferencesHolder.UserSettings.ConnectionInfo.Add(EditConnectionModel.CreatePersistentModel(x))));
      foreach (ConnectionInfo connectionInfo in UserPreferencesHolder.UserSettings.ConnectionInfo)
        this.trackingService.Track("Connections", "Usage", connectionInfo.ConnectionTypeName, new int?());
      UserPreferencesHolder.Save();
      this.TryClose(new bool?(true));
    }

    private bool ValidateConnection()
    {
      ConnectionModel model = this.Connections.FirstOrDefault<ConnectionModel>((Func<ConnectionModel, bool>) (c => c.IsSelected));
      if (model == null)
      {
        if (!this.RequireConnection)
          return true;
        int num = (int) MessageBox.Show("Please add a connection", "Edit Connection Strings", MessageBoxButton.OK, MessageBoxImage.Hand);
        return false;
      }
      ConnectionInfo persistentModel = EditConnectionModel.CreatePersistentModel(model);
      ConnectionBuilder.Result result = ConnectionBuilder.BuildConnectionFromInfo(persistentModel);
      if (!result.WasSuccessful)
      {
        int num = (int) MessageBox.Show("Invalid connection string: " + result.Error, "Edit Connection Strings", MessageBoxButton.OK, MessageBoxImage.Hand);
        return false;
      }
      Assembly assembly = result.Connection.GetType().Assembly;
      ResolveEventHandler resolveEventHandler = (ResolveEventHandler) ((sender, args) =>
      {
        if (args.Name == assembly.FullName)
          return assembly;
        return (Assembly) null;
      });
      AppDomain.CurrentDomain.AssemblyResolve += resolveEventHandler;
      try
      {
        using (IDbConnection connection = result.Connection)
        {
          try
          {
            connection.ConnectionString = persistentModel.ConnectionString;
            if (connection.State != ConnectionState.Open)
            {
              using (new OverrideCursor(Cursors.Wait))
                connection.Open();
            }
            return true;
          }
          catch (Exception ex)
          {
            int num = (int) MessageBox.Show("Could not open connection to database: " + ex.Message, "Edit Connection Strings", MessageBoxButton.OK, MessageBoxImage.Hand);
            return false;
          }
        }
      }
      finally
      {
        AppDomain.CurrentDomain.AssemblyResolve -= resolveEventHandler;
      }
    }

    public void Cancel()
    {
      this.Cancelled = true;
      this.TryClose(new bool?(false));
    }

    private ConnectionModel CreateViewModel(ConnectionInfo info)
    {
      ConnectionModel connectionModel = new ConnectionModel();
      connectionModel.IsSelected = info.IsSelected;
      connectionModel.ConnectionString = info.ConnectionString;
      connectionModel.FullPathToAssembly = info.FullPathToAssembly;
      connectionModel.Definition = this.AvailableConnectionTypes.FirstOrDefault<IConnectionDefinition>((Func<IConnectionDefinition, bool>) (type => type.ClassName == info.ConnectionTypeName));
      return connectionModel;
    }

    private static ConnectionInfo CreatePersistentModel(ConnectionModel model)
    {
      return new ConnectionInfo()
      {
        ConnectionString = model.ConnectionString,
        ConnectionTypeName = model.Definition.ClassName,
        FullPathToAssembly = model.FullPathToAssembly,
        IsSelected = model.IsSelected
      };
    }
  }
}
