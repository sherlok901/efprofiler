﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Connections.RemoteConnectionRepository
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.Client.Settings;
using HibernatingRhinos.Profiler.Client.Shell;
using HibernatingRhinos.Profiler.Client.Tracking;
using System;
using System.Collections.Specialized;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace HibernatingRhinos.Profiler.Client.Connections
{
  public class RemoteConnectionRepository : PropertyChangedBase, IHandle<UserPreferences>, IHandle
  {
    private readonly ApplicationModel applicationModel;
    private readonly BackendBridge backendBridge;
    private readonly ITrackingService trackingService;
    private readonly IWindowManager windowManager;
    private readonly IEventAggregator events;
    private IObservableCollection<RemoteConnection> connections;
    private UserPreferences settings;

    public event EventHandler<AlertEventArgs> ConnectionAlert;

    public RemoteConnectionRepository(BackendBridge backendBridge, ApplicationModel applicationModel, ITrackingService trackingService, IWindowManager windowManager, IEventAggregator events)
    {
      this.backendBridge = backendBridge;
      this.applicationModel = applicationModel;
      this.trackingService = trackingService;
      this.windowManager = windowManager;
      this.events = events;
      this.events.Subscribe((object) this);
      this.LoadConnections();
    }

    public bool ConnectionsExist
    {
      get
      {
        return this.connections.Count > 0;
      }
    }

    public bool ConnectionsDoNotExist
    {
      get
      {
        return !this.ConnectionsExist;
      }
    }

    public IObservableCollection<RemoteConnection> Connections
    {
      get
      {
        return this.connections;
      }
    }

    public async Task<bool> TryMakeNewConnectionAsync(string host, string port, string password, CancellationTokenSource cts)
    {
      RemoteConnection connection = this.AddConnection(host, port, password);
      try
      {
        await connection.Connect(cts);
        return true;
      }
      catch (Exception ex)
      {
        this.RemoveConnection(connection);
        return false;
      }
    }

    public RemoteConnection AddConnection(string host, string port, string password)
    {
      RemoteConnection remoteConnection = new RemoteConnection(this, this.backendBridge, this.applicationModel, this.trackingService, this.windowManager, new PersistentRemoteConnection()
      {
        Host = host,
        Port = port,
        Password = password
      });
      this.connections.Insert(0, remoteConnection);
      this.PersistConnections();
      return remoteConnection;
    }

    public void RemoveConnection(RemoteConnection connection)
    {
      this.connections.Remove(connection);
      this.PersistConnections();
    }

    private void PersistConnections()
    {
      this.settings.RemoteConnections.Clear();
      this.connections.Apply<RemoteConnection>((System.Action<RemoteConnection>) (x => this.settings.RemoteConnections.Add(x.Details)));
      UserPreferencesHolder.Save();
    }

    private void LoadConnections()
    {
      this.connections = (IObservableCollection<RemoteConnection>) new BindableCollection<RemoteConnection>();
      this.settings = UserPreferencesHolder.UserSettings;
      this.settings.RemoteConnections.Apply<PersistentRemoteConnection>((System.Action<PersistentRemoteConnection>) (x => this.connections.Add(new RemoteConnection(this, this.backendBridge, this.applicationModel, this.trackingService, this.windowManager, x))));
      this.connections.CollectionChanged += (NotifyCollectionChangedEventHandler) ((param0, param1) =>
      {
        this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.ConnectionsExist));
        this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.ConnectionsDoNotExist));
      });
    }

    public void Handle(UserPreferences message)
    {
      this.settings = message;
      this.LoadConnections();
    }

    public void NotifyConnectionChanged(RemoteConnection remoteConnection)
    {
      this.PersistConnections();
    }

    public void RaiseConnectionAlert(Alert alert)
    {
      this.OnConnectionAlert(alert);
    }

    protected virtual void OnConnectionAlert(Alert alert)
    {
      EventHandler<AlertEventArgs> connectionAlert = this.ConnectionAlert;
      if (connectionAlert == null)
        return;
      connectionAlert((object) this, new AlertEventArgs()
      {
        Alert = alert
      });
    }
  }
}
