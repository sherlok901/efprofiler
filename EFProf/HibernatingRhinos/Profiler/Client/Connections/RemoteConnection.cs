﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Connections.RemoteConnection
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.Client.CaliburnInfrastructure;
using HibernatingRhinos.Profiler.Client.Controls;
using HibernatingRhinos.Profiler.Client.Shell;
using HibernatingRhinos.Profiler.Client.Tracking;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace HibernatingRhinos.Profiler.Client.Connections
{
  public class RemoteConnection : PropertyChangedBase
  {
    private readonly RemoteConnectionRepository repository;
    private readonly BackendBridge backendBridge;
    private readonly ApplicationModel applicationModel;
    private readonly PersistentRemoteConnection details;
    private readonly ITrackingService trackingService;
    private readonly IWindowManager windowManager;
    private ValidatableProperty<string> proposedPasswordProperty;
    private Guid disconnectId;
    private CancellationTokenSource cancellationTokenSource;
    private bool isConnecting;
    private bool isEditingPassword;

    public RemoteConnection(RemoteConnectionRepository repository, BackendBridge backendBridge, ApplicationModel applicationModel, ITrackingService trackingService, IWindowManager windowManager, PersistentRemoteConnection details)
    {
      this.repository = repository;
      this.backendBridge = backendBridge;
      this.applicationModel = applicationModel;
      this.trackingService = trackingService;
      this.windowManager = windowManager;
      this.details = details;
      ValidatableProperty<string> validatableProperty = new ValidatableProperty<string>("Password");
      validatableProperty.Validators.Add((ValidationAttribute) new RequiredAttribute());
      validatableProperty.Validators.Add((ValidationAttribute) new StringLengthAttribute(512)
      {
        MinimumLength = 8
      });
      validatableProperty.InitialValue = (object) "";
      this.proposedPasswordProperty = validatableProperty;
    }

    public string Name
    {
      get
      {
        return this.details.Host + ":" + this.details.Port;
      }
    }

    public bool IsConnecting
    {
      get
      {
        return this.isConnecting;
      }
      set
      {
        this.isConnecting = value;
        this.NotifyOfPropertyChange(nameof (IsConnecting));
      }
    }

    public bool IsConnected
    {
      get
      {
        return this.disconnectId != Guid.Empty;
      }
    }

    public bool IsDisconnected
    {
      get
      {
        return !this.IsConnected;
      }
    }

    public PersistentRemoteConnection Details
    {
      get
      {
        return this.details;
      }
    }

    public bool IsEditingPassword
    {
      get
      {
        return this.isEditingPassword;
      }
      set
      {
        this.isEditingPassword = value;
        this.NotifyOfPropertyChange(nameof (IsEditingPassword));
      }
    }

    public ValidatableProperty<string> ProposedPassword
    {
      get
      {
        return this.proposedPasswordProperty;
      }
    }

    public async Task Connect(CancellationTokenSource cancellationTokenSource)
    {
      if (this.IsConnected)
        return;
      this.cancellationTokenSource = cancellationTokenSource;
      try
      {
        this.IsConnecting = true;
        //this.disconnectId = await this.ConnectCoreAsync(cancellationTokenSource.Token, new System.Action<string, bool>(this.HandleConnectionStatus));
        this.trackingService.Track("Production", nameof (Connect), (string) null, new int?());
      }
      catch (OperationCanceledException ex)
      {
        throw;
      }
      catch (Exception ex)
      {
        this.repository.RaiseConnectionAlert(new Alert()
        {
          Message = ex.Message,
          AlertType = AlertType.Error,
          Bumpable = false
        });
        throw;
      }
      finally
      {
        this.IsConnecting = false;
        this.cancellationTokenSource = (CancellationTokenSource) null;
      }
    }

    //private Task<Guid> ConnectCoreAsync(CancellationToken token, System.Action<string, bool> handleConnectionStatus)
    //{
    //  return Task.Run<Guid>((Func<Task<Guid>>) (async () =>
    //  {
    //    IPAddress hostAddress = await this.GetIpAddress();
    //    IPEndPoint ipEndPoint = new IPEndPoint(hostAddress, int.Parse(this.details.Port));
    //    this.ConnectCoreInternal(ipEndPoint, handleConnectionStatus);
    //    return this.disconnectId;
    //  }), token).WithCancellation<Guid>(token);
    //}

    private void ConnectCoreInternal(IPEndPoint ipEndPoint, System.Action<string, bool> handleConnectionStatus)
    {
      System.Action reconnectDialog = (System.Action) (() => ((System.Action) (() =>
      {
        this.DisconnectCore();
        ReconnectToProductionModel toProductionModel = IoC.Get<ReconnectToProductionModel>((string) null);
        toProductionModel.ReconnectAction = (System.Action) (() => this.ConnectCoreInternal(ipEndPoint, handleConnectionStatus));
        Dialog.Show((object) toProductionModel, (object) null, false);
      })).OnUIThread());
      try
      {
        this.disconnectId = this.backendBridge.Connect(ipEndPoint, this.details.Password, handleConnectionStatus, reconnectDialog);
      }
      finally
      {
        this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.IsConnected));
        this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.IsDisconnected));
      }
    }

    private void HandleConnectionStatus(string message, bool isError)
    {
      if (message == "")
        message = "You have successfully connected to " + this.Name + ".";
      this.repository.RaiseConnectionAlert(new Alert()
      {
        AlertType = isError ? AlertType.Error : AlertType.Information,
        Message = message,
        Bumpable = !isError
      });
    }

    private async Task<IPAddress> GetIpAddress()
    {
      IPAddress address;
      if (IPAddress.TryParse(this.details.Host, out address))
        return address;
      IPHostEntry hostEntry = await Dns.GetHostEntryAsync(this.details.Host);
      IPAddress hostAddress = ((IEnumerable<IPAddress>) hostEntry.AddressList).First<IPAddress>((Func<IPAddress, bool>) (ip => ip.AddressFamily == AddressFamily.InterNetwork));
      return hostAddress;
    }

    public void ChangePassword()
    {
      this.ProposedPassword.InitialValue = (object) this.details.Password;
      this.IsEditingPassword = true;
    }

    public void CancelChangePassword()
    {
      this.IsEditingPassword = false;
    }

    public void CommitChangePassword()
    {
      if (!this.ProposedPassword.IsValid)
        return;
      this.IsEditingPassword = false;
      this.details.Password = this.ProposedPassword.ValidatedValue;
      if (this.IsConnected)
      {
        this.Disconnect();
        this.Connect();
      }
      this.repository.NotifyConnectionChanged(this);
    }

    public void CancelConnection()
    {
      if (this.cancellationTokenSource == null)
        return;
      this.cancellationTokenSource.Cancel();
    }

    public void Connect()
    {
      this.Connect(new CancellationTokenSource());
    }

    public void Disconnect()
    {
      if (!this.IsConnected)
        return;
      this.DisconnectCore();
      this.repository.RaiseConnectionAlert(new Alert()
      {
        Message = "You have successfully disconnected from " + this.Name + ".",
        AlertType = AlertType.Information,
        Bumpable = true
      });
    }

    private void DisconnectCore()
    {
      this.backendBridge.DisposeOf(this.disconnectId);
      this.disconnectId = Guid.Empty;
      this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.IsConnected));
      this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.IsDisconnected));
    }
  }
}
