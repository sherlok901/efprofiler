﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Connections.Alerter
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using System;
using System.Windows.Threading;

namespace HibernatingRhinos.Profiler.Client.Connections
{
  public class Alerter : PropertyChangedBase
  {
    private DispatcherTimer timer;
    private Alert currentAlert;

    public Alerter()
    {
      this.timer = new DispatcherTimer();
      this.timer.Tick += new EventHandler(this.HandleTimerTick);
      this.timer.Interval = TimeSpan.FromSeconds(10.0);
    }

    private void HandleTimerTick(object sender, EventArgs e)
    {
      this.CurrentAlert = (Alert) null;
    }

    public Alert CurrentAlert
    {
      get
      {
        return this.currentAlert;
      }
      private set
      {
        if (object.Equals((object) value, (object) this.currentAlert))
          return;
        this.currentAlert = value;
        this.NotifyOfPropertyChange(nameof (CurrentAlert));
      }
    }

    public void ClearAlert()
    {
      this.CurrentAlert = (Alert) null;
      this.timer.Stop();
    }

    public void RaiseAlert(Alert alert)
    {
      this.CurrentAlert = alert;
      this.timer.Stop();
      this.timer.Start();
    }
  }
}
