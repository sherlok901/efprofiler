﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Connections.Alert
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

namespace HibernatingRhinos.Profiler.Client.Connections
{
  public class Alert
  {
    public string Message { get; set; }

    public AlertType AlertType { get; set; }

    public bool Bumpable { get; set; }
  }
}
