﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Connections.ReconnectToProductionModel
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.Client.Infrastructure;
using System;
using System.Linq.Expressions;

namespace HibernatingRhinos.Profiler.Client.Connections
{
  public class ReconnectToProductionModel : ProfilerScreen
  {
    private Exception exception;

    public ReconnectToProductionModel()
    {
      this.DisplayName = "Reconnect";
    }

    public Exception Exception
    {
      get
      {
        return this.exception;
      }
      set
      {
        this.exception = value;
        this.NotifyOfPropertyChange<Exception>((Expression<Func<Exception>>) (() => this.Exception));
      }
    }

    public Action ReconnectAction { get; set; }

    public void Reconnect()
    {
      this.Exception = (Exception) null;
      try
      {
        this.ReconnectAction();
        this.TryClose(new bool?(true));
      }
      catch (Exception ex)
      {
        this.Exception = ex;
      }
    }
  }
}
