﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Connections.ConnectToProductionModel
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.Client.CaliburnInfrastructure;
using HibernatingRhinos.Profiler.Client.Infrastructure;
using HibernatingRhinos.Profiler.Client.Infrastructure.Validation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;
using System.Threading;

namespace HibernatingRhinos.Profiler.Client.Connections
{
  public class ConnectToProductionModel : ProfilerScreen
  {
    private Alerter alerter = new Alerter();
    private const int DefaultPort = 22897;
    private ValidatableProperty<string> hostProperty;
    private ValidatableProperty<int> portProperty;
    private ValidatableProperty<string> passwordProperty;
    private readonly RemoteConnectionRepository repository;
    private ValidatablePropertyCollection properties;
    private CancellationTokenSource cancellationTokenSource;
    private bool isConnecting;

    public ConnectToProductionModel(RemoteConnectionRepository repository)
    {
      this.repository = repository;
      repository.ConnectionAlert += (EventHandler<AlertEventArgs>) ((sender, args) => this.alerter.RaiseAlert(args.Alert));
      this.DisplayName = "Remote Applications";
      ValidatableProperty<string> validatableProperty1 = new ValidatableProperty<string>(nameof (Host));
      validatableProperty1.Validators.Add((ValidationAttribute) new RequiredAttribute());
      IList<ValidationAttribute> validators = validatableProperty1.Validators;
      HostValidator hostValidator1 = new HostValidator();
      hostValidator1.ErrorMessage = "Host must be a valid host name or IP address";
      HostValidator hostValidator2 = hostValidator1;
      validators.Add((ValidationAttribute) hostValidator2);
      validatableProperty1.InitialValue = (object) "";
      this.hostProperty = validatableProperty1;
      ValidatableProperty<int> validatableProperty2 = new ValidatableProperty<int>(nameof (Port));
      validatableProperty2.Validators.Add((ValidationAttribute) new RangeAttribute(1, (int) ushort.MaxValue));
      validatableProperty2.InitialValue = (object) "";
      this.portProperty = validatableProperty2;
      ValidatableProperty<string> validatableProperty3 = new ValidatableProperty<string>(nameof (Password));
      validatableProperty3.Validators.Add((ValidationAttribute) new RequiredAttribute());
      validatableProperty3.Validators.Add((ValidationAttribute) new StringLengthAttribute(512)
      {
        MinimumLength = 8
      });
      validatableProperty3.InitialValue = (object) "";
      this.passwordProperty = validatableProperty3;
      ValidatablePropertyCollection propertyCollection = new ValidatablePropertyCollection();
      propertyCollection.Add((IValidatableProperty) this.hostProperty);
      propertyCollection.Add((IValidatableProperty) this.portProperty);
      propertyCollection.Add((IValidatableProperty) this.passwordProperty);
      this.properties = propertyCollection;
      this.properties.ErrorsChanged += (EventHandler<DataErrorsChangedEventArgs>) ((param0, param1) => this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.CanAddConnection)));
    }

    public ValidatableProperty<string> Host
    {
      get
      {
        return this.hostProperty;
      }
    }

    public ValidatableProperty<int> Port
    {
      get
      {
        return this.portProperty;
      }
    }

    public ValidatableProperty<string> Password
    {
      get
      {
        return this.passwordProperty;
      }
    }

    public RemoteConnectionRepository Repository
    {
      get
      {
        return this.repository;
      }
    }

    public bool CanAddConnection
    {
      get
      {
        return this.properties.IsValid;
      }
    }

    public bool IsConnecting
    {
      get
      {
        return this.isConnecting;
      }
      private set
      {
        this.isConnecting = value;
        this.NotifyOfPropertyChange(nameof (IsConnecting));
      }
    }

    public Alerter Alerter
    {
      get
      {
        return this.alerter;
      }
    }

    public async void AddConnection()
    {
      if (this.IsConnecting)
        return;
      using (CancellationTokenSource cts = new CancellationTokenSource())
      {
        try
        {
          this.IsConnecting = true;
          this.cancellationTokenSource = cts;
          bool succeeded = await this.repository.TryMakeNewConnectionAsync(this.Host.ValidatedValue, this.Port.ValidatedValue.ToString(), this.Password.ValidatedValue, cts);
          if (!succeeded)
            return;
          this.Host.InitialValue = this.Password.InitialValue = this.Port.InitialValue = (object) string.Empty;
        }
        finally
        {
          this.IsConnecting = false;
          this.cancellationTokenSource = (CancellationTokenSource) null;
        }
      }
    }

    public void CancelConnection()
    {
      if (this.cancellationTokenSource == null)
        return;
      this.cancellationTokenSource.Cancel();
    }

    public void RemoveConnection(RemoteConnection connection)
    {
      if (connection.IsConnected)
        connection.Disconnect();
      this.repository.RemoveConnection(connection);
    }
  }
}
