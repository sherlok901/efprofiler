﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Connections.ConnectionModel
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.BackEnd.Infrastructure.Connections;
using HibernatingRhinos.Profiler.Client.Infrastructure;
using System;
using System.Linq.Expressions;

namespace HibernatingRhinos.Profiler.Client.Connections
{
  public class ConnectionModel : SelectionBase
  {
    private string connectionString;
    private IConnectionDefinition definition;
    private string fullPathToAssembly;

    public string ConnectionString
    {
      get
      {
        return this.connectionString;
      }
      set
      {
        this.connectionString = value;
        this.NotifyOfPropertyChange<string>((Expression<Func<string>>) (() => this.ConnectionString));
      }
    }

    public IConnectionDefinition Definition
    {
      get
      {
        return this.definition;
      }
      set
      {
        this.definition = value;
        this.NotifyOfPropertyChange<IConnectionDefinition>((Expression<Func<IConnectionDefinition>>) (() => this.Definition));
        if (this.definition == null)
          return;
        this.DisplayName = this.definition.DisplayName;
      }
    }

    public string FullPathToAssembly
    {
      get
      {
        return this.fullPathToAssembly;
      }
      set
      {
        this.fullPathToAssembly = value;
        this.NotifyOfPropertyChange<string>((Expression<Func<string>>) (() => this.FullPathToAssembly));
      }
    }

    public bool IsExternalConnection
    {
      get
      {
        return this.Definition is IExternalConnectionDefinition;
      }
    }
  }
}
