﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.External.SilverlightToolkit.Controls.HyperlinkButton
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.Client.Infrastructure;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls.Primitives;

namespace HibernatingRhinos.Profiler.Client.External.SilverlightToolkit.Controls
{
  [TemplateVisualState(GroupName = "CommonStates", Name = "Pressed")]
  [TemplateVisualState(GroupName = "CommonStates", Name = "Normal")]
  [TemplateVisualState(GroupName = "FocusStates", Name = "Unfocused")]
  [TemplateVisualState(GroupName = "CommonStates", Name = "Disabled")]
  [TemplateVisualState(GroupName = "FocusStates", Name = "Focused")]
  [TemplateVisualState(GroupName = "CommonStates", Name = "MouseOver")]
  public class HyperlinkButton : ButtonBase
  {
    public static readonly DependencyProperty NavigateUriProperty = DependencyProperty.Register(nameof (NavigateUri), typeof (Uri), typeof (HyperlinkButton), (PropertyMetadata) null);
    public static readonly DependencyProperty TargetNameProperty = DependencyProperty.Register(nameof (TargetName), typeof (string), typeof (HyperlinkButton), (PropertyMetadata) null);

    [TypeConverter(typeof (UriTypeConverter))]
    public Uri NavigateUri
    {
      get
      {
        return this.GetValue(HyperlinkButton.NavigateUriProperty) as Uri;
      }
      set
      {
        this.SetValue(HyperlinkButton.NavigateUriProperty, (object) value);
      }
    }

    public string TargetName
    {
      get
      {
        return this.GetValue(HyperlinkButton.TargetNameProperty) as string;
      }
      set
      {
        this.SetValue(HyperlinkButton.TargetNameProperty, (object) value);
      }
    }

    public HyperlinkButton()
    {
      this.DefaultStyleKey = (object) typeof (HyperlinkButton);
    }

    internal new void ChangeVisualState(bool useTransitions)
    {
      if (!this.IsEnabled)
        VisualStateManager.GoToState((FrameworkElement) this, "Disabled", useTransitions);
      else if (this.IsPressed)
        VisualStateManager.GoToState((FrameworkElement) this, "Pressed", useTransitions);
      else if (this.IsMouseOver)
        VisualStateManager.GoToState((FrameworkElement) this, "MouseOver", useTransitions);
      else
        VisualStateManager.GoToState((FrameworkElement) this, "Normal", useTransitions);
      if (this.IsFocused && this.IsEnabled)
        VisualStateManager.GoToState((FrameworkElement) this, "Focused", useTransitions);
      else
        VisualStateManager.GoToState((FrameworkElement) this, "Unfocused", useTransitions);
    }

    protected override void OnClick()
    {
      base.OnClick();
      if (!(this.NavigateUri != (Uri) null) || DesignerProperties.GetIsInDesignMode((DependencyObject) this))
        return;
      BrowserLauncher.Url(this.NavigateUri.AbsoluteUri, "Failed to open a URL.");
    }
  }
}
