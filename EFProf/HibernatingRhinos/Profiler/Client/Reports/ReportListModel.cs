﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Reports.ReportListModel
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using HibernatingRhinos.Profiler.Client.Filtering;
using HibernatingRhinos.Profiler.Client.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace HibernatingRhinos.Profiler.Client.Reports
{
  public class ReportListModel : Conductor<IReport>.Collection.OneActive, ISupportFiltering, INavigateItems
  {
    private System.Action whenFilteringSupportedChanged;

    public ReportListModel(IReport[] reports)
    {
      this.DisplayName = "Analysis";
      this.Items.Add(((IEnumerable<IReport>) reports).FirstOrDefault<IReport>((Func<IReport, bool>) (x => x is UniqueQueriesModel)));
      this.Items.Add(((IEnumerable<IReport>) reports).FirstOrDefault<IReport>((Func<IReport, bool>) (x => x is ExpensiveQueriesModel)));
      this.Items.Add(((IEnumerable<IReport>) reports).FirstOrDefault<IReport>((Func<IReport, bool>) (x => x is QueriesByIsolationLevelModel)));
      this.Items.Add(((IEnumerable<IReport>) reports).FirstOrDefault<IReport>((Func<IReport, bool>) (x => x is QueriesByMethodModel)));
      this.Items.Add(((IEnumerable<IReport>) reports).FirstOrDefault<IReport>((Func<IReport, bool>) (x => x is QueriesByTableModel)));
      this.Items.Add(((IEnumerable<IReport>) reports).FirstOrDefault<IReport>((Func<IReport, bool>) (x => x is QueriesByAlertModel)));
      this.Items.Add(((IEnumerable<IReport>) reports).FirstOrDefault<IReport>((Func<IReport, bool>) (x => x is QueriesByUrlModel)));
      this.Items.Add(((IEnumerable<IReport>) reports).FirstOrDefault<IReport>((Func<IReport, bool>) (x => x is OverallUsageModel)));
    }

    public void NextItem()
    {
      this.ActiveItem = NavigationManager.GetNext<IReport>((IList<IReport>) this.Items, this.ActiveItem);
    }

    public void PreviousItem()
    {
      this.ActiveItem = NavigationManager.GetPrevious<IReport>((IList<IReport>) this.Items, this.ActiveItem);
    }

    public bool FilteringSupported
    {
      get
      {
        if (this.ActiveItem != null)
          return this.ActiveItem.FilteringSupported;
        return false;
      }
    }

    public void CallWhenFilteringSupportedChanged(System.Action action)
    {
      this.whenFilteringSupportedChanged = action;
    }

    public void Clear()
    {
      foreach (IReport report in (IEnumerable<IReport>) this.Items)
      {
        report.Clear();
        report.TimerTicked((object) this, EventArgs.Empty);
      }
    }

    protected override void ChangeActiveItem(IReport newItem, bool closePrevious)
    {
      base.ChangeActiveItem(newItem, closePrevious);
      if (this.whenFilteringSupportedChanged != null)
        this.whenFilteringSupportedChanged();
      this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.FilteringSupported));
    }

    protected override void OnInitialize()
    {
      this.ActivateItem(this.Items.First<IReport>());
      base.OnInitialize();
    }

    public void SetActiveItem(IReport newItem)
    {
      this.ChangeActiveItem(newItem, false);
    }
  }
}
