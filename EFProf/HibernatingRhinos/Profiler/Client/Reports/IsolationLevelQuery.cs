﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Reports.IsolationLevelQuery
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.BackEnd.Reports;
using HibernatingRhinos.Profiler.Client.Filtering;
using HibernatingRhinos.Profiler.Client.Infrastructure;
using HibernatingRhinos.Profiler.Client.Tracking;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Data;
using System.Windows.Input;

namespace HibernatingRhinos.Profiler.Client.Reports
{
  public class IsolationLevelQuery : SelectionBase, INavigateItems
  {
    private readonly FilterServiceModel filterService;
    private readonly ObservableCollection<QueryAggregation> unfilteredStatements;
    private EventHandler filterServiceOnFiltersChanged;

    public IsolationLevelQuery(FilterServiceModel filterService, ITrackingService trackingService)
    {
      this.filterService = filterService;
      this.filterServiceOnFiltersChanged = (EventHandler) ((sender, args) => this.ApplyFilters());
      filterService.FiltersChanged += this.filterServiceOnFiltersChanged;
      this.unfilteredStatements = new ObservableCollection<QueryAggregation>();
      this.StatementsCollection = new CollectionViewSource()
      {
        Source = (object) this.unfilteredStatements
      };
      this.ApplyFilters();
    }

    public string IsolationLevel { get; set; }

    public string SessionId { get; set; }

    public CollectionViewSource StatementsCollection { get; set; }

    public ObservableCollection<QueryAggregation> UnfilteredStatements
    {
      get
      {
        return this.unfilteredStatements;
      }
    }

    public void NextItem()
    {
      if (this.StatementsCollection.View.MoveCurrentToNext())
        return;
      this.StatementsCollection.View.MoveCurrentToLast();
    }

    public void PreviousItem()
    {
      if (this.StatementsCollection.View.MoveCurrentToPrevious())
        return;
      this.StatementsCollection.View.MoveCurrentToFirst();
    }

    private void ApplyFilters()
    {
      this.StatementsCollection.View.Filter = (Predicate<object>) null;
      if (this.filterService.Filters.Count == 0)
        return;
      this.StatementsCollection.View.Filter = new Predicate<object>(this.FilterStatements);
    }

    private bool FilterStatements(object statement)
    {
      return this.filterService.FilterStatement((IFilterableStatementSnapshot) statement);
    }

    public void UpdateFrom(IsolationLevelQueriesSnapshot snapshot)
    {
      foreach (QueryAggregationSnapshot statement1 in snapshot.Statements)
      {
        QueryAggregationSnapshot statement = statement1;
        QueryAggregation queryAggregation = this.unfilteredStatements.Where<QueryAggregation>((Func<QueryAggregation, bool>) (item => item.RawSql == statement.RawSql)).FirstOrDefault<QueryAggregation>();
        if (queryAggregation == null)
          this.unfilteredStatements.Add(QueryAggregation.CreateFrom(statement));
        else
          queryAggregation.UpdateFrom(statement);
      }
    }

    public static IsolationLevelQuery CreateFrom(IsolationLevelQueriesSnapshot snapshot, FilterServiceModel filterService, ITrackingService trackingService)
    {
      IsolationLevelQuery isolationLevelQuery = new IsolationLevelQuery(filterService, trackingService)
      {
        IsolationLevel = snapshot.IsolationLevel,
        SessionId = snapshot.SessionId
      };
      foreach (QueryAggregationSnapshot statement in snapshot.Statements)
      {
        QueryAggregation from = QueryAggregation.CreateFrom(statement);
        isolationLevelQuery.unfilteredStatements.Add(from);
      }
      return isolationLevelQuery;
    }

    public void Clear()
    {
      this.unfilteredStatements.Clear();
    }

    public void CancelSorting(MouseButtonEventArgs e)
    {
      this.StatementsCollection.View.SortDescriptions.Clear();
      e.Handled = true;
    }
  }
}
