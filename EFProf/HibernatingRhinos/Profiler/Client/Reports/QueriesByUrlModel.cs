﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Reports.QueriesByUrlModel
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.Reports;
using HibernatingRhinos.Profiler.Client.Filtering;
using HibernatingRhinos.Profiler.Client.Infrastructure;
using HibernatingRhinos.Profiler.Client.Tracking;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;
using System.Windows.Data;
using System.Xml.Linq;

namespace HibernatingRhinos.Profiler.Client.Reports
{
  [Serializable]
  public class QueriesByUrlModel : ReportBase<IEnumerable<UrlQueriesSnapshot>>, INavigateItems
  {
    private readonly FilterServiceModel filterService;

    public QueriesByUrlModel(BackendBridge backendBridge, FilterServiceModel filterService, ITrackingService trackingService)
      : base(trackingService, new Func<IEnumerable<UrlQueriesSnapshot>>(backendBridge.Reports.QueriesByUrl.GetReportSnapshot))
    {
      this.filterService = filterService;
      this.DisplayName = "Queries By Url";
      this.StatementsByUrl = new BindableCollection<UrlQuery>();
      this.StatementsByUrlCollection = new CollectionViewSource()
      {
        Source = (object) this.StatementsByUrl
      };
    }

    public override bool FilteringSupported
    {
      get
      {
        return true;
      }
    }

    public void NextItem()
    {
      if (this.StatementsByUrlCollection.View.MoveCurrentToNext())
        return;
      this.StatementsByUrlCollection.View.MoveCurrentToLast();
    }

    public void PreviousItem()
    {
      if (this.StatementsByUrlCollection.View.MoveCurrentToPrevious())
        return;
      this.StatementsByUrlCollection.View.MoveCurrentToFirst();
    }

    public BindableCollection<UrlQuery> StatementsByUrl { get; private set; }

    public CollectionViewSource StatementsByUrlCollection { get; set; }

    protected override void Update(IEnumerable<UrlQueriesSnapshot> snapshots)
    {
      foreach (UrlQueriesSnapshot snapshot1 in snapshots)
      {
        UrlQueriesSnapshot snapshot = snapshot1;
        UrlQuery urlQuery = this.StatementsByUrl.Where<UrlQuery>((Func<UrlQuery, bool>) (shot => shot.Url == snapshot.Url)).FirstOrDefault<UrlQuery>();
        if (urlQuery == null)
        {
          this.StatementsByUrl.Add(UrlQuery.CreateFrom(snapshot, this.filterService, this.trackingService));
          if (this.StatementsByUrl.Count == 1)
            this.StatementsByUrlCollection.View.MoveCurrentToFirst();
        }
        else
          urlQuery.UpdateFrom(snapshot);
      }
      if (this.NotifyOnUpdate == null)
        return;
      this.NotifyOnUpdate();
    }

    public override void Clear()
    {
      foreach (UrlQuery urlQuery in (Collection<UrlQuery>) this.StatementsByUrl)
        urlQuery.Clear();
      this.StatementsByUrl.Clear();
      this.NotifyOfPropertyChange("");
      this.NotifyOfPropertyChange<BindableCollection<UrlQuery>>((Expression<Func<BindableCollection<UrlQuery>>>) (() => this.StatementsByUrl));
    }

    public override string JsonReportName
    {
      get
      {
        return "queriesByUrl";
      }
    }

    public override XElement ExportReport()
    {
      return new XElement(ReportBase.Namespace + "queries-by-url", new object[2]
      {
        (object) new XAttribute((XName) "url-count", (object) this.StatementsByUrl.Count),
        (object) this.StatementsByUrl.Select<UrlQuery, XElement>((Func<UrlQuery, XElement>) (x => new XElement(ReportBase.Namespace + "url", new object[3]
        {
          (object) new XAttribute((XName) "url", (object) x.Url),
          (object) new XAttribute((XName) "statement-count", (object) x.UnfilteredStatements.Count),
          (object) x.UnfilteredStatements.Select<QueryAggregation, XElement>((Func<QueryAggregation, XElement>) (q => new XElement(ReportBase.Namespace + "query", new object[7]
          {
            (object) q.Duration.ToXml(),
            (object) q.AverageDuration.ToXml("average-duration"),
            (object) new XElement(ReportBase.Namespace + "count", (object) q.Count),
            (object) new XAttribute((XName) "is-cached", (object) q.IsCached),
            (object) new XAttribute((XName) "is-ddl", (object) q.IsDDL),
            (object) new XElement(ReportBase.Namespace + "short-sql", (object) q.ShortSql),
            (object) new XElement(ReportBase.Namespace + "formatted-sql", (object) q.FormattedSql)
          })))
        })))
      });
    }

    [CLSCompliant(false)]
    protected override JContainer ExportReportToJson()
    {
      return (JContainer) new JArray((object) this.StatementsByUrl.Select<UrlQuery, JObject>((Func<UrlQuery, JObject>) (x => new JObject(new object[3]
      {
        (object) new JProperty("url", (object) x.Url),
        (object) new JProperty("statementCount", (object) x.UnfilteredStatements.Count),
        (object) new JProperty("queries", (object) new JArray((object) x.UnfilteredStatements.Select<QueryAggregation, JObject>((Func<QueryAggregation, JObject>) (q => new JObject(new object[6]
        {
          (object) new JProperty("averageDuration", (object) q.AverageDuration),
          (object) new JProperty("count", (object) q.Count),
          (object) new JProperty("isCached", (object) q.IsCached),
          (object) new JProperty("isDdl", (object) q.IsDDL),
          (object) new JProperty("shortSql", (object) q.ShortSql),
          (object) new JProperty("formattedSql", (object) q.FormattedSql)
        })))))
      }))));
    }
  }
}
