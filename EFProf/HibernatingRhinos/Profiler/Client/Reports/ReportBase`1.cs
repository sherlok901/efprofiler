﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Reports.ReportBase`1
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.Client.Infrastructure;
using HibernatingRhinos.Profiler.Client.Tracking;
using System;

namespace HibernatingRhinos.Profiler.Client.Reports
{
  public abstract class ReportBase<T> : ReportBase
  {
    private readonly Func<T> getSnapshot;

    protected ReportBase(ITrackingService trackingService, Func<T> getSnapshot)
      : base(trackingService)
    {
      this.getSnapshot = getSnapshot;
    }

    public override void TimerTicked(object sender, EventArgs e)
    {
      if (this.State == PollingState.Finalized)
      {
        PollingService.UnRegister(new EventHandler(((PollingScreen) this).TimerTicked));
      }
      else
      {
        this.Update(this.getSnapshot());
        if (this.State != PollingState.NotFinalized)
          return;
        this.State = PollingState.Finalized;
      }
    }

    protected abstract void Update(T snapshot);

    public override void ForceUpdate()
    {
      this.Update(this.getSnapshot());
      if (this.State != PollingState.NotFinalized)
        return;
      this.State = PollingState.Finalized;
    }
  }
}
