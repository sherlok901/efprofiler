﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Reports.QueriesByIsolationLevelView
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace HibernatingRhinos.Profiler.Client.Reports
{
  public partial class QueriesByIsolationLevelView : UserControl, IComponentConnector
  {
    //internal DataGrid methodGrid;
    //internal DataGrid queriesGrid;
    //private bool _contentLoaded;

    public QueriesByIsolationLevelView()
    {
      this.InitializeComponent();
    }

    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //public void InitializeComponent()
    //{
    //  if (this._contentLoaded)
    //    return;
    //  this._contentLoaded = true;
    //  Application.LoadComponent((object) this, new Uri("/EFProf;component/reports/queriesbyisolationlevelview.xaml", UriKind.Relative));
    //}

    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //internal Delegate _CreateDelegate(Type delegateType, string handler)
    //{
    //  return Delegate.CreateDelegate(delegateType, (object) this, handler);
    //}

    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[EditorBrowsable(EditorBrowsableState.Never)]
    //void IComponentConnector.Connect(int connectionId, object target)
    //{
    //  switch (connectionId)
    //  {
    //    case 1:
    //      this.methodGrid = (DataGrid) target;
    //      break;
    //    case 2:
    //      this.queriesGrid = (DataGrid) target;
    //      break;
    //    default:
    //      this._contentLoaded = true;
    //      break;
    //  }
    //}
  }
}
