﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Reports.AlertQuery
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.BackEnd.Reports;
using HibernatingRhinos.Profiler.Client.Filtering;
using HibernatingRhinos.Profiler.Client.Infrastructure;
using HibernatingRhinos.Profiler.Client.Tracking;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Data;
using System.Windows.Input;

namespace HibernatingRhinos.Profiler.Client.Reports
{
  public class AlertQuery : SelectionBase, INavigateItems
  {
    private readonly ObservableCollection<QueryAggregation> unfilteredStatements = new ObservableCollection<QueryAggregation>();
    private readonly FilterServiceModel filterService;
    private readonly ITrackingService trackingService;
    private EventHandler filterServiceOnFiltersChanged;

    public AlertQuery(FilterServiceModel filterService, ITrackingService trackingService)
    {
      this.filterService = filterService;
      this.trackingService = trackingService;
      this.filterServiceOnFiltersChanged = (EventHandler) ((sender, args) => this.ApplyFilters());
      this.filterService.FiltersChanged += this.filterServiceOnFiltersChanged;
      this.StatementsCollection = new CollectionViewSource()
      {
        Source = (object) this.unfilteredStatements
      };
      this.ApplyFilters();
    }

    public string Title { get; set; }

    public Severity Severity { get; set; }

    public string AlertTitle
    {
      get
      {
        return this.Title.Replace("[[", "").Replace("]]", "");
      }
    }

    public CollectionViewSource StatementsCollection { get; set; }

    public ObservableCollection<QueryAggregation> UnfilteredStatements
    {
      get
      {
        return this.unfilteredStatements;
      }
    }

    private void ApplyFilters()
    {
      this.StatementsCollection.View.Filter = (Predicate<object>) null;
      if (this.filterService.Filters.Count == 0)
        return;
      this.StatementsCollection.View.Filter = new Predicate<object>(this.FilterStatements);
    }

    private bool FilterStatements(object statement)
    {
      return this.filterService.FilterStatement((IFilterableStatementSnapshot) statement);
    }

    public void NextItem()
    {
      if (this.StatementsCollection.View.MoveCurrentToNext())
        return;
      this.StatementsCollection.View.MoveCurrentToLast();
    }

    public void PreviousItem()
    {
      if (this.StatementsCollection.View.MoveCurrentToPrevious())
        return;
      this.StatementsCollection.View.MoveCurrentToFirst();
    }

    public static AlertQuery CreateFrom(AlertQueriesSnapshot snapshot, FilterServiceModel filterService, ITrackingService trackingService)
    {
      AlertQuery alertQuery = new AlertQuery(filterService, trackingService)
      {
        Title = snapshot.Alert.Title,
        Severity = snapshot.Alert.Severity
      };
      foreach (QueryAggregationSnapshot statement in snapshot.Statements)
      {
        QueryAggregation from = QueryAggregation.CreateFrom(statement);
        alertQuery.unfilteredStatements.Add(from);
      }
      return alertQuery;
    }

    public void UpdateFrom(AlertQueriesSnapshot snapshot)
    {
      foreach (QueryAggregationSnapshot statement1 in snapshot.Statements)
      {
        QueryAggregationSnapshot statement = statement1;
        QueryAggregation queryAggregation = this.unfilteredStatements.Where<QueryAggregation>((Func<QueryAggregation, bool>) (item => item.RawSql == statement.RawSql)).FirstOrDefault<QueryAggregation>();
        if (queryAggregation == null)
        {
          this.unfilteredStatements.Add(QueryAggregation.CreateFrom(statement));
          if (this.unfilteredStatements.Count == 1)
            this.StatementsCollection.View.MoveCurrentToFirst();
        }
        else
          queryAggregation.UpdateFrom(statement);
      }
    }

    public void Clear()
    {
      this.unfilteredStatements.Clear();
    }

    public void CancelSorting(MouseButtonEventArgs e)
    {
      this.StatementsCollection.View.SortDescriptions.Clear();
      e.Handled = true;
    }
  }
}
