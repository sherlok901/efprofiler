﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Reports.QueryAggregation
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.BackEnd.Messages;
using HibernatingRhinos.Profiler.BackEnd.Reports;
using HibernatingRhinos.Profiler.Client.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace HibernatingRhinos.Profiler.Client.Reports
{
  public class QueryAggregation : SelectionBase, IFilterableStatementSnapshot
  {
    private IEnumerable<RelatedSessionSpecification> relatedSessions = (IEnumerable<RelatedSessionSpecification>) new List<RelatedSessionSpecification>();
    private int count;
    private double? averageDuration;

    public int Count
    {
      get
      {
        return this.count;
      }
      set
      {
        this.count = value;
        this.NotifyOfPropertyChange<int>((Expression<Func<int>>) (() => this.Count));
      }
    }

    public double? AverageDuration
    {
      get
      {
        return this.averageDuration;
      }
      set
      {
        this.averageDuration = value;
        this.NotifyOfPropertyChange<double?>((Expression<Func<double?>>) (() => this.AverageDuration));
      }
    }

    public string RawSql { get; set; }

    public QueryDuration Duration
    {
      get
      {
        QueryDuration queryDuration1 = new QueryDuration();
        QueryDuration queryDuration2 = queryDuration1;
        double? averageDuration1 = this.averageDuration;
        int? nullable1 = averageDuration1.HasValue ? new int?((int) averageDuration1.GetValueOrDefault()) : new int?();
        queryDuration2.InDatabase = nullable1;
        QueryDuration queryDuration3 = queryDuration1;
        double? averageDuration2 = this.averageDuration;
        int? nullable2 = averageDuration2.HasValue ? new int?((int) averageDuration2.GetValueOrDefault()) : new int?();
        queryDuration3.InNHibernate = nullable2;
        return queryDuration1;
      }
    }

    public bool IsCached { get; set; }

    public bool IsDDL { get; set; }

    public bool IsTransaction { get; set; }

    public int SessionStatementCount
    {
      get
      {
        return -1;
      }
    }

    public string ShortSql { get; set; }

    public string FormattedSql { get; set; }

    public IEnumerable<RelatedSessionSpecification> RelatedSessions
    {
      get
      {
        return this.relatedSessions;
      }
      set
      {
        if (this.relatedSessions.Count<RelatedSessionSpecification>() == value.Count<RelatedSessionSpecification>())
          return;
        this.relatedSessions = value;
        this.NotifyOfPropertyChange<IEnumerable<RelatedSessionSpecification>>((Expression<Func<IEnumerable<RelatedSessionSpecification>>>) (() => this.RelatedSessions));
      }
    }

    public void UpdateFrom(QueryAggregationSnapshot statement)
    {
      this.Count = statement.Count;
      this.AverageDuration = statement.AverageDuration;
      this.RelatedSessions = (IEnumerable<RelatedSessionSpecification>) statement.RelatedSessions;
    }

    public static QueryAggregation CreateFrom(QueryAggregationSnapshot statement)
    {
      return new QueryAggregation()
      {
        AverageDuration = statement.AverageDuration,
        Count = statement.Count,
        FormattedSql = statement.FormattedSql,
        RawSql = statement.RawSql,
        ShortSql = statement.ShortSql,
        IsCached = statement.Options.HasFlag((Enum) SqlStatementOptions.Cached),
        IsDDL = statement.Options.HasFlag((Enum) SqlStatementOptions.DDL),
        IsTransaction = statement.Options.HasFlag((Enum) SqlStatementOptions.Transaction) || statement.Options.HasFlag((Enum) SqlStatementOptions.DtcTransaction)
      };
    }
  }
}
