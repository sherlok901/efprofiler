﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Reports.IReport
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using HibernatingRhinos.Profiler.Client.Infrastructure;
using System.ComponentModel;
using System.Xml.Linq;

namespace HibernatingRhinos.Profiler.Client.Reports
{
  public interface IReport : IScreen, IHaveDisplayName, IActivate, IDeactivate, IGuardClose, IClose, INotifyPropertyChangedEx, INotifyPropertyChanged, IPoller
  {
    string JsonReportName { get; }

    bool FilteringSupported { get; }

    void Clear();

    string ExportReportToJsonString();

    void ExportJson(string file);

    XElement ExportReport();

    void ForceUpdate();
  }
}
