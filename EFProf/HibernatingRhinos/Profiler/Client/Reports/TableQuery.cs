﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Reports.TableQuery
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.BackEnd.Reports;
using HibernatingRhinos.Profiler.Client.Filtering;
using HibernatingRhinos.Profiler.Client.Infrastructure;
using HibernatingRhinos.Profiler.Client.Tracking;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows.Data;
using System.Windows.Input;

namespace HibernatingRhinos.Profiler.Client.Reports
{
  public class TableQuery : SelectionBase, INavigateItems
  {
    private readonly ObservableCollection<QueryAggregation> unfilteredStatements = new ObservableCollection<QueryAggregation>();
    private readonly FilterServiceModel filterService;
    private readonly ITrackingService trackingService;
    private EventHandler filterServiceOnFiltersChanged;

    public TableQuery(FilterServiceModel filterService, ITrackingService trackingService)
    {
      this.filterService = filterService;
      this.trackingService = trackingService;
      this.filterServiceOnFiltersChanged = (EventHandler) ((sender, args) => this.ApplyFilters());
      this.filterService.FiltersChanged += this.filterServiceOnFiltersChanged;
      this.StatementsCollection = new CollectionViewSource()
      {
        Source = (object) this.unfilteredStatements
      };
      this.ApplyFilters();
    }

    [Display(Name = "Table")]
    public string Name { get; set; }

    public CollectionViewSource StatementsCollection { get; set; }

    public ObservableCollection<QueryAggregation> UnfilteredStatements
    {
      get
      {
        return this.unfilteredStatements;
      }
    }

    public void NextItem()
    {
      if (this.StatementsCollection.View.MoveCurrentToNext())
        return;
      this.StatementsCollection.View.MoveCurrentToLast();
    }

    public void PreviousItem()
    {
      if (this.StatementsCollection.View.MoveCurrentToPrevious())
        return;
      this.StatementsCollection.View.MoveCurrentToFirst();
    }

    private void ApplyFilters()
    {
      this.StatementsCollection.View.Filter = (Predicate<object>) null;
      if (this.filterService.Filters.Count == 0)
        return;
      this.StatementsCollection.View.Filter = new Predicate<object>(this.FilterStatements);
    }

    private bool FilterStatements(object statement)
    {
      return this.filterService.FilterStatement((IFilterableStatementSnapshot) statement);
    }

    public static TableQuery CreateFrom(TableQueriesSnapshot snapshot, FilterServiceModel filterService, ITrackingService trackingService)
    {
      TableQuery tableQuery = new TableQuery(filterService, trackingService)
      {
        Name = snapshot.Name
      };
      foreach (QueryAggregationSnapshot statement in snapshot.Statements)
      {
        QueryAggregation from = QueryAggregation.CreateFrom(statement);
        tableQuery.unfilteredStatements.Add(from);
      }
      return tableQuery;
    }

    public void UpdateFrom(TableQueriesSnapshot snapshot)
    {
      foreach (QueryAggregationSnapshot statement1 in snapshot.Statements)
      {
        QueryAggregationSnapshot statement = statement1;
        QueryAggregation queryAggregation = this.unfilteredStatements.Where<QueryAggregation>((Func<QueryAggregation, bool>) (item => item.RawSql == statement.RawSql)).FirstOrDefault<QueryAggregation>();
        if (queryAggregation == null)
        {
          this.unfilteredStatements.Add(QueryAggregation.CreateFrom(statement));
          if (this.unfilteredStatements.Count == 1)
            this.StatementsCollection.View.MoveCurrentToFirst();
        }
        else
          queryAggregation.UpdateFrom(statement);
      }
    }

    public void Clear()
    {
      this.unfilteredStatements.Clear();
    }

    public void CancelSorting(MouseButtonEventArgs e)
    {
      this.StatementsCollection.View.SortDescriptions.Clear();
      e.Handled = true;
    }
  }
}
