﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Reports.QueriesByMethodModel
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.Reports;
using HibernatingRhinos.Profiler.Client.Filtering;
using HibernatingRhinos.Profiler.Client.Infrastructure;
using HibernatingRhinos.Profiler.Client.Tracking;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows.Data;
using System.Xml.Linq;

namespace HibernatingRhinos.Profiler.Client.Reports
{
  [Serializable]
  public class QueriesByMethodModel : ReportBase<IEnumerable<MethodQueriesSnapshot>>, INavigateItems
  {
    public readonly ObservableCollection<MethodQuery> methods = new ObservableCollection<MethodQuery>();
    private readonly FilterServiceModel filterService;

    public QueriesByMethodModel(BackendBridge backendBridge, FilterServiceModel filterService, ITrackingService trackingService)
      : base(trackingService, new Func<IEnumerable<MethodQueriesSnapshot>>(backendBridge.Reports.QueriesByMethod.GetReportSnapshot))
    {
      this.filterService = filterService;
      this.DisplayName = "Queries By Method";
      this.MethodsByTypeCollectionView = (CollectionView) new ListCollectionView((IList) this.methods);
      this.MethodsByTypeCollectionView.GroupDescriptions.Add((GroupDescription) new PropertyGroupDescription("TypeName"));
      this.MethodsByTypeCollectionView.SortDescriptions.Add(new SortDescription("TypeName", ListSortDirection.Ascending));
      this.methods.CollectionChanged += (NotifyCollectionChangedEventHandler) ((s, e) => this.MethodsByTypeCollectionView.Refresh());
    }

    public void NextItem()
    {
      if (this.MethodsByTypeCollectionView.MoveCurrentToNext())
        return;
      this.MethodsByTypeCollectionView.MoveCurrentToLast();
    }

    public void PreviousItem()
    {
      if (this.MethodsByTypeCollectionView.MoveCurrentToPrevious())
        return;
      this.MethodsByTypeCollectionView.MoveCurrentToFirst();
    }

    public CollectionView MethodsByTypeCollectionView { get; set; }

    public override bool FilteringSupported
    {
      get
      {
        return true;
      }
    }

    protected override void Update(IEnumerable<MethodQueriesSnapshot> snapshots)
    {
      foreach (MethodQueriesSnapshot snapshot1 in snapshots)
      {
        MethodQueriesSnapshot snapshot = snapshot1;
        MethodQuery methodQuery = this.methods.Where<MethodQuery>((Func<MethodQuery, bool>) (shot => shot.FullName == snapshot.FullName)).FirstOrDefault<MethodQuery>();
        if (methodQuery == null)
          this.methods.Add(MethodQuery.CreateFrom(snapshot, this.filterService, this.trackingService));
        else
          methodQuery.UpdateFrom(snapshot);
      }
      if (this.NotifyOnUpdate == null)
        return;
      this.NotifyOnUpdate();
    }

    public override string JsonReportName
    {
      get
      {
        return "queriesByMethod";
      }
    }

    public override void Clear()
    {
      foreach (MethodQuery method in (Collection<MethodQuery>) this.methods)
        method.Clear();
      this.methods.Clear();
    }

    public override XElement ExportReport()
    {
      int queryCount = 0;
      Dictionary<QueryAggregation, int> indexes = new Dictionary<QueryAggregation, int>();
      Func<QueryAggregation, int> getId = (Func<QueryAggregation, int>) (queryAgg =>
      {
        int num1;
        if (indexes.TryGetValue(queryAgg, out num1))
          return num1;
        Dictionary<QueryAggregation, int> dictionary = indexes;
        QueryAggregation index = queryAgg;
        int num2 = queryCount++;
        int num3;
        int num4 = num3 = num2;
        dictionary[index] = num3;
        return num4;
      });
      return new XElement(ReportBase.Namespace + "queries-by-method", (object) this.methods.Select<MethodQuery, XElement>((Func<MethodQuery, XElement>) (x => new XElement(ReportBase.Namespace + "method", new object[3]
      {
        (object) new XAttribute((XName) "full-name", (object) x.FullName),
        (object) new XAttribute((XName) "statement-count", (object) x.UnfilteredStatements.Count),
        (object) x.UnfilteredStatements.Select<QueryAggregation, XElement>((Func<QueryAggregation, XElement>) (q => new XElement(ReportBase.Namespace + "query", new object[8]
        {
          (object) new XAttribute((XName) "query-id", (object) getId(q)),
          (object) q.Duration.ToXml(),
          (object) q.AverageDuration.ToXml("average-duration"),
          (object) new XElement(ReportBase.Namespace + "count", (object) q.Count),
          (object) new XAttribute((XName) "is-cached", (object) q.IsCached),
          (object) new XAttribute((XName) "is-ddl", (object) q.IsDDL),
          (object) new XElement(ReportBase.Namespace + "short-sql", (object) q.ShortSql),
          (object) new XElement(ReportBase.Namespace + "formatted-sql", (object) q.FormattedSql)
        })))
      }))));
    }

    [CLSCompliant(false)]
    protected override JContainer ExportReportToJson()
    {
      int queryCount = 0;
      Dictionary<QueryAggregation, int> indexes = new Dictionary<QueryAggregation, int>();
      Func<QueryAggregation, int> getId = (Func<QueryAggregation, int>) (queryAgg =>
      {
        int num1;
        if (indexes.TryGetValue(queryAgg, out num1))
          return num1;
        Dictionary<QueryAggregation, int> dictionary = indexes;
        QueryAggregation index = queryAgg;
        int num2 = queryCount++;
        int num3;
        int num4 = num3 = num2;
        dictionary[index] = num3;
        return num4;
      });
      return (JContainer) new JArray((object) this.methods.Select<MethodQuery, JObject>((Func<MethodQuery, JObject>) (x => new JObject(new object[3]
      {
        (object) new JProperty("fullName", (object) x.FullName),
        (object) new JProperty("statementCount", (object) x.UnfilteredStatements.Count),
        (object) new JProperty("queries", (object) new JArray((object) x.UnfilteredStatements.Select<QueryAggregation, JObject>((Func<QueryAggregation, JObject>) (q => new JObject(new object[7]
        {
          (object) new JProperty("queryId", (object) getId(q)),
          (object) new JProperty("averageDuration", (object) q.AverageDuration),
          (object) new JProperty("count", (object) q.Count),
          (object) new JProperty("isCached", (object) q.IsCached),
          (object) new JProperty("isDdl", (object) q.IsDDL),
          (object) new JProperty("shortSql", (object) q.ShortSql),
          (object) new JProperty("formattedSql", (object) q.FormattedSql)
        })))))
      }))));
    }
  }
}
