﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Reports.ExpensiveQueriesModel
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.BackEnd.Reports;
using HibernatingRhinos.Profiler.Client.Filtering;
using HibernatingRhinos.Profiler.Client.Infrastructure;
using HibernatingRhinos.Profiler.Client.Tracking;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Data;
using System.Windows.Input;
using System.Xml.Linq;

namespace HibernatingRhinos.Profiler.Client.Reports
{
  [Serializable]
  public class ExpensiveQueriesModel : ReportBase<IEnumerable<QueryAggregationSnapshot>>, INavigateItems
  {
    private readonly ObservableCollection<QueryAggregation> unfilteredQueries = new ObservableCollection<QueryAggregation>();
    private readonly FilterServiceModel filterservice;
    private EventHandler filterserviceOnFiltersChanged;

    public ExpensiveQueriesModel(BackendBridge backendBridge, FilterServiceModel filterservice, ITrackingService trackingService)
      : base(trackingService, new Func<IEnumerable<QueryAggregationSnapshot>>(backendBridge.Reports.ExpensiveQueries.GetReportSnapshot))
    {
      this.filterservice = filterservice;
      this.filterserviceOnFiltersChanged = (EventHandler) ((sender, args) => this.ApplyFilters());
      filterservice.FiltersChanged += this.filterserviceOnFiltersChanged;
      this.DisplayName = "Expensive Queries";
      this.StatementsCollection = new CollectionViewSource()
      {
        Source = (object) this.unfilteredQueries
      };
    }

    public void NextItem()
    {
      if (this.StatementsCollection.View.MoveCurrentToNext())
        return;
      this.StatementsCollection.View.MoveCurrentToLast();
    }

    public void PreviousItem()
    {
      if (this.StatementsCollection.View.MoveCurrentToPrevious())
        return;
      this.StatementsCollection.View.MoveCurrentToFirst();
    }

    public CollectionViewSource StatementsCollection { get; set; }

    public override void Clear()
    {
      this.unfilteredQueries.Clear();
    }

    private void ApplyFilters()
    {
      this.StatementsCollection.View.Filter = (Predicate<object>) null;
      if (this.filterservice.Filters.Count == 0)
        return;
      this.StatementsCollection.View.Filter = new Predicate<object>(this.FilterStatements);
    }

    private bool FilterStatements(object statement)
    {
      return this.filterservice.FilterStatement((IFilterableStatementSnapshot) statement);
    }

    protected override void Update(IEnumerable<QueryAggregationSnapshot> snapshots)
    {
      this.RemoveNoLongerPresent(snapshots);
      foreach (QueryAggregationSnapshot snapshot1 in snapshots)
      {
        QueryAggregationSnapshot snapshot = snapshot1;
        QueryAggregation queryAggregation = this.unfilteredQueries.Where<QueryAggregation>((Func<QueryAggregation, bool>) (query => query.RawSql == snapshot.RawSql)).FirstOrDefault<QueryAggregation>();
        if (queryAggregation != null)
        {
          queryAggregation.UpdateFrom(snapshot);
        }
        else
        {
          this.unfilteredQueries.Add(QueryAggregation.CreateFrom(snapshot));
          if (this.unfilteredQueries.Count == 1)
            this.StatementsCollection.View.MoveCurrentToFirst();
        }
      }
      if (this.NotifyOnUpdate == null)
        return;
      this.NotifyOnUpdate();
    }

    private void RemoveNoLongerPresent(IEnumerable<QueryAggregationSnapshot> snapshots)
    {
      IList<QueryAggregation> queryAggregationList = (IList<QueryAggregation>) new List<QueryAggregation>();
      foreach (QueryAggregation unfilteredQuery in (Collection<QueryAggregation>) this.unfilteredQueries)
      {
        QueryAggregation query = unfilteredQuery;
        if (snapshots.Where<QueryAggregationSnapshot>((Func<QueryAggregationSnapshot, bool>) (snapshot => snapshot.RawSql == query.RawSql)).FirstOrDefault<QueryAggregationSnapshot>() == null)
          queryAggregationList.Add(query);
      }
      foreach (QueryAggregation queryAggregation in (IEnumerable<QueryAggregation>) queryAggregationList)
        this.unfilteredQueries.Remove(queryAggregation);
    }

    public override bool FilteringSupported
    {
      get
      {
        return true;
      }
    }

    public override XElement ExportReport()
    {
      return new XElement(ReportBase.Namespace + "expensive-queries", (object) this.unfilteredQueries.Select<QueryAggregation, XElement>((Func<QueryAggregation, XElement>) (x => new XElement(ReportBase.Namespace + "query", new object[7]
      {
        (object) new XAttribute((XName) "count", (object) x.Count),
        (object) x.Duration.ToXml(),
        (object) x.AverageDuration.ToXml("average-duration"),
        (object) new XAttribute((XName) "is-cached", (object) x.IsCached),
        (object) new XAttribute((XName) "is-ddl", (object) x.IsDDL),
        (object) new XElement(ReportBase.Namespace + "short-sql", (object) x.ShortSql),
        (object) new XElement(ReportBase.Namespace + "formatted-sql", (object) x.FormattedSql)
      }))));
    }

    public override string JsonReportName
    {
      get
      {
        return "expensiveQueries";
      }
    }

    [CLSCompliant(false)]
    protected override JContainer ExportReportToJson()
    {
      return (JContainer) new JArray((object) this.unfilteredQueries.Select<QueryAggregation, JObject>((Func<QueryAggregation, JObject>) (x => new JObject(new object[6]
      {
        (object) new JProperty("averageDuration", (object) x.AverageDuration),
        (object) new JProperty("count", (object) x.Count),
        (object) new JProperty("isCached", (object) x.IsCached),
        (object) new JProperty("isDdl", (object) x.IsDDL),
        (object) new JProperty("shortSql", (object) x.ShortSql),
        (object) new JProperty("formattedSql", (object) x.FormattedSql)
      }))));
    }

    public void CancelSorting(MouseButtonEventArgs e)
    {
      this.StatementsCollection.View.SortDescriptions.Clear();
      e.Handled = true;
    }
  }
}
