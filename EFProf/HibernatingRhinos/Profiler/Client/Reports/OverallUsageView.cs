﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Reports.OverallUsageView
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.BackEnd.Profiles;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace HibernatingRhinos.Profiler.Client.Reports
{
  public partial class OverallUsageView : UserControl, IComponentConnector
  {
    private readonly FieldInfo contextMenuMousePositionFieldInfo;
    //internal Grid MainGrid;
    //internal RowDefinition SplitterRow;
    //internal RowDefinition EntitiesRow;
    //internal GridSplitter Splitter;
    //internal TabControl Entities;
    //private bool _contentLoaded;

    public OverallUsageView()
    {
      this.InitializeComponent();
      if (Profile.Current.Supports(SupportedFeatures.Entities))
        return;
      this.MainGrid.RowDefinitions.Remove(this.SplitterRow);
      this.MainGrid.RowDefinitions.Remove(this.EntitiesRow);
      this.MainGrid.Children.Remove((UIElement) this.Splitter);
      this.MainGrid.Children.Remove((UIElement) this.Entities);
    }

    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //public void InitializeComponent()
    //{
    //  if (this._contentLoaded)
    //    return;
    //  this._contentLoaded = true;
    //  Application.LoadComponent((object) this, new Uri("/EFProf;component/reports/overallusageview.xaml", UriKind.Relative));
    //}

    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //internal Delegate _CreateDelegate(Type delegateType, string handler)
    //{
    //  return Delegate.CreateDelegate(delegateType, (object) this, handler);
    //}

    //[EditorBrowsable(EditorBrowsableState.Never)]
    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //void IComponentConnector.Connect(int connectionId, object target)
    //{
    //  switch (connectionId)
    //  {
    //    case 1:
    //      this.MainGrid = (Grid) target;
    //      break;
    //    case 2:
    //      this.SplitterRow = (RowDefinition) target;
    //      break;
    //    case 3:
    //      this.EntitiesRow = (RowDefinition) target;
    //      break;
    //    case 4:
    //      this.Splitter = (GridSplitter) target;
    //      break;
    //    case 5:
    //      this.Entities = (TabControl) target;
    //      break;
    //    default:
    //      this._contentLoaded = true;
    //      break;
    //  }
    //}
  }
}
