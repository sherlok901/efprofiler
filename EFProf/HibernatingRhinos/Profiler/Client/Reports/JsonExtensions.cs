﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Reports.JsonExtensions
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.IO;

namespace HibernatingRhinos.Profiler.Client.Reports
{
  public static class JsonExtensions
  {
    [CLSCompliant(false)]
    public static void WriteJson(this JToken obj, TextWriter writer)
    {
      JsonTextWriter jsonTextWriter1 = new JsonTextWriter(writer);
      jsonTextWriter1.Indentation = 1;
      jsonTextWriter1.IndentChar = '\t';
      jsonTextWriter1.Formatting = Formatting.Indented;
      JsonTextWriter jsonTextWriter2 = jsonTextWriter1;
      obj.WriteTo((JsonWriter) jsonTextWriter2);
      jsonTextWriter2.Flush();
      writer.Flush();
    }
  }
}
