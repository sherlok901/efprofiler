﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Reports.QueriesByTableModel
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.Reports;
using HibernatingRhinos.Profiler.Client.Filtering;
using HibernatingRhinos.Profiler.Client.Infrastructure;
using HibernatingRhinos.Profiler.Client.Tracking;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows.Data;
using System.Xml.Linq;

namespace HibernatingRhinos.Profiler.Client.Reports
{
  [Serializable]
  public class QueriesByTableModel : ReportBase<IEnumerable<TableQueriesSnapshot>>, INavigateItems
  {
    public readonly ObservableCollection<TableQuery> tables = new ObservableCollection<TableQuery>();
    private readonly FilterServiceModel filterService;

    public QueriesByTableModel(BackendBridge backendBridge, FilterServiceModel filterService, ITrackingService trackingService)
      : base(trackingService, new Func<IEnumerable<TableQueriesSnapshot>>(backendBridge.Reports.QueriesByTable.GetReportSnapshot))
    {
      this.filterService = filterService;
      this.DisplayName = "Queries By Table";
      this.TableCollectionView = (CollectionView) new ListCollectionView((IList) this.tables);
      this.TableCollectionView.SortDescriptions.Add(new SortDescription("Name", ListSortDirection.Ascending));
      this.tables.CollectionChanged += (NotifyCollectionChangedEventHandler) ((s, e) => this.TableCollectionView.Refresh());
    }

    public CollectionView TableCollectionView { get; set; }

    public void NextItem()
    {
      if (this.TableCollectionView.MoveCurrentToPrevious())
        return;
      this.TableCollectionView.MoveCurrentToFirst();
    }

    public void PreviousItem()
    {
      if (this.TableCollectionView.MoveCurrentToPrevious())
        return;
      this.TableCollectionView.MoveCurrentToFirst();
    }

    public override bool FilteringSupported
    {
      get
      {
        return true;
      }
    }

    protected override void Update(IEnumerable<TableQueriesSnapshot> snapshots)
    {
      foreach (TableQueriesSnapshot snapshot1 in snapshots)
      {
        TableQueriesSnapshot snapshot = snapshot1;
        TableQuery tableQuery = this.tables.Where<TableQuery>((Func<TableQuery, bool>) (shot => shot.Name == snapshot.Name)).FirstOrDefault<TableQuery>();
        if (tableQuery == null)
          this.tables.Add(TableQuery.CreateFrom(snapshot, this.filterService, this.trackingService));
        else
          tableQuery.UpdateFrom(snapshot);
      }
      if (this.NotifyOnUpdate == null)
        return;
      this.NotifyOnUpdate();
    }

    public override string JsonReportName
    {
      get
      {
        return "queriesByTable";
      }
    }

    public override void Clear()
    {
      foreach (TableQuery table in (Collection<TableQuery>) this.tables)
        table.Clear();
      this.tables.Clear();
    }

    public override XElement ExportReport()
    {
      int queryCount = 0;
      Dictionary<QueryAggregation, int> indexes = new Dictionary<QueryAggregation, int>();
      Func<QueryAggregation, int> getId = (Func<QueryAggregation, int>) (queryAgg =>
      {
        int num1;
        if (indexes.TryGetValue(queryAgg, out num1))
          return num1;
        Dictionary<QueryAggregation, int> dictionary = indexes;
        QueryAggregation index = queryAgg;
        int num2 = queryCount++;
        int num3;
        int num4 = num3 = num2;
        dictionary[index] = num3;
        return num4;
      });
      return new XElement(ReportBase.Namespace + "queries-by-table", (object) this.tables.Select<TableQuery, XElement>((Func<TableQuery, XElement>) (x => new XElement(ReportBase.Namespace + "table", new object[3]
      {
        (object) new XAttribute((XName) "name", (object) x.Name),
        (object) new XAttribute((XName) "statement-count", (object) x.UnfilteredStatements.Count),
        (object) x.UnfilteredStatements.Select<QueryAggregation, XElement>((Func<QueryAggregation, XElement>) (q => new XElement(ReportBase.Namespace + "query", new object[8]
        {
          (object) new XAttribute((XName) "query-id", (object) getId(q)),
          (object) q.Duration.ToXml(),
          (object) q.AverageDuration.ToXml("average-duration"),
          (object) new XElement(ReportBase.Namespace + "count", (object) q.Count),
          (object) new XAttribute((XName) "is-cached", (object) q.IsCached),
          (object) new XAttribute((XName) "is-ddl", (object) q.IsDDL),
          (object) new XElement(ReportBase.Namespace + "short-sql", (object) q.ShortSql),
          (object) new XElement(ReportBase.Namespace + "formatted-sql", (object) q.FormattedSql)
        })))
      }))));
    }

    [CLSCompliant(false)]
    protected override JContainer ExportReportToJson()
    {
      int queryCount = 0;
      Dictionary<QueryAggregation, int> indexes = new Dictionary<QueryAggregation, int>();
      Func<QueryAggregation, int> getId = (Func<QueryAggregation, int>) (queryAgg =>
      {
        int num1;
        if (indexes.TryGetValue(queryAgg, out num1))
          return num1;
        Dictionary<QueryAggregation, int> dictionary = indexes;
        QueryAggregation index = queryAgg;
        int num2 = queryCount++;
        int num3;
        int num4 = num3 = num2;
        dictionary[index] = num3;
        return num4;
      });
      return (JContainer) new JArray((object) this.tables.Select<TableQuery, JObject>((Func<TableQuery, JObject>) (x => new JObject(new object[3]
      {
        (object) new JProperty("name", (object) x.Name),
        (object) new JProperty("statementCount", (object) x.UnfilteredStatements.Count),
        (object) new JProperty("queries", (object) new JArray((object) x.UnfilteredStatements.Select<QueryAggregation, JObject>((Func<QueryAggregation, JObject>) (q => new JObject(new object[7]
        {
          (object) new JProperty("queryId", (object) getId(q)),
          (object) new JProperty("averageDuration", (object) q.AverageDuration),
          (object) new JProperty("count", (object) q.Count),
          (object) new JProperty("isCached", (object) q.IsCached),
          (object) new JProperty("isDdl", (object) q.IsDDL),
          (object) new JProperty("shortSql", (object) q.ShortSql),
          (object) new JProperty("formattedSql", (object) q.FormattedSql)
        })))))
      }))));
    }
  }
}
