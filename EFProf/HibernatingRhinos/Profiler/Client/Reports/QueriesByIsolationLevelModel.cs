﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Reports.QueriesByIsolationLevelModel
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.Reports;
using HibernatingRhinos.Profiler.Client.Filtering;
using HibernatingRhinos.Profiler.Client.Infrastructure;
using HibernatingRhinos.Profiler.Client.Tracking;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;
using System.Windows.Data;
using System.Xml.Linq;

namespace HibernatingRhinos.Profiler.Client.Reports
{
  public class QueriesByIsolationLevelModel : ReportBase<IEnumerable<IsolationLevelQueriesSnapshot>>, INavigateItems
  {
    private readonly FilterServiceModel filterService;

    public QueriesByIsolationLevelModel(BackendBridge backendBridge, FilterServiceModel filterService, ITrackingService trackingService)
      : base(trackingService, new Func<IEnumerable<IsolationLevelQueriesSnapshot>>(backendBridge.Reports.QueriesByIsolationLevel.GetReportSnapshot))
    {
      this.filterService = filterService;
      this.DisplayName = "Queries By Isolation Level";
      this.StatementsByIsolationLevel = new ObservableCollection<IsolationLevelQuery>();
      this.StatementsByIsolationLevelCollection = new CollectionViewSource()
      {
        Source = (object) this.StatementsByIsolationLevel
      };
    }

    public CollectionViewSource StatementsByIsolationLevelCollection { get; set; }

    public ObservableCollection<IsolationLevelQuery> StatementsByIsolationLevel { get; private set; }

    public override bool FilteringSupported
    {
      get
      {
        return true;
      }
    }

    public void NextItem()
    {
      if (this.StatementsByIsolationLevelCollection.View.MoveCurrentToNext())
        return;
      this.StatementsByIsolationLevelCollection.View.MoveCurrentToLast();
    }

    public void PreviousItem()
    {
      if (this.StatementsByIsolationLevelCollection.View.MoveCurrentToPrevious())
        return;
      this.StatementsByIsolationLevelCollection.View.MoveCurrentToFirst();
    }

    protected override void Update(IEnumerable<IsolationLevelQueriesSnapshot> snapshots)
    {
      foreach (IsolationLevelQueriesSnapshot snapshot1 in snapshots)
      {
        IsolationLevelQueriesSnapshot snapshot = snapshot1;
        IsolationLevelQuery isolationLevelQuery = this.StatementsByIsolationLevel.Where<IsolationLevelQuery>((Func<IsolationLevelQuery, bool>) (shot => shot.IsolationLevel == snapshot.IsolationLevel)).FirstOrDefault<IsolationLevelQuery>();
        if (isolationLevelQuery == null)
          this.StatementsByIsolationLevel.Add(IsolationLevelQuery.CreateFrom(snapshot, this.filterService, this.trackingService));
        else
          isolationLevelQuery.UpdateFrom(snapshot);
      }
      if (this.NotifyOnUpdate == null)
        return;
      this.NotifyOnUpdate();
    }

    public override void Clear()
    {
      foreach (IsolationLevelQuery isolationLevelQuery in (Collection<IsolationLevelQuery>) this.StatementsByIsolationLevel)
        isolationLevelQuery.Clear();
      this.StatementsByIsolationLevel.Clear();
      this.NotifyOfPropertyChange("");
      this.NotifyOfPropertyChange<ObservableCollection<IsolationLevelQuery>>((Expression<Func<ObservableCollection<IsolationLevelQuery>>>) (() => this.StatementsByIsolationLevel));
    }

    public override XElement ExportReport()
    {
      int queryCount = 0;
      Dictionary<QueryAggregation, int> indexes = new Dictionary<QueryAggregation, int>();
      Func<QueryAggregation, int> getId = (Func<QueryAggregation, int>) (queryAgg =>
      {
        int num1;
        if (indexes.TryGetValue(queryAgg, out num1))
          return num1;
        Dictionary<QueryAggregation, int> dictionary = indexes;
        QueryAggregation index = queryAgg;
        int num2 = queryCount++;
        int num3;
        int num4 = num3 = num2;
        dictionary[index] = num3;
        return num4;
      });
      return new XElement(ReportBase.Namespace + "queries-by-isolation-level", (object) this.StatementsByIsolationLevel.Select<IsolationLevelQuery, XElement>((Func<IsolationLevelQuery, XElement>) (x => new XElement(ReportBase.Namespace + "isolation-level", new object[3]
      {
        (object) new XAttribute((XName) "isolation-level", (object) x.IsolationLevel),
        (object) new XAttribute((XName) "statement-count", (object) x.UnfilteredStatements.Count),
        (object) x.UnfilteredStatements.Select<QueryAggregation, XElement>((Func<QueryAggregation, XElement>) (q => new XElement(ReportBase.Namespace + "query", new object[8]
        {
          (object) new XAttribute((XName) "query-id", (object) getId(q)),
          (object) q.Duration.ToXml(),
          (object) q.AverageDuration.ToXml("average-duration"),
          (object) new XElement(ReportBase.Namespace + "count", (object) q.Count),
          (object) new XAttribute((XName) "is-cached", (object) q.IsCached),
          (object) new XAttribute((XName) "is-ddl", (object) q.IsDDL),
          (object) new XElement(ReportBase.Namespace + "short-sql", (object) q.ShortSql),
          (object) new XElement(ReportBase.Namespace + "formatted-sql", (object) q.FormattedSql)
        })))
      }))));
    }

    [CLSCompliant(false)]
    protected override JContainer ExportReportToJson()
    {
      int queryCount = 0;
      Dictionary<QueryAggregation, int> indexes = new Dictionary<QueryAggregation, int>();
      Func<QueryAggregation, int> getId = (Func<QueryAggregation, int>) (queryAgg =>
      {
        int num1;
        if (indexes.TryGetValue(queryAgg, out num1))
          return num1;
        Dictionary<QueryAggregation, int> dictionary = indexes;
        QueryAggregation index = queryAgg;
        int num2 = queryCount++;
        int num3;
        int num4 = num3 = num2;
        dictionary[index] = num3;
        return num4;
      });
      return (JContainer) new JArray((object) this.StatementsByIsolationLevel.Select<IsolationLevelQuery, JObject>((Func<IsolationLevelQuery, JObject>) (x => new JObject(new object[3]
      {
        (object) new JProperty("isolationLevel", (object) x.IsolationLevel),
        (object) new JProperty("statementCount", (object) x.UnfilteredStatements.Count),
        (object) new JProperty("queries", (object) new JArray((object) x.UnfilteredStatements.Select<QueryAggregation, JObject>((Func<QueryAggregation, JObject>) (q => new JObject(new object[7]
        {
          (object) new JProperty("queryId", (object) getId(q)),
          (object) new JProperty("averageDuration", (object) q.AverageDuration),
          (object) new JProperty("count", (object) q.Count),
          (object) new JProperty("isCached", (object) q.IsCached),
          (object) new JProperty("isDdl", (object) q.IsDDL),
          (object) new JProperty("shortSql", (object) q.ShortSql),
          (object) new JProperty("formattedSql", (object) q.FormattedSql)
        })))))
      }))));
    }

    public override string JsonReportName
    {
      get
      {
        return "queriesByIsolationLevel";
      }
    }
  }
}
