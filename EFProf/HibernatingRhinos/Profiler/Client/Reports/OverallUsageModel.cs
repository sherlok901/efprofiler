﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Reports.OverallUsageModel
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.Client.Tracking;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Windows.Data;
using System.Windows.Input;
using System.Xml.Linq;

namespace HibernatingRhinos.Profiler.Client.Reports
{
  [Serializable]
  public class OverallUsageModel : ReportBase<OverallUsageSnapshot>
  {
    public OverallUsageModel(BackendBridge backendBridge, ITrackingService trackingService)
      : base(trackingService, new Func<OverallUsageSnapshot>(backendBridge.Reports.OverallUsage.GetReportSnapshot))
    {
      this.DisplayName = "Overall Usage";
      this.AggregatedAlerts = new IAggregatedAlertSnapshot[0];
      this.AggregatedEntities = (IDictionary<string, TotalAndAverage>) new Dictionary<string, TotalAndAverage>();
      this.AggregatedAlertsCollectionView = (ICollectionView) new ListCollectionView((IList) this.AggregatedAlerts);
    }

    public IDictionary<string, TotalAndAverage> AggregatedEntities { get; set; }

    public IAggregatedAlertSnapshot[] AggregatedAlerts { get; set; }

    public double AverageEntitiesLoadedPerSession { get; set; }

    public int NumberOfEntitiesTypes { get; set; }

    public int NumberOfAlerts { get; set; }

    public double AverageQueryRowCount { get; set; }

    public int NumberOfTransactionRollbacks { get; set; }

    public int NumberOfTransactionCommits { get; set; }

    public int NumberOfTransactions { get; set; }

    public double AverageStatementsPerSession { get; set; }

    public int CountOfSessions { get; set; }

    public ICollectionView AggregatedAlertsCollectionView { get; set; }

    public override string JsonReportName
    {
      get
      {
        return "overallUsage";
      }
    }

    protected override void Update(OverallUsageSnapshot snapshot)
    {
      this.AggregatedAlerts = snapshot.AggregatedAlerts ?? new IAggregatedAlertSnapshot[0];
      this.AggregatedEntities = (IDictionary<string, TotalAndAverage>) (snapshot.AggregatedEntities ?? new Dictionary<string, TotalAndAverage>());
      this.AverageEntitiesLoadedPerSession = snapshot.AverageEntitiesLoadedPerSession;
      this.AverageQueryRowCount = snapshot.AverageQueryRowCount.GetValueOrDefault(0.0);
      this.AverageStatementsPerSession = snapshot.AverageStatementsPerSession;
      this.CountOfSessions = snapshot.CountOfSessions;
      this.NumberOfAlerts = snapshot.NumberOfAlerts;
      this.NumberOfEntitiesTypes = snapshot.NumberOfEntitiesTypes;
      this.NumberOfTransactionCommits = snapshot.NumberOfTransactionCommits;
      this.NumberOfTransactionRollbacks = snapshot.NumberOfTransactionRollbacks;
      this.NumberOfTransactions = snapshot.NumberOfTransactions;
      SortDescriptionCollection sortDescriptions = this.AggregatedAlertsCollectionView.SortDescriptions;
      this.AggregatedAlertsCollectionView = (ICollectionView) new ListCollectionView((IList) this.AggregatedAlerts);
      sortDescriptions.ToList<SortDescription>().ForEach((Action<SortDescription>) (x => this.AggregatedAlertsCollectionView.SortDescriptions.Add(x)));
      this.NotifyOfPropertyChange<ICollectionView>((Expression<Func<ICollectionView>>) (() => this.AggregatedAlertsCollectionView));
      this.NotifyOfPropertyChange("");
      if (this.NotifyOnUpdate == null)
        return;
      this.NotifyOnUpdate();
    }

    public override void Clear()
    {
    }

    public override XElement ExportReport()
    {
      XElement xelement = new XElement(ReportBase.Namespace + "overall-usage", new object[10]
      {
        (object) new XElement(ReportBase.Namespace + "aggregated-alerts", (object) ((IEnumerable<IAggregatedAlertSnapshot>) this.AggregatedAlerts).Select<IAggregatedAlertSnapshot, XElement>((Func<IAggregatedAlertSnapshot, XElement>) (x => new XElement(ReportBase.Namespace + "alert", new object[4]
        {
          (object) new XAttribute((XName) "title", (object) x.Alert.Title),
          (object) new XAttribute((XName) "help-topic", (object) x.Alert.HelpTopic),
          (object) new XAttribute((XName) "severity", (object) x.Alert.Severity),
          (object) new XAttribute((XName) "count", (object) x.Count)
        })))),
        (object) new XElement(ReportBase.Namespace + "aggregated-entities", (object) this.AggregatedEntities.Select<KeyValuePair<string, TotalAndAverage>, XElement>((Func<KeyValuePair<string, TotalAndAverage>, XElement>) (x => new XElement(ReportBase.Namespace + "entity", new object[3]
        {
          (object) new XAttribute((XName) "name", (object) x.Key),
          (object) new XAttribute((XName) "total", (object) x.Value.Total),
          (object) new XAttribute((XName) "average-per-session", (object) x.Value.Average)
        })))),
        (object) new XElement(ReportBase.Namespace + "average-entities-loaded-per-session", (object) this.AverageEntitiesLoadedPerSession),
        (object) new XElement(ReportBase.Namespace + "average-statements-per-session", (object) this.AverageStatementsPerSession),
        (object) new XElement(ReportBase.Namespace + "count-of-sessions", (object) this.CountOfSessions),
        (object) new XElement(ReportBase.Namespace + "number-of-alerts", (object) this.NumberOfAlerts),
        (object) new XElement(ReportBase.Namespace + "number-of-entities-types", (object) this.NumberOfEntitiesTypes),
        (object) new XElement(ReportBase.Namespace + "number-of-transaction-commits", (object) this.NumberOfTransactionCommits),
        (object) new XElement(ReportBase.Namespace + "number-of-transaction-rollbacks", (object) this.NumberOfTransactionRollbacks),
        (object) new XElement(ReportBase.Namespace + "number-of-transactions", (object) this.NumberOfTransactions)
      });
      xelement.Add((object) new XElement(ReportBase.Namespace + "average-query-row-count", (object) this.AverageQueryRowCount));
      return xelement;
    }

    [CLSCompliant(false)]
    protected override JContainer ExportReportToJson()
    {
      return (JContainer) new JObject(new object[11]
      {
        (object) new JProperty("aggregatedAlerts", (object) new JArray((object) ((IEnumerable<IAggregatedAlertSnapshot>) this.AggregatedAlerts).Select<IAggregatedAlertSnapshot, JObject>((Func<IAggregatedAlertSnapshot, JObject>) (x => new JObject(new object[3]
        {
          (object) new JProperty("alert", (object) x.Alert.Title),
          (object) new JProperty("helpTopic", (object) x.Alert.HelpTopic),
          (object) new JProperty("count", (object) x.Count)
        }))))),
        (object) new JProperty("aggregatedEntities", (object) new JArray((object) this.AggregatedEntities.Select<KeyValuePair<string, TotalAndAverage>, JObject>((Func<KeyValuePair<string, TotalAndAverage>, JObject>) (x => new JObject(new object[3]
        {
          (object) new JProperty("name", (object) x.Key),
          (object) new JProperty("total", (object) x.Value.Total),
          (object) new JProperty("averagePerSession", (object) Math.Round(x.Value.Average, 2))
        }))))),
        (object) new JProperty("averageEntitiesLoadedPerSession", (object) this.AverageEntitiesLoadedPerSession),
        (object) new JProperty("averageQueryRowCount", (object) Math.Round(this.AverageQueryRowCount, 2)),
        (object) new JProperty("averageStatementsPerSession", (object) Math.Round(this.AverageStatementsPerSession, 2)),
        (object) new JProperty("countOfSessions", (object) this.CountOfSessions),
        (object) new JProperty("numberOfAlerts", (object) this.NumberOfAlerts),
        (object) new JProperty("numberOfEntitiesTypes", (object) this.NumberOfEntitiesTypes),
        (object) new JProperty("numberOfTransactionCommits", (object) this.NumberOfTransactionCommits),
        (object) new JProperty("numberOfTransactionRollbacks", (object) this.NumberOfTransactionRollbacks),
        (object) new JProperty("numberOfTransactions", (object) this.NumberOfTransactions)
      });
    }

    public void CancelSorting(MouseButtonEventArgs e)
    {
      this.AggregatedAlertsCollectionView.SortDescriptions.Clear();
      e.Handled = true;
    }
  }
}
