﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Reports.UniqueQueriesView
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.BackEnd.Profiles;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace HibernatingRhinos.Profiler.Client.Reports
{
  public partial class UniqueQueriesView : UserControl, IComponentConnector
  {
    //internal DataGrid queriesGrid;
    //private bool _contentLoaded;

    public UniqueQueriesView()
    {
      this.InitializeComponent();
      if (Profile.Current.Supports(SupportedFeatures.Duration))
        return;
      DataGridColumn dataGridColumn = this.queriesGrid.Columns.FirstOrDefault<DataGridColumn>((Func<DataGridColumn, bool>) (x => x.Header.Equals((object) "Avg. Duration")));
      if (dataGridColumn == null)
        return;
      this.queriesGrid.Columns.Remove(dataGridColumn);
    }

    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //public void InitializeComponent()
    //{
    //  if (this._contentLoaded)
    //    return;
    //  this._contentLoaded = true;
    //  Application.LoadComponent((object) this, new Uri("/EFProf;component/reports/uniquequeriesview.xaml", UriKind.Relative));
    //}

    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //internal Delegate _CreateDelegate(Type delegateType, string handler)
    //{
    //  return Delegate.CreateDelegate(delegateType, (object) this, handler);
    //}

    //[EditorBrowsable(EditorBrowsableState.Never)]
    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //void IComponentConnector.Connect(int connectionId, object target)
    //{
    //  if (connectionId == 1)
    //    this.queriesGrid = (DataGrid) target;
    //  else
    //    this._contentLoaded = true;
    //}
  }
}
