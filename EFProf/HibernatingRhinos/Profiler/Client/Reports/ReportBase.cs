﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Reports.ReportBase
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using HibernatingRhinos.Profiler.BackEnd.Extensions;
using HibernatingRhinos.Profiler.BackEnd.Profiles;
using HibernatingRhinos.Profiler.Client.Infrastructure;
using HibernatingRhinos.Profiler.Client.Tracking;
using Newtonsoft.Json.Linq;
using System;
using System.ComponentModel;
using System.IO;
using System.Xml.Linq;

namespace HibernatingRhinos.Profiler.Client.Reports
{
  [Serializable]
  public abstract class ReportBase : PollingScreen, IReport, IScreen, IHaveDisplayName, IActivate, IDeactivate, IGuardClose, IClose, INotifyPropertyChangedEx, INotifyPropertyChanged, IPoller
  {
    protected readonly ITrackingService trackingService;

    public static XNamespace Namespace
    {
      get
      {
        return XNamespace.Get("http://reports.hibernatingrhinos.com/" + Profile.CurrentProfile + ".Profiler/2009/10");
      }
    }

    protected ReportBase(ITrackingService trackingService)
    {
      this.trackingService = trackingService;
    }

    public abstract string JsonReportName { get; }

    public virtual bool FilteringSupported
    {
      get
      {
        return false;
      }
    }

    public abstract void Clear();

    public abstract XElement ExportReport();

    protected override void OnActivate()
    {
      this.trackingService.Track("Reports", this.GetType().Name.MakeStatementFromPascalCase(), (string) null, new int?());
      base.OnActivate();
    }

    public string ExportReportToJsonString()
    {
      JContainer json = this.ExportReportToJson();
      StringWriter stringWriter = new StringWriter();
      json.WriteJson((TextWriter) stringWriter);
      return stringWriter.GetStringBuilder().ToString();
    }

    [CLSCompliant(false)]
    protected virtual JContainer ExportReportToJson()
    {
      throw new NotSupportedException("Exporting to JSON is not supported by this report");
    }

    public void ExportJson(string file)
    {
      using (StreamWriter text = File.CreateText(file))
        this.ExportReportToJson().WriteJson((TextWriter) text);
    }

    public void ExportXml(string file)
    {
      new XDocument(new object[1]
      {
        (object) this.ExportReport()
      }).Save(file);
    }
  }
}
