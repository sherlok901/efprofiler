﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Licensing.LicenseTrialRegistrationModel
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using HibernatingRhinos.Profiler.BackEnd.Profiles;
using HibernatingRhinos.Profiler.Client.CaliburnInfrastructure;
using HibernatingRhinos.Profiler.Client.Infrastructure;
using HibernatingRhinos.Profiler.Client.Services;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;
using System.Net;
using System.Text.RegularExpressions;

namespace HibernatingRhinos.Profiler.Client.Licensing
{
  public class LicenseTrialRegistrationModel : ProfilerScreen
  {
    private readonly IWindowManager windowManager;
    private readonly ILog logger;
    private string company;
    private readonly ValidatableProperty<string> fullNameProperty;
    private ValidatablePropertyCollection validatableProperties;
    private readonly ValidatableProperty<string> emailProperty;
    private bool isRequestTrialExtend;

    public bool IsExtendMode { get; set; }

    public event LicenseTrialRegistrationModel.AdditionalMessage OnAdditionalMessage;

    public string TrailTicketText { get; set; }

    public LicenseTrialRegistrationModel(IWindowManager windowManager)
    {
      ValidatableProperty<string> validatableProperty1 = new ValidatableProperty<string>("Full Name");
      validatableProperty1.Validators.Add((ValidationAttribute) new RequiredAttribute());
      validatableProperty1.InitialValue = (object) "";
      this.fullNameProperty = validatableProperty1;
      ValidatableProperty<string> validatableProperty2 = new ValidatableProperty<string>(nameof (Email));
      validatableProperty2.Validators.Add((ValidationAttribute) new RequiredAttribute());
      validatableProperty2.Validators.Add((ValidationAttribute) new EmailAddressAttribute());
      validatableProperty2.InitialValue = (object) "";
      this.emailProperty = validatableProperty2;
      // ISSUE: explicit constructor call
      //base.\u002Ector();
      
      this.windowManager = windowManager;
      this.TrailTicketText = InitialTrialService.GetTrialTicketText();
      this.fullNameProperty.InitialValue = (object) this.GetTrailTicketProrerty(nameof (FullName));
      this.emailProperty.InitialValue = (object) this.GetTrailTicketProrerty(nameof (Email));
      this.Company = this.GetTrailTicketProrerty(nameof (Company));
      ValidatablePropertyCollection propertyCollection = new ValidatablePropertyCollection();
      propertyCollection.Add((IValidatableProperty) this.fullNameProperty);
      propertyCollection.Add((IValidatableProperty) this.emailProperty);
      this.validatableProperties = propertyCollection;
    }

    private string GetTrailTicketProrerty(string property)
    {
      if (string.IsNullOrEmpty(this.TrailTicketText))
        return "";
      try
      {
        Match match = new Regex(string.Format("{0}=(\\S+);", (object) property)).Match(this.TrailTicketText);
        if (match.Success)
          return match.Groups[1].Value;
      }
      catch
      {
      }
      return "";
    }

    public ValidatableProperty<string> FullName
    {
      get
      {
        return this.fullNameProperty;
      }
    }

    public ValidatableProperty<string> Email
    {
      get
      {
        return this.emailProperty;
      }
    }

    public string Company
    {
      get
      {
        return this.company;
      }
      set
      {
        this.company = value;
        this.NotifyOfPropertyChange<string>((Expression<Func<string>>) (() => this.Company));
      }
    }

    public bool IsValid
    {
      get
      {
        return this.validatableProperties.IsValid;
      }
    }

    public string TrialUrl
    {
      get
      {
        return Profile.Current.TrialUrl;
      }
    }

    public string TrialExtendedUrl
    {
      get
      {
        return Profile.Current.TrialExtendedUrl;
      }
    }

    public string SiteTrialUrl
    {
      get
      {
        return Profile.Current.SiteTrialUrl;
      }
    }

    public bool RequestSent { get; private set; }

    public bool IsRequestTrialExtend
    {
      get
      {
        return this.isRequestTrialExtend;
      }
      set
      {
        this.isRequestTrialExtend = value;
        this.DisplayName = this.isRequestTrialExtend ? "Extend trial license" : "Get a trial license";
        this.StartTrailToolTip = this.isRequestTrialExtend ? "Extend license for 15 days." : "Get a 30 days license.";
        this.Alternative = this.isRequestTrialExtend ? "Alternative way to extend trial" : "Alternative way to request the trial";
        this.NotifyOfPropertyChange<string>((Expression<Func<string>>) (() => this.DisplayName));
        this.NotifyOfPropertyChange<string>((Expression<Func<string>>) (() => this.StartTrailToolTip));
        this.NotifyOfPropertyChange<string>((Expression<Func<string>>) (() => this.Alternative));
      }
    }

    public string StartTrailToolTip { get; set; }

    public string Alternative { get; set; }

    public void OnSendRegistrationRequest()
    {
      if (!this.IsValid)
      {
        this.validatableProperties.MarkAllAsDirty();
      }
      else
      {
        InitialTrialService.StartTrial(this.FullName.ValidatedValue, this.Email.ValidatedValue, this.Company);
        this.SendRegistrationRequest(InitialTrialService.GetTrialTicket(), false);
        this.RequestSent = true;
        this.TryClose();
      }
    }

    public void SendRegistrationRequest(Guid? trialTicket, bool extended = false)
    {
      try
      {
        string str = string.Format("trial.fullname={0}&trial.email={1}&trial.company={2}", (object) this.FullName.ValidatedValue, (object) this.Email.ValidatedValue, (object) this.Company);
        string uriString = extended ? this.TrialExtendedUrl : this.TrialUrl;
        HttpWebRequest http = WebRequest.CreateHttp(new Uri(uriString + "?" + str));
        http.Method = "POST";
        CookieCollection cookies = new CookieCollection()
        {
          new Cookie("tracking-id", trialTicket.ToString())
        };
        http.CookieContainer = new CookieContainer();
        http.CookieContainer.Add(new Uri(uriString), cookies);
        http.BeginGetRequestStream(new AsyncCallback(this.WebRequestCallback), (object) http);
      }
      catch (Exception ex)
      {
        this.logger.Warn("Could not start trial. Error: " + (object) ex);
        throw;
      }
    }

    private void WebRequestCallback(IAsyncResult result)
    {
      HttpWebRequest asyncState = (HttpWebRequest) result.AsyncState;
      asyncState.BeginGetResponse(new AsyncCallback(this.Response), (object) asyncState);
    }

    private void Response(IAsyncResult result)
    {
      HttpWebRequest asyncState = (HttpWebRequest) result.AsyncState;
      HttpWebResponse response;
      try
      {
        response = (HttpWebResponse) asyncState.EndGetResponse(result);
      }
      catch (WebException ex)
      {
        response = (HttpWebResponse) ex.Response;
        int statusCode = (int) response.StatusCode;
        if (response.StatusCode == HttpStatusCode.Conflict)
        {
          this.SendRegistrationRequest(InitialTrialService.GetTrialTicket(), true);
          return;
        }
      }
      ((System.Action) (() =>
      {
        if (response.StatusCode == HttpStatusCode.MethodNotAllowed)
        {
          string msg = "You have already requested a trial license twice and it expired on " + response.StatusDescription;
          if (this.OnAdditionalMessage == null)
            return;
          this.OnAdditionalMessage(msg, false);
        }
        else if (response.StatusCode != HttpStatusCode.OK)
        {
          string msg = "Failed to connect to trial server, trial was not started." + Environment.NewLine + string.Format("(Response code was: {0})", (object) response.StatusDescription);
          if (this.OnAdditionalMessage == null)
            return;
          this.OnAdditionalMessage(msg, false);
        }
        else
        {
          if (response.StatusCode != HttpStatusCode.OK)
            return;
          string msg = "We have registered you for a trial license. Please check your email for the provided license and attach it here.";
          if (this.OnAdditionalMessage == null)
            return;
          this.OnAdditionalMessage(msg, false);
        }
      })).OnUIThread();
    }

    public delegate void AdditionalMessage(string msg, bool ext);
  }
}
