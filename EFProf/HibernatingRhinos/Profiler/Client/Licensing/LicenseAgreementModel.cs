﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Licensing.LicenseAgreementModel
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.Client.Infrastructure;
using HibernatingRhinos.Profiler.Client.Settings;
using HibernatingRhinos.Profiler.Client.Shell;
using System;
using System.IO;

namespace HibernatingRhinos.Profiler.Client.Licensing
{
  public class LicenseAgreementModel : ProfilerScreen
  {
    private const string EulaAgreementFileName = "agreedToEula.txt";
    private bool accepted;

    public LicenseAgreementModel()
    {
      this.DisplayName = "License Agreement";
    }

    public string LicenseText
    {
      get
      {
        using (FileStream fileStream = File.OpenRead(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "License.txt")))
        {
          using (StreamReader streamReader = new StreamReader((Stream) fileStream))
            return streamReader.ReadToEnd();
        }
      }
    }

    public void AcceptAgreement()
    {
      using (StreamWriter text = File.CreateText(Path.Combine(UserPreferencesHolder.SettingsDirectory, "agreedToEula.txt")))
        text.WriteLine("EULA Agreement accepted on {0}.", (object) DateTime.Now);
      this.accepted = true;
      this.TryClose(new bool?(true));
    }

    public static bool HasAgreementBeenAccepted()
    {
      return File.Exists(Path.Combine(UserPreferencesHolder.SettingsDirectory, "agreedToEula.txt"));
    }

    protected override void OnDeactivate(bool close)
    {
      base.OnDeactivate(close);
      if (this.accepted)
        return;
      ApplicationTask.Exit();
    }
  }
}
