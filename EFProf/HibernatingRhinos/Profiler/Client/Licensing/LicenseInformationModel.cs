﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Licensing.LicenseInformationModel
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using HibernatingRhinos.Profiler.BackEnd.Profiles;
using HibernatingRhinos.Profiler.Client.Controls;
using HibernatingRhinos.Profiler.Client.Services;
using HibernatingRhinos.Profiler.Client.Shell;
using Rhino.Licensing;
using Rhino.Licensing.Discovery;
using System;
using System.Windows;

namespace HibernatingRhinos.Profiler.Client.Licensing
{
  public class LicenseInformationModel : PropertyChangedBase
  {
    private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(typeof (LicensePromptModel));
    public ProfilerLicenseValidator licenseValidator;
    private int? daysLeftInLicense;
    private bool isRegistered;
    private string licensedTo;

    public LicenseInformationModel()
    {
      this.licenseValidator = LicensePromptModel.CreateLicenseValidator(LicensingDescription.GetLocalLicenseFile());
      this.licenseValidator.LicenseInvalidated += new System.Action<InvalidationType>(this.LicenseValidatorOnLicenseInvalidated);
      this.licenseValidator.MultipleLicensesWereDiscovered += new EventHandler<DiscoveryHost.ClientDiscoveredEventArgs>(this.LicenseValidatorOnMultipleLicensesWereDiscovered);
    }

    private void LicenseValidatorOnLicenseInvalidated(InvalidationType invalidationType)
    {
      LicenseInformationModel.Logger.InfoFormat("Got invalidation notification " + (object) invalidationType);
      ((System.Action) (() =>
      {
        LicensePromptModel licensePromptModel = IoC.Get<LicensePromptModel>((string) null);
        licensePromptModel.Prompt = invalidationType != InvalidationType.CannotGetNewLicense ? "The license has expired, cannot continue working with an expired license." + Environment.NewLine + "Add a valid license to continue working with the profiler." : "Could not renew license lease, it is likely that the licensing service has run out of licenses." + Environment.NewLine + "Consider buying more licenses.";
        if (Dialog.Show((object) licensePromptModel, (object) null, true))
          return;
        Environment.Exit(-1337);
      })).OnUIThread();
    }

    private void LicenseValidatorOnMultipleLicensesWereDiscovered(object sender, DiscoveryHost.ClientDiscoveredEventArgs args)
    {
      LicenseInformationModel.Logger.InfoFormat("Duplicate license discovered. Show an error dialog.");
      ((System.Action) (() =>
      {
        if (LicenseInformationModel.PromptUserForLicenseFile(string.Format("A duplicate license was found at {0} for user {1}. You cannot use the same license on two machines concurrently. Each user has to have his own license!", (object) args.MachineName, (object) args.UserName) + Environment.NewLine + "Consider buying more licenses.", false, false))
          return;
        Environment.Exit(-1337);
      })).OnUIThread();
    }

    public static bool PromptUserForLicenseFile(string reason, bool isCanRequestTrial, bool isCanRequestTrialExtend = false)
    {
      LicensePromptModel licensePromptModel = IoC.Get<LicensePromptModel>((string) null);
      licensePromptModel.Prompt = reason;
      licensePromptModel.IsCanRequestTrial = isCanRequestTrial;
      licensePromptModel.IsCanRequestTrialExtendAndBuy = isCanRequestTrialExtend;
      bool flag = Dialog.Show((object) licensePromptModel, (object) null, true);
      if (flag)
        IoC.Get<ApplicationModel>((string) null).LicenseInfo.UpdateLicenseInfo();
      return flag;
    }

    public string LicensedTo
    {
      get
      {
        return this.licensedTo;
      }
      set
      {
        this.licensedTo = value;
        this.NotifyOfPropertyChange<string>((System.Linq.Expressions.Expression<Func<string>>) (() => this.LicensedTo));
      }
    }

    public int? DaysLeftInLicense
    {
      get
      {
        return this.daysLeftInLicense;
      }
      set
      {
        this.daysLeftInLicense = value;
        this.NotifyOfPropertyChange<int?>((System.Linq.Expressions.Expression<Func<int?>>) (() => this.DaysLeftInLicense));
      }
    }

    public bool IsRegistered
    {
      get
      {
        return this.isRegistered;
      }
      set
      {
        this.isRegistered = value;
        this.NotifyOfPropertyChange<bool>((System.Linq.Expressions.Expression<Func<bool>>) (() => this.IsRegistered));
      }
    }

    public void UpdateLicenseInfo()
    {
      this.IsRegistered = this.licenseValidator.LicenseType != LicenseType.Trial;
      this.LicensedTo = this.isRegistered ? this.licenseValidator.Name : (string) null;
      if (this.licenseValidator.LicenseType == LicenseType.Floating && this.licenseValidator.ExpirationDateFloatingLicense != DateTime.MaxValue && this.licenseValidator.ExpirationDateFloatingLicense != DateTime.MinValue)
      {
        this.DaysLeftInLicense = new int?((int) Math.Round((this.licenseValidator.ExpirationDateFloatingLicense - DateTime.Now).TotalDays + 1.0));
      }
      else
      {
        if (!(this.licenseValidator.ExpirationDate != DateTime.MaxValue) || !(this.licenseValidator.ExpirationDate != DateTime.MinValue))
          return;
        this.DaysLeftInLicense = new int?((int) Math.Round((this.licenseValidator.ExpirationDate - DateTime.Now).TotalDays + 1.0));
      }
    }

    public void RegisterLicense()
    {
      LicensePromptModel licensePromptModel = IoC.Get<LicensePromptModel>((string) null);
      licensePromptModel.Prompt = "Please select a new license.";
      licensePromptModel.IsCanRequestTrial = false;
      licensePromptModel.IsUsingTrialLicense = true;
      if (!Dialog.Show((object) licensePromptModel, (object) null, true))
        return;
      this.UpdateValidator();
      this.UpdateLicenseInfo();
    }

    public void DeactivateLicense()
    {
      this.LicensedTo = (string) null;
      this.DaysLeftInLicense = new int?();
      this.IsRegistered = false;
      this.licenseValidator.RemoveExistingLicense();
      if (LicenseInformationModel.PromptUserForLicenseFile("To continue using " + Profile.CurrentProfileDisplayName + ", you must provide another license.", false, false))
      {
        this.UpdateValidator();
        this.UpdateLicenseInfo();
      }
      else
      {
        int num = (int) MessageBox.Show("Since a new license was not provided, the application will now exit.", "No license was provided", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        Environment.Exit(-1337);
      }
    }

    private void UpdateValidator()
    {
      this.licenseValidator = new ProfilerLicenseValidator(LicensingDescription.PublicKey, LicensingDescription.GetLocalLicenseFile());
      this.licenseValidator.AssertValidLicense();
    }

    public bool ConfirmLicense(bool showDialogIfLicenseDoesNotExists)
    {
      try
      {
        this.licenseValidator.AssertValidLicense();
        return true;
      }
      catch (FloatingLicenseNotAvialableException ex)
      {
        if (!showDialogIfLicenseDoesNotExists)
          return false;
        if (!LicenseInformationModel.PromptUserForLicenseFile(string.Format("There are no free floating licenses available on the licensing server.{0}Reduce the number of concurrent users of the {0}profiler or purchase more licenses.", (object) Environment.NewLine), false, false))
          return false;
      }
      catch (LicenseNotFoundException ex)
      {
        if (!showDialogIfLicenseDoesNotExists)
          return false;
        string reason = "The installed license for " + Profile.CurrentProfileDisplayName + " is not valid.";
        if (this.licenseValidator.LicenseType == LicenseType.Trial)
          reason += " The trial period has expired!";
        if (!LicenseInformationModel.PromptUserForLicenseFile(reason, false, false))
          return false;
      }
      catch (LicenseFileNotFoundException ex)
      {
        if (!showDialogIfLicenseDoesNotExists)
          return false;
        if (!LicenseInformationModel.PromptUserForLicenseFile(Profile.CurrentProfileDisplayName + " requires a valid license.", true, false))
          return false;
      }
      catch (LicenseUpgradeRequiredException ex)
      {
        if (!LicenseInformationModel.PromptUserForLicenseFile(ex.Message, false, false))
          return false;
      }
      catch (LicenseExpiredException ex)
      {
        bool isCanRequestTrialExtend = ex.Message.StartsWith("Expiration Date:") && this.licenseValidator.LicenseType == LicenseType.Trial;
        if (!LicenseInformationModel.PromptUserForLicenseFile(ex.Message, false, isCanRequestTrialExtend))
          return false;
      }
      catch (RhinoLicensingException ex)
      {
        if (!LicenseInformationModel.PromptUserForLicenseFile(ex.Message, false, false))
          return false;
      }
      catch (InvalidOperationException ex)
      {
        if (!LicenseInformationModel.PromptUserForLicenseFile(ex.Message, false, false))
          return false;
      }
      try
      {
        if (LicenseInformationModel.HandleInitialTrialMode())
          return true;
        this.licenseValidator.AssertValidLicense();
        return true;
      }
      catch (Exception ex)
      {
        LicenseInformationModel.Logger.Warn((object) "Second chance license validation has failed", ex);
        return false;
      }
    }

    private static bool HandleInitialTrialMode()
    {
      bool flag = InitialTrialService.IsInInitialTrialMode();
      if (flag)
        InitialTrialService.StartTrialBackgroundTimer();
      return flag;
    }
  }
}
