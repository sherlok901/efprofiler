﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Licensing.LicensePromptModel
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using HibernatingRhinos.Profiler.BackEnd.Profiles;
using HibernatingRhinos.Profiler.Client.CaliburnInfrastructure;
using HibernatingRhinos.Profiler.Client.Infrastructure;
using HibernatingRhinos.Profiler.Client.Services;
using HibernatingRhinos.Profiler.Client.Settings;
using HibernatingRhinos.Profiler.Client.Shell;
using Microsoft.Win32;
using Rhino.Licensing;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Windows;

namespace HibernatingRhinos.Profiler.Client.Licensing
{
  public class LicensePromptModel : ProfilerScreen
  {
    private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(typeof (LicensePromptModel));
    private bool isCanRequestTrialExtend = true;
    private readonly ProfilerWindowManager windowManager;
    private bool isValidLicense;
    private string additionalMessage;
    private string validationResult;
    private bool isCanRequestTrial;
    private bool isCanRequestTrialExtendAndBuy;

    public LicensePromptModel(ProfilerWindowManager windowManager)
    {
      this.windowManager = windowManager;
      this.DisplayName = "You need a license!";
    }

    public string Prompt { get; set; }

    public bool IsUsingTrialLicense { get; set; }

    public string AdditionalMessage
    {
      get
      {
        return this.additionalMessage;
      }
      private set
      {
        this.additionalMessage = value;
        this.NotifyOfPropertyChange<string>((System.Linq.Expressions.Expression<Func<string>>) (() => this.AdditionalMessage));
      }
    }

    public string ValidationResult
    {
      get
      {
        return this.validationResult;
      }
      private set
      {
        this.validationResult = value;
        this.NotifyOfPropertyChange<string>((System.Linq.Expressions.Expression<Func<string>>) (() => this.ValidationResult));
      }
    }

    public bool IsCanRequestTrial
    {
      get
      {
        return this.isCanRequestTrial;
      }
      set
      {
        this.isCanRequestTrial = value;
        this.NotifyOfPropertyChange<bool>((System.Linq.Expressions.Expression<Func<bool>>) (() => this.IsCanRequestTrial));
      }
    }

    public bool IsCanRequestTrialExtendAndBuy
    {
      get
      {
        return this.isCanRequestTrialExtendAndBuy;
      }
      set
      {
        this.isCanRequestTrialExtendAndBuy = value;
        this.NotifyOfPropertyChange<bool>((System.Linq.Expressions.Expression<Func<bool>>) (() => this.IsCanRequestTrialExtendAndBuy));
      }
    }

    public bool IsCanRequestTrialExtend
    {
      get
      {
        return this.isCanRequestTrialExtend;
      }
      set
      {
        this.isCanRequestTrialExtend = value;
        this.NotifyOfPropertyChange<bool>((System.Linq.Expressions.Expression<Func<bool>>) (() => this.IsCanRequestTrialExtend));
      }
    }

    private bool AttemptToGetLicenseFile()
    {
      this.ValidationResult = (string) null;
      OpenFileDialog openFileDialog1 = new OpenFileDialog();
      openFileDialog1.Filter = "License Files (*.xml)|*.xml|All files (*.*)|*.*";
      openFileDialog1.Title = "Locate Your License";
      openFileDialog1.RestoreDirectory = true;
      OpenFileDialog openFileDialog2 = openFileDialog1;
      bool? nullable = this.windowManager.ShowFileDialog((FileDialog) openFileDialog2);
      if ((!nullable.GetValueOrDefault() ? 1 : (!nullable.HasValue ? 1 : 0)) != 0)
        return false;
      string fileName = openFileDialog2.FileName;
      try
      {
        ProfilerLicenseValidator licenseValidator = LicensePromptModel.CreateLicenseValidator(fileName);
        licenseValidator.DisableFutureChecks();
        try
        {
          licenseValidator.AssertValidLicense();
        }
        catch (NotValidLicenseFileException ex)
        {
          this.ValidationResult = ex.Message;
          return false;
        }
        catch (LicenseNotFoundException ex)
        {
          string str = "The file you selected is not a valid license.";
          if (licenseValidator.LicenseType == LicenseType.Trial)
            str = str + Environment.NewLine + "The trial period has expired!";
          this.ValidationResult = str;
          return false;
        }
        catch (Exception ex)
        {
          this.ValidationResult = "The file you selected is not a valid license." + Environment.NewLine + ex.Message;
          return false;
        }
        try
        {
          string fullPath1 = Path.GetFullPath(fileName);
          string localLicenseFile = LicensingDescription.GetLocalLicenseFile();
          string fullPath2 = Path.GetFullPath(localLicenseFile);
          if (!fullPath1.Equals(fullPath2, StringComparison.InvariantCultureIgnoreCase))
            File.Copy(fileName, localLicenseFile, true);
        }
        catch (Exception ex)
        {
          this.ValidationResult = "The file you selected is a valid license but the profiler could not copy it to its permanent location because: " + Environment.NewLine + ex.Message;
          return false;
        }
        return true;
      }
      catch (UnauthorizedAccessException ex)
      {
        if (MessageBox.Show("Could not save license file:" + Environment.NewLine + ex.Message + Environment.NewLine + "Would you like to see the FAQ entry for this error?", Profile.CurrentProfileDisplayName, MessageBoxButton.YesNo) == MessageBoxResult.Yes)
          BrowserLauncher.Url(Profile.Current.GetLearnTopic("faq", "UnlockInternetPermissions"), "Failed to open the browser.");
        return false;
      }
    }

    public void GetLicenseFile()
    {
      this.isValidLicense = this.AttemptToGetLicenseFile();
      if (!this.isValidLicense)
        return;
      this.TryClose(new bool?(true));
    }

    internal static ProfilerLicenseValidator CreateLicenseValidator(string licenseFile)
    {
      string licenseServerUrl = (string) null;
      try
      {
        licenseServerUrl = ConfigurationManager.AppSettings["licenseServer"];
        if (!string.IsNullOrEmpty(licenseServerUrl))
          LicensePromptModel.Logger.Info((object) ("Using floating license server: " + licenseServerUrl));
      }
      catch (Exception ex)
      {
        LicensePromptModel.Logger.Error((object) "Error when trying to get 'licenseServer' from configuration", ex);
      }
      return LicensePromptModel.CreateLicenseValidator(licenseServerUrl, licenseFile);
    }

    private static ProfilerLicenseValidator CreateLicenseValidator(string licenseServerUrl, string licenseFile)
    {
      if (string.IsNullOrEmpty(licenseServerUrl))
        return new ProfilerLicenseValidator(LicensingDescription.PublicKey, licenseFile);
      return new ProfilerLicenseValidator(LicensingDescription.PublicKey, licenseFile, licenseServerUrl, UserPreferencesHolder.UserSettings.UserId);
    }

    public void ShowTrialRegistrationWindows()
    {
      LicenseTrialRegistrationModel registrationModel = IoC.Get<LicenseTrialRegistrationModel>((string) null);
      string additionalMsg = "Please Wait...";
      registrationModel.IsRequestTrialExtend = this.IsCanRequestTrialExtendAndBuy;
      registrationModel.OnAdditionalMessage += (LicenseTrialRegistrationModel.AdditionalMessage) ((msg, ext) =>
      {
        this.IsCanRequestTrialExtend = ext;
        this.AdditionalMessage = msg;
        additionalMsg = msg;
      });
      this.windowManager.ShowDialog((object) registrationModel, (object) null, (IDictionary<string, object>) null);
      if (!registrationModel.RequestSent)
        return;
      if (InitialTrialService.IsInInitialTrialMode() && !this.IsCanRequestTrialExtendAndBuy && !this.IsCanRequestTrial)
      {
        this.TryClose(new bool?(true));
      }
      else
      {
        this.AdditionalMessage = additionalMsg;
        this.IsCanRequestTrial = false;
      }
    }

    public string BuyLicensePageUrl
    {
      get
      {
        return Profile.Current.BuyUrl;
      }
    }

    protected override void OnDeactivate(bool close)
    {
      base.OnDeactivate(close);
      if (this.IsUsingTrialLicense || InitialTrialService.IsInInitialTrialMode() || this.isValidLicense)
        return;
      ApplicationTask.Exit();
    }
  }
}
