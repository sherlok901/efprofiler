﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.CaliburnInfrastructure.BindableEnum
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace HibernatingRhinos.Profiler.Client.CaliburnInfrastructure
{
  public class BindableEnum
  {
    public int UnderlyingValue { get; set; }

    public string DisplayName { get; set; }

    public object Value { get; set; }

    public override string ToString()
    {
      return this.DisplayName;
    }

    public override bool Equals(object obj)
    {
      BindableEnum bindableEnum = obj as BindableEnum;
      if (bindableEnum != null)
        return this.UnderlyingValue == bindableEnum.UnderlyingValue;
      if (obj is int)
        return this.UnderlyingValue.Equals((int) obj);
      return false;
    }

    public override int GetHashCode()
    {
      return this.UnderlyingValue.GetHashCode();
    }

    public static BindableEnum Create(object value)
    {
      foreach (FieldInfo field in value.GetType().GetFields(BindingFlags.Static | BindingFlags.Public))
      {
        if (field.GetValue((object) null).Equals(value))
          return BindableEnum.Create(field);
      }
      return (BindableEnum) null;
    }

    public static BindableEnum Create(FieldInfo field)
    {
      DescriptionAttribute descriptionAttribute = field.GetAttributes<DescriptionAttribute>(false).FirstOrDefault<DescriptionAttribute>();
      object obj = field.GetValue((object) null);
      return new BindableEnum()
      {
        Value = obj,
        UnderlyingValue = Convert.ToInt32(obj),
        DisplayName = descriptionAttribute != null ? descriptionAttribute.Description : field.Name
      };
    }
  }
}
