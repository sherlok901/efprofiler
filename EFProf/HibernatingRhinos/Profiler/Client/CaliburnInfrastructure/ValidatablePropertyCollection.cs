﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.CaliburnInfrastructure.ValidatablePropertyCollection
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;

namespace HibernatingRhinos.Profiler.Client.CaliburnInfrastructure
{
  public class ValidatablePropertyCollection : ObservableCollection<IValidatableProperty>
  {
    public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;

    protected virtual void OnErrorsChanged(DataErrorsChangedEventArgs e)
    {
      EventHandler<DataErrorsChangedEventArgs> errorsChanged = this.ErrorsChanged;
      if (errorsChanged == null)
        return;
      errorsChanged((object) this, e);
    }

    public bool IsValid
    {
      get
      {
        return this.All<IValidatableProperty>((Func<IValidatableProperty, bool>) (i => i.IsValid));
      }
    }

    public void MarkAllAsDirty()
    {
      foreach (IValidatableProperty validatableProperty in (Collection<IValidatableProperty>) this)
        validatableProperty.MarkAsDirty();
    }

    protected override void InsertItem(int index, IValidatableProperty item)
    {
      base.InsertItem(index, item);
      this.ListenForValidity(item);
    }

    protected override void RemoveItem(int index)
    {
      this.StopListeningForValidity(this[index]);
      base.RemoveItem(index);
    }

    protected override void ClearItems()
    {
      base.ClearItems();
      foreach (IValidatableProperty validatableProperty in (Collection<IValidatableProperty>) this)
        this.StopListeningForValidity(validatableProperty);
    }

    private void StopListeningForValidity(IValidatableProperty item)
    {
      item.ErrorsChanged -= new EventHandler<DataErrorsChangedEventArgs>(this.HandleErrorsChanged);
    }

    private void ListenForValidity(IValidatableProperty item)
    {
      item.ErrorsChanged += new EventHandler<DataErrorsChangedEventArgs>(this.HandleErrorsChanged);
    }

    private void HandleErrorsChanged(object sender, DataErrorsChangedEventArgs e)
    {
      this.OnErrorsChanged(e);
    }
  }
}
