﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.CaliburnInfrastructure.IValidatableProperty
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System;
using System.ComponentModel;

namespace HibernatingRhinos.Profiler.Client.CaliburnInfrastructure
{
  public interface IValidatableProperty
  {
    bool IsValid { get; }

    void MarkAsDirty();

    event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;
  }
}
