﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.CaliburnInfrastructure.ValidatableProperty`1
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;

namespace HibernatingRhinos.Profiler.Client.CaliburnInfrastructure
{
  public class ValidatableProperty<T> : PropertyChangedBase, INotifyDataErrorInfo, IValidatableProperty
  {
    private static readonly DataErrorsChangedEventArgs ChangedEventArgs = new DataErrorsChangedEventArgs(nameof (Value));
    private IList<string> errorMessages = (IList<string>) new string[0];
    private readonly string displayName;
    private object value;
    private object initialValue;
    private T validatedValue;
    private IList<ValidationAttribute> validators;
    private bool isDirty;

    public ValidatableProperty(string displayName)
    {
      if (string.IsNullOrEmpty(displayName))
        throw new ArgumentException("displayName cannot be null or empty");
      this.displayName = displayName;
    }

    public IList<ValidationAttribute> Validators
    {
      get
      {
        return this.validators ?? (this.validators = (IList<ValidationAttribute>) new List<ValidationAttribute>());
      }
    }

    public object Value
    {
      get
      {
        return this.value;
      }
      set
      {
        this.isDirty = true;
        if (object.Equals(value, this.value))
          return;
        this.value = value;
        this.NotifyOfPropertyChange(nameof (Value));
        this.ValidateValue();
      }
    }

    public object InitialValue
    {
      get
      {
        return this.initialValue;
      }
      set
      {
        this.initialValue = value;
        this.value = value;
        this.isDirty = false;
        this.NotifyOfPropertyChange(nameof (InitialValue));
        this.NotifyOfPropertyChange<object>((Expression<Func<object>>) (() => this.Value));
        this.ValidateValue();
      }
    }

    public T ValidatedValue
    {
      get
      {
        return this.validatedValue;
      }
      private set
      {
        if (object.Equals((object) value, (object) this.validatedValue))
          return;
        this.validatedValue = value;
        this.NotifyOfPropertyChange(nameof (ValidatedValue));
      }
    }

    public void MarkAsDirty()
    {
      this.isDirty = true;
      this.OnErrorsChanged();
    }

    public IEnumerable GetErrors(string propertyName)
    {
      if (!this.isDirty)
        return (IEnumerable) new string[0];
      return (IEnumerable) this.errorMessages;
    }

    private void ValidateValue()
    {
      List<ValidationResult> source = new List<ValidationResult>();
      try
      {
        T obj = (T) Convert.ChangeType(this.value, typeof (T));
        if (Validator.TryValidateValue((object) obj, new ValidationContext(this.Value)
        {
          DisplayName = this.displayName
        }, (ICollection<ValidationResult>) source, (IEnumerable<ValidationAttribute>) this.Validators))
          this.ValidatedValue = obj;
      }
      catch (FormatException ex)
      {
        source.Add(new ValidationResult(string.Format("Value '{0}' is not valid for this type of input", (object) this.value.ToString())));
      }
      List<string> list = source.Select<ValidationResult, string>((Func<ValidationResult, string>) (r => r.ErrorMessage)).ToList<string>();
      bool flag = !this.errorMessages.SequenceEqual<string>((IEnumerable<string>) list);
      this.errorMessages = (IList<string>) list;
      if (!flag)
        return;
      this.OnErrorsChanged();
    }

    public bool HasErrors
    {
      get
      {
        if (this.isDirty)
          return !this.IsValid;
        return false;
      }
    }

    public bool IsValid
    {
      get
      {
        return this.errorMessages.Count == 0;
      }
    }

    public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;

    protected virtual void OnErrorsChanged()
    {
      EventHandler<DataErrorsChangedEventArgs> errorsChanged = this.ErrorsChanged;
      if (errorsChanged == null)
        return;
      errorsChanged((object) this, ValidatableProperty<T>.ChangedEventArgs);
    }
  }
}
