﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.CaliburnInfrastructure.ViewAttribute
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows;

namespace HibernatingRhinos.Profiler.Client.CaliburnInfrastructure
{
  [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Enum, AllowMultiple = true)]
  public class ViewAttribute : Attribute, IViewStrategy
  {
    private static readonly ILog Log = LogManager.GetLog(typeof (ViewAttribute));
    private readonly Type key;

    public ViewAttribute(Type key)
    {
      this.key = key;
    }

    public Type Key
    {
      get
      {
        return this.key;
      }
    }

    public object Context { get; set; }

    public bool Matches(object context)
    {
      if (this.Context == null)
        return context == null;
      return this.Context.Equals(context);
    }

    public UIElement Locate(Type modelType, DependencyObject displayLocation, object context)
    {
      try
      {
        Type type1 = AssemblySource.Instance.SelectMany((Func<Assembly, IEnumerable<Type>>) (assmebly => (IEnumerable<Type>) assmebly.GetExportedTypes()), (assmebly, type) => new
        {
          assmebly = assmebly,
          type = type
        }).Where(_param1 => _param1.type == this.key).Select(_param0 => _param0.type).FirstOrDefault<Type>();
        UIElement uiElement = ViewLocator.GetOrCreateViewType(type1);
        ViewAttribute.Log.Info("Custom view {0} located for {1}.", (object) uiElement, (object) modelType);
        return uiElement;
      }
      catch (Exception ex)
      {
        ViewAttribute.Log.Warn("Failed to locate a view for {0}. Exception: {1}", (object) modelType, (object) ex);
        throw;
      }
    }
  }
}
