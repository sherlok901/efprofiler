﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.CaliburnInfrastructure.KeyTrigger
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System.Windows;
using System.Windows.Input;

namespace HibernatingRhinos.Profiler.Client.CaliburnInfrastructure
{
  public class KeyTrigger : System.Windows.Interactivity.TriggerBase<UIElement>
  {
    public static readonly DependencyProperty KeyProperty = DependencyProperty.Register(nameof (Key), typeof (Key), typeof (KeyTrigger), (PropertyMetadata) null);
    public static readonly DependencyProperty ModifiersProperty = DependencyProperty.Register(nameof (Modifiers), typeof (ModifierKeys), typeof (KeyTrigger), (PropertyMetadata) null);

    public Key Key
    {
      get
      {
        return (Key) this.GetValue(KeyTrigger.KeyProperty);
      }
      set
      {
        this.SetValue(KeyTrigger.KeyProperty, (object) value);
      }
    }

    public ModifierKeys Modifiers
    {
      get
      {
        return (ModifierKeys) this.GetValue(KeyTrigger.ModifiersProperty);
      }
      set
      {
        this.SetValue(KeyTrigger.ModifiersProperty, (object) value);
      }
    }

    protected override void OnAttached()
    {
      base.OnAttached();
      this.AssociatedObject.KeyDown += new KeyEventHandler(this.AssociatedObjectKeyDown);
    }

    protected override void OnDetaching()
    {
      base.OnDetaching();
      this.AssociatedObject.KeyDown -= new KeyEventHandler(this.AssociatedObjectKeyDown);
    }

    private void AssociatedObjectKeyDown(object sender, KeyEventArgs e)
    {
      if (e.Key != this.Key || Keyboard.Modifiers != KeyTrigger.GetActualModifiers(e.Key, this.Modifiers))
        return;
      this.InvokeActions((object) e);
    }

    private static ModifierKeys GetActualModifiers(Key key, ModifierKeys modifiers)
    {
      switch (key)
      {
        case Key.LeftShift:
        case Key.RightShift:
          modifiers |= ModifierKeys.Shift;
          break;
        case Key.LeftCtrl:
        case Key.RightCtrl:
          modifiers |= ModifierKeys.Control;
          return modifiers;
        case Key.LeftAlt:
        case Key.RightAlt:
          modifiers |= ModifierKeys.Alt;
          return modifiers;
      }
      return modifiers;
    }
  }
}
