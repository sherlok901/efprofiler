﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.CaliburnInfrastructure.ProfilerWindowManager
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using HibernatingRhinos.Profiler.Client.Controls;
using Microsoft.Win32;
using System.Windows;
using System.Windows.Data;

namespace HibernatingRhinos.Profiler.Client.CaliburnInfrastructure
{
  public class ProfilerWindowManager : WindowManager
  {
    protected override Window EnsureWindow(object model, object view, bool isDialog)
    {
      Window window1 = view as Window;
      if (window1 == null)
      {
        window1 = ProfilerWindowManager.CreateWindowInstance(view, isDialog);
        window1.SetValue(View.IsGeneratedProperty, (object) true);
        Window window2 = this.InferOwnerOf(window1);
        if (window2 != null)
        {
          window1.WindowStartupLocation = WindowStartupLocation.CenterOwner;
          window1.Owner = window2;
        }
        else
          window1.WindowStartupLocation = WindowStartupLocation.CenterScreen;
      }
      else
      {
        Window window2 = this.InferOwnerOf(window1);
        if (window2 != null && isDialog)
          window1.Owner = window2;
      }
      return window1;
    }

    private static Window CreateWindowInstance(object view, bool isDialog)
    {
      if (isDialog)
      {
        DependencyObject dependencyObject = (DependencyObject) view;
        DialogWindow dialogWindow1 = new DialogWindow();
        dialogWindow1.Content = view;
        dialogWindow1.SizeToContent = Dialog.GetEnableMaximise(dependencyObject) ? SizeToContent.Manual : SizeToContent.WidthAndHeight;
        dialogWindow1.ResizeMode = Dialog.GetEnableMaximise(dependencyObject) ? ResizeMode.CanResizeWithGrip : ResizeMode.NoResize;
        DialogWindow dialogWindow2 = dialogWindow1;
        if (!double.IsNaN(Dialog.GetWidth(dependencyObject)))
          dialogWindow2.Width = Dialog.GetWidth(dependencyObject);
        if (!double.IsNaN(Dialog.GetHeight(dependencyObject)))
          dialogWindow2.Height = Dialog.GetHeight(dependencyObject);
        dialogWindow2.SetBinding(Window.TitleProperty, "DisplayName");
        dialogWindow2.SetBinding(DialogWindow.CanMaximiseProperty, (BindingBase) new Binding()
        {
          Path = new PropertyPath((object) Dialog.EnableMaximiseProperty),
          Source = view
        });
        dialogWindow2.SetBinding(FrameworkElement.MinWidthProperty, (BindingBase) new Binding()
        {
          Path = new PropertyPath((object) Dialog.MinWidthProperty),
          Source = view
        });
        dialogWindow2.SetBinding(FrameworkElement.MinHeightProperty, (BindingBase) new Binding()
        {
          Path = new PropertyPath((object) Dialog.MinHeightProperty),
          Source = view
        });
        return (Window) dialogWindow2;
      }
      Window window = new Window();
      window.Content = view;
      window.SizeToContent = SizeToContent.WidthAndHeight;
      return window;
    }

    public bool? ShowFileDialog(FileDialog dialog)
    {
      Window owner = this.InferOwnerOf((Window) null);
      return dialog.ShowDialog(owner);
    }
  }
}
