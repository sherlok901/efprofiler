﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.ApplicationContainer
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using Castle.Core;
using Castle.MicroKernel;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.Resolvers.SpecializedResolvers;
using Castle.Windsor;
using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.Client.CaliburnInfrastructure;
using HibernatingRhinos.Profiler.Client.Connections;
using HibernatingRhinos.Profiler.Client.ErrorReporting;
using HibernatingRhinos.Profiler.Client.Filtering;
using HibernatingRhinos.Profiler.Client.Infrastructure.AutoUpdate;
using HibernatingRhinos.Profiler.Client.Licensing;
using HibernatingRhinos.Profiler.Client.Reports;
using HibernatingRhinos.Profiler.Client.Sessions;
using HibernatingRhinos.Profiler.Client.Shell;
using HibernatingRhinos.Profiler.Client.Statements;
using HibernatingRhinos.Profiler.Client.Statistics;
using HibernatingRhinos.Profiler.Client.Tracking;
using System.Reflection;

namespace HibernatingRhinos.Profiler.Client
{
  public sealed class ApplicationContainer : WindsorContainer
  {
    public ApplicationContainer()
    {
      this.Register((IRegistration) Classes.FromAssembly(Assembly.GetExecutingAssembly()).BasedOn<IReport>().WithService.FromInterface(typeof (IReport)).Configure((System.Action<ComponentRegistration>) (x => x.LifestyleSingleton())), (IRegistration) Component.For<IWindowManager>().ImplementedBy<ProfilerWindowManager>().LifeStyle.Is(LifestyleType.Singleton).Forward<ProfilerWindowManager>(), (IRegistration) Component.For<IEventAggregator>().ImplementedBy<EventAggregator>().LifeStyle.Is(LifestyleType.Singleton), (IRegistration) Component.For<ProfilerAutoUpdate>().ImplementedBy<ProfilerAutoUpdate>(), (IRegistration) Component.For<BackendBridge>().ImplementedBy<BackendBridge>(), (IRegistration) Component.For<ApplicationModel>().ImplementedBy<ApplicationModel>().LifestyleSingleton(), (IRegistration) Component.For<ApplicationMenuModel>().ImplementedBy<ApplicationMenuModel>(), (IRegistration) Component.For<NavigatorModel>().ImplementedBy<NavigatorModel>(), (IRegistration) Component.For<SessionListModel>().ImplementedBy<SessionListModel>(), (IRegistration) Component.For<ReportListModel>().ImplementedBy<ReportListModel>(), (IRegistration) Component.For<StatisticsModel>().ImplementedBy<StatisticsModel>(), (IRegistration) Component.For<RemoteConnectionRepository>().ImplementedBy<RemoteConnectionRepository>(), (IRegistration) Component.For<FilterServiceModel>().ImplementedBy<FilterServiceModel>().LifeStyle.Is(LifestyleType.Singleton), (IRegistration) Component.For<RecentStatementsModel>().ImplementedBy<RecentStatementsModel>(), (IRegistration) Component.For<SessionRenameModel>().ImplementedBy<SessionRenameModel>(), (IRegistration) Component.For<LicensePromptModel>().ImplementedBy<LicensePromptModel>().LifeStyle.Is(LifestyleType.Transient), (IRegistration) Component.For<LicenseTrialRegistrationModel>().ImplementedBy<LicenseTrialRegistrationModel>().LifeStyle.Is(LifestyleType.Transient), (IRegistration) Component.For<EditConnectionModel>().ImplementedBy<EditConnectionModel>().LifeStyle.Is(LifestyleType.Transient), (IRegistration) Component.For<LicenseAgreementModel>().ImplementedBy<LicenseAgreementModel>(), (IRegistration) Component.For<LicenseInformationModel>().ImplementedBy<LicenseInformationModel>().LifeStyle.Is(LifestyleType.Singleton), (IRegistration) Component.For<ReportingUserInfoModel>().ImplementedBy<ReportingUserInfoModel>(), (IRegistration) Component.For<ConnectToProductionModel>().ImplementedBy<ConnectToProductionModel>().LifeStyle.Is(LifestyleType.Transient), (IRegistration) Component.For<ReconnectToProductionModel>().ImplementedBy<ReconnectToProductionModel>().LifeStyle.Is(LifestyleType.Transient), (IRegistration) Component.For<ITrackingService>().Instance(TrackingService.Create()));
      this.Kernel.Resolver.AddSubResolver((ISubDependencyResolver) new ArrayResolver(this.Kernel));
    }
  }
}
