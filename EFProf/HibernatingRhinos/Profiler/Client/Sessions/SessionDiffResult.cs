﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Sessions.SessionDiffResult
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.Client.Statements;
using System.Collections.Generic;

namespace HibernatingRhinos.Profiler.Client.Sessions
{
  public class SessionDiffResult
  {
    private readonly Dictionary<IStatementModel, DiffStatus> diffStatuses = new Dictionary<IStatementModel, DiffStatus>();
    private readonly Dictionary<IStatementModel, ISessionItem> statementSessions = new Dictionary<IStatementModel, ISessionItem>();
    private readonly List<IStatementModel> allStatements = new List<IStatementModel>();

    public IEnumerable<IStatementModel> Statements
    {
      get
      {
        return (IEnumerable<IStatementModel>) this.allStatements;
      }
    }

    public void Add(IStatementModel statement, ISessionItem session, DiffStatus status)
    {
      this.allStatements.Add(statement);
      this.diffStatuses.Add(statement, status);
      this.statementSessions.Add(statement, session);
    }

    public bool Contains(IStatementModel statement)
    {
      return this.diffStatuses.ContainsKey(statement);
    }

    public DiffStatus GetStatementStatus(IStatementModel statementModel)
    {
      DiffStatus diffStatus;
      if (!this.diffStatuses.TryGetValue(statementModel, out diffStatus))
        return DiffStatus.Matched;
      return diffStatus;
    }
  }
}
