﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Sessions.SessionDiffGenerator
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.Client.Statements;
using System.Collections.ObjectModel;

namespace HibernatingRhinos.Profiler.Client.Sessions
{
  public class SessionDiffGenerator
  {
    private readonly ISessionItem first;
    private readonly ISessionItem second;

    public SessionDiffGenerator(ISessionItem first, ISessionItem second)
    {
      this.first = first;
      this.second = second;
    }

    public SessionDiffResult Generate()
    {
      SessionDiffResult sessionDiffResult = new SessionDiffResult();
      foreach (IStatementModel statement1 in (Collection<IStatementModel>) this.first.SessionStatements.Statements)
      {
        if (!statement1.IsTransaction)
        {
          bool flag = false;
          foreach (IStatementModel statement2 in (Collection<IStatementModel>) this.second.SessionStatements.Statements)
          {
            if (!sessionDiffResult.Contains(statement2) && statement1.MatchWith(statement2))
            {
              sessionDiffResult.Add(statement2, this.second, DiffStatus.Matched);
              flag = true;
              break;
            }
          }
          if (!flag)
            sessionDiffResult.Add(statement1, this.first, DiffStatus.Removed);
        }
      }
      foreach (IStatementModel statement in (Collection<IStatementModel>) this.second.SessionStatements.Statements)
      {
        if (!statement.IsTransaction && !sessionDiffResult.Contains(statement))
          sessionDiffResult.Add(statement, this.second, DiffStatus.Added);
      }
      return sessionDiffResult;
    }
  }
}
