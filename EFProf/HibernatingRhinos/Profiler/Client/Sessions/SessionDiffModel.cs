﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Sessions.SessionDiffModel
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using HibernatingRhinos.Profiler.BackEnd.Profiles;
using HibernatingRhinos.Profiler.Client.Infrastructure;
using HibernatingRhinos.Profiler.Client.Statements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace HibernatingRhinos.Profiler.Client.Sessions
{
  public class SessionDiffModel : ProfilerScreen
  {
    private readonly IObservableCollection<DiffStatement> left = (IObservableCollection<DiffStatement>) new BindableCollection<DiffStatement>();
    private readonly IObservableCollection<DiffStatement> right = (IObservableCollection<DiffStatement>) new BindableCollection<DiffStatement>();
    private readonly SessionDiffResult sessionDiffResult;
    private bool selectedSessionIsRight;
    private DiffStatement selectedStatement;
    private DiffStatement selectedStatementLeft;
    private DiffStatement selectedStatementRight;

    public SessionDiffModel(SessionDiffResult sessionDiffResult, string leftSessionName, string rightSessionName)
    {
      this.DisplayName = Profile.Current.Translate("[[session]] Diff");
      this.sessionDiffResult = sessionDiffResult;
      foreach (IStatementModel statementModel in (IEnumerable<IStatementModel>) this.sessionDiffResult.Statements.OrderBy<IStatementModel, DateTime>((Func<IStatementModel, DateTime>) (x => x.Timestamp)))
      {
        switch (this.sessionDiffResult.GetStatementStatus(statementModel))
        {
          case DiffStatus.Matched:
            this.left.Add(new DiffStatement(statementModel, DiffStatus.Matched, false));
            this.right.Add(new DiffStatement(statementModel, DiffStatus.Matched, false));
            continue;
          case DiffStatus.Added:
            this.left.Add(new DiffStatement(statementModel, DiffStatus.Added, true));
            this.right.Add(new DiffStatement(statementModel, DiffStatus.Added, false));
            continue;
          case DiffStatus.Removed:
            this.left.Add(new DiffStatement(statementModel, DiffStatus.Removed, false));
            this.right.Add(new DiffStatement(statementModel, DiffStatus.Removed, true));
            continue;
          default:
            throw new ArgumentOutOfRangeException();
        }
      }
      this.LeftSessionName = leftSessionName;
      this.RightSessionName = rightSessionName;
    }

    public IObservableCollection<DiffStatement> Left
    {
      get
      {
        return this.left;
      }
    }

    public IObservableCollection<DiffStatement> Right
    {
      get
      {
        return this.right;
      }
    }

    public bool SelectedSessionIsRight
    {
      get
      {
        return this.selectedSessionIsRight;
      }
      set
      {
        this.selectedSessionIsRight = value;
        this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.SelectedSessionIsRight));
        this.SelectedStatement = value ? this.SelectedStatementRight : this.SelectedStatementLeft;
      }
    }

    public string LeftSessionName { get; set; }

    public string RightSessionName { get; set; }

    public DiffStatement SelectedStatement
    {
      get
      {
        return this.selectedStatement;
      }
      set
      {
        if (value == null || !value.CanSelect)
          return;
        if (this.selectedStatement != null)
          this.selectedStatement.IsSelected = false;
        this.selectedStatement = value;
        this.NotifyOfPropertyChange<DiffStatement>((Expression<Func<DiffStatement>>) (() => this.SelectedStatement));
        this.selectedStatement.IsSelected = true;
      }
    }

    public DiffStatement SelectedStatementLeft
    {
      get
      {
        return this.selectedStatementLeft;
      }
      set
      {
        this.selectedStatementLeft = value;
        this.NotifyOfPropertyChange<DiffStatement>((Expression<Func<DiffStatement>>) (() => this.SelectedStatementLeft));
        this.SelectedStatement = value;
      }
    }

    public DiffStatement SelectedStatementRight
    {
      get
      {
        return this.selectedStatementRight;
      }
      set
      {
        this.selectedStatementRight = value;
        this.NotifyOfPropertyChange<DiffStatement>((Expression<Func<DiffStatement>>) (() => this.SelectedStatementRight));
        this.SelectedStatement = value;
      }
    }
  }
}
