﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Sessions.AggregatedAlertInformation
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.Profiles;
using System.Collections.Generic;

namespace HibernatingRhinos.Profiler.Client.Sessions
{
  public class AggregatedAlertInformation : IAggregatedAlertSnapshot
  {
    public AggregatedAlertInformation(KeyValuePair<AlertInformation, int> pair)
    {
      this.Alert = pair.Key;
      this.Count = pair.Value;
      this.HelpUri = Profile.Current.GetLearnTopic("alert", this.Alert.HelpTopic);
      this.HelpTopic = this.Alert.HelpTopic;
      this.AlertTitle = Profile.Current.Translate(this.Alert.Title);
    }

    public AlertInformation Alert { get; set; }

    public int Count { get; set; }

    public string HelpUri { get; set; }

    public string HelpTopic { get; set; }

    public string AlertTitle { get; set; }
  }
}
