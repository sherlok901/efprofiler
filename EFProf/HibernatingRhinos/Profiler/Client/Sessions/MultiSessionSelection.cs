﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Sessions.MultiSessionSelection
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.Client.CaliburnInfrastructure;
using HibernatingRhinos.Profiler.Client.Filtering;
using HibernatingRhinos.Profiler.Client.Infrastructure;
using HibernatingRhinos.Profiler.Client.Statements;
using HibernatingRhinos.Profiler.Client.Tracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Windows.Threading;

namespace HibernatingRhinos.Profiler.Client.Sessions
{
  [View(typeof (SessionView))]
  public class MultiSessionSelection : SessionItemBase, IDiffableSelection
  {
    private readonly SessionModel[] sessions;
    private readonly SessionStatementsModel sessionStatements;

    public MultiSessionSelection(FilterServiceModel filterService, ITrackingService trackingService, SessionModel[] sessions, BackendBridge backendBridge, IEventAggregator eventAggregator)
      : base(eventAggregator)
    {
      this.sessions = sessions;
      this.sessionStatements = new SessionStatementsModel(filterService, trackingService, new int?(), false, (SessionItemBase) this, backendBridge);
      this.Items.Add((PollingScreen) this.sessionStatements);
      this.DisplayName = "Selection";
    }

    public override bool IsSelected
    {
      get
      {
        return ((IEnumerable<SessionModel>) this.sessions).All<SessionModel>((Func<SessionModel, bool>) (x => x.IsSelected));
      }
      set
      {
        foreach (SessionItemBase session in this.sessions)
          session.IsSelected = value;
        this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.IsSelected));
      }
    }

    public override bool IsStarred
    {
      get
      {
        return false;
      }
      set
      {
        throw new NotSupportedException();
      }
    }

    public override bool CanStar
    {
      get
      {
        return false;
      }
    }

    public DateTime Timestamp
    {
      get
      {
        return ((IEnumerable<SessionModel>) this.sessions).First<SessionModel>().Timestamp;
      }
    }

    public override SessionStatementsModel SessionStatements
    {
      get
      {
        return this.sessionStatements;
      }
    }

    public override int StatementsEtag
    {
      get
      {
        return -1;
      }
    }

    public bool CanDiff
    {
      get
      {
        return this.sessions.Length == 2;
      }
    }

    public ISessionItem First
    {
      get
      {
        return (ISessionItem) this.sessions[0];
      }
    }

    public ISessionItem Second
    {
      get
      {
        return (ISessionItem) this.sessions[1];
      }
    }

    public override IEnumerable<StatementSnapshot> GetStatements()
    {
      return ((IEnumerable<SessionModel>) this.sessions).SelectMany<SessionModel, StatementSnapshot>((Func<SessionModel, IEnumerable<StatementSnapshot>>) (x => (IEnumerable<StatementSnapshot>) x.Session.GetStatementsSnapshots()));
    }

    public override ISessionItem RemoveFrom(IList<ISessionItem> items)
    {
      DispatcherTimer timer = new DispatcherTimer();
      timer.Interval = new TimeSpan(0, 0, 0, 0, 100);
      timer.Tag = (object) items;
      timer.Tick += (EventHandler) ((sender, e) =>
      {
        timer.Stop();
        foreach (ISessionItem session in this.sessions)
          items.Remove(session);
      });
      this.IsSelected = false;
      timer.Start();
      return (ISessionItem) null;
    }

    protected override void OnInitialize()
    {
      base.OnInitialize();
      this.ActivateItem(this.Items.First<PollingScreen>());
    }

    protected override void OnActivate()
    {
      base.OnActivate();
      this.sessions.OfType<IActivate>().Apply<IActivate>((System.Action<IActivate>) (x => x.Activate()));
    }

    protected override void OnDeactivate(bool close)
    {
      base.OnDeactivate(close);
      this.sessions.OfType<IDeactivate>().Apply<IDeactivate>((System.Action<IDeactivate>) (x => x.Deactivate(close)));
    }

    public bool CanSetStarColor
    {
      get
      {
        return false;
      }
    }
  }
}
