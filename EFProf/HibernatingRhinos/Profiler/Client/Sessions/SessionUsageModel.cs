﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Sessions.SessionUsageModel
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.BackEnd.Profiles;
using HibernatingRhinos.Profiler.Client.Infrastructure;
using HibernatingRhinos.Profiler.Client.Tracking;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Windows.Data;
using System.Windows.Input;

namespace HibernatingRhinos.Profiler.Client.Sessions
{
  public class SessionUsageModel : PollingScreen<SessionUsageSnapshot>
  {
    private readonly SessionModel session;
    private readonly ITrackingService trackingService;
    private IDictionary<AlertInformation, int> aggregatedAlerts;
    private TimeSpan duration;
    private int entitiesLoaded;

    public SessionUsageModel(BackendBridge backendBridge, SessionModel session, ITrackingService trackingService)
      : base(new Func<Guid, SessionUsageSnapshot>(backendBridge.Notifications.GetSessionUsageSnapshot), session.Id)
    {
      this.session = session;
      this.trackingService = trackingService;
      this.DisplayName = Profile.Current.Translate("[[session]] Usage");
      this.AggregatedAlertsCollectionView = (ICollectionView) new ListCollectionView((IList) this.AggregatedAlerts);
    }

    public string OpenedAtText
    {
      get
      {
        return string.Format((IFormatProvider) CultureInfo.InvariantCulture, Profile.Current.Translate("This [[session]] was opened at {0:hh:mm:ss.FFFFFFF tt} on {0:dddd, MMMM dd, yyyy}."), new object[1]
        {
          (object) this.Timestamp
        });
      }
    }

    public TimeSpan Duration
    {
      get
      {
        return this.duration;
      }
      set
      {
        this.duration = value;
        this.NotifyOfPropertyChange<TimeSpan>((Expression<Func<TimeSpan>>) (() => this.Duration));
      }
    }

    public int EntitiesLoaded
    {
      get
      {
        return this.entitiesLoaded;
      }
      set
      {
        this.entitiesLoaded = value;
        this.NotifyOfPropertyChange<int>((Expression<Func<int>>) (() => this.EntitiesLoaded));
      }
    }

    public ICollectionView AggregatedAlertsCollectionView { get; set; }

    public AggregatedAlertInformation[] AggregatedAlerts
    {
      get
      {
        if (this.aggregatedAlerts == null)
          return new AggregatedAlertInformation[0];
        return this.aggregatedAlerts.Select<KeyValuePair<AlertInformation, int>, AggregatedAlertInformation>((Func<KeyValuePair<AlertInformation, int>, AggregatedAlertInformation>) (pair => new AggregatedAlertInformation(pair))).ToArray<AggregatedAlertInformation>();
      }
    }

    public int TotalNumberOfAlerts
    {
      get
      {
        return ((IEnumerable<AggregatedAlertInformation>) this.AggregatedAlerts).Sum<AggregatedAlertInformation>((Func<AggregatedAlertInformation, int>) (x => x.Count));
      }
    }

    public string Url
    {
      get
      {
        return this.session.Url;
      }
    }

    public DateTime Timestamp
    {
      get
      {
        return this.session.Timestamp;
      }
    }

    public override void Update(SessionUsageSnapshot snapshot)
    {
      if (snapshot == null)
        return;
      this.Duration = snapshot.Duration;
      this.EntitiesLoaded = snapshot.EntitiesLoaded;
      this.aggregatedAlerts = (IDictionary<AlertInformation, int>) snapshot.AggregatedAlerts;
      this.AggregatedAlertsCollectionView = (ICollectionView) new ListCollectionView((IList) this.AggregatedAlerts);
      this.NotifyOfPropertyChange<ICollectionView>((Expression<Func<ICollectionView>>) (() => this.AggregatedAlertsCollectionView));
      this.NotifyOfPropertyChange<int>((Expression<Func<int>>) (() => this.TotalNumberOfAlerts));
      if (this.NotifyOnUpdate == null)
        return;
      this.NotifyOnUpdate();
    }

    protected override void OnActivate()
    {
      this.trackingService.Track("Sessions", "Usage", (string) null, new int?());
      base.OnActivate();
    }

    public void CancelSorting(MouseButtonEventArgs e)
    {
      this.AggregatedAlertsCollectionView.SortDescriptions.Clear();
      e.Handled = true;
    }
  }
}
