﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Sessions.SessionStatisticsModel
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using HibernatingRhinos.Profiler.BackEnd.Bridge;
using System.Text;

namespace HibernatingRhinos.Profiler.Client.Sessions
{
  public class SessionStatisticsModel : PropertyChangedBase
  {
    public int NumberOfStatements { get; private set; }

    public int NumberOfCachedStatements { get; private set; }

    public int NumberOfTransactionsStatements { get; private set; }

    public string ShortDescription
    {
      get
      {
        if (this.NumberOfCachedStatements == 0)
          return "[" + (object) this.NumberOfStatements + "]";
        return "[" + (object) this.NumberOfCachedStatements + " / " + (object) this.NumberOfStatements + "]";
      }
    }

    public string LongDescription
    {
      get
      {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.Append("Database Statements: ").Append(this.NumberOfStatements).AppendLine();
        stringBuilder.Append("Transaction Statements: ").Append(this.NumberOfTransactionsStatements).AppendLine();
        if (this.NumberOfCachedStatements != 0)
          stringBuilder.Append("Cached Statements: ").Append(this.NumberOfCachedStatements).AppendLine();
        return stringBuilder.Remove(stringBuilder.Length - 2, 2).ToString();
      }
    }

    public void UpdateFrom(SessionStatistics statistics, SessionModel model)
    {
      this.NumberOfStatements = statistics.NumberOfStatements;
      this.NumberOfCachedStatements = statistics.NumberOfCachedStatements;
      this.NumberOfTransactionsStatements = statistics.NumberOfTransactionsStatements;
      model.DisplayName = statistics.Name;
      model.SetWarnings(statistics.HasWarnings);
      model.SetSuggestions(statistics.HasSuggestions);
      model.SetMajor(statistics.HasMajor);
      this.NotifyOfPropertyChange("");
    }
  }
}
