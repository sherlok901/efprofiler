﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Sessions.SessionItemBase
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using HibernatingRhinos.Profiler.Client.Events;
using HibernatingRhinos.Profiler.Client.Infrastructure;
using HibernatingRhinos.Profiler.Client.Statements;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.Client.Sessions
{
  public abstract class SessionItemBase : Conductor<PollingScreen>.Collection.OneActive, ISessionItem, IConductor, IParent, INotifyPropertyChangedEx, INotifyPropertyChanged, IHaveDisplayName
  {
    private readonly IEventAggregator eventAggregator;
    private bool hasSeparator;

    protected SessionItemBase(IEventAggregator eventAggregator)
    {
      this.eventAggregator = eventAggregator;
    }

    public virtual Guid Id
    {
      get
      {
        return Guid.Empty;
      }
    }

    public abstract SessionStatementsModel SessionStatements { get; }

    public abstract int StatementsEtag { get; }

    public abstract IEnumerable<StatementSnapshot> GetStatements();

    public virtual ISessionItem RemoveFrom(IList<ISessionItem> items)
    {
      return (ISessionItem) null;
    }

    public abstract bool IsSelected { get; set; }

    public abstract bool IsStarred { get; set; }

    public abstract bool CanStar { get; }

    public bool FilteringSupported
    {
      get
      {
        return this.ActiveItem is SessionStatementsModel;
      }
    }

    public virtual void SetStarColor(string color)
    {
    }

    protected override void OnViewLoaded(object view)
    {
    }

    protected override void ChangeActiveItem(PollingScreen newItem, bool closePrevious)
    {
      base.ChangeActiveItem(newItem, closePrevious);
      this.eventAggregator.PublishOnUIThread((object) new ActiveSessionItemChanged());
    }

    public bool HasSeparator
    {
      get
      {
        return this.hasSeparator;
      }
      set
      {
        this.hasSeparator = value;
        this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.HasSeparator));
      }
    }

    public virtual Session Session
    {
      get
      {
        return (Session) null;
      }
    }

    //[SpecialName]
    //IObservableCollection<PollingScreen> ISessionItem.get_Items()
    //{
    //  return this.Items;
    //}
  }
}
