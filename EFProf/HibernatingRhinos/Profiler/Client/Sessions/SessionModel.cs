﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Sessions.SessionModel
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using HibernatingRhinos.Profiler.BackEnd.Profiles;
using HibernatingRhinos.Profiler.Client.Controls;
using HibernatingRhinos.Profiler.Client.Entities;
using HibernatingRhinos.Profiler.Client.Filtering;
using HibernatingRhinos.Profiler.Client.Infrastructure;
using HibernatingRhinos.Profiler.Client.Statements;
using HibernatingRhinos.Profiler.Client.Tracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace HibernatingRhinos.Profiler.Client.Sessions
{
  public class SessionModel : SessionItemBase, IFilterableSessionSnapshot
  {
    private readonly SessionStatisticsModel statistics = new SessionStatisticsModel();
    private int statementsEtag = 1;
    private readonly FilterServiceModel filter;
    private readonly SessionStatementsModel sessionStatements;
    private EventHandler filterOnFiltersChanged;
    private bool isStarred;
    private bool isSelected;
    private string starColor;
    private string lastStarColor;
    private int? statementCount;

    public new Session Session { get; private set; }

    public SessionModel(BackendBridge backendBridge, FilterServiceModel filter, Session session, ITrackingService trackingService, IEventAggregator eventAggregator)
      : base(eventAggregator)
    {
      this.filter = filter;
      this.Session = session;
      this.DisplayName = session.Name;
      Uri result;
      if (Uri.TryCreate(session.Url, UriKind.Absolute, out result))
      {
        this.ShortUrl = result.AbsolutePath;
        if (!string.IsNullOrEmpty(result.Query))
          this.ShortUrl += result.Query;
      }
      else
        this.ShortUrl = session.Url;
      this.statistics = new SessionStatisticsModel();
      this.filterOnFiltersChanged = (EventHandler) ((s, e) => this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.FilteringSupported)));
      this.filter.FiltersChanged += this.filterOnFiltersChanged;
      this.sessionStatements = new SessionStatementsModel(this.filter, trackingService, new int?(), false, (SessionItemBase) this, backendBridge);
      this.Items.Add((PollingScreen) this.sessionStatements);
      if (Profile.Current.Supports(SupportedFeatures.Entities))
        this.Items.Add((PollingScreen) new SessionEntitiesModel(this, trackingService));
      this.Items.Add((PollingScreen) new SessionUsageModel(backendBridge, this, trackingService));
      this.lastStarColor = StarToggleButton.StarColors[0];
      this.StarColor = StarToggleButton.DefaultStarColor;
      this.Update(session);
    }

    public override bool IsStarred
    {
      get
      {
        return this.isStarred;
      }
      set
      {
        this.StarColor = value ? this.lastStarColor : StarToggleButton.DefaultStarColor;
        this.isStarred = value;
        this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.IsStarred));
      }
    }

    public override bool IsSelected
    {
      get
      {
        return this.isSelected;
      }
      set
      {
        this.isSelected = value;
        this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.IsSelected));
      }
    }

    public override bool CanStar
    {
      get
      {
        return true;
      }
    }

    public List<string> StarColors
    {
      get
      {
        return StarToggleButton.StarColors;
      }
    }

    public string StarColor
    {
      get
      {
        return this.starColor;
      }
      set
      {
        this.starColor = value;
        this.NotifyOfPropertyChange<string>((Expression<Func<string>>) (() => this.StarColor));
      }
    }

    public override void SetStarColor(string color)
    {
      this.IsStarred = true;
      this.StarColor = this.lastStarColor = color;
    }

    public override SessionStatementsModel SessionStatements
    {
      get
      {
        return this.sessionStatements;
      }
    }

    protected override void OnViewLoaded(object view)
    {
      base.OnViewLoaded(view);
      if (this.ActiveItem != null)
        return;
      this.ChangeActiveItem((PollingScreen) this.sessionStatements, true);
    }

    public override int StatementsEtag
    {
      get
      {
        return this.statementsEtag;
      }
    }

    public SessionStatisticsModel Statistics
    {
      get
      {
        return this.statistics;
      }
    }

    public string ShortUrl { get; set; }

    public string Url
    {
      get
      {
        return this.Session.Url;
      }
    }

    public int StatementCount
    {
      get
      {
        return this.statementCount ?? this.SessionStatements.Statements.Count<IStatementModel>((Func<IStatementModel, bool>) (x => !x.IsTransaction));
      }
      private set
      {
        this.statementCount = new int?(value);
      }
    }

    public bool HasMajor { get; private set; }

    public bool HasSuggestions { get; private set; }

    public bool HasWarnings { get; private set; }

    public TimeSpan Duration
    {
      get
      {
        return this.Session.Duration.GetValueOrDefault();
      }
    }

    public override Guid Id
    {
      get
      {
        return this.Session.InternalId;
      }
    }

    public IDictionary<StatementAlert, int> AggregatedAlerts
    {
      get
      {
        Dictionary<StatementAlert, int> dictionary = new Dictionary<StatementAlert, int>();
        foreach (StatementAlert key in this.SessionStatements.Statements.SelectMany<IStatementModel, StatementAlert>((Func<IStatementModel, IEnumerable<StatementAlert>>) (x => (IEnumerable<StatementAlert>) x.Alerts)))
        {
          int num;
          if (!dictionary.TryGetValue(key, out num))
            num = 0;
          dictionary[key] = num + 1;
        }
        return (IDictionary<StatementAlert, int>) dictionary;
      }
    }

    public DateTime Timestamp
    {
      get
      {
        return this.Session.Id.Timestamp;
      }
    }

    public string ShortDescription
    {
      get
      {
        if (!string.IsNullOrEmpty(this.Url))
          return string.Format("{0} [{1}]", (object) this.DisplayName, (object) this.ShortUrl);
        return this.DisplayName;
      }
    }

    public override IEnumerable<StatementSnapshot> GetStatements()
    {
      return (IEnumerable<StatementSnapshot>) this.Session.GetStatementsSnapshots();
    }

    public override ISessionItem RemoveFrom(IList<ISessionItem> items)
    {
      int index = items.IndexOf((ISessionItem) this);
      items.Remove((ISessionItem) this);
      if (index - 1 >= 0)
        return items[index - 1];
      if (index < items.Count)
        return items[index];
      return (ISessionItem) null;
    }

    public void Update(Session updatedSession)
    {
      ++this.statementsEtag;
      this.DisplayName = updatedSession.Name;
      this.Session = updatedSession;
      SessionStatistics sessionStatistics = updatedSession.CreateSessionStatistics();
      this.StatementCount = sessionStatistics.NumberOfStatements;
      this.Statistics.UpdateFrom(sessionStatistics, this);
      if (this.WasClosed)
      {
        if (this.Session.StatementsCount == this.Session.Statements.Count)
        {
          foreach (PollingScreen pollingScreen in (IEnumerable<PollingScreen>) this.Items)
            pollingScreen.DonePolling();
        }
        else
        {
          foreach (PollingScreen pollingScreen in (IEnumerable<PollingScreen>) this.Items)
            pollingScreen.ContinuePolling();
        }
      }
      this.NotifyOfPropertyChange("");
    }

    public bool WasClosed
    {
      get
      {
        return !this.Session.IsOpen;
      }
    }

    public string ScopeName
    {
      get
      {
        return this.Session.Id.ScopeName;
      }
    }

    public void SetWarnings(bool warnings)
    {
      this.HasWarnings = warnings;
      this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.HasWarnings));
    }

    public void SetSuggestions(bool suggestions)
    {
      this.HasSuggestions = suggestions;
      this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.HasSuggestions));
    }

    public void SetMajor(bool major)
    {
      this.HasMajor = major;
      this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.HasMajor));
    }

    public void CopySessionName()
    {
      SafeClipboard.SetText(Profile.Current.Translate(this.DisplayName));
    }

    public void CopySessionUrl()
    {
      SafeClipboard.SetText(this.Url);
    }

    public void RenameByUser(string sessionName)
    {
      this.DisplayName = sessionName;
      this.NotifyOfPropertyChange<string>((Expression<Func<string>>) (() => this.ShortDescription));
    }
  }
}
