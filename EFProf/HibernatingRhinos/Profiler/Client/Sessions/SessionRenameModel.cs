﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Sessions.SessionRenameModel
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.BackEnd.Profiles;
using HibernatingRhinos.Profiler.Client.Infrastructure;
using System;
using System.Linq.Expressions;

namespace HibernatingRhinos.Profiler.Client.Sessions
{
  public class SessionRenameModel : ProfilerScreen
  {
    private string sessionName;
    private SessionModel sessionModel;

    public SessionRenameModel()
    {
      this.DisplayName = Profile.Current.Translate("Rename [[session]]");
    }

    public void SetSessionToRename(SessionModel session)
    {
      this.sessionModel = session;
      this.SessionName = Profile.Current.Translate(this.sessionModel.DisplayName);
    }

    public string SessionName
    {
      get
      {
        return this.sessionName;
      }
      set
      {
        this.sessionName = value;
        this.NotifyOfPropertyChange<string>((Expression<Func<string>>) (() => this.SessionName));
        this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.CanRename));
      }
    }

    public void Rename()
    {
      if (string.IsNullOrEmpty(this.SessionName))
        return;
      this.sessionModel.RenameByUser(this.sessionName);
      this.TryClose();
    }

    public bool CanRename
    {
      get
      {
        return !string.IsNullOrEmpty(this.SessionName);
      }
    }
  }
}
