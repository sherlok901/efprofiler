﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Sessions.ISessionItem
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using HibernatingRhinos.Profiler.Client.Infrastructure;
using HibernatingRhinos.Profiler.Client.Statements;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace HibernatingRhinos.Profiler.Client.Sessions
{
  public interface ISessionItem : IConductor, IParent, INotifyPropertyChangedEx, INotifyPropertyChanged, IHaveDisplayName
  {
    Guid Id { get; }

    SessionStatementsModel SessionStatements { get; }

    IObservableCollection<PollingScreen> Items { get; }

    int StatementsEtag { get; }

    IEnumerable<StatementSnapshot> GetStatements();

    ISessionItem RemoveFrom(IList<ISessionItem> items);

    bool IsSelected { get; set; }

    bool IsStarred { get; set; }

    bool CanStar { get; }

    bool FilteringSupported { get; }

    void SetStarColor(string color);

    bool HasSeparator { get; set; }

    Session Session { get; }
  }
}
