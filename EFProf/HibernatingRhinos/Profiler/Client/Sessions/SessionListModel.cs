﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Sessions.SessionListModel
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.BackEnd.Profiles;
using HibernatingRhinos.Profiler.Client.Controls;
using HibernatingRhinos.Profiler.Client.Events;
using HibernatingRhinos.Profiler.Client.Filtering;
using HibernatingRhinos.Profiler.Client.Infrastructure;
using HibernatingRhinos.Profiler.Client.Reports;
using HibernatingRhinos.Profiler.Client.Sessions.SessionListViews;
using HibernatingRhinos.Profiler.Client.Statements;
using HibernatingRhinos.Profiler.Client.Tracking;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Windows.Data;
using System.Xml.Linq;

namespace HibernatingRhinos.Profiler.Client.Sessions
{
  public class SessionListModel : Conductor<ISessionItem>.Collection.OneActive, INavigateItems, ISupportFiltering, IHandle<ActiveSessionItemChanged>, IHandle
  {
    private readonly RecentStatementsModel recentStatements;
    private readonly FilterServiceModel filterService;
    private readonly ITrackingService trackingService;
    private readonly IWindowManager windowManager;
    private readonly IEventAggregator eventAggregator;
    private readonly BackendBridge backendBridge;
    private bool _isGrouping;
    private bool? _isAllCollapsed;

    public SessionListModel(RecentStatementsModel recentStatements, FilterServiceModel filterService, ITrackingService trackingService, IWindowManager windowManager, IEventAggregator eventAggregator, BackendBridge backendBridge)
    {
      this.DisplayName = Profile.Current.Translate("[[sessions]]");
      this.recentStatements = recentStatements;
      this.filterService = filterService;
      this.trackingService = trackingService;
      this.windowManager = windowManager;
      this.eventAggregator = eventAggregator;
      this.backendBridge = backendBridge;
      this.filterService.FiltersChanged += (EventHandler) ((sender, args) => this.ApplyFilters());
      this.eventAggregator.Subscribe((object) this);
      this.SessionCollection = new CollectionViewSource()
      {
        Source = (object) this.Items
      };
    }

    public int Count
    {
      get
      {
        return this.SessionCollection.View.Count();
      }
    }

    protected override void OnInitialize()
    {
      this.ActivateItem((ISessionItem) this.recentStatements);
      base.OnInitialize();
    }

    public bool IsGrouping
    {
      get
      {
        return this._isGrouping;
      }
      set
      {
        this._isGrouping = value;
        this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.IsGrouping));
      }
    }

    public void ToggleScopes()
    {
      if (this.SessionCollection.View.GroupDescriptions.Count > 0)
      {
        this.SessionCollection.View.GroupDescriptions.Clear();
        this.IsGrouping = false;
      }
      else
      {
        this.SessionCollection.View.GroupDescriptions.Add((GroupDescription) new PropertyGroupDescription("ScopeName"));
        this.IsGrouping = true;
      }
    }

    public void ExpandAll()
    {
      this.IsAllCollapsed = new bool?(false);
    }

    public void CollapseAll()
    {
      this.IsAllCollapsed = new bool?(true);
    }

    public void Clear()
    {
      this.Items.Clear();
      this.recentStatements.Clear();
      this.ActivateItem((ISessionItem) this.recentStatements);
    }

    public CollectionViewSource SessionCollection { get; set; }

    public void ClearExcept(Guid[] sessionIds)
    {
      if (((IEnumerable<Guid>) sessionIds).Any<Guid>())
      {
        foreach (ISessionItem sessionItem in this.Items.Skip<ISessionItem>(1).ToArray<ISessionItem>())
        {
          if (!((IEnumerable<Guid>) sessionIds).Contains<Guid>(sessionItem.Id))
            this.Items.Remove(sessionItem);
        }
        this.recentStatements.ClearExcept(sessionIds);
      }
      else
        this.Clear();
    }

    public void Star(SessionModel item)
    {
      if (item == null)
        return;
      item.IsStarred = !item.IsStarred;
    }

    public bool CanStar(ISessionItem item)
    {
      return item is SessionModel;
    }

    public bool CanRemoveSelection
    {
      get
      {
        if (this.ActiveItem != null)
          return this.ActiveItem != this.recentStatements;
        return false;
      }
    }

    public void RemoveSelection()
    {
      if (!this.CanRemoveSelection)
        return;
      this.ActiveItem = this.ActiveItem.RemoveFrom((IList<ISessionItem>) this.Items) ?? (ISessionItem) this.recentStatements;
    }

    public void NextItem()
    {
      this.ActiveItem = NavigationManager.GetNext<ISessionItem>((IList<ISessionItem>) this.Items, this.ActiveItem);
    }

    public void PreviousItem()
    {
      this.ActiveItem = NavigationManager.GetPrevious<ISessionItem>((IList<ISessionItem>) this.Items, this.ActiveItem);
    }

    protected override void ChangeActiveItem(ISessionItem newItem, bool closePrevious)
    {
      try
      {
        base.ChangeActiveItem(newItem, closePrevious);
      }
      catch (Exception ex)
      {
        if (!ex.Message.StartsWith("No target found for method"))
          throw;
      }
      if (!(newItem is MultiSessionSelection))
      {
        Master view = this.GetView((object) "Master") as Master;
        if (view != null)
          view.Items.SelectedItem = (object) newItem;
      }
      this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.FilteringSupported));
    }

    public string ExportToJsonString()
    {
      JArray jarray = new JArray((object) this.Items.Where<ISessionItem>((Func<ISessionItem, bool>) (x => x is SessionModel)).Cast<SessionModel>().Select<SessionModel, JObject>((Func<SessionModel, JObject>) (x => new JObject(new object[3]
      {
        (object) new JProperty("session", (object) Profile.Current.Translate(x.DisplayName)),
        (object) new JProperty("statementCount", (object) x.Statistics.NumberOfStatements),
        (object) new JProperty("queries", (object) new JArray((object) x.SessionStatements.Statements.Select<IStatementModel, JObject>((Func<IStatementModel, JObject>) (y => new JObject(new object[6]
        {
          (object) new JProperty("isCached", (object) y.IsCached),
          (object) new JProperty("isDdl", (object) y.IsDDL),
          (object) new JProperty("duration", (object) y.Duration.Value),
          (object) new JProperty("formattedSql", (object) y.Text),
          (object) new JProperty("shortSql", (object) y.ShortText),
          (object) new JProperty("count", (object) SessionListModel.FormatAsCommaSeparatedList((IEnumerable<int>) y.CountOfRows))
        })))))
      }))));
      StringWriter stringWriter = new StringWriter();
      jarray.WriteJson((TextWriter) stringWriter);
      return stringWriter.GetStringBuilder().ToString();
    }

    public XElement Export()
    {
      foreach (ISessionItem sessionItem in (IEnumerable<ISessionItem>) this.Items)
      {
        sessionItem.Items.Apply<PollingScreen>((System.Action<PollingScreen>) (x => x.TimerTicked((object) this, EventArgs.Empty)));
        sessionItem.SessionStatements.TimerTicked((object) this, EventArgs.Empty);
      }
      return new XElement(ReportBase.Namespace + "sessions", (object) this.Items.Select<ISessionItem, XElement>((Func<ISessionItem, XElement>) (x => new XElement(ReportBase.Namespace + "session", new object[3]
      {
        (object) new XAttribute((XName) "name", (object) Profile.Current.Translate(x.DisplayName)),
        (object) new XAttribute((XName) "statementCount", (object) x.SessionStatements.StatementCount),
        (object) new XElement(ReportBase.Namespace + "queries", (object) x.SessionStatements.Statements.Select<IStatementModel, XElement>((Func<IStatementModel, XElement>) (y => y.ToXml())))
      }))));
    }

    private static string FormatAsCommaSeparatedList(IEnumerable<int> nums)
    {
      return string.Join(", ", nums.Select<int, string>((Func<int, string>) (i => i.ToString())).ToArray<string>());
    }

    public void SelectSessions(IList items)
    {
      if (items == null)
        return;
      IOrderedEnumerable<ISessionItem> source = items.OfType<ISessionItem>().OrderBy<ISessionItem, int>(new Func<ISessionItem, int>(this.GetStatementIndex));
      foreach (SessionItemBase sessionItemBase in this.Items.OfType<SessionModel>())
        sessionItemBase.IsSelected = false;
      this.SetSelectedSessions(source.ToArray<ISessionItem>());
    }

    public int GetStatementIndex(ISessionItem statementModel)
    {
      return this.Items.IndexOf(statementModel);
    }

    public void SetSelectedSessions(ISessionItem[] selection)
    {
      this.trackingService.Track("Main", "Selection", "Sessions", new int?(selection.Length));
      switch (selection.Length)
      {
        case 0:
          this.ActiveItem = (ISessionItem) null;
          break;
        case 1:
          Conductor<PollingScreen>.Collection.OneActive activeItem1 = (Conductor<PollingScreen>.Collection.OneActive) this.ActiveItem;
          if (activeItem1 != null)
          {
            PollingScreen activeItem = activeItem1.Items.FirstOrDefault<PollingScreen>((Func<PollingScreen, bool>) (screen => screen.IsActive));
            if (activeItem != null)
            {
              PollingScreen pollingScreen = selection[0].Items.FirstOrDefault<PollingScreen>((Func<PollingScreen, bool>) (screen => screen.DisplayName == activeItem.DisplayName)) ?? selection[0].Items.FirstOrDefault<PollingScreen>();
              selection[0].ActivateItem((object) pollingScreen);
            }
          }
          selection[0].IsSelected = true;
          this.ActiveItem = selection[0];
          break;
        default:
          MultiSessionSelection sessionSelection1 = new MultiSessionSelection(this.filterService, this.trackingService, selection.OfType<SessionModel>().ToArray<SessionModel>(), this.backendBridge, this.eventAggregator);
          sessionSelection1.IsSelected = true;
          MultiSessionSelection sessionSelection2 = sessionSelection1;
          this.trackingService.Track("Sessions", "Aggregate Sessions", (string) null, new int?());
          this.ActiveItem = (ISessionItem) sessionSelection2;
          this.Items.Remove(this.ActiveItem);
          break;
      }
      this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.CanDiffSessions));
      this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.CanRenameSession));
      this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.CanSeparateSession));
    }

    public void DiffSessions()
    {
      IDiffableSelection activeItem = this.ActiveItem as IDiffableSelection;
      if (activeItem == null)
        return;
      SessionDiffResult sessionDiffResult = new SessionDiffGenerator(activeItem.First, activeItem.Second).Generate();
      this.trackingService.Track("Sessions", "Diff", (string) null, new int?());
      this.windowManager.ShowDialog((object) new SessionDiffModel(sessionDiffResult, activeItem.First.DisplayName, activeItem.Second.DisplayName), (object) null, (IDictionary<string, object>) null);
    }

    public bool CanDiffSessions
    {
      get
      {
        if (this.ActiveItem is RecentStatementsModel)
          return false;
        IDiffableSelection activeItem = this.ActiveItem as IDiffableSelection;
        if (activeItem != null)
          return activeItem.CanDiff;
        return false;
      }
    }

    public void RemoveSession()
    {
      this.trackingService.Track("Sessions", "Remove", (string) null, new int?());
      this.RemoveSelection();
    }

    public bool CanRemoveSession()
    {
      return this.CanRemoveSelection;
    }

    public void ShowAlerts()
    {
      this.ActiveItem.SessionStatements.ShowAlerts();
    }

    public void ApplyFilters()
    {
      this.SessionCollection.View.Filter = (Predicate<object>) null;
      if (this.filterService.Filters.Count == 0)
        return;
      this.SessionCollection.View.Filter = new Predicate<object>(this.FilterSession);
    }

    private bool FilterSession(object session)
    {
      SessionModel sessionModel = session as SessionModel;
      if (sessionModel != null)
        return this.filterService.FilterSession((IFilterableSessionSnapshot) sessionModel);
      return true;
    }

    public void RenameSession()
    {
      this.trackingService.Track("Sessions", "Rename", (string) null, new int?());
      SessionRenameModel sessionRenameModel = IoC.Get<SessionRenameModel>((string) null);
      sessionRenameModel.SetSessionToRename(this.ActiveItem as SessionModel);
      Dialog.Show((object) sessionRenameModel, (object) null, false);
    }

    public bool CanRenameSession
    {
      get
      {
        return this.ActiveItem is SessionModel;
      }
    }

    public void SeparateSession()
    {
      this.trackingService.Track("Sessions", "Separate", (string) null, new int?());
      SessionModel activeItem = this.ActiveItem as SessionModel;
      if (activeItem == null)
        return;
      activeItem.HasSeparator = !activeItem.HasSeparator;
    }

    public bool CanSeparateSession
    {
      get
      {
        return this.ActiveItem is SessionModel;
      }
    }

    public void RenameScope()
    {
    }

    public void RemoveScope()
    {
    }

    public bool FilteringSupported
    {
      get
      {
        if (this.ActiveItem != null)
          return this.ActiveItem.FilteringSupported;
        return false;
      }
    }

    public IEnumerable<SessionModel> Sessions
    {
      get
      {
        return this.Items.OfType<SessionModel>();
      }
    }

    public void CallWhenFilteringSupportedChanged(System.Action action)
    {
      throw new NotImplementedException();
    }

    public void Handle(ActiveSessionItemChanged message)
    {
      if (this.filterService.Filters.Count <= 0)
        return;
      this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.FilteringSupported));
    }

    public void Add(SessionModel model)
    {
      this.Items.Add((ISessionItem) model);
    }

    public bool? IsAllCollapsed
    {
      get
      {
        return this._isAllCollapsed;
      }
      set
      {
        this._isAllCollapsed = value;
        this.NotifyOfPropertyChange<bool?>((Expression<Func<bool?>>) (() => this.IsAllCollapsed));
      }
    }
  }
}
