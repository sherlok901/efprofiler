﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Sessions.DiffStatement
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using HibernatingRhinos.Profiler.Client.Statements;
using System;
using System.Linq.Expressions;
using System.Windows.Media;

namespace HibernatingRhinos.Profiler.Client.Sessions
{
  public class DiffStatement : PropertyChangedBase
  {
    private readonly bool isPlaceholder;
    private readonly IStatementModel model;
    private readonly DiffStatus status;
    private bool isSelected;

    public DiffStatement(IStatementModel model, DiffStatus status, bool isPlaceholder)
    {
      this.model = model;
      this.status = status;
      this.isPlaceholder = isPlaceholder;
    }

    public bool ShowAsMultiline
    {
      get
      {
        if (!this.isPlaceholder)
          return this.model.ShortText.Contains("\n");
        return false;
      }
    }

    public bool HasStrikethrough
    {
      get
      {
        if (!this.isPlaceholder)
          return this.status == DiffStatus.Removed;
        return false;
      }
    }

    public Brush Foreground
    {
      get
      {
        if (this.isPlaceholder)
          return (Brush) new SolidColorBrush(Colors.Transparent);
        if (this.status == DiffStatus.Removed)
          return (Brush) new SolidColorBrush(Colors.Red);
        if (this.status == DiffStatus.Added)
          return (Brush) new SolidColorBrush(Colors.Blue);
        return (Brush) new SolidColorBrush(Colors.Black);
      }
    }

    public bool CanSelect
    {
      get
      {
        return !this.isPlaceholder;
      }
    }

    public string ShortText
    {
      get
      {
        return this.model.ShortText;
      }
    }

    public bool IsSelected
    {
      get
      {
        return this.isSelected;
      }
      set
      {
        this.isSelected = value;
        this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.IsSelected));
      }
    }

    public string Text
    {
      get
      {
        return this.model.Text;
      }
    }
  }
}
