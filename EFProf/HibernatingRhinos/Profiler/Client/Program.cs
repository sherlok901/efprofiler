﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Program
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using gudusoft.gsqlparser;
using HibernatingRhinos.Profiler.BackEnd.Profiles;
using HibernatingRhinos.Profiler.Client.Services;
using HibernatingRhinos.Profiler.Client.Startup;
using log4net;
using log4net.Config;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace HibernatingRhinos.Profiler.Client
{
  public class Program
  {
    private static readonly ILog Log = LogManager.GetLogger(typeof (Program));
    private static readonly string[] RequiredFiles = new string[20]
    {
      "gudusoft.gsqlparser.dll",
      "HibernatingRhinos.Profiler.Appender.dll",
      "HibernatingRhinos.Profiler.BackEnd.dll",
      "Newtonsoft.Json.dll",
      Path.GetFileName(typeof (Program).Assembly.Location),
      Path.GetFileName(typeof (Program).Assembly.Location) + ".config",
      "License.txt",
      "log4net.dll",
      "System.Reactive.Core.dll",
      "System.Reactive.Interfaces.dll",
      "System.Reactive.Linq.dll",
      "System.Reactive.PlatformServices.dll",
      "Microsoft.Expression.Interactions.dll",
      "System.Windows.Interactivity.dll",
      "NAppUpdate.Framework.dll",
      "ICSharpCode.SharpZipLib.dll",
      "Caliburn.Micro.dll",
      "Caliburn.Micro.Platform.dll",
      "Castle.Windsor.dll",
      "AqiStar.TextBox.dll"
    };

    [STAThread]
    public static int Main(string[] args)
    {
      if (!Program.AssertAllFilesPresent())
        return -3;
      if (!Program.AssertValidRuntimeVersion())
        return -4;
      Environment.CurrentDirectory = AppDomain.CurrentDomain.BaseDirectory;
      XmlConfigurator.Configure();
      Program.WarmUpSqlParserOnBackgroundThread();
      Program.EnsureReferencesToHibernatingRhinoProfilerClientGoesToThisAssembly();
      return StartupParser.ParseCommandLineArguments(args).Execute();
    }

    private static void EnsureReferencesToHibernatingRhinoProfilerClientGoesToThisAssembly()
    {
      AppDomain.CurrentDomain.AssemblyResolve += (ResolveEventHandler) ((sender, args) =>
      {
        if (!args.Name.Contains("HibernatingRhinos.Profiler.Client,"))
          return (Assembly) null;
        return typeof (Program).Assembly;
      });
    }

    private static void WarmUpSqlParserOnBackgroundThread()
    {
      Task.Factory.StartNew((Action) (() =>
      {
        Stopwatch stopwatch = Stopwatch.StartNew();
        new TGSqlParser(TDbVendor.DbVMssql)
        {
          SqlText = {
            Text = "select * from customers"
          }
        }.Parse();
        Trace.WriteLine(string.Format("Background parser first call took: {0:#,#}", (object) stopwatch.ElapsedMilliseconds));
      }));
    }

    private static bool AssertAllFilesPresent()
    {
      List<string> stringList = new List<string>();
      foreach (string requiredFile in Program.RequiredFiles)
      {
        if (!File.Exists(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, requiredFile)))
          stringList.Add(requiredFile);
      }
      if (stringList.Count == 0)
        return true;
      int num = (int) MessageBox.Show("Could not find the following required files: " + Environment.NewLine + string.Join(Environment.NewLine, stringList.ToArray()), Profile.CurrentProfileDisplayName + " - Required dependencies are not found", MessageBoxButton.OK, MessageBoxImage.Hand);
      return false;
    }

    private static bool AssertValidRuntimeVersion()
    {
      //Thread thread = new Thread(new ThreadStart(Program.ValidateRuntimeVersionOnBackgroundThread))
      //{
      //  IsBackground = true
      //};
      //thread.SetApartmentState(ApartmentState.STA);
      //thread.Start();
      try
      {
        using (IEnumerator<Version> enumerator = Program.InstalledDotNetVersions().GetEnumerator())
        {
          if (enumerator.MoveNext())
          {
            Version current = enumerator.Current;
            return true;
          }
        }
      }
      catch (Exception ex)
      {
      }
      int num = (int) MessageBox.Show("Please install .Net 4.0, this is required for the profiler user interface.", Profile.CurrentProfileDisplayName + " - Required dependencies are not found", MessageBoxButton.OK, MessageBoxImage.Hand);
      return false;
    }

    public static Collection<Version> InstalledDotNetVersions()
    {
      Collection<Version> versions = new Collection<Version>();
      RegistryKey registryKey = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\NET Framework Setup\\NDP");
      if (registryKey != null)
      {
        foreach (string subKeyName in registryKey.GetSubKeyNames())
        {
          Program.GetDotNetVersion(registryKey.OpenSubKey(subKeyName), subKeyName, versions);
          Program.GetDotNetVersion(registryKey.OpenSubKey(subKeyName).OpenSubKey("Client"), subKeyName, versions);
          Program.GetDotNetVersion(registryKey.OpenSubKey(subKeyName).OpenSubKey("Full"), subKeyName, versions);
        }
      }
      return versions;
    }

    private static void GetDotNetVersion(RegistryKey parentKey, string subVersionName, Collection<Version> versions)
    {
      if (parentKey == null || !(Convert.ToString(parentKey.GetValue("Install")) == "1"))
        return;
      string version1 = Convert.ToString(parentKey.GetValue("Version"));
      if (string.IsNullOrEmpty(version1))
        version1 = !subVersionName.StartsWith("v") ? subVersionName : subVersionName.Substring(1);
      Version version2 = new Version(version1);
      if (versions.Contains(version2))
        return;
      versions.Add(version2);
    }

    private static void ValidateRuntimeVersionOnBackgroundThread()
    {
      Thread.Sleep(Math.Abs((int) DateTime.Now.Ticks % 60000));
      Assembly assembly = typeof (Program).Assembly;
      using (Stream manifestResourceStream = assembly.GetManifestResourceStream("HibernatingRhinos.Profiler.Client.key_gen.private"))
      {
        using (BinaryReader binaryReader = new BinaryReader(manifestResourceStream))
        {
          byte[] numArray = binaryReader.ReadBytes((int) manifestResourceStream.Length);
          byte[] publicKey = assembly.GetName().GetPublicKey();
          for (int index = 0; index < publicKey.Length; ++index)
          {
            if ((int) publicKey[index] != (int) numArray[index])
              throw new InvalidOleVariantTypeException("pk");
          }
          bool pfWasVerified = false;
          if (!HostProgram.StrongNameSignatureVerificationEx(assembly.Location, true, ref pfWasVerified))
            throw new InvalidOleVariantTypeException("sn");
        }
      }
    }
  }
}
