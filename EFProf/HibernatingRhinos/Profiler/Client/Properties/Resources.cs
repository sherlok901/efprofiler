﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Properties.Resources
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.Client.Properties
{
  [CompilerGenerated]
  [DebuggerNonUserCode]
  [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
  internal class Resources
  {
    private static ResourceManager resourceMan;
    private static CultureInfo resourceCulture;

    internal Resources()
    {
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static ResourceManager ResourceManager
    {
      get
      {
        if (object.ReferenceEquals((object) HibernatingRhinos.Profiler.Client.Properties.Resources.resourceMan, (object) null))
          HibernatingRhinos.Profiler.Client.Properties.Resources.resourceMan = new ResourceManager("HibernatingRhinos.Profiler.Client.Properties.Resources", typeof (HibernatingRhinos.Profiler.Client.Properties.Resources).Assembly);
        return HibernatingRhinos.Profiler.Client.Properties.Resources.resourceMan;
      }
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static CultureInfo Culture
    {
      get
      {
        return HibernatingRhinos.Profiler.Client.Properties.Resources.resourceCulture;
      }
      set
      {
        HibernatingRhinos.Profiler.Client.Properties.Resources.resourceCulture = value;
      }
    }
  }
}
