﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Properties.Settings
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System.CodeDom.Compiler;
using System.Configuration;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.Client.Properties
{
  [GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "12.0.0.0")]
  [CompilerGenerated]
  internal sealed class Settings : ApplicationSettingsBase
  {
    private static Settings defaultInstance = (Settings) SettingsBase.Synchronized((SettingsBase) new Settings());

    public static Settings Default
    {
      get
      {
        return Settings.defaultInstance;
      }
    }

    [DebuggerNonUserCode]
    [DefaultSettingValue("")]
    [UserScopedSetting]
    public string WindowSettings
    {
      get
      {
        return (string) this[nameof (WindowSettings)];
      }
      set
      {
        this[nameof (WindowSettings)] = (object) value;
      }
    }

    [UserScopedSetting]
    [DefaultSettingValue("00000000-0000-0000-0000-000000000000")]
    [DebuggerNonUserCode]
    public string UserId
    {
      get
      {
        return (string) this[nameof (UserId)];
      }
      set
      {
        this[nameof (UserId)] = (object) value;
      }
    }

    [DebuggerNonUserCode]
    [DefaultSettingValue("")]
    [UserScopedSetting]
    public string CompactWindowSettings
    {
      get
      {
        return (string) this[nameof (CompactWindowSettings)];
      }
      set
      {
        this[nameof (CompactWindowSettings)] = (object) value;
      }
    }
  }
}
