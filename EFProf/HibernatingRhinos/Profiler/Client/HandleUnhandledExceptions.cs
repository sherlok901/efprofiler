﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.HandleUnhandledExceptions
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using HibernatingRhinos.Profiler.BackEnd.Profiles;
using HibernatingRhinos.Profiler.Client.Controls;
using HibernatingRhinos.Profiler.Client.ErrorReporting;
using HibernatingRhinos.Profiler.Client.Settings;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Threading;

namespace HibernatingRhinos.Profiler.Client
{
  public class HandleUnhandledExceptions : BootstrapperBase
  {
    private static object ErrorReportedKey = new object();
    private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof (HandleUnhandledExceptions));

    static HandleUnhandledExceptions()
    {
      AppDomain.CurrentDomain.UnhandledException += (UnhandledExceptionEventHandler) ((sender, args) => HandleUnhandledExceptions.UnhandledException((Exception) args.ExceptionObject));
      if (Application.Current == null)
        return;
      Application.Current.DispatcherUnhandledException += (DispatcherUnhandledExceptionEventHandler) ((sender, args) =>
      {
        HandleUnhandledExceptions.UnhandledException(args.Exception);
        args.Handled = true;
      });
    }

    private static void UnhandledException(Exception exceptionObject)
    {
      try
      {
        if (HandleUnhandledExceptions.HandleUltraMonError(exceptionObject))
          Environment.Exit(1);
        if (exceptionObject.Data.Contains(HandleUnhandledExceptions.ErrorReportedKey))
          return;
        exceptionObject.Data.Add(HandleUnhandledExceptions.ErrorReportedKey, HandleUnhandledExceptions.ErrorReportedKey);
        HandleUnhandledExceptions.Log.Fatal((object) "Unhandled exception ruthlessly killed application", exceptionObject);
        if (MessageBox.Show("Unhandled exception occurred: " + Environment.NewLine + exceptionObject.Message + Environment.NewLine + "Report error to support?", Profile.CurrentProfileDisplayName + ": Unhandled exception", MessageBoxButton.YesNo) == MessageBoxResult.No)
          return;
        ReportingUserInfoModel reportingUserInfoModel1 = new ReportingUserInfoModel();
        reportingUserInfoModel1.DisplayName = Profile.CurrentProfileDisplayName + ": Unhandled exception";
        reportingUserInfoModel1.Email.InitialValue = (object) (UserPreferencesHolder.UserSettings.EmailAddress ?? "");
        ReportingUserInfoModel reportingUserInfoModel2 = reportingUserInfoModel1;
        if (!Dialog.Show((object) reportingUserInfoModel2, (object) null, false))
          return;
        if (reportingUserInfoModel2.Email.ValidatedValue != null)
        {
          UserPreferencesHolder.UserSettings.EmailAddress = reportingUserInfoModel2.Email.ValidatedValue;
          UserPreferencesHolder.Save();
        }
        int num = (int) MessageBox.Show("Successfully reported unhandled exception: " + new ErrorReporter().ReportError(UserPreferencesHolder.UserSettings, exceptionObject.ToString()));
        Environment.Exit(1);
      }
      catch (Exception ex)
      {
        int num = (int) MessageBox.Show("Failed to report unhandled exception: " + Environment.NewLine + (object) exceptionObject + Environment.NewLine + "Could not report the exception because: " + ex.Message);
      }
    }

    private static bool HandleUltraMonError(Exception exception)
    {
      if (!((IEnumerable<Process>) Process.GetProcesses()).Where<Process>((Func<Process, bool>) (process => process.ProcessName.StartsWith("UltraMon"))).Any<Process>() || !(exception is OverflowException) && !(exception.InnerException is OverflowException))
        return false;
      HandleUnhandledExceptions.Log.Error((object) "UltraMon handled exception", exception);
      int num = (int) MessageBox.Show("An error occurred in " + Profile.CurrentProfileDisplayName + " which is known to be caused by using UltraMon on your computer." + Environment.NewLine + "Please follow the instructions in the following URL in order to solve this issue: " + Profile.Current.GetLearnTopic("faq", "UltraMon"));
      return true;
    }

    public HandleUnhandledExceptions()
      : base(true)
    {
    }
  }
}
