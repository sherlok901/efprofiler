﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Services.InitialTrialService
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.BackEnd.Profiles;
using HibernatingRhinos.Profiler.Client.Licensing;
using HibernatingRhinos.Profiler.Client.Util;
using System;
using System.IO;
using System.Threading;
using System.Windows;

namespace HibernatingRhinos.Profiler.Client.Services
{
  public static class InitialTrialService
  {
    private const int InitialTrialTimeInMinutes = 15;
    private static Timer timer;

    public static void StartTrial(string fullName, string email, string company)
    {
      if (!InitialTrialService.CreateTrialTicket(fullName, email, company))
        return;
      InitialTrialService.StartTrialBackgroundTimer();
    }

    public static void StartTrialBackgroundTimer()
    {
      if (InitialTrialService.timer != null)
        return;
      InitialTrialService.timer = new Timer((TimerCallback) (state => InitialTrialService.ValidateThatUserInTrialTime()));
      InitialTrialService.timer.Change(TimeSpan.FromSeconds(10.0), TimeSpan.FromMinutes(1.0));
    }

    private static void ValidateThatUserInTrialTime()
    {
      if (InitialTrialService.IsInInitialTrialMode())
        return;
      InitialTrialService.timer.Dispose();
      InitialTrialService.timer = (Timer) null;
      if (LicenseInformationModel.PromptUserForLicenseFile(Profile.CurrentProfileDisplayName + " requires a valid license. If you requested a trial, please check your email for a trial license.", true, false))
        return;
      Application.Current.MainWindow.Close();
    }

    private static bool CreateTrialTicket(string fullName, string email, string company)
    {
      if (InitialTrialService.GetTrialTicket().HasValue)
        return false;
      long binary = DateTime.Now.ToBinary();
      byte[] byteArray = Guid.NewGuid().ToByteArray();
      Array.Copy((Array) BitConverter.GetBytes(binary), 0, (Array) byteArray, 8, 8);
      string trialTicketFilePath = InitialTrialService.GetTrialTicketFilePath();
      File.WriteAllText(trialTicketFilePath, string.Format("FullName={0}; Email={1}; Company={2};", (object) fullName, (object) email, (object) company));
      AlternativeDataStreamFileUtil.Write(byteArray, trialTicketFilePath, "md5-signature");
      return true;
    }

    public static Guid? GetTrialTicket()
    {
      string trialTicketFilePath = InitialTrialService.GetTrialTicketFilePath();
      Guid guid;
      try
      {
        guid = new Guid(AlternativeDataStreamFileUtil.ReadBytes(trialTicketFilePath, "md5-signature"));
      }
      catch (Exception ex)
      {
        return new Guid?();
      }
      return new Guid?(guid);
    }

    public static string GetTrialTicketText()
    {
      string trialTicketFilePath = InitialTrialService.GetTrialTicketFilePath();
      try
      {
        return File.ReadAllText(trialTicketFilePath);
      }
      catch (Exception ex)
      {
        return (string) null;
      }
    }

    public static bool IsInInitialTrialMode()
    {
      if (!File.Exists(InitialTrialService.GetTrialTicketFilePath()))
        return false;
      Guid? trialTicket = InitialTrialService.GetTrialTicket();
      if (!trialTicket.HasValue)
        return false;
      DateTime dateTime = DateTime.FromBinary(BitConverter.ToInt64(trialTicket.Value.ToByteArray(), 8));
      if (dateTime == DateTime.MinValue)
        return false;
      TimeSpan timeSpan = DateTime.Now - dateTime;
      return timeSpan.TotalMinutes <= 15.0 && timeSpan.TotalMinutes >= 0.0;
    }

    private static string GetTrialTicketFilePath()
    {
      return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), Path.Combine(Profile.CurrentProfileDisplayName, "TrialTicket"));
    }
  }
}
