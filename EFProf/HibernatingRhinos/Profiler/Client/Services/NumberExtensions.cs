﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Services.NumberExtensions
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System;

namespace HibernatingRhinos.Profiler.Client.Services
{
  public static class NumberExtensions
  {
    private const double BytesInMegabyte = 1048576.0;

    public static double Megabytes(this int integer)
    {
      return (double) integer / 1048576.0;
    }

    public static double Megabytes(this double @double)
    {
      return @double / 1048576.0;
    }

    public static double Percent(this double @double)
    {
      return Math.Round(@double * 100.0);
    }
  }
}
