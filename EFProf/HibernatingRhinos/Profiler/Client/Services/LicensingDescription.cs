﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Services.LicensingDescription
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.Client.Settings;
using Rhino.Licensing;
using System;
using System.IO;

namespace HibernatingRhinos.Profiler.Client.Services
{
  public class LicensingDescription
  {
    public static string PublicKey
    {
      get
      {
        using (Stream manifestResourceStream = typeof (LicenseValidator).Assembly.GetManifestResourceStream(typeof (BuildInfo).Namespace + ".Key.public"))
        {
          using (StreamReader streamReader = new StreamReader(manifestResourceStream))
            return streamReader.ReadToEnd();
        }
      }
    }

    public static string DefaultLicenseFile
    {
      get
      {
        return Path.Combine(UserPreferencesHolder.SettingsDirectory, "license.xml");
      }
    }

    public static string GetLocalLicenseFile()
    {
      if (!string.IsNullOrWhiteSpace(LicensingDescription.CustomLicensePath))
      {
        if (!File.Exists(LicensingDescription.CustomLicensePath))
          throw new InvalidOperationException("Can find a license in " + LicensingDescription.CustomLicensePath);
        return LicensingDescription.CustomLicensePath;
      }
      string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "license.xml");
      if (!File.Exists(path))
        return LicensingDescription.DefaultLicenseFile;
      return path;
    }

    public static string CustomLicensePath { get; set; }
  }
}
