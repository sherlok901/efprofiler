﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Services.ProfilerLicenseValidator
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.BackEnd.Profiles;
using HibernatingRhinos.Profiler.Client.Infrastructure.AutoUpdate;
using Rhino.Licensing;
using Rhino.Licensing.Discovery;
using System;
using System.Globalization;

namespace HibernatingRhinos.Profiler.Client.Services
{
  public class ProfilerLicenseValidator : LicenseValidator
  {
    private const string SubscriptionEndpointUrl = "http://uberprof.com/Subscriptions.svc";

    public ProfilerLicenseValidator(string publicKey, string licensePath)
      : base(publicKey, licensePath)
    {
      this.Initialize();
    }

    public ProfilerLicenseValidator(string publicKey, string licensePath, string licenseServerUrl, Guid userId)
      : base(publicKey, licensePath, licenseServerUrl, userId)
    {
      this.Initialize();
    }

    private void Initialize()
    {
      this.SubscriptionEndpoint = "http://uberprof.com/Subscriptions.svc";
      this.LicenseInvalidated += new Action<InvalidationType>(this.OnLicenseInvalidated);
      this.MultipleLicensesWereDiscovered += new EventHandler<DiscoveryHost.ClientDiscoveredEventArgs>(this.OnMultipleLicensesWereDiscovered);
    }

    private void OnMultipleLicensesWereDiscovered(object sender, DiscoveryHost.ClientDiscoveredEventArgs clientDiscoveredEventArgs)
    {
      AbstractLicenseValidator.Logger.ErrorFormat("A duplicate license was found at {0} for user {1}. User Id: {2}. Both licenses were disabled!", (object) clientDiscoveredEventArgs.MachineName, (object) clientDiscoveredEventArgs.UserName, (object) clientDiscoveredEventArgs.UserId);
    }

    private void OnLicenseInvalidated(InvalidationType invalidationType)
    {
      AbstractLicenseValidator.Logger.Error((object) "The license have expired and can no longer be used");
    }

    public override void AssertValidLicense()
    {
      base.AssertValidLicense();
      string str1;
      if (!this.LicenseAttributes.TryGetValue("prof", out str1) || !string.Equals(str1, "Uber", StringComparison.OrdinalIgnoreCase) && !Profile.Current.IsMatch(str1))
      {
        AbstractLicenseValidator.Logger.WarnFormat("Not accepting '{0}' license for '{1}'", (object) str1, (object) Profile.CurrentProfileDisplayName);
        throw new NotValidLicenseFileException("The license you provided isn't valid for this application." + Environment.NewLine + "License is for: '" + str1 + "' but the current application profile is: " + Profile.CurrentProfileDisplayName);
      }
      if (this.LicenseType != LicenseType.Subscription && this.LicenseType != LicenseType.Trial)
      {
        if (this.LicenseType == LicenseType.Floating)
        {
          string s;
          DateTime result;
          if (this.LicenseAttributes.TryGetValue("FloatingLicenseExpiration", out s) && DateTime.TryParseExact(s, "yyyy-MM-ddTHH:mm:ss.fffffff", (IFormatProvider) CultureInfo.InvariantCulture, DateTimeStyles.None, out result))
          {
            this.ExpirationDateFloatingLicense = result;
            AbstractLicenseValidator.Logger.DebugFormat("Floating license expiration on: {0}", (object) this.ExpirationDateFloatingLicense);
          }
          string str2;
          if (this.LicenseAttributes.TryGetValue("FloatingLicenseType", out str2) && str2 == LicenseType.Subscription.ToString())
          {
            AbstractLicenseValidator.Logger.DebugFormat("Floating license accepted for {0}", (object) Profile.CurrentProfileDisplayName);
            return;
          }
        }
        string str3;
        if (!this.LicenseAttributes.TryGetValue("version", out str3) || str3 != "3")
        {
          AbstractLicenseValidator.Logger.WarnFormat("Not accepting license of version {0}. A license for version {1} is required.", (object) (str3 ?? "1"), (object) "3");
          throw new LicenseUpgradeRequiredException(string.Format("Your license is only valid for version {0} of {1}. You need to upgrade your license in order to work with version {2}.", (object) (str3 ?? "1"), (object) Profile.CurrentProfileDisplayName, (object) "3"));
        }
        string s1;
        DateTime result1;
        if (!this.LicenseAttributes.TryGetValue("updatesExpiration", out s1) || !DateTime.TryParseExact(s1, "yyyy-MMM-dd", (IFormatProvider) CultureInfo.InstalledUICulture, DateTimeStyles.None, out result1))
          result1 = new DateTime(2013, 12, 29);
        if (result1 < DateTime.UtcNow)
        {
          AbstractLicenseValidator.Logger.WarnFormat("Auto updates stopped due to a license older than 12 months. Updates expiration: {0} ({1})", (object) s1, (object) result1.ToString("O"));
          ProfilerAutoUpdate.IsLicenseDoesNotAllowAutoUpdate = true;
        }
      }
      AbstractLicenseValidator.Logger.DebugFormat("License accepted for {0}", (object) Profile.CurrentProfileDisplayName);
    }
  }
}
