﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Services.ConnectionBuilder
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.BackEnd.Infrastructure;
using log4net;
using System;
using System.Data;
using System.Reflection;

namespace HibernatingRhinos.Profiler.Client.Services
{
  public static class ConnectionBuilder
  {
    private static readonly ILog Log = LogManager.GetLogger(typeof (ConnectionBuilder));

    private static Type GetTypeForConnection(ConnectionInfo connectionInfo)
    {
      if (string.IsNullOrEmpty(connectionInfo.FullPathToAssembly))
        return Type.GetType(connectionInfo.ConnectionTypeName);
      return Assembly.LoadFrom(connectionInfo.FullPathToAssembly).GetType(connectionInfo.ConnectionTypeName);
    }

    public static ConnectionBuilder.Result BuildConnectionFromInfo(ConnectionInfo connectionInfo)
    {
      if (connectionInfo == null)
        return new ConnectionBuilder.Result("Can not execute query when connection info is null.");
      if (string.IsNullOrEmpty(connectionInfo.ConnectionString))
        return new ConnectionBuilder.Result("Can not execute query when connection string is not valid.");
      if (string.IsNullOrEmpty(connectionInfo.ConnectionTypeName))
        return new ConnectionBuilder.Result("Can not execute query when the connection type is not specified.");
      Type typeForConnection;
      try
      {
        typeForConnection = ConnectionBuilder.GetTypeForConnection(connectionInfo);
      }
      catch (Exception ex)
      {
        return new ConnectionBuilder.Result(string.Format("Could not load connection type: {0}. {1}", (object) connectionInfo.ConnectionTypeName, (object) ex.ToString()));
      }
      if (typeForConnection == (Type) null)
        return new ConnectionBuilder.Result("Could not find connection type: " + connectionInfo.ConnectionTypeName);
      try
      {
        return new ConnectionBuilder.Result((IDbConnection) Activator.CreateInstance(typeForConnection));
      }
      catch (TypeInitializationException ex)
      {
        Exception exception = (Exception) ex;
        if (ex.InnerException != null && ex.InnerException.GetType().Name == "OracleException" && ex.InnerException.Message == "The provider is not compatible with the version of Oracle client")
          exception = (Exception) new InvalidOperationException("OracleException: " + ex.InnerException.Message + Environment.NewLine + "Fix instructions:" + Environment.NewLine + "1. Go to Oracle website and download the '64-bit Oracle Data Access Components (ODAC)'" + Environment.NewLine + "2. Install it." + Environment.NewLine + "3. Target the x64 platform in your project." + Environment.NewLine + "4. Replace all of the Oracle.DataAccess.dll references in your projects with the x64 version. Tip: You can use the 'odp.net.x64' nuget package.", (Exception) ex);
        ConnectionBuilder.Log.Error((object) exception);
        return new ConnectionBuilder.Result("Could not create an instance of " + connectionInfo.ConnectionTypeName)
        {
          Exception = exception
        };
      }
      catch (Exception ex)
      {
        ConnectionBuilder.Log.Error((object) ex);
        return new ConnectionBuilder.Result("Could not create an instance of " + connectionInfo.ConnectionTypeName)
        {
          Exception = ex
        };
      }
    }

    public class Result
    {
      public Result(string error)
      {
        this.Error = error;
      }

      public Result(IDbConnection connection)
      {
        this.Connection = connection;
      }

      public IDbConnection Connection { get; set; }

      public bool WasSuccessful
      {
        get
        {
          return this.Connection != null;
        }
      }

      public string Error { get; set; }

      public Exception Exception { get; set; }
    }
  }
}
