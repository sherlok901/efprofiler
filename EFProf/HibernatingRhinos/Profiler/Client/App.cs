﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.App
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using AqiStar;
using Caliburn.Micro;
using HibernatingRhinos.Profiler.BackEnd;
using log4net.Config;
using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Windows;

namespace HibernatingRhinos.Profiler.Client
{
  public partial class App : Application
  {
    protected static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof (App));
    //private bool _contentLoaded;

    public App()
    {
      this.ShutdownMode = ShutdownMode.OnExplicitShutdown;
      Environment.CurrentDirectory = AppDomain.CurrentDomain.BaseDirectory;
      XmlConfigurator.Configure();
      App.Log.Debug((object) ("Profiler initialized. Build: " + BuildInfo.GetCurrentBuild()));
      new ProfilerBootstrapper().Initialize();
    }

    protected override void OnExit(ExitEventArgs e)
    {
      BackendBridge backendBridge = IoC.Get<BackendBridge>((string) null);
      if (backendBridge != null)
        backendBridge.Dispose();
      base.OnExit(e);
    }

    protected override void OnStartup(StartupEventArgs e)
    {
      TextBox.LicenseKey = "AQTBL-2XQCJ-W8VT5-NP3XX-KT5UA";
      base.OnStartup(e);
    }

    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //public void InitializeComponent()
    //{
    //  if (this._contentLoaded)
    //    return;
    //  this._contentLoaded = true;
    //  Application.LoadComponent((object) this, new Uri("/EFProf;component/app.xaml", UriKind.Relative));
    //}

    //[STAThread]
    ////[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //public static void Main()
    //{
    //  App app = new App();
    //  app.InitializeComponent();
    //  app.Run();
    //}
  }
}
