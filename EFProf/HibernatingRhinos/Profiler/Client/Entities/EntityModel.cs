﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Entities.EntityModel
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using HibernatingRhinos.Profiler.Client.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace HibernatingRhinos.Profiler.Client.Entities
{
  [Serializable]
  public class EntityModel : SelectionBase
  {
    private IList<string> identifiers;

    public EntityModel(EntitySnapshot entitySnapshot)
    {
      this.DisplayName = entitySnapshot.Name;
      this.Identifiers = (IList<string>) new List<string>((IEnumerable<string>) entitySnapshot.Identifiers);
    }

    public IList<string> Identifiers
    {
      get
      {
        return this.identifiers;
      }
      set
      {
        this.identifiers = value;
        this.NotifyOfPropertyChange<IList<string>>((Expression<Func<IList<string>>>) (() => this.Identifiers));
      }
    }
  }
}
