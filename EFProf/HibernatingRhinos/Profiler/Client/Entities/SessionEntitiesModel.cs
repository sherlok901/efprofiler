﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Entities.SessionEntitiesModel
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using HibernatingRhinos.Profiler.Client.Infrastructure;
using HibernatingRhinos.Profiler.Client.Sessions;
using HibernatingRhinos.Profiler.Client.Tracking;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;

namespace HibernatingRhinos.Profiler.Client.Entities
{
  public class SessionEntitiesModel : PollingScreen, INavigateItems
  {
    private readonly SessionModel sessionModel;
    private readonly ITrackingService trackingService;
    private EntityModel selectedItem;
    private bool updated;

    public SessionEntitiesModel(SessionModel sessionModel, ITrackingService trackingService)
    {
      this.DisplayName = "Entities";
      this.sessionModel = sessionModel;
      this.trackingService = trackingService;
      this.EntityModels = new ObservableCollection<EntityModel>();
      this.Update();
    }

    public ObservableCollection<EntityModel> EntityModels { get; set; }

    public bool DoesNotHaveEntities
    {
      get
      {
        if (this.updated)
          return this.EntityModels.Count < 1;
        return false;
      }
    }

    public EntityModel SelectedItem
    {
      get
      {
        return this.selectedItem;
      }
      set
      {
        if (this.selectedItem != null)
          this.selectedItem.IsSelected = false;
        this.selectedItem = value;
        if (this.selectedItem != null)
          this.selectedItem.IsSelected = true;
        this.NotifyOfPropertyChange<EntityModel>((Expression<Func<EntityModel>>) (() => this.SelectedItem));
      }
    }

    public void NextItem()
    {
      this.SelectedItem = NavigationManager.GetNext<EntityModel>((IList<EntityModel>) this.EntityModels, this.SelectedItem);
    }

    public void PreviousItem()
    {
      this.SelectedItem = NavigationManager.GetPrevious<EntityModel>((IList<EntityModel>) this.EntityModels, this.SelectedItem);
    }

    protected override void OnActivate()
    {
      this.trackingService.Track("Sessions", "Entities", (string) null, new int?());
      base.OnActivate();
    }

    public override void TimerTicked(object sender, EventArgs e)
    {
      if (this.State == PollingState.Finalized)
      {
        PollingService.UnRegister(new EventHandler(((PollingScreen) this).TimerTicked));
      }
      else
      {
        this.Update();
        if (this.State != PollingState.NotFinalized)
          return;
        this.State = PollingState.Finalized;
      }
    }

    public override void ForceUpdate()
    {
      this.Update();
      if (this.State != PollingState.NotFinalized)
        return;
      this.State = PollingState.Finalized;
    }

    public void Update()
    {
      foreach (EntitySnapshot entitySnapshot1 in this.sessionModel.Session.EntitySnapshots)
      {
        EntitySnapshot entitySnapshot = entitySnapshot1;
        EntityModel entityModel = this.EntityModels.FirstOrDefault<EntityModel>((Func<EntityModel, bool>) (model => model.DisplayName == entitySnapshot.Name));
        if (entityModel == null)
          this.EntityModels.Add(new EntityModel(entitySnapshot));
        else
          entityModel.Identifiers = (IList<string>) new List<string>((IEnumerable<string>) entitySnapshot.Identifiers);
      }
      this.updated = true;
      this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.DoesNotHaveEntities));
      if (this.NotifyOnUpdate == null)
        return;
      this.NotifyOnUpdate();
    }
  }
}
