﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Shell.ApplicationViews.Full
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.Client.Infrastructure;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace HibernatingRhinos.Profiler.Client.Shell.ApplicationViews
{
  public partial class Full : UserControl, IComponentConnector
  {
    //internal Border Header;
    //internal ContentControl MainMenu;
    //internal Border ChromeControls;
    //internal ApplicationToolbar Toolbar;
    //internal Border Footer;
    //internal ContentControl Navigator;
    //internal ContentControl Statistics;
    //internal ContentControl ActiveItem;
    //internal ContentControl TrackingService;
    //private bool _contentLoaded;

    public Full()
    {
      this.InitializeComponent();
      this.Loaded += new RoutedEventHandler(this.OnLoaded);
    }

    private void OnLoaded(object sender, RoutedEventArgs e)
    {
      NavigationManager.Initialize((FrameworkElement) this, (DependencyObject) (this.Navigator.Content as NavigatorView).Items);
    }

    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //public void InitializeComponent()
    //{
    //  if (this._contentLoaded)
    //    return;
    //  this._contentLoaded = true;
    //  Application.LoadComponent((object) this, new Uri("/EFProf;component/shell/applicationviews/full.xaml", UriKind.Relative));
    //}

    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //internal Delegate _CreateDelegate(Type delegateType, string handler)
    //{
    //  return Delegate.CreateDelegate(delegateType, (object) this, handler);
    //}

    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //[EditorBrowsable(EditorBrowsableState.Never)]
    //void IComponentConnector.Connect(int connectionId, object target)
    //{
    //  switch (connectionId)
    //  {
    //    case 1:
    //      this.Header = (Border) target;
    //      break;
    //    case 2:
    //      this.MainMenu = (ContentControl) target;
    //      break;
    //    case 3:
    //      this.ChromeControls = (Border) target;
    //      break;
    //    case 4:
    //      this.Toolbar = (ApplicationToolbar) target;
    //      break;
    //    case 5:
    //      this.Footer = (Border) target;
    //      break;
    //    case 6:
    //      this.Navigator = (ContentControl) target;
    //      break;
    //    case 7:
    //      this.Statistics = (ContentControl) target;
    //      break;
    //    case 8:
    //      this.ActiveItem = (ContentControl) target;
    //      break;
    //    case 9:
    //      this.TrackingService = (ContentControl) target;
    //      break;
    //    default:
    //      this._contentLoaded = true;
    //      break;
    //  }
    //}
  }
}
