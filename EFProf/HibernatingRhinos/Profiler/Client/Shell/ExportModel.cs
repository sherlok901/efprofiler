﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Shell.ExportModel
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.BackEnd.Profiles;
using HibernatingRhinos.Profiler.Client.Reports;
using HibernatingRhinos.Profiler.Client.Tracking;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Linq;

namespace HibernatingRhinos.Profiler.Client.Shell
{
  public class ExportModel
  {
    private readonly ApplicationModel applicationModel;
    private readonly ITrackingService trackingService;

    public ExportModel(ApplicationModel applicationModel, ITrackingService trackingService)
    {
      this.applicationModel = applicationModel;
      this.trackingService = trackingService;
    }

    public void ExportModelAsHtml(string fileName)
    {
      this.trackingService.Track("Reports", "Export", "Html", new int?());
      string reportTemplate = this.GetReportTemplate();
      StringBuilder stringBuilder = new StringBuilder();
      foreach (IReport report in (IEnumerable<IReport>) this.applicationModel.Navigator.Reports.Items)
      {
        report.TimerTicked((object) this, EventArgs.Empty);
        string jsonString = report.ExportReportToJsonString();
        stringBuilder.Append("var ").Append(report.JsonReportName).Append(" = ").Append(jsonString).AppendLine(";").AppendLine();
      }
      stringBuilder.Append("var queriesBySession = ").Append(this.applicationModel.Navigator.Sessions.ExportToJsonString()).AppendLine(";");
      stringBuilder.AppendLine("var supportedFeatures = ");
      stringBuilder.AppendLine("{");
      foreach (SupportedFeatures feature in Enum.GetValues(typeof (SupportedFeatures)))
        stringBuilder.Append("\t").Append((object) feature).Append(": ").Append(Profile.Current.Supports(feature).ToString().ToLower()).AppendLine(",");
      stringBuilder.Append("\t").Append("profileName: '").Append(Profile.CurrentProfile).AppendLine("'");
      stringBuilder.AppendLine("};");
      File.WriteAllText(fileName, reportTemplate.Replace("/*{{report-css}}*/", this.GetExportCss()).Replace("{{report-name}}", Profile.CurrentProfile).Replace("{{report-title}}", Profile.CurrentProfileDisplayName).Replace("{{report-date}}", DateTime.Now.ToString()).Replace("{{report-root-url}}", Profile.Current.HomeUrl).Replace("{{reports-as-json}}", stringBuilder.ToString()));
    }

    private string GetExportCss()
    {
      using (Stream manifestResourceStream = this.GetType().Assembly.GetManifestResourceStream("HibernatingRhinos.Profiler.Client.Exports." + Profile.CurrentProfile + ".css"))
      {
        using (StreamReader streamReader = new StreamReader(manifestResourceStream))
          return streamReader.ReadToEnd();
      }
    }

    private string GetReportTemplate()
    {
      using (Stream manifestResourceStream = this.GetType().Assembly.GetManifestResourceStream("HibernatingRhinos.Profiler.Client.Exports.ReportTemplate.html"))
      {
        using (StreamReader streamReader = new StreamReader(manifestResourceStream))
          return streamReader.ReadToEnd();
      }
    }

    public void ExportModelAsXml(string file)
    {
      this.trackingService.Track("Reports", "Export", "Xml", new int?());
      XElement xelement = new XElement(ReportBase.Namespace + "report");
      xelement.Add((object) this.applicationModel.Navigator.Sessions.Export());
      foreach (IReport report in (IEnumerable<IReport>) this.applicationModel.Navigator.Reports.Items)
      {
        report.TimerTicked((object) this, EventArgs.Empty);
        xelement.Add((object) report.ExportReport());
      }
      new XDocument(new object[1]{ (object) xelement }).Save(file);
    }
  }
}
