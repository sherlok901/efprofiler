﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Shell.NavigatorModel
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using HibernatingRhinos.Profiler.Client.Reports;
using HibernatingRhinos.Profiler.Client.Sessions;
using System;

namespace HibernatingRhinos.Profiler.Client.Shell
{
  public class NavigatorModel : Conductor<IScreen>.Collection.OneActive
  {
    public NavigatorModel(SessionListModel sessions, ReportListModel reports)
    {
      this.Sessions = sessions;
      this.Reports = reports;
    }

    public SessionListModel Sessions { get; private set; }

    public ReportListModel Reports { get; private set; }

    protected override void OnInitialize()
    {
      this.Items.Add((IScreen) this.Sessions);
      this.Items.Add((IScreen) this.Reports);
      this.ActivateItem((IScreen) this.Sessions);
      base.OnInitialize();
    }

    public void Clear()
    {
      this.Sessions.Clear();
      this.Reports.Clear();
    }

    public void ClearExcept(Guid[] sessionIds)
    {
      this.Sessions.ClearExcept(sessionIds);
      this.Reports.Clear();
    }
  }
}
