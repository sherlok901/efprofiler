﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Shell.AttachedApplicationInstance
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System;

namespace HibernatingRhinos.Profiler.Client.Shell
{
  public class AttachedApplicationInstance
  {
    public Guid Id { get; set; }

    public override bool Equals(object obj)
    {
      AttachedApplicationInstance applicationInstance = obj as AttachedApplicationInstance;
      if (applicationInstance == null)
        return false;
      return this.Id == applicationInstance.Id;
    }

    public override int GetHashCode()
    {
      return this.Id.GetHashCode();
    }
  }
}
