﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Shell.ApplicationMenuModel
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using HibernatingRhinos.Profiler.BackEnd.Profiles;
using HibernatingRhinos.Profiler.Client.Licensing;
using HibernatingRhinos.Profiler.Client.Reports;
using System.Collections;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace HibernatingRhinos.Profiler.Client.Shell
{
  public class ApplicationMenuModel
  {
    private readonly List<MenuItem> reportMenuItems = new List<MenuItem>();
    private readonly LicenseInformationModel licenseInfo;

    public ApplicationMenuModel(NavigatorModel navigatorModel, LicenseInformationModel licenseInformation)
    {
      this.BuildReportsMenu(navigatorModel);
      this.licenseInfo = licenseInformation;
    }

    public string CommercialSupportUrl
    {
      get
      {
        return Profile.Current.CommercialSupportUrl;
      }
    }

    public string AboutUrl
    {
      get
      {
        return Profile.Current.AboutUrl;
      }
    }

    public IEnumerable ReportMenuItems
    {
      get
      {
        return (IEnumerable) this.reportMenuItems;
      }
    }

    public LicenseInformationModel LicenseInfo
    {
      get
      {
        return this.licenseInfo;
      }
    }

    private void BuildReportsMenu(NavigatorModel navigatorModel)
    {
      foreach (IReport report in (IEnumerable<IReport>) navigatorModel.Reports.Items)
      {
        IReport theReport = report;
        MenuItem menuItem = new MenuItem();
        menuItem.Header = (object) theReport.DisplayName;
        menuItem.Click += (RoutedEventHandler) ((param0, param1) =>
        {
          navigatorModel.ActivateItem((IScreen) navigatorModel.Reports);
          navigatorModel.Reports.ActivateItem(theReport);
        });
        this.reportMenuItems.Add(menuItem);
      }
    }
  }
}
