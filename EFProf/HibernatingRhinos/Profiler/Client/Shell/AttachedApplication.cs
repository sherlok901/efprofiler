﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Shell.AttachedApplication
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace HibernatingRhinos.Profiler.Client.Shell
{
  public class AttachedApplication : PropertyChangedBase
  {
    private readonly IList<AttachedApplicationInstance> registeredInstances = (IList<AttachedApplicationInstance>) new List<AttachedApplicationInstance>();
    private readonly ApplicationModel applicationModel;

    public string Name { get; private set; }

    public AttachedApplication(ApplicationModel applicationModel, string name)
    {
      this.Name = name;
      this.applicationModel = applicationModel;
    }

    public bool IsProductionProfiling { get; set; }

    public string DisplayName
    {
      get
      {
        string str = (this.IsProductionProfiling ? "Prod: " : "") + this.Name;
        if (this.registeredInstances.Count > 1)
          str = str + "[#" + (object) this.registeredInstances.Count + "]";
        return str;
      }
    }

    public ICollection<AttachedApplicationInstance> Instances
    {
      get
      {
        return (ICollection<AttachedApplicationInstance>) this.registeredInstances;
      }
    }

    public string Details
    {
      get
      {
        StringBuilder stringBuilder = new StringBuilder();
        foreach (AttachedApplicationInstance registeredInstance in (IEnumerable<AttachedApplicationInstance>) this.registeredInstances)
          stringBuilder.Append((object) registeredInstance);
        return stringBuilder.ToString();
      }
    }

    public void RegisterNewInstance(Guid instanceIdentifier)
    {
      AttachedApplicationInstance applicationInstance = new AttachedApplicationInstance()
      {
        Id = instanceIdentifier
      };
      if (this.registeredInstances.Contains(applicationInstance))
        return;
      this.registeredInstances.Add(applicationInstance);
      this.RefreshDisplay();
    }

    public void UnregisterInstance(Guid instanceIdentifier)
    {
      AttachedApplicationInstance applicationInstance = new AttachedApplicationInstance()
      {
        Id = instanceIdentifier
      };
      if (!this.registeredInstances.Contains(applicationInstance))
        return;
      this.registeredInstances.Remove(applicationInstance);
      if (this.registeredInstances.Count == 0)
        this.applicationModel.AttachedApplications.Remove(this);
      this.RefreshDisplay();
    }

    private void RefreshDisplay()
    {
      this.NotifyOfPropertyChange<string>((Expression<Func<string>>) (() => this.DisplayName));
      this.NotifyOfPropertyChange<string>((Expression<Func<string>>) (() => this.Details));
    }
  }
}
