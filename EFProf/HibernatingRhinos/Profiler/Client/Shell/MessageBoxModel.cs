﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Shell.MessageBoxModel
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.Client.Infrastructure;
using System;
using System.Linq.Expressions;

namespace HibernatingRhinos.Profiler.Client.Shell
{
  public class MessageBoxModel : ProfilerScreen
  {
    private string message;

    public MessageBoxModel(string message)
    {
      this.DisplayName = nameof (Message);
      this.Message = message;
    }

    public MessageBoxModel(string displayName, string message)
    {
      this.DisplayName = displayName;
      this.Message = message;
    }

    public string Message
    {
      get
      {
        return this.message;
      }
      set
      {
        this.message = value;
        this.NotifyOfPropertyChange<string>((Expression<Func<string>>) (() => this.Message));
      }
    }

    public void Close()
    {
      this.TryClose();
    }
  }
}
