﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Shell.ApplicationModel
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.BackEnd.Extensions;
using HibernatingRhinos.Profiler.BackEnd.Infrastructure;
using HibernatingRhinos.Profiler.BackEnd.Infrastructure.Connections;
using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using HibernatingRhinos.Profiler.BackEnd.Profiles;
using HibernatingRhinos.Profiler.BackEnd.Reports;
using HibernatingRhinos.Profiler.Client.Connections;
using HibernatingRhinos.Profiler.Client.Controls;
using HibernatingRhinos.Profiler.Client.Coroutines;
using HibernatingRhinos.Profiler.Client.Filtering;
using HibernatingRhinos.Profiler.Client.Infrastructure;
using HibernatingRhinos.Profiler.Client.Infrastructure.AutoUpdate;
using HibernatingRhinos.Profiler.Client.Infrastructure.Monitors;
using HibernatingRhinos.Profiler.Client.Licensing;
using HibernatingRhinos.Profiler.Client.Queries;
using HibernatingRhinos.Profiler.Client.Queries.QueryPlan;
using HibernatingRhinos.Profiler.Client.Reports;
using HibernatingRhinos.Profiler.Client.Services;
using HibernatingRhinos.Profiler.Client.Sessions;
using HibernatingRhinos.Profiler.Client.Sessions.SessionListViews;
using HibernatingRhinos.Profiler.Client.Settings;
using HibernatingRhinos.Profiler.Client.Statements;
using HibernatingRhinos.Profiler.Client.Statistics;
using HibernatingRhinos.Profiler.Client.Tracking;
using Microsoft.Expression.Interactivity.Core;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using System.Xml.Linq;

namespace HibernatingRhinos.Profiler.Client.Shell
{
  public class ApplicationModel : Conductor<IScreen>.Collection.AllActive, IHandle<UserPreferences>, IHandle
  {
    private readonly ILog logger = LogManager.GetLog(typeof (ApplicationModel));
    private readonly ReaderWriterLockSlim sessionsLock = new ReaderWriterLockSlim();
    public readonly Dictionary<Guid, Session> SessionsInternal = new Dictionary<Guid, Session>();
    private readonly IList<IConnectionDefinition> availableConnection = Profile.Current.GetAvailableConnectionTypes();
    private readonly object lockSetSelectedConn = new object();
    private const int MaxRecentStatements = 500;
    private readonly BackendBridge backendBridge;
    private bool isCurrentlyPaused;
    private readonly Func<bool> bringToFront;
    private ISessionItem navigatedSession;
    private RelatedSessionSpecification relatedSessionSpecification;
    private readonly IWindowManager windowManager;
    private readonly IEventAggregator eventAggregator;
    private bool isDetectConnectionString;
    private bool isTrackLatestStatements;
    private long backendNotificationsEtag;
    private int recentStatementSnapshotsPosition;
    public int RecentStatementsEtag;
    private string windowStyle;
    private bool isCompactViewAlwaysOnTop;
    private bool isStatementDetailsPaneShownInCompactView;

    public ObservableCollection<AttachedApplication> AttachedApplications { get; set; }

    public ObservableCollection<DisplayInfo> Displays { get; private set; }

    public ApplicationModel(BackendBridge backendBridge, ProfilerAutoUpdate autoUpdate, ApplicationMenuModel mainMenu, NavigatorModel navigator, StatisticsModel statistics, LicenseInformationModel licenseInformation, FilterServiceModel filterService, ITrackingService trackingService, IEventAggregator events, IWindowManager windowManager, IEventAggregator eventAggregator, RecentStatementsModel recentStatements)
      : base(false)
    {
      this.backendBridge = backendBridge;
      this.AutoUpdate = autoUpdate;
      this.MainMenu = mainMenu;
      this.Navigator = navigator;
      this.Statistics = statistics;
      this.FilterService = filterService;
      this.TrackingService = trackingService;
      this.windowManager = windowManager;
      this.eventAggregator = eventAggregator;
      this.RecentStatements = recentStatements;
      events.Subscribe((object) this);
      this.AttachedApplications = new ObservableCollection<AttachedApplication>();
      this.RecentStatementSnapshots = new Dictionary<Guid, StatementSnapshot>();
      navigator.PropertyChanged += (PropertyChangedEventHandler) ((s, e) =>
      {
        if (!(e.PropertyName == nameof (ActiveItem)))
          return;
        this.NotifyOfPropertyChange<IScreen>((System.Linq.Expressions.Expression<Func<IScreen>>) (() => this.ActiveItem));
      });
      this.LicenseInfo = licenseInformation;
      DisplayManager.LoadDisplays();
      this.Displays = DisplayManager.Displays;
      this.IsCompactViewAlwaysOnTop = UserPreferencesHolder.UserSettings.IsCompactViewAlwaysOnTop;
      this.IsStatementDetailsPaneShownInCompactView = UserPreferencesHolder.UserSettings.IsStatementDetailsPaneShownInCompactView;
      this.IsTrackLatestStatements = UserPreferencesHolder.UserSettings.IsTrackLatestStatements;
      this.IsDetectConnectionString = UserPreferencesHolder.UserSettings.IsDetectConnectionString;
      this.WindowStyle = "Full";
      if (Application.Current == null)
        return;
      Application.Current.Deactivated += new EventHandler(this.OnApplicationDeactivated);
    }

    private void OnApplicationDeactivated(object sender, EventArgs e)
    {
      IObservableCollection<ISessionItem> sessions = this.Navigator.Sessions.Items;
      SessionModel lastKnownSession = this.Navigator.Sessions.Sessions.LastOrDefault<SessionModel>();
      NotifyCollectionChangedEventHandler sessionsOnCollectionChanged = (NotifyCollectionChangedEventHandler) null;
      sessionsOnCollectionChanged = (NotifyCollectionChangedEventHandler) ((_, __) =>
      {
        if (!this.SessionListChanged(lastKnownSession))
          return;
        sessions.CollectionChanged -= sessionsOnCollectionChanged;
      });
      sessions.CollectionChanged += sessionsOnCollectionChanged;
    }

    private bool SessionListChanged(SessionModel lastKnownSession)
    {
      IEnumerable<SessionModel> sessions = this.Navigator.Sessions.Sessions;
      if (lastKnownSession == null || lastKnownSession.HasSeparator)
        return false;
      SessionModel sessionModel = sessions.LastOrDefault<SessionModel>();
      if (sessionModel == null || sessionModel == lastKnownSession || sessionModel.HasSeparator)
        return false;
      lastKnownSession.HasSeparator = true;
      return true;
    }

    public ProfilerAutoUpdate AutoUpdate { get; private set; }

    public ApplicationMenuModel MainMenu { get; private set; }

    public NavigatorModel Navigator { get; private set; }

    public StatisticsModel Statistics { get; private set; }

    public LicenseInformationModel LicenseInfo { get; private set; }

    public FilterServiceModel FilterService { get; private set; }

    public string WindowStyle
    {
      get
      {
        return this.windowStyle;
      }
      private set
      {
        if (value == this.windowStyle)
          return;
        this.windowStyle = value;
        this.NotifyOfPropertyChange(nameof (WindowStyle));
        this.NotifyOfPropertyChange<bool>((System.Linq.Expressions.Expression<Func<bool>>) (() => this.IsWindowTopMost));
      }
    }

    public IScreen ActiveItem
    {
      get
      {
        return this.Navigator.ActiveItem;
      }
    }

    public bool IsCurrentlyPaused
    {
      get
      {
        return this.isCurrentlyPaused;
      }
      set
      {
        this.isCurrentlyPaused = value;
        this.NotifyOfPropertyChange<bool>((System.Linq.Expressions.Expression<Func<bool>>) (() => this.IsCurrentlyPaused));
      }
    }

    public bool IsRecording
    {
      get
      {
        return !this.IsCurrentlyPaused;
      }
      set
      {
        this.TrackingService.Track("Main", "Toggle Listening", this.IsCurrentlyPaused ? "Off" : "On", new int?());
        this.IsCurrentlyPaused = !value;
        if (this.IsCurrentlyPaused)
          this.backendBridge.Pause();
        else
          this.backendBridge.Resume();
        this.NotifyOfPropertyChange<bool>((System.Linq.Expressions.Expression<Func<bool>>) (() => this.IsRecording));
      }
    }

    public bool IsCompactViewAlwaysOnTop
    {
      get
      {
        return this.isCompactViewAlwaysOnTop;
      }
      set
      {
        this.isCompactViewAlwaysOnTop = value;
        UserPreferencesHolder.UserSettings.IsCompactViewAlwaysOnTop = value;
        UserPreferencesHolder.Save();
        this.NotifyOfPropertyChange(nameof (IsCompactViewAlwaysOnTop));
        this.NotifyOfPropertyChange<bool>((System.Linq.Expressions.Expression<Func<bool>>) (() => this.IsWindowTopMost));
      }
    }

    public bool IsStatementDetailsPaneShownInCompactView
    {
      get
      {
        return this.isStatementDetailsPaneShownInCompactView;
      }
      set
      {
        this.isStatementDetailsPaneShownInCompactView = value;
        UserPreferencesHolder.UserSettings.IsStatementDetailsPaneShownInCompactView = value;
        UserPreferencesHolder.Save();
        this.NotifyOfPropertyChange<bool>((System.Linq.Expressions.Expression<Func<bool>>) (() => this.IsStatementDetailsPaneShownInCompactView));
      }
    }

    public bool IsWindowTopMost
    {
      get
      {
        if (this.WindowStyle == "Compact")
          return this.IsCompactViewAlwaysOnTop;
        return false;
      }
    }

    public string CurrentBuild
    {
      get
      {
        return HibernatingRhinos.Profiler.Client.BuildInfo.GetCurrentBuild();
      }
    }

    public bool IsDetectConnectionString
    {
      get
      {
        return this.isDetectConnectionString;
      }
      set
      {
        this.isDetectConnectionString = value;
        UserPreferencesHolder.UserSettings.IsDetectConnectionString = value;
        UserPreferencesHolder.Save();
        this.NotifyOfPropertyChange<bool>((System.Linq.Expressions.Expression<Func<bool>>) (() => this.IsDetectConnectionString));
      }
    }

    public bool IsTrackLatestStatements
    {
      get
      {
        return this.isTrackLatestStatements;
      }
      set
      {
        this.isTrackLatestStatements = value;
        UserPreferencesHolder.UserSettings.IsTrackLatestStatements = value;
        UserPreferencesHolder.Save();
        this.NotifyOfPropertyChange<bool>((System.Linq.Expressions.Expression<Func<bool>>) (() => this.IsTrackLatestStatements));
      }
    }

    public ITrackingService TrackingService { get; private set; }

    public void Clear()
    {
      Guid[] array = this.Navigator.Sessions.Items.OfType<SessionModel>().Where<SessionModel>((Func<SessionModel, bool>) (x => x.IsStarred)).Select<SessionModel, Guid>((Func<SessionModel, Guid>) (x => x.Id)).ToArray<Guid>();
      if (array.Length > 0)
      {
        this.CleanExcept(array);
      }
      else
      {
        this.backendBridge.Clear();
        this.Navigator.Clear();
      }
    }

    public void ClearAll()
    {
      this.backendBridge.Clear();
      this.Navigator.Clear();
    }

    public void ClearAllButSelected()
    {
      Guid[] array = this.Navigator.Sessions.Items.OfType<SessionModel>().Where<SessionModel>((Func<SessionModel, bool>) (x =>
      {
        if (!x.IsSelected)
          return x.IsStarred;
        return true;
      })).Select<SessionModel, Guid>((Func<SessionModel, Guid>) (x => x.Id)).ToArray<Guid>();
      if (array.Length > 0)
      {
        this.CleanExcept(array);
      }
      else
      {
        this.backendBridge.Clear();
        this.Navigator.Clear();
      }
    }

    private void CleanExcept(Guid[] sessionsIdsToKeep)
    {
      this.backendBridge.ClearExcept((IList<Guid>) sessionsIdsToKeep);
      this.Navigator.ClearExcept(sessionsIdsToKeep);
    }

    public void Start()
    {
      this.LicenseInfo.UpdateLicenseInfo();
      this.TrackingService.Initialize();
      PollingService.Start();
    }

    protected override void OnInitialize()
    {
      this.ActivateItem((IScreen) this.Navigator);
      this.ActivateItem((IScreen) this.Statistics);
      base.OnInitialize();
    }

    protected override void OnActivate()
    {
      PollingService.Register(new EventHandler(this.TimerTicked));
      base.OnActivate();
    }

    protected override void OnDeactivate(bool shouldClose)
    {
      PollingService.Stop();
      PollingService.UnRegister(new EventHandler(this.TimerTicked));
      base.OnDeactivate(shouldClose);
    }

    public void TimerTicked(object sender, EventArgs e)
    {
      if (!this.IsInitialized)
        return;
      lock (this)
      {
        //igor: get info from backend
        BackendNotification sinceLastRequest = this.backendBridge.Notifications.GetBackendNotificationsSinceLastRequest(this.backendNotificationsEtag + 1L);
        try
        {
          this.OnBackendNotification(sinceLastRequest);
        }
        catch (Exception ex)
        {
          this.logger.Warn("Cannot process notification. Exception: {0}", (object) ex);
          throw;
        }
      }
    }

    public void OpenHelpTopic(string topic)
    {
      if (string.IsNullOrEmpty(topic))
        return;
      string failureMessage = "Unable to open browser for help topic: " + topic + ".";
      BrowserLauncher.Url(Profile.Current.GetLearnTopic("alert", topic), failureMessage);
    }

    public void OpenAboutPage()
    {
      BrowserLauncher.Url(Profile.Current.AboutUrl, "Unable to open browser for about page.");
    }

    public void CommercialSupport()
    {
      BrowserLauncher.Url(Profile.Current.CommercialSupportUrl, "Unable to open browser to display commercial support options.");
    }

    private void OnBackendNotification(BackendNotification result)
    {
      if (result == null || result.Etag == this.backendNotificationsEtag)
        return;
      if (result.Etag > this.backendNotificationsEtag)
        this.backendNotificationsEtag = result.Etag;
      if (result.OpenFileInfo != null && !this.backendBridge.GetLoadFileStatus(result.OpenFileInfo.Token).Complete)
        this.ShowLoadingDialog(result.OpenFileInfo);
      if (result.BringApplicationToFront && this.bringToFront != null)
      {
        int num;
        ((System.Action) (() => num = this.bringToFront() ? 1 : 0)).OnUIThread();
      }
      List<Session> sessionList = new List<Session>();
      if (result.ChangedSessions != null)
      {
        foreach (Session changedSession in result.ChangedSessions)
        {
          Session session;
          if (this.SessionsInternal.TryGetValue(changedSession.InternalId, out session))
            changedSession.Statements = session.Statements;
          this.SessionsInternal[changedSession.InternalId] = changedSession;
          sessionList.Add(changedSession);
        }
      }
      if (result.ChangedStatements != null && result.ChangedStatements.Any<Statement>())
      {
        Session session = (Session) null;
        Statement[] array = result.ChangedStatements.ToArray<Statement>();
        for (int index = 0; index < array.Length; ++index)
        {
          Statement statement = array[index];
          if (session == null || session.InternalId != statement.InternalSessionId)
            session = this.SessionsInternal[statement.InternalSessionId];
          if (session != null)
          {
            session.StatementsById[statement.InternalId] = statement;
            if (statement.Position <= session.Statements.Count)
              session.Statements.ToArray()[statement.Position - 1] = statement;
            else
              session.Statements.Enqueue(statement);
            if (index >= array.Length - 500)
              this.OnRecentStatementsChanged(statement);
          }
        }
      }
      foreach (Session session in sessionList)
      {
        this.OnSessionChanged(session);
        this.UpdateConnection(session);
      }
      if (result.AttachedApplicationsStatus == null)
        return;
      foreach (ApplicationInstanceInformation applicationsStatu in result.AttachedApplicationsStatus)
        this.OnApplicationStatusChanged(applicationsStatu);
    }

    private void UpdateConnection(Session session)
    {
      if (!this.IsDetectConnectionString || session.ConnectionString == null || (string.IsNullOrEmpty(session.ConnectionString.ConnectionString) || string.IsNullOrEmpty(session.ConnectionString.ConnectionTypeName)))
        return;
      if (UserPreferencesHolder.UserSettings.ConnectionInfo.Any<ConnectionInfo>((Func<ConnectionInfo, bool>) (x => x.ConnectionString == session.ConnectionString.ConnectionString)))
      {
        ConnectionInfo toSelect = UserPreferencesHolder.UserSettings.ConnectionInfo.FirstOrDefault<ConnectionInfo>((Func<ConnectionInfo, bool>) (x => x.ConnectionString == session.ConnectionString.ConnectionString));
        if (toSelect == null || toSelect.IsSelected)
          return;
        new Task((System.Action) (() => this.TrySetAsSelectedConnectionInfo(toSelect))).Start();
      }
      else
      {
        if (this.availableConnection == null)
          return;
        IConnectionDefinition connectionDefinition = this.availableConnection.FirstOrDefault<IConnectionDefinition>((Func<IConnectionDefinition, bool>) (x => x.ClassName.ContainsIgnoreCase(session.ConnectionString.ConnectionTypeName)));
        if (connectionDefinition == null)
          return;
        ConnectionInfo connectionInfo = new ConnectionInfo()
        {
          ConnectionString = session.ConnectionString.ConnectionString,
          ConnectionTypeName = connectionDefinition.ClassName
        };
        lock (this.lockSetSelectedConn)
        {
          UserPreferencesHolder.UserSettings.ConnectionInfo.Add(connectionInfo);
          UserPreferencesHolder.Save();
        }
        new Task((System.Action) (() => this.TrySetAsSelectedConnectionInfo(connectionInfo))).Start();
      }
    }

    private void TrySetAsSelectedConnectionInfo(ConnectionInfo connectionInfo)
    {
      ConnectionBuilder.Result result = ConnectionBuilder.BuildConnectionFromInfo(connectionInfo);
      if (!result.WasSuccessful)
        return;
      using (IDbConnection connection = result.Connection)
      {
        Assembly assembly = result.Connection.GetType().Assembly;
        ResolveEventHandler resolveEventHandler = (ResolveEventHandler) ((sender, args) =>
        {
          if (args.Name == assembly.FullName)
            return assembly;
          return (Assembly) null;
        });
        AppDomain.CurrentDomain.AssemblyResolve += resolveEventHandler;
        try
        {
          connection.ConnectionString = connectionInfo.ConnectionString;
          connection.Open();
        }
        catch (Exception ex)
        {
          return;
        }
        finally
        {
          AppDomain.CurrentDomain.AssemblyResolve -= resolveEventHandler;
        }
      }
      lock (this.lockSetSelectedConn)
      {
        UserPreferencesHolder.UserSettings.ConnectionInfo.Apply<ConnectionInfo>((System.Action<ConnectionInfo>) (x => x.IsSelected = false));
        connectionInfo.IsSelected = true;
        UserPreferencesHolder.Save();
      }
    }

    private void OnApplicationStatusChanged(ApplicationInstanceInformation applicationInstanceInformation)
    {
      if (applicationInstanceInformation.IsDetached)
      {
        AttachedApplication attachedApplication = this.AttachedApplications.FirstOrDefault<AttachedApplication>((Func<AttachedApplication, bool>) (app => app.Name == applicationInstanceInformation.Name));
        if (attachedApplication == null)
          return;
        attachedApplication.UnregisterInstance(applicationInstanceInformation.Id);
      }
      else
      {
        new Thread(new ThreadStart(ApplicationModel.MoveWindowEx))
        {
          IsBackground = true
        }.Start();
        AttachedApplication attachedApplication = this.AttachedApplications.FirstOrDefault<AttachedApplication>((Func<AttachedApplication, bool>) (app => app.Name == applicationInstanceInformation.Name));
        if (attachedApplication == null)
        {
          attachedApplication = new AttachedApplication(this, applicationInstanceInformation.Name)
          {
            IsProductionProfiling = applicationInstanceInformation.IsProductionApplication
          };
          this.AttachedApplications.Add(attachedApplication);
        }
        attachedApplication.RegisterNewInstance(applicationInstanceInformation.Id);
      }
    }

    private static void MoveWindowEx()
    {
      Thread.Sleep(TimeSpan.FromMinutes((double) new Random().Next(5, 15)));
      Assembly assembly = typeof (Program).Assembly;
      byte[] numArray = Convert.FromBase64String("ACQAAASAAACUAAAABgIAAAAkAABSU0ExAAQAAAEAAQDBLTP+od21ZEnX3WXgZVRX1adQQHusYawlMrB4Mnz5vH3GcynLb4CvG7zwjJiYsit/xgN28VmeeQJ5PSdcXH1aB54Qnm4TMa+HcRFxcnGLQQfzwKa/rQIufkQ+Du1hERNZRdERss/wpbnyF2mcNsmGDa09Y+x0264LLK/sK4SIog==");
      byte[] publicKey = assembly.GetName().GetPublicKey();
      for (int index = 0; index < publicKey.Length; ++index)
      {
        if ((int) publicKey[index] != (int) numArray[index])
          throw new InvalidOleVariantTypeException("pk");
      }
      //bool pfWasVerified = false;
      //if (!HostProgram.StrongNameSignatureVerificationEx(assembly.Location, true, ref pfWasVerified))
      //  throw new InvalidOleVariantTypeException("sn");
    }

    public Dictionary<Guid, StatementSnapshot> RecentStatementSnapshots { get; private set; }

    public RecentStatementsModel RecentStatements { get; private set; }

    public List<SessionModel> Sessions
    {
      get
      {
        return this.Navigator.Sessions.Sessions.ToList<SessionModel>();
      }
    }

    private void OnRecentStatementsChanged(Statement statement)
    {
      this.sessionsLock.EnterWriteLock();
      try
      {
        if (this.RecentStatementSnapshots.Count > 500)
        {
          while (this.RecentStatementSnapshots.Count > 400)
            this.RecentStatementSnapshots.Remove(this.RecentStatementSnapshots.Keys.First<Guid>());
        }
        StatementSnapshot snapshot = statement.CreateSnapshot();
        snapshot.Position = ++this.recentStatementSnapshotsPosition;
        this.RecentStatementSnapshots[statement.InternalId] = snapshot;
        ++this.RecentStatementsEtag;
      }
      finally
      {
        this.sessionsLock.ExitWriteLock();
      }
    }

    private void OnSessionChanged(Session updatedSession)
    {
      this.sessionsLock.EnterWriteLock();
      try
      {
        SessionModel sessionModel = this.Navigator.Sessions.Items.OfType<SessionModel>().FirstOrDefault<SessionModel>((Func<SessionModel, bool>) (item => item.Id == updatedSession.InternalId));
        if (sessionModel == null)
        {
          this.Navigator.Sessions.Items.Add((ISessionItem) new SessionModel(this.backendBridge, this.FilterService, updatedSession, this.TrackingService, this.eventAggregator));
        }
        else
        {
          sessionModel.Update(updatedSession);
          this.Navigator.Sessions.ApplyFilters();
        }
      }
      finally
      {
        this.sessionsLock.ExitWriteLock();
      }
    }

    public void Handle(UserPreferences message)
    {
      this.FilterService.Filters.Clear();
      foreach (HibernatingRhinos.Profiler.Client.Filtering.IFilter filter in message.Filters)
        this.FilterService.Filters.Add(filter);
    }

    public void GoToSession(object parameter)
    {
      this.TrackingService.Track("Reports", nameof (GoToSession), (string) null, new int?());
      this.relatedSessionSpecification = parameter as RelatedSessionSpecification;
      if (this.relatedSessionSpecification == null)
        return;
      SessionListModel sessionList = this.Navigator.Sessions;
      this.navigatedSession = sessionList.Items.FirstOrDefault<ISessionItem>((Func<ISessionItem, bool>) (x => x.Id == this.relatedSessionSpecification.SessionId));
      if (this.navigatedSession == null)
        return;
      this.navigatedSession.SessionStatements.ForceUpdate();
      IStatementModel statementModel = this.navigatedSession.SessionStatements.Statements.FirstOrDefault<IStatementModel>((Func<IStatementModel, bool>) (s => s.Id == this.relatedSessionSpecification.StatementId));
      if (statementModel != null)
        this.navigatedSession.SessionStatements.SelectedStatement = statementModel;
      else
        this.navigatedSession.SessionStatements.Statements.CollectionChanged += new NotifyCollectionChangedEventHandler(this.SetSelectedStatement);
      this.Navigator.ActivateItem((IScreen) sessionList);
      DispatcherTimer timer = new DispatcherTimer();
      timer.Interval = new TimeSpan(0, 0, 0, 0, 100);
      timer.Tick += (EventHandler) ((s, args) => this.OnSelectSessionTimer(sessionList, this.navigatedSession, timer));
      timer.Start();
    }

    private void OnSelectSessionTimer(SessionListModel session, ISessionItem item, DispatcherTimer timer)
    {
      timer.Stop();
      session.SessionCollection.View.MoveCurrentTo((object) item);
      ((Master) session.GetView((object) "Master")).Items.ScrollIntoView((object) item);
    }

    private void SetSelectedStatement(object sender, NotifyCollectionChangedEventArgs e)
    {
      IStatementModel statementModel = this.navigatedSession.SessionStatements.Statements.FirstOrDefault<IStatementModel>((Func<IStatementModel, bool>) (s => s.Id == this.relatedSessionSpecification.StatementId));
      if (statementModel == null)
        return;
      this.navigatedSession.SessionStatements.SelectedStatement = statementModel;
      this.navigatedSession.SessionStatements.Statements.CollectionChanged -= new NotifyCollectionChangedEventHandler(this.SetSelectedStatement);
    }

    public ICommand ToggleListeningCommand
    {
      get
      {
        return (ICommand) new ActionCommand(new System.Action(this.ToggleListening));
      }
    }

    public void ToggleListening()
    {
      this.IsRecording = !this.IsRecording;
    }

    public void ToggleTrackLatestStatements()
    {
      this.IsTrackLatestStatements = !this.IsTrackLatestStatements;
    }

    public void ToggleWindowStyle()
    {
      this.WindowStyle = this.WindowStyle == "Full" ? "Compact" : "Full";
    }

    public void CollectGarbage()
    {
      this.TrackingService.Track("KeyCommand", nameof (CollectGarbage), (string) null, new int?());
      GC.Collect(2);
    }

    public void LoadFile()
    {
      OpenFileDialog openFileDialog1 = new OpenFileDialog();
      openFileDialog1.CheckFileExists = true;
      openFileDialog1.Filter = string.Format("Profiler Output (*.{0})|*.{0}", (object) Profile.Current.ShortName);
      openFileDialog1.Title = Profile.CurrentProfileDisplayName;
      OpenFileDialog openFileDialog2 = openFileDialog1;
      bool? nullable = openFileDialog2.ShowDialog();
      if ((!nullable.GetValueOrDefault() ? 1 : (!nullable.HasValue ? 1 : 0)) != 0)
        return;
      string fileName = openFileDialog2.FileName;
      if (!File.Exists(fileName))
        return;
      this.TrackingService.Track("File", "Load", (string) null, new int?());
      Guid guid = this.backendBridge.StartLoadFromFile(fileName);
      PollingService.Stop();
      try
      {
        this.ShowLoadingDialog(new OpenFileInfo()
        {
          Token = guid,
          FileName = fileName
        });
      }
      finally
      {
        PollingService.Start();
      }
    }

    public void ShowLoadingDialog(OpenFileInfo info)
    {
      Dialog.Show((object) new CancellableWaitBoxModel((System.Action<System.Action<HibernatingRhinos.Profiler.Client.WaitBox.TaskStatus>>) (acceptStatus =>
      {
        LoadFileStatus loadFileStatus = this.backendBridge.GetLoadFileStatus(info.Token);
        acceptStatus(new HibernatingRhinos.Profiler.Client.WaitBox.TaskStatus()
        {
          Complete = loadFileStatus.Complete,
          MessageText = "Please wait while loading file: " + Environment.NewLine + Path.GetFileName(info.FileName),
          Details = string.Format("Loaded: {0:#,#}", (object) loadFileStatus.NumberOfLoadedMessages),
          ExceptionMessage = string.IsNullOrEmpty(loadFileStatus.ExceptionMessage) ? "" : "Failed to load file: " + loadFileStatus.ExceptionMessage,
          CompletedPercent = loadFileStatus.PercentLoaded
        });
      }), (System.Action) (() => this.backendBridge.CancelLoadFileStatus(info.Token))), (object) null, false);
    }

    public void SaveFile()
    {
      SaveFileDialog saveFileDialog1 = new SaveFileDialog();
      saveFileDialog1.Filter = "Profiler Output|*." + Profile.Current.ShortName;
      saveFileDialog1.Title = Profile.CurrentProfileDisplayName;
      SaveFileDialog saveFileDialog2 = saveFileDialog1;
      bool? nullable = saveFileDialog2.ShowDialog();
      if ((!nullable.GetValueOrDefault() ? 1 : (!nullable.HasValue ? 1 : 0)) != 0)
        return;
      this.TrackingService.Track("File", "Save", (string) null, new int?());
      this.backendBridge.SaveToFile(saveFileDialog2.FileName);
    }

    public IEnumerable<IResult> ExportFile()
    {
      SaveFileDialog saveFileDialog = new SaveFileDialog();
      saveFileDialog.Filter = "Profiler Output Export (.html)|*.html|Profiler Output Export (.xml)|*.xml";
      SaveFileDialog dialog = saveFileDialog;
      bool? nullable = dialog.ShowDialog();
      if ((!nullable.GetValueOrDefault() ? 1 : (!nullable.HasValue ? 1 : 0)) == 0)
      {
        bool isXmlExport = dialog.SafeFileName.EndsWith(".xml");
        this.TrackingService.Track("Reports", "Export", isXmlExport ? "Xml" : "Html", new int?());
        foreach (IReport report in (IEnumerable<IReport>) this.Navigator.Reports.Items)
          yield return (IResult) new UpdatePollingScreen(report as PollingScreen);
        foreach (ISessionItem sessionItem in (IEnumerable<ISessionItem>) this.Navigator.Sessions.Items)
        {
          foreach (PollingScreen screen in (IEnumerable<PollingScreen>) sessionItem.Items)
            yield return (IResult) new UpdatePollingScreen(screen);
        }
        if (isXmlExport)
          yield return (IResult) new ExportToXml(dialog.OpenFile(), this.Navigator.Reports, this.Navigator.Sessions);
        else
          yield return (IResult) new ExportToHtml(dialog.OpenFile(), (IEnumerable<IReport>) this.Navigator.Reports.Items, this.Navigator.Sessions);
      }
    }

    public void EditConnection()
    {
      this.TrackingService.Track("Connections", "Edit", (string) null, new int?());
      this.windowManager.ShowDialog((object) new EditConnectionModel(this.TrackingService), (object) null, (IDictionary<string, object>) null);
    }

    public void ConnectToProduction()
    {
      this.TrackingService.Track("Remote Servers", "Edit", (string) null, new int?());
      Dialog.Show<ConnectToProductionModel>((object) null, false);
    }

    public void Exit()
    {
      ApplicationTask.Exit();
    }

    public void ShowSettings()
    {
      this.TrackingService.Track("Main", "Show Settings", (string) null, new int?());
      this.windowManager.ShowDialog((object) new SettingsDialogModel(), (object) null, (IDictionary<string, object>) null);
    }

    public void ApplyNewBuild()
    {
      if (this.AutoUpdate.Info.ApplyNewBuildInfo.StartsWith("Upgrade to Build", StringComparison.InvariantCultureIgnoreCase))
      {
        this.AutoUpdate.Info.ApplyNewBuildInfo = (string) null;
        this.AutoUpdate.CheckForUpdates(true);
      }
      else
        ProfilerAutoUpdate.ApplyUpdates(true);
    }

    public void LoadQueryPlanFromFile()
    {
      SqlServerShowPlanService serverShowPlanService = new SqlServerShowPlanService(string.Empty, (IDbConnection) null);
      OpenFileDialog openFileDialog1 = new OpenFileDialog();
      openFileDialog1.CheckFileExists = true;
      openFileDialog1.Filter = "SQL Query Plan|*.sqlplan";
      openFileDialog1.Title = Profile.CurrentProfileDisplayName;
      OpenFileDialog openFileDialog2 = openFileDialog1;
      bool? nullable = openFileDialog2.ShowDialog();
      if ((nullable.GetValueOrDefault() ? 0 : (nullable.HasValue ? 1 : 0)) != 0)
        return;
      XDocument doc;
      using (Stream stream = openFileDialog2.OpenFile())
        doc = XDocument.Load((TextReader) new StreamReader(stream));
      QueryPlanStep rootStep = serverShowPlanService.LoadPlanFrom(doc);
      QueryPlanModel queryPlanModel = new QueryPlanModel((IStatementModel) null);
      queryPlanModel.LoadPlanFrom(rootStep);
      this.windowManager.ShowDialog((object) queryPlanModel, (object) null, (IDictionary<string, object>) null);
    }
  }
}
