﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Actions.SetFocusAction
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System.Windows;
using System.Windows.Interactivity;

namespace HibernatingRhinos.Profiler.Client.Actions
{
  internal class SetFocusAction : TargetedTriggerAction<FrameworkElement>
  {
    private bool _hasTriggered;

    public bool FirstTimeOnly { get; set; }

    protected override void Invoke(object parameter)
    {
      if (this.Target == null || this._hasTriggered)
        return;
      this.Target.Focus();
      if (!this.FirstTimeOnly)
        return;
      this._hasTriggered = true;
    }
  }
}
