﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Actions.ChangeAttachedPropertyAction
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System.Windows;
using System.Windows.Data;
using System.Windows.Interactivity;

namespace HibernatingRhinos.Profiler.Client.Actions
{
  public class ChangeAttachedPropertyAction : TargetedTriggerAction<DependencyObject>
  {
    public static readonly DependencyProperty ValueProperty = DependencyProperty.Register(nameof (Value), typeof (object), typeof (ChangeAttachedPropertyAction), new PropertyMetadata((PropertyChangedCallback) null));
    public static readonly DependencyProperty PropertyProperty = DependencyProperty.Register(nameof (Property), typeof (DependencyProperty), typeof (ChangeAttachedPropertyAction), new PropertyMetadata((PropertyChangedCallback) null));

    public DependencyProperty Property
    {
      get
      {
        return (DependencyProperty) this.GetValue(ChangeAttachedPropertyAction.PropertyProperty);
      }
      set
      {
        this.SetValue(ChangeAttachedPropertyAction.PropertyProperty, (object) value);
      }
    }

    public object Value
    {
      get
      {
        return this.GetValue(ChangeAttachedPropertyAction.ValueProperty);
      }
      set
      {
        this.SetValue(ChangeAttachedPropertyAction.ValueProperty, value);
      }
    }

    public BindingBase Binding { get; set; }

    protected override void Invoke(object parameter)
    {
      if (this.Property == null)
        return;
      if (this.Binding != null)
        BindingOperations.SetBinding(this.Target, this.Property, this.Binding);
      else
        BindingOperations.ClearBinding(this.Target, this.Property);
    }
  }
}
