﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Statements.StatementSummaryModel
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.Client.Tracking;
using System.ComponentModel;
using System.Linq;

namespace HibernatingRhinos.Profiler.Client.Statements
{
  public class StatementSummaryModel : Conductor<IStatementSummaryScreen>.Collection.OneActive
  {
    private StatementAlertsModel alerts = new StatementAlertsModel();
    private StatementTablesModel tables = new StatementTablesModel();
    private readonly SessionStatementsModel statements;
    private readonly BackendBridge backendBridge;
    private StatementDetailsModel details;
    private StatementStackTraceModel stackTrace;
    private readonly ITrackingService trackingService;

    public StatementSummaryModel(SessionStatementsModel statements, BackendBridge backendBridge, ITrackingService trackingService)
    {
      this.details = new StatementDetailsModel(trackingService);
      this.statements = statements;
      this.backendBridge = backendBridge;
      this.trackingService = trackingService;
      this.stackTrace = new StatementStackTraceModel();
      this.Items.Add((IStatementSummaryScreen) this.details);
      this.Items.Add((IStatementSummaryScreen) this.alerts);
      this.Items.Add((IStatementSummaryScreen) this.stackTrace);
      this.Items.Add((IStatementSummaryScreen) this.tables);
      this.ActivateItem((IStatementSummaryScreen) this.details);
      statements.PropertyChanged += new PropertyChangedEventHandler(this.EnsurePropperSummaries);
    }

    public void SelectAlerts()
    {
      StatementAlertsModel statementAlertsModel = this.Items.OfType<StatementAlertsModel>().FirstOrDefault<StatementAlertsModel>();
      if (statementAlertsModel == null)
        return;
      this.ActivateItem((IStatementSummaryScreen) statementAlertsModel);
    }

    protected override void OnViewLoaded(object view)
    {
    }

    private void EnsurePropperSummaries(object s, PropertyChangedEventArgs e)
    {
      if (e.PropertyName != "SelectedStatement")
        return;
      if (this.statements.SelectedStatement == null)
      {
        this.Items.Clear();
        this.details = new StatementDetailsModel(this.trackingService);
        this.alerts = new StatementAlertsModel();
        this.stackTrace = new StatementStackTraceModel();
        this.tables = new StatementTablesModel();
        this.Items.Add((IStatementSummaryScreen) this.details);
        this.Items.Add((IStatementSummaryScreen) this.alerts);
        this.Items.Add((IStatementSummaryScreen) this.stackTrace);
        this.Items.Add((IStatementSummaryScreen) this.tables);
        this.ActivateItem((IStatementSummaryScreen) this.details);
      }
      else
      {
        if (this.statements.SelectedStatement.IsAggregate)
        {
          this.CloseItem<IStatementSummaryScreen>((IStatementSummaryScreen) this.alerts);
          this.CloseItem<IStatementSummaryScreen>((IStatementSummaryScreen) this.stackTrace);
          this.CloseItem<IStatementSummaryScreen>((IStatementSummaryScreen) this.tables);
        }
        else if (this.statements.SelectedStatement.Alerts != null)
        {
          if (!this.Items.Contains((IStatementSummaryScreen) this.alerts))
            this.Items.Insert(1, (IStatementSummaryScreen) this.alerts);
          if (!this.Items.Contains((IStatementSummaryScreen) this.stackTrace))
            this.Items.Add((IStatementSummaryScreen) this.stackTrace);
          if (!this.Items.Contains((IStatementSummaryScreen) this.tables))
            this.Items.Add((IStatementSummaryScreen) this.tables);
        }
        this.Items.Apply<IStatementSummaryScreen>((System.Action<IStatementSummaryScreen>) (x => x.UpdateStatement(this.statements.SelectedStatement)));
      }
    }

    public void Clear()
    {
      this.details.Clear();
      this.alerts.Clear();
      this.stackTrace.Clear();
    }
  }
}
