﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Statements.IStatementModel
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.Appender.StackTraces;
using HibernatingRhinos.Profiler.BackEnd.Bridge;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace HibernatingRhinos.Profiler.Client.Statements
{
  public interface IStatementModel : IFilterableStatementSnapshot, INotifyPropertyChanged
  {
    Guid SessionId { get; }

    string Url { get; }

    string StarColor { get; set; }

    bool HasWarnings { get; }

    bool HasSuggestions { get; }

    bool HasMajor { get; }

    string Prefix { get; }

    string Text { get; }

    string ShortText { get; }

    bool IsSelected { get; set; }

    StackTraceInfo StackTrace { get; }

    QueryDurationModel DurationModel { get; }

    bool IsSelectStatement { get; }

    int[] CountOfRows { get; }

    string RowCountDescription { get; }

    string ShortUrl { get; set; }

    IEnumerable<DisplayParameter> Parameters { get; }

    ObservableCollection<StatementAlert> Alerts { get; }

    Guid Id { get; }

    bool IsAggregate { get; set; }

    DateTime Timestamp { get; }

    bool UpdateWith(StatementSnapshot statementSnapshot);

    bool MatchWith(IStatementModel statement);

    void UnrootStatement();

    void RootStatement(IStatementModel statement);

    void CopyToClipboard();

    void ShortenUrl(int maxLength);

    bool IsStarred { get; set; }

    void SetStarColor(string color);

    ObservableCollection<StatementTable> Tables { get; }
  }
}
