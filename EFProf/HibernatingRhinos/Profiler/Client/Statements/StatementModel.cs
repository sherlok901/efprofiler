﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Statements.StatementModel
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using HibernatingRhinos.Profiler.Appender.StackTraces;
using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.BackEnd.Extensions;
using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using HibernatingRhinos.Profiler.BackEnd.Profiles;
using HibernatingRhinos.Profiler.Client.Controls;
using HibernatingRhinos.Profiler.Client.Converters;
using HibernatingRhinos.Profiler.Client.Infrastructure;
using HibernatingRhinos.Profiler.Client.Sessions;
using HibernatingRhinos.Profiler.Client.Sessions.SessionListViews;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Windows.Controls;

namespace HibernatingRhinos.Profiler.Client.Statements
{
  [DebuggerDisplay("{Text}")]
  [Serializable]
  public class StatementModel : SelectionBase, IStatementModel, IFilterableStatementSnapshot, INotifyPropertyChanged
  {
    private readonly string nl = Environment.NewLine;
    private IEnumerable<IStatementModel> children;
    private IStatementModel rootStatement;
    private StatementSnapshot snapshot;
    private string shortUrl;
    private bool multiSelection;
    private bool isStarred;
    private string starColor;
    private string lastStarColor;

    public StatementModel(StatementSnapshot snapshot)
    {
      this.Alerts = new ObservableCollection<StatementAlert>();
      this.Tables = new ObservableCollection<StatementTable>();
      this.UpdateWith(snapshot);
    }

    public bool IsStarred
    {
      get
      {
        return this.isStarred;
      }
      set
      {
        this.StarColor = value ? this.lastStarColor : StarToggleButton.DefaultStarColor;
        this.isStarred = value;
        this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.IsStarred));
      }
    }

    public string StarColor
    {
      get
      {
        return this.starColor;
      }
      set
      {
        this.starColor = value;
        this.NotifyOfPropertyChange<string>((Expression<Func<string>>) (() => this.StarColor));
      }
    }

    public List<string> StarColors
    {
      get
      {
        return StarToggleButton.StarColors;
      }
    }

    public void SetStarColor(string color)
    {
      this.StarColor = this.lastStarColor = color;
      this.IsStarred = true;
    }

    public IEnumerable<IStatementModel> Children
    {
      set
      {
        if (this.children != null)
          this.children.ForEach<IStatementModel>((System.Action<IStatementModel>) (x => x.UnrootStatement()));
        this.children = value;
        if (this.children == null)
          return;
        this.children.ForEach<IStatementModel>((System.Action<IStatementModel>) (x => x.RootStatement((IStatementModel) this)));
      }
    }

    public Guid SessionId
    {
      get
      {
        return this.snapshot.SessionId;
      }
    }

    public bool CanJumpToSession { get; set; }

    public bool IsAggregate { get; set; }

    public Guid Id
    {
      get
      {
        return this.snapshot.StatementId;
      }
    }

    public int[] CountOfRows
    {
      get
      {
        return this.snapshot.CountOfRows;
      }
    }

    public string RowCountDescription
    {
      get
      {
        return CountOfRowsConverter.Write(this.CountOfRows);
      }
    }

    public ObservableCollection<StatementTable> Tables { get; private set; }

    public string CountOfRowsText
    {
      get
      {
        if (Profile.Current.Supports(SupportedFeatures.RowCount))
          return string.Format("See the {0} row(s) resulting from this statement.", new CountOfRowsConverter().Convert((object) this.CountOfRows, typeof (string), (object) null, CultureInfo.CurrentCulture));
        return "See the row(s) resulting from this statement.";
      }
    }

    public string CompactCountOfRowsText
    {
      get
      {
        if (Profile.Current.Supports(SupportedFeatures.RowCount))
          return string.Format("View {0} row(s)", new CountOfRowsConverter().Convert((object) this.CountOfRows, typeof (string), (object) null, CultureInfo.CurrentCulture));
        return "View row(s)";
      }
    }

    public IEnumerable<DisplayParameter> Parameters
    {
      get
      {
        return this.snapshot.Parameters.Where<HibernatingRhinos.Profiler.BackEnd.Messages.Parameter>((Func<HibernatingRhinos.Profiler.BackEnd.Messages.Parameter, bool>) (x => x != null)).Select<HibernatingRhinos.Profiler.BackEnd.Messages.Parameter, DisplayParameter>((Func<HibernatingRhinos.Profiler.BackEnd.Messages.Parameter, DisplayParameter>) (x => new DisplayParameter()
        {
          Name = x.Name,
          Value = x.Value,
          ParameterValue = x.ParameterValue,
          Type = x.Type,
          Column = x.AssociateColumn
        }));
      }
    }

    public ObservableCollection<StatementAlert> Alerts { get; private set; }

    public DateTime Timestamp
    {
      get
      {
        return this.snapshot.Timestamp;
      }
    }

    public bool HasWarnings
    {
      get
      {
        return this.Alerts.Any<StatementAlert>((Func<StatementAlert, bool>) (x => x.Severity == Severity.Warning));
      }
    }

    public bool HasMajor
    {
      get
      {
        return this.Alerts.Any<StatementAlert>((Func<StatementAlert, bool>) (x => x.Severity == Severity.Major));
      }
    }

    public bool HasSuggestions
    {
      get
      {
        return this.Alerts.Any<StatementAlert>((Func<StatementAlert, bool>) (x => x.Severity == Severity.Suggestion));
      }
    }

    public int AlertsSeverity
    {
      get
      {
        return this.Alerts.Sum<StatementAlert>((Func<StatementAlert, int>) (statementAlert => (int) statementAlert.Severity));
      }
    }

    public StackTraceInfo StackTrace
    {
      get
      {
        return this.snapshot.StackTrace;
      }
    }

    public QueryDurationModel DurationModel
    {
      get
      {
        return new QueryDurationModel(this.snapshot.Duration);
      }
    }

    public string Text
    {
      get
      {
        string str = this.FormattedSql.Replace(this.nl + this.nl + this.nl + this.nl, this.nl + this.nl + "--//////////////////////////////////////////////////" + this.nl + this.nl);
        if (string.IsNullOrEmpty(this.Prefix))
          return str;
        return this.Prefix + Environment.NewLine + str;
      }
    }

    public string FormattedSql
    {
      get
      {
        return this.snapshot.FormattedSql;
      }
    }

    public string ShortText
    {
      get
      {
        return this.Prefix + this.snapshot.ShortSql;
      }
    }

    public string Url
    {
      get
      {
        return this.snapshot.Url;
      }
    }

    public bool CanExecuteQuery
    {
      get
      {
        return this.snapshot.CanExecuteQuery;
      }
    }

    public bool IsSelectStatement
    {
      get
      {
        return this.snapshot.IsSelectStatement;
      }
    }

    public string RawSql
    {
      get
      {
        return this.snapshot.RawSql;
      }
    }

    public QueryDuration Duration
    {
      get
      {
        return this.snapshot.Duration;
      }
    }

    public bool IsCached
    {
      get
      {
        return this.snapshot.IsCached;
      }
    }

    public bool IsDDL
    {
      get
      {
        return this.snapshot.IsDDL;
      }
    }

    public StatementSnapshot Snapshot
    {
      get
      {
        return this.snapshot;
      }
    }

    public bool IsTransaction
    {
      get
      {
        return this.snapshot.IsTransaction;
      }
    }

    public int SessionStatementCount
    {
      get
      {
        return this.snapshot.SessionStatementCount;
      }
    }

    public string OrmError
    {
      get
      {
        return this.snapshot.OrmError;
      }
    }

    public SolutionItem ErrorSolution
    {
      get
      {
        return this.snapshot.ErrorSolution;
      }
    }

    public bool UpdateWith(StatementSnapshot statementSnapshot)
    {
      if (statementSnapshot.Equals(this.snapshot))
        return false;
      this.snapshot = statementSnapshot;
      this.Alerts.Clear();
      foreach (AlertInformation alert in this.snapshot.Alerts)
        this.Alerts.Add(new StatementAlert(alert, this));
      this.Tables.Clear();
      if (this.snapshot.Tables != null)
      {
        foreach (string table in this.snapshot.Tables)
          this.Tables.Add(new StatementTable(table, this));
      }
      if (string.IsNullOrEmpty(this.snapshot.StarColor))
      {
        this.lastStarColor = StarToggleButton.StarColors[0];
        this.StarColor = StarToggleButton.DefaultStarColor;
      }
      else
        this.SetStarColor(this.snapshot.StarColor);
      this.NotifyOfPropertyChange("");
      return true;
    }

    public bool MatchWith(IStatementModel statement)
    {
      if (this.RawSql != statement.RawSql || this.IsCached != statement.IsCached)
        return false;
      DisplayParameter[] array1 = this.Parameters.ToArray<DisplayParameter>();
      DisplayParameter[] array2 = statement.Parameters.ToArray<DisplayParameter>();
      if (array1.Length != array2.Length)
        return false;
      for (int index = 0; index < array1.Length; ++index)
      {
        if (array1[index].Name != array2[index].Name || array1[index].Value != array2[index].Value)
          return false;
      }
      return true;
    }

    public void UnrootStatement()
    {
      this.rootStatement = (IStatementModel) null;
      if (this.children == null)
        return;
      this.children.ForEach<IStatementModel>((System.Action<IStatementModel>) (x => x.UnrootStatement()));
    }

    public void RootStatement(IStatementModel statement)
    {
      this.rootStatement = statement;
    }

    public void CopyToClipboard()
    {
      if (this.rootStatement != null)
        this.rootStatement.CopyToClipboard();
      else
        SafeClipboard.SetText(this.Text);
    }

    public void CopyParametersToClipboard()
    {
      if (this.Parameters == null || !this.Parameters.Any<DisplayParameter>())
        return;
      string str = string.Empty;
      foreach (DisplayParameter parameter in this.Parameters)
        str = str + parameter.Name + ": " + parameter.Value + Environment.NewLine;
      SafeClipboard.SetText(str.Remove(str.LastIndexOf(Environment.NewLine)));
    }

    public string ShortUrl
    {
      get
      {
        if (!string.IsNullOrEmpty(this.shortUrl))
          return this.shortUrl;
        return this.Url;
      }
      set
      {
        this.shortUrl = value;
        this.NotifyOfPropertyChange<string>((Expression<Func<string>>) (() => this.ShortUrl));
      }
    }

    public string Prefix
    {
      get
      {
        return this.snapshot.Prefix;
      }
    }

    public bool MultiSelection
    {
      get
      {
        return this.multiSelection;
      }
      set
      {
        this.multiSelection = value;
        this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.MultiSelection));
      }
    }

    public void ShortenUrl(int maxLength)
    {
      if (this.Url.Length > maxLength)
      {
        string[] strArray = this.Url.Split('/');
        StringBuilder stringBuilder1 = new StringBuilder("...");
        StringBuilder stringBuilder2 = new StringBuilder();
        StringBuilder stringBuilder3 = new StringBuilder();
        int count = strArray.Length / 2;
        List<string> list1 = ((IEnumerable<string>) strArray).Take<string>(count).ToList<string>();
        List<string> list2 = ((IEnumerable<string>) strArray).Skip<string>(count).Reverse<string>().ToList<string>();
        for (int index = 0; index < Math.Max(list1.Count, list2.Count); ++index)
        {
          if (index < list2.Count && stringBuilder1.Length + list2[index].Length <= maxLength)
          {
            stringBuilder1.Append("/");
            stringBuilder1.Append(list2[index]);
            stringBuilder3.Insert(0, list2[index]);
            stringBuilder3.Insert(0, "/");
          }
          if (index < list1.Count && stringBuilder1.Length + list1[index].Length <= maxLength)
          {
            stringBuilder1.Append("/");
            stringBuilder1.Append(list1[index]);
            stringBuilder2.Append(list1[index]);
            stringBuilder2.Append("/");
          }
        }
        this.ShortUrl = stringBuilder2.Append("...").Append((object) stringBuilder3).ToString();
      }
      else
        this.ShortUrl = this.Url;
    }

    public void JumpToSession()
    {
      SessionListModel sessionListModel = IoC.Get<SessionListModel>((string) null);
      ISessionItem sessionItem = sessionListModel.Items.FirstOrDefault<ISessionItem>((Func<ISessionItem, bool>) (x => x.Id == this.SessionId));
      if (sessionItem == null)
        return;
      sessionListModel.ActivateItem(sessionItem);
      DataGrid items = ((Master) sessionListModel.GetView((object) "Master")).Items;
      items.ScrollIntoView((object) sessionItem, items.Columns[0]);
    }
  }
}
