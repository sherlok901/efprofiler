﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Statements.RecentStatementsModel
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.Client.CaliburnInfrastructure;
using HibernatingRhinos.Profiler.Client.Filtering;
using HibernatingRhinos.Profiler.Client.Infrastructure;
using HibernatingRhinos.Profiler.Client.Sessions;
using HibernatingRhinos.Profiler.Client.Shell;
using HibernatingRhinos.Profiler.Client.Tracking;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;

namespace HibernatingRhinos.Profiler.Client.Statements
{
  [View(typeof (SessionView))]
  public class RecentStatementsModel : SessionItemBase
  {
    private readonly ITrackingService trackingService;
    private readonly SessionStatementsModel sessionStatements;
    private bool isSelected;
    private ApplicationModel applicationModel;

    public DateTime? Duration { get; set; }

    public Uri Url { get; set; }

    public RecentStatementsModel(FilterServiceModel filter, ITrackingService trackingService, BackendBridge backendBridge, IEventAggregator eventAggregator)
      : base(eventAggregator)
    {
      this.DisplayName = "Recent Statements";
      this.trackingService = trackingService;
      this.sessionStatements = new SessionStatementsModel(filter, trackingService, new int?(100), true, (SessionItemBase) this, backendBridge);
      this.sessionStatements.StatementsChanged += new System.Action<ObservableCollection<IStatementModel>>(this.RaiseChanges);
      this.Items.Add((PollingScreen) this.sessionStatements);
    }

    public override SessionStatementsModel SessionStatements
    {
      get
      {
        return this.sessionStatements;
      }
    }

    public override int StatementsEtag
    {
      get
      {
        return this.ApplicationModel.RecentStatementsEtag;
      }
    }

    public override bool IsSelected
    {
      get
      {
        return this.isSelected;
      }
      set
      {
        this.isSelected = value;
        this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.IsSelected));
      }
    }

    public override bool IsStarred
    {
      get
      {
        return false;
      }
      set
      {
        throw new NotSupportedException();
      }
    }

    public override bool CanStar
    {
      get
      {
        return false;
      }
    }

    public bool HasMajor
    {
      get
      {
        return this.SessionStatements.FilteredStatements.Any<IStatementModel>((Func<IStatementModel, bool>) (x => x.HasMajor));
      }
    }

    public bool HasSuggestions
    {
      get
      {
        return this.SessionStatements.FilteredStatements.Any<IStatementModel>((Func<IStatementModel, bool>) (x => x.HasSuggestions));
      }
    }

    public bool HasWarnings
    {
      get
      {
        return this.SessionStatements.FilteredStatements.Any<IStatementModel>((Func<IStatementModel, bool>) (x => x.HasWarnings));
      }
    }

    public int StatementCount
    {
      get
      {
        return this.SessionStatements.FilteredStatements.Count<IStatementModel>((Func<IStatementModel, bool>) (x => !x.IsTransaction));
      }
    }

    public IDictionary<StatementAlert, int> AggregatedAlerts
    {
      get
      {
        Dictionary<StatementAlert, int> dictionary = new Dictionary<StatementAlert, int>();
        foreach (StatementAlert key in this.SessionStatements.Statements.SelectMany<IStatementModel, StatementAlert>((Func<IStatementModel, IEnumerable<StatementAlert>>) (x => (IEnumerable<StatementAlert>) x.Alerts)))
        {
          int num;
          if (!dictionary.TryGetValue(key, out num))
            num = 0;
          dictionary[key] = num + 1;
        }
        return (IDictionary<StatementAlert, int>) dictionary;
      }
    }

    public void Clear()
    {
      this.SessionStatements.Clear();
      this.ApplicationModel.RecentStatementSnapshots.Clear();
    }

    public void ClearExcept(Guid[] sessionIds)
    {
      this.SessionStatements.ClearExcept((ICollection<Guid>) sessionIds);
      foreach (Guid key in this.ApplicationModel.SessionsInternal.Keys.Except<Guid>((IEnumerable<Guid>) sessionIds).ToList<Guid>())
        this.ApplicationModel.SessionsInternal.Remove(key);
      foreach (KeyValuePair<Guid, StatementSnapshot> keyValuePair in this.ApplicationModel.RecentStatementSnapshots.Where<KeyValuePair<Guid, StatementSnapshot>>((Func<KeyValuePair<Guid, StatementSnapshot>, bool>) (snapshot => !((IEnumerable<Guid>) sessionIds).Contains<Guid>(snapshot.Value.SessionId))).ToList<KeyValuePair<Guid, StatementSnapshot>>())
        this.ApplicationModel.RecentStatementSnapshots.Remove(keyValuePair.Key);
    }

    protected override void OnInitialize()
    {
      base.OnInitialize();
      this.ActivateItem(this.Items.First<PollingScreen>());
    }

    protected override void OnActivate()
    {
      base.OnActivate();
      this.trackingService.Track("Main", "Recent Statements", (string) null, new int?());
    }

    private void RaiseChanges(ObservableCollection<IStatementModel> statements)
    {
      this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.HasMajor));
      this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.HasSuggestions));
      this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.HasWarnings));
      this.NotifyOfPropertyChange<int>((Expression<Func<int>>) (() => this.StatementCount));
    }

    public bool CanSetStarColor
    {
      get
      {
        return false;
      }
    }

    public List<StatementModel> Statements
    {
      get
      {
        return this.GetStatements().Select<StatementSnapshot, StatementModel>((Func<StatementSnapshot, StatementModel>) (x => new StatementModel(x))).ToList<StatementModel>();
      }
    }

    public override IEnumerable<StatementSnapshot> GetStatements()
    {
      return (IEnumerable<StatementSnapshot>) this.ApplicationModel.RecentStatementSnapshots.Values.OrderBy<StatementSnapshot, int>((Func<StatementSnapshot, int>) (snapshot => snapshot.Position));
    }

    private ApplicationModel ApplicationModel
    {
      get
      {
        return this.applicationModel ?? (this.applicationModel = IoC.Get<ApplicationModel>((string) null));
      }
    }
  }
}
