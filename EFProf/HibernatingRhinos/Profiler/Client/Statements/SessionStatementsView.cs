﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Statements.SessionStatementsView
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.BackEnd.Profiles;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace HibernatingRhinos.Profiler.Client.Statements
{
  public partial class SessionStatementsView : UserControl, IComponentConnector
  {
    //internal Grid Footer;
    //internal DataGrid StatementsGrid;
    //private bool _contentLoaded;

    public SessionStatementsView()
    {
      this.InitializeComponent();
      if (!Profile.Current.Supports(SupportedFeatures.RowCount))
      {
        DataGridColumn dataGridColumn = this.StatementsGrid.Columns.FirstOrDefault<DataGridColumn>((Func<DataGridColumn, bool>) (x => x.Header.Equals((object) "Row Count")));
        if (dataGridColumn != null)
          this.StatementsGrid.Columns.Remove(dataGridColumn);
      }
      if (Profile.Current.Supports(SupportedFeatures.Duration))
        return;
      DataGridColumn dataGridColumn1 = this.StatementsGrid.Columns.FirstOrDefault<DataGridColumn>((Func<DataGridColumn, bool>) (x => x.Header.Equals((object) "Duration")));
      if (dataGridColumn1 == null)
        return;
      this.StatementsGrid.Columns.Remove(dataGridColumn1);
    }

    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //public void InitializeComponent()
    //{
    //  if (this._contentLoaded)
    //    return;
    //  this._contentLoaded = true;
    //  Application.LoadComponent((object) this, new Uri("/EFProf;component/statements/sessionstatementsview.xaml", UriKind.Relative));
    //}

    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[EditorBrowsable(EditorBrowsableState.Never)]
    //void IComponentConnector.Connect(int connectionId, object target)
    //{
    //  switch (connectionId)
    //  {
    //    case 1:
    //      this.Footer = (Grid) target;
    //      break;
    //    case 2:
    //      this.StatementsGrid = (DataGrid) target;
    //      break;
    //    default:
    //      this._contentLoaded = true;
    //      break;
    //  }
    //}
  }
}
