﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Statements.StatementTable
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using System;

namespace HibernatingRhinos.Profiler.Client.Statements
{
  [Serializable]
  public class StatementTable : PropertyChangedBase
  {
    private readonly StatementModel statementModel;

    public StatementTable(string table, StatementModel statementModel)
    {
      this.statementModel = statementModel;
      this.Name = table;
    }

    public string Name { get; set; }

    public override int GetHashCode()
    {
      return this.Name.GetHashCode();
    }

    public override bool Equals(object obj)
    {
      StatementTable statementTable = obj as StatementTable;
      if (statementTable != null)
        return statementTable.Name == this.Name;
      return false;
    }
  }
}
