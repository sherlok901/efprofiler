﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Statements.StatementTablesModel
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using HibernatingRhinos.Profiler.Client.Infrastructure;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq.Expressions;

namespace HibernatingRhinos.Profiler.Client.Statements
{
  public class StatementTablesModel : ScreenWithoutViewCache, IStatementSummaryScreen, IScreen, IHaveDisplayName, IActivate, IDeactivate, IGuardClose, IClose, INotifyPropertyChangedEx, INotifyPropertyChanged
  {
    private ObservableCollection<StatementTable> tables;

    public StatementTablesModel()
    {
      this.TablesStatusCheck();
    }

    public ObservableCollection<StatementTable> Tables
    {
      get
      {
        return this.tables;
      }
      set
      {
        this.tables = value;
        this.NotifyOfPropertyChange<ObservableCollection<StatementTable>>((Expression<Func<ObservableCollection<StatementTable>>>) (() => this.Tables));
        this.TablesStatusCheck();
      }
    }

    public void UpdateStatement(IStatementModel model)
    {
      this.Tables = model == null ? (ObservableCollection<StatementTable>) null : model.Tables;
    }

    private void TablesStatusCheck()
    {
      if (this.Tables != null && this.Tables.Count > 1)
        this.DisplayName = this.Tables.Count.ToString() + " Tables";
      else if (this.Tables != null && this.Tables.Count == 1)
        this.DisplayName = this.Tables.Count.ToString() + " Table";
      else
        this.DisplayName = "No Tables";
    }

    public void Clear()
    {
      if (this.Tables == null)
        return;
      this.Tables.Clear();
    }
  }
}
