﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Statements.StatementSummaryView
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace HibernatingRhinos.Profiler.Client.Statements
{
  public partial class StatementSummaryView : UserControl, IComponentConnector
  {
    //internal Border highlight;
    //internal ListBox Items;
    //internal ContentControl ActiveItem;
    //private bool _contentLoaded;

    public StatementSummaryView()
    {
      this.InitializeComponent();
    }

    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //public void InitializeComponent()
    //{
    //  if (this._contentLoaded)
    //    return;
    //  this._contentLoaded = true;
    //  Application.LoadComponent((object) this, new Uri("/EFProf;component/statements/statementsummaryview.xaml", UriKind.Relative));
    //}

    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //internal Delegate _CreateDelegate(Type delegateType, string handler)
    //{
    //  return Delegate.CreateDelegate(delegateType, (object) this, handler);
    //}

    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[EditorBrowsable(EditorBrowsableState.Never)]
    //[DebuggerNonUserCode]
    //void IComponentConnector.Connect(int connectionId, object target)
    //{
    //  switch (connectionId)
    //  {
    //    case 1:
    //      this.highlight = (Border) target;
    //      break;
    //    case 2:
    //      this.Items = (ListBox) target;
    //      break;
    //    case 3:
    //      this.ActiveItem = (ContentControl) target;
    //      break;
    //    default:
    //      this._contentLoaded = true;
    //      break;
    //  }
    //}
  }
}
