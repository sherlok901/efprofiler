﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Statements.StatementDetailsModel
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using HibernatingRhinos.Profiler.Client.Controls;
using HibernatingRhinos.Profiler.Client.Infrastructure;
using HibernatingRhinos.Profiler.Client.Queries;
using HibernatingRhinos.Profiler.Client.Tracking;
using System;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;

namespace HibernatingRhinos.Profiler.Client.Statements
{
  public class StatementDetailsModel : ScreenWithoutViewCache, IStatementSummaryScreen, IScreen, IHaveDisplayName, IActivate, IDeactivate, IGuardClose, IClose, INotifyPropertyChangedEx, INotifyPropertyChanged
  {
    private readonly ITrackingService trackingService;
    private IStatementModel statement;
    private QueryPlanModel queryPlanModel;
    private QueryResultsModel queryResultsModel;

    public StatementDetailsModel(ITrackingService trackingService)
    {
      this.trackingService = trackingService;
      this.DisplayName = "Details";
    }

    public IStatementModel Statement
    {
      get
      {
        return this.statement;
      }
      private set
      {
        this.statement = value;
        this.NotifyOfPropertyChange<IStatementModel>((Expression<Func<IStatementModel>>) (() => this.Statement));
        this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.CanShowQueryPlanForStatement));
        this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.CanShowResultsForStatement));
        this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.CanCopyParameters));
        this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.CanShowColumn));
      }
    }

    public bool CanCopyParameters
    {
      get
      {
        return this.statement is StatementModel;
      }
    }

    public void CopyParameters()
    {
      StatementModel statement = this.statement as StatementModel;
      if (statement == null)
        return;
      statement.CopyParametersToClipboard();
    }

    public void UpdateStatement(IStatementModel model)
    {
      this.Statement = model;
    }

    public bool CanShowColumn
    {
      get
      {
        if (this.statement == null || this.statement.Parameters == null)
          return false;
        return this.statement.Parameters.Any<DisplayParameter>((Func<DisplayParameter, bool>) (x => !string.IsNullOrEmpty(x.Column)));
      }
    }

    public bool CanShowQueryPlanForStatement
    {
      get
      {
        if (this.statement == null || this.statement.IsTransaction || this.statement.IsDDL)
          return false;
        return !this.statement.IsCached;
      }
    }

    public void ShowQueryPlanForStatement()
    {
      this.queryPlanModel = new QueryPlanModel(this.statement);
      this.queryPlanModel.Query();
    }

    public bool CanShowResultsForStatement
    {
      get
      {
        return this.statement is StatementModel;
      }
    }

    public void ShowResultsForStatement()
    {
      this.trackingService.Track("Sessions", "Execute Query", (string) null, new int?());
      this.queryResultsModel = new QueryResultsModel(((StatementModel) this.statement).Snapshot, this.trackingService);
      if (!this.queryResultsModel.GetQueryResults())
        return;
      Dialog.Show((object) this.queryResultsModel, (object) null, false);
    }

    public void Clear()
    {
      this.Statement = (IStatementModel) null;
    }
  }
}
