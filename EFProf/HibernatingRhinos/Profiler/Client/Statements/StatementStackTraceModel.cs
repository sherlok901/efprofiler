﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Statements.StatementStackTraceModel
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using HibernatingRhinos.Profiler.Appender.StackTraces;
using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.Client.Infrastructure;
using HibernatingRhinos.Profiler.Client.Settings;
using System;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Windows.Input;

namespace HibernatingRhinos.Profiler.Client.Statements
{
  public class StatementStackTraceModel : ScreenWithoutViewCache, IStatementSummaryScreen, IScreen, IHaveDisplayName, IActivate, IDeactivate, IGuardClose, IClose, INotifyPropertyChangedEx, INotifyPropertyChanged
  {
    private StackTraceFrame[] frames;

    public StatementStackTraceModel()
    {
      this.DisplayName = "Stack Trace";
    }

    public StackTraceFrame[] Frames
    {
      get
      {
        return this.frames;
      }
      private set
      {
        this.frames = value;
        this.NotifyOfPropertyChange<StackTraceFrame[]>((Expression<Func<StackTraceFrame[]>>) (() => this.Frames));
      }
    }

    public void UpdateStatement(IStatementModel model)
    {
      if (model == null || model.StackTrace == null)
        this.Frames = (StackTraceFrame[]) null;
      else
        this.Frames = model.StackTrace.Frames;
    }

    public bool CanShowToUser(StackTraceFrame frame)
    {
      return frame.FileExists;
    }

    public void ShowToUser(StackTraceFrame frame, MouseButtonEventArgs mouseClickArgs)
    {
      if (!this.CanShowToUser(frame) || mouseClickArgs.ClickCount < 2)
        return;
      string defaultEditor = UserPreferencesHolder.UserSettings.Configuration.DefaultEditor;
      string editorPath = UserPreferencesHolder.UserSettings.Configuration.EditorPath;
      OpenFileProcessor.OpenFile(frame, defaultEditor, editorPath);
    }

    public void Clear()
    {
      this.Frames = (StackTraceFrame[]) null;
    }
  }
}
