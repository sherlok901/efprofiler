﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Statements.DisplayParameter
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System.Data;

namespace HibernatingRhinos.Profiler.Client.Statements
{
  public class DisplayParameter
  {
    public string Name { get; set; }

    public string Value { get; set; }

    public string Type { get; set; }

    public string Column { get; set; }

    public object ParameterValue { get; set; }

    public DataTable DataTable
    {
      get
      {
        return this.ParameterValue as DataTable;
      }
    }

    public bool IsStructuredParameter
    {
      get
      {
        return this.Value == "Structured";
      }
    }
  }
}
