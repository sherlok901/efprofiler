﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Statements.StatementAlertsModel
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using HibernatingRhinos.Profiler.Client.Infrastructure;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq.Expressions;

namespace HibernatingRhinos.Profiler.Client.Statements
{
  public class StatementAlertsModel : ScreenWithoutViewCache, IStatementSummaryScreen, IScreen, IHaveDisplayName, IActivate, IDeactivate, IGuardClose, IClose, INotifyPropertyChangedEx, INotifyPropertyChanged
  {
    private ObservableCollection<StatementAlert> alerts;

    public StatementAlertsModel()
    {
      this.AlertsStatusCheck();
    }

    public ObservableCollection<StatementAlert> Alerts
    {
      get
      {
        return this.alerts;
      }
      private set
      {
        this.alerts = value;
        this.NotifyOfPropertyChange<ObservableCollection<StatementAlert>>((Expression<Func<ObservableCollection<StatementAlert>>>) (() => this.Alerts));
        this.AlertsStatusCheck();
      }
    }

    public void UpdateStatement(IStatementModel model)
    {
      if (model == null)
        this.Alerts = (ObservableCollection<StatementAlert>) null;
      else
        this.Alerts = model.Alerts;
    }

    public void Clear()
    {
      if (this.Alerts == null)
        return;
      this.Alerts.Clear();
    }

    private void AlertsStatusCheck()
    {
      if (this.Alerts != null && this.Alerts.Count > 1)
        this.DisplayName = this.Alerts.Count.ToString() + " Alerts";
      else if (this.Alerts != null && this.Alerts.Count == 1)
        this.DisplayName = this.Alerts.Count.ToString() + " Alert";
      else
        this.DisplayName = "No Alerts";
    }
  }
}
