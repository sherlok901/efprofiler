﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Statements.SessionStatementsModel
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.BackEnd.Extensions;
using HibernatingRhinos.Profiler.Client.Filtering;
using HibernatingRhinos.Profiler.Client.Infrastructure;
using HibernatingRhinos.Profiler.Client.Sessions;
using HibernatingRhinos.Profiler.Client.Shell;
using HibernatingRhinos.Profiler.Client.Tracking;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;

namespace HibernatingRhinos.Profiler.Client.Statements
{
  public class SessionStatementsModel : PollingScreen, INavigateItems
  {
    private readonly IDictionary<Guid, IStatementModel> statementsById = (IDictionary<Guid, IStatementModel>) new Dictionary<Guid, IStatementModel>();
    private readonly ObservableCollection<IStatementModel> unfilteredStatements = new ObservableCollection<IStatementModel>();
    private static SessionStatementsView singletonView;
    private readonly FilterServiceModel filter;
    private readonly ITrackingService trackingService;
    private readonly int? maxStatements;
    private readonly bool canJumpToSession;
    private readonly SessionItemBase sessionModel;
    private IStatementModel selectedStatement;
    private int statementCount;
    private readonly StatementSummaryModel selectedSummary;
    private bool skipSelectionChange;
    private EventHandler filterOnFiltersChanged;
    private int etag;
    private ApplicationModel applicationModel;

    public SessionStatementsModel(FilterServiceModel filter, ITrackingService trackingService, int? maxStatements, bool canJumpToSession, SessionItemBase sessionModel, BackendBridge backendBridge)
    {
      this.filter = filter;
      this.trackingService = trackingService;
      this.maxStatements = maxStatements;
      this.canJumpToSession = canJumpToSession;
      this.sessionModel = sessionModel;
      this.filterOnFiltersChanged = new EventHandler(this.HandleStatementsOrFiltersChanging);
      filter.FiltersChanged += this.filterOnFiltersChanged;
      this.selectedSummary = new StatementSummaryModel(this, backendBridge, trackingService);
      this.DisplayName = nameof (Statements);
      this.StatementsCollection = new CollectionViewSource()
      {
        Source = (object) this.Statements
      };
    }

    public int StatementCount
    {
      get
      {
        return this.statementCount;
      }
      private set
      {
        this.statementCount = value;
        this.NotifyOfPropertyChange<int>((Expression<Func<int>>) (() => this.StatementCount));
      }
    }

    public CollectionViewSource StatementsCollection { get; set; }

    public ObservableCollection<IStatementModel> Statements
    {
      get
      {
        return this.unfilteredStatements;
      }
    }

    public IEnumerable<IStatementModel> FilteredStatements
    {
      get
      {
        if (this.StatementsCollection.View.Filter != null)
          return (IEnumerable<IStatementModel>) this.Statements.Where<IStatementModel>(new Func<IStatementModel, bool>(this.FilterStatements)).ToList<IStatementModel>();
        return (IEnumerable<IStatementModel>) this.Statements;
      }
    }

    public StatementSummaryModel SelectedSummary
    {
      get
      {
        return this.selectedSummary;
      }
    }

    public void LoadViewAction(Control ctrl)
    {
      this.CurrentViewControl = ctrl;
    }

    public Control CurrentViewControl { get; private set; }

    public IStatementModel SelectedStatement
    {
      get
      {
        if (this.selectedStatement == null && this.unfilteredStatements.Count > 0 && this.FilteredStatements.Any<IStatementModel>())
          this.SelectedStatement = this.unfilteredStatements[0];
        return this.selectedStatement;
      }
      set
      {
        if (this.selectedStatement == value)
          return;
        foreach (IStatementModel unfilteredStatement in (Collection<IStatementModel>) this.unfilteredStatements)
          unfilteredStatement.IsSelected = false;
        this.selectedStatement = value;
        if (this.selectedStatement != null)
        {
          this.selectedStatement.IsSelected = true;
          if (!this.selectedStatement.IsAggregate)
          {
            this.skipSelectionChange = true;
            this.StatementsCollection.View.MoveCurrentTo((object) this.selectedStatement);
            if (this.ApplicationModel.IsTrackLatestStatements && this.CurrentViewControl != null)
            {
              FieldInfo field = this.CurrentViewControl.GetType().GetField("StatementsGrid", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
              if (field != (FieldInfo) null)
              {
                DataGrid dataGrid = field.GetValue((object) this.CurrentViewControl) as DataGrid;
                if (dataGrid != null)
                  dataGrid.ScrollIntoView((object) this.selectedStatement);
              }
            }
            this.skipSelectionChange = false;
          }
        }
        this.NotifyOfPropertyChange<IStatementModel>((Expression<Func<IStatementModel>>) (() => this.SelectedStatement));
        this.NotifyOfPropertyChange<StatementSummaryModel>((Expression<Func<StatementSummaryModel>>) (() => this.SelectedSummary));
      }
    }

    public event System.Action<ObservableCollection<IStatementModel>> StatementsChanged = param0 => {};

    public void Clear()
    {
      this.SelectedStatement = (IStatementModel) null;
      this.unfilteredStatements.Clear();
      this.statementsById.Clear();
      this.selectedSummary.Clear();
      this.HandleStatementsOrFiltersChanging((object) this, EventArgs.Empty);
    }

    public void ClearExcept(ICollection<Guid> sessionIds)
    {
      if (this.SelectedStatement != null && !sessionIds.Contains(this.SelectedStatement.SessionId))
        this.SelectedStatement = (IStatementModel) null;
      foreach (IStatementModel statementModel in this.unfilteredStatements.ToArray<IStatementModel>())
      {
        if (!sessionIds.Contains(statementModel.SessionId))
          this.unfilteredStatements.Remove(statementModel);
      }
      ((IEnumerable<KeyValuePair<Guid, IStatementModel>>) this.statementsById.Where<KeyValuePair<Guid, IStatementModel>>((Func<KeyValuePair<Guid, IStatementModel>, bool>) (x => !sessionIds.Contains(x.Value.SessionId))).ToArray<KeyValuePair<Guid, IStatementModel>>()).ForEach<KeyValuePair<Guid, IStatementModel>>((System.Action<KeyValuePair<Guid, IStatementModel>>) (pair => this.statementsById.Remove(pair.Key)));
      this.HandleStatementsOrFiltersChanging((object) this, EventArgs.Empty);
    }

    public void Update(IEnumerable<StatementSnapshot> snapshots)
    {
      bool flag = false;
      foreach (StatementSnapshot snapshot in snapshots)
      {
        IStatementModel statementModel1;
        if (this.statementsById.TryGetValue(snapshot.StatementId, out statementModel1))
        {
          flag |= statementModel1.UpdateWith(snapshot);
        }
        else
        {
          flag = true;
          StatementModel statementModel2 = new StatementModel(snapshot)
          {
            CanJumpToSession = this.canJumpToSession
          };
          this.unfilteredStatements.Add((IStatementModel) statementModel2);
          this.statementsById.Add(snapshot.StatementId, (IStatementModel) statementModel2);
          if (this.maxStatements.HasValue)
          {
            int count = this.unfilteredStatements.Count;
            int? maxStatements = this.maxStatements;
            if ((count <= maxStatements.GetValueOrDefault() ? 0 : (maxStatements.HasValue ? 1 : 0)) != 0)
            {
              IStatementModel unfilteredStatement = this.unfilteredStatements[0];
              this.unfilteredStatements.RemoveAt(0);
              this.statementsById.Remove(unfilteredStatement.SessionId);
            }
          }
        }
      }
      if (flag)
        this.HandleStatementsOrFiltersChanging((object) this, EventArgs.Empty);
      if (this.NotifyOnUpdate != null)
        this.NotifyOnUpdate();
      this.TrackLatestStatement();
    }

    private void TrackLatestStatement()
    {
      if (!this.ApplicationModel.IsTrackLatestStatements)
        return;
      IStatementModel statementModel = this.Statements.LastOrDefault<IStatementModel>();
      if (statementModel == null)
        return;
      this.skipSelectionChange = true;
      this.SelectedStatement = statementModel;
      this.skipSelectionChange = false;
    }

    protected override void OnActivate()
    {
      this.trackingService.Track("Sessions", "Statements", (string) null, new int?());
      base.OnActivate();
    }

    public override void TimerTicked(object sender, EventArgs e)
    {
      if (this.State == PollingState.Finalized)
      {
        PollingService.UnRegister(new EventHandler(((PollingScreen) this).TimerTicked));
      }
      else
      {
        int statementsEtag = this.sessionModel.StatementsEtag;
        if (statementsEtag > this.etag || statementsEtag == -1)
        {
          this.etag = statementsEtag;
          this.Update(this.sessionModel.GetStatements());
        }
        if (this.State != PollingState.NotFinalized)
          return;
        this.State = PollingState.Finalized;
      }
    }

    public override void ForceUpdate()
    {
      this.Update(this.sessionModel.GetStatements());
      if (this.State != PollingState.NotFinalized)
        return;
      this.State = PollingState.Finalized;
    }

    private void HandleStatementsOrFiltersChanging(object sender, EventArgs eventArgs)
    {
      this.BuildFilteredView();
      this.StatementsChanged(this.Statements);
      this.NotifyOfPropertyChange<ObservableCollection<IStatementModel>>((Expression<Func<ObservableCollection<IStatementModel>>>) (() => this.Statements));
      this.NotifyOfPropertyChange<IEnumerable<IStatementModel>>((Expression<Func<IEnumerable<IStatementModel>>>) (() => this.FilteredStatements));
    }

    private void BuildFilteredView()
    {
      this.StatementsCollection.View.Filter = (Predicate<object>) null;
      if (this.filter.Filters.Count != 0)
        this.StatementsCollection.View.Filter = new Predicate<object>(this.FilterStatements);
      if (this.SelectedStatement != null)
        return;
      this.SelectedStatement = (IStatementModel) this.StatementsCollection.View.CurrentItem;
    }

    private bool FilterStatements(object statement)
    {
      return this.filter.FilterStatement((IFilterableStatementSnapshot) statement);
    }

    public void NextItem()
    {
      this.SelectedStatement = NavigationManager.GetNext<IStatementModel>((IList<IStatementModel>) this.Statements, this.SelectedStatement);
    }

    public void PreviousItem()
    {
      this.SelectedStatement = NavigationManager.GetPrevious<IStatementModel>((IList<IStatementModel>) this.Statements, this.SelectedStatement);
    }

    public void CancelSorting(MouseButtonEventArgs e)
    {
      this.StatementsCollection.View.SortDescriptions.Clear();
      e.Handled = true;
    }

    public void SelectStatements(IList items)
    {
      if (items == null || this.skipSelectionChange)
        return;
      this.skipSelectionChange = true;
      this.SetSelectedStatements(items.OfType<IStatementModel>().OrderBy<IStatementModel, int>(new Func<IStatementModel, int>(this.GetStatementIndex)).ToArray<IStatementModel>());
      this.skipSelectionChange = false;
    }

    public int GetStatementIndex(IStatementModel statementModel)
    {
      return this.Statements.IndexOf(statementModel);
    }

    public void SetSelectedStatements(IStatementModel[] selection)
    {
      if (this.SelectedStatement != null)
        this.SelectedStatement.UnrootStatement();
      this.trackingService.Track("Main", "Selection", "Statements", new int?(selection.Length));
      switch (selection.Length)
      {
        case 0:
          this.SelectedStatement = (IStatementModel) null;
          break;
        case 1:
          this.SelectedStatement = selection[0];
          break;
        default:
          if (selection.Length <= 1)
            break;
          this.SelectedStatement = (IStatementModel) new StatementModel(new StatementSnapshot()
          {
            FormattedSql = ((IEnumerable<IStatementModel>) selection).Aggregate((Func<IStatementModel, string>) (x => x.Text)),
            ShortSql = ((IEnumerable<IStatementModel>) selection).Aggregate((Func<IStatementModel, string>) (x => x.ShortText)),
            RawSql = ((IEnumerable<IStatementModel>) selection).Aggregate((Func<IStatementModel, string>) (x => x.RawSql)),
            CanExecuteQuery = false,
            IsSelectStatement = false,
            Url = selection[0].Url,
            StarColor = selection[0].StarColor,
            Alerts = new AlertInformation[0],
            Parameters = (IEnumerable<HibernatingRhinos.Profiler.BackEnd.Messages.Parameter>) new HibernatingRhinos.Profiler.BackEnd.Messages.Parameter[0],
            Duration = ((IEnumerable<IStatementModel>) selection).Aggregate((Func<IStatementModel, QueryDuration>) (x => x.Duration)),
            CountOfRows = ((IEnumerable<IStatementModel>) selection).Aggregate((Func<IStatementModel, int[]>) (x => x.CountOfRows)),
            Tables = new string[0]
          })
          {
            CanJumpToSession = false,
            Children = (IEnumerable<IStatementModel>) selection,
            IsAggregate = true
          };
          foreach (IStatementModel statementModel in selection)
            statementModel.IsSelected = true;
          break;
      }
    }

    public void ShowAlerts()
    {
      this.SelectedSummary.SelectAlerts();
    }

    public void Star(StatementModel item)
    {
      if (item == null)
        return;
      item.IsStarred = !item.IsStarred;
    }

    private ApplicationModel ApplicationModel
    {
      get
      {
        return this.applicationModel ?? (this.applicationModel = IoC.Get<ApplicationModel>((string) null));
      }
    }
  }
}
