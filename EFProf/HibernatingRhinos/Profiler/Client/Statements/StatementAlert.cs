﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Statements.StatementAlert
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using HibernatingRhinos.Profiler.Appender.StackTraces;
using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.Profiles;
using HibernatingRhinos.Profiler.Client.Settings;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HibernatingRhinos.Profiler.Client.Statements
{
  [Serializable]
  public class StatementAlert : PropertyChangedBase
  {
    private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(typeof (StatementAlert));
    private readonly StatementModel statementModel;

    public StatementAlert(AlertInformation alert, StatementModel statementModel)
    {
      this.statementModel = statementModel;
      this.Title = alert.Title;
      this.Severity = alert.Severity;
      if (statementModel.OrmError != null)
      {
        try
        {
          this.HelpUri = Profile.Current.GetLearnTopic("error", statementModel.ErrorSolution == null ? "detected?message=" + Uri.EscapeDataString(statementModel.OrmError) : statementModel.ErrorSolution.Slug);
        }
        catch (UriFormatException ex)
        {
          if (ex.Message == "Invalid URI: The Uri string is too long.")
            StatementAlert.Logger.Debug((object) ("Please send us the following log to support@hibernatingrhinos.com. " + statementModel.OrmError), (Exception) ex);
          else
            StatementAlert.Logger.Debug((object) ("Got an exception. Orm Error: " + statementModel.OrmError), (Exception) ex);
        }
      }
      else
        this.HelpUri = !(alert.HelpTopic == "error/detected") ? Profile.Current.GetLearnTopic(nameof (alert), alert.HelpTopic) : Profile.Current.GetLearnTopic("error", "detected");
      this.HelpTopic = alert.HelpTopic;
    }

    public void IgnoreAlert()
    {
      IgnoredAlertInfo ignoredAlertInfo = new IgnoredAlertInfo()
      {
        Alert = this.Title,
        RawSql = this.statementModel.RawSql,
        StackTraceFrames = this.GetStackTraceFrames() ?? new string[0]
      };
      this.statementModel.Alerts.Remove(this);
      UserPreferencesHolder.UserSettings.Configuration.IgnoredAlertsOnStatements.Add(ignoredAlertInfo);
    }

    private string[] GetStackTraceFrames()
    {
      if (this.statementModel.Snapshot.StackTrace == null || this.statementModel.Snapshot.StackTrace.Frames == null)
        return (string[]) null;
      return ((IEnumerable<StackTraceFrame>) this.statementModel.Snapshot.StackTrace.Frames).Select<StackTraceFrame, string>((Func<StackTraceFrame, string>) (x => x.Method)).ToArray<string>();
    }

    public Severity Severity { get; set; }

    public string Title { get; set; }

    public string HelpUri { get; set; }

    public string HelpTopic { get; set; }

    public override int GetHashCode()
    {
      return this.Title.GetHashCode();
    }

    public override bool Equals(object obj)
    {
      StatementAlert statementAlert = obj as StatementAlert;
      if (statementAlert != null)
        return statementAlert.Title == this.Title;
      return false;
    }
  }
}
