﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Statements.StatementsExtensions
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.BackEnd;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HibernatingRhinos.Profiler.Client.Statements
{
  public static class StatementsExtensions
  {
    public static string Aggregate(this IEnumerable<IStatementModel> self, Func<IStatementModel, string> func)
    {
      StringBuilder stringBuilder = new StringBuilder();
      int num = 1;
      foreach (IStatementModel statementModel in self)
      {
        stringBuilder.AppendLine("-- statement #" + (object) num).AppendLine(func(statementModel)).AppendLine();
        ++num;
      }
      return stringBuilder.ToString();
    }

    public static QueryDuration Aggregate(this IEnumerable<IStatementModel> self, Func<IStatementModel, QueryDuration> func)
    {
      QueryDuration queryDuration1 = new QueryDuration()
      {
        InDatabase = new int?(0),
        InNHibernate = new int?(0)
      };
      foreach (IStatementModel statementModel in self)
      {
        QueryDuration queryDuration2 = queryDuration1;
        int? inDatabase = queryDuration2.InDatabase;
        int num1 = func(statementModel).InDatabase ?? 0;
        queryDuration2.InDatabase = inDatabase.HasValue ? new int?(inDatabase.GetValueOrDefault() + num1) : new int?();
        QueryDuration queryDuration3 = queryDuration1;
        int? inNhibernate = queryDuration3.InNHibernate;
        int num2 = func(statementModel).InNHibernate ?? 0;
        queryDuration3.InNHibernate = inNhibernate.HasValue ? new int?(inNhibernate.GetValueOrDefault() + num2) : new int?();
      }
      if (self.Where<IStatementModel>((Func<IStatementModel, bool>) (x => !x.Duration.InDatabase.HasValue)).Count<IStatementModel>() == self.Count<IStatementModel>())
        queryDuration1.InDatabase = new int?();
      if (self.Where<IStatementModel>((Func<IStatementModel, bool>) (x => !x.Duration.InNHibernate.HasValue)).Count<IStatementModel>() == self.Count<IStatementModel>())
        queryDuration1.InNHibernate = new int?();
      return queryDuration1;
    }

    public static int[] Aggregate(this IEnumerable<IStatementModel> self, Func<IStatementModel, int[]> func)
    {
      int length = self.Aggregate<IStatementModel, int>(0, (Func<int, IStatementModel, int>) ((current, model) =>
      {
        if (model.CountOfRows.Length <= current)
          return current;
        return model.CountOfRows.Length;
      }));
      int[] numArray = new int[length];
      using (IEnumerator<IStatementModel> enumerator = self.GetEnumerator())
      {
label_5:
        while (enumerator.MoveNext())
        {
          IStatementModel current = enumerator.Current;
          int index = 0;
          while (true)
          {
            if (index < length && index < ((IEnumerable<int>) func(current)).Count<int>())
            {
              numArray[index] += func(current)[index];
              ++index;
            }
            else
              goto label_5;
          }
        }
      }
      return numArray;
    }
  }
}
