﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Converters.CollapsedWhenEmptyConverter
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System;
using System.Collections;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace HibernatingRhinos.Profiler.Client.Converters
{
  public class CollapsedWhenEmptyConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      ICollection collection = value as ICollection;
      if (collection != null)
      {
        if (parameter != null && parameter is string && (string) parameter == "!")
          return (object) (Visibility) (collection.Count > 0 ? 2 : 0);
        return (object) (Visibility) (collection.Count > 0 ? 0 : 2);
      }
      string str = value as string;
      if (str == null)
        return (object) Visibility.Collapsed;
      if (parameter != null && parameter is string && (string) parameter == "!")
        return (object) (Visibility) (string.IsNullOrEmpty(str) ? 0 : 2);
      return (object) (Visibility) (string.IsNullOrEmpty(str) ? 2 : 0);
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
