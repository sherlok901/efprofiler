﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Converters.IsRecentStatementsModelConverter
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.Client.Statements;
using System;
using System.Globalization;
using System.Windows.Data;

namespace HibernatingRhinos.Profiler.Client.Converters
{
  public class IsRecentStatementsModelConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      CollectionViewGroup collectionViewGroup = value as CollectionViewGroup;
      if (collectionViewGroup == null)
        return (object) false;
      if (string.IsNullOrEmpty(collectionViewGroup.Name.ToString()))
        return (object) true;
      if (collectionViewGroup.Items != null && collectionViewGroup.Items.Count > 0 && collectionViewGroup.Items[0] is RecentStatementsModel)
        return (object) true;
      return (object) false;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
