﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Converters.CountOfRowsConverter
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System;
using System.Globalization;
using System.Text;
using System.Windows.Data;

namespace HibernatingRhinos.Profiler.Client.Converters
{
  public class CountOfRowsConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      return (object) CountOfRowsConverter.Write((int[]) value);
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }

    public static string Write(int[] list)
    {
      if (list == null || list.Length == 0)
        return "";
      if (list.Length == 1)
        return list[0].ToString((IFormatProvider) CultureInfo.InvariantCulture);
      StringBuilder stringBuilder = new StringBuilder();
      foreach (int num in list)
        stringBuilder.Append(num).Append(" / ");
      stringBuilder.Length -= 3;
      return stringBuilder.ToString();
    }
  }
}
