﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Converters.OperatorDisplayNameConverter
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq.Expressions;
using System.Windows.Data;

namespace HibernatingRhinos.Profiler.Client.Converters
{
  public class OperatorDisplayNameConverter : IValueConverter
  {
    private static readonly Dictionary<ExpressionType, string> Lookup = new Dictionary<ExpressionType, string>()
    {
      {
        ExpressionType.GreaterThan,
        "greater than"
      },
      {
        ExpressionType.LessThan,
        "less than"
      }
    };

    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if (value == null)
        return (object) null;
      ExpressionType key = (ExpressionType) value;
      if (!OperatorDisplayNameConverter.Lookup.ContainsKey(key))
        return value;
      return (object) OperatorDisplayNameConverter.Lookup[key];
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
