﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Converters.TypeToDescriptionConverter
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.BackEnd.Profiles;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace HibernatingRhinos.Profiler.Client.Converters
{
  public class TypeToDescriptionConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      Type type = value as Type;
      if (!(type != (Type) null))
        throw new InvalidOperationException("Value must by Type object");
      DescriptionAttribute[] customAttributes = (DescriptionAttribute[]) type.GetCustomAttributes(typeof (DescriptionAttribute), false);
      if (((IEnumerable<DescriptionAttribute>) customAttributes).Count<DescriptionAttribute>() == 0)
        return (object) "No description attribute";
      return (object) Profile.Current.Translate(customAttributes[0].Description);
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
