﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Converters.VisibleWhenHasRelatedSessionsConverter
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.Client.Reports;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace HibernatingRhinos.Profiler.Client.Converters
{
  public class VisibleWhenHasRelatedSessionsConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if (value is AggregatedAlertSnapshot || value is QueryAggregation)
        return (object) Visibility.Visible;
      return (object) Visibility.Collapsed;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
