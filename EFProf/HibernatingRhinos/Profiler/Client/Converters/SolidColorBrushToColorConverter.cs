﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Converters.SolidColorBrushToColorConverter
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace HibernatingRhinos.Profiler.Client.Converters
{
  public class SolidColorBrushToColorConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      SolidColorBrush solidColorBrush = value as SolidColorBrush;
      return (object) (solidColorBrush == null ? Colors.Black : solidColorBrush.Color);
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
