﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Converters.SessionIdToSessionNameConverter
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using HibernatingRhinos.Profiler.BackEnd.Profiles;
using HibernatingRhinos.Profiler.Client.Sessions;
using System;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace HibernatingRhinos.Profiler.Client.Converters
{
  public class SessionIdToSessionNameConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      Guid sessionId = (Guid) value;
      ISessionItem sessionItem = IoC.Get<SessionListModel>((string) null).Items.FirstOrDefault<ISessionItem>((Func<ISessionItem, bool>) (x => x.Id == sessionId));
      if (sessionItem == null)
        return (object) string.Empty;
      return (object) string.Format("Jump to {0}", (object) Profile.Current.Translate(sessionItem.DisplayName));
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
