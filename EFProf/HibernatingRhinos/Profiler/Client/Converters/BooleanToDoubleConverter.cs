﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Converters.BooleanToDoubleConverter
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System;
using System.Globalization;
using System.Windows.Data;

namespace HibernatingRhinos.Profiler.Client.Converters
{
  public class BooleanToDoubleConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      bool? nullable = (bool?) value;
      if (nullable.HasValue)
        return (object) (nullable.Value ? 1.0 : 0.0);
      return (object) 0.0;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      double? nullable = (double?) value;
      if (nullable.HasValue)
        return (object) (nullable.Value != 0.0);
      return (object) false;
    }
  }
}
