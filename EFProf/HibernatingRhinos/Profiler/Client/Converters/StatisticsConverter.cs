﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Converters.StatisticsConverter
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System;
using System.Collections;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace HibernatingRhinos.Profiler.Client.Converters
{
  public class StatisticsConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      IEnumerable source = value as IEnumerable;
      if (source != null && !(value is string))
        return (object) (source.Cast<object>().Count<object>().ToString() + " items");
      return value;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
