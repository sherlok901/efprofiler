﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Converters.EnumConverter
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.Client.CaliburnInfrastructure;
using System;
using System.Globalization;
using System.Windows.Data;

namespace HibernatingRhinos.Profiler.Client.Converters
{
  public class EnumConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if (typeof (int).IsAssignableFrom(targetType))
        return (object)System.Convert.ToInt32(value);
      if (typeof (byte).IsAssignableFrom(targetType))
        return (object) System.Convert.ToByte(value);
      return (object) BindableEnum.Create(value);
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if (value == null)
        return (object) null;
      if (value is string)
        return Enum.Parse(targetType, value.ToString(), true);
      if (!(value.GetType() == targetType))
        return ((BindableEnum) value).Value;
      return value;
    }
  }
}
