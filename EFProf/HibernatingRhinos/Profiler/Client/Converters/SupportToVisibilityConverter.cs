﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Converters.SupportToVisibilityConverter
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.BackEnd.Profiles;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace HibernatingRhinos.Profiler.Client.Converters
{
  public class SupportToVisibilityConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      string str = value as string;
      if (str == null)
        return (object) Visibility.Collapsed;
      return (object) (Visibility) (Profile.Current.Supports((SupportedFeatures) Enum.Parse(typeof (SupportedFeatures), str, true)) ? 0 : 2);
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
