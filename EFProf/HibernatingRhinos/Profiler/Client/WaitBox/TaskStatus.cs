﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.WaitBox.TaskStatus
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System.Threading.Tasks;

namespace HibernatingRhinos.Profiler.Client.WaitBox
{
  public class TaskStatus
  {
    public string ExceptionMessage { get; set; }

    public bool Complete { get; set; }

    public string MessageText { get; set; }

    public string Details { get; set; }

    public int WaitingMsgs { get; set; }

    public Task Task { get; set; }

    public int CompletedPercent { get; set; }
  }
}
