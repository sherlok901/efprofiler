﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.ErrorReporting.ErrorReporter
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.BackEnd.Profiles;
using HibernatingRhinos.Profiler.Client.Infrastructure;
using HibernatingRhinos.Profiler.Client.Settings;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Net;
using System.Text;

namespace HibernatingRhinos.Profiler.Client.ErrorReporting
{
  public class ErrorReporter
  {
    private readonly string baseUrl = Profile.Current.ErrorUrl;

    public string ReportError(UserPreferences userPreferences, string errorMessage)
    {
      FileVersionInfo versionInfo = FileVersionInfo.GetVersionInfo(typeof (ErrorReporter).Assembly.Location);
      Dictionary<string, string> dictionary = new Dictionary<string, string>()
      {
        {
          "UserId",
          userPreferences.UserId.ToString()
        },
        {
          "Error",
          errorMessage + Environment.NewLine + "----- EOF report."
        },
        {
          "UserName",
          Environment.UserName
        },
        {
          "OperationSystem",
          Environment.OSVersion.ToString()
        },
        {
          "ProcessorCount",
          Environment.ProcessorCount.ToString((IFormatProvider) CultureInfo.InvariantCulture)
        },
        {
          "UserEmail",
          this.GetEmail(userPreferences.EmailAddress)
        },
        {
          "FrameworkVersion",
          Environment.Version.ToString()
        },
        {
          "Build",
          BuildInfo.GetCurrentBuild() + ":" + (object) versionInfo
        },
        {
          "Reporter",
          "Client"
        },
        {
          "Profile",
          Profile.CurrentProfile
        }
      };
      StringBuilder stringBuilder = new StringBuilder();
      foreach (KeyValuePair<string, string> keyValuePair in dictionary)
      {
        stringBuilder.Append(Uri.EscapeDataString(keyValuePair.Key ?? "Null_Key"));
        stringBuilder.Append("=");
        stringBuilder.Append(Uri.EscapeDataString(keyValuePair.Value ?? "Null_Value"));
        stringBuilder.Append("&");
      }
      string str = stringBuilder.ToString();
      HttpWebRequest httpWebRequest = (HttpWebRequest) WebRequest.Create(this.baseUrl);
      httpWebRequest.Method = "POST";
      httpWebRequest.ContentType = "application/x-www-form-urlencoded";
      using (StreamWriter streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
      {
        streamWriter.Write(str);
        streamWriter.Flush();
      }
      using (WebResponse response = httpWebRequest.GetResponse())
        return new StreamReader(response.GetResponseStream()).ReadToEnd();
    }

    private string GetEmail(string emailAddress)
    {
      if (string.IsNullOrWhiteSpace(emailAddress))
        return "no-provided-email@profiler.com";
      if (!ModelValidator.EmailValidator(emailAddress))
        return "no-valid-email@profiler.com";
      return emailAddress;
    }
  }
}
