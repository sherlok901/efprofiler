﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.ErrorReporting.ReportingUserInfoModel
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.BackEnd.Profiles;
using HibernatingRhinos.Profiler.Client.CaliburnInfrastructure;
using HibernatingRhinos.Profiler.Client.Infrastructure;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;

namespace HibernatingRhinos.Profiler.Client.ErrorReporting
{
  public class ReportingUserInfoModel : ProfilerScreen
  {
    private ValidatableProperty<string> emailProperty;

    public ReportingUserInfoModel()
    {
      ValidatableProperty<string> validatableProperty = new ValidatableProperty<string>(nameof (Email));
      validatableProperty.InitialValue = (object) "";
      validatableProperty.Validators.Add((ValidationAttribute) new RequiredAttribute());
      validatableProperty.Validators.Add((ValidationAttribute) new EmailAddressAttribute());
      this.emailProperty = validatableProperty;
      // ISSUE: explicit constructor call
      //base.\u002Ector();
      this.DisplayName = Profile.CurrentProfileDisplayName + ": Unhandled exception";
      this.emailProperty.PropertyChanged += (PropertyChangedEventHandler) ((param0, param1) => this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.CanSend)));
    }

    public ValidatableProperty<string> Email
    {
      get
      {
        return this.emailProperty;
      }
    }

    public void Send()
    {
      this.TryClose(new bool?(true));
    }

    public bool CanSend
    {
      get
      {
        return this.emailProperty.IsValid;
      }
    }
  }
}
