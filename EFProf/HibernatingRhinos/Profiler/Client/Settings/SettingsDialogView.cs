﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Settings.SettingsDialogView
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace HibernatingRhinos.Profiler.Client.Settings
{
  public partial class SettingsDialogView : UserControl, IComponentConnector
  {
    //internal Button Ok;
    //internal Button TryClose;
    //internal Button Reset;
    //internal TextBox namespaceToAdd;
    //internal ComboBox alertToDisable;
    //private bool _contentLoaded;

    public SettingsDialogView()
    {
      this.InitializeComponent();
    }

    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //public void InitializeComponent()
    //{
    //  if (this._contentLoaded)
    //    return;
    //  this._contentLoaded = true;
    //  Application.LoadComponent((object) this, new Uri("/EFProf;component/settings/settingsdialogview.xaml", UriKind.Relative));
    //}

    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[EditorBrowsable(EditorBrowsableState.Never)]
    //void IComponentConnector.Connect(int connectionId, object target)
    //{
    //  switch (connectionId)
    //  {
    //    case 1:
    //      this.Ok = (Button) target;
    //      break;
    //    case 2:
    //      this.TryClose = (Button) target;
    //      break;
    //    case 3:
    //      this.Reset = (Button) target;
    //      break;
    //    case 4:
    //      this.namespaceToAdd = (TextBox) target;
    //      break;
    //    case 5:
    //      this.alertToDisable = (ComboBox) target;
    //      break;
    //    default:
    //      this._contentLoaded = true;
    //      break;
    //  }
    //}
  }
}
