﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Settings.UserPreferencesHolder
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.BackEnd.Profiles;
using log4net;
using Newtonsoft.Json;
using System;
using System.IO;

namespace HibernatingRhinos.Profiler.Client.Settings
{
  [Serializable]
  public static class UserPreferencesHolder
  {
    private static readonly ILog Log = LogManager.GetLogger(typeof (UserPreferencesHolder));
    private static UserPreferences userSettings;

    public static UserPreferences UserSettings
    {
      get
      {
        return UserPreferencesHolder.userSettings ?? (UserPreferencesHolder.userSettings = UserPreferencesHolder.LoadUserSettings());
      }
      set
      {
        UserPreferencesHolder.userSettings = value;
      }
    }

    private static UserPreferences LoadUserSettings()
    {
      try
      {
        if (!File.Exists(UserPreferencesHolder.UsersSettingsFilename))
          return new UserPreferences(true);
        using (FileStream fileStream = File.Open(UserPreferencesHolder.UsersSettingsFilename, FileMode.Open, FileAccess.Read))
        {
          using (StreamReader streamReader = new StreamReader((Stream) fileStream))
          {
            using (JsonTextReader jsonTextReader = new JsonTextReader((TextReader) streamReader))
            {
              UserPreferences userPreferences = new JsonSerializer().Deserialize<UserPreferences>((JsonReader) jsonTextReader);
              userPreferences.EnsureIsValid();
              return userPreferences;
            }
          }
        }
      }
      catch (UnauthorizedAccessException ex)
      {
        return new UserPreferences(true);
      }
      catch (JsonReaderException ex)
      {
        File.Delete(UserPreferencesHolder.UsersSettingsFilename);
        return new UserPreferences(true);
      }
      catch (Exception ex)
      {
        return new UserPreferences(true);
      }
    }

    private static string UsersSettingsFilename
    {
      get
      {
        return Path.Combine(UserPreferencesHolder.SettingsDirectory, "UserSettings.json");
      }
    }

    public static string SettingsDirectory
    {
      get
      {
        string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), Profile.CurrentProfileDisplayName);
        if (!Directory.Exists(path))
          Directory.CreateDirectory(path);
        return path;
      }
    }

    public static void Save()
    {
      try
      {
        using (FileStream fileStream = File.Create(UserPreferencesHolder.UsersSettingsFilename))
        {
          using (StreamWriter streamWriter = new StreamWriter((Stream) fileStream))
          {
            JsonTextWriter jsonTextWriter1 = new JsonTextWriter((TextWriter) streamWriter);
            jsonTextWriter1.Formatting = Formatting.Indented;
            using (JsonTextWriter jsonTextWriter2 = jsonTextWriter1)
            {
              new JsonSerializer().Serialize((JsonWriter) jsonTextWriter2, (object) UserPreferencesHolder.UserSettings);
              fileStream.Flush();
            }
          }
        }
      }
      catch (UnauthorizedAccessException ex)
      {
        UserPreferencesHolder.Log.Error((object) "Could not save users settings. Got UnauthorizedAccessException.", (Exception) ex);
      }
      catch (Exception ex)
      {
        UserPreferencesHolder.Log.Error((object) "Could not save users settings", ex);
      }
    }
  }
}
