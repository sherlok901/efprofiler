﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Settings.SettingsDialogModel
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.Client.CaliburnInfrastructure;
using HibernatingRhinos.Profiler.Client.Infrastructure;
using log4net;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Windows;

namespace HibernatingRhinos.Profiler.Client.Settings
{
  public class SettingsDialogModel : ProfilerScreen
  {
    private readonly ILog logger;
    private readonly ValidatableProperty<int> maxRepeatedStatementsForSelectNPlusOneProperty;
    private readonly ValidatableProperty<int> listenPortProperty;
    private readonly ValidatableProperty<int> maxRowCountForExcessiveRowCountSuggestionProperty;
    private readonly ValidatableProperty<int> maxRowCountForExcessiveRowCountWarningProperty;
    private readonly ValidatableProperty<int> maxRepeatedStatementsForUsingStatementBatchingProperty;
    private readonly ValidatableProperty<int> maxNumberOfStatementsPerSessionProperty;
    private readonly ValidatableProperty<int> maxNumberOfJoinsPerStatementProperty;
    private readonly ValidatableProperty<int> maxNumberOfTablesPerStatementProperty;
    private readonly ValidatableProperty<int> maxNumberOfNestingSelectStatementProperty;
    private readonly ValidatableProperty<int> maxNumberOfWhereClausePerStatementProperty;
    private readonly ValidatableProperty<int> maxNumberOfExpressionsPerWhereClauseProperty;
    private bool autoUpdate;
    private bool avoidTrackingFeaturesUsage;
    private string tempFolderPath;
    private string namespaceToAdd;
    private string defaultEditor;
    private string editorPath;
    private readonly ObservableCollection<string> namespaces;
    private readonly ObservableCollection<string> availableEditors;
    private readonly ObservableCollection<string> disabledAlerts;
    private readonly ObservableCollection<string> alertsToDisable;
    private readonly ObservableCollection<IgnoredAlertInfo> ignoredAlerts;
    private readonly IEnumerable<string> availableAlerts;
    private readonly ValidatablePropertyCollection validatableProperties;

    public SettingsDialogModel()
    {
      ValidatableProperty<int> validatableProperty1 = new ValidatableProperty<int>("Max Repeated Statements");
      validatableProperty1.Validators.Add((ValidationAttribute) new RangeAttribute(1, int.MaxValue));
      this.maxRepeatedStatementsForSelectNPlusOneProperty = validatableProperty1;
      ValidatableProperty<int> validatableProperty2 = new ValidatableProperty<int>("Listen Port");
      validatableProperty2.Validators.Add((ValidationAttribute) new RangeAttribute(1, (int) short.MaxValue));
      this.listenPortProperty = validatableProperty2;
      ValidatableProperty<int> validatableProperty3 = new ValidatableProperty<int>("Max Row Count For Excessive Row Count Suggestion");
      validatableProperty3.Validators.Add((ValidationAttribute) new RangeAttribute(1, int.MaxValue));
      this.maxRowCountForExcessiveRowCountSuggestionProperty = validatableProperty3;
      ValidatableProperty<int> validatableProperty4 = new ValidatableProperty<int>("Max Row Count For Excessive Row Count Warning");
      validatableProperty4.Validators.Add((ValidationAttribute) new RangeAttribute(1, int.MaxValue));
      this.maxRowCountForExcessiveRowCountWarningProperty = validatableProperty4;
      ValidatableProperty<int> validatableProperty5 = new ValidatableProperty<int>("Max Repeated Statements For Using Statement Batching");
      validatableProperty5.Validators.Add((ValidationAttribute) new RangeAttribute(1, int.MaxValue));
      this.maxRepeatedStatementsForUsingStatementBatchingProperty = validatableProperty5;
      ValidatableProperty<int> validatableProperty6 = new ValidatableProperty<int>("Max Number Of Statements Per Session");
      validatableProperty6.Validators.Add((ValidationAttribute) new RangeAttribute(1, int.MaxValue));
      this.maxNumberOfStatementsPerSessionProperty = validatableProperty6;
      ValidatableProperty<int> validatableProperty7 = new ValidatableProperty<int>("Max Number Of Joins Per Statement");
      validatableProperty7.Validators.Add((ValidationAttribute) new RangeAttribute(1, int.MaxValue));
      this.maxNumberOfJoinsPerStatementProperty = validatableProperty7;
      ValidatableProperty<int> validatableProperty8 = new ValidatableProperty<int>("Max Number Of Tables Per Statement");
      validatableProperty8.Validators.Add((ValidationAttribute) new RangeAttribute(1, int.MaxValue));
      this.maxNumberOfTablesPerStatementProperty = validatableProperty8;
      ValidatableProperty<int> validatableProperty9 = new ValidatableProperty<int>("Max Number Of Nesting Select Statements Per Statement");
      validatableProperty9.Validators.Add((ValidationAttribute) new RangeAttribute(1, int.MaxValue));
      this.maxNumberOfNestingSelectStatementProperty = validatableProperty9;
      ValidatableProperty<int> validatableProperty10 = new ValidatableProperty<int>("Max Number Of Where Clauses Per Statement");
      validatableProperty10.Validators.Add((ValidationAttribute) new RangeAttribute(1, int.MaxValue));
      this.maxNumberOfWhereClausePerStatementProperty = validatableProperty10;
      ValidatableProperty<int> validatableProperty11 = new ValidatableProperty<int>("Max Number Of Expressions Per Where Clause");
      validatableProperty11.Validators.Add((ValidationAttribute) new RangeAttribute(1, int.MaxValue));
      this.maxNumberOfExpressionsPerWhereClauseProperty = validatableProperty11;
      this.namespaces = new ObservableCollection<string>();
      this.availableEditors = new ObservableCollection<string>();
      this.disabledAlerts = new ObservableCollection<string>();
      this.alertsToDisable = new ObservableCollection<string>();
      this.ignoredAlerts = new ObservableCollection<IgnoredAlertInfo>();
      // ISSUE: explicit constructor call
      //base.\u002Ector();
      this.DisplayName = "Settings";
      this.availableAlerts = AbstractStatementProcessor.AllAlerts;
      this.ReadProperties(UserPreferencesHolder.UserSettings);
      this.SynchronizeDisabledAlerts();
      ValidatablePropertyCollection propertyCollection = new ValidatablePropertyCollection();
      propertyCollection.Add((IValidatableProperty) this.maxRepeatedStatementsForSelectNPlusOneProperty);
      propertyCollection.Add((IValidatableProperty) this.listenPortProperty);
      propertyCollection.Add((IValidatableProperty) this.maxNumberOfJoinsPerStatementProperty);
      propertyCollection.Add((IValidatableProperty) this.maxNumberOfTablesPerStatementProperty);
      propertyCollection.Add((IValidatableProperty) this.maxNumberOfNestingSelectStatementProperty);
      propertyCollection.Add((IValidatableProperty) this.maxNumberOfWhereClausePerStatementProperty);
      propertyCollection.Add((IValidatableProperty) this.maxNumberOfExpressionsPerWhereClauseProperty);
      propertyCollection.Add((IValidatableProperty) this.maxNumberOfStatementsPerSessionProperty);
      propertyCollection.Add((IValidatableProperty) this.maxRepeatedStatementsForUsingStatementBatchingProperty);
      propertyCollection.Add((IValidatableProperty) this.maxRowCountForExcessiveRowCountSuggestionProperty);
      propertyCollection.Add((IValidatableProperty) this.maxRowCountForExcessiveRowCountWarningProperty);
      this.validatableProperties = propertyCollection;
    }

    public bool AutoUpdate
    {
      get
      {
        return this.autoUpdate;
      }
      set
      {
        this.autoUpdate = value;
        this.NotifyOfPropertyChange<bool>((System.Linq.Expressions.Expression<Func<bool>>) (() => this.AutoUpdate));
      }
    }

    public bool AvoidTrackingFeaturesUsage
    {
      get
      {
        return this.avoidTrackingFeaturesUsage;
      }
      set
      {
        this.avoidTrackingFeaturesUsage = value;
        this.NotifyOfPropertyChange<bool>((System.Linq.Expressions.Expression<Func<bool>>) (() => this.AvoidTrackingFeaturesUsage));
      }
    }

    public ValidatableProperty<int> ListenPort
    {
      get
      {
        return this.listenPortProperty;
      }
    }

    public ValidatableProperty<int> MaxRepeatedStatementsForSelectNPlusOne
    {
      get
      {
        return this.maxRepeatedStatementsForSelectNPlusOneProperty;
      }
    }

    public ValidatableProperty<int> MaxRowCountForExcessiveRowCountSuggestion
    {
      get
      {
        return this.maxRowCountForExcessiveRowCountSuggestionProperty;
      }
    }

    public ValidatableProperty<int> MaxRowCountForExcessiveRowCountWarning
    {
      get
      {
        return this.maxRowCountForExcessiveRowCountWarningProperty;
      }
    }

    public ValidatableProperty<int> MaxRepeatedStatementsForUsingStatementBatching
    {
      get
      {
        return this.maxRepeatedStatementsForUsingStatementBatchingProperty;
      }
    }

    public ValidatableProperty<int> MaxNumberOfStatementsPerSession
    {
      get
      {
        return this.maxNumberOfStatementsPerSessionProperty;
      }
    }

    public string TempFolderPath
    {
      get
      {
        return this.tempFolderPath;
      }
      set
      {
        this.tempFolderPath = value;
        this.NotifyOfPropertyChange<string>((System.Linq.Expressions.Expression<Func<string>>) (() => this.TempFolderPath));
      }
    }

    public string DefaultEditor
    {
      get
      {
        return this.defaultEditor;
      }
      set
      {
        this.defaultEditor = value;
        this.NotifyOfPropertyChange<string>((System.Linq.Expressions.Expression<Func<string>>) (() => this.DefaultEditor));
      }
    }

    public string EditorPath
    {
      get
      {
        return this.editorPath;
      }
      set
      {
        this.editorPath = value;
        this.NotifyOfPropertyChange<string>((System.Linq.Expressions.Expression<Func<string>>) (() => this.EditorPath));
      }
    }

    public ValidatableProperty<int> MaxNumberOfJoinsPerStatement
    {
      get
      {
        return this.maxNumberOfJoinsPerStatementProperty;
      }
    }

    public ValidatableProperty<int> MaxNumberOfTablesPerStatement
    {
      get
      {
        return this.maxNumberOfTablesPerStatementProperty;
      }
    }

    public ValidatableProperty<int> MaxNumberOfNestingSelectStatement
    {
      get
      {
        return this.maxNumberOfNestingSelectStatementProperty;
      }
    }

    public ValidatableProperty<int> MaxNumberOfWhereClausePerStatement
    {
      get
      {
        return this.maxNumberOfWhereClausePerStatementProperty;
      }
    }

    public ValidatableProperty<int> MaxNumberOfExpressionsPerWhereClause
    {
      get
      {
        return this.maxNumberOfExpressionsPerWhereClauseProperty;
      }
    }

    public string NamespaceToAdd
    {
      get
      {
        return this.namespaceToAdd;
      }
      set
      {
        this.namespaceToAdd = value;
        this.NotifyOfPropertyChange<string>((System.Linq.Expressions.Expression<Func<string>>) (() => this.NamespaceToAdd));
      }
    }

    public ObservableCollection<string> Namespaces
    {
      get
      {
        return this.namespaces;
      }
    }

    public ObservableCollection<string> AvailableEditors
    {
      get
      {
        return this.availableEditors;
      }
    }

    public ObservableCollection<string> DisabledAlerts
    {
      get
      {
        return this.disabledAlerts;
      }
    }

    public ObservableCollection<string> AlertsToDisable
    {
      get
      {
        return this.alertsToDisable;
      }
    }

    public ObservableCollection<IgnoredAlertInfo> IgnoredAlerts
    {
      get
      {
        return this.ignoredAlerts;
      }
    }

    public void Ok()
    {
      if (!this.validatableProperties.IsValid)
      {
        this.validatableProperties.MarkAllAsDirty();
      }
      else
      {
        if (!this.WriteProperties())
          return;
        this.TryClose();
      }
    }

    public void Reset()
    {
      this.ReadProperties(new UserPreferences(true));
    }

    public bool CanAddNamespace(string namespaceToAdd)
    {
      if (!string.IsNullOrEmpty(namespaceToAdd))
        return !this.namespaces.Contains(namespaceToAdd);
      return false;
    }

    public void AddNamespace(string namespaceToAdd)
    {
      this.namespaces.Add(namespaceToAdd);
      this.NamespaceToAdd = string.Empty;
    }

    public void RemoveNamespace(string namespaceToRemove)
    {
      this.namespaces.Remove(namespaceToRemove);
    }

    public bool CanDisableAlert(string alert)
    {
      return !string.IsNullOrEmpty(alert);
    }

    public void DisableAlert(string alert)
    {
      this.disabledAlerts.Add(alert);
      this.SynchronizeDisabledAlerts();
    }

    public void EnableAlert(string alert)
    {
      this.disabledAlerts.Remove(alert);
      this.SynchronizeDisabledAlerts();
    }

    public void RemoveIgnoredAlert(IgnoredAlertInfo info)
    {
      this.ignoredAlerts.Remove(info);
    }

    private void SynchronizeDisabledAlerts()
    {
      this.alertsToDisable.Clear();
      foreach (string str in this.availableAlerts.Except<string>((IEnumerable<string>) this.disabledAlerts))
      {
        if (!this.disabledAlerts.Contains(str))
          this.alertsToDisable.Add(str);
      }
    }

    private void ReadProperties(UserPreferences userSettings)
    {
      this.ListenPort.InitialValue = (object) userSettings.ListenPort;
      this.AutoUpdate = userSettings.AutoUpdate;
      this.AvoidTrackingFeaturesUsage = userSettings.AvoidTrackingFeaturesUsage;
      this.MaxRepeatedStatementsForSelectNPlusOne.InitialValue = (object) userSettings.Configuration.MaxRepeatedStatementsForSelectNPlusOne;
      this.MaxRowCountForExcessiveRowCountSuggestion.InitialValue = (object) userSettings.Configuration.MaxRowCountForExcessiveRowCountSuggestion;
      this.MaxRowCountForExcessiveRowCountWarning.InitialValue = (object) userSettings.Configuration.MaxRowCountForExcessiveRowCountWarning;
      this.MaxRepeatedStatementsForUsingStatementBatching.InitialValue = (object) userSettings.Configuration.MaxRepeatedStatementsForUsingStatementBatching;
      this.MaxNumberOfStatementsPerSession.InitialValue = (object) userSettings.Configuration.MaxNumberOfStatementsPerSession;
      this.MaxNumberOfJoinsPerStatement.InitialValue = (object) userSettings.Configuration.MaxNumberOfJoinsPerStatement;
      this.MaxNumberOfTablesPerStatement.InitialValue = (object) userSettings.Configuration.MaxNumberOfTablesPerStatement;
      this.MaxNumberOfNestingSelectStatement.InitialValue = (object) userSettings.Configuration.MaxNumberOfNestingSelectStatement;
      this.MaxNumberOfWhereClausePerStatement.InitialValue = (object) userSettings.Configuration.MaxNumberOfWhereClausePerStatement;
      this.MaxNumberOfExpressionsPerWhereClause.InitialValue = (object) userSettings.Configuration.MaxNumberOfExpressionsPerWhereClause;
      this.TempFolderPath = userSettings.Configuration.TempFolderPath;
      this.DefaultEditor = userSettings.Configuration.DefaultEditor;
      this.EditorPath = userSettings.Configuration.EditorPath;
      foreach (string infrastructureNamespace in userSettings.Configuration.InfrastructureNamespaces)
        this.namespaces.Add(infrastructureNamespace);
      foreach (string availableEditor in userSettings.Configuration.AvailableEditors)
        this.availableEditors.Add(availableEditor);
      foreach (string disabledAlert in userSettings.Configuration.DisabledAlerts)
        this.disabledAlerts.Add(disabledAlert);
      foreach (IgnoredAlertInfo alertsOnStatement in (IEnumerable<IgnoredAlertInfo>) userSettings.Configuration.IgnoredAlertsOnStatements)
        this.ignoredAlerts.Add(alertsOnStatement);
    }

    private bool CanPortBeUsed(int port)
    {
      try
      {
        TcpListener tcpListener = new TcpListener(IPAddress.Any, port);
        tcpListener.Start();
        tcpListener.Stop();
        return true;
      }
      catch (SocketException ex)
      {
        if (ex.ErrorCode == 10048)
          return false;
      }
      catch
      {
      }
      return true;
    }

    private bool WriteProperties()
    {
      bool flag1 = UserPreferencesHolder.UserSettings.ListenPort != this.ListenPort.ValidatedValue;
      if (!this.CanPortBeUsed(this.ListenPort.ValidatedValue) && flag1)
      {
        int num = (int) MessageBox.Show(string.Format("You can not change the profiler's listening port to {0} because only one usage of each socket address is allowed.", (object) this.ListenPort.ValidatedValue), "Port in use");
        return false;
      }
      int listenPort = UserPreferencesHolder.UserSettings.ListenPort;
      UserPreferencesHolder.UserSettings.ListenPort = this.ListenPort.ValidatedValue;
      UserPreferencesHolder.UserSettings.AutoUpdate = this.AutoUpdate;
      UserPreferencesHolder.UserSettings.AvoidTrackingFeaturesUsage = this.AvoidTrackingFeaturesUsage;
      UserPreferencesHolder.UserSettings.Configuration.MaxRepeatedStatementsForSelectNPlusOne = this.MaxRepeatedStatementsForSelectNPlusOne.ValidatedValue;
      UserPreferencesHolder.UserSettings.Configuration.MaxRowCountForExcessiveRowCountSuggestion = this.MaxRowCountForExcessiveRowCountSuggestion.ValidatedValue;
      UserPreferencesHolder.UserSettings.Configuration.MaxRowCountForExcessiveRowCountWarning = this.MaxRowCountForExcessiveRowCountWarning.ValidatedValue;
      UserPreferencesHolder.UserSettings.Configuration.MaxRepeatedStatementsForUsingStatementBatching = this.MaxRepeatedStatementsForUsingStatementBatching.ValidatedValue;
      UserPreferencesHolder.UserSettings.Configuration.MaxNumberOfStatementsPerSession = this.MaxNumberOfStatementsPerSession.ValidatedValue;
      UserPreferencesHolder.UserSettings.Configuration.MaxNumberOfJoinsPerStatement = this.MaxNumberOfJoinsPerStatement.ValidatedValue;
      UserPreferencesHolder.UserSettings.Configuration.MaxNumberOfTablesPerStatement = this.MaxNumberOfTablesPerStatement.ValidatedValue;
      UserPreferencesHolder.UserSettings.Configuration.MaxNumberOfNestingSelectStatement = this.MaxNumberOfNestingSelectStatement.ValidatedValue;
      UserPreferencesHolder.UserSettings.Configuration.MaxNumberOfWhereClausePerStatement = this.MaxNumberOfWhereClausePerStatement.ValidatedValue;
      UserPreferencesHolder.UserSettings.Configuration.MaxNumberOfExpressionsPerWhereClause = this.MaxNumberOfExpressionsPerWhereClause.ValidatedValue;
      bool flag2 = UserPreferencesHolder.UserSettings.Configuration.TempFolderPath != this.TempFolderPath;
      string tempFolderPath = UserPreferencesHolder.UserSettings.Configuration.TempFolderPath;
      UserPreferencesHolder.UserSettings.Configuration.TempFolderPath = this.TempFolderPath;
      UserPreferencesHolder.UserSettings.Configuration.DefaultEditor = this.DefaultEditor;
      UserPreferencesHolder.UserSettings.Configuration.EditorPath = this.EditorPath;
      UserPreferencesHolder.UserSettings.Configuration.InfrastructureNamespaces.Clear();
      foreach (string str in (Collection<string>) this.namespaces)
        UserPreferencesHolder.UserSettings.Configuration.InfrastructureNamespaces.Add(str);
      UserPreferencesHolder.UserSettings.Configuration.DisabledAlerts.Clear();
      foreach (string disabledAlert in (Collection<string>) this.disabledAlerts)
        UserPreferencesHolder.UserSettings.Configuration.DisabledAlerts.Add(disabledAlert);
      UserPreferencesHolder.UserSettings.Configuration.IgnoredAlertsOnStatements.Clear();
      foreach (IgnoredAlertInfo ignoredAlert in (Collection<IgnoredAlertInfo>) this.ignoredAlerts)
        UserPreferencesHolder.UserSettings.Configuration.IgnoredAlertsOnStatements.Add(ignoredAlert);
      UserPreferencesHolder.Save();
      if (flag1)
        this.PropmpToRestart(string.Format("You chose to change the profiler's listening port from {0} to {1}. Restarting the profiler is required in order to continue.", (object) listenPort, (object) this.ListenPort.ValidatedValue));
      if (flag2)
        this.PropmpToRestart(string.Format("You chose to change the profiler's temp folder path from '{0}' to '{1}'. Restarting the profiler is required in order to continue.", (object) tempFolderPath, (object) this.TempFolderPath));
      return true;
    }

    private void PropmpToRestart(string messsage)
    {
      if (MessageBox.Show(messsage, "Restart the profiler", MessageBoxButton.OK) != MessageBoxResult.OK)
        return;
      this.logger.Info((object) messsage);
      Process.Start(Application.ResourceAssembly.Location);
      Application.Current.Shutdown();
    }
  }
}
