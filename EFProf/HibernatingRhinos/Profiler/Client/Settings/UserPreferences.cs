﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Settings.UserPreferences
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.Profiles;
using HibernatingRhinos.Profiler.Client.Connections;
using HibernatingRhinos.Profiler.Client.Filtering;
using HibernatingRhinos.Profiler.Client.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace HibernatingRhinos.Profiler.Client.Settings
{
  [Serializable]
  public class UserPreferences
  {
    private ProfilerConfiguration configuration;
    private List<PersistentRemoteConnection> remoteConnections;
    private bool autoUpdate;
    private bool avoidTrackingFeaturesUsage;

    public UserPreferences()
    {
      this.Filters = new List<IFilter>();
      this.ConnectionInfo = new List<HibernatingRhinos.Profiler.BackEnd.Infrastructure.ConnectionInfo>();
      this.UserId = Guid.NewGuid();
      this.IsStatementDetailsPaneShownInCompactView = true;
      this.IsCompactViewAlwaysOnTop = true;
      this.IsTrackLatestStatements = false;
      this.IsDetectConnectionString = true;
    }

    public UserPreferences(bool initializeDefaults)
      : this()
    {
      if (!initializeDefaults)
        return;
      this.ResetSettings();
    }

    public void ResetSettings()
    {
      this.ListenPort = Profile.Current.DefaultPort;
      this.AutoUpdate = true;
      this.AvoidTrackingFeaturesUsage = false;
      this.Configuration.TempFolderPath = (string) null;
      this.IsCompactViewAlwaysOnTop = true;
      this.IsStatementDetailsPaneShownInCompactView = true;
      this.IsTrackLatestStatements = false;
      this.IsDetectConnectionString = true;
    }

    public int ListenPort { get; set; }

    public ProfilerConfiguration Configuration
    {
      get
      {
        return this.configuration ?? (this.configuration = new ProfilerConfiguration());
      }
      set
      {
        this.configuration = value;
      }
    }

    public List<HibernatingRhinos.Profiler.BackEnd.Infrastructure.ConnectionInfo> ConnectionInfo { get; set; }

    public List<PersistentRemoteConnection> RemoteConnections
    {
      get
      {
        return this.remoteConnections ?? (this.remoteConnections = new List<PersistentRemoteConnection>());
      }
      set
      {
        this.remoteConnections = value;
      }
    }

    public bool AutoUpdate
    {
      get
      {
        Assembly assembly = typeof (UserPreferences).Assembly;
        byte[] numArray = Convert.FromBase64String("ACQAAASAAACUAAAABgIAAAAkAABSU0ExAAQAAAEAAQDBLTP+od21ZEnX3WXgZVRX1adQQHusYawlMrB4Mnz5vH3GcynLb4CvG7zwjJiYsit/xgN28VmeeQJ5PSdcXH1aB54Qnm4TMa+HcRFxcnGLQQfzwKa/rQIufkQ+Du1hERNZRdERss/wpbnyF2mcNsmGDa09Y+x0264LLK/sK4SIog==");
        byte[] publicKey = assembly.GetName().GetPublicKey();
        for (int index = 0; index < publicKey.Length; ++index)
        {
          if ((int) publicKey[index] != (int) numArray[index])
            return true;
        }
        bool pfWasVerified = false;
        if (!HostProgram.StrongNameSignatureVerificationEx(assembly.Location, true, ref pfWasVerified))
          return true;
        return this.autoUpdate;
      }
      set
      {
        this.autoUpdate = value;
      }
    }

    public bool AvoidTrackingFeaturesUsage
    {
      get
      {
        Assembly assembly = typeof (UserPreferences).Assembly;
        byte[] numArray = Convert.FromBase64String("ACQAAASAAACUAAAABgIAAAAkAABSU0ExAAQAAAEAAQDBLTP+od21ZEnX3WXgZVRX1adQQHusYawlMrB4Mnz5vH3GcynLb4CvG7zwjJiYsit/xgN28VmeeQJ5PSdcXH1aB54Qnm4TMa+HcRFxcnGLQQfzwKa/rQIufkQ+Du1hERNZRdERss/wpbnyF2mcNsmGDa09Y+x0264LLK/sK4SIog==");
        byte[] publicKey = assembly.GetName().GetPublicKey();
        for (int index = 0; index < publicKey.Length; ++index)
        {
          if ((int) publicKey[index] != (int) numArray[index])
            return false;
        }
        bool pfWasVerified = false;
        if (!HostProgram.StrongNameSignatureVerificationEx(assembly.Location, true, ref pfWasVerified))
          return false;
        return this.avoidTrackingFeaturesUsage;
      }
      set
      {
        this.avoidTrackingFeaturesUsage = value;
      }
    }

    public Guid UserId { get; set; }

    public List<IFilter> Filters { get; set; }

    public string EmailAddress { get; set; }

    public HibernatingRhinos.Profiler.BackEnd.Infrastructure.ConnectionInfo GetCurrentConnection()
    {
      return this.ConnectionInfo.FirstOrDefault<HibernatingRhinos.Profiler.BackEnd.Infrastructure.ConnectionInfo>((Func<HibernatingRhinos.Profiler.BackEnd.Infrastructure.ConnectionInfo, bool>) (info => info.IsSelected));
    }

    public bool IsCompactViewAlwaysOnTop { get; set; }

    public bool IsStatementDetailsPaneShownInCompactView { get; set; }

    public bool IsTrackLatestStatements { get; set; }

    public bool IsDetectConnectionString { get; set; }

    public void EnsureIsValid()
    {
      this.Configuration.EnsureIsValid();
      if (this.Filters != null)
        return;
      this.Filters = new List<IFilter>();
    }

    public static UserPreferences GetDefault()
    {
      return new UserPreferences(true);
    }
  }
}
