﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.BuildInfo
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using log4net;
using System;
using System.IO;

namespace HibernatingRhinos.Profiler.Client
{
  public class BuildInfo
  {
    private static readonly ILog logger = LogManager.GetLogger(typeof (BuildInfo));

    public static string GetCurrentBuild()
    {
      try
      {
        using (Stream manifestResourceStream = typeof (BuildInfo).Assembly.GetManifestResourceStream(typeof (BuildInfo).Namespace + ".Build.Info"))
          return new StreamReader(manifestResourceStream).ReadToEnd().Trim();
      }
      catch (Exception ex)
      {
        BuildInfo.logger.Error((object) "Could not get build information", ex);
        return "INVALID Build!!!";
      }
    }
  }
}
