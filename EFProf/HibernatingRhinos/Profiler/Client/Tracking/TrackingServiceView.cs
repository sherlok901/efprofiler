﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Tracking.TrackingServiceView
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.BackEnd.Profiles;
using log4net;
using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using System.Windows.Navigation;
using System.Windows.Threading;

namespace HibernatingRhinos.Profiler.Client.Tracking
{
  public partial class TrackingServiceView : Window, ITrackingService, IDisposable, IComponentConnector
  {
    private readonly DispatcherTimer timer = new DispatcherTimer(DispatcherPriority.Background)
    {
      Interval = TimeSpan.FromMilliseconds(250.0)
    };
    private readonly LinkedList<TrackingServiceView.TrackingData> tracking = new LinkedList<TrackingServiceView.TrackingData>();
    private readonly ILog logger = LogManager.GetLogger(typeof (TrackingServiceView));
    private Action<string> script;
    private bool started;
    //internal WebBrowser Browser;
    //private bool _contentLoaded;

    public TrackingServiceView()
    {
      this.InitializeComponent();
      this.Browser.LoadCompleted += new LoadCompletedEventHandler(this.BrowserOnLoadCompleted);
      Uri source = new Uri("http://" + Profile.Current.ShortName + ".uberprof.com");
      try
      {
        this.Browser.Navigate(source);
        this.timer.Tick += new EventHandler(this.TimerOnTick);
        this.timer.Start();
      }
      catch (Exception ex)
      {
        this.logger.Error((object) ("Could not navigate to " + (object) source), ex);
      }
    }

    private void TimerOnTick(object sender, EventArgs eventArgs)
    {
      if (this.script == null)
        return;
      try
      {
        TrackingServiceView.TrackingData trackingData;
        lock (this.tracking)
        {
          if (this.tracking.Count == 0)
            return;
          trackingData = this.tracking.First.Value;
          this.tracking.RemoveFirst();
        }
        if (trackingData.Value.HasValue)
          this.script("pageTracker._trackEvent('" + trackingData.Category + "','" + trackingData.Action + "','" + trackingData.Label + "'," + (object) trackingData.Value + ");");
        else if (trackingData.Label != null)
          this.script("pageTracker._trackEvent('" + trackingData.Category + "','" + trackingData.Action + "','" + trackingData.Label + "');");
        else
          this.script("pageTracker._trackEvent('" + trackingData.Category + "','" + trackingData.Action + "');");
      }
      catch (Exception ex)
      {
        this.logger.Error((object) "Tracking service tick failed", ex);
      }
    }

    private void BrowserOnLoadCompleted(object sender, NavigationEventArgs e)
    {
      object document = this.Browser.Document;
      object target = ((IEnumerable) TrackingServiceView.Invoke(document, "getElementsByTagName", (object) "head")).Cast<object>().First<object>();
      object scriptEl = TrackingServiceView.Invoke(document, "createElement", (object) "script");
      TrackingServiceView.Invoke(target, "appendChild", scriptEl);
      this.script = (Action<string>) (s => TrackingServiceView.Set(scriptEl, "text", (object) ("try \r\n{ \r\n" + s + "\r\n}\r\ncatch(err)\r\n{\r\n // ignore\r\n}")));
    }

    private static void Set(object target, string name, object value)
    {
      target.GetType().InvokeMember(name, BindingFlags.SetProperty, (Binder) null, target, new object[1]
      {
        value
      });
    }

    private static object Invoke(object target, string method, params object[] args)
    {
      return target.GetType().InvokeMember(method, BindingFlags.InvokeMethod, (Binder) null, target, args);
    }

    public void Dispose()
    {
      if (!this.started)
        return;
      this.started = false;
      this.timer.Stop();
      this.Close();
    }

    public void Track(string category, string action)
    {
      this.Track(category, action, (string) null, new int?());
    }

    public void Track(string category, string action, string label)
    {
      this.Track(category, action, label, new int?());
    }

    public void Track(string category, string action, string label, int? value)
    {
      lock (this.tracking)
        this.tracking.AddLast(new TrackingServiceView.TrackingData()
        {
          Action = action,
          Category = category,
          Label = label,
          Value = value
        });
    }

    public void Initialize()
    {
      this.started = true;
      this.Show();
    }

    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //public void InitializeComponent()
    //{
    //  if (this._contentLoaded)
    //    return;
    //  this._contentLoaded = true;
    //  Application.LoadComponent((object) this, new Uri("/EFProf;component/tracking/trackingserviceview.xaml", UriKind.Relative));
    //}

    //[EditorBrowsable(EditorBrowsableState.Never)]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //void IComponentConnector.Connect(int connectionId, object target)
    //{
    //  if (connectionId == 1)
    //    this.Browser = (WebBrowser) target;
    //  else
    //    this._contentLoaded = true;
    //}

    private class TrackingData
    {
      public string Category;
      public string Action;
      public string Label;
      public int? Value;
    }
  }
}
