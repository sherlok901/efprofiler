﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Tracking.ITrackingService
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System;

namespace HibernatingRhinos.Profiler.Client.Tracking
{
  public interface ITrackingService : IDisposable
  {
    void Initialize();

    void Track(string category, string action, string label = null, int? value = null);
  }
}
