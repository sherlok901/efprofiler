﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Exports.ExportConsts
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

namespace HibernatingRhinos.Profiler.Client.Exports
{
  public class ExportConsts
  {
    public const string ReportCss = "/*{{report-css}}*/";
    public const string ReportName = "{{report-name}}";
    public const string ReportTitle = "{{report-title}}";
    public const string ReportDate = "{{report-date}}";
    public const string ReportRootUrl = "{{report-root-url}}";
    public const string ReportsAsJson = "{{reports-as-json}}";
  }
}
