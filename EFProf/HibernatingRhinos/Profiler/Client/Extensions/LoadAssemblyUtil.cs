﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Extensions.LoadAssemblyUtil
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using log4net;
using System;
using System.Reflection;

namespace HibernatingRhinos.Profiler.Client.Extensions
{
  public static class LoadAssemblyUtil
  {
    private static readonly ILog Log = LogManager.GetLogger(typeof (LoadAssemblyUtil));

    public static void VerifyThatAssemblyCanBeLoaded(string fullPathToAssembly, Action<Exception> exceptionCallback)
    {
      try
      {
        Assembly.LoadFrom(fullPathToAssembly);
      }
      catch (BadImageFormatException ex)
      {
        InvalidOperationException operationException = new InvalidOperationException(string.Format("It seems like you're trying to load the x86 version of '{0}' assembly.", (object) fullPathToAssembly) + Environment.NewLine + "Please try to load the x64 version of this assembly.", (Exception) ex);
        LoadAssemblyUtil.Log.Error((object) operationException);
        exceptionCallback((Exception) operationException);
      }
      catch (Exception ex)
      {
        LoadAssemblyUtil.Log.Error((object) ex);
        exceptionCallback(ex);
      }
    }
  }
}
