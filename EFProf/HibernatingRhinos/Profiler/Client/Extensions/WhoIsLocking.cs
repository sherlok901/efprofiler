﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Extensions.WhoIsLocking
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace HibernatingRhinos.Profiler.Client.Extensions
{
  public class WhoIsLocking
  {
    private const int RmRebootReasonNone = 0;
    private const int CCH_RM_MAX_APP_NAME = 255;
    private const int CCH_RM_MAX_SVC_NAME = 63;

    public static IList<Process> GetProcessesUsingFile(string filePath)
    {
      List<Process> processList = new List<Process>();
      uint pSessionHandle;
      if (WhoIsLocking.RmStartSession(out pSessionHandle, 0, Guid.NewGuid().ToString()) != 0)
        throw new Win32Exception();
      try
      {
        string[] rgsFilenames = new string[1]{ filePath };
        if (WhoIsLocking.RmRegisterResources(pSessionHandle, (uint) rgsFilenames.Length, rgsFilenames, 0U, (WhoIsLocking.RM_UNIQUE_PROCESS[]) null, 0U, (string[]) null) != 0)
          throw new Win32Exception();
        uint pnProcInfoNeeded = 0;
        uint pnProcInfo = 0;
        uint lpdwRebootReasons = 0;
        switch (WhoIsLocking.RmGetList(pSessionHandle, out pnProcInfoNeeded, ref pnProcInfo, (WhoIsLocking.RM_PROCESS_INFO[]) null, ref lpdwRebootReasons))
        {
          case 0:
            break;
          case 234:
            WhoIsLocking.RM_PROCESS_INFO[] rgAffectedApps = new WhoIsLocking.RM_PROCESS_INFO[(int) (IntPtr) pnProcInfoNeeded];
            uint length = (uint) rgAffectedApps.Length;
            if (WhoIsLocking.RmGetList(pSessionHandle, out pnProcInfoNeeded, ref length, rgAffectedApps, ref lpdwRebootReasons) != 0)
              throw new Win32Exception();
            for (int index = 0; (long) index < (long) length; ++index)
            {
              try
              {
                processList.Add(Process.GetProcessById(rgAffectedApps[index].Process.dwProcessId));
              }
              catch (ArgumentException ex)
              {
              }
            }
            break;
          default:
            throw new Win32Exception();
        }
      }
      finally
      {
        WhoIsLocking.RmEndSession(pSessionHandle);
      }
      return (IList<Process>) processList;
    }

    [DllImport("rstrtmgr.dll", CharSet = CharSet.Unicode)]
    private static extern int RmStartSession(out uint pSessionHandle, int dwSessionFlags, string strSessionKey);

    [DllImport("rstrtmgr.dll")]
    private static extern int RmEndSession(uint pSessionHandle);

    [DllImport("rstrtmgr.dll", CharSet = CharSet.Unicode)]
    private static extern int RmRegisterResources(uint pSessionHandle, uint nFiles, string[] rgsFilenames, uint nApplications, [In] WhoIsLocking.RM_UNIQUE_PROCESS[] rgApplications, uint nServices, string[] rgsServiceNames);

    [DllImport("rstrtmgr.dll")]
    private static extern int RmGetList(uint dwSessionHandle, out uint pnProcInfoNeeded, ref uint pnProcInfo, [In, Out] WhoIsLocking.RM_PROCESS_INFO[] rgAffectedApps, ref uint lpdwRebootReasons);

    private enum RM_APP_TYPE
    {
      RmUnknownApp = 0,
      RmMainWindow = 1,
      RmOtherWindow = 2,
      RmService = 3,
      RmExplorer = 4,
      RmConsole = 5,
      RmCritical = 1000, // 0x000003E8
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
    private struct RM_PROCESS_INFO
    {
      public WhoIsLocking.RM_UNIQUE_PROCESS Process;
      [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
      public readonly string strAppName;
      [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
      public readonly string strServiceShortName;
      public readonly WhoIsLocking.RM_APP_TYPE ApplicationType;
      public readonly uint AppStatus;
      public readonly uint TSSessionId;
      [MarshalAs(UnmanagedType.Bool)]
      public readonly bool bRestartable;
    }

    private struct RM_UNIQUE_PROCESS
    {
      public readonly int dwProcessId;
      public readonly System.Runtime.InteropServices.ComTypes.FILETIME ProcessStartTime;
    }
  }
}
