﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Extensions.TaskExtensions
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System;
using System.Threading;
using System.Threading.Tasks;

namespace HibernatingRhinos.Profiler.Client.Extensions
{
  public static class TaskExtensions
  {
    public static async Task<T> WithCancellation<T>(this Task<T> task, CancellationToken cancellationToken)
    {
      TaskCompletionSource<bool> tcs = new TaskCompletionSource<bool>();
      using (cancellationToken.Register((Action<object>) (s => ((TaskCompletionSource<bool>) s).TrySetResult(true)), (object) tcs))
      {
        if ((Task<T>) task != await Task.WhenAny((Task) task, (Task) tcs.Task))
          throw new OperationCanceledException(cancellationToken);
      }
      return await task;
    }
  }
}
