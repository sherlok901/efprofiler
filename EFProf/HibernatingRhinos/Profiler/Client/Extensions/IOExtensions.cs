﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Extensions.IOExtensions
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;

namespace HibernatingRhinos.Profiler.Client.Extensions
{
  public static class IOExtensions
  {
    private const int retries = 10;

    public static void DeleteDirectory(string directory)
    {
      for (int i = 0; i < 10; ++i)
      {
        try
        {
          if (!Directory.Exists(directory))
            break;
          try
          {
            File.SetAttributes(directory, FileAttributes.Normal);
          }
          catch (IOException ex)
          {
          }
          catch (UnauthorizedAccessException ex)
          {
          }
          Directory.Delete(directory, true);
          break;
        }
        catch (IOException ex1)
        {
          try
          {
            foreach (string directory1 in Directory.GetDirectories(directory, "*", SearchOption.AllDirectories))
            {
              try
              {
                File.SetAttributes(directory1, FileAttributes.Normal);
              }
              catch (IOException ex2)
              {
              }
              catch (UnauthorizedAccessException ex2)
              {
              }
            }
          }
          catch (IOException ex2)
          {
          }
          catch (UnauthorizedAccessException ex2)
          {
          }
          IOExtensions.TryHandlingError(directory, i, (Exception) ex1);
        }
        catch (UnauthorizedAccessException ex)
        {
          IOExtensions.TryHandlingError(directory, i, (Exception) ex);
        }
      }
    }

    private static void TryHandlingError(string directory, int i, Exception e)
    {
      if (i == 9)
      {
        foreach (string file in Directory.GetFiles(directory, "*", SearchOption.AllDirectories))
        {
          string fullPath = Path.GetFullPath(file);
          try
          {
            File.Delete(fullPath);
          }
          catch (UnauthorizedAccessException ex)
          {
            IList<Process> processesUsingFile = WhoIsLocking.GetProcessesUsingFile(fullPath);
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("The following processing are locking ").Append(fullPath).AppendLine();
            foreach (Process process in (IEnumerable<Process>) processesUsingFile)
              stringBuilder.Append("\t").Append(process.ProcessName).Append(' ').Append(process.Id).AppendLine();
            throw new IOException(stringBuilder.ToString());
          }
        }
        throw new IOException("Could not delete " + Path.GetFullPath(directory), e);
      }
      GC.Collect();
      GC.WaitForPendingFinalizers();
      Thread.Sleep(100);
    }

    public static string ToFullPath(this string path)
    {
      path = Environment.ExpandEnvironmentVariables(path);
      if (path.StartsWith("~\\") || path.StartsWith("~/"))
        path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, path.Substring(2));
      if (!Path.IsPathRooted(path))
        return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, path);
      return path;
    }

    public static void CopyDirectory(string from, string to)
    {
      try
      {
        IOExtensions.CopyDirectory(new DirectoryInfo(from), new DirectoryInfo(to));
      }
      catch (Exception ex)
      {
        throw new Exception(string.Format("Exception encountered copying directory from {0} to {1}.", (object) from, (object) to), ex);
      }
    }

    private static void CopyDirectory(DirectoryInfo source, DirectoryInfo target)
    {
      if (!target.Exists)
        Directory.CreateDirectory(target.FullName);
      foreach (FileInfo file in source.GetFiles())
        file.CopyTo(Path.Combine(target.ToString(), file.Name), true);
      foreach (DirectoryInfo directory in source.GetDirectories())
      {
        DirectoryInfo subdirectory = target.CreateSubdirectory(directory.Name);
        IOExtensions.CopyDirectory(directory, subdirectory);
      }
    }
  }
}
