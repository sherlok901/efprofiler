﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Infrastructure.CancellableWaitBoxModel
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.BackEnd.Profiles;
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace HibernatingRhinos.Profiler.Client.Infrastructure
{
  public class CancellableWaitBoxModel : ProfilerScreen
  {
    private readonly Action cancel;
    private readonly Action<Action<HibernatingRhinos.Profiler.Client.WaitBox.TaskStatus>> onUpdateMessage;
    private bool alreadyClosed;
    private int progress;
    private string message;
    private string details;
    private CancellationTokenSource cancellationToken;

    public CancellableWaitBoxModel(Action<Action<HibernatingRhinos.Profiler.Client.WaitBox.TaskStatus>> onUpdateMessage, Action cancel)
    {
      this.DisplayName = "Working...";
      this.onUpdateMessage = onUpdateMessage;
      this.cancel = cancel;
      this.Timer = new DispatcherTimer()
      {
        Interval = TimeSpan.FromMilliseconds(250.0)
      };
      this.Timer.Tick += new EventHandler(this.TimerOnTick);
      this.TimerOnTick((object) this.Timer, EventArgs.Empty);
    }

    protected DispatcherTimer Timer { get; set; }

    public int Progress
    {
      get
      {
        return this.progress;
      }
      set
      {
        this.progress = value;
        this.NotifyOfPropertyChange<int>((System.Linq.Expressions.Expression<Func<int>>) (() => this.Progress));
      }
    }

    public string Message
    {
      get
      {
        return this.message;
      }
      set
      {
        this.message = value;
        this.NotifyOfPropertyChange<string>((System.Linq.Expressions.Expression<Func<string>>) (() => this.Message));
      }
    }

    public CancellableWaitBoxModel(Task task, Action<Action<HibernatingRhinos.Profiler.Client.WaitBox.TaskStatus>> onUpdateMessage, CancellationTokenSource token)
    {
      this.DisplayName = "Working...";
      this.onUpdateMessage = onUpdateMessage;
      this.cancellationToken = token;
      task.ContinueWith((Action<Task>) (r => this.TryClose()));
      this.Timer = new DispatcherTimer()
      {
        Interval = TimeSpan.FromMilliseconds(250.0)
      };
      this.Timer.Tick += new EventHandler(this.TimerOnTick);
      this.TimerOnTick((object) this.Timer, EventArgs.Empty);
    }

    public string Details
    {
      get
      {
        return this.details;
      }
      set
      {
        this.details = value;
        this.NotifyOfPropertyChange(nameof (Details));
      }
    }

    public void Cancel()
    {
      if (this.cancel != null)
        this.cancel();
      if (this.cancellationToken != null)
        this.cancellationToken.Cancel();
      this.TryClose();
    }

    private void TimerOnTick(object sender, EventArgs eventArgs)
    {
      this.onUpdateMessage((Action<HibernatingRhinos.Profiler.Client.WaitBox.TaskStatus>) (status =>
      {
        if (!string.IsNullOrEmpty(status.ExceptionMessage))
        {
          int num = (int) MessageBox.Show(status.ExceptionMessage, Profile.CurrentProfileDisplayName, MessageBoxButton.OK);
        }
        if (status.Complete && !this.alreadyClosed)
        {
          this.TryClose();
          this.alreadyClosed = true;
        }
        else
        {
          this.Message = status.MessageText;
          this.Details = status.Details;
          this.Progress = status.CompletedPercent;
        }
      }));
    }

    protected override void OnInitialize()
    {
      this.Timer.Start();
      base.OnInitialize();
    }

    protected override void OnDeactivate(bool close)
    {
      if (close)
        this.Timer.Stop();
      base.OnDeactivate(close);
    }
  }
}
