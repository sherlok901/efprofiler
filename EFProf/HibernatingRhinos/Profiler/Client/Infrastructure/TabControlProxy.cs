﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Infrastructure.TabControlProxy
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System.Windows;
using System.Windows.Controls;

namespace HibernatingRhinos.Profiler.Client.Infrastructure
{
  public class TabControlProxy : INavigationProxy
  {
    private readonly TabControl tabControl;

    public TabControlProxy(TabControl tabControl)
    {
      this.tabControl = tabControl;
    }

    public event RoutedEventHandler Loaded
    {
      add
      {
        this.tabControl.Loaded += value;
      }
      remove
      {
        this.tabControl.Loaded -= value;
      }
    }

    public event RoutedEventHandler Unloaded
    {
      add
      {
        this.tabControl.Unloaded += value;
      }
      remove
      {
        this.tabControl.Unloaded -= value;
      }
    }

    public event RoutedEventHandler GotFocus
    {
      add
      {
        this.tabControl.GotFocus += value;
      }
      remove
      {
        this.tabControl.GotFocus -= value;
      }
    }

    public object SelectedItem
    {
      get
      {
        return this.tabControl.SelectedItem;
      }
      set
      {
        this.tabControl.SelectedItem = value;
      }
    }

    public void EnsureItemSelected()
    {
      if (this.tabControl.SelectedItem == null && this.tabControl.Items.Count > 0)
        this.tabControl.SelectedItem = this.tabControl.Items[0];
      object selectedItem = this.tabControl.SelectedItem;
      Control control = selectedItem as Control ?? this.tabControl.ItemContainerGenerator.ContainerFromItem(selectedItem) as Control;
      if (control == null)
        return;
      control.Focus();
    }

    public void Focus()
    {
      this.tabControl.Focus();
    }
  }
}
