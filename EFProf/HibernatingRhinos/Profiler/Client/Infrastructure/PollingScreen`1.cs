﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Infrastructure.PollingScreen`1
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System;

namespace HibernatingRhinos.Profiler.Client.Infrastructure
{
  public abstract class PollingScreen<T> : PollingScreen
  {
    private readonly Func<Guid, T> getSnapshot;
    private readonly Guid[] sessionIds;

    protected PollingScreen(Func<Guid, T> getSnapshot, params Guid[] sessionIds)
    {
      this.sessionIds = sessionIds;
      this.getSnapshot = getSnapshot;
    }

    public override void TimerTicked(object sender, EventArgs e)
    {
      if (this.State == PollingState.Finalized)
      {
        PollingService.UnRegister(new EventHandler(((PollingScreen) this).TimerTicked));
      }
      else
      {
        foreach (Guid sessionId in this.sessionIds)
          this.Update(this.getSnapshot(sessionId));
        if (this.State != PollingState.NotFinalized)
          return;
        this.State = PollingState.Finalized;
      }
    }

    public abstract void Update(T snapshot);

    public override void ForceUpdate()
    {
      foreach (Guid sessionId in this.sessionIds)
        this.Update(this.getSnapshot(sessionId));
      if (this.State != PollingState.NotFinalized)
        return;
      this.State = PollingState.Finalized;
    }
  }
}
