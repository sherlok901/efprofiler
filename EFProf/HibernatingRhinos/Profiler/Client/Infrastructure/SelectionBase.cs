﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Infrastructure.SelectionBase
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using System;
using System.Linq.Expressions;

namespace HibernatingRhinos.Profiler.Client.Infrastructure
{
  public abstract class SelectionBase : PropertyChangedBase
  {
    [NonSerialized]
    private bool isSelected;
    [NonSerialized]
    private string displayName;

    public bool IsSelected
    {
      get
      {
        return this.isSelected;
      }
      set
      {
        this.isSelected = value;
        this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.IsSelected));
      }
    }

    public string DisplayName
    {
      get
      {
        return this.displayName;
      }
      set
      {
        this.displayName = value;
        this.NotifyOfPropertyChange<string>((Expression<Func<string>>) (() => this.DisplayName));
      }
    }
  }
}
