﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Infrastructure.ValueHolder
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System.Windows;

namespace HibernatingRhinos.Profiler.Client.Infrastructure
{
  public class ValueHolder : Freezable
  {
    public static readonly DependencyProperty ValueProperty = DependencyProperty.Register(nameof (Value), typeof (object), typeof (ValueHolder), new PropertyMetadata((PropertyChangedCallback) null));

    public object Value
    {
      get
      {
        return this.GetValue(ValueHolder.ValueProperty);
      }
      set
      {
        this.SetValue(ValueHolder.ValueProperty, value);
      }
    }

    protected override Freezable CreateInstanceCore()
    {
      return (Freezable) new ValueHolder();
    }
  }
}
