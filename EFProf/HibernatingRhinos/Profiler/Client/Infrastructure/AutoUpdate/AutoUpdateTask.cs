﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Infrastructure.AutoUpdate.AutoUpdateTask
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.BackEnd.Profiles;
using HibernatingRhinos.Profiler.Client.Extensions;
using ICSharpCode.SharpZipLib.Zip;
using log4net;
using NAppUpdate.Framework;
using NAppUpdate.Framework.Conditions;
using NAppUpdate.Framework.Sources;
using NAppUpdate.Framework.Tasks;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace HibernatingRhinos.Profiler.Client.Infrastructure.AutoUpdate
{
  public class AutoUpdateTask : IUpdateTask
  {
    private static readonly ILog Log = LogManager.GetLogger(typeof (AutoUpdateTask));
    private IList<string> filesList;
    private readonly string updateDirectory;

    public AutoUpdateTask()
    {
      this.Attributes = (IDictionary<string, string>) new Dictionary<string, string>();
      this.UpdateConditions = new BooleanCondition();
      this.updateDirectory = UpdateManager.Instance.TempFolder;
    }

    public IDictionary<string, string> Attributes { get; private set; }

    public string Description { get; set; }

    public BooleanCondition UpdateConditions { get; set; }

    public bool Prepare(IUpdateSource source)
    {
      IOExtensions.DeleteDirectory(this.updateDirectory);
      try
      {
        Directory.CreateDirectory(this.updateDirectory);
      }
      catch (Exception ex)
      {
        AutoUpdateTask.Log.Error((object) "Error on creating a temp directory for auto update task", ex);
        throw new UpdateProcessFailedException("Couldn't create a temp directory for auto update", ex);
      }
      string tempLocation = (string) null;
      try
      {
        if (!source.GetData(Profile.Current.LatestVersionDownloadUrl, string.Empty, ref tempLocation))
          return false;
      }
      catch (Exception ex)
      {
        AutoUpdateTask.Log.Error((object) "Cannot get update package from source", ex);
        throw new UpdateProcessFailedException("Couldn't get Data from source", ex);
      }
      if (string.IsNullOrEmpty(tempLocation))
        return false;
      return this.Extract(tempLocation);
    }

    private bool Extract(string zipLocation)
    {
      int num = 0;
      using (ZipFile zipFile = new ZipFile(zipLocation))
      {
        if (!zipFile.TestArchive(true))
        {
          AutoUpdateTask.Log.ErrorFormat("Update package zip file was corrupt");
          throw new InvalidOperationException("Zip file was corrupt");
        }
        using (ZipInputStream zipInputStream = new ZipInputStream((Stream) new FileStream(zipLocation, FileMode.Open, FileAccess.Read)))
        {
          this.filesList = (IList<string>) new List<string>();
          ZipEntry nextEntry;
          while ((nextEntry = zipInputStream.GetNextEntry()) != null)
          {
            if (!".config".Equals(Path.GetExtension(nextEntry.Name), StringComparison.InvariantCultureIgnoreCase))
            {
              string path = Path.Combine(this.updateDirectory, nextEntry.Name);
              if (File.Exists(path))
                File.Delete(path);
              using (FileStream fileStream = File.Create(path))
              {
                byte[] buffer = new byte[zipInputStream.Length];
                num += buffer.Length;
                zipInputStream.Read(buffer, 0, (int) zipInputStream.Length);
                fileStream.Write(buffer, 0, buffer.Length);
              }
              this.filesList.Add(nextEntry.Name);
            }
          }
        }
      }
      return true;
    }

    public bool Execute()
    {
      return true;
    }

    public IEnumerator<KeyValuePair<string, object>> GetColdUpdates()
    {
      if (this.filesList != null)
      {
        Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);
        foreach (string files in (IEnumerable<string>) this.filesList)
        {
          yield return new KeyValuePair<string, object>(files, (object) Path.Combine(this.updateDirectory, files));
          AutoUpdateTask.Log.DebugFormat("Registering file {0} to be updated with {1}", (object) files, (object) Path.Combine(this.updateDirectory, files));
        }
      }
    }

    public bool Rollback()
    {
      return true;
    }
  }
}
