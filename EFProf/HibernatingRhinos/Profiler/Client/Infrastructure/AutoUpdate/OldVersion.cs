﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Infrastructure.AutoUpdate.OldVersion
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.BackEnd.Profiles;
using System;
using System.Windows;

namespace HibernatingRhinos.Profiler.Client.Infrastructure.AutoUpdate
{
  public static class OldVersion
  {
    public const string PrivateBuild = "PRIVATE-BUILD";

    public static void ShowOldVersionWarning(int latestBuild)
    {
      int currentBuild;
      if (!OldVersion.TryGetCurrentBuildNumber(latestBuild, out currentBuild))
        return;
      int num = latestBuild - currentBuild;
      if (num < 10)
        return;
      string str = "Your [[profile]] Profiler version is too old, you need to download the new version to continue with [[profile]] Profiler." + Environment.NewLine + "Do you want to go to the [[profile]] Profiler download page?";
      if (num > 25)
        str = "Your [[profile]] Profiler version is too old, you need to download the new version to continue with [[profile]] Profiler." + Environment.NewLine + "You can continue using this version of the [[profile]] Profiler, but there is no support provided for old versions." + Environment.NewLine + "Do you want to go to the [[profile]] Profiler download page?";
      OldVersion.AskIfUserWantsToUpgrade(Profile.Current.Translate(str));
    }

    public static bool IsDevelopmentBuild
    {
      get
      {
        return BuildInfo.GetCurrentBuild().Trim() == "PRIVATE-BUILD";
      }
    }

    public static bool TryGetCurrentBuildNumber(int latestBuild, out int currentBuild)
    {
      string s = BuildInfo.GetCurrentBuild().Trim();
      if (s == "PRIVATE-BUILD")
        s = (latestBuild - 1).ToString();
      return int.TryParse(s, out currentBuild);
    }

    private static void AskIfUserWantsToUpgrade(string message)
    {
      if (MessageBox.Show(message, Profile.Current.Translate("[[profile]] Profiler is too old, please update the profiler..."), MessageBoxButton.YesNo, MessageBoxImage.Hand) != MessageBoxResult.Yes)
        return;
      BrowserLauncher.Url(Profile.Current.DownloadUrl, "Unable to open browser for application update.");
    }
  }
}
