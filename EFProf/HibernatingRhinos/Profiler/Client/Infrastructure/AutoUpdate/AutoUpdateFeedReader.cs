﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Infrastructure.AutoUpdate.AutoUpdateFeedReader
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using NAppUpdate.Framework.FeedReaders;
using NAppUpdate.Framework.Tasks;
using System.Collections.Generic;

namespace HibernatingRhinos.Profiler.Client.Infrastructure.AutoUpdate
{
  public class AutoUpdateFeedReader : IUpdateFeedReader
  {
    public int LatestVersionNumber { get; set; }

    public IList<IUpdateTask> Read(string feed)
    {
      if (!this.NeedToUpdate(feed))
        return (IList<IUpdateTask>) null;
      return (IList<IUpdateTask>) new List<IUpdateTask>()
      {
        (IUpdateTask) new AutoUpdateTask()
      };
    }

    protected bool NeedToUpdate(string serverVersion)
    {
      int result;
      int currentBuild;
      if (!int.TryParse(serverVersion, out result) || !OldVersion.TryGetCurrentBuildNumber(result, out currentBuild))
        return false;
      this.LatestVersionNumber = result;
      return currentBuild < result;
    }
  }
}
