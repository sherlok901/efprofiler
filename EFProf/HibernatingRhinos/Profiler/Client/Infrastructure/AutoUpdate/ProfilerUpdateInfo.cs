﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Infrastructure.AutoUpdate.ProfilerUpdateInfo
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using System;
using System.Linq.Expressions;

namespace HibernatingRhinos.Profiler.Client.Infrastructure.AutoUpdate
{
  public class ProfilerUpdateInfo : PropertyChangedBase
  {
    private string applyNewBuildInfo;
    private string downloadingNewBuildInfo;
    private string autoUpdateNotAllowedInfo;

    public bool IsUpdateAvailable { get; set; }

    public string ApplyNewBuildInfo
    {
      get
      {
        return this.applyNewBuildInfo;
      }
      set
      {
        this.applyNewBuildInfo = value;
        this.NotifyOfPropertyChange<string>((Expression<Func<string>>) (() => this.ApplyNewBuildInfo));
      }
    }

    public string DownloadingNewBuildInfo
    {
      get
      {
        return this.downloadingNewBuildInfo;
      }
      set
      {
        this.downloadingNewBuildInfo = value;
        this.NotifyOfPropertyChange<string>((Expression<Func<string>>) (() => this.DownloadingNewBuildInfo));
      }
    }

    public string AutoUpdateNotAllowedInfo
    {
      get
      {
        return this.autoUpdateNotAllowedInfo;
      }
      set
      {
        this.autoUpdateNotAllowedInfo = value;
        this.NotifyOfPropertyChange<string>((Expression<Func<string>>) (() => this.AutoUpdateNotAllowedInfo));
      }
    }
  }
}
