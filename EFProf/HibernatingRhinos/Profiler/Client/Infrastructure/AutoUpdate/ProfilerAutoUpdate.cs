﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Infrastructure.AutoUpdate.ProfilerAutoUpdate
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.BackEnd.Profiles;
using HibernatingRhinos.Profiler.Client.Settings;
using log4net;
using NAppUpdate.Framework;
using NAppUpdate.Framework.FeedReaders;
using NAppUpdate.Framework.Sources;
using Rhino.Licensing;
using System;
using System.Diagnostics;
using System.IO;
using System.Threading;

namespace HibernatingRhinos.Profiler.Client.Infrastructure.AutoUpdate
{
  public class ProfilerAutoUpdate
  {
    private static readonly ILog Log = LogManager.GetLogger(typeof (ProfilerAutoUpdate));
    private const int minute = 60000;
    public const string UpgradeToBuildText = "Upgrade to Build #{0}";
    private readonly UpdateManager updateManager;
    private Timer timer;
    private int failuresNumber;
    private bool autoUpdateAllowed;

    public static bool IsLicenseDoesNotAllowAutoUpdate { get; set; }

    public ProfilerAutoUpdate()
    {
      this.updateManager = UpdateManager.Instance;
      this.Info = new ProfilerUpdateInfo();
    }

    public ProfilerUpdateInfo Info { get; private set; }

    public ProfilerAutoUpdate Configure()
    {
      this.updateManager.UpdateProcessName = Profile.CurrentProfile + "UpdateProcess";
      this.updateManager.UpdateFeedReader = (IUpdateFeedReader) new AutoUpdateFeedReader();
      this.updateManager.UpdateSource = (IUpdateSource) new SimpleWebSource(Profile.Current.LatestVersionUrl);
      this.updateManager.TempFolder = ProfilerAutoUpdate.GetUpdateDirectory();
      return this;
    }

    public void CheckForUpdates(bool forceAutoUpdate)
    {
      this.autoUpdateAllowed = forceAutoUpdate || UserPreferencesHolder.UserSettings.AutoUpdate;
      if (!forceAutoUpdate && OldVersion.IsDevelopmentBuild)
        return;
      this.StartAutoUpdateTimer();
    }

    private void StartAutoUpdateTimer()
    {
      this.timer = new Timer((TimerCallback) (state =>
      {
        if (this.timer != null)
        {
          this.timer.Dispose();
          this.timer = (Timer) null;
        }
        ProfilerAutoUpdate.Log.InfoFormat("Checking for updates...");
        this.updateManager.CheckForUpdateAsync(new Action<int>(this.StartDownloadingUpdate));
      }), (object) null, 2 * this.failuresNumber * 60000, -1);
    }

    private void StartDownloadingUpdate(int updates)
    {
      if (updates == 0)
        this.Info.IsUpdateAvailable = false;
      else if (updates < 0)
      {
        if (!this.updateManager.LatestError.Contains("NullReferenceException") && !this.updateManager.LatestError.Contains("NAppUpdate.Framework.UpdateManager.CheckForUpdates(IUpdateSource source, Action`1 callback) in z:\\Projects\\NAppUpdate\\src\\NAppUpdate.Framework\\UpdateManager.cs:line 133"))
          ProfilerAutoUpdate.Log.DebugFormat("This is an error that is handled internally, so you can safely ignore this error log. NAppUpdate had the following error internally: {0}", (object) this.updateManager.LatestError);
        this.Info.IsUpdateAvailable = false;
      }
      else
      {
        this.Info.IsUpdateAvailable = true;
        int latestBuild = ((AutoUpdateFeedReader) this.updateManager.UpdateFeedReader).LatestVersionNumber;
        if (ProfilerAutoUpdate.IsLicenseDoesNotAllowAutoUpdate)
        {
          ProfilerAutoUpdate.Log.Info((object) "Your license was valid for updates for 12 months, and has expired for upgrades. You may continue to use the product, but for upgrades you would need to upgrade your license.");
          this.Info.AutoUpdateNotAllowedInfo = string.Format("Update to build #{0} is not allowed", (object) latestBuild);
        }
        else if (!this.autoUpdateAllowed)
        {
          ProfilerAutoUpdate.Log.InfoFormat("Found an update, show notification of build #{0}", (object) latestBuild);
          OldVersion.ShowOldVersionWarning(latestBuild);
          this.Info.ApplyNewBuildInfo = string.Format("Upgrade to Build #{0}", (object) latestBuild);
        }
        else
        {
          ProfilerAutoUpdate.Log.InfoFormat("Found an update, starting downloading build #{0}", (object) latestBuild);
          this.updateManager.PrepareUpdatesAsync((Action<bool>) (success =>
          {
            if (!success)
            {
              if (this.updateManager.LatestError != null)
                ProfilerAutoUpdate.Log.ErrorFormat("Error downloading updates: {0}", (object) this.updateManager.LatestError);
              this.Info.DownloadingNewBuildInfo += " failed.";
              if (this.failuresNumber++ < 5)
                this.StartAutoUpdateTimer();
              else
                OldVersion.ShowOldVersionWarning(latestBuild);
            }
            else if (this.updateManager.State != UpdateManager.UpdateProcessState.Prepared)
            {
              this.Info.DownloadingNewBuildInfo += " failed.";
            }
            else
            {
              this.Info.DownloadingNewBuildInfo = (string) null;
              this.Info.ApplyNewBuildInfo = string.Format("Apply build #{0}", (object) latestBuild);
            }
          }));
          this.Info.DownloadingNewBuildInfo = string.Format("Downloading build #{0}...", (object) latestBuild);
        }
      }
    }

    public static void ApplyUpdates(bool relaunchApplication)
    {
      if (ProfilerAutoUpdate.IsLicenseDoesNotAllowAutoUpdate)
        throw new LicenseUpgradeRequiredException("Could not apply update. Your license was valid for updates for 12 months, and has expired for upgrades. You may continue to use the product, but for upgrades you would need to upgrade your license.");
      UpdateManager instance = UpdateManager.Instance;
      if (instance.State != UpdateManager.UpdateProcessState.Prepared)
        return;
      instance.ApplyUpdates(relaunchApplication);
    }

    private static string GetUpdateDirectory()
    {
      return Path.Combine(Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName) ?? string.Empty, "AutoUpdate");
    }
  }
}
