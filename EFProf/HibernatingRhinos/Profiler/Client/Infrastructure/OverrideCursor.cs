﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Infrastructure.OverrideCursor
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System;
using System.Collections.Generic;
using System.Windows.Input;

namespace HibernatingRhinos.Profiler.Client.Infrastructure
{
  public class OverrideCursor : IDisposable
  {
    private static readonly Stack<Cursor> stack = new Stack<Cursor>();

    public OverrideCursor(Cursor changeToCursor)
    {
      OverrideCursor.stack.Push(changeToCursor);
      if (Mouse.OverrideCursor == changeToCursor)
        return;
      Mouse.OverrideCursor = changeToCursor;
    }

    public void Dispose()
    {
      OverrideCursor.stack.Pop();
      Cursor cursor = OverrideCursor.stack.Count > 0 ? OverrideCursor.stack.Peek() : (Cursor) null;
      if (cursor == Mouse.OverrideCursor)
        return;
      Mouse.OverrideCursor = cursor;
    }
  }
}
