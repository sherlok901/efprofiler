﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Infrastructure.CollectionViewExtentions
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System.ComponentModel;
using System.Windows.Data;

namespace HibernatingRhinos.Profiler.Client.Infrastructure
{
  public static class CollectionViewExtentions
  {
    public static int Count(this ICollectionView view)
    {
      return ((CollectionView) view).Count;
    }
  }
}
