﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Infrastructure.MarkupExtensions.IoCExtension
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using System;
using System.Windows.Markup;

namespace HibernatingRhinos.Profiler.Client.Infrastructure.MarkupExtensions
{
  public class IoCExtension : MarkupExtension
  {
    public string Key { get; set; }

    public IoCExtension()
    {
    }

    public IoCExtension(string key)
    {
      this.Key = key;
    }

    public override object ProvideValue(IServiceProvider serviceProvider)
    {
      return IoC.GetInstance((Type) null, this.Key);
    }
  }
}
