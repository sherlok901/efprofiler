﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Infrastructure.Monitors.MonitorInfoEx
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System.Runtime.InteropServices;

namespace HibernatingRhinos.Profiler.Client.Infrastructure.Monitors
{
  [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
  internal struct MonitorInfoEx
  {
    public int Size;
    public Win32Rect Monitor;
    public Win32Rect WorkArea;
    public uint Flags;
    [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
    public string DeviceName;

    public void Init()
    {
      this.Size = 104;
      this.DeviceName = string.Empty;
    }
  }
}
