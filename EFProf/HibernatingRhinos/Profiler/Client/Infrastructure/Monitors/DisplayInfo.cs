﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Infrastructure.Monitors.DisplayInfo
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

namespace HibernatingRhinos.Profiler.Client.Infrastructure.Monitors
{
  public class DisplayInfo
  {
    public string MonitorName { get; internal set; }

    public Win32Rect MonitorArea { get; internal set; }

    public Win32Rect WorkArea { get; internal set; }

    public int Width { get; internal set; }

    public int Height { get; internal set; }

    public bool IsPrimary { get; internal set; }
  }
}
