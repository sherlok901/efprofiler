﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Infrastructure.Monitors.DisplayManager
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System;
using System.Collections.ObjectModel;
using System.Runtime.InteropServices;

namespace HibernatingRhinos.Profiler.Client.Infrastructure.Monitors
{
  public class DisplayManager
  {
    private static ObservableCollection<DisplayInfo> _displays = new ObservableCollection<DisplayInfo>();
    internal const int DeviceNameCharacterCount = 32;

    [DllImport("user32.dll")]
    private static extern bool EnumDisplayMonitors(IntPtr hdc, IntPtr lprcClip, DisplayManager.MonitorEnumProcDelegate lpfnEnum, uint dwData);

    [DllImport("user32.dll", CharSet = CharSet.Auto)]
    private static extern bool GetMonitorInfo(IntPtr hMonitor, ref MonitorInfoEx lpmi);

    public static ObservableCollection<DisplayInfo> Displays
    {
      get
      {
        return DisplayManager._displays;
      }
    }

    public static void LoadDisplays()
    {
      DisplayManager.EnumDisplayMonitors(IntPtr.Zero, IntPtr.Zero, new DisplayManager.MonitorEnumProcDelegate(DisplayManager.MonitorEnumProc), 0U);
    }

    [AllowReversePInvokeCalls]
    internal static bool MonitorEnumProc(IntPtr hMonitor, IntPtr hdcMonitor, ref Win32Rect lprcMonitor, uint dwData)
    {
      MonitorInfoEx lpmi = new MonitorInfoEx();
      lpmi.Init();
      if (DisplayManager.GetMonitorInfo(hMonitor, ref lpmi))
        DisplayManager._displays.Add(new DisplayInfo()
        {
          MonitorName = lpmi.DeviceName,
          Width = lpmi.Monitor.Right - lpmi.Monitor.Left,
          Height = lpmi.Monitor.Bottom - lpmi.Monitor.Top,
          MonitorArea = lpmi.Monitor,
          WorkArea = lpmi.WorkArea,
          IsPrimary = lpmi.Flags > 0U
        });
      return true;
    }

    private delegate bool MonitorEnumProcDelegate(IntPtr hMonitor, IntPtr hdcMonitor, ref Win32Rect lprcMonitor, uint dwData);
  }
}
