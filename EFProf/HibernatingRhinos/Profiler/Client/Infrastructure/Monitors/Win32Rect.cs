﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Infrastructure.Monitors.Win32Rect
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

namespace HibernatingRhinos.Profiler.Client.Infrastructure.Monitors
{
  public struct Win32Rect
  {
    public int Left { get; set; }

    public int Top { get; set; }

    public int Right { get; set; }

    public int Bottom { get; set; }

    public override string ToString()
    {
      return string.Format("{0}, {1}, {2}, {3}", (object) this.Left, (object) this.Top, (object) this.Right, (object) this.Bottom);
    }
  }
}
