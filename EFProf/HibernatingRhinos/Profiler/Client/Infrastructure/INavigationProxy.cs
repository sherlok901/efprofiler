﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Infrastructure.INavigationProxy
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System.Windows;

namespace HibernatingRhinos.Profiler.Client.Infrastructure
{
  public interface INavigationProxy
  {
    event RoutedEventHandler Loaded;

    event RoutedEventHandler Unloaded;

    event RoutedEventHandler GotFocus;

    object SelectedItem { get; set; }

    void EnsureItemSelected();

    void Focus();
  }
}
