﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Infrastructure.SelectorProxy
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace HibernatingRhinos.Profiler.Client.Infrastructure
{
  public class SelectorProxy : INavigationProxy
  {
    private readonly Selector selector;

    public SelectorProxy(Selector selector)
    {
      this.selector = selector;
    }

    public event RoutedEventHandler Loaded
    {
      add
      {
        this.selector.Loaded += value;
      }
      remove
      {
        this.selector.Loaded -= value;
      }
    }

    public event RoutedEventHandler Unloaded
    {
      add
      {
        this.selector.Unloaded += value;
      }
      remove
      {
        this.selector.Unloaded -= value;
      }
    }

    public event RoutedEventHandler GotFocus
    {
      add
      {
        this.selector.GotFocus += value;
      }
      remove
      {
        this.selector.GotFocus -= value;
      }
    }

    public object SelectedItem
    {
      get
      {
        return this.selector.SelectedItem;
      }
      set
      {
        this.selector.SelectedItem = value;
      }
    }

    public void EnsureItemSelected()
    {
      if (this.selector.SelectedItem == null && this.selector.Items.Count > 0)
        this.selector.SelectedItem = this.selector.Items[0];
      object selectedItem = this.selector.SelectedItem;
      Control control = selectedItem as Control ?? this.selector.ItemContainerGenerator.ContainerFromItem(selectedItem) as Control;
      if (control == null)
        return;
      control.Focus();
    }

    public void Focus()
    {
      this.selector.Focus();
    }
  }
}
