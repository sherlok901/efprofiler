﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Infrastructure.PollingScreen
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System;

namespace HibernatingRhinos.Profiler.Client.Infrastructure
{
  public abstract class PollingScreen : ScreenWithoutViewCache, IPoller
  {
    private PollingState state;

    [CLSCompliant(false)]
    protected PollingState State
    {
      get
      {
        return this.state;
      }
      set
      {
        this.state = value;
      }
    }

    public Action NotifyOnUpdate { get; set; }

    protected override void OnActivate()
    {
      PollingService.Register(new EventHandler(this.TimerTicked));
      base.OnActivate();
    }

    protected override void OnDeactivate(bool close)
    {
      PollingService.UnRegister(new EventHandler(this.TimerTicked));
      base.OnDeactivate(close);
    }

    public abstract void TimerTicked(object sender, EventArgs e);

    public void DonePolling()
    {
      this.State = PollingState.NotFinalized;
    }

    public void ContinuePolling()
    {
      this.State = PollingState.Polling;
    }

    public abstract void ForceUpdate();
  }
}
