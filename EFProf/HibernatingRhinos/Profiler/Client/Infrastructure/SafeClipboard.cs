﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Infrastructure.SafeClipboard
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System.Runtime.InteropServices;
using System.Threading;
using System.Windows;

namespace HibernatingRhinos.Profiler.Client.Infrastructure
{
  public static class SafeClipboard
  {
    public static void SetText(string sql)
    {
      for (int index = 0; index < 15; ++index)
      {
        try
        {
          Clipboard.SetText(sql);
          break;
        }
        catch (COMException ex)
        {
          Thread.Sleep(10);
        }
      }
    }
  }
}
