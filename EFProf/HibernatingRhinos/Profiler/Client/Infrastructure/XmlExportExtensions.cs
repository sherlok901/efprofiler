﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Infrastructure.XmlExportExtensions
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.Client.Reports;
using HibernatingRhinos.Profiler.Client.Statements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace HibernatingRhinos.Profiler.Client.Infrastructure
{
  public static class XmlExportExtensions
  {
    public static XElement ToXml(this QueryDuration queryDuration)
    {
      XElement xelement = new XElement(ReportBase.Namespace + "duration");
      if (queryDuration.InDatabase.HasValue)
        xelement.Add((object) new XAttribute((XName) "database-only", (object) queryDuration.InDatabase));
      if (queryDuration.InNHibernate.HasValue)
        xelement.Add((object) new XAttribute((XName) "with-materialization", (object) queryDuration.InNHibernate));
      return xelement;
    }

    public static XElement ToXml(this IStatementModel y)
    {
      XElement xelement = new XElement(ReportBase.Namespace + "query", new object[6]
      {
        (object) new XAttribute((XName) "is-cached", (object) y.IsCached),
        (object) new XAttribute((XName) "is-ddl", (object) y.IsDDL),
        (object) y.Duration.ToXml(),
        (object) new XElement(ReportBase.Namespace + "formatted-sql", (object) y.Text),
        (object) new XElement(ReportBase.Namespace + "short-sql", (object) y.ShortText),
        (object) new XElement(ReportBase.Namespace + "alerts", (object) y.Alerts.Select<StatementAlert, XElement>((Func<StatementAlert, XElement>) (z => z.ToXml())))
      });
      if (y.CountOfRows.Length != 0)
        xelement.Add((object) new XElement(ReportBase.Namespace + "row-counts", (object) ((IEnumerable<int>) y.CountOfRows).Select<int, XElement>((Func<int, XElement>) (i => new XElement(ReportBase.Namespace + "row-count", (object) i)))));
      return xelement;
    }

    public static XElement ToXml(this double? y, string element)
    {
      XElement xelement = new XElement(ReportBase.Namespace + element);
      if (y.HasValue)
        xelement.Add((object) y.ToString());
      return xelement;
    }

    public static XElement ToXml(this StatementAlert z)
    {
      return new XElement(ReportBase.Namespace + "alert", new object[3]
      {
        (object) new XAttribute((XName) "title", (object) z.Title),
        (object) new XAttribute((XName) "severity", (object) z.Severity),
        (object) new XAttribute((XName) "help-topic", (object) z.HelpTopic)
      });
    }
  }
}
