﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Infrastructure.ModelValidator
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System.Text.RegularExpressions;

namespace HibernatingRhinos.Profiler.Client.Infrastructure
{
  public static class ModelValidator
  {
    public static bool EmptyOrNullValidator(string value)
    {
      return string.IsNullOrEmpty(value);
    }

    public static string EmptyOrNullFail(string fieldName)
    {
      return string.Format("{0} cannot be empty", (object) fieldName);
    }

    public static bool EmailValidator(string value)
    {
      if (value == null)
        return true;
      return !Regex.IsMatch(value, "[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}", RegexOptions.IgnoreCase);
    }

    public static string EmailFail(string fieldName)
    {
      return string.Format("{0} must be a valid email address", (object) fieldName);
    }
  }
}
