﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Infrastructure.Validation.HostValidator
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Text.RegularExpressions;

namespace HibernatingRhinos.Profiler.Client.Infrastructure.Validation
{
  public class HostValidator : ValidationAttribute
  {
    private const string HostNameRegEx = "^([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]{0,61}[a-zA-Z0-9])(\\.([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]{0,61}[a-zA-Z0-9]))*$";

    public override bool IsValid(object value)
    {
      string str = value as string;
      if (string.IsNullOrWhiteSpace(str))
        return false;
      IPAddress address;
      if (IPAddress.TryParse(str, out address))
        return true;
      if (Regex.IsMatch(str, "^([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]{0,61}[a-zA-Z0-9])(\\.([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]{0,61}[a-zA-Z0-9]))*$"))
        return str.Length < (int) byte.MaxValue;
      return false;
    }
  }
}
