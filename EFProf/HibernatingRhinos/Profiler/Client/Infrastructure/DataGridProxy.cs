﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Infrastructure.DataGridProxy
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace HibernatingRhinos.Profiler.Client.Infrastructure
{
  public class DataGridProxy : INavigationProxy
  {
    private readonly DataGrid grid;

    public DataGridProxy(DataGrid grid)
    {
      this.grid = grid;
    }

    public event RoutedEventHandler Loaded
    {
      add
      {
        this.grid.Loaded += value;
      }
      remove
      {
        this.grid.Loaded -= value;
      }
    }

    public event RoutedEventHandler Unloaded
    {
      add
      {
        this.grid.Unloaded += value;
      }
      remove
      {
        this.grid.Unloaded -= value;
      }
    }

    public event RoutedEventHandler GotFocus
    {
      add
      {
        this.grid.GotFocus += value;
      }
      remove
      {
        this.grid.GotFocus -= value;
      }
    }

    public object SelectedItem
    {
      get
      {
        return this.grid.SelectedItem;
      }
      set
      {
        this.grid.SelectedItem = value;
      }
    }

    public void EnsureItemSelected()
    {
      if (this.grid.SelectedItem != null)
        return;
      List<object> list = this.grid.ItemsSource.OfType<object>().ToList<object>();
      if (list.Count <= 0)
        return;
      this.grid.SelectedItem = list[0];
    }

    public void Focus()
    {
      this.grid.Focus();
    }
  }
}
