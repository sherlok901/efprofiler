﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Infrastructure.PollingService
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System;
using System.Collections.Generic;
using System.Windows.Threading;

namespace HibernatingRhinos.Profiler.Client.Infrastructure
{
  public static class PollingService
  {
    private static readonly List<EventHandler> runOnStart = new List<EventHandler>();
    private static readonly DispatcherTimer Timer = new DispatcherTimer()
    {
      Interval = TimeSpan.FromSeconds(1.0)
    };
    private static bool isStarted;

    public static void Register(EventHandler handler)
    {
      PollingService.Timer.Tick += handler;
      if (PollingService.isStarted)
        handler((object) PollingService.Timer, EventArgs.Empty);
      else
        PollingService.runOnStart.Add(handler);
    }

    public static void UnRegister(EventHandler handler)
    {
      PollingService.Timer.Tick -= handler;
    }

    public static void Start()
    {
      PollingService.isStarted = true;
      PollingService.Timer.Start();
      foreach (EventHandler eventHandler in PollingService.runOnStart)
        eventHandler((object) PollingService.Timer, EventArgs.Empty);
      PollingService.runOnStart.Clear();
    }

    public static void Stop()
    {
      PollingService.Timer.Stop();
    }
  }
}
