﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Infrastructure.BrowserLauncher
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System;
using System.Diagnostics;
using System.Windows;

namespace HibernatingRhinos.Profiler.Client.Infrastructure
{
  public class BrowserLauncher
  {
    public static void Url(string url, string failureMessage)
    {
      try
      {
        Process.Start("explorer.exe", url);
      }
      catch (Exception ex)
      {
        int num = (int) MessageBox.Show(failureMessage + Environment.NewLine + "Please visit " + url + " directly." + Environment.NewLine + "Error: " + ex.Message);
      }
    }
  }
}
