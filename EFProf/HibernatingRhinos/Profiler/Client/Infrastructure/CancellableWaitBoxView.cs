﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Infrastructure.CancellableWaitBoxView
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using System.Windows.Media.Animation;

namespace HibernatingRhinos.Profiler.Client.Infrastructure
{
  public partial class CancellableWaitBoxView : UserControl, IComponentConnector
  {
    //internal Button Cancel;
    //internal ProgressBar PbFileLoadBar;
    //internal Image image;
    //private bool _contentLoaded;

    public CancellableWaitBoxView()
    {
      this.InitializeComponent();
      this.Loaded += (RoutedEventHandler) ((param0, param1) => (this.Resources[(object) "RhinoCharging"] as Storyboard).Begin());
    }

    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //public void InitializeComponent()
    //{
    //  if (this._contentLoaded)
    //    return;
    //  this._contentLoaded = true;
    //  Application.LoadComponent((object) this, new Uri("/EFProf;component/waitbox/cancellablewaitboxview.xaml", UriKind.Relative));
    //}

    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[EditorBrowsable(EditorBrowsableState.Never)]
    //void IComponentConnector.Connect(int connectionId, object target)
    //{
    //  switch (connectionId)
    //  {
    //    case 1:
    //      this.Cancel = (Button) target;
    //      break;
    //    case 2:
    //      this.PbFileLoadBar = (ProgressBar) target;
    //      break;
    //    case 3:
    //      this.image = (Image) target;
    //      break;
    //    default:
    //      this._contentLoaded = true;
    //      break;
    //  }
    //}
  }
}
