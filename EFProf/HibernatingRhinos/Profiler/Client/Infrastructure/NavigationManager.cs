﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Infrastructure.NavigationManager
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.Client.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;

namespace HibernatingRhinos.Profiler.Client.Infrastructure
{
  public class NavigationManager
  {
    public static readonly DependencyProperty PaneProperty = DependencyProperty.RegisterAttached("Pane", typeof (string), typeof (NavigationManager), new PropertyMetadata(new PropertyChangedCallback(NavigationManager.PaneChanged)));
    private static readonly Dictionary<Key, INavigationProxy> SpecialPanes = new Dictionary<Key, INavigationProxy>();
    private static readonly Dictionary<int, List<INavigationProxy>> Panes = new Dictionary<int, List<INavigationProxy>>();
    private static int _row;
    private static int _column;
    private static bool _isFocusing;

    private static INavigationProxy CurrentPane { get; set; }

    public static void SetPane(DependencyObject d, string value)
    {
      d.SetValue(NavigationManager.PaneProperty, (object) value);
    }

    public static string GetPane(DependencyObject d)
    {
      return d.GetValue(NavigationManager.PaneProperty) as string;
    }

    public static void NextPane()
    {
      ++NavigationManager._column;
      if (NavigationManager._column < NavigationManager.Panes[NavigationManager._row].Count)
      {
        NavigationManager.Focus(NavigationManager.Panes[NavigationManager._row][NavigationManager._column]);
      }
      else
      {
        NavigationManager._column = 0;
        ++NavigationManager._row;
        if (NavigationManager.Panes.ContainsKey(NavigationManager._row))
        {
          List<INavigationProxy> pane = NavigationManager.Panes[NavigationManager._row];
          if (NavigationManager._column < pane.Count)
          {
            NavigationManager.Focus(NavigationManager.Panes[NavigationManager._row][NavigationManager._column]);
            return;
          }
        }
        NavigationManager._row = 0;
        NavigationManager.Focus(NavigationManager.Panes[NavigationManager._row][NavigationManager._column]);
      }
    }

    public static void PreviousPane()
    {
      --NavigationManager._column;
      if (NavigationManager._column < NavigationManager.Panes[NavigationManager._row].Count && NavigationManager._column >= 0)
      {
        NavigationManager.Focus(NavigationManager.Panes[NavigationManager._row][NavigationManager._column]);
      }
      else
      {
        --NavigationManager._row;
        if (NavigationManager._row >= 0)
        {
          List<INavigationProxy> pane = NavigationManager.Panes[NavigationManager._row];
          NavigationManager._column = pane.Count - 1;
          if (NavigationManager._column >= 0)
          {
            NavigationManager.Focus(pane[NavigationManager._column]);
            return;
          }
        }
        KeyValuePair<int, List<INavigationProxy>> keyValuePair = NavigationManager.Panes.Last<KeyValuePair<int, List<INavigationProxy>>>();
        NavigationManager._row = keyValuePair.Key;
        NavigationManager._column = keyValuePair.Value.Count - 1;
        NavigationManager.Focus(NavigationManager.Panes[NavigationManager._row][NavigationManager._column]);
      }
    }

    public static void NextTab()
    {
      NavigationManager.FocusTab(NavigationManager.CurrentPane);
    }

    public static void PreviousTab()
    {
      NavigationManager.FocusTab(NavigationManager.CurrentPane);
    }

    public static void MoveDown()
    {
      if (NavigationManager.CurrentPane == null || NavigationManager.CurrentPane is SelectorProxy)
        return;
      INavigateItems navigateItems = NavigationManager.CurrentPane.SelectedItem as INavigateItems;
      if (navigateItems == null && NavigationManager.CurrentPane.SelectedItem is FrameworkElement)
        navigateItems = ((FrameworkElement) NavigationManager.CurrentPane.SelectedItem).DataContext as INavigateItems;
      if (navigateItems == null)
        return;
      navigateItems.NextItem();
    }

    public static void MoveUp()
    {
      if (NavigationManager.CurrentPane == null || NavigationManager.CurrentPane is SelectorProxy)
        return;
      INavigateItems navigateItems = NavigationManager.CurrentPane.SelectedItem as INavigateItems;
      if (navigateItems == null && NavigationManager.CurrentPane.SelectedItem is FrameworkElement)
        navigateItems = ((FrameworkElement) NavigationManager.CurrentPane.SelectedItem).DataContext as INavigateItems;
      if (navigateItems == null)
        return;
      navigateItems.PreviousItem();
    }

    public static T GetNext<T>(IList<T> items, T currentItem)
    {
      int index = items.IndexOf(currentItem) + 1;
      if (index >= items.Count)
        return currentItem;
      return items[index];
    }

    public static T GetPrevious<T>(IList<T> items, T currentItem)
    {
      int index = items.IndexOf(currentItem) - 1;
      if (index < 0)
        return currentItem;
      return items[index];
    }

    public static void Initialize(FrameworkElement observable, DependencyObject initialPane)
    {
      NavigationManager.Focus(NavigationManager.GetProxy(initialPane));
      observable.KeyUp += (KeyEventHandler) ((s, e) =>
      {
        if (e.Key == Key.Tab)
        {
          if (Keyboard.Modifiers == ModifierKeys.Shift)
            NavigationManager.PreviousPane();
          else
            NavigationManager.NextPane();
          e.Handled = true;
        }
        else if (e.Key == Key.Right || e.Key == Key.L)
        {
          NavigationManager.NextTab();
          e.Handled = true;
        }
        else if (e.Key == Key.Left || e.Key == Key.J)
        {
          NavigationManager.PreviousTab();
          e.Handled = true;
        }
        else if (e.Key == Key.Down || e.Key == Key.K)
        {
          NavigationManager.MoveDown();
          e.Handled = true;
        }
        else if (e.Key == Key.Up || e.Key == Key.I)
        {
          NavigationManager.MoveUp();
          e.Handled = true;
        }
        else
        {
          INavigationProxy pane;
          if (!NavigationManager.SpecialPanes.TryGetValue(e.Key, out pane))
            return;
          NavigationManager.FixUpFocus(pane);
          NavigationManager.Focus(pane);
          e.Handled = true;
        }
      });
    }

    private static void Focus(INavigationProxy pane)
    {
      NavigationManager.CurrentPane = pane;
      NavigationManager.CurrentPane.Focus();
      NavigationManager.FocusTab(NavigationManager.CurrentPane);
    }

    private static void FixUpFocus(INavigationProxy pane)
    {
      NavigationManager.CurrentPane = pane;
      foreach (KeyValuePair<int, List<INavigationProxy>> pane1 in NavigationManager.Panes)
      {
        if (pane1.Value.Contains(NavigationManager.CurrentPane))
        {
          NavigationManager._row = pane1.Key;
          NavigationManager._column = pane1.Value.IndexOf(NavigationManager.CurrentPane);
          break;
        }
      }
    }

    private static void FocusTab(INavigationProxy pane)
    {
      if (pane == null)
        return;
      pane.EnsureItemSelected();
    }

    private static void PaneChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      if (e.NewValue == null || e.NewValue == e.OldValue)
        return;
      INavigationProxy proxy = NavigationManager.GetProxy(d);
      if (proxy == null)
        return;
      string[] strArray = ((string) e.NewValue).Split(new char[4]
      {
        ',',
        ' ',
        ';',
        ':'
      }, StringSplitOptions.RemoveEmptyEntries);
      int row = int.Parse(strArray[0]);
      int column = int.Parse(strArray[1]);
      string empty = string.Empty;
      if (strArray.Length == 3)
        empty = strArray[2];
      NavigationManager.ConfigurePane(proxy, row, column, empty);
    }

    private static void ConfigurePane(INavigationProxy pane, int row, int column, string specialKey)
    {
      Key key = Key.None;
      if (!string.IsNullOrEmpty(specialKey))
        key = (Key) Enum.Parse(typeof (Key), specialKey, true);
      pane.Loaded += (RoutedEventHandler) ((param0, param1) =>
      {
        NavigationManager.GetOrCreateRow(row).Insert(column, pane);
        if (key == Key.None)
          return;
        NavigationManager.SpecialPanes[key] = pane;
      });
      pane.Unloaded += (RoutedEventHandler) ((param0, param1) =>
      {
        NavigationManager.GetOrCreateRow(row).Remove(pane);
        if (key == Key.None)
          return;
        NavigationManager.SpecialPanes.Remove(key);
      });
      pane.GotFocus += (RoutedEventHandler) ((s, e) =>
      {
        if (e.OriginalSource is SyntaxHighlighter || NavigationManager._isFocusing)
          return;
        NavigationManager.FixUpFocus(pane);
        if (e.OriginalSource is ListBoxItem)
          return;
        NavigationManager._isFocusing = true;
        NavigationManager.Focus(pane);
        NavigationManager._isFocusing = false;
      });
    }

    private static List<INavigationProxy> GetOrCreateRow(int row)
    {
      List<INavigationProxy> navigationProxyList;
      if (!NavigationManager.Panes.TryGetValue(row, out navigationProxyList))
      {
        navigationProxyList = new List<INavigationProxy>();
        NavigationManager.Panes[row] = navigationProxyList;
      }
      return navigationProxyList;
    }

    public static INavigationProxy GetProxy(DependencyObject instance)
    {
      Selector selector = instance as Selector;
      if (selector != null)
        return (INavigationProxy) new SelectorProxy(selector);
      DataGrid grid = instance as DataGrid;
      if (grid != null)
        return (INavigationProxy) new DataGridProxy(grid);
      TabControl tabControl = instance as TabControl;
      if (tabControl != null)
        return (INavigationProxy) new TabControlProxy(tabControl);
      return (INavigationProxy) null;
    }
  }
}
