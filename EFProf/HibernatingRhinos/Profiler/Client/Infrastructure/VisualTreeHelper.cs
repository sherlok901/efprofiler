﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Infrastructure.VisualTreeHelper
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System.Windows;

namespace HibernatingRhinos.Profiler.Client.Infrastructure
{
  public static class VisualTreeHelper
  {
    public static T GetAncestor<T>(this DependencyObject element) where T : class
    {
      DependencyObject parent;
      for (; element != null; element = parent)
      {
        parent = System.Windows.Media.VisualTreeHelper.GetParent(element);
        if (parent is T)
          return parent as T;
      }
      return default (T);
    }
  }
}
