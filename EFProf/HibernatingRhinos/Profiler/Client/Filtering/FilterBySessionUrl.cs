﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Filtering.FilterBySessionUrl
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.BackEnd.Profiles;
using HibernatingRhinos.Profiler.Client.CaliburnInfrastructure;
using HibernatingRhinos.Profiler.Client.Filtering.Filters;
using System;
using System.ComponentModel;
using System.Linq.Expressions;

namespace HibernatingRhinos.Profiler.Client.Filtering
{
  [Description("[[sessions]] by URL")]
  [View(typeof (FilterBySessionUrlView))]
  [Serializable]
  public class FilterBySessionUrl : AbstractFilter
  {
    private bool contains = true;
    private string url;

    public bool[] SupportedOperators
    {
      get
      {
        return new bool[2]{ true, false };
      }
    }

    public override bool IsValid
    {
      get
      {
        return !string.IsNullOrEmpty(this.Url);
      }
    }

    public bool Contains
    {
      get
      {
        return this.contains;
      }
      set
      {
        this.contains = value;
        this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.Contains));
      }
    }

    public string Url
    {
      get
      {
        return this.url;
      }
      set
      {
        this.url = value;
        this.NotifyOfPropertyChange<string>((Expression<Func<string>>) (() => this.Url));
        this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.IsValid));
      }
    }

    public override string ToolTip
    {
      get
      {
        return Profile.Current.Translate(string.Format("[[sessions]] with url contains: '{0}'.", (object) this.Url));
      }
    }

    public override bool IsValidSession(IFilterableSessionSnapshot snapshot)
    {
      if (string.IsNullOrEmpty(snapshot.Url))
        return !this.Contains;
      return snapshot.Url.IndexOf(this.Url, StringComparison.InvariantCultureIgnoreCase) != -1 == this.Contains;
    }
  }
}
