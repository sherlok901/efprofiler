﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Filtering.IFilter
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.BackEnd.Bridge;

namespace HibernatingRhinos.Profiler.Client.Filtering
{
  public interface IFilter
  {
    bool IsValidStatement(IFilterableStatementSnapshot snapshot);

    bool IsValidSession(IFilterableSessionSnapshot snapshot);

    bool IsValid { get; }

    string ToolTip { get; }

    bool IsActive { get; set; }
  }
}
