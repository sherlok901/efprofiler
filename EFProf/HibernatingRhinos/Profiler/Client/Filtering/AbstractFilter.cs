﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Filtering.AbstractFilter
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using HibernatingRhinos.Profiler.BackEnd.Bridge;
using Newtonsoft.Json;
using System;
using System.Linq.Expressions;
using System.Runtime.Serialization;

namespace HibernatingRhinos.Profiler.Client.Filtering
{
  [Serializable]
  public abstract class AbstractFilter : PropertyChangedBase, IFilter
  {
    private bool isActive = true;

    public virtual bool IsValidStatement(IFilterableStatementSnapshot snapshot)
    {
      return true;
    }

    public virtual bool IsValidSession(IFilterableSessionSnapshot snapshot)
    {
      return true;
    }

    [JsonIgnore]
    public abstract bool IsValid { get; }

    [JsonIgnore]
    public abstract string ToolTip { get; }

    public bool IsActive
    {
      get
      {
        return this.isActive;
      }
      set
      {
        this.isActive = value;
        this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.IsActive));
        this.NotifyOfPropertyChange<string>((Expression<Func<string>>) (() => this.IsActiveTooltip));
      }
    }

    public string IsActiveTooltip
    {
      get
      {
        return string.Format("{0} filter", this.IsActive ? (object) "Disable" : (object) "Enable");
      }
    }

    [OnDeserialized]
    public void OnDeserialized(StreamingContext sc)
    {
    }
  }
}
