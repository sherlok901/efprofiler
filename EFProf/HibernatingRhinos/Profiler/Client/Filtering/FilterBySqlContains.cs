﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Filtering.FilterBySqlContains
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.Client.CaliburnInfrastructure;
using HibernatingRhinos.Profiler.Client.Filtering.Filters;
using System;
using System.ComponentModel;
using System.Linq.Expressions;

namespace HibernatingRhinos.Profiler.Client.Filtering
{
  [View(typeof (FilterBySqlContainsView))]
  [Description("SQL Contains")]
  [Serializable]
  public class FilterBySqlContains : AbstractFilter
  {
    private bool contains = true;
    private string text;

    public bool[] SupportedOperators
    {
      get
      {
        return new bool[2]{ true, false };
      }
    }

    public override bool IsValid
    {
      get
      {
        return !string.IsNullOrEmpty(this.Text);
      }
    }

    public bool Contains
    {
      get
      {
        return this.contains;
      }
      set
      {
        this.contains = value;
        this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.Contains));
        this.NotifyOfPropertyChange<string>((Expression<Func<string>>) (() => this.ToolTip));
      }
    }

    public string Text
    {
      get
      {
        return this.text;
      }
      set
      {
        this.text = value;
        this.NotifyOfPropertyChange<string>((Expression<Func<string>>) (() => this.Text));
        this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.IsValid));
      }
    }

    public override string ToolTip
    {
      get
      {
        return string.Format("The raw sql {1}contains '{0}'.", (object) this.Text, this.contains ? (object) (string) null : (object) "not ");
      }
    }

    public override bool IsValidStatement(IFilterableStatementSnapshot snapshot)
    {
      return snapshot.RawSql.Contains(this.Text) == this.Contains;
    }
  }
}
