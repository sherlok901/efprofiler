﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Filtering.FilterByDuration
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.Client.CaliburnInfrastructure;
using HibernatingRhinos.Profiler.Client.Filtering.Filters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq.Expressions;

namespace HibernatingRhinos.Profiler.Client.Filtering
{
  [View(typeof (FilterByDurationView))]
  [Description("Duration")]
  [Serializable]
  public class FilterByDuration : AbstractFilter
  {
    private static readonly Dictionary<ExpressionType, string> expressionDescriptions = new Dictionary<ExpressionType, string>()
    {
      {
        ExpressionType.GreaterThan,
        "is greater than"
      },
      {
        ExpressionType.LessThan,
        "is less than"
      }
    };
    private ExpressionType @operator;
    private string value;

    public ExpressionType[] SupportedOperators
    {
      get
      {
        return new ExpressionType[2]
        {
          ExpressionType.GreaterThan,
          ExpressionType.LessThan
        };
      }
    }

    public virtual string Value
    {
      get
      {
        return this.value;
      }
      set
      {
        this.value = value;
        this.NotifyOfPropertyChange<string>((Expression<Func<string>>) (() => this.Value));
        this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.IsValid));
        this.NotifyOfPropertyChange<string>((Expression<Func<string>>) (() => this.ToolTip));
      }
    }

    public ExpressionType Operator
    {
      get
      {
        return this.@operator;
      }
      set
      {
        this.@operator = value;
        this.NotifyOfPropertyChange<ExpressionType>((Expression<Func<ExpressionType>>) (() => this.Operator));
        this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.IsValid));
      }
    }

    public FilterByDuration()
    {
      this.Operator = ExpressionType.GreaterThan;
    }

    public override bool IsValid
    {
      get
      {
        if (this.Operator == ExpressionType.GreaterThan || this.Operator == ExpressionType.LessThan)
          return FilterByDuration.IsDouble((object) this.Value);
        return false;
      }
    }

    public override string ToolTip
    {
      get
      {
        return string.Format("Total duration {0} {1}ms.", (object) FilterByDuration.LabelFor(this.Operator), (object) this.Value);
      }
    }

    protected static string LabelFor(ExpressionType type)
    {
      if (!FilterByDuration.expressionDescriptions.ContainsKey(type))
        throw new InvalidOperationException("A label for the ExpressionType, " + (object) type + ", should be added to expressionDescriptions.");
      return FilterByDuration.expressionDescriptions[type];
    }

    public override bool IsValidStatement(IFilterableStatementSnapshot snapshot)
    {
      switch (this.Operator)
      {
        case ExpressionType.GreaterThan:
          return this.GreaterThanDuration(snapshot);
        case ExpressionType.LessThan:
          return this.LessThanDuration(snapshot);
        default:
          throw new InvalidOperationException("No idea what to do: " + (object) this.Operator);
      }
    }

    private bool LessThanDuration(IFilterableStatementSnapshot statement)
    {
      if (!statement.Duration.Value.HasValue)
        return false;
      int? nullable = statement.Duration.Value;
      double num = Convert.ToDouble(this.Value);
      if ((double) nullable.GetValueOrDefault() < num)
        return nullable.HasValue;
      return false;
    }

    private bool GreaterThanDuration(IFilterableStatementSnapshot statement)
    {
      if (!statement.Duration.Value.HasValue)
        return false;
      int? nullable = statement.Duration.Value;
      double num = Convert.ToDouble(this.Value);
      if ((double) nullable.GetValueOrDefault() > num)
        return nullable.HasValue;
      return false;
    }

    private static bool IsDouble(object value)
    {
      if (value == null)
        return false;
      try
      {
        Convert.ToDouble(value);
        return true;
      }
      catch
      {
        return false;
      }
    }
  }
}
