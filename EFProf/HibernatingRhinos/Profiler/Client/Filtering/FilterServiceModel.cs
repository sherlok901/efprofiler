﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Filtering.FilterServiceModel
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.BackEnd.Extensions;
using HibernatingRhinos.Profiler.BackEnd.Profiles;
using HibernatingRhinos.Profiler.Client.Controls;
using HibernatingRhinos.Profiler.Client.Infrastructure;
using HibernatingRhinos.Profiler.Client.Settings;
using HibernatingRhinos.Profiler.Client.Tracking;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Xml;
using System.Xml.Serialization;

namespace HibernatingRhinos.Profiler.Client.Filtering
{
  public class FilterServiceModel : ProfilerScreen
  {
    private bool allFilters = true;
    private readonly ITrackingService trackingService;
    private ObservableCollection<IFilter> filters;

    public FilterServiceModel(ITrackingService trackingService)
    {
      this.FiltersChanged += (EventHandler) ((s, e) => {});
      this.trackingService = trackingService;
      this.Filters = new ObservableCollection<IFilter>();
      this.EditedFilters = new ObservableCollection<IFilter>((IEnumerable<IFilter>) this.Filters);
    }

    protected override void OnViewAttached(object view, object context)
    {
      base.OnViewAttached(view, context);
      if (!(context is string) || !((string) context == "Edit"))
        return;
      this.DisplayName = "Edit Filter";
      this.EditedFilters = new ObservableCollection<IFilter>((IEnumerable<IFilter>) this.Filters);
      this.EditedFilters.CollectionChanged += (NotifyCollectionChangedEventHandler) ((s, e) => this.NotifyOfPropertyChange<ObservableCollection<IFilter>>((Expression<Func<ObservableCollection<IFilter>>>) (() => this.EditedFilters)));
    }

    public event EventHandler FiltersChanged = (s, e) => {};

    public ObservableCollection<IFilter> Filters
    {
      get
      {
        return this.filters;
      }
      set
      {
        this.filters = value;
        this.RaisedFiltersChanged();
        this.filters.CollectionChanged += (NotifyCollectionChangedEventHandler) ((sender, args) =>
        {
          this.RaisedFiltersChanged();
          this.NotifyOfPropertyChange<ObservableCollection<IFilter>>((Expression<Func<ObservableCollection<IFilter>>>) (() => this.Filters));
        });
        this.NotifyOfPropertyChange<ObservableCollection<IFilter>>((Expression<Func<ObservableCollection<IFilter>>>) (() => this.Filters));
      }
    }

    public bool AllFilters
    {
      get
      {
        return this.allFilters;
      }
      set
      {
        this.allFilters = value;
        this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.AllFilters));
      }
    }

    private void RaisedFiltersChanged()
    {
      this.FiltersChanged((object) this, EventArgs.Empty);
    }

    public ObservableCollection<IFilter> EditedFilters { get; set; }

    public IEnumerable<Type> AvailableFilterTypes
    {
      get
      {
        Type target = typeof (IFilter);
        return ((IEnumerable<Type>) target.Assembly.GetTypes()).Where<Type>((Func<Type, bool>) (type =>
        {
          if (target.IsAssignableFrom(type))
            return !type.IsAbstract;
          return false;
        }));
      }
    }

    public bool FilterStatement(IFilterableStatementSnapshot statement)
    {
      if (this.AllFilters)
      {
        foreach (IFilter filter in this.Filters.ToArray<IFilter>())
        {
          if (filter.IsActive && filter.IsValid && !filter.IsValidStatement(statement))
            return false;
        }
        return true;
      }
      if (!this.Filters.Any<IFilter>((Func<IFilter, bool>) (x =>
      {
        if (x.IsActive)
          return x.IsValid;
        return false;
      })))
        return true;
      return this.Filters.Any<IFilter>((Func<IFilter, bool>) (x =>
      {
        if (x.IsActive && x.IsValid)
          return x.IsValidStatement(statement);
        return false;
      }));
    }

    public bool FilterSession(IFilterableSessionSnapshot session)
    {
      if (this.AllFilters)
      {
        foreach (IFilter filter in this.Filters.ToArray<IFilter>())
        {
          if (filter.IsActive && filter.IsValid && !filter.IsValidSession(session))
            return false;
        }
        return true;
      }
      if (!this.Filters.Any<IFilter>((Func<IFilter, bool>) (x =>
      {
        if (x.IsActive)
          return x.IsValid;
        return false;
      })))
        return true;
      return this.Filters.Any<IFilter>((Func<IFilter, bool>) (x =>
      {
        if (x.IsActive && x.IsValid)
          return x.IsValidSession(session);
        return false;
      }));
    }

    public void SaveFilters()
    {
      SaveFileDialog saveFileDialog1 = new SaveFileDialog();
      saveFileDialog1.Filter = "Filters Output|*." + Profile.Current.ShortName + "Filters.xml";
      saveFileDialog1.Title = Profile.CurrentProfileDisplayName;
      SaveFileDialog saveFileDialog2 = saveFileDialog1;
      bool? nullable = saveFileDialog2.ShowDialog();
      if ((!nullable.GetValueOrDefault() ? 1 : (!nullable.HasValue ? 1 : 0)) != 0)
        return;
      XmlDocument xmlDocument = new XmlDocument();
      XmlElement element1 = xmlDocument.CreateElement("Filters");
      xmlDocument.AppendChild((XmlNode) element1);
      XmlElement element2 = xmlDocument.CreateElement("AllFilters");
      element2.InnerText = FilterServiceModel.SerializeObject((object) this.AllFilters);
      element1.AppendChild((XmlNode) element2);
      XmlElement element3 = xmlDocument.CreateElement("DataList");
      element1.AppendChild((XmlNode) element3);
      foreach (IFilter editedFilter in (Collection<IFilter>) this.EditedFilters)
      {
        XmlElement element4 = xmlDocument.CreateElement("Data");
        string fullName = editedFilter.GetType().FullName;
        string str = FilterServiceModel.SerializeObject((object) editedFilter);
        XmlElement element5 = xmlDocument.CreateElement("Type");
        element5.InnerText = fullName;
        element4.AppendChild((XmlNode) element5);
        XmlElement element6 = xmlDocument.CreateElement("Xml");
        element6.InnerText = str;
        element4.AppendChild((XmlNode) element6);
        element3.AppendChild((XmlNode) element4);
      }
      xmlDocument.Save(saveFileDialog2.FileName);
      this.trackingService.Track("Filtering", "LoadFilters", saveFileDialog2.FileName, new int?());
    }

    public void LoadFilters()
    {
      OpenFileDialog openFileDialog1 = new OpenFileDialog();
      openFileDialog1.Filter = "Filters Output|*." + Profile.Current.ShortName + "Filters.xml";
      openFileDialog1.Title = Profile.CurrentProfileDisplayName;
      OpenFileDialog openFileDialog2 = openFileDialog1;
      bool? nullable = openFileDialog2.ShowDialog();
      if ((!nullable.GetValueOrDefault() ? 1 : (!nullable.HasValue ? 1 : 0)) != 0)
        return;
      try
      {
        XmlDocument xmlDocument = new XmlDocument();
        xmlDocument.Load(openFileDialog2.FileName);
        XmlNode xmlNode = xmlDocument.SelectSingleNode(".//AllFilters");
        if (xmlNode != null)
          this.AllFilters = (bool) FilterServiceModel.XmlDeserializeFromString(xmlNode.InnerText, typeof (bool));
        List<Tuple<Type, string>> source = new List<Tuple<Type, string>>();
        foreach (XmlNode selectNode in xmlDocument.SelectSingleNode(".//DataList").SelectNodes("Data"))
        {
          Tuple<Type, string> tuple = new Tuple<Type, string>(Type.GetType(selectNode.SelectSingleNode("Type").InnerText), selectNode.SelectSingleNode("Xml").InnerText);
          source.Add(tuple);
        }
        List<IFilter> list = source.Where<Tuple<Type, string>>((Func<Tuple<Type, string>, bool>) (x => !string.IsNullOrEmpty(x.Item2))).Select<Tuple<Type, string>, IFilter>((Func<Tuple<Type, string>, IFilter>) (x => (IFilter) FilterServiceModel.XmlDeserializeFromString(x.Item2, x.Item1))).ToList<IFilter>();
        this.EditedFilters.Clear();
        list.ForEach((Action<IFilter>) (x => this.EditedFilters.Add(x)));
      }
      catch (Exception ex)
      {
        this.trackingService.Track("Filtering", "Error", ex.Message, new int?());
        throw;
      }
      this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.CanClearEditedFilters));
      this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.CanSaveFilters));
      this.trackingService.Track("Filtering", nameof (LoadFilters), openFileDialog2.FileName, new int?());
    }

    public void Apply()
    {
      this.Filters.Clear();
      this.EditedFilters.ToList<IFilter>().ForEach((Action<IFilter>) (x =>
      {
        this.Filters.Add(x);
        this.trackingService.Track("Filtering", "Usage", x.GetType().Name.MakeStatementFromPascalCase(), new int?());
      }));
    }

    public static string SerializeObject(object toSerialize)
    {
      using (StringWriter stringWriter = new StringWriter())
      {
        new XmlSerializer(toSerialize.GetType()).Serialize((TextWriter) stringWriter, toSerialize);
        return stringWriter.ToString();
      }
    }

    public static object XmlDeserializeFromString(string objectData, Type type)
    {
      using (TextReader textReader = (TextReader) new StringReader(objectData))
        return new XmlSerializer(type).Deserialize(textReader);
    }

    public void PersistFilters()
    {
      UserPreferencesHolder.UserSettings.Filters.Clear();
      UserPreferencesHolder.UserSettings.Filters.AddRange((IEnumerable<IFilter>) this.Filters);
      UserPreferencesHolder.Save();
      this.RaisedFiltersChanged();
    }

    public void AddFilter(Type filterType)
    {
      if (filterType == (Type) null)
        return;
      this.EditedFilters.Add((IFilter) Activator.CreateInstance(filterType));
      this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.CanClearEditedFilters));
      this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.CanSaveFilters));
    }

    public void RemoveFilter(IFilter filter)
    {
      if (filter == null)
        return;
      this.EditedFilters.Remove(filter);
      this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.CanClearEditedFilters));
      this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.CanSaveFilters));
    }

    public void ClearEditedFilters()
    {
      this.EditedFilters.Clear();
      this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.CanClearEditedFilters));
      this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.CanSaveFilters));
    }

    public void ClearFilters()
    {
      this.Filters.Clear();
      this.PersistFilters();
    }

    public bool CanClearEditedFilters
    {
      get
      {
        return this.EditedFilters.Count > 0;
      }
    }

    public bool CanSaveFilters
    {
      get
      {
        return this.EditedFilters.Count > 0;
      }
    }

    public void EditFilters()
    {
      this.trackingService.Track("Filtering", "Edit", (string) null, new int?());
      Dialog.Show((object) this, (object) "Edit", false);
    }
  }
}
