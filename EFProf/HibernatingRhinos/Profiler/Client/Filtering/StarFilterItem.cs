﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Filtering.StarFilterItem
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using System;
using System.Linq.Expressions;

namespace HibernatingRhinos.Profiler.Client.Filtering
{
  [Serializable]
  public class StarFilterItem : PropertyChangedBase
  {
    private bool isActive;

    public StarFilterItem()
    {
    }

    public StarFilterItem(string colorName, bool isActive)
    {
      this.ColorName = colorName;
      this.IsActive = isActive;
    }

    public string ColorName { get; set; }

    public bool IsActive
    {
      get
      {
        return this.isActive;
      }
      set
      {
        this.isActive = value;
        this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.IsActive));
      }
    }
  }
}
