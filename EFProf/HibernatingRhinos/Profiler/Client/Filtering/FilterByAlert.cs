﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Filtering.FilterByAlert
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using HibernatingRhinos.Profiler.Client.CaliburnInfrastructure;
using HibernatingRhinos.Profiler.Client.Filtering.Filters;
using HibernatingRhinos.Profiler.Client.Sessions;
using HibernatingRhinos.Profiler.Client.Statements;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;

namespace HibernatingRhinos.Profiler.Client.Filtering
{
  [Description("Alerts")]
  [View(typeof (FilterByAlertView))]
  [Serializable]
  public class FilterByAlert : AbstractFilter
  {
    private AlertFilterItem[] alerts;

    public FilterByAlert()
    {
      this.Alerts = new AlertFilterItem[3]
      {
        new AlertFilterItem()
        {
          IsActive = true,
          Severity = Severity.Suggestion
        },
        new AlertFilterItem()
        {
          IsActive = true,
          Severity = Severity.Warning
        },
        new AlertFilterItem()
        {
          IsActive = true,
          Severity = Severity.Major
        }
      };
    }

    public AlertFilterItem[] Alerts
    {
      get
      {
        return this.alerts;
      }
      set
      {
        this.alerts = value;
        if (this.alerts == null)
          return;
        ((IEnumerable<AlertFilterItem>) this.alerts).Apply<AlertFilterItem>((System.Action<AlertFilterItem>) (x => x.PropertyChanged += new PropertyChangedEventHandler(this.Alert_PropertyChanged)));
      }
    }

    private void Alert_PropertyChanged(object sender, PropertyChangedEventArgs e)
    {
      this.NotifyOfPropertyChange<string>((Expression<Func<string>>) (() => this.ToolTip));
      this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.IsValid));
    }

    public override bool IsValid
    {
      get
      {
        return ((IEnumerable<AlertFilterItem>) this.alerts).Any<AlertFilterItem>((Func<AlertFilterItem, bool>) (x => x.IsActive));
      }
    }

    public override string ToolTip
    {
      get
      {
        return "Sessions contains " + ((IEnumerable<AlertFilterItem>) this.Alerts).Where<AlertFilterItem>((Func<AlertFilterItem, bool>) (x => x.IsActive)).Select<AlertFilterItem, string>((Func<AlertFilterItem, string>) (x => x.Severity.ToString())).Aggregate<string>((Func<string, string, string>) ((c, n) => c + ", " + n)) + ", alerts";
      }
    }

    public override bool IsValidSession(IFilterableSessionSnapshot snapshot)
    {
      SessionModel sessionModel = snapshot as SessionModel;
      if (sessionModel != null && sessionModel.Session.Statements.Any<Statement>((Func<Statement, bool>) (x => x.Alerts.Any<AlertInformation>())))
        return ((IEnumerable<AlertFilterItem>) this.Alerts).Where<AlertFilterItem>((Func<AlertFilterItem, bool>) (alertFilterItem => alertFilterItem.IsActive)).Any<AlertFilterItem>((Func<AlertFilterItem, bool>) (activeItem => sessionModel.Session.Statements.Any<Statement>((Func<Statement, bool>) (x => x.Alerts.Any<AlertInformation>((Func<AlertInformation, bool>) (a => a.Severity == activeItem.Severity))))));
      return false;
    }

    public override bool IsValidStatement(IFilterableStatementSnapshot snapshot)
    {
      StatementModel statementModel = snapshot as StatementModel;
      if (statementModel == null || !statementModel.Alerts.Any<StatementAlert>())
        return false;
      IEnumerable<AlertFilterItem> activeItems = ((IEnumerable<AlertFilterItem>) this.Alerts).Where<AlertFilterItem>((Func<AlertFilterItem, bool>) (alertFilterItem => alertFilterItem.IsActive));
      return statementModel.Alerts.Any<StatementAlert>((Func<StatementAlert, bool>) (x => activeItems.Any<AlertFilterItem>((Func<AlertFilterItem, bool>) (a => a.Severity == x.Severity))));
    }
  }
}
