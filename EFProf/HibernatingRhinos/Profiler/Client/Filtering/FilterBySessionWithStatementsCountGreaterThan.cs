﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Filtering.FilterBySessionWithStatementsCountGreaterThan
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.Client.CaliburnInfrastructure;
using HibernatingRhinos.Profiler.Client.Filtering.Filters;
using HibernatingRhinos.Profiler.Client.Sessions;
using System;
using System.ComponentModel;
using System.Linq.Expressions;

namespace HibernatingRhinos.Profiler.Client.Filtering
{
  [View(typeof (FilterBySessionWithStatementsCountGreaterThanView))]
  [Description("Session With Statement Count and greater")]
  [Serializable]
  public class FilterBySessionWithStatementsCountGreaterThan : AbstractFilter
  {
    private bool isDatabase = true;
    private bool isCache;
    private int value;

    public bool IsCache
    {
      get
      {
        return this.isCache;
      }
      set
      {
        this.isCache = value;
        this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.IsCache));
        this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.IsValid));
        this.NotifyOfPropertyChange<string>((Expression<Func<string>>) (() => this.ToolTip));
      }
    }

    public bool IsDatabase
    {
      get
      {
        return this.isDatabase;
      }
      set
      {
        this.isDatabase = value;
        this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.IsDatabase));
        this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.IsValid));
        this.NotifyOfPropertyChange<string>((Expression<Func<string>>) (() => this.ToolTip));
      }
    }

    public virtual int Value
    {
      get
      {
        return this.value;
      }
      set
      {
        this.value = value;
        this.NotifyOfPropertyChange<int>((Expression<Func<int>>) (() => this.Value));
        this.NotifyOfPropertyChange<bool>((Expression<Func<bool>>) (() => this.IsValid));
        this.NotifyOfPropertyChange<string>((Expression<Func<string>>) (() => this.ToolTip));
      }
    }

    public override bool IsValid
    {
      get
      {
        if (this.Value <= 0)
          return false;
        if (!this.IsDatabase)
          return this.IsCache;
        return true;
      }
    }

    public override string ToolTip
    {
      get
      {
        return string.Format("Sessions that contain {0} or more statements of type, {1}.", (object) this.Value, (object) ((this.IsDatabase ? "Database statements" : "") + (!this.IsDatabase || !this.IsCache ? "" : " and ") + (this.IsCache ? "Second level cache statements" : "")));
      }
    }

    public override bool IsValidSession(IFilterableSessionSnapshot snapshot)
    {
      SessionModel sessionModel = snapshot as SessionModel;
      if (sessionModel != null)
        return this.Value <= (this.IsCache ? sessionModel.Session.CountOfCachedStatements : 0) + (this.IsDatabase ? sessionModel.Session.CountOfStandardDatabaseStatements : 0);
      return true;
    }

    public override bool IsValidStatement(IFilterableStatementSnapshot snapshot)
    {
      if (snapshot == null)
        return true;
      if (snapshot.IsTransaction)
        return false;
      if (snapshot.IsCached == this.IsCache)
        return true;
      if (this.IsDatabase)
        return !snapshot.IsCached;
      return false;
    }
  }
}
