﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Filtering.FilterBySessionWithStatements
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.Client.CaliburnInfrastructure;
using HibernatingRhinos.Profiler.Client.Filtering.Filters;
using System;
using System.ComponentModel;

namespace HibernatingRhinos.Profiler.Client.Filtering
{
  [Description("[[sessions]] with statements")]
  [View(typeof (FilterBySessionWithStatementsView))]
  [Serializable]
  public class FilterBySessionWithStatements : AbstractFilter
  {
    public override bool IsValid
    {
      get
      {
        return true;
      }
    }

    public override string ToolTip
    {
      get
      {
        return "[[sessions]] with statements.";
      }
    }

    public override bool IsValidSession(IFilterableSessionSnapshot snapshot)
    {
      return snapshot.StatementCount > 0;
    }

    public override bool IsValidStatement(IFilterableStatementSnapshot snapshot)
    {
      if (snapshot.SessionStatementCount == -1)
        return true;
      return snapshot.SessionStatementCount > 0;
    }
  }
}
