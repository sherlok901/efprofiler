﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Filtering.FilterByStarredSessions
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using Caliburn.Micro;
using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.BackEnd.Reports;
using HibernatingRhinos.Profiler.Client.CaliburnInfrastructure;
using HibernatingRhinos.Profiler.Client.Filtering.Filters;
using HibernatingRhinos.Profiler.Client.Sessions;
using HibernatingRhinos.Profiler.Client.Shell;
using HibernatingRhinos.Profiler.Client.Statements;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace HibernatingRhinos.Profiler.Client.Filtering
{
  [Description("Starred [[sessions]]")]
  [View(typeof (FilterByStarredSessionsView))]
  [Serializable]
  public class FilterByStarredSessions : AbstractFilter
  {
    private ApplicationModel applicationModel;

    public FilterByStarredSessions(ApplicationModel model)
    {
      this.applicationModel = model;
      this.Stars = new StarFilterItem[6]
      {
        new StarFilterItem("Yellow", true),
        new StarFilterItem("Orange", true),
        new StarFilterItem("Red", true),
        new StarFilterItem("Green", true),
        new StarFilterItem("Blue", true),
        new StarFilterItem("Gray", true)
      };
    }

    public FilterByStarredSessions()
      : this(IoC.Get<ApplicationModel>((string) null))
    {
    }

    public StarFilterItem[] Stars { get; set; }

    public override bool IsValid
    {
      get
      {
        return true;
      }
    }

    public override string ToolTip
    {
      get
      {
        StringBuilder stringBuilder = new StringBuilder("Starred [[sessions]]: ");
        IEnumerable<StarFilterItem> source = ((IEnumerable<StarFilterItem>) this.Stars).Where<StarFilterItem>((Func<StarFilterItem, bool>) (starFilterItem => starFilterItem.IsActive));
        if (source.Count<StarFilterItem>() == ((IEnumerable<StarFilterItem>) this.Stars).Count<StarFilterItem>())
        {
          stringBuilder.Append("all");
        }
        else
        {
          foreach (StarFilterItem starFilterItem in source)
            stringBuilder.Append(starFilterItem.ColorName.ToLowerInvariant()).Append(", ");
          stringBuilder.Remove(stringBuilder.Length - 2, 1);
        }
        return stringBuilder.ToString();
      }
    }

    public override bool IsValidSession(IFilterableSessionSnapshot snapshot)
    {
      SessionModel sessionModel = snapshot as SessionModel;
      if (sessionModel != null)
        return ((IEnumerable<StarFilterItem>) this.Stars).Where<StarFilterItem>((Func<StarFilterItem, bool>) (starFilterItem => starFilterItem.IsActive)).Any<StarFilterItem>((Func<StarFilterItem, bool>) (activeItem =>
        {
          if (sessionModel.StarColor != null)
            return sessionModel.StarColor == activeItem.ColorName;
          return false;
        }));
      return true;
    }

    public override bool IsValidStatement(IFilterableStatementSnapshot snapshot)
    {
      StatementModel statementModel = snapshot as StatementModel;
      if (statementModel != null)
      {
        IFilterableSessionSnapshot snapshot1 = this.applicationModel.Navigator.Sessions.Items.FirstOrDefault<ISessionItem>((Func<ISessionItem, bool>) (x => x.Id.Equals(statementModel.SessionId))) as IFilterableSessionSnapshot;
        if (snapshot1 != null)
          return this.IsValidSession(snapshot1);
        return true;
      }
      HibernatingRhinos.Profiler.Client.Reports.QueryAggregation queryAggregation = snapshot as HibernatingRhinos.Profiler.Client.Reports.QueryAggregation;
      if (queryAggregation != null)
        return queryAggregation.RelatedSessions.Select<RelatedSessionSpecification, ISessionItem>((Func<RelatedSessionSpecification, ISessionItem>) (rss => this.applicationModel.Navigator.Sessions.Items.FirstOrDefault<ISessionItem>((Func<ISessionItem, bool>) (x => x.Id.Equals(rss.SessionId))))).Where<ISessionItem>((Func<ISessionItem, bool>) (sessionModel => sessionModel != null)).ToList<ISessionItem>().Where<ISessionItem>((Func<ISessionItem, bool>) (x => x is IFilterableSessionSnapshot)).Cast<IFilterableSessionSnapshot>().Select<IFilterableSessionSnapshot, bool>(new Func<IFilterableSessionSnapshot, bool>(((AbstractFilter) this).IsValidSession)).Any<bool>((Func<bool, bool>) (result => result));
      return true;
    }
  }
}
