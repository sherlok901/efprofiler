﻿// Decompiled with JetBrains decompiler
// Type: Rhino.Licensing.AbstractLicenseValidator
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using HibernatingRhinos.Profiler.BackEnd.Profiles;
using log4net;
using Rhino.Licensing.Discovery;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Security;
using System.Security.Cryptography;
using System.Security.Cryptography.Xml;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Xml;

namespace Rhino.Licensing
{
  public abstract class AbstractLicenseValidator
  {
    protected static readonly ILog Logger = LogManager.GetLogger(typeof (AbstractLicenseValidator));
    protected readonly string[] TimeServers = new string[12]
    {
      "time.nist.gov",
      "time-nw.nist.gov",
      "time-a.nist.gov",
      "time-b.nist.gov",
      "time-a.timefreq.bldrdoc.gov",
      "time-b.timefreq.bldrdoc.gov",
      "time-c.timefreq.bldrdoc.gov",
      "utcnist.colorado.edu",
      "nist1.datum.com",
      "nist1.dc.certifiedtime.com",
      "nist1.nyc.certifiedtime.com",
      "nist1.sjc.certifiedtime.com"
    };
    private readonly Guid senderId = Guid.NewGuid();
    private bool licenseInfoLogged;
    private readonly string licenseServerUrl;
    private readonly Guid clientId;
    private readonly string publicKey;
    private readonly System.Threading.Timer nextLeaseTimer;
    private bool disableFutureChecks;
    private bool currentlyValidatingLicense;
    private readonly DiscoveryHost discoveryHost;
    private DiscoveryClient discoveryClient;
    private string explicitError;

    public event Action<InvalidationType> LicenseInvalidated;

    public DateTime ExpirationDate { get; private set; }

    public DateTime ExpirationDateFloatingLicense { get; set; }

    public AbstractLicenseValidator.MultipleLicenseUsage MultipleLicenseUsageBehavior { get; set; }

    public string SubscriptionEndpoint { get; set; }

    public LicenseType LicenseType { get; private set; }

    public Guid UserId { get; private set; }

    public string Name { get; private set; }

    public bool DisableFloatingLicenses { get; set; }

    public IDictionary<string, string> LicenseAttributes { get; private set; }

    protected abstract string License { get; set; }

    private void LeaseLicenseAgain(object state)
    {
      DiscoveryClient discoveryClient = this.discoveryClient;
      if (discoveryClient != null)
        discoveryClient.PublishMyPresence();
      try
      {
        if (this.IsLicenseValid())
          return;
      }
      catch (RhinoLicensingException ex1)
      {
        try
        {
          this.RaiseLicenseInvalidated();
          return;
        }
        catch (InvalidOperationException ex2)
        {
          return;
        }
      }
      this.RaiseLicenseInvalidated();
    }

    private void RaiseLicenseInvalidated()
    {
      Action<InvalidationType> licenseInvalidated = this.LicenseInvalidated;
      if (licenseInvalidated == null)
        throw new InvalidOperationException("License was invalidated, but there is no one subscribe to the LicenseInvalidated event");
      licenseInvalidated(this.LicenseType == LicenseType.Floating ? InvalidationType.CannotGetNewLicense : InvalidationType.TimeExpired);
    }

    protected AbstractLicenseValidator(string publicKey)
    {
      this.LeaseTimeout = TimeSpan.FromMinutes(5.0);
      this.LicenseAttributes = (IDictionary<string, string>) new Dictionary<string, string>();
      this.nextLeaseTimer = new System.Threading.Timer(new TimerCallback(this.LeaseLicenseAgain));
      this.publicKey = publicKey;
      this.discoveryHost = new DiscoveryHost();
      this.discoveryHost.ClientDiscovered += new EventHandler<DiscoveryHost.ClientDiscoveredEventArgs>(this.DiscoveryHostOnClientDiscovered);
    }

    private void DiscoveryHostOnClientDiscovered(object sender, DiscoveryHost.ClientDiscoveredEventArgs clientDiscoveredEventArgs)
    {
      if (this.senderId == clientDiscoveredEventArgs.SenderId || this.UserId != clientDiscoveredEventArgs.UserId)
        return;
      switch (this.MultipleLicenseUsageBehavior)
      {
        case AbstractLicenseValidator.MultipleLicenseUsage.Deny:
          if (Environment.UserName == clientDiscoveredEventArgs.UserName && Environment.MachineName == clientDiscoveredEventArgs.MachineName)
            return;
          break;
        case AbstractLicenseValidator.MultipleLicenseUsage.AllowForSameUser:
          if (Environment.UserName == clientDiscoveredEventArgs.UserName)
            return;
          break;
        case AbstractLicenseValidator.MultipleLicenseUsage.AllowSameLicense:
          return;
        default:
          throw new ArgumentOutOfRangeException("invalid MultipleLicenseUsageBehavior: " + (object) this.MultipleLicenseUsageBehavior);
      }
      DiscoveryClient discoveryClient = this.discoveryClient;
      if (discoveryClient != null)
        discoveryClient.PublishMyPresence();
      this.RaiseMultipleLicensesWereDiscovered(clientDiscoveredEventArgs);
    }

    private void RaiseMultipleLicensesWereDiscovered(DiscoveryHost.ClientDiscoveredEventArgs clientDiscoveredEventArgs)
    {
      EventHandler<DiscoveryHost.ClientDiscoveredEventArgs> licensesWereDiscovered = this.MultipleLicensesWereDiscovered;
      if (licensesWereDiscovered == null)
        throw new InvalidOperationException("Multiple licenses were discovered, but no one is handling the MultipleLicensesWereDiscovered event");
      licensesWereDiscovered((object) this, clientDiscoveredEventArgs);
    }

    public event EventHandler<DiscoveryHost.ClientDiscoveredEventArgs> MultipleLicensesWereDiscovered;

    protected AbstractLicenseValidator(string publicKey, string licenseServerUrl, Guid clientId)
      : this(publicKey)
    {
      this.licenseServerUrl = licenseServerUrl;
      this.clientId = clientId;
    }

    public virtual void AssertValidLicense()
    {
      this.AssertValidLicense((Action) (() => {}));
    }

    public virtual void AssertValidLicense(Action onValidLicense)
    {
      this.LicenseAttributes.Clear();
      if (this.IsLicenseValid())
      {
        onValidLicense();
        if (this.MultipleLicenseUsageBehavior == AbstractLicenseValidator.MultipleLicenseUsage.AllowSameLicense)
          return;
        try
        {
          this.discoveryHost.Start();
        }
        catch (Exception ex)
        {
          AbstractLicenseValidator.Logger.Error((object) "Could not setup node discovery", ex);
        }
        try
        {
          this.discoveryClient = new DiscoveryClient(this.senderId, this.UserId, Environment.MachineName, Environment.UserName);
          this.discoveryClient.PublishMyPresence();
        }
        catch (SecurityException ex)
        {
          AbstractLicenseValidator.Logger.Error((object) "Could not publish application's presence", (Exception) ex);
        }
      }
      else
      {
        if (this.explicitError != null)
          throw new InvalidOperationException(this.explicitError);
        AbstractLicenseValidator.Logger.WarnFormat("Could not validate existing license\r\n{0}", (object) this.License);
        throw new LicenseNotFoundException("Could not find a valid license.");
      }
    }

    private bool IsLicenseValid()
    {
      try
      {
        if (!this.TryLoadingLicenseValuesFromValidatedXml())
        {
          AbstractLicenseValidator.Logger.WarnFormat("Failed validating license:\r\n{0}", (object) this.License);
          return false;
        }
        if (!this.licenseInfoLogged)
        {
          AbstractLicenseValidator.Logger.WarnFormat("License expiration date is {0}", (object) this.ExpirationDate);
          this.licenseInfoLogged = true;
        }
        bool flag;
        if (this.LicenseType == LicenseType.Floating)
          flag = true;
        else if (this.LicenseType == LicenseType.Subscription)
        {
          flag = this.ValidateLicense();
        }
        else
        {
          flag = DateTime.UtcNow < this.ExpirationDate;
          if (flag)
            flag = this.ValidateLicense();
        }
        if (!flag)
          throw new LicenseExpiredException("Expiration Date: " + (object) this.ExpirationDate);
        this.ValidateUsingNetworkTime();
        return true;
      }
      catch (RhinoLicensingException ex)
      {
        throw;
      }
      catch (Exception ex)
      {
        FileLoadException fileLoadException = ex as FileLoadException;
        if (fileLoadException != null && fileLoadException.FileName.StartsWith("System.Threading.Tasks"))
        {
          this.explicitError = "Please upgrade to the latest version of the profiler by downloading it from: http://hibernatingrhinos.com/builds/uber-prof-v2";
          AbstractLicenseValidator.Logger.Debug((object) this.explicitError);
        }
        else
          AbstractLicenseValidator.Logger.Debug((object) "Failed to accept the license.", ex);
        return false;
      }
    }

    private bool ValidateLicense()
    {
      if ((this.ExpirationDate - DateTime.UtcNow).TotalDays > 4.0)
        return true;
      if (this.currentlyValidatingLicense)
        return DateTime.UtcNow < this.ExpirationDate;
      if (this.SubscriptionEndpoint == null)
        throw new InvalidOperationException("Subscription endpoints are not supported for this license validator");
      try
      {
        if (this.LicenseType != LicenseType.Trial)
          this.TryGettingNewLeaseSubscription();
      }
      catch (ProtocolException ex)
      {
        AbstractLicenseValidator.Logger.Error((object) "Could not re-lease subscription license. ProtocolException have been thrown.", (Exception) ex);
        WebException innerException = ex.InnerException as WebException;
        if (innerException != null)
        {
          AbstractLicenseValidator.Logger.Info((object) string.Format("Could not re-lease subscription license. Response status is: '{0}'", (object) innerException.Status), (Exception) ex);
          if (innerException.Status != WebExceptionStatus.ProtocolError && innerException.Status != WebExceptionStatus.ProxyNameResolutionFailure)
          {
            if (innerException.Status != WebExceptionStatus.RequestProhibitedByProxy)
              goto label_13;
          }
          string messageBoxText = string.Format("It seems that you're using a proxy. You need to configure the proxy setting in the {0}.exe.config file using the <defaultProxy> element.", (object) Profile.CurrentProfile);
          AbstractLicenseValidator.Logger.Info((object) messageBoxText);
          int num = (int) MessageBox.Show(messageBoxText);
        }
      }
      catch (Exception ex)
      {
        AbstractLicenseValidator.Logger.Error((object) "Could not re-lease subscription license", ex);
      }
label_13:
      return this.ValidateWithoutUsingSubscriptionLeasing();
    }

    private bool ValidateWithoutUsingSubscriptionLeasing()
    {
      this.currentlyValidatingLicense = true;
      try
      {
        return this.IsLicenseValid();
      }
      finally
      {
        this.currentlyValidatingLicense = false;
      }
    }

    private void TryGettingNewLeaseSubscription()
    {
      ISubscriptionLicensingService channel = ChannelFactory<ISubscriptionLicensingService>.CreateChannel((Binding) new BasicHttpBinding(), new EndpointAddress(this.SubscriptionEndpoint));
      try
      {
        this.TryOverwritingWithNewLicense(channel.LeaseLicense(this.License));
      }
      catch (FaultException ex)
      {
        string message = ex.Message;
        if (message.StartsWith("The order has been cancelled"))
          throw new LicenseExpiredException(message);
        if (message.StartsWith("Invalid license"))
          AbstractLicenseValidator.Logger.Debug((object) "Got invalid license", (Exception) ex);
        else
          AbstractLicenseValidator.Logger.Debug((object) "Got Fault Exception", (Exception) ex);
      }
      catch (ProtocolException ex)
      {
        AbstractLicenseValidator.Logger.Error((object) "Could not re-lease subscription license. See details.", (Exception) ex);
        WebException innerException = ex.InnerException as WebException;
        if (innerException == null)
          return;
        AbstractLicenseValidator.Logger.Info((object) string.Format("Could not re-lease subscription license. Response status is: '{0}'", (object) innerException.Status), (Exception) ex);
        if (innerException.Status != WebExceptionStatus.ProtocolError && innerException.Status != WebExceptionStatus.ProxyNameResolutionFailure && innerException.Status != WebExceptionStatus.RequestProhibitedByProxy)
          return;
        string messageBoxText = string.Format("It seems that you're using a proxy. You need to configure the proxy setting in the {0}.exe.config file using the <defaultProxy> element.", (object) Profile.CurrentProfile);
        AbstractLicenseValidator.Logger.Info((object) messageBoxText);
        int num = (int) MessageBox.Show(messageBoxText);
      }
      catch (Exception ex)
      {
        AbstractLicenseValidator.Logger.Error((object) "Could not re-lease subscription license", ex);
      }
      finally
      {
        ICommunicationObject communicationObject = channel as ICommunicationObject;
        if (communicationObject != null)
        {
          try
          {
            communicationObject.Close(TimeSpan.FromMilliseconds(200.0));
          }
          catch
          {
            communicationObject.Abort();
          }
        }
      }
    }

    protected bool TryOverwritingWithNewLicense(string newLicense)
    {
      if (string.IsNullOrEmpty(newLicense))
        return false;
      try
      {
        new XmlDocument().LoadXml(newLicense);
      }
      catch (Exception ex)
      {
        AbstractLicenseValidator.Logger.Error((object) ("New license is not valid XML\r\n" + newLicense), ex);
        return false;
      }
      this.License = newLicense;
      return true;
    }

    private void ValidateUsingNetworkTime()
    {
      if (this.LicenseType == LicenseType.Standard && this.ExpirationDate == DateTime.MaxValue || !NetworkInterface.GetIsNetworkAvailable())
        return;
      SntpClient sntpClient = new SntpClient(this.TimeServers);
      try
      {
        sntpClient.GetDateAsync().ContinueWith((Action<Task<DateTime>>) (task =>
        {
          if (!(task.Result > this.ExpirationDate))
            return;
          this.RaiseLicenseInvalidated();
        }));
      }
      catch (FileLoadException ex)
      {
        if (ex.Message.Contains("System.Core, Version=2.0.5.0"))
          throw new InvalidOperationException("You need to install the following update to .NET 4.0: http://support.microsoft.com/kb/2468871/en-us", (Exception) ex);
        throw;
      }
    }

    public virtual void RemoveExistingLicense()
    {
      this.discoveryHost.Stop();
    }

    public bool TryLoadingLicenseValuesFromValidatedXml()
    {
      try
      {
        XmlDocument doc = new XmlDocument();
        try
        {
          doc.LoadXml(this.License.Trim());
        }
        catch (Exception ex)
        {
          throw new CorruptLicenseFileException("Could not understand the license, it isn't a valid XML file", ex);
        }
        if (!this.TryGetValidDocument(this.publicKey, doc))
        {
          AbstractLicenseValidator.Logger.WarnFormat("Could not validate xml signature of:\r\n{0}", (object) this.License);
          return false;
        }
        if (doc.FirstChild == null)
        {
          AbstractLicenseValidator.Logger.WarnFormat("Could not find first child of:\r\n{0}", (object) this.License);
          return false;
        }
        if (doc.SelectSingleNode("/floating-license") != null)
        {
          XmlNode xmlNode = doc.SelectSingleNode("/floating-license/license-server-public-key/text()");
          if (xmlNode == null)
          {
            AbstractLicenseValidator.Logger.WarnFormat("Invalid license, floating license without license server public key:\r\n{0}", (object) this.License);
            throw new InvalidOperationException("Invalid license file format, floating license without license server public key");
          }
          return this.ValidateFloatingLicense(xmlNode.InnerText);
        }
        bool flag = this.ValidateXmlDocumentLicense(doc);
        if (flag && !this.disableFutureChecks)
          this.nextLeaseTimer.Change(this.LeaseTimeout, this.LeaseTimeout);
        return flag;
      }
      catch (RhinoLicensingException ex)
      {
        throw;
      }
      catch (Exception ex)
      {
        AbstractLicenseValidator.Logger.Error((object) "Could not validate license", ex);
        return false;
      }
    }

    public TimeSpan LeaseTimeout { get; set; }

    private bool ValidateFloatingLicense(string publicKeyOfFloatingLicense)
    {
      if (this.DisableFloatingLicenses)
      {
        AbstractLicenseValidator.Logger.Warn((object) "Floating licenses have been disabled");
        return false;
      }
      if (this.licenseServerUrl == null)
      {
        AbstractLicenseValidator.Logger.Warn((object) "Could not find license server url");
        throw new InvalidOperationException("Floating license encountered, but licenseServerUrl was not set");
      }
      bool flag1 = false;
      AbstractLicenseValidator.Logger.Info((object) ("Creating endpoint address for: " + this.licenseServerUrl));
      EndpointAddress endpointAddress;
      try
      {
        endpointAddress = new EndpointAddress(this.licenseServerUrl);
      }
      catch (Exception ex)
      {
        AbstractLicenseValidator.Logger.Warn((object) ("Could not create endpoint address for: " + this.licenseServerUrl), ex);
        throw;
      }
      ILicensingService channel = ChannelFactory<ILicensingService>.CreateChannel((Binding) new BasicHttpBinding(BasicHttpSecurityMode.None), endpointAddress);
      try
      {
        string xml = channel.LeaseLicense(Environment.MachineName, Environment.UserName, this.clientId);
        ((ICommunicationObject) channel).Close();
        flag1 = true;
        if (xml == null)
        {
          AbstractLicenseValidator.Logger.WarnFormat("Got an empty response from the license server: {0}", (object) this.licenseServerUrl);
          throw new FloatingLicenseNotAvialableException();
        }
        XmlDocument doc = new XmlDocument();
        doc.LoadXml(xml);
        if (!this.TryGetValidDocument(publicKeyOfFloatingLicense, doc))
        {
          AbstractLicenseValidator.Logger.WarnFormat("Could not get valid license from floating license server {0}", (object) this.licenseServerUrl);
          throw new FloatingLicenseNotAvialableException();
        }
        bool flag2 = this.ValidateXmlDocumentLicense(doc);
        if (flag2)
        {
          TimeSpan timeSpan = this.ExpirationDate.AddMinutes(-5.0) - DateTime.UtcNow;
          AbstractLicenseValidator.Logger.DebugFormat("Will lease license again at {0}", (object) timeSpan);
          if (!this.disableFutureChecks)
            this.nextLeaseTimer.Change(timeSpan, timeSpan);
        }
        return flag2;
      }
      catch (Exception ex)
      {
        AbstractLicenseValidator.Logger.Warn((object) "Failed to contact the floating license server", ex);
        throw;
      }
      finally
      {
        if (!flag1)
          ((ICommunicationObject) channel).Abort();
      }
    }

    internal bool ValidateXmlDocumentLicense(XmlDocument doc)
    {
      XmlNode xmlNode1 = doc.SelectSingleNode("/license/@id");
      if (xmlNode1 == null)
      {
        AbstractLicenseValidator.Logger.WarnFormat("Could not find id attribute in license:\r\n{0}", (object) this.License);
        return false;
      }
      this.UserId = new Guid(xmlNode1.Value);
      XmlNode xmlNode2 = doc.SelectSingleNode("/license/@expiration");
      if (xmlNode2 == null)
      {
        AbstractLicenseValidator.Logger.WarnFormat("Could not find expiration in license:\r\n{0}", (object) this.License);
        return false;
      }
      this.ExpirationDate = DateTime.ParseExact(xmlNode2.Value, "yyyy-MM-ddTHH:mm:ss.fffffff", (IFormatProvider) CultureInfo.InvariantCulture);
      XmlNode xmlNode3 = doc.SelectSingleNode("/license/@type");
      if (xmlNode3 == null)
      {
        AbstractLicenseValidator.Logger.WarnFormat("Could not find license type in {0}", (object) xmlNode3);
        return false;
      }
      this.LicenseType = (LicenseType) System.Enum.Parse(typeof (LicenseType), xmlNode3.Value);
      XmlNode xmlNode4 = doc.SelectSingleNode("/license/name/text()");
      if (xmlNode4 == null)
      {
        AbstractLicenseValidator.Logger.WarnFormat("Could not find licensee's name in license:\r\n{0}", (object) this.License);
        return false;
      }
      this.Name = xmlNode4.Value;
      foreach (XmlAttribute attribute in (XmlNamedNodeMap) doc.SelectSingleNode("/license").Attributes)
      {
        if (!(attribute.Name == "type") && !(attribute.Name == "expiration") && !(attribute.Name == "id"))
          this.LicenseAttributes[attribute.Name] = attribute.Value;
      }
      return true;
    }

    private bool TryGetValidDocument(string licensePublicKey, XmlDocument doc)
    {
      RSACryptoServiceProvider cryptoServiceProvider = new RSACryptoServiceProvider();
      cryptoServiceProvider.FromXmlString(licensePublicKey);
      XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
      nsmgr.AddNamespace("sig", "http://www.w3.org/2000/09/xmldsig#");
      SignedXml signedXml = new SignedXml(doc);
      XmlElement xmlElement = (XmlElement) doc.SelectSingleNode("//sig:Signature", nsmgr);
      if (xmlElement == null)
      {
        AbstractLicenseValidator.Logger.WarnFormat("Could not find this signature node on license:\r\n{0}", (object) this.License);
        return false;
      }
      signedXml.LoadXml(xmlElement);
      return signedXml.CheckSignature((AsymmetricAlgorithm) cryptoServiceProvider);
    }

    public void DisableFutureChecks()
    {
      this.disableFutureChecks = true;
      this.nextLeaseTimer.Dispose();
    }

    public enum MultipleLicenseUsage
    {
      Deny,
      AllowForSameUser,
      AllowSameLicense,
    }
  }
}
