﻿// Decompiled with JetBrains decompiler
// Type: Rhino.Licensing.SntpClient
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace Rhino.Licensing
{
  public class SntpClient
  {
    private readonly ILog log = LogManager.GetLogger(typeof (SntpClient));
    private int index = -1;
    private const byte SntpDataLength = 48;
    private readonly string[] hosts;

    public SntpClient(string[] hosts)
    {
      this.hosts = hosts;
    }

    private static bool GetIsServerMode(byte[] sntpData)
    {
      return ((int) sntpData[0] & 7) == 4;
    }

    private static DateTime GetTransmitTimestamp(byte[] sntpData)
    {
      return SntpClient.ComputeDate(SntpClient.GetMilliseconds(sntpData, (byte) 40));
    }

    private static DateTime ComputeDate(ulong milliseconds)
    {
      return new DateTime(1900, 1, 1).Add(TimeSpan.FromMilliseconds((double) milliseconds));
    }

    private static ulong GetMilliseconds(byte[] sntpData, byte offset)
    {
      ulong num1 = 0;
      ulong num2 = 0;
      for (int index = 0; index <= 3; ++index)
        num1 = 256UL * num1 + (ulong) sntpData[(int) offset + index];
      for (int index = 4; index <= 7; ++index)
        num2 = 256UL * num2 + (ulong) sntpData[(int) offset + index];
      return num1 * 1000UL + num2 * 1000UL / 4294967296UL;
    }

    public async Task<DateTime> GetDateAsync()
    {
      ++this.index;
      if (this.hosts.Length <= this.index)
      {
        if (this.IsComputerOffline())
          return DateTime.Now;
        throw new InvalidOperationException("After trying out all the hosts, was unable to find anyone that could tell us what the time is");
      }
      string host = this.hosts[this.index];
      try
      {
        IPAddress[] ipAddresses = await Task.Factory.FromAsync<IPAddress[]>((Func<AsyncCallback, object, IAsyncResult>) ((callback, state) => Dns.BeginGetHostAddresses(host, callback, state)), new Func<IAsyncResult, IPAddress[]>(Dns.EndGetHostAddresses), (object) host);
        IPEndPoint endPoint = new IPEndPoint(ipAddresses[0], 123);
        UdpClient socket = new UdpClient();
        socket.Connect(endPoint);
        socket.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, 500);
        socket.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.SendTimeout, 500);
        byte[] sntpData = new byte[48];
        sntpData[0] = (byte) 27;
        try
        {
          int num = await Task.Factory.FromAsync<int>((Func<AsyncCallback, object, IAsyncResult>) ((callback, state) => socket.BeginSend(sntpData, sntpData.Length, callback, state)), new Func<IAsyncResult, int>(socket.EndSend), (object) null);
          try
          {
            byte[] receivedBytes = await Task.Factory.FromAsync<byte[]>(new Func<AsyncCallback, object, IAsyncResult>(socket.BeginReceive), (Func<IAsyncResult, byte[]>) (ar => socket.EndReceive(ar, ref endPoint)), (object) null);
            if (!this.IsResponseValid(receivedBytes))
            {
              this.log.Debug((object) ("Did not get valid time information from " + host));
              return await this.GetDateAsync();
            }
            DateTime transmitTimestamp = SntpClient.GetTransmitTimestamp(receivedBytes);
            return transmitTimestamp;
          }
          catch (SocketException ex)
          {
            this.SafelyCloseSocket(socket);
            this.log.Debug((object) ("Could not get a time response from: " + host), (Exception) ex);
          }
          catch (Exception ex)
          {
            this.SafelyCloseSocket(socket);
            this.log.Debug((object) ("Could not get time response from: " + host), ex);
          }
          return await this.GetDateAsync();
        }
        catch (Exception ex)
        {
          this.SafelyCloseSocket(socket);
          this.log.Debug((object) ("Could not send time request to : " + host), ex);
        }
        return await this.GetDateAsync();
      }
      catch (Exception ex)
      {
        this.log.Debug((object) ("Could not get time from: " + host), ex);
      }
      return await this.GetDateAsync();
    }

    private void SafelyCloseSocket(UdpClient socket)
    {
      try
      {
        socket.Close();
      }
      catch (Exception ex)
      {
      }
    }

    private bool IsComputerOffline()
    {
      string[] strArray = new string[5]
      {
        "https://www.google.com/",
        "http://msdn.microsoft.com/",
        "http://www.w3.org/",
        "https://twitter.com/",
        "https://www.facebook.com/"
      };
      List<WebsiteStatus> source = new List<WebsiteStatus>();
      foreach (string requestUriString in strArray)
      {
        try
        {
          WebRequest.Create(requestUriString).GetResponse();
          source.Add(new WebsiteStatus()
          {
            Website = requestUriString,
            Status = true
          });
        }
        catch (WebException ex)
        {
          if (ex.Status == WebExceptionStatus.NameResolutionFailure)
          {
            source.Add(new WebsiteStatus()
            {
              Website = requestUriString,
              Status = false
            });
          }
          else
          {
            this.log.Error((object) "Failed to check if the computer is online.", (Exception) ex);
            source.Add(new WebsiteStatus()
            {
              Website = requestUriString,
              Status = false,
              Exception = (Exception) ex
            });
          }
        }
        catch (Exception ex)
        {
          this.log.Error((object) "Failed to check if the computer is online.", ex);
          source.Add(new WebsiteStatus()
          {
            Website = requestUriString,
            Status = false,
            Exception = ex
          });
        }
      }
      return source.Select<WebsiteStatus, bool>((Func<WebsiteStatus, bool>) (status => status.Status)).All<bool>((Func<bool, bool>) (statusStatus => !statusStatus));
    }

    private bool IsResponseValid(byte[] sntpData)
    {
      if (sntpData.Length >= 48)
        return SntpClient.GetIsServerMode(sntpData);
      return false;
    }
  }
}
