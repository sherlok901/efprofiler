﻿// Decompiled with JetBrains decompiler
// Type: Rhino.Licensing.ILicensingService
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System;
using System.ServiceModel;

namespace Rhino.Licensing
{
  [ServiceContract(SessionMode = SessionMode.NotAllowed)]
  public interface ILicensingService
  {
    [OperationContract]
    string LeaseLicense(string machine, string user, Guid id);
  }
}
