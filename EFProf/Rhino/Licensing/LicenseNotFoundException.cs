﻿// Decompiled with JetBrains decompiler
// Type: Rhino.Licensing.LicenseNotFoundException
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System;
using System.Runtime.Serialization;

namespace Rhino.Licensing
{
  [Serializable]
  public class LicenseNotFoundException : RhinoLicensingException
  {
    public LicenseNotFoundException()
    {
    }

    public LicenseNotFoundException(string message)
      : base(message)
    {
    }

    public LicenseNotFoundException(string message, Exception inner)
      : base(message, inner)
    {
    }

    protected LicenseNotFoundException(SerializationInfo info, StreamingContext context)
      : base(info, context)
    {
    }
  }
}
