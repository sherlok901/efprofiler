﻿// Decompiled with JetBrains decompiler
// Type: Rhino.Licensing.LicenseGenerator
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Security.Cryptography;
using System.Security.Cryptography.Xml;
using System.Text;
using System.Xml;

namespace Rhino.Licensing
{
  public class LicenseGenerator
  {
    private readonly string privateKey;

    public LicenseGenerator(string privateKey)
    {
      this.privateKey = privateKey;
    }

    public string GenerateFloatingLicense(string name, string publicKey)
    {
      using (RSACryptoServiceProvider cryptoServiceProvider = new RSACryptoServiceProvider())
      {
        cryptoServiceProvider.FromXmlString(this.privateKey);
        XmlDocument x = new XmlDocument();
        XmlElement element1 = x.CreateElement("floating-license");
        x.AppendChild((XmlNode) element1);
        XmlElement element2 = x.CreateElement("license-server-public-key");
        element1.AppendChild((XmlNode) element2);
        element2.InnerText = publicKey;
        XmlElement element3 = x.CreateElement(nameof (name));
        element1.AppendChild((XmlNode) element3);
        element3.InnerText = name;
        XmlElement digitalSignature = LicenseGenerator.GetXmlDigitalSignature(x, (AsymmetricAlgorithm) cryptoServiceProvider);
        x.FirstChild.AppendChild(x.ImportNode((XmlNode) digitalSignature, true));
        MemoryStream memoryStream = new MemoryStream();
        XmlWriter w = XmlWriter.Create((Stream) memoryStream, new XmlWriterSettings()
        {
          Indent = true,
          Encoding = Encoding.UTF8
        });
        x.Save(w);
        memoryStream.Position = 0L;
        return new StreamReader((Stream) memoryStream).ReadToEnd();
      }
    }

    public string Generate(string name, Guid id, DateTime expirationDate, LicenseType licenseType)
    {
      return this.Generate(name, id, expirationDate, (IDictionary<string, string>) new Dictionary<string, string>(), licenseType);
    }

    public string Generate(string name, Guid id, DateTime expirationDate, IDictionary<string, string> attributes, LicenseType licenseType)
    {
      using (RSACryptoServiceProvider cryptoServiceProvider = new RSACryptoServiceProvider())
      {
        cryptoServiceProvider.FromXmlString(this.privateKey);
        XmlDocument document = LicenseGenerator.CreateDocument(id, name, expirationDate, attributes, licenseType);
        XmlElement digitalSignature = LicenseGenerator.GetXmlDigitalSignature(document, (AsymmetricAlgorithm) cryptoServiceProvider);
        document.FirstChild.AppendChild(document.ImportNode((XmlNode) digitalSignature, true));
        MemoryStream memoryStream = new MemoryStream();
        XmlWriter w = XmlWriter.Create((Stream) memoryStream, new XmlWriterSettings()
        {
          Indent = true,
          Encoding = Encoding.UTF8
        });
        document.Save(w);
        memoryStream.Position = 0L;
        return new StreamReader((Stream) memoryStream).ReadToEnd();
      }
    }

    private static XmlElement GetXmlDigitalSignature(XmlDocument x, AsymmetricAlgorithm key)
    {
      SignedXml signedXml = new SignedXml(x)
      {
        SigningKey = key
      };
      Reference reference = new Reference()
      {
        Uri = ""
      };
      reference.AddTransform((Transform) new XmlDsigEnvelopedSignatureTransform());
      signedXml.AddReference(reference);
      signedXml.ComputeSignature();
      return signedXml.GetXml();
    }

    private static XmlDocument CreateDocument(Guid id, string name, DateTime expirationDate, IDictionary<string, string> attributes, LicenseType licenseType)
    {
      XmlDocument xmlDocument = new XmlDocument();
      XmlElement element1 = xmlDocument.CreateElement("license");
      xmlDocument.AppendChild((XmlNode) element1);
      XmlAttribute attribute1 = xmlDocument.CreateAttribute(nameof (id));
      element1.Attributes.Append(attribute1);
      attribute1.Value = id.ToString();
      XmlAttribute attribute2 = xmlDocument.CreateAttribute("expiration");
      element1.Attributes.Append(attribute2);
      attribute2.Value = expirationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffffff", (IFormatProvider) CultureInfo.InvariantCulture);
      XmlAttribute attribute3 = xmlDocument.CreateAttribute("type");
      element1.Attributes.Append(attribute3);
      attribute3.Value = licenseType.ToString();
      XmlElement element2 = xmlDocument.CreateElement(nameof (name));
      element1.AppendChild((XmlNode) element2);
      element2.InnerText = name;
      foreach (KeyValuePair<string, string> attribute4 in (IEnumerable<KeyValuePair<string, string>>) attributes)
      {
        XmlAttribute attribute5 = xmlDocument.CreateAttribute(attribute4.Key);
        attribute5.Value = attribute4.Value;
        element1.Attributes.Append(attribute5);
      }
      return xmlDocument;
    }
  }
}
