﻿// Decompiled with JetBrains decompiler
// Type: Rhino.Licensing.LicensingService
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;

namespace Rhino.Licensing
{
  [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Single, InstanceContextMode = InstanceContextMode.Single)]
  public class LicensingService : ILicensingService
  {
    private readonly List<LicenseValidator> availableLicenses = new List<LicenseValidator>();
    private readonly Dictionary<string, KeyValuePair<DateTime, LicenseValidator>> leasedLicenses = new Dictionary<string, KeyValuePair<DateTime, LicenseValidator>>();
    private readonly string state;

    public LicensingService()
    {
      if (LicensingService.SoftwarePublicKey == null)
        throw new InvalidOperationException("SoftwarePublicKey must be set before starting the service");
      if (LicensingService.LicenseServerPrivateKey == null)
        throw new InvalidOperationException("LicenseServerPrivateKey must be set before starting the service");
      string licensesDirectory = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Licenses");
      this.state = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "licenseServer.state");
      LicensingService.EnsureLicenseDirectoryExists(licensesDirectory);
      this.ReadAvailableLicenses(licensesDirectory);
      this.ReadInitialState();
    }

    public static string SoftwarePublicKey { get; set; }

    public static string LicenseServerPrivateKey { get; set; }

    private static void EnsureLicenseDirectoryExists(string licensesDirectory)
    {
      if (Directory.Exists(licensesDirectory))
        return;
      try
      {
        Directory.CreateDirectory(licensesDirectory);
      }
      catch (Exception ex)
      {
        throw new DirectoryNotFoundException("Could not find licenses directory: " + licensesDirectory, ex);
      }
    }

    private void ReadAvailableLicenses(string licensesDirectory)
    {
      foreach (string file in Directory.GetFiles(licensesDirectory, "*.xml"))
      {
        HashSet<Guid> guidSet = new HashSet<Guid>();
        LicenseValidator licenseValidator1 = new LicenseValidator(LicensingService.SoftwarePublicKey, file);
        licenseValidator1.DisableFloatingLicenses = true;
        LicenseValidator licenseValidator2 = licenseValidator1;
        try
        {
          licenseValidator2.AssertValidLicense();
          if (licenseValidator2.LicenseType != LicenseType.Standard)
          {
            if (licenseValidator2.LicenseType != LicenseType.Subscription)
              continue;
          }
          if (guidSet.Add(licenseValidator2.UserId))
            this.availableLicenses.Add(licenseValidator2);
        }
        catch (Exception ex)
        {
        }
      }
    }

    private void ReadInitialState()
    {
      try
      {
        using (FileStream fileStream = new FileStream(this.state, FileMode.OpenOrCreate, FileAccess.ReadWrite))
          this.ReadState((Stream) fileStream);
      }
      catch (AccessViolationException ex)
      {
        throw new AccessViolationException("Could not open file '" + this.state + "' for read/write, please grant read/write access to the file.", (Exception) ex);
      }
      catch (Exception ex)
      {
        throw new InvalidOperationException("Could not understand file '" + this.state + "'.", ex);
      }
    }

    private void ReadState(Stream stream)
    {
      try
      {
        using (BinaryReader binaryReader = new BinaryReader(stream))
        {
          while (true)
          {
            string index;
            DateTime key;
            LicenseValidator licenseValidator;
            do
            {
              index = binaryReader.ReadString();
              key = DateTime.FromBinary(binaryReader.ReadInt64());
              Guid userId = new Guid(binaryReader.ReadBytes(16));
              licenseValidator = this.availableLicenses.FirstOrDefault<LicenseValidator>((Func<LicenseValidator, bool>) (x => x.UserId == userId));
            }
            while (licenseValidator == null);
            this.leasedLicenses[index] = new KeyValuePair<DateTime, LicenseValidator>(key, licenseValidator);
            this.availableLicenses.Remove(licenseValidator);
          }
        }
      }
      catch (EndOfStreamException ex)
      {
      }
    }

    private void WriteState(Stream stream)
    {
      using (BinaryWriter binaryWriter = new BinaryWriter(stream))
      {
        foreach (KeyValuePair<string, KeyValuePair<DateTime, LicenseValidator>> leasedLicense in this.leasedLicenses)
        {
          binaryWriter.Write(leasedLicense.Key);
          binaryWriter.Write(leasedLicense.Value.Key.ToBinary());
          binaryWriter.Write(leasedLicense.Value.Value.UserId.ToByteArray());
        }
        binaryWriter.Flush();
        stream.Flush();
      }
    }

    public string LeaseLicense(string machine, string user, Guid id)
    {
      string str = machine + "\\" + user + ": " + (object) id;
      KeyValuePair<DateTime, LicenseValidator> keyValuePair;
      if (this.leasedLicenses.TryGetValue(str, out keyValuePair))
      {
        LicenseValidator licenseValidator = keyValuePair.Value;
        return this.GenerateLicenseAndRenewLease(str, id, licenseValidator, keyValuePair.Value.LicenseAttributes);
      }
      if (this.availableLicenses.Count > 0)
      {
        LicenseValidator availableLicense = this.availableLicenses[this.availableLicenses.Count - 1];
        this.availableLicenses.RemoveAt(this.availableLicenses.Count - 1);
        return this.GenerateLicenseAndRenewLease(str, id, availableLicense, availableLicense.LicenseAttributes);
      }
      foreach (KeyValuePair<string, KeyValuePair<DateTime, LicenseValidator>> leasedLicense in this.leasedLicenses)
      {
        if ((DateTime.UtcNow - leasedLicense.Value.Key).TotalMinutes >= 45.0)
        {
          this.leasedLicenses.Remove(leasedLicense.Key);
          return this.GenerateLicenseAndRenewLease(str, id, leasedLicense.Value.Value, leasedLicense.Value.Value.LicenseAttributes);
        }
      }
      return (string) null;
    }

    private string GenerateLicenseAndRenewLease(string identifier, Guid id, LicenseValidator licenseValidator, IDictionary<string, string> attributes)
    {
      this.leasedLicenses[identifier] = new KeyValuePair<DateTime, LicenseValidator>(DateTime.UtcNow.AddMinutes(30.0), licenseValidator);
      using (FileStream fileStream = new FileStream(this.state, FileMode.Create, FileAccess.ReadWrite))
        this.WriteState((Stream) fileStream);
      attributes["FloatingLicenseType"] = licenseValidator.LicenseType.ToString();
      attributes["FloatingLicenseExpiration"] = licenseValidator.ExpirationDate.ToString("O");
      return LicensingService.GenerateLicense(id, licenseValidator, attributes);
    }

    private static string GenerateLicense(Guid id, LicenseValidator validator, IDictionary<string, string> attributes)
    {
      return new LicenseGenerator(LicensingService.LicenseServerPrivateKey).Generate(validator.Name, id, DateTime.UtcNow.AddMinutes(45.0), attributes, LicenseType.Floating);
    }
  }
}
