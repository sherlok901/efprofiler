﻿// Decompiled with JetBrains decompiler
// Type: Rhino.Licensing.LicenseValidator
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System;
using System.IO;

namespace Rhino.Licensing
{
  public class LicenseValidator : AbstractLicenseValidator
  {
    private readonly string licensePath;
    private string inMemoryLicense;

    public LicenseValidator(string publicKey, string licensePath)
      : base(publicKey)
    {
      this.licensePath = licensePath;
    }

    public LicenseValidator(string publicKey, string licensePath, string licenseServerUrl, Guid clientId)
      : base(publicKey, licenseServerUrl, clientId)
    {
      this.licensePath = licensePath;
    }

    protected override string License
    {
      get
      {
        if (string.IsNullOrEmpty(this.licensePath))
          return (string) null;
        return this.inMemoryLicense ?? File.ReadAllText(this.licensePath);
      }
      set
      {
        try
        {
          File.WriteAllText(this.licensePath, value);
        }
        catch (Exception ex)
        {
          this.inMemoryLicense = value;
          AbstractLicenseValidator.Logger.Warn((object) "Could not write new license value, using in memory model instead", ex);
        }
      }
    }

    public override void AssertValidLicense()
    {
      if (!File.Exists(this.licensePath))
      {
        AbstractLicenseValidator.Logger.WarnFormat("Could not find license file: {0}", (object) this.licensePath);
        throw new LicenseFileNotFoundException();
      }
      base.AssertValidLicense();
    }

    public override void RemoveExistingLicense()
    {
      base.RemoveExistingLicense();
      File.Delete(this.licensePath);
    }
  }
}
