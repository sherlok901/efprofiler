﻿// Decompiled with JetBrains decompiler
// Type: Rhino.Licensing.Discovery.DiscoveryClient
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using log4net;
using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Rhino.Licensing.Discovery
{
  public class DiscoveryClient : IDisposable
  {
    private readonly ILog logger = LogManager.GetLogger(typeof (DiscoveryClient));
    private readonly TimeSpan publishLimit;
    private readonly byte[] buffer;
    private readonly UdpClient udpClient;
    private readonly IPEndPoint allHostsGroup;
    private DateTime lastPublish;

    public DiscoveryClient(Guid senderId, Guid userId, string machineName, string userName)
      : this(senderId, userId, machineName, userName, TimeSpan.FromMinutes(5.0))
    {
    }

    public DiscoveryClient(Guid senderId, Guid userId, string machineName, string userName, TimeSpan publishLimit)
    {
      this.publishLimit = publishLimit;
      this.buffer = Encoding.UTF8.GetBytes(senderId.ToString() + Environment.NewLine + (object) userId + Environment.NewLine + machineName + Environment.NewLine + userName);
      this.udpClient = new UdpClient()
      {
        ExclusiveAddressUse = false
      };
      this.allHostsGroup = new IPEndPoint(IPAddress.Parse("224.0.0.1"), 12391);
    }

    public async void PublishMyPresence()
    {
      if (DateTime.UtcNow - this.lastPublish < this.publishLimit)
        return;
      lock (this)
      {
        if (DateTime.UtcNow - this.lastPublish < this.publishLimit)
          return;
        this.lastPublish = DateTime.UtcNow;
      }
      try
      {
        int num = await Task.Factory.FromAsync<byte[], int, IPEndPoint, int>(new Func<byte[], int, IPEndPoint, AsyncCallback, object, IAsyncResult>(this.udpClient.BeginSend), new Func<IAsyncResult, int>(this.udpClient.EndSend), this.buffer, this.buffer.Length, this.allHostsGroup, (object) null);
      }
      catch (Exception ex)
      {
        this.logger.Error((object) "Failed to publish application's presence", ex);
      }
    }

    void IDisposable.Dispose()
    {
      this.udpClient.Close();
    }
  }
}
