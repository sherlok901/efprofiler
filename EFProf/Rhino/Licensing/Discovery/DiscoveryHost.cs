﻿// Decompiled with JetBrains decompiler
// Type: Rhino.Licensing.Discovery.DiscoveryHost
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;

namespace Rhino.Licensing.Discovery
{
  public class DiscoveryHost : IDisposable
  {
    private readonly byte[] buffer = new byte[4096];
    private Socket socket;

    public void Start()
    {
      this.IsStop = false;
      this.socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
      this.socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, 1);
      foreach (NetworkInterface networkInterface in ((IEnumerable<NetworkInterface>) NetworkInterface.GetAllNetworkInterfaces()).Where<NetworkInterface>((Func<NetworkInterface, bool>) (card => card.OperationalStatus == OperationalStatus.Up)))
      {
        foreach (IPAddressInformation addressInformation in networkInterface.GetIPProperties().UnicastAddresses.Where<UnicastIPAddressInformation>((Func<UnicastIPAddressInformation, bool>) (x => x.Address.AddressFamily == AddressFamily.InterNetwork)))
          this.socket.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.AddMembership, (object) new MulticastOption(IPAddress.Parse("224.0.0.1"), addressInformation.Address));
      }
      this.socket.Bind((EndPoint) new IPEndPoint(IPAddress.Any, 12391));
      this.StartListening();
    }

    private void StartListening()
    {
      SocketAsyncEventArgs socketAsyncEventArgs = new SocketAsyncEventArgs()
      {
        RemoteEndPoint = (EndPoint) new IPEndPoint(IPAddress.Any, 0)
      };
      socketAsyncEventArgs.Completed += new EventHandler<SocketAsyncEventArgs>(this.Completed);
      socketAsyncEventArgs.SetBuffer(this.buffer, 0, this.buffer.Length);
      bool fromAsync;
      try
      {
        fromAsync = this.socket.ReceiveFromAsync(socketAsyncEventArgs);
      }
      catch (Exception ex)
      {
        return;
      }
      if (fromAsync)
        return;
      this.Completed((object) this, socketAsyncEventArgs);
    }

    private void Completed(object sender, SocketAsyncEventArgs socketAsyncEventArgs)
    {
      if (this.IsStop)
        return;
      using (socketAsyncEventArgs)
      {
        try
        {
          using (MemoryStream memoryStream = new MemoryStream(socketAsyncEventArgs.Buffer, 0, socketAsyncEventArgs.BytesTransferred))
          {
            using (StreamReader streamReader = new StreamReader((Stream) memoryStream))
            {
              string input1 = streamReader.ReadLine();
              string input2 = streamReader.ReadLine();
              DiscoveryHost.ClientDiscoveredEventArgs e = new DiscoveryHost.ClientDiscoveredEventArgs()
              {
                MachineName = streamReader.ReadLine(),
                UserName = streamReader.ReadLine()
              };
              Guid result;
              if (Guid.TryParse(input2, out result))
                e.UserId = result;
              if (Guid.TryParse(input1, out result))
              {
                e.SenderId = result;
                this.InvokeClientDiscovered(e);
              }
            }
          }
        }
        catch
        {
        }
        this.StartListening();
      }
    }

    public event EventHandler<DiscoveryHost.ClientDiscoveredEventArgs> ClientDiscovered;

    private void InvokeClientDiscovered(DiscoveryHost.ClientDiscoveredEventArgs e)
    {
      EventHandler<DiscoveryHost.ClientDiscoveredEventArgs> clientDiscovered = this.ClientDiscovered;
      if (clientDiscovered == null)
        return;
      clientDiscovered((object) this, e);
    }

    public void Dispose()
    {
      if (this.socket == null)
        return;
      this.socket.Dispose();
    }

    public void Stop()
    {
      this.IsStop = true;
    }

    protected bool IsStop { get; set; }

    public class ClientDiscoveredEventArgs : EventArgs
    {
      public Guid UserId { get; set; }

      public string MachineName { get; set; }

      public string UserName { get; set; }

      public Guid SenderId { get; set; }
    }
  }
}
