﻿// Decompiled with JetBrains decompiler
// Type: XamlGeneratedNamespace.GeneratedInternalTypeHelper
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Reflection;
using System.Windows.Markup;

namespace XamlGeneratedNamespace
{
  //[EditorBrowsable(EditorBrowsableState.Never)]
  //[DebuggerNonUserCode]
  //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
  //public sealed partial class GeneratedInternalTypeHelper : InternalTypeHelper
  //{
    //protected override object CreateInstance(Type type, CultureInfo culture)
    //{
    //  return Activator.CreateInstance(type, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.CreateInstance, (Binder) null, (object[]) null, culture);
    //}

    //protected override object GetPropertyValue(PropertyInfo propertyInfo, object target, CultureInfo culture)
    //{
    //  return propertyInfo.GetValue(target, BindingFlags.Default, (Binder) null, (object[]) null, culture);
    //}

    //protected override void SetPropertyValue(PropertyInfo propertyInfo, object target, object value, CultureInfo culture)
    //{
    //  propertyInfo.SetValue(target, value, BindingFlags.Default, (Binder) null, (object[]) null, culture);
    //}

    //protected override Delegate CreateDelegate(Type delegateType, object target, string handler)
    //{
    //  return (Delegate) target.GetType().InvokeMember("_CreateDelegate", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.InvokeMethod, (Binder) null, target, new object[2]
    //  {
    //    (object) delegateType,
    //    (object) handler
    //  }, (CultureInfo) null);
    //}

    //protected override void AddEventHandler(EventInfo eventInfo, object target, Delegate handler)
    //{
    //  eventInfo.AddEventHandler(target, handler);
    //}
 // }
}
