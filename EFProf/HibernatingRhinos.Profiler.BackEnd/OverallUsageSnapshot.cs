using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd
{
	public class OverallUsageSnapshot
	{
		public IAggregatedAlertSnapshot[] AggregatedAlerts
		{
			get;
			set;
		}

		public Dictionary<string, TotalAndAverage> AggregatedEntities
		{
			get;
			set;
		}

		public double AverageEntitiesLoadedPerSession
		{
			get;
			set;
		}

		public double? AverageQueryRowCount
		{
			get;
			set;
		}

		public double AverageStatementsPerSession
		{
			get;
			set;
		}

		public int CountOfSessions
		{
			get;
			set;
		}

		public int NumberOfAlerts
		{
			get;
			set;
		}

		public int NumberOfEntitiesTypes
		{
			get;
			set;
		}

		public int NumberOfTransactionCommits
		{
			get;
			set;
		}

		public int NumberOfTransactionRollbacks
		{
			get;
			set;
		}

		public int NumberOfTransactions
		{
			get;
			set;
		}

		public OverallUsageSnapshot()
		{
		}
	}
}