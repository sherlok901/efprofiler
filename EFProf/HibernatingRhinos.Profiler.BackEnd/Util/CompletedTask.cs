using System;
using System.Threading.Tasks;

namespace HibernatingRhinos.Profiler.BackEnd.Util
{
	public class CompletedTask : CompletedTask<object>
	{
		public new System.Threading.Tasks.Task Task
		{
			get
			{
				return base.Task;
			}
		}

		public CompletedTask()
		{
		}

		public CompletedTask(Exception error) : base(error)
		{
		}

		public static CompletedTask<T> With<T>(T result)
		{
			return new CompletedTask<T>(result);
		}
	}
}