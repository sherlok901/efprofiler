using System;
using System.Threading.Tasks;

namespace HibernatingRhinos.Profiler.BackEnd.Util
{
	public class CompletedTask<T>
	{
		private readonly Exception error;

		public readonly T Result;

		public Task<T> Task
		{
			get
			{
				TaskCompletionSource<T> taskCompletionSource = new TaskCompletionSource<T>();
				if (this.error != null)
				{
					taskCompletionSource.SetException(this.error);
				}
				else
				{
					taskCompletionSource.SetResult(this.Result);
				}
				return taskCompletionSource.Task;
			}
		}

		public CompletedTask() : this(default(T))
		{
		}

		public CompletedTask(T result)
		{
			this.Result = result;
		}

		public CompletedTask(Exception error)
		{
			this.error = error;
		}

		public static implicit operator Task<T>(CompletedTask<T> t)
		{
			return t.Task;
		}

		public static implicit operator Task(CompletedTask<T> t)
		{
			return t.Task;
		}
	}
}