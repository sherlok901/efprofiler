using System;

namespace HibernatingRhinos.Profiler.BackEnd.Util
{
	public static class LocalStorage
	{
		[ThreadStatic]
		public static Guid AttachedApplicationId;
	}
}