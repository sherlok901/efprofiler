using gudusoft.gsqlparser;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HibernatingRhinos.Profiler.BackEnd.Util
{
	public class SqlStatementsAnalysis
	{
		private readonly TLzStatementList subStatements;

		private int maxNestingSelectInStatement;

		private int nestingLevels;

		private int maxAndOrCount;

		private int andOrCount;

		private readonly List<object> whereClausesList = new List<object>();

		private readonly List<int> statementsWhereClusesCountList = new List<int>();

		public int MaxAndOrCountInWhereClause
		{
			get
			{
				return this.maxAndOrCount;
			}
		}

		public int MaxNestingSelectInStatement
		{
			get
			{
				return this.maxNestingSelectInStatement;
			}
		}

		public int MaxWhereClauseInStatement
		{
			get
			{
				if (!this.statementsWhereClusesCountList.Any<int>())
				{
					return 0;
				}
				return this.statementsWhereClusesCountList.Max();
			}
		}

		public SqlStatementsAnalysis(object subStatements)
		{
			this.subStatements = subStatements as TLzStatementList;
		}

		public void DoAnalysis()
		{
			this.statementsWhereClusesCountList.Clear();
			this.maxNestingSelectInStatement = -1;
			this.maxAndOrCount = 0;
			if (this.subStatements == null)
			{
				return;
			}
			foreach (TCustomSqlStatement subStatement in this.subStatements)
			{
				this.whereClausesList.Clear();
				this.nestingLevels = -1;
				this.TestNesting(subStatement);
				this.maxNestingSelectInStatement = Math.Max(this.maxNestingSelectInStatement, this.nestingLevels);
				foreach (object obj in this.whereClausesList)
				{
					this.andOrCount = 0;
					TLzCustomExpression tLzCustomExpression = obj as TLzCustomExpression;
					if (tLzCustomExpression == null)
					{
						continue;
					}
					tLzCustomExpression.PreOrderTraverse(new TLzExprVisitFunc(this.TreeNodeVisitor));
					this.maxAndOrCount = Math.Max(this.maxAndOrCount, this.andOrCount);
				}
				this.statementsWhereClusesCountList.Add(this.whereClausesList.Count);
			}
		}

		private void TestNesting(TCustomSqlStatement pstmt)
		{
			if (this.nestingLevels < 0 || pstmt.SqlStatementType == TSqlStatementType.sstSelect && pstmt.AsText.StartsWith("("))
			{
				this.nestingLevels++;
			}
			if (pstmt.WhereClause != null)
			{
				this.whereClausesList.Add(pstmt.WhereClause);
			}
			int num = pstmt.ChildNodes.Count();
			for (int i = 0; i < num; i++)
			{
				if (pstmt.ChildNodes[i] is TCustomSqlStatement)
				{
					this.TestNesting((TCustomSqlStatement)pstmt.ChildNodes[i]);
				}
			}
		}

		private bool TreeNodeVisitor(object node, bool pIsLeafNode)
		{
			if (!pIsLeafNode)
			{
				TLzCustomExpression tLzCustomExpression = node as TLzCustomExpression;
				if (tLzCustomExpression == null)
				{
					return true;
				}
				switch (tLzCustomExpression.oper)
				{
					case TLzOpType.Expr_AND:
					case TLzOpType.Expr_OR:
					{
						this.andOrCount++;
						break;
					}
				}
			}
			return true;
		}
	}
}