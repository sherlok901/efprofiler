using System;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.Messages
{
	[Serializable]
	public class ApplicationAttached : IRequiredDelivery
	{
		public Guid Id
		{
			get;
			set;
		}

		public bool IsProductionApplication
		{
			get;
			set;
		}

		public string Name
		{
			get;
			set;
		}

		public ApplicationAttached()
		{
		}
	}
}