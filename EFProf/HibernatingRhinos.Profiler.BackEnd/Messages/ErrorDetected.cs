using HibernatingRhinos.Profiler.Appender.StackTraces;
using System;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.Messages
{
	[Serializable]
	public class ErrorDetected : IHasSessionUrl, IHasSessionId
	{
		public string ExceptionString
		{
			get;
			set;
		}

		public string Message
		{
			get;
			set;
		}

		public SessionIdHolder SessionId
		{
			get
			{
				return JustDecompileGenerated_get_SessionId();
			}
			set
			{
				JustDecompileGenerated_set_SessionId(value);
			}
		}

		private SessionIdHolder JustDecompileGenerated_SessionId_k__BackingField;

		public SessionIdHolder JustDecompileGenerated_get_SessionId()
		{
			return this.JustDecompileGenerated_SessionId_k__BackingField;
		}

		public void JustDecompileGenerated_set_SessionId(SessionIdHolder value)
		{
			this.JustDecompileGenerated_SessionId_k__BackingField = value;
		}

		public string Severity
		{
			get;
			set;
		}

		public StackTraceInfo StackTrace
		{
			get;
			set;
		}

		public string Url
		{
			get
			{
				return JustDecompileGenerated_get_Url();
			}
			set
			{
				JustDecompileGenerated_set_Url(value);
			}
		}

		private string JustDecompileGenerated_Url_k__BackingField;

		public string JustDecompileGenerated_get_Url()
		{
			return this.JustDecompileGenerated_Url_k__BackingField;
		}

		public void JustDecompileGenerated_set_Url(string value)
		{
			this.JustDecompileGenerated_Url_k__BackingField = value;
		}

		public ErrorDetected()
		{
		}
	}
}