using HibernatingRhinos.Profiler.Appender.StackTraces;
using log4net.Core;
using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.Messages
{
	[DebuggerDisplay("{Logger} - {Message} - {Date}")]
	public class LoggingEvent
	{
		public string Culture
		{
			get;
			set;
		}

		public string DbDialect
		{
			get;
			set;
		}

		public string Exception
		{
			get;
			set;
		}

		public log4net.Core.Level Level
		{
			get;
			set;
		}

		public string Logger
		{
			get;
			set;
		}

		public string Message
		{
			get;
			set;
		}

		public SessionIdHolder SessionId
		{
			get;
			set;
		}

		public StackTraceInfo StackTrace
		{
			get;
			set;
		}

		public string StarColor
		{
			get;
			set;
		}

		public string ThreadContext
		{
			get;
			set;
		}

		public string Url
		{
			get;
			set;
		}

		public LoggingEvent()
		{
		}

		public override string ToString()
		{
			return string.Concat(this.Logger, ": ", this.Message);
		}
	}
}