using System;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.Messages
{
	[Serializable]
	public class TransactionBegan : TransactionMessageBase
	{
		public bool IsDtcTransaction
		{
			get;
			set;
		}

		public string IsolationLevel
		{
			get;
			set;
		}

		public TransactionBegan()
		{
		}
	}
}