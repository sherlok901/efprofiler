using System;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.Messages
{
	[Serializable]
	public class BringApplicationToFront
	{
		public string ContextFile
		{
			get;
			set;
		}

		public BringApplicationToFront()
		{
		}
	}
}