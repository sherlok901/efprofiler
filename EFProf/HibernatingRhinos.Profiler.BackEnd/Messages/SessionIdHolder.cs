using System;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.Messages
{
	public class SessionIdHolder : IEquatable<SessionIdHolder>
	{
		public string ScopeName
		{
			get;
			set;
		}

		public string SessionId
		{
			get;
			set;
		}

		public string ThreadId
		{
			get;
			set;
		}

		public DateTime Timestamp
		{
			get;
			set;
		}

		public SessionIdHolder()
		{
		}

		public bool Equals(SessionIdHolder other)
		{
			if (object.ReferenceEquals(null, other))
			{
				return false;
			}
			if (object.ReferenceEquals(this, other))
			{
				return true;
			}
			return object.Equals(other.SessionId, this.SessionId);
		}

		public override bool Equals(object obj)
		{
			if (object.ReferenceEquals(null, obj))
			{
				return false;
			}
			if (object.ReferenceEquals(this, obj))
			{
				return true;
			}
			return this.Equals(obj as SessionIdHolder);
		}

		public override int GetHashCode()
		{
			return this.SessionId.GetHashCode();
		}

		public static SessionIdHolder NewSessionIdForTest(string sessionId = null)
		{
			SessionIdHolder sessionIdHolder = new SessionIdHolder()
			{
				SessionId = sessionId ?? Guid.NewGuid().ToString(),
				ThreadId = "Test thread",
				ScopeName = "Test scope",
				Timestamp = DateTime.Now
			};
			return sessionIdHolder;
		}
	}
}