using System;

namespace HibernatingRhinos.Profiler.BackEnd.Messages
{
	[Flags]
	public enum SqlStatementOptions
	{
		None = 0,
		DoNotTryToParseStatement = 1,
		Cached = 2,
		DDL = 4,
		LuceneQuery = 8,
		Transaction = 16,
		DtcTransaction = 32,
		Warning = 64
	}
}