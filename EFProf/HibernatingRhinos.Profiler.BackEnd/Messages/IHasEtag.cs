using System;

namespace HibernatingRhinos.Profiler.BackEnd.Messages
{
	public interface IHasEtag
	{
		long Etag
		{
			get;
		}
	}
}