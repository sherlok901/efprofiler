using System;

namespace HibernatingRhinos.Profiler.BackEnd.Messages
{
	[Serializable]
	public class TransactionCommitted : TransactionMessageBase
	{
		public TransactionCommitted()
		{
		}
	}
}