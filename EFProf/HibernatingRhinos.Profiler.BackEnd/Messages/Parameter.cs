using HibernatingRhinos.Profiler.BackEnd.Extensions;
using HibernatingRhinos.Profiler.BackEnd.Infrastructure;
using System;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;

namespace HibernatingRhinos.Profiler.BackEnd.Messages
{
	[DebuggerDisplay("{Name} = {Value}")]
	public class Parameter : IEquatable<Parameter>
	{
		public readonly string Dialect = "";

		private readonly static Regex TypeLength;

		public string AssociateColumn
		{
			get;
			set;
		}

		public string AssociateTable
		{
			get;
			set;
		}

		public SqlType ColumnType
		{
			get;
			set;
		}

		public string Name
		{
			get;
			set;
		}

		public string OriginalValue
		{
			get;
			set;
		}

		public object ParameterValue
		{
			get;
			set;
		}

		public string Type
		{
			get;
			set;
		}

		public string Value
		{
			get;
			set;
		}

		static Parameter()
		{
			Parameter.TypeLength = new Regex("\\((\\d+)\\)", RegexOptions.Compiled);
		}

		public Parameter()
		{
		}

		public Parameter(string name, string value, string type = null, string dialect = null)
		{
			this.Name = name;
			this.Dialect = dialect;
			this.Type = Parameter.FormatType(type);
			this.OriginalValue = value;
			this.Value = this.GetFormattedValue(value, this.Type);
		}

		public bool Equals(Parameter other)
		{
			if (object.ReferenceEquals(null, other))
			{
				return false;
			}
			if (object.ReferenceEquals(this, other))
			{
				return true;
			}
			if (!object.Equals(other.Name, this.Name))
			{
				return false;
			}
			return object.Equals(other.Value, this.Value);
		}

		public override bool Equals(object obj)
		{
			if (object.ReferenceEquals(null, obj))
			{
				return false;
			}
			if (object.ReferenceEquals(this, obj))
			{
				return true;
			}
			return this.Equals((Parameter)obj);
		}

		private static string FormatType(string type)
		{
			if (type == null)
			{
				return null;
			}
			string str = type.Trim();
			char[] chrArray = new char[] { '[', '-' };
			string str1 = str.TrimStart(chrArray);
			char[] chrArray1 = new char[] { ']', '-' };
			type = str1.TrimEnd(chrArray1);
			if (type.EndsWith("(0)") || type.EndsWith("[0]"))
			{
				type = type.Substring(0, type.Length - 3).Trim();
			}
			return type;
		}

		private string GetFormattedValue(string value, string type)
		{
			Guid guid;
			bool flag;
			int num;
			long num1;
			DateTime dateTime;
			double num2;
			if (value == null)
			{
				return "NULL";
			}
			if (value == "NULL")
			{
				return value;
			}
			if (type != null && type.IndexOf("String", StringComparison.OrdinalIgnoreCase) != -1)
			{
				return string.Concat("'", value.Truncate(512, FormattingOptions.AllowMultiLine).Replace("'", "''"), "'");
			}
			if (type != null && type.Equals("Structured", StringComparison.OrdinalIgnoreCase))
			{
				DataTable dataTable = new DataTable();
				using (StringReader stringReader = new StringReader(value))
				{
					dataTable.ReadXml(stringReader);
				}
				this.ParameterValue = dataTable;
				return "Structured";
			}
			if (value.StartsWith("0x"))
			{
				if (value.Length < 256)
				{
					return value;
				}
				return string.Concat("'", value.Truncate(256, FormattingOptions.ForceSingleLine), "'");
			}
			if (type != null && type.IndexOf("Guid", StringComparison.OrdinalIgnoreCase) != -1 && Guid.TryParse(value, out guid))
			{
				this.ParameterValue = guid;
				return string.Format("'{0}'", value);
			}
			if (bool.TryParse(value, out flag))
			{
				if (this.Dialect.ContainsIgnoreCase("postgresql"))
				{
					if (!flag)
					{
						return "'0'";
					}
					return "'1'";
				}
				if (this.Dialect.ContainsIgnoreCase("postgis"))
				{
					if (!flag)
					{
						return "false";
					}
					return "true";
				}
				if (!flag)
				{
					return "0";
				}
				return "1";
			}
			if (int.TryParse(value, out num) && Parameter.IsValidInteger(num, value))
			{
				return value;
			}
			if (long.TryParse(value, out num1) && Parameter.IsValidLong(num1, value))
			{
				return value;
			}
			if (!Parameter.TryParseDate(value, out dateTime))
			{
				if (value.Contains(".") && double.TryParse(value, NumberStyles.AllowLeadingWhite | NumberStyles.AllowTrailingWhite | NumberStyles.AllowLeadingSign | NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands | NumberStyles.AllowExponent | NumberStyles.Integer | NumberStyles.Float, CultureInfo.InvariantCulture, out num2))
				{
					return value;
				}
				return string.Concat("'", value.Truncate(512, FormattingOptions.AllowMultiLine).Replace("'", "''"), "'");
			}
			this.ParameterValue = dateTime;
			if (this.Dialect.ContainsIgnoreCase("oracle"))
			{
				return string.Format("TIMESTAMP '{0}'", dateTime.ToString("yyyy-MM-dd HH:mm:ss.FFFFFFF"));
			}
			return string.Format("'{0}'", dateTime.ToString("yyyy-MM-ddTHH:mm:ss.FFFFFFF"));
		}

		public override int GetHashCode()
		{
			return (this.Name != null ? this.Name.GetHashCode() : 0) * 397 ^ (this.Value != null ? this.Value.GetHashCode() : 0);
		}

		public object GetParameterValue()
		{
			if (this.Value == "NULL")
			{
				return null;
			}
			this.Value = this.GetFormattedValue(this.OriginalValue, this.Type);
			if (this.ParameterValue != null)
			{
				return this.ParameterValue;
			}
			return this.Value;
		}

		private bool IsColumnLengthMismatch(bool isEntityFramework)
		{
			int num;
			if (!isEntityFramework || string.IsNullOrEmpty(this.Type) || this.ColumnType == null || this.ColumnType.Length <= 0)
			{
				return false;
			}
			Match match = Parameter.TypeLength.Match(this.Type);
			if (!match.Success)
			{
				return false;
			}
			if (!int.TryParse(match.Groups[1].Value, out num))
			{
				return false;
			}
			return this.ColumnType.Length < num;
		}

		public bool IsColumnTypeMismatch(bool isEntityFramework)
		{
			if (string.IsNullOrEmpty(this.Type) || this.ColumnType == null || this.ColumnType.Type == null)
			{
				return false;
			}
			string lower = this.ColumnType.Type.ToLower();
			if (lower == this.Type.ToLower())
			{
				return false;
			}
			string str = lower;
			string str1 = str;
			if (str != null)
			{
				switch (str1)
				{
					case "tinyint":
					{
						return !this.Type.StartsWith("Byte");
					}
					case "smallint":
					{
						return !this.Type.StartsWith("Int16");
					}
					case "bigint":
					{
						return !this.Type.StartsWith("Int64");
					}
					case "int":
					{
						return !this.Type.StartsWith("Int32");
					}
					case "nvarchar":
					case "nchar":
					{
						if (!this.Type.StartsWith("String"))
						{
							return true;
						}
						return this.IsColumnLengthMismatch(isEntityFramework);
					}
					case "varchar":
					case "char":
					{
						if (!this.Type.StartsWith("AnsiString"))
						{
							return true;
						}
						return this.IsColumnLengthMismatch(isEntityFramework);
					}
					case "binary":
					{
						if (!this.Type.StartsWith("Binary"))
						{
							return true;
						}
						return this.IsColumnLengthMismatch(isEntityFramework);
					}
					case "bit":
					{
						return !this.Type.StartsWith("Boolean");
					}
					case "float":
					{
						return !this.Type.StartsWith("Double");
					}
					case "real":
					{
						return !this.Type.StartsWith("Single");
					}
					case "date":
					{
						if (this.Type.StartsWith("Date"))
						{
							return false;
						}
						return !this.Type.StartsWith("DateTime");
					}
					case "time":
					{
						if (this.Type.StartsWith("Time"))
						{
							return false;
						}
						return !this.Type.StartsWith("DateTime");
					}
					case "datetime":
					{
						return !this.Type.StartsWith("DateTime");
					}
					case "datetime2":
					{
						if (this.Type.StartsWith("DateTime"))
						{
							return false;
						}
						return !this.Type.StartsWith("DateTime2");
					}
					case "dateTimeOffset":
					{
						if (this.Type.StartsWith("DateTime"))
						{
							return false;
						}
						return !this.Type.StartsWith("DateTimeOffset");
					}
					case "money":
					{
						return !this.Type.StartsWith("Currency");
					}
					case "decimal":
					case "numeric":
					{
						return !this.Type.StartsWith("Decimal");
					}
				}
			}
			return false;
		}

		private static bool IsValidInteger(int result, string value)
		{
			if (result == 0)
			{
				return true;
			}
			if (value.Any<char>(new Func<char, bool>(char.IsLetter)))
			{
				return false;
			}
			return value[0] != '0';
		}

		private static bool IsValidLong(long result, string value)
		{
			if (result == (long)0)
			{
				return true;
			}
			if (value.Any<char>(new Func<char, bool>(char.IsLetter)))
			{
				return false;
			}
			return value[0] != '0';
		}

		private static bool TryParseDate(string value, out DateTime dateResult)
		{
			decimal num;
			DateTimeOffset dateTimeOffset;
			bool flag;
			bool flag1;
			if (DateTime.TryParseExact(value, CultureInfo.CurrentCulture.DateTimeFormat.GetAllDateTimePatterns('G'), CultureInfo.CurrentCulture, DateTimeStyles.None, out dateResult))
			{
				flag = true;
			}
			else
			{
				flag = (!value.Contains(" ") ? false : DateTime.TryParse(value, CultureInfo.InvariantCulture, DateTimeStyles.None, out dateResult));
			}
			if (flag && !decimal.TryParse(value, NumberStyles.Any, CultureInfo.CurrentCulture, out num))
			{
				return true;
			}
			if (DateTimeOffset.TryParseExact(value, (
				from s in CultureInfo.CurrentCulture.DateTimeFormat.GetAllDateTimePatterns('G')
				select string.Concat(s, " zzz")).ToArray<string>(), CultureInfo.CurrentCulture, DateTimeStyles.None, out dateTimeOffset))
			{
				flag1 = true;
			}
			else
			{
				char[] chrArray = new char[] { ' ' };
				flag1 = ((int)value.Split(chrArray).Length <= 2 ? false : DateTimeOffset.TryParse(value, CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTimeOffset));
			}
			if (flag1)
			{
				dateResult = dateTimeOffset.ToUniversalTime().DateTime;
				if (!decimal.TryParse(value, NumberStyles.Any, CultureInfo.CurrentCulture, out num))
				{
					return true;
				}
			}
			return false;
		}
	}
}