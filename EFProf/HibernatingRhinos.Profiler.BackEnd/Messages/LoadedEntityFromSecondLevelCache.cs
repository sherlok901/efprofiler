using HibernatingRhinos.Profiler.Appender.StackTraces;
using System;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.Messages
{
	[Serializable]
	public class LoadedEntityFromSecondLevelCache : LoadedEntity
	{
		public StackTraceInfo StackTrace
		{
			get;
			set;
		}

		public LoadedEntityFromSecondLevelCache()
		{
		}
	}
}