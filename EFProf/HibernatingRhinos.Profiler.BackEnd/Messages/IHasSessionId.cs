namespace HibernatingRhinos.Profiler.BackEnd.Messages
{
	public interface IHasSessionId
	{
		SessionIdHolder SessionId
		{
			get;
		}
	}
}