using System;

namespace HibernatingRhinos.Profiler.BackEnd.Messages
{
	[Serializable]
	public class TransactionDisposed : TransactionMessageBase
	{
		public TransactionDisposed()
		{
		}
	}
}