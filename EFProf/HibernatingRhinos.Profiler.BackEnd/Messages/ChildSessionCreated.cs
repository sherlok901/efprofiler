using System;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.Messages
{
	public class ChildSessionCreated : IHasSessionId
	{
		public SessionIdHolder SessionId
		{
			get
			{
				return JustDecompileGenerated_get_SessionId();
			}
			set
			{
				JustDecompileGenerated_set_SessionId(value);
			}
		}

		private SessionIdHolder JustDecompileGenerated_SessionId_k__BackingField;

		public SessionIdHolder JustDecompileGenerated_get_SessionId()
		{
			return this.JustDecompileGenerated_SessionId_k__BackingField;
		}

		public void JustDecompileGenerated_set_SessionId(SessionIdHolder value)
		{
			this.JustDecompileGenerated_SessionId_k__BackingField = value;
		}

		public ChildSessionCreated()
		{
		}
	}
}