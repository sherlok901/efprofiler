using System;

namespace HibernatingRhinos.Profiler.BackEnd.Messages
{
	[Serializable]
	public class ApplicationDetached : IRequiredDelivery
	{
		public Guid Id;

		public ApplicationDetached(Guid id)
		{
			this.Id = id;
		}
	}
}