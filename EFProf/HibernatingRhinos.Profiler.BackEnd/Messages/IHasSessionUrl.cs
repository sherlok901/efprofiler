using System;

namespace HibernatingRhinos.Profiler.BackEnd.Messages
{
	public interface IHasSessionUrl : IHasSessionId
	{
		string Url
		{
			get;
		}
	}
}