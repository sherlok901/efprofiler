using HibernatingRhinos.Profiler.Appender.StackTraces;
using System;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.Messages
{
	public class SqlStatementMessage : IHasSessionUrl, IHasSessionId
	{
		public readonly Guid Id = Guid.NewGuid();

		private Parameter[] parameters;

		public int BatchSize
		{
			get;
			set;
		}

		public string Dialect
		{
			get;
			set;
		}

		public SqlStatementOptions Options
		{
			get;
			set;
		}

		public Parameter[] Parameters
		{
			get
			{
				Parameter[] parameterArray = this.parameters;
				if (parameterArray == null)
				{
					Parameter[] parameterArray1 = new Parameter[0];
					Parameter[] parameterArray2 = parameterArray1;
					this.parameters = parameterArray1;
					parameterArray = parameterArray2;
				}
				return parameterArray;
			}
			set
			{
				this.parameters = value;
			}
		}

		public string Prefix
		{
			get;
			set;
		}

		public string RawSql
		{
			get;
			set;
		}

		public SessionIdHolder SessionId
		{
			get
			{
				return JustDecompileGenerated_get_SessionId();
			}
			set
			{
				JustDecompileGenerated_set_SessionId(value);
			}
		}

		private SessionIdHolder JustDecompileGenerated_SessionId_k__BackingField;

		public SessionIdHolder JustDecompileGenerated_get_SessionId()
		{
			return this.JustDecompileGenerated_SessionId_k__BackingField;
		}

		public void JustDecompileGenerated_set_SessionId(SessionIdHolder value)
		{
			this.JustDecompileGenerated_SessionId_k__BackingField = value;
		}

		public StackTraceInfo StackTrace
		{
			get;
			set;
		}

		public string StarColor
		{
			get;
			set;
		}

		public string StatementId
		{
			get;
			set;
		}

		public string Url
		{
			get
			{
				return JustDecompileGenerated_get_Url();
			}
			set
			{
				JustDecompileGenerated_set_Url(value);
			}
		}

		private string JustDecompileGenerated_Url_k__BackingField;

		public string JustDecompileGenerated_get_Url()
		{
			return this.JustDecompileGenerated_Url_k__BackingField;
		}

		public void JustDecompileGenerated_set_Url(string value)
		{
			this.JustDecompileGenerated_Url_k__BackingField = value;
		}

		public SqlStatementMessage()
		{
		}
	}
}