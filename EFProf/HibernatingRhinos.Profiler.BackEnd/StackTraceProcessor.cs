using HibernatingRhinos.Profiler.Appender.StackTraces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace HibernatingRhinos.Profiler.BackEnd
{
	[Serializable]
	public class StackTraceProcessor
	{
		private readonly ProfilerConfiguration configuration;

		private static Regex namespacesToClear;

		private static Regex namespacesToKeep;

		public StackTraceProcessor(ProfilerConfiguration configuration)
		{
			this.configuration = configuration;
			StringBuilder stringBuilder = new StringBuilder();
			if (configuration.InfrastructureNamespaces != null && configuration.InfrastructureNamespaces.Count > 0)
			{
				foreach (string infrastructureNamespace in configuration.InfrastructureNamespaces)
				{
					stringBuilder.Append("(^").Append(Regex.Escape(infrastructureNamespace)).Append(")|");
				}
				if (stringBuilder.Length > 0)
				{
					stringBuilder.Length = stringBuilder.Length - 1;
				}
				string str = stringBuilder.ToString();
				if (StackTraceProcessor.namespacesToClear == null || str != StackTraceProcessor.namespacesToClear.ToString())
				{
					StackTraceProcessor.namespacesToClear = this.NewRegex(str);
				}
				stringBuilder.Length = 0;
			}
			if (configuration.NonInfrastructureNamespaces != null && configuration.NonInfrastructureNamespaces.Count > 0)
			{
				foreach (string nonInfrastructureNamespace in configuration.NonInfrastructureNamespaces)
				{
					stringBuilder.Append("(^").Append(Regex.Escape(nonInfrastructureNamespace)).Append(")|");
				}
				if (stringBuilder.Length > 0)
				{
					stringBuilder.Length = stringBuilder.Length - 1;
				}
				string str1 = stringBuilder.ToString();
				if (StackTraceProcessor.namespacesToKeep == null || str1 != StackTraceProcessor.namespacesToKeep.ToString())
				{
					StackTraceProcessor.namespacesToKeep = this.NewRegex(str1);
				}
			}
		}

		protected StackTraceFrame[] ClearStackTraceFromInfrastructureFrameworks(StackTraceFrame[] frames)
		{
			if (this.configuration.InfrastructureNamespaces == null)
			{
				return frames;
			}
			return frames.Where<StackTraceFrame>(new Func<StackTraceFrame, bool>(StackTraceProcessor.IsValidStackFrameMethod)).ToArray<StackTraceFrame>();
		}

		private static bool IsValidStackFrameMethod(StackTraceFrame frame)
		{
			if (string.IsNullOrEmpty(frame.Namespace))
			{
				if (string.IsNullOrEmpty(frame.Filename))
				{
					return false;
				}
				return true;
			}
			if (StackTraceProcessor.namespacesToClear == null || !StackTraceProcessor.namespacesToClear.IsMatch(frame.Namespace))
			{
				return true;
			}
			if (StackTraceProcessor.namespacesToKeep == null)
			{
				return true;
			}
			return StackTraceProcessor.namespacesToKeep.IsMatch(frame.Namespace);
		}

		private Regex NewRegex(string pattern)
		{
			return new Regex(pattern, RegexOptions.IgnoreCase | RegexOptions.Compiled);
		}

		public StackTraceInfo ProcessStackTrace(StackTraceInfo stackTrace)
		{
			if (stackTrace == null)
			{
				return null;
			}
			StackTraceInfo stackTraceInfo = new StackTraceInfo()
			{
				Frames = this.ClearStackTraceFromInfrastructureFrameworks(stackTrace.Frames)
			};
			return stackTraceInfo;
		}
	}
}