using System;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd
{
	public class TotalAndAverage
	{
		public double Average
		{
			get;
			set;
		}

		public string Key
		{
			get;
			set;
		}

		public int Total
		{
			get;
			set;
		}

		public TotalAndAverage()
		{
		}
	}
}