using gudusoft.gsqlparser;
using HibernatingRhinos.Profiler.BackEnd.Infrastructure;
using HibernatingRhinos.Profiler.BackEnd.Messages;
using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace HibernatingRhinos.Profiler.BackEnd
{
	public class WhereClauseParametersProcessor
	{
		private readonly TLzCustomExpression whereClause;

		private TLzOpType opType;

		private int leafPos;

		private WhereClauseParameter current;

		public List<WhereClauseParameter> ParametersList
		{
			get;
			private set;
		}

		public WhereClauseParametersProcessor(object whereClause)
		{
			this.whereClause = whereClause as TLzCustomExpression;
			this.ParametersList = new List<WhereClauseParameter>();
		}

		public void AddParmeterInfoToFormattedSql(Statement statement)
		{
			Func<WhereClauseParameter, bool> parameter = null;
			Task.Factory.StartNew(() => {
				List<WhereClauseParameter> parametersList = this.ParametersList;
				if (parameter == null)
				{
					parameter = (WhereClauseParameter x) => {
						if (x.Parameter == null)
						{
							return false;
						}
						return x.Parameter.AssociateColumn != null;
					};
				}
				if (!parametersList.Any<WhereClauseParameter>(parameter))
				{
					return;
				}
				StringBuilder stringBuilder = new StringBuilder(statement.FormattedSql);
				int length = 0;
				int num = 0;
				foreach (WhereClauseParameter whereClauseParameter in this.ParametersList)
				{
					if (whereClauseParameter.Parameter == null || whereClauseParameter.Parameter.AssociateColumn == null)
					{
						continue;
					}
					int num1 = statement.FormattedSql.IndexOf(whereClauseParameter.Parameter.Name, num);
					if (num1 == -1)
					{
						continue;
					}
					num = num1;
					string str = string.Concat(" - ", whereClauseParameter.Parameter.AssociateColumn);
					stringBuilder.Insert(num1 + whereClauseParameter.Parameter.Name.Length + length, str);
					length += str.Length;
				}
				statement.FormattedSql = stringBuilder.ToString();
			});
		}

		public void Process(List<Parameter> toDoParameters, List<TableNamesAndScema> tableList)
		{
			if (this.whereClause == null)
			{
				return;
			}
			this.whereClause.PreOrderTraverse(new TLzExprVisitFunc(this.TreeNodeVisitor));
			foreach (WhereClauseParameter parametersList in this.ParametersList)
			{
				parametersList.Parameter = toDoParameters.FirstOrDefault<Parameter>((Parameter y) => y.Name == parametersList.RightExp);
				toDoParameters.Remove(parametersList.Parameter);
				if (parametersList.Parameter == null)
				{
					continue;
				}
				TableNamesAndScema item = tableList.FirstOrDefault<TableNamesAndScema>((TableNamesAndScema t) => {
					if (string.IsNullOrEmpty(t.Alias))
					{
						return false;
					}
					return parametersList.LeftExp.Contains(t.Alias);
				});
				if (item == null)
				{
					TableNamesAndScema tableNamesAndScema = tableList.FirstOrDefault<TableNamesAndScema>((TableNamesAndScema t) => {
						if (string.IsNullOrEmpty(t.Name))
						{
							return false;
						}
						return parametersList.LeftExp.Contains(t.Name);
					});
					item = tableNamesAndScema;
					if (tableNamesAndScema == null)
					{
						item = tableList[0];
						parametersList.Parameter.AssociateColumn = parametersList.LeftExp;
						parametersList.Parameter.AssociateTable = item.Name;
						parametersList.Schema = item.TableSchema;
						parametersList.IsTabaleNameExist = true;
					}
					else
					{
						if (item.Name.Length + 1 >= parametersList.LeftExp.Length)
						{
							continue;
						}
						parametersList.Parameter.AssociateColumn = parametersList.LeftExp.Substring(item.Name.Length + 1);
						parametersList.Parameter.AssociateTable = item.Name;
						parametersList.Schema = item.TableSchema;
						parametersList.IsTabaleNameExist = true;
					}
				}
				else
				{
					if (item.Alias.Length + 1 >= parametersList.LeftExp.Length)
					{
						continue;
					}
					parametersList.Parameter.AssociateColumn = parametersList.LeftExp.Substring(item.Alias.Length + 1);
					parametersList.Parameter.AssociateTable = item.Name;
					parametersList.Schema = item.TableSchema;
					parametersList.IsTabaleNameExist = true;
				}
				if (!parametersList.IsTabaleNameExist)
				{
					continue;
				}
				string str = parametersList.Parameter.AssociateColumn.Replace("[", "").Replace("]", "");
				if (!parametersList.Schema.Columns.ContainsKey(str))
				{
					continue;
				}
				parametersList.Parameter.ColumnType = parametersList.Schema.Columns[str];
			}
		}

		private bool TreeNodeVisitor(object node, bool pIsLeafNode)
		{
			TLz_Node tLzNode = node as TLz_Node;
			if (tLzNode == null)
			{
				return true;
			}
			if (!pIsLeafNode)
			{
				TLzCustomExpression tLzCustomExpression = (TLzCustomExpression)tLzNode;
				TLzOpType tLzOpType = tLzCustomExpression.oper;
				switch (tLzOpType)
				{
					case TLzOpType.Expr_Comparison:
					case TLzOpType.Expr_Like:
					case TLzOpType.Expr_NotLike:
					{
						if (tLzCustomExpression.opname == null)
						{
							break;
						}
						this.leafPos = 0;
						this.opType = tLzCustomExpression.oper;
						WhereClauseParameter whereClauseParameter = new WhereClauseParameter()
						{
							Operation = tLzCustomExpression.opname.AsText
						};
						this.current = whereClauseParameter;
						this.ParametersList.Add(this.current);
						break;
					}
					case TLzOpType.Expr_Between:
					case TLzOpType.Expr_NotBetween:
					case TLzOpType.Expr_In:
					case TLzOpType.Expr_NotIn:
					{
						if (tLzCustomExpression.opname == null)
						{
							break;
						}
						this.leafPos = 0;
						this.opType = tLzCustomExpression.oper;
						WhereClauseParameter whereClauseParameter1 = new WhereClauseParameter()
						{
							Operation = tLzCustomExpression.opname.AsText
						};
						this.current = whereClauseParameter1;
						break;
					}
					case TLzOpType.Expr_BetweenTo:
					{
						break;
					}
					default:
					{
						if (tLzOpType != TLzOpType.Expr_OP)
						{
							break;
						}
						else
						{
							goto case TLzOpType.Expr_NotLike;
						}
					}
				}
			}
			else
			{
				if (this.current == null)
				{
					return true;
				}
				if (this.leafPos != 0)
				{
					TLzOpType tLzOpType1 = this.opType;
					switch (tLzOpType1)
					{
						case TLzOpType.Expr_Comparison:
						case TLzOpType.Expr_Like:
						case TLzOpType.Expr_NotLike:
						{
							this.current.RightExp = tLzNode.AsText;
							break;
						}
						case TLzOpType.Expr_Between:
						case TLzOpType.Expr_NotBetween:
						case TLzOpType.Expr_In:
						case TLzOpType.Expr_NotIn:
						{
							WhereClauseParameter whereClauseParameter2 = new WhereClauseParameter()
							{
								Operation = this.current.Operation,
								LeftExp = this.current.LeftExp,
								RightExp = tLzNode.AsText
							};
							this.ParametersList.Add(whereClauseParameter2);
							break;
						}
						case TLzOpType.Expr_BetweenTo:
						{
							break;
						}
						default:
						{
							if (tLzOpType1 != TLzOpType.Expr_OP)
							{
								break;
							}
							else
							{
								goto case TLzOpType.Expr_NotLike;
							}
						}
					}
				}
				else
				{
					this.current.LeftExp = tLzNode.AsText;
				}
				this.leafPos++;
			}
			return true;
		}
	}
}