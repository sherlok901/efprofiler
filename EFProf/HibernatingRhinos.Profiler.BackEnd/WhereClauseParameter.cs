using HibernatingRhinos.Profiler.BackEnd.Infrastructure;
using HibernatingRhinos.Profiler.BackEnd.Messages;
using System;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd
{
	public class WhereClauseParameter
	{
		public bool IsTabaleNameExist
		{
			get;
			set;
		}

		public string LeftExp
		{
			get;
			set;
		}

		public string Operation
		{
			get;
			set;
		}

		public HibernatingRhinos.Profiler.BackEnd.Messages.Parameter Parameter
		{
			get;
			set;
		}

		public string RightExp
		{
			get;
			set;
		}

		public TableSchema Schema
		{
			get;
			set;
		}

		public WhereClauseParameter()
		{
		}
	}
}