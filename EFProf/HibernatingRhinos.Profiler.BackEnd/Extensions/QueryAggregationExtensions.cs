using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using HibernatingRhinos.Profiler.BackEnd.Reports;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.Extensions
{
	public static class QueryAggregationExtensions
	{
		public static QueryAggregation AggregateStatement(this IDictionary<string, QueryAggregation> dictionary, Statement statement)
		{
			QueryAggregation queryAggregation;
			if (!dictionary.TryGetValue(statement.RawSql, out queryAggregation))
			{
				queryAggregation = new QueryAggregation(statement);
				dictionary[statement.RawSql] = queryAggregation;
			}
			queryAggregation.AddQuery(statement);
			return queryAggregation;
		}
	}
}