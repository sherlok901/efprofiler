using System;

namespace HibernatingRhinos.Profiler.BackEnd.Extensions
{
	public enum FormattingOptions
	{
		ForceSingleLine,
		AllowMultiLine
	}
}