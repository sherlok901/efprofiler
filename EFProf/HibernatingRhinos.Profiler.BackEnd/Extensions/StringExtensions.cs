using System;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;

namespace HibernatingRhinos.Profiler.BackEnd.Extensions
{
	public static class StringExtensions
	{
		public static bool ContainsIgnoreCase(this string dialect, string databaseIdentifier)
		{
			if (dialect == null || databaseIdentifier == null)
			{
				return false;
			}
			return dialect.IndexOf(databaseIdentifier, StringComparison.OrdinalIgnoreCase) != -1;
		}

		public static int CountSubstrings(this string self, string substring)
		{
			if (self == null)
			{
				return 0;
			}
			int num = 0;
			int length = 0;
			while (true)
			{
				int num1 = self.IndexOf(substring, length, StringComparison.InvariantCultureIgnoreCase);
				length = num1;
				if (num1 == -1)
				{
					break;
				}
				length += substring.Length;
				num++;
			}
			return num;
		}

		public static bool Empty(this string self)
		{
			return string.IsNullOrEmpty(self);
		}

		public static string First50Characters(this string self)
		{
			return self.FirstCharacters(50, FormattingOptions.ForceSingleLine);
		}

		public static string FirstCharacters(this string self, int numOfChars, FormattingOptions formattingOptions)
		{
			if (self == null)
			{
				return "";
			}
			if (formattingOptions == FormattingOptions.ForceSingleLine)
			{
				self = self.Replace("\n", " ").Replace("\r", " ").Replace("  ", " ");
			}
			if (self.Length < numOfChars)
			{
				return self;
			}
			return string.Concat(self.Substring(0, numOfChars - 3), "...");
		}

		public static string MakeStatementFromPascalCase(this string self)
		{
			bool flag = true;
			StringBuilder stringBuilder = new StringBuilder(self);
			int num = 0;
			for (int i = 0; i < self.Length; i++)
			{
				if (char.IsUpper(self[i]) && !flag)
				{
					stringBuilder.Insert(i + num, ' ');
					num++;
				}
				flag = char.IsUpper(self[i]);
			}
			return stringBuilder.ToString();
		}

		public static bool StartsWithAny(this string self, params string[] candidates)
		{
			return self.StartsWithAny(false, candidates);
		}

		private static bool StartsWithAny(this string self, bool ignoreCase, params string[] candidates)
		{
			string[] strArrays = candidates;
			for (int i = 0; i < (int)strArrays.Length; i++)
			{
				if (self.StartsWith(strArrays[i], ignoreCase, Thread.CurrentThread.CurrentCulture))
				{
					return true;
				}
			}
			return false;
		}

		public static bool StartsWithAnyIgnoreCase(this string self, params string[] candidates)
		{
			return self.StartsWithAny(true, candidates);
		}

		public static string Truncate(this string self, int numOfChars, FormattingOptions formattingOptions)
		{
			if (self == null)
			{
				return "";
			}
			if (formattingOptions == FormattingOptions.ForceSingleLine)
			{
				self = self.Replace(Environment.NewLine, " ");
			}
			if (self.Length < numOfChars)
			{
				return self;
			}
			int num = numOfChars / 4 * 3 - 3;
			if (num < 1)
			{
				return self.Substring(0, numOfChars);
			}
			int num1 = numOfChars / 4;
			return string.Concat(self.Substring(0, num), "...", self.Substring(self.Length - num1));
		}
	}
}