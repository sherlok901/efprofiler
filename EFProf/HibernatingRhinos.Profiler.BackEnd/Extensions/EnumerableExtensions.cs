using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.Extensions
{
	public static class EnumerableExtensions
	{
		public static IEnumerable<TSource> DistinctBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
		{
			HashSet<TKey> tKeys = new HashSet<TKey>();
			foreach (TSource tSource in source)
			{
				if (!tKeys.Add(keySelector(tSource)))
				{
					continue;
				}
				yield return tSource;
			}
		}
	}
}