using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.Extensions
{
	public static class EnumerableExtension
	{
		public static void ForEach<T>(this IEnumerable<T> self, Action<T> action)
		{
			foreach (T t in self)
			{
				action(t);
			}
		}
	}
}