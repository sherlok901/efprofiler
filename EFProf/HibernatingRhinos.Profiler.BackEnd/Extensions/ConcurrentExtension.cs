using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.Extensions
{
	public static class ConcurrentExtension
	{
		public static ConcurrentQueue<T> ClearMatches<T>(this ConcurrentQueue<T> self, Predicate<T> match)
		{
			List<T> list = self.ToList<T>();
			list.RemoveAll(match);
			return new ConcurrentQueue<T>(list);
		}

		public static void EnqueueMaxCapacity<T>(this ConcurrentQueue<T> self, T item, int max)
		{
			T t;
			self.Enqueue(item);
			while (self.Count > max)
			{
				self.TryDequeue(out t);
			}
		}

		public static ConcurrentQueue<T> Remove<T>(this ConcurrentQueue<T> self, T item)
		{
			List<T> list = self.ToList<T>();
			list.Remove(item);
			return new ConcurrentQueue<T>(list);
		}

		public static ConcurrentQueue<T> Remove<T>(this ConcurrentQueue<T> self, Func<T, bool> predicate)
		{
			List<T> list = self.ToList<T>();
			T t = list.FirstOrDefault<T>(predicate);
			if (!object.ReferenceEquals(t, default(T)))
			{
				list.Remove(t);
			}
			return new ConcurrentQueue<T>(list);
		}
	}
}