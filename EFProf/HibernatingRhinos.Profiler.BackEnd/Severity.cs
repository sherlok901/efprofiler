using System;

namespace HibernatingRhinos.Profiler.BackEnd
{
	public enum Severity
	{
		Suggestion = 5,
		Warning = 10,
		Major = 15
	}
}