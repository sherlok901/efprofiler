using HibernatingRhinos.Profiler.Appender.StackTraces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd
{
	public class ProfilerConfiguration
	{
		public HashSet<string> AvailableEditors
		{
			get;
			set;
		}

		public string DefaultEditor
		{
			get;
			set;
		}

		public HashSet<string> DisabledAlerts
		{
			get;
			set;
		}

		public string EditorPath
		{
			get;
			set;
		}

		public IList<IgnoredAlertInfo> IgnoredAlertsOnStatements
		{
			get;
			set;
		}

		public HashSet<string> InfrastructureNamespaces
		{
			get;
			set;
		}

		public int MaxNumberOfCachedStatementsPerSession
		{
			get;
			set;
		}

		public int MaxNumberOfExpressionsPerWhereClause
		{
			get;
			set;
		}

		public int MaxNumberOfJoinsPerStatement
		{
			get;
			set;
		}

		public int MaxNumberOfNestingSelectStatement
		{
			get;
			set;
		}

		public int MaxNumberOfStatementsPerSession
		{
			get;
			set;
		}

		public int MaxNumberOfTablesPerStatement
		{
			get;
			set;
		}

		public int MaxNumberOfWhereClausePerStatement
		{
			get;
			set;
		}

		public int MaxRepeatedStatementsForSelectNPlusOne
		{
			get;
			set;
		}

		public int MaxRepeatedStatementsForUsingStatementBatching
		{
			get;
			set;
		}

		public int MaxRowCountForExcessiveRowCountSuggestion
		{
			get;
			set;
		}

		public int MaxRowCountForExcessiveRowCountWarning
		{
			get;
			set;
		}

		public HashSet<string> NonInfrastructureNamespaces
		{
			get;
			set;
		}

		public string TempFolderPath
		{
			get;
			set;
		}

		public ProfilerConfiguration()
		{
			HashSet<string> strs = new HashSet<string>();
			strs.Add("NHibernate");
			strs.Add("log4net");
			strs.Add("Castle.");
			strs.Add("System.");
			strs.Add("Microsoft.");
			strs.Add("HibernatingRhinos.Profiler.Appender");
			strs.Add("Rhino.Commons");
			strs.Add("System.Data");
			this.InfrastructureNamespaces = strs;
			HashSet<string> strs1 = new HashSet<string>();
			strs1.Add("Visual Studio");
			strs1.Add("NotePad++");
			strs1.Add("Other");
			this.AvailableEditors = strs1;
			HashSet<string> strs2 = new HashSet<string>();
			strs2.Add("hibernatingrhinos.hibernate.profiler.integration");
			this.NonInfrastructureNamespaces = strs2;
			this.DisabledAlerts = new HashSet<string>();
			this.IgnoredAlertsOnStatements = new List<IgnoredAlertInfo>();
			this.MaxRepeatedStatementsForSelectNPlusOne = 3;
			this.MaxRepeatedStatementsForUsingStatementBatching = 4;
			this.MaxRowCountForExcessiveRowCountSuggestion = 500;
			this.MaxRowCountForExcessiveRowCountWarning = 1000;
			this.MaxNumberOfStatementsPerSession = 30;
			this.MaxNumberOfCachedStatementsPerSession = 60;
			this.MaxNumberOfJoinsPerStatement = 3;
			this.MaxNumberOfTablesPerStatement = 5;
			this.MaxNumberOfNestingSelectStatement = 3;
			this.MaxNumberOfWhereClausePerStatement = 4;
			this.MaxNumberOfExpressionsPerWhereClause = 9;
			this.DefaultEditor = this.AvailableEditors.ToList<string>()[0];
		}

		public void EnsureIsValid()
		{
			ProfilerConfiguration profilerConfiguration = new ProfilerConfiguration();
			if (this.AvailableEditors == null)
			{
				this.AvailableEditors = profilerConfiguration.AvailableEditors;
			}
			if (this.AvailableEditors.FirstOrDefault<string>((string x) => x == this.DefaultEditor) == null)
			{
				this.DefaultEditor = this.AvailableEditors.ToList<string>()[0];
			}
			if (this.MaxNumberOfJoinsPerStatement == 0)
			{
				this.MaxNumberOfJoinsPerStatement = 3;
			}
			if (this.MaxNumberOfTablesPerStatement <= 0)
			{
				this.MaxNumberOfTablesPerStatement = 5;
			}
			if (this.MaxNumberOfNestingSelectStatement < 1)
			{
				this.MaxNumberOfNestingSelectStatement = 3;
			}
			if (this.MaxNumberOfWhereClausePerStatement < 1)
			{
				this.MaxNumberOfWhereClausePerStatement = 4;
			}
			if (this.MaxNumberOfExpressionsPerWhereClause < 1)
			{
				this.MaxNumberOfExpressionsPerWhereClause = 9;
			}
			if (this.DisabledAlerts == null)
			{
				this.DisabledAlerts = profilerConfiguration.DisabledAlerts;
			}
			if (this.InfrastructureNamespaces == null)
			{
				this.InfrastructureNamespaces = profilerConfiguration.InfrastructureNamespaces;
			}
			if (this.NonInfrastructureNamespaces == null)
			{
				this.NonInfrastructureNamespaces = profilerConfiguration.NonInfrastructureNamespaces;
			}
			if (this.IgnoredAlertsOnStatements == null)
			{
				this.IgnoredAlertsOnStatements = profilerConfiguration.IgnoredAlertsOnStatements;
			}
		}

		public string[] FindIgnoredAlerts(string rawSql, StackTraceInfo stackTrace)
		{
			List<string> strs = new List<string>();
			foreach (IgnoredAlertInfo ignoredAlertsOnStatement in this.IgnoredAlertsOnStatements)
			{
				if (ignoredAlertsOnStatement.RawSql != rawSql || stackTrace == null)
				{
					continue;
				}
				StackTraceFrame[] frames = stackTrace.Frames;
				if (ignoredAlertsOnStatement.StackTraceFrames == null || (int)frames.Length != (int)ignoredAlertsOnStatement.StackTraceFrames.Length)
				{
					continue;
				}
				strs.Add(ignoredAlertsOnStatement.Alert);
			}
			return strs.ToArray();
		}

		public string GetTempFileName()
		{
			if (string.IsNullOrEmpty(this.TempFolderPath) || !Directory.Exists(this.TempFolderPath))
			{
				return Path.GetTempFileName();
			}
			string tempFolderPath = this.TempFolderPath;
			Guid guid = Guid.NewGuid();
			return Path.Combine(tempFolderPath, guid.ToString());
		}
	}
}