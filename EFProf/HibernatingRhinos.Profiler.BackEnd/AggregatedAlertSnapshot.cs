using HibernatingRhinos.Profiler.BackEnd.Profiles;
using HibernatingRhinos.Profiler.BackEnd.Reports;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd
{
	public class AggregatedAlertSnapshot : IAggregatedAlertSnapshot
	{
		public AlertInformation Alert
		{
			get
			{
				return JustDecompileGenerated_get_Alert();
			}
			set
			{
				JustDecompileGenerated_set_Alert(value);
			}
		}

		private AlertInformation JustDecompileGenerated_Alert_k__BackingField;

		public AlertInformation JustDecompileGenerated_get_Alert()
		{
			return this.JustDecompileGenerated_Alert_k__BackingField;
		}

		public void JustDecompileGenerated_set_Alert(AlertInformation value)
		{
			this.JustDecompileGenerated_Alert_k__BackingField = value;
		}

		public string AlertTitle
		{
			get;
			set;
		}

		public int Count
		{
			get
			{
				return JustDecompileGenerated_get_Count();
			}
			set
			{
				JustDecompileGenerated_set_Count(value);
			}
		}

		private int JustDecompileGenerated_Count_k__BackingField;

		public int JustDecompileGenerated_get_Count()
		{
			return this.JustDecompileGenerated_Count_k__BackingField;
		}

		public void JustDecompileGenerated_set_Count(int value)
		{
			this.JustDecompileGenerated_Count_k__BackingField = value;
		}

		public string HelpUri
		{
			get;
			set;
		}

		public List<RelatedSessionSpecification> RelatedSessions
		{
			get;
			set;
		}

		public AggregatedAlertSnapshot(AlertInformation alert)
		{
			this.Alert = alert;
			this.HelpUri = Profile.Current.GetLearnTopic("alert", this.Alert.HelpTopic);
			this.AlertTitle = Profile.Current.Translate(this.Alert.Title);
		}
	}
}