using HibernatingRhinos.Profiler.BackEnd.Infrastructure.Connections;
using System;
using System.Collections.Generic;

namespace HibernatingRhinos.Profiler.BackEnd.Profiles
{
	public abstract class ProfilerBase : IProfile
	{
		public string AboutUrl
		{
			get
			{
				return string.Concat(this.RootUrl, "/about");
			}
		}

		public string BuyUrl
		{
			get
			{
				return string.Concat(this.HomeUrl, "/buy");
			}
		}

		public string CommercialSupportUrl
		{
			get
			{
				return string.Concat(this.RootUrl, "/services/support");
			}
		}

		public abstract int DefaultPort
		{
			get;
		}

		public string DownloadUrl
		{
			get
			{
				return string.Concat(this.RootUrl, "/downloads/", this.ShortName, "/latest");
			}
		}

		public string ErrorUrl
		{
			get
			{
				return string.Concat(this.RootUrl, "/exception/report");
			}
		}

		public string HomeUrl
		{
			get
			{
				return string.Concat(this.RootUrl, "/products/", this.ShortName);
			}
		}

		public string LatestVersionDownloadUrl
		{
			get
			{
				return string.Format("{0}/downloads/{1}/latest", this.RootUrl, this.ShortName.ToLowerInvariant());
			}
		}

		public string LatestVersionUrl
		{
			get
			{
				return string.Format("{0}/downloads/{1}/latestVersion", this.RootUrl, this.ShortName.ToLowerInvariant());
			}
		}

		public abstract string ProfileName
		{
			get;
		}

		public string RootUrl
		{
			get
			{
				return "http://hibernatingrhinos.com";
			}
		}

		public abstract string ShortName
		{
			get;
		}

		public string SiteTrialUrl
		{
			get
			{
				return string.Concat(this.HomeUrl, "/trial");
			}
		}

		public abstract string[] StatementProcessorNamespaces
		{
			get;
		}

		public string StatisticsUrl
		{
			get
			{
				return string.Format("http://{0}.uberprof.com", this.ShortName.ToLowerInvariant());
			}
		}

		public abstract IDictionary<string, string> Translations
		{
			get;
		}

		public string TrialExtendedUrl
		{
			get
			{
				return string.Concat(this.HomeUrl, "/trialextension-profiler");
			}
		}

		public string TrialUrl
		{
			get
			{
				return string.Concat(this.HomeUrl, "/trial-profiler");
			}
		}

		protected ProfilerBase()
		{
		}

		public virtual IList<IConnectionDefinition> GetAvailableConnectionTypes()
		{
			List<IConnectionDefinition> connectionDefinitions = new List<IConnectionDefinition>()
			{
				new SqlServerConnectionDefinition(),
				new OracleConnectionDefinition(),
				new ManagedOracleConnectionDefinition(),
				new MySqlConnectionDefinition(),
				new NpgsqlConnectionDefinition(),
				new SQLiteConnectionDefinition(),
				new Firebird1ConnectionDefinition(),
				new Firebird2ConnectionDefinition(),
				new Firebird25ConnectionDefinition(),
				new SqlServerCeConnectionDefinition(),
				new OdbcConnectionDefinition(),
				new OleDbConnectionDefinition(),
				new IBMiSeriesConnectionDefinition(),
				new IbmDb2ConnectionDefinition()
			};
			return connectionDefinitions;
		}

		public string GetLearnTopic(string type, string slug)
		{
			return string.Format("{0}/learn/{1}/{2}", this.HomeUrl, type, slug);
		}

		public bool IsMatch(string licenseProfile)
		{
			return this.ShortName.Equals(licenseProfile, StringComparison.InvariantCultureIgnoreCase);
		}

		public abstract bool Supports(SupportedFeatures feature);
	}
}