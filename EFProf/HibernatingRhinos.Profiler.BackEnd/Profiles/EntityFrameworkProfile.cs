using System;
using System.Collections.Generic;

namespace HibernatingRhinos.Profiler.BackEnd.Profiles
{
	public class EntityFrameworkProfile : ProfilerBase
	{
		private readonly IDictionary<string, string> translations = new Dictionary<string, string>()
		{
			{ "[[session]]", "object context" },
			{ "[[sessionfactory]]", "application" },
			{ "[[sessions]]", "object contexts" },
			{ "[[profile]]", "Entity Framework" },
			{ "[[profile-note]]", "" }
		};

		public override int DefaultPort
		{
			get
			{
				return 22898;
			}
		}

		public override string ProfileName
		{
			get
			{
				return "EntityFramework";
			}
		}

		public override string ShortName
		{
			get
			{
				return "EFProf";
			}
		}

		public override string[] StatementProcessorNamespaces
		{
			get
			{
				return new string[] { "EntityFramework" };
			}
		}

		public bool SupportsCommercialSupport
		{
			get
			{
				return false;
			}
		}

		public bool SupportsRowCount
		{
			get
			{
				return true;
			}
		}

		public override IDictionary<string, string> Translations
		{
			get
			{
				return this.translations;
			}
		}

		public EntityFrameworkProfile()
		{
		}

		public override bool Supports(SupportedFeatures feature)
		{
			switch (feature)
			{
				case SupportedFeatures.Transactions:
				case SupportedFeatures.Duration:
				case SupportedFeatures.RowCount:
				{
					return true;
				}
				case SupportedFeatures.Cache:
				case SupportedFeatures.Batching:
				case SupportedFeatures.Entities:
				case SupportedFeatures.CommercialSupport:
				{
					return false;
				}
			}
			return false;
		}
	}
}