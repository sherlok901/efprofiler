using System;

namespace HibernatingRhinos.Profiler.BackEnd.Profiles
{
	public enum SupportedFeatures
	{
		Transactions,
		Duration,
		RowCount,
		Cache,
		Batching,
		Entities,
		CommercialSupport,
		RemoteConnections
	}
}