using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.Profiles
{
	public static class ProfileExtensions
	{
		public static string Translate(this IProfile self, string str)
		{
			bool flag = str.StartsWith("[[");
			foreach (KeyValuePair<string, string> translation in self.Translations)
			{
				str = str.Replace(translation.Key, translation.Value);
			}
			if (flag && str.Length > 1)
			{
				str = string.Concat(char.ToUpper(str[0]), str.Substring(1));
			}
			return str;
		}
	}
}