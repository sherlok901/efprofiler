using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.Profiles
{
	public static class Profile
	{
		private static IProfile current;

		public static IProfile Current
		{
			get
			{
				IProfile profile = Profile.current;
				if (profile == null)
				{
					profile = Profile.InstantiateProfile(Profile.GetCurrentProfile());
					Profile.current = profile;
				}
				return profile;
			}
			set
			{
				Profile.current = value;
			}
		}

		public static string CurrentProfile
		{
			get
			{
				return Profile.Current.ProfileName;
			}
		}

		public static string CurrentProfileDisplayName
		{
			get
			{
				return string.Concat(Profile.Current.ProfileName, " Profiler");
			}
		}

		private static string GetCurrentProfile()
		{
			string str;
			Assembly assembly = typeof(Profile).Assembly;
			string str1 = assembly.GetManifestResourceNames().First<string>((string x) => x.EndsWith("Profile.Info"));
			using (Stream manifestResourceStream = assembly.GetManifestResourceStream(str1))
			{
				using (StreamReader streamReader = new StreamReader(manifestResourceStream))
				{
					str = streamReader.ReadToEnd().Trim();
				}
			}
			return str;
		}

		public static IProfile InstantiateProfile(string profile)
		{
			string str = string.Concat("HibernatingRhinos.Profiler.BackEnd.Profiles.", profile, "Profile");
			Type type = typeof(IProfile).Assembly.GetType(str);
			if (type == null)
			{
				throw new InvalidOperationException(string.Concat("Could not find ", str));
			}
			return (IProfile)Activator.CreateInstance(type);
		}
	}
}