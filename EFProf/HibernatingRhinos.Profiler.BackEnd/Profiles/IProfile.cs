using HibernatingRhinos.Profiler.BackEnd.Infrastructure.Connections;
using System;
using System.Collections.Generic;

namespace HibernatingRhinos.Profiler.BackEnd.Profiles
{
	public interface IProfile
	{
		string AboutUrl
		{
			get;
		}

		string BuyUrl
		{
			get;
		}

		string CommercialSupportUrl
		{
			get;
		}

		int DefaultPort
		{
			get;
		}

		string DownloadUrl
		{
			get;
		}

		string ErrorUrl
		{
			get;
		}

		string HomeUrl
		{
			get;
		}

		string LatestVersionDownloadUrl
		{
			get;
		}

		string LatestVersionUrl
		{
			get;
		}

		string ProfileName
		{
			get;
		}

		string RootUrl
		{
			get;
		}

		string ShortName
		{
			get;
		}

		string SiteTrialUrl
		{
			get;
		}

		string[] StatementProcessorNamespaces
		{
			get;
		}

		string StatisticsUrl
		{
			get;
		}

		IDictionary<string, string> Translations
		{
			get;
		}

		string TrialExtendedUrl
		{
			get;
		}

		string TrialUrl
		{
			get;
		}

		IList<IConnectionDefinition> GetAvailableConnectionTypes();

		string GetLearnTopic(string type, string slug);

		bool IsMatch(string licenseProfile);

		bool Supports(SupportedFeatures feature);
	}
}