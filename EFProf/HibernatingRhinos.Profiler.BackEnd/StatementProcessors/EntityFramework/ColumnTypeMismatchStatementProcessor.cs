using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.Messages;
using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.StatementProcessors.EntityFramework
{
	public class ColumnTypeMismatchStatementProcessor : AbstractStatementProcessor
	{
		public ColumnTypeMismatchStatementProcessor()
		{
		}

		public override void AfterAttachingToSession(Session session, Statement statement)
		{
			if (!statement.IsSelectStatement)
			{
				return;
			}
			if (statement.Tables == null || (int)statement.Tables.Length == 0)
			{
				return;
			}
			if (statement.Parameters != null && (int)statement.Parameters.Length != 0)
			{
				if (!((IEnumerable<Parameter>)statement.Parameters).Any<Parameter>((Parameter x) => x.ColumnType == null))
				{
					if (((IEnumerable<Parameter>)statement.Parameters).Any<Parameter>((Parameter x) => x.IsColumnTypeMismatch(true)))
					{
						statement.AcceptAlert(AlertInformation.ColumnTypeMismatch);
					}
					return;
				}
			}
		}
	}
}