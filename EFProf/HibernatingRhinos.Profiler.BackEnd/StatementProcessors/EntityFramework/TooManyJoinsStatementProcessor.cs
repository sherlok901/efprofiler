using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using System;
using System.Linq;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.StatementProcessors.EntityFramework
{
	public class TooManyJoinsStatementProcessor : AbstractStatementProcessor
	{
		public TooManyJoinsStatementProcessor()
		{
		}

		public override void AfterAttachingToSession(Session session, Statement statement)
		{
			if (statement.RawSql.Split(new char[0]).Count<string>((string x) => {
				if ("join".Equals(x, StringComparison.InvariantCultureIgnoreCase))
				{
					return true;
				}
				return "apply".Equals(x, StringComparison.InvariantCultureIgnoreCase);
			}) <= base.Configuration.MaxNumberOfJoinsPerStatement)
			{
				return;
			}
			statement.AcceptAlert(AlertInformation.TooManyJoins);
		}
	}
}