using HibernatingRhinos.Profiler.Appender.StackTraces;
using System;

namespace HibernatingRhinos.Profiler.BackEnd.StatementProcessors
{
	public class WinFormsQueriesInDataBinding : QueriesInDataBinding
	{
		public WinFormsQueriesInDataBinding()
		{
		}

		public override bool DoesFrameMatch(StackTraceFrame frame)
		{
			if (frame.Type == "System.Windows.Forms.BindingContext")
			{
				return true;
			}
			return frame.Type == "System.Windows.Forms.CurrencyManager";
		}
	}
}