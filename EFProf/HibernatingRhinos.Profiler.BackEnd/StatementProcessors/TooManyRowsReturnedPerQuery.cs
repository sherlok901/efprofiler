using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.StatementProcessors
{
	public class TooManyRowsReturnedPerQuery : AbstractStatementProcessor
	{
		private readonly Action<Statement> statementOnValuesRefreshed;

		public TooManyRowsReturnedPerQuery()
		{
			this.statementOnValuesRefreshed = new Action<Statement>(this.ExecuteCheck);
		}

		public override void AfterAttachingToSession(Session session, Statement statement)
		{
			statement.ValuesRefreshed += this.statementOnValuesRefreshed;
			this.ExecuteCheck(statement);
		}

		private void ExecuteCheck(Statement statement)
		{
			if (statement.QueryCountWasProcessed || statement.CountOfRows.Count == 0)
			{
				return;
			}
			statement.QueryCountWasProcessed = true;
			if (statement.CountOfRows.Any<int>((int x) => x > base.Configuration.MaxRowCountForExcessiveRowCountWarning))
			{
				statement.AcceptAlert(AlertInformation.ExcessiveNumberOfRowsWarning);
				return;
			}
			if (statement.CountOfRows.Any<int>((int x) => x > base.Configuration.MaxRowCountForExcessiveRowCountSuggestion))
			{
				statement.AcceptAlert(AlertInformation.ExcessiveNumberOfRowsSuggestion);
			}
		}
	}
}