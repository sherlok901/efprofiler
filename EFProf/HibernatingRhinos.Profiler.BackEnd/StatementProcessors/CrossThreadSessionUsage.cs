using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.Messages;
using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using System;

namespace HibernatingRhinos.Profiler.BackEnd.StatementProcessors
{
	public class CrossThreadSessionUsage : AbstractStatementProcessor
	{
		public CrossThreadSessionUsage()
		{
		}

		public override void AfterAttachingToSession(Session session, Statement statement)
		{
			if (statement.Options.HasFlag(SqlStatementOptions.Transaction) || statement.Options.HasFlag(SqlStatementOptions.DtcTransaction))
			{
				return;
			}
			if (session.Id.ThreadId == statement.SessionId.ThreadId)
			{
				return;
			}
			if (session.IsInDtcTransaction)
			{
				return;
			}
			statement.AcceptAlert(AlertInformation.CrossThreadSessionUsage);
		}
	}
}