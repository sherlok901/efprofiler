using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.Extensions;
using HibernatingRhinos.Profiler.BackEnd.Messages;
using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.StatementProcessors
{
	public class DetectUncachedQueryPlans : AbstractStatementProcessor
	{
		public DetectUncachedQueryPlans()
		{
		}

		public override void AfterAttachingToSession(Session session, Statement statement)
		{
			Parameter parameter = statement.Parameters.FirstOrDefault<Parameter>();
			if (parameter != null && !parameter.Dialect.ContainsIgnoreCase("MsSql"))
			{
				return;
			}
			IEnumerable<Statement> statements = 
				from x in session.Statements
				where x.RawSql == statement.RawSql
				select x;
			bool flag = false;

            foreach (Statement statement1 in statements)
            {
                if (statement1.Parameters.Length == statement.Parameters.Length)
                {
                    for (int index = 0; index < statement.Parameters.Length; ++index)
                    {
                        if (statement.Parameters[index].Type != statement1.Parameters[index].Type)
                        {
                            flag = true;
                            break;
                        }
                    }
                    if (flag)
                        break;
                }
            }

            //using (IEnumerator<Statement> enumerator = statements.GetEnumerator())
            //{
            //do
            //{
            //Label1:
            //	if (!enumerator.MoveNext())
            //	{
            //		break;
            //	}
            //	Statement current = enumerator.Current;
            //	if ((int)current.Parameters.Length == (int)statement.Parameters.Length)
            //	{
            //		int num = 0;
            //		while (num < (int)statement.Parameters.Length)
            //		{
            //			if (statement.Parameters[num].Type == current.Parameters[num].Type)
            //			{
            //				num++;
            //			}
            //			else
            //			{
            //				flag = true;
            //				goto Label0;
            //			}
            //		}
            //	}
            //	else
            //	{
            //		goto Label1;
            //	}
            //Label0:
            //}
            //while (!flag);
            //}
            if (!flag)
			{
				return;
			}
			statement.AcceptAlert(AlertInformation.UncachedQueryPlan);
		}
	}
}