using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.Messages;
using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using System;

namespace HibernatingRhinos.Profiler.BackEnd.StatementProcessors
{
	public class TooManyDatabaseCallsPerSession : AbstractStatementProcessor
	{
		public TooManyDatabaseCallsPerSession()
		{
		}

		public override void AfterAttachingToSession(Session session, Statement statement)
		{
			if (statement.Options.HasFlag(SqlStatementOptions.Transaction) || statement.Options.HasFlag(SqlStatementOptions.DtcTransaction) || statement.Options.HasFlag(SqlStatementOptions.Cached) || statement.Options.HasFlag(SqlStatementOptions.DDL))
			{
				return;
			}
			if (session.CountOfStandardDatabaseStatements < base.Configuration.MaxNumberOfStatementsPerSession)
			{
				return;
			}
			statement.AcceptAlert(AlertInformation.TooManyDatabaseCalls);
		}
	}
}