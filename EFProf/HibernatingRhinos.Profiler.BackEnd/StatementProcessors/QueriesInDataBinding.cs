using HibernatingRhinos.Profiler.Appender.StackTraces;
using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using System;
using System.Linq;

namespace HibernatingRhinos.Profiler.BackEnd.StatementProcessors
{
	public abstract class QueriesInDataBinding : AbstractStatementProcessor
	{
		protected QueriesInDataBinding()
		{
		}

		public override void AfterAttachingToSession(Session session, Statement statement)
		{
			if (statement.StackTrace == null)
			{
				return;
			}
			QueriesInDataBinding queriesInDataBinding = this;
			if (statement.UnfilteredStackTrace.Frames.Any<StackTraceFrame>(new Func<StackTraceFrame, bool>(queriesInDataBinding.DoesFrameMatch)))
			{
				statement.AcceptAlert(AlertInformation.DataBindingQueries);
			}
		}

		public abstract bool DoesFrameMatch(StackTraceFrame frame);
	}
}