using HibernatingRhinos.Profiler.Appender.StackTraces;
using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using System;
using System.Linq;

namespace HibernatingRhinos.Profiler.BackEnd.StatementProcessors
{
	public class AspNetQueriesInDataBinding : AbstractStatementProcessor
	{
		public AspNetQueriesInDataBinding()
		{
		}

		public override void AfterAttachingToSession(Session session, Statement statement)
		{
			if (statement.StackTrace == null)
			{
				return;
			}
			if (!statement.UnfilteredStackTrace.Frames.Any<StackTraceFrame>(new Func<StackTraceFrame, bool>(this.DoesFrameMatch)))
			{
				return;
			}
			if (!statement.UnfilteredStackTrace.Frames.Any<StackTraceFrame>(new Func<StackTraceFrame, bool>(this.IsMvcStackTrace)))
			{
				return;
			}
			if (statement.UnfilteredStackTrace.Frames.Any<StackTraceFrame>(new Func<StackTraceFrame, bool>(this.IsRenderActionStackTrace)))
			{
				return;
			}
			statement.AcceptAlert(AlertInformation.QueriesFromViews);
		}

		private bool DoesFrameMatch(StackTraceFrame frame)
		{
			if (frame.Type.EndsWith("_aspx") || frame.Type.EndsWith("_ascx"))
			{
				return true;
			}
			return frame.Type.EndsWith("_master");
		}

		private bool IsMvcStackTrace(StackTraceFrame frame)
		{
			return frame.Type == "System.Web.Mvc.ViewPage";
		}

		private bool IsRenderActionStackTrace(StackTraceFrame frame)
		{
			return frame.Method == "RenderAction";
		}
	}
}