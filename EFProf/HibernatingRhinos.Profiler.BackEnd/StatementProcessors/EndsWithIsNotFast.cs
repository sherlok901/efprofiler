using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.Messages;
using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using System;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;

namespace HibernatingRhinos.Profiler.BackEnd.StatementProcessors
{
	public class EndsWithIsNotFast : AbstractStatementProcessor
	{
		private readonly Regex endWithUsingRight = new Regex("RIGHT\\( \\s* ([\\[\\].\\w]+) \\s*, \\s* ([\\[\\].'\\w ()]+) \\s* \\) \\s* =", RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.IgnorePatternWhitespace);

		private readonly Regex containsQueryUsingCharIndex = new Regex("CHARINDEX\\( \\s* (.+) \\s*, \\s* ([\\[\\].'\\w ()]+) \\s* \\) \\s* > \\s* 0", RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.IgnorePatternWhitespace);

		private readonly Regex endsWithLikeUsingConstantValue = new Regex("like \\s+ N?'%", RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.IgnorePatternWhitespace);

		private readonly Regex parameterForLike = new Regex("like \\s+ ([@?:]p\\d+)", RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.IgnorePatternWhitespace);

		public EndsWithIsNotFast()
		{
		}

		public override void AfterAttachingToSession(Session session, Statement statement)
		{
			if (!this.HasConstantLikeParameter(statement) && !this.HasEndsWithUsingRight(statement) && !this.HasLikeParameterStartingWithPrecentage(statement) && !this.HasContainsQueryUsingCharIndex(statement))
			{
				return;
			}
			statement.AcceptAlert(AlertInformation.EndsWithWillForceTableScan);
		}

		private bool HasConstantLikeParameter(Statement statement)
		{
			return this.endsWithLikeUsingConstantValue.Match(statement.RawSql).Success;
		}

		private bool HasContainsQueryUsingCharIndex(Statement statement)
		{
			return this.containsQueryUsingCharIndex.Match(statement.RawSql).Success;
		}

		private bool HasEndsWithUsingRight(Statement statement)
		{
			return this.endWithUsingRight.Match(statement.RawSql).Success;
		}

		private bool HasLikeParameterStartingWithPrecentage(Statement statement)
		{
			return (
				from Match parameter in this.parameterForLike.Matches(statement.RawSql)
				select parameter.Groups[1].Value into value
				select statement.Parameters.FirstOrDefault<Parameter>((Parameter x) => x.Name == value)).Any<Parameter>((Parameter parameter) => {
				if (parameter == null)
				{
					return false;
				}
				return parameter.Value.StartsWith("'%");
			});
		}
	}
}