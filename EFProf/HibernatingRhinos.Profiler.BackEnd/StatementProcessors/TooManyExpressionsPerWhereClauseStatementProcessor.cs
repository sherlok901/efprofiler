using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using HibernatingRhinos.Profiler.BackEnd.Util;
using System;

namespace HibernatingRhinos.Profiler.BackEnd.StatementProcessors
{
	internal class TooManyExpressionsPerWhereClauseStatementProcessor : AbstractStatementProcessor
	{
		public TooManyExpressionsPerWhereClauseStatementProcessor()
		{
		}

		public override void AfterAttachingToSession(Session session, Statement statement)
		{
			if (!statement.IsSelectStatement || statement.StatementsAnalysis == null)
			{
				return;
			}
			if (statement.StatementsAnalysis.MaxAndOrCountInWhereClause >= base.Configuration.MaxNumberOfExpressionsPerWhereClause)
			{
				statement.AcceptAlert(AlertInformation.TooManyExpressionsPerWhereClause);
			}
		}
	}
}