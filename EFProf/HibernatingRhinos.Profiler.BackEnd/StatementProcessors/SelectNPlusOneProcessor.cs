using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using System;
using System.Text.RegularExpressions;

namespace HibernatingRhinos.Profiler.BackEnd.StatementProcessors
{
	public class SelectNPlusOneProcessor : AbstractStatementProcessor
	{
		private const string MySqlSelectLastIdentity = "SELECT LAST_INSERT_ID()";

		private readonly Regex oracleSequenceNextVal = new Regex("\\.nextval \\s+ from \\s+ dual", RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.IgnorePatternWhitespace);

		public SelectNPlusOneProcessor()
		{
		}

		public override void AfterAttachingToSession(Session session, Statement statement)
		{
			if (!statement.IsSelectStatement)
			{
				return;
			}
			if (statement.RawSql == "SELECT LAST_INSERT_ID()")
			{
				return;
			}
			if (this.oracleSequenceNextVal.IsMatch(statement.RawSql))
			{
				return;
			}
			if (session.GetCountOfStatementsByRawSql(statement.RawSql) <= base.Configuration.MaxRepeatedStatementsForSelectNPlusOne)
			{
				return;
			}
			statement.AcceptAlert(AlertInformation.SelectNPlusOne);
		}
	}
}