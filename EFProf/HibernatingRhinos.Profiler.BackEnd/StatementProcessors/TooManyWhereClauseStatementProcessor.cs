using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using HibernatingRhinos.Profiler.BackEnd.Util;
using System;

namespace HibernatingRhinos.Profiler.BackEnd.StatementProcessors
{
	public class TooManyWhereClauseStatementProcessor : AbstractStatementProcessor
	{
		public TooManyWhereClauseStatementProcessor()
		{
		}

		public override void AfterAttachingToSession(Session session, Statement statement)
		{
			if (!statement.IsSelectStatement || statement.StatementsAnalysis == null)
			{
				return;
			}
			if (statement.StatementsAnalysis.MaxWhereClauseInStatement >= base.Configuration.MaxNumberOfWhereClausePerStatement)
			{
				statement.AcceptAlert(AlertInformation.TooManyWhereClauses);
			}
		}
	}
}