using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using System;
using System.Linq;

namespace HibernatingRhinos.Profiler.BackEnd.StatementProcessors
{
	public class TooManyTablesStatementProcessor : AbstractStatementProcessor
	{
		public TooManyTablesStatementProcessor()
		{
		}

		public override void AfterAttachingToSession(Session session, Statement statement)
		{
			if (!statement.IsSelectStatement)
			{
				return;
			}
			if (statement.Tables.Count<string>() >= base.Configuration.MaxNumberOfTablesPerStatement)
			{
				statement.AcceptAlert(AlertInformation.TooManyTables);
			}
		}
	}
}