using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.Messages;
using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;

namespace HibernatingRhinos.Profiler.BackEnd.StatementProcessors
{
	public class UnboundedResultSetStatementProcessor : AbstractStatementProcessor
	{
		private readonly static Regex LimitKeywords;

		private readonly static Regex ColumnAliasName;

		private readonly static Regex InClause;

		private readonly static Regex MsSql2012OffsetSyntax;

		private readonly Action<Statement> statementOnValuesRefreshed;

		static UnboundedResultSetStatementProcessor()
		{
			UnboundedResultSetStatementProcessor.LimitKeywords = new Regex("[ (),](top|offset|limit|rownum|first)[ (),]", RegexOptions.IgnoreCase | RegexOptions.Compiled);
			UnboundedResultSetStatementProcessor.ColumnAliasName = new Regex("([ (),]|\\n)AS ((\\w|\\[|\\]|[_])*)([ (),]|\\n)", RegexOptions.IgnoreCase | RegexOptions.Compiled);
			UnboundedResultSetStatementProcessor.InClause = new Regex("in \\s* (?<Open>\\() (.*?) (?<Close-Open>\\))", RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.IgnorePatternWhitespace);
			UnboundedResultSetStatementProcessor.MsSql2012OffsetSyntax = new Regex("order\\s+by.+offset.+rows\\s+fetch\\s+next.+rows\\s+only", RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.Singleline);
		}

		public UnboundedResultSetStatementProcessor()
		{
			this.statementOnValuesRefreshed = new Action<Statement>(this.CheckStatementForUnboundedResultSet);
		}

		public override void AfterAttachingToSession(Session session, Statement statement)
		{
			if (statement.Options.HasFlag(SqlStatementOptions.DDL) || statement.Options.HasFlag(SqlStatementOptions.DtcTransaction) || statement.Options.HasFlag(SqlStatementOptions.Transaction) || statement.Options.HasFlag(SqlStatementOptions.Warning))
			{
				return;
			}
			if (statement.CountOfRows.Count > 0)
			{
				this.CheckStatementForUnboundedResultSet(statement);
				return;
			}
			statement.ValuesRefreshed += this.statementOnValuesRefreshed;
		}

		private void CheckStatementForUnboundedResultSet(Statement statement)
		{
			if (statement.UnboundedResultSetWasProcessed || statement.CountOfRows.Count == 0)
			{
				return;
			}
			statement.UnboundedResultSetWasProcessed = true;
			if (statement.CountOfRows.All<int>((int x) => x < 2))
			{
				return;
			}
			if (UnboundedResultSetStatementProcessor.LimitKeywords.IsMatch(statement.RawSql))
			{
				return;
			}
			MatchCollection matchCollections = UnboundedResultSetStatementProcessor.InClause.Matches(statement.RawSql);
			if (matchCollections.Count > 0)
			{
				int num = matchCollections.OfType<Match>().Max<Match>((Match match) => match.Groups[1].Value.Count<char>((char ch) => ch == ',') + 1);
				if (statement.CountOfRows.All<int>((int x) => num >= x))
				{
					return;
				}
			}
			if (UnboundedResultSetStatementProcessor.IsLimitedByRowNumber(statement.RawSql))
			{
				return;
			}
			if (this.IsLimitedByMsSql2012OffsetRowsFetchNextRowsOnly(statement.RawSql))
			{
				return;
			}
			statement.AcceptAlert(AlertInformation.UnboundedResultSet);
		}

		private bool IsLimitedByMsSql2012OffsetRowsFetchNextRowsOnly(string rawSql)
		{
			return UnboundedResultSetStatementProcessor.MsSql2012OffsetSyntax.IsMatch(rawSql);
		}

		public static bool IsLimitedByRowNumber(string rawSql)
		{
			int num = rawSql.IndexOf("ROW_NUMBER()", StringComparison.OrdinalIgnoreCase);
			if (num == -1)
			{
				return false;
			}
			string str = rawSql.Substring(num + "ROW_NUMBER()".Length);
			Match match = UnboundedResultSetStatementProcessor.ColumnAliasName.Match(str);
			if (!match.Success)
			{
				return false;
			}
			string value = match.Groups[2].Value;
			return str.IndexOf(string.Format("{0} {1}", value, "BETWEEN"), StringComparison.OrdinalIgnoreCase) >= 0;
		}
	}
}