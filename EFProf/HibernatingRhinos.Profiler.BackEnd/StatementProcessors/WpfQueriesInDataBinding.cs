using HibernatingRhinos.Profiler.Appender.StackTraces;
using System;

namespace HibernatingRhinos.Profiler.BackEnd.StatementProcessors
{
	public class WpfQueriesInDataBinding : QueriesInDataBinding
	{
		public WpfQueriesInDataBinding()
		{
		}

		public override bool DoesFrameMatch(StackTraceFrame frame)
		{
			return frame.Type == "MS.Internal.Data.DataBindEngine";
		}
	}
}