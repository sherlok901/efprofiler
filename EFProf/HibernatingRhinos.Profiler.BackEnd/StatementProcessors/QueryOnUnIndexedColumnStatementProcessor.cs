using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.Messages;
using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.StatementProcessors
{
	internal class QueryOnUnIndexedColumnStatementProcessor : AbstractStatementProcessor
	{
		public QueryOnUnIndexedColumnStatementProcessor()
		{
		}

		public override void AfterAttachingToSession(Session session, Statement statement)
		{
			if (!statement.IsSelectStatement)
			{
				return;
			}
			if (statement.Tables == null || (int)statement.Tables.Length == 0)
			{
				return;
			}
			if (statement.Parameters != null && (int)statement.Parameters.Length != 0)
			{
				if (!((IEnumerable<Parameter>)statement.Parameters).Any<Parameter>((Parameter x) => x.ColumnType == null))
				{
					if (statement.IsUnIdexedColumn)
					{
						statement.AcceptAlert(AlertInformation.QueryOnUnIndexedColumn);
					}
					return;
				}
			}
		}
	}
}