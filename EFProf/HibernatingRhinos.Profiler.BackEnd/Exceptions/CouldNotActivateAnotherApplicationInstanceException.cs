using System;
using System.Runtime.Serialization;

namespace HibernatingRhinos.Profiler.BackEnd.Exceptions
{
	[Serializable]
	public class CouldNotActivateAnotherApplicationInstanceException : Exception
	{
		public CouldNotActivateAnotherApplicationInstanceException()
		{
		}

		public CouldNotActivateAnotherApplicationInstanceException(string message) : base(message)
		{
		}

		public CouldNotActivateAnotherApplicationInstanceException(string message, Exception inner) : base(message, inner)
		{
		}

		protected CouldNotActivateAnotherApplicationInstanceException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}