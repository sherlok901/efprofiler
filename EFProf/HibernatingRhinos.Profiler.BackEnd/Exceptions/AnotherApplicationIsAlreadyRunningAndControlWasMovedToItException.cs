using System;
using System.Runtime.Serialization;

namespace HibernatingRhinos.Profiler.BackEnd.Exceptions
{
	[Serializable]
	public class AnotherApplicationIsAlreadyRunningAndControlWasMovedToItException : Exception
	{
		public AnotherApplicationIsAlreadyRunningAndControlWasMovedToItException()
		{
		}

		public AnotherApplicationIsAlreadyRunningAndControlWasMovedToItException(string message) : base(message)
		{
		}

		public AnotherApplicationIsAlreadyRunningAndControlWasMovedToItException(string message, Exception inner) : base(message, inner)
		{
		}

		protected AnotherApplicationIsAlreadyRunningAndControlWasMovedToItException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}