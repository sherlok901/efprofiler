using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.BackEnd.Listeners;
using HibernatingRhinos.Profiler.BackEnd.Messages;
using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using System;
using System.Reactive.Linq;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.LoggingProcessors
{
	public class ProfiledApplicationStateProcessor : AbstractMessageProcessor
	{
		private readonly ApplicationInstanceDictionary profiledInstances = new ApplicationInstanceDictionary();

		public ProfiledApplicationStateProcessor()
		{
		}

		public override void Attach(EventsObserver eventsObserver)
		{
			eventsObserver.OfType<ApplicationAttached>().Subscribe<ApplicationAttached>((ApplicationAttached message) => {
				ApplicationInstanceInformation applicationInstanceInformation = new ApplicationInstanceInformation()
				{
					Id = message.Id,
					Name = message.Name,
					IsProductionApplication = message.IsProductionApplication
				};
				this.profiledInstances.Add(applicationInstanceInformation);
				base.ModelBuilder.Notifications.OnApplicationStatusChanged(applicationInstanceInformation);
			});
			eventsObserver.OfType<ApplicationDetached>().Subscribe<ApplicationDetached>((ApplicationDetached message) => {
				ApplicationInstanceInformation applicationInstanceInformation = this.profiledInstances.Get(message.Id);
				if (applicationInstanceInformation == null)
				{
					return;
				}
				this.profiledInstances.Remove(message.Id);
				applicationInstanceInformation.IsDetached = true;
				base.ModelBuilder.Notifications.OnApplicationStatusChanged(applicationInstanceInformation);
			});
		}
	}
}