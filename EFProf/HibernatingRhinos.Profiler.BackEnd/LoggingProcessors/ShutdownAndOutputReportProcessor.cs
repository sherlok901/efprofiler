using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.BackEnd.Listeners;
using HibernatingRhinos.Profiler.BackEnd.Messages;
using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using System;
using System.Reactive.Linq;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.LoggingProcessors
{
	public class ShutdownAndOutputReportProcessor : AbstractMessageProcessor
	{
		public ShutdownAndOutputReportProcessor()
		{
		}

		public override void Attach(EventsObserver eventsObserver)
		{
			eventsObserver.OfType<ShutdownAndOutputReport>().Subscribe<ShutdownAndOutputReport>((ShutdownAndOutputReport message) => base.ModelBuilder.Notifications.RaiseShutdownAndOutputReport());
		}
	}
}