using HibernatingRhinos.Profiler.BackEnd.Listeners;
using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using System;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.LoggingProcessors
{
	public abstract class AbstractMessageProcessor
	{
		public ReactiveModelBuilder ModelBuilder
		{
			get;
			set;
		}

		protected AbstractMessageProcessor()
		{
		}

		public abstract void Attach(EventsObserver eventsObserver);
	}
}