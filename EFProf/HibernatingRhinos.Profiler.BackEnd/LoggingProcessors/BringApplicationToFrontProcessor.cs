using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.BackEnd.Listeners;
using HibernatingRhinos.Profiler.BackEnd.Messages;
using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using System;
using System.Reactive.Linq;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.LoggingProcessors
{
	public class BringApplicationToFrontProcessor : AbstractMessageProcessor
	{
		public BringApplicationToFrontProcessor()
		{
		}

		public override void Attach(EventsObserver eventsObserver)
		{
			eventsObserver.OfType<BringApplicationToFront>().Subscribe<BringApplicationToFront>((BringApplicationToFront message) => {
				OpenFileInfo openFileInfo = null;
				if (!string.IsNullOrEmpty(message.ContextFile))
				{
					openFileInfo = new OpenFileInfo()
					{
						FileName = message.ContextFile
					};
				}
				base.ModelBuilder.Notifications.RaiseBringApplicationToFront(openFileInfo);
			});
		}
	}
}