using HibernatingRhinos.Profiler.BackEnd.LoggingProcessors;
using HibernatingRhinos.Profiler.BackEnd.Messages;
using System;

namespace HibernatingRhinos.Profiler.BackEnd.LoggingProcessors.EntityFramework
{
	public class DtcTransactionEnlistedProcessor : AbstractLoggingEventProcessor
	{
		public DtcTransactionEnlistedProcessor()
		{
		}

		protected override bool LoggerNameApplies(string logger)
		{
			return logger == "EntityFramework.Dtc.Transactions";
		}

		protected override void OnLoggingEvent(LoggingEvent loggingEvent)
		{
			if (!loggingEvent.Message.StartsWith("Transaction enlisted: "))
			{
				return;
			}
			TransactionBegan transactionBegan = new TransactionBegan()
			{
				SessionId = loggingEvent.SessionId,
				IsolationLevel = loggingEvent.Message.Substring("Transaction enlisted: ".Length),
				StackTrace = loggingEvent.StackTrace,
				IsDtcTransaction = true,
				Url = loggingEvent.Url
			};
			base.Publish(transactionBegan);
		}
	}
}