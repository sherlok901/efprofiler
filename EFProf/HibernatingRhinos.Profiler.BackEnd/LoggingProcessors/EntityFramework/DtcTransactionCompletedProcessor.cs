using HibernatingRhinos.Profiler.BackEnd.LoggingProcessors;
using HibernatingRhinos.Profiler.BackEnd.Messages;
using System;

namespace HibernatingRhinos.Profiler.BackEnd.LoggingProcessors.EntityFramework
{
	public class DtcTransactionCompletedProcessor : AbstractLoggingEventProcessor
	{
		public DtcTransactionCompletedProcessor()
		{
		}

		protected override bool LoggerNameApplies(string logger)
		{
			return logger == "EntityFramework.Dtc.Transactions";
		}

		protected override void OnLoggingEvent(LoggingEvent loggingEvent)
		{
			if (!loggingEvent.Message.StartsWith("Transaction completed: "))
			{
				return;
			}
			string str = loggingEvent.Message.Substring("Transaction completed: ".Length);
			string str1 = str;
			if (str != null)
			{
				if (str1 == "Aborted" || str1 == "InDoubt")
				{
					TransactionRollback transactionRollback = new TransactionRollback()
					{
						SessionId = loggingEvent.SessionId,
						StackTrace = loggingEvent.StackTrace,
						Url = loggingEvent.Url
					};
					base.Publish(transactionRollback);
					return;
				}
				if (str1 != "Committed")
				{
					return;
				}
				TransactionCommitted transactionCommitted = new TransactionCommitted()
				{
					SessionId = loggingEvent.SessionId,
					StackTrace = loggingEvent.StackTrace,
					Url = loggingEvent.Url
				};
				base.Publish(transactionCommitted);
			}
		}
	}
}