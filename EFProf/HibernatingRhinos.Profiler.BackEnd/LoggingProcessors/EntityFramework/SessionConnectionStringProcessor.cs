using HibernatingRhinos.Profiler.BackEnd.LoggingProcessors;
using HibernatingRhinos.Profiler.BackEnd.Messages;
using System;

namespace HibernatingRhinos.Profiler.BackEnd.LoggingProcessors.EntityFramework
{
	public class SessionConnectionStringProcessor : AbstractLoggingEventProcessor
	{
		public SessionConnectionStringProcessor()
		{
		}

		protected override bool LoggerNameApplies(string logger)
		{
			return logger == "EntityFramework.SessionConnectionString";
		}

		protected override void OnLoggingEvent(LoggingEvent loggingEvent)
		{
			string message = loggingEvent.Message;
			char[] chrArray = new char[] { '|' };
			string[] strArrays = message.Split(chrArray, StringSplitOptions.RemoveEmptyEntries);
			SessionConnectionString sessionConnectionString = new SessionConnectionString()
			{
				SessionId = loggingEvent.SessionId,
				ConnectionString = strArrays[0],
				ConnectionTypeName = strArrays[1]
			};
			base.Publish(sessionConnectionString);
		}
	}
}