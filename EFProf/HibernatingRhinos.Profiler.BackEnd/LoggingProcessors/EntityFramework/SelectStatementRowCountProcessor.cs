using HibernatingRhinos.Profiler.BackEnd.LoggingProcessors;
using HibernatingRhinos.Profiler.BackEnd.Messages;
using System;
using System.Text.RegularExpressions;

namespace HibernatingRhinos.Profiler.BackEnd.LoggingProcessors.EntityFramework
{
	public class SelectStatementRowCountProcessor : AbstractLoggingEventProcessor
	{
		private readonly static Regex LogStatment;

		static SelectStatementRowCountProcessor()
		{
			SelectStatementRowCountProcessor.LogStatment = new Regex("Read (\\d+) rows for ([\\d\\w-]+)", RegexOptions.Compiled);
		}

		public SelectStatementRowCountProcessor()
		{
		}

		protected override bool LoggerNameApplies(string logger)
		{
			return logger == "EntityFramework.StatementRowCount";
		}

		protected override void OnLoggingEvent(LoggingEvent loggingEvent)
		{
			int num;
			Match match = SelectStatementRowCountProcessor.LogStatment.Match(loggingEvent.Message);
			if (!match.Success)
			{
				return;
			}
			if (!int.TryParse(match.Groups[1].Value, out num))
			{
				return;
			}
			string value = match.Groups[2].Value;
			CountOfRowsInQuery countOfRowsInQuery = new CountOfRowsInQuery()
			{
				SessionId = loggingEvent.SessionId,
				StatementId = value,
				RowCount = num
			};
			base.Publish(countOfRowsInQuery);
		}
	}
}