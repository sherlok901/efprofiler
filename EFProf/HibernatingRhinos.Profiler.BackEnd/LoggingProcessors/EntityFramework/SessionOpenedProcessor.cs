using HibernatingRhinos.Profiler.BackEnd.LoggingProcessors;
using HibernatingRhinos.Profiler.BackEnd.Messages;
using System;

namespace HibernatingRhinos.Profiler.BackEnd.LoggingProcessors.EntityFramework
{
	public class SessionOpenedProcessor : AbstractLoggingEventProcessor
	{
		public SessionOpenedProcessor()
		{
		}

		protected override bool LoggerNameApplies(string logger)
		{
			return logger == "EntityFramework.Connection";
		}

		protected override void OnLoggingEvent(LoggingEvent loggingEvent)
		{
			if (loggingEvent.Message != "Connection opened")
			{
				return;
			}
			SessionOpened sessionOpened = new SessionOpened()
			{
				SessionId = loggingEvent.SessionId,
				Url = loggingEvent.Url
			};
			base.Publish(sessionOpened);
		}
	}
}