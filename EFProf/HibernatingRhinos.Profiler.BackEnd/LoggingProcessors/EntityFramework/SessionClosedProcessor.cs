using HibernatingRhinos.Profiler.BackEnd.LoggingProcessors;
using HibernatingRhinos.Profiler.BackEnd.Messages;
using System;

namespace HibernatingRhinos.Profiler.BackEnd.LoggingProcessors.EntityFramework
{
	public class SessionClosedProcessor : AbstractLoggingEventProcessor
	{
		public SessionClosedProcessor()
		{
		}

		protected override bool LoggerNameApplies(string logger)
		{
			return logger == "EntityFramework.Connection";
		}

		protected override void OnLoggingEvent(LoggingEvent loggingEvent)
		{
			if (loggingEvent.Message != "Connection closed")
			{
				return;
			}
			base.Publish(new SessionClosed()
			{
				SessionId = loggingEvent.SessionId
			});
		}
	}
}