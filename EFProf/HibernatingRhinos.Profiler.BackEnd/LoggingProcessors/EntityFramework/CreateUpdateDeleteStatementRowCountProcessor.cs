using HibernatingRhinos.Profiler.BackEnd.LoggingProcessors;
using HibernatingRhinos.Profiler.BackEnd.Messages;
using System;
using System.Text.RegularExpressions;

namespace HibernatingRhinos.Profiler.BackEnd.LoggingProcessors.EntityFramework
{
	public class CreateUpdateDeleteStatementRowCountProcessor : AbstractLoggingEventProcessor
	{
		private readonly static Regex Duration;

		static CreateUpdateDeleteStatementRowCountProcessor()
		{
			CreateUpdateDeleteStatementRowCountProcessor.Duration = new Regex("Executed in (\\d+) ms with (\\d+) rows", RegexOptions.IgnoreCase | RegexOptions.Compiled);
		}

		public CreateUpdateDeleteStatementRowCountProcessor()
		{
		}

		protected override bool LoggerNameApplies(string logger)
		{
			return logger == "EntityFramework.StatementStats";
		}

		protected override void OnLoggingEvent(LoggingEvent loggingEvent)
		{
			int num;
			Match match = CreateUpdateDeleteStatementRowCountProcessor.Duration.Match(loggingEvent.Message);
			if (!match.Success)
			{
				return;
			}
			if (!int.TryParse(match.Groups[2].Value, out num))
			{
				return;
			}
			CountOfRowsInQuery countOfRowsInQuery = new CountOfRowsInQuery()
			{
				SessionId = loggingEvent.SessionId,
				RowCount = num
			};
			base.Publish(countOfRowsInQuery);
		}
	}
}