using HibernatingRhinos.Profiler.BackEnd.LoggingProcessors;
using HibernatingRhinos.Profiler.BackEnd.Messages;
using System;

namespace HibernatingRhinos.Profiler.BackEnd.LoggingProcessors.EntityFramework
{
	public class TransactionCommittedProcessor : AbstractLoggingEventProcessor
	{
		public TransactionCommittedProcessor()
		{
		}

		protected override bool LoggerNameApplies(string logger)
		{
			return "EntityFramework.Transactions" == logger;
		}

		protected override void OnLoggingEvent(LoggingEvent loggingEvent)
		{
			if (loggingEvent.Message != "Transaction committed")
			{
				return;
			}
			TransactionCommitted transactionCommitted = new TransactionCommitted()
			{
				SessionId = loggingEvent.SessionId,
				StackTrace = loggingEvent.StackTrace,
				Url = loggingEvent.Url
			};
			base.Publish(transactionCommitted);
		}
	}
}