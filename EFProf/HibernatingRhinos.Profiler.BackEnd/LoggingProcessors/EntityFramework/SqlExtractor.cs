using HibernatingRhinos.Profiler.BackEnd.LoggingProcessors;
using HibernatingRhinos.Profiler.BackEnd.Messages;
using System;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;

namespace HibernatingRhinos.Profiler.BackEnd.LoggingProcessors.EntityFramework
{
	public class SqlExtractor : AbstractLoggingEventProcessor
	{
		private readonly static Regex StatementIdRegEx;

		private readonly static Regex ParameterExtractor;

		static SqlExtractor()
		{
			SqlExtractor.StatementIdRegEx = new Regex("^-- Statement ([\\d\\w-]+)\\s+", RegexOptions.Compiled);
			SqlExtractor.ParameterExtractor = new Regex("-- ([@:?\\w_\\d]+) = \\[-\\[(.*?)\\]-\\] \\[-\\[([\\d\\w]+\\s\\(-?\\d+\\))\\]-\\]", RegexOptions.Compiled | RegexOptions.Singleline);
		}

		public SqlExtractor()
		{
		}

		private static Parameter[] ExtractParameters(string parametersAsString)
		{
			return (
				from Match match in SqlExtractor.ParameterExtractor.Matches(parametersAsString)
				where match.Success
				select new Parameter(match.Groups[1].Value, match.Groups[2].Value, match.Groups[3].Value, null)).ToArray<Parameter>();
		}

		protected override bool LoggerNameApplies(string logger)
		{
			return logger == "EntityFramework.Sql";
		}

		protected override void OnLoggingEvent(LoggingEvent loggingEvent)
		{
			Match match = SqlExtractor.StatementIdRegEx.Match(loggingEvent.Message);
			if (!match.Success)
			{
				return;
			}
			string value = match.Groups[1].Value;
			string str = match.Result("$'");
			Parameter[] parameterArray = new Parameter[0];
			int num = str.IndexOf("-- Parameters:\r\n");
			if (num != -1)
			{
				parameterArray = SqlExtractor.ExtractParameters(str.Substring(num + "-- Parameters:\r\n".Length));
				str = str.Substring(0, num);
			}
			SqlStatementMessage sqlStatementMessage = new SqlStatementMessage()
			{
				RawSql = str,
				Parameters = parameterArray,
				SessionId = loggingEvent.SessionId,
				StackTrace = loggingEvent.StackTrace,
				Url = loggingEvent.Url,
				Options = SqlStatementOptions.None,
				BatchSize = 1,
				StatementId = value,
				Dialect = loggingEvent.DbDialect,
				StarColor = loggingEvent.StarColor
			};
			base.Publish(sqlStatementMessage);
		}
	}
}