using HibernatingRhinos.Profiler.BackEnd.LoggingProcessors;
using HibernatingRhinos.Profiler.BackEnd.Messages;
using System;

namespace HibernatingRhinos.Profiler.BackEnd.LoggingProcessors.EntityFramework
{
	public class TransactionBeganProcessor : AbstractLoggingEventProcessor
	{
		public TransactionBeganProcessor()
		{
		}

		protected override bool LoggerNameApplies(string logger)
		{
			return "EntityFramework.Transactions" == logger;
		}

		protected override void OnLoggingEvent(LoggingEvent loggingEvent)
		{
			if (!loggingEvent.Message.StartsWith("Transaction began: "))
			{
				return;
			}
			TransactionBegan transactionBegan = new TransactionBegan()
			{
				SessionId = loggingEvent.SessionId,
				IsolationLevel = loggingEvent.Message.Substring("Transaction began: ".Length),
				StackTrace = loggingEvent.StackTrace,
				IsDtcTransaction = false,
				Url = loggingEvent.Url
			};
			base.Publish(transactionBegan);
		}
	}
}