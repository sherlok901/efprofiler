using HibernatingRhinos.Profiler.BackEnd.LoggingProcessors;
using HibernatingRhinos.Profiler.BackEnd.Messages;
using System;

namespace HibernatingRhinos.Profiler.BackEnd.LoggingProcessors.EntityFramework
{
	public class TransactionRollbackProcessor : AbstractLoggingEventProcessor
	{
		public TransactionRollbackProcessor()
		{
		}

		protected override bool LoggerNameApplies(string logger)
		{
			return "EntityFramework.Transactions" == logger;
		}

		protected override void OnLoggingEvent(LoggingEvent loggingEvent)
		{
			if (loggingEvent.Message != "Transaction rollbacked")
			{
				return;
			}
			TransactionRollback transactionRollback = new TransactionRollback()
			{
				SessionId = loggingEvent.SessionId,
				StackTrace = loggingEvent.StackTrace,
				Url = loggingEvent.Url
			};
			base.Publish(transactionRollback);
		}
	}
}