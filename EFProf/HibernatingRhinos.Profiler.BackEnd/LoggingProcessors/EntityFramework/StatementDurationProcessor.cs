using HibernatingRhinos.Profiler.BackEnd.LoggingProcessors;
using HibernatingRhinos.Profiler.BackEnd.Messages;
using System;
using System.Text.RegularExpressions;

namespace HibernatingRhinos.Profiler.BackEnd.LoggingProcessors.EntityFramework
{
	public class StatementDurationProcessor : AbstractLoggingEventProcessor
	{
		private readonly static Regex duration;

		static StatementDurationProcessor()
		{
			StatementDurationProcessor.duration = new Regex("Executed in (\\d+) ms", RegexOptions.Compiled);
		}

		public StatementDurationProcessor()
		{
		}

		protected override bool LoggerNameApplies(string logger)
		{
			return logger == "EntityFramework.StatementStats";
		}

		protected override void OnLoggingEvent(LoggingEvent loggingEvent)
		{
			int num;
			Match match = StatementDurationProcessor.duration.Match(loggingEvent.Message);
			if (!match.Success)
			{
				return;
			}
			if (!int.TryParse(match.Groups[1].Value, out num))
			{
				return;
			}
			QueryDurationInDatabase queryDurationInDatabase = new QueryDurationInDatabase()
			{
				SessionId = loggingEvent.SessionId,
				Milliseconds = num
			};
			base.Publish(queryDurationInDatabase);
		}
	}
}