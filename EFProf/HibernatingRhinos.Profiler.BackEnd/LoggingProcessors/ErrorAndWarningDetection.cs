using HibernatingRhinos.Profiler.BackEnd.Messages;
using log4net.Core;
using System;

namespace HibernatingRhinos.Profiler.BackEnd.LoggingProcessors
{
	public class ErrorAndWarningDetection : AbstractLoggingEventProcessor
	{
		public ErrorAndWarningDetection()
		{
		}

		protected override bool LoggerNameApplies(string logger)
		{
			return true;
		}

		protected override void OnLoggingEvent(HibernatingRhinos.Profiler.BackEnd.Messages.LoggingEvent loggingEvent)
		{
			if (loggingEvent.Level < Level.Warn || loggingEvent.Level == Level.Off)
			{
				return;
			}
			ErrorDetected errorDetected = new ErrorDetected()
			{
				SessionId = loggingEvent.SessionId,
				Message = loggingEvent.Message,
				Severity = loggingEvent.Level.DisplayName,
				StackTrace = loggingEvent.StackTrace,
				Url = loggingEvent.Url,
				ExceptionString = loggingEvent.Exception
			};
			base.Publish(errorDetected);
		}
	}
}