using HibernatingRhinos.Profiler.BackEnd.LoggingProcessors;
using HibernatingRhinos.Profiler.BackEnd.Messages;
using System;

namespace HibernatingRhinos.Profiler.BackEnd.LoggingProcessors.CustomReporting
{
	public class CustomTransactionRollbackProcessor : AbstractLoggingEventProcessor
	{
		public CustomTransactionRollbackProcessor()
		{
		}

		protected override bool LoggerNameApplies(string logger)
		{
			return logger == "HibernatingRhinos.Profiler.Appender.CustomReporting.Transactions";
		}

		protected override void OnLoggingEvent(LoggingEvent loggingEvent)
		{
			if (!loggingEvent.Message.Equals("Transaction rollbacked", StringComparison.InvariantCultureIgnoreCase))
			{
				return;
			}
			TransactionRollback transactionRollback = new TransactionRollback()
			{
				SessionId = loggingEvent.SessionId,
				StackTrace = loggingEvent.StackTrace,
				Url = loggingEvent.Url
			};
			base.Publish(transactionRollback);
		}
	}
}