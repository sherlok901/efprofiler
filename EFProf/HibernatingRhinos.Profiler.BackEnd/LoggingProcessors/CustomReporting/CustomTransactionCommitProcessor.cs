using HibernatingRhinos.Profiler.BackEnd.LoggingProcessors;
using HibernatingRhinos.Profiler.BackEnd.Messages;
using System;

namespace HibernatingRhinos.Profiler.BackEnd.LoggingProcessors.CustomReporting
{
	public class CustomTransactionCommitProcessor : AbstractLoggingEventProcessor
	{
		public CustomTransactionCommitProcessor()
		{
		}

		protected override bool LoggerNameApplies(string logger)
		{
			return logger == "HibernatingRhinos.Profiler.Appender.CustomReporting.Transactions";
		}

		protected override void OnLoggingEvent(LoggingEvent loggingEvent)
		{
			if (!loggingEvent.Message.Equals("Transaction committed", StringComparison.InvariantCultureIgnoreCase))
			{
				return;
			}
			TransactionCommitted transactionCommitted = new TransactionCommitted()
			{
				SessionId = loggingEvent.SessionId,
				StackTrace = loggingEvent.StackTrace,
				Url = loggingEvent.Url
			};
			base.Publish(transactionCommitted);
		}
	}
}