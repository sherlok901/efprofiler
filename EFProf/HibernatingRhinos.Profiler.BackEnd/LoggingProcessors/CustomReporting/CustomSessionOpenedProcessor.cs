using HibernatingRhinos.Profiler.BackEnd.LoggingProcessors;
using HibernatingRhinos.Profiler.BackEnd.Messages;
using System;

namespace HibernatingRhinos.Profiler.BackEnd.LoggingProcessors.CustomReporting
{
	public class CustomSessionOpenedProcessor : AbstractLoggingEventProcessor
	{
		public CustomSessionOpenedProcessor()
		{
		}

		protected override bool LoggerNameApplies(string logger)
		{
			return logger == "HibernatingRhinos.Profiler.Appender.CustomReporting.Session";
		}

		protected override void OnLoggingEvent(LoggingEvent loggingEvent)
		{
			if (loggingEvent.Message != "Session opened")
			{
				return;
			}
			SessionOpened sessionOpened = new SessionOpened()
			{
				SessionId = loggingEvent.SessionId,
				Url = loggingEvent.Url
			};
			base.Publish(sessionOpened);
		}
	}
}