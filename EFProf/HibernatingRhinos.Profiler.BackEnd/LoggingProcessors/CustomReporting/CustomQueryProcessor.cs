using HibernatingRhinos.Profiler.BackEnd.LoggingProcessors;
using HibernatingRhinos.Profiler.BackEnd.Messages;
using System;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;

namespace HibernatingRhinos.Profiler.BackEnd.LoggingProcessors.CustomReporting
{
	public class CustomQueryProcessor : AbstractLoggingEventProcessor
	{
		private readonly static Regex DbTimeExtractor;

		private readonly static Regex TotalTimeExtractor;

		private readonly static Regex RowCountExtractor;

		private readonly static Regex ParameterExtractor;

		static CustomQueryProcessor()
		{
			CustomQueryProcessor.DbTimeExtractor = new Regex("-- db time: (\\d+)", RegexOptions.IgnoreCase | RegexOptions.Compiled);
			CustomQueryProcessor.TotalTimeExtractor = new Regex("-- total time: (\\d+)", RegexOptions.IgnoreCase | RegexOptions.Compiled);
			CustomQueryProcessor.RowCountExtractor = new Regex("-- row count: (\\d+)", RegexOptions.IgnoreCase | RegexOptions.Compiled);
			CustomQueryProcessor.ParameterExtractor = new Regex("-- ([@:?\\w_\\d]+) = \\[-\\[(.*?)\\]-\\]( \\[-\\[([\\d\\w]+\\s\\(-?\\d+\\))\\]-\\])?", RegexOptions.Compiled | RegexOptions.Singleline);
		}

		public CustomQueryProcessor()
		{
		}

		private static Parameter[] ExtractParameters(string parametersAsString)
		{
			return (
				from Match match in CustomQueryProcessor.ParameterExtractor.Matches(parametersAsString)
				where match.Success
				select new Parameter(match.Groups[1].Value, match.Groups[2].Value, match.Groups[3].Value, null)).ToArray<Parameter>();
		}

		protected override bool LoggerNameApplies(string logger)
		{
			return logger == "HibernatingRhinos.Profiler.Appender.CustomReporting";
		}

		protected override void OnLoggingEvent(LoggingEvent loggingEvent)
		{
			int num;
			int num1 = loggingEvent.Message.IndexOf("\r\n-- db time:", StringComparison.InvariantCultureIgnoreCase);
			if (num1 == -1)
			{
				num1 = loggingEvent.Message.IndexOf("\r\n-- total time:", StringComparison.InvariantCultureIgnoreCase);
				if (num1 == -1)
				{
					return;
				}
			}
			string str = loggingEvent.Message.Substring(0, num1);
			Parameter[] parameterArray = new Parameter[0];
			int num2 = str.IndexOf("-- Parameters:\r\n", StringComparison.InvariantCultureIgnoreCase);
			if (num2 != -1)
			{
				parameterArray = CustomQueryProcessor.ExtractParameters(str.Substring(num2 + "-- Parameters:\r\n".Length));
				str = str.Substring(0, num2);
			}
			string str1 = Guid.NewGuid().ToString();
			SqlStatementMessage sqlStatementMessage = new SqlStatementMessage()
			{
				RawSql = str,
				Parameters = parameterArray,
				SessionId = loggingEvent.SessionId,
				StackTrace = loggingEvent.StackTrace,
				Url = loggingEvent.Url,
				Options = SqlStatementOptions.None,
				BatchSize = 1,
				StatementId = str1,
				Dialect = loggingEvent.DbDialect,
				StarColor = loggingEvent.StarColor
			};
			base.Publish(sqlStatementMessage);
			Match i = CustomQueryProcessor.DbTimeExtractor.Match(loggingEvent.Message);
			if (i.Success && int.TryParse(i.Groups[1].Value, out num))
			{
				QueryDurationInDatabase queryDurationInDatabase = new QueryDurationInDatabase()
				{
					SessionId = loggingEvent.SessionId,
					Milliseconds = num
				};
				base.Publish(queryDurationInDatabase);
			}
			i = CustomQueryProcessor.TotalTimeExtractor.Match(loggingEvent.Message);
			if (i.Success && int.TryParse(i.Groups[1].Value, out num))
			{
				QueryDurationInApplication queryDurationInApplication = new QueryDurationInApplication()
				{
					SessionId = loggingEvent.SessionId,
					Milliseconds = num
				};
				base.Publish(queryDurationInApplication);
			}
			for (i = CustomQueryProcessor.RowCountExtractor.Match(loggingEvent.Message); i.Success && int.TryParse(i.Groups[1].Value, out num); i = i.NextMatch())
			{
				CountOfRowsInQuery countOfRowsInQuery = new CountOfRowsInQuery()
				{
					SessionId = loggingEvent.SessionId,
					StatementId = str1,
					RowCount = num
				};
				base.Publish(countOfRowsInQuery);
			}
		}
	}
}