using HibernatingRhinos.Profiler.BackEnd.LoggingProcessors;
using HibernatingRhinos.Profiler.BackEnd.Messages;
using System;

namespace HibernatingRhinos.Profiler.BackEnd.LoggingProcessors.CustomReporting
{
	public class CustomTransactionBeganProcessor : AbstractLoggingEventProcessor
	{
		public CustomTransactionBeganProcessor()
		{
		}

		protected override bool LoggerNameApplies(string logger)
		{
			return logger == "HibernatingRhinos.Profiler.Appender.CustomReporting.Transactions";
		}

		protected override void OnLoggingEvent(LoggingEvent loggingEvent)
		{
			if (!loggingEvent.Message.StartsWith("Transaction began: ", StringComparison.InvariantCultureIgnoreCase))
			{
				return;
			}
			TransactionBegan transactionBegan = new TransactionBegan()
			{
				SessionId = loggingEvent.SessionId,
				IsolationLevel = loggingEvent.Message.Substring("Transaction began: ".Length),
				StackTrace = loggingEvent.StackTrace,
				IsDtcTransaction = false,
				Url = loggingEvent.Url
			};
			base.Publish(transactionBegan);
		}
	}
}