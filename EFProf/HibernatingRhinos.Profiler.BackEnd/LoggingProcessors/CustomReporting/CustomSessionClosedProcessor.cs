using HibernatingRhinos.Profiler.BackEnd.LoggingProcessors;
using HibernatingRhinos.Profiler.BackEnd.Messages;
using System;

namespace HibernatingRhinos.Profiler.BackEnd.LoggingProcessors.CustomReporting
{
	public class CustomSessionClosedProcessor : AbstractLoggingEventProcessor
	{
		public CustomSessionClosedProcessor()
		{
		}

		protected override bool LoggerNameApplies(string logger)
		{
			return logger == "HibernatingRhinos.Profiler.Appender.CustomReporting.Session";
		}

		protected override void OnLoggingEvent(LoggingEvent loggingEvent)
		{
			if (loggingEvent.Message != "Session closed")
			{
				return;
			}
			base.Publish(new SessionClosed()
			{
				SessionId = loggingEvent.SessionId
			});
		}
	}
}