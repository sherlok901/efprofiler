using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.BackEnd.Extensions;
using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.LoggingProcessors
{
	public class ApplicationInstanceDictionary
	{
		private ConcurrentQueue<ApplicationInstanceInformation> applications = new ConcurrentQueue<ApplicationInstanceInformation>();

		public ApplicationInstanceDictionary()
		{
		}

		public void Add(ApplicationInstanceInformation instanceInformation)
		{
			if (instanceInformation == null)
			{
				throw new InvalidOperationException("Instance information must be provided. ");
			}
			this.applications.Enqueue(instanceInformation);
		}

		public ApplicationInstanceInformation Get(Guid id)
		{
			return this.applications.ToArray().FirstOrDefault<ApplicationInstanceInformation>((ApplicationInstanceInformation information) => information.Id == id);
		}

		public void Remove(Guid id)
		{
			this.applications = this.applications.Remove<ApplicationInstanceInformation>((ApplicationInstanceInformation information) => information.Id == id);
		}
	}
}