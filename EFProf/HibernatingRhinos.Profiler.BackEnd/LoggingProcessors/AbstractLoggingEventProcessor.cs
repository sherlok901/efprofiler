using HibernatingRhinos.Profiler.BackEnd.Listeners;
using HibernatingRhinos.Profiler.BackEnd.Messages;
using System;
using System.Globalization;
using System.Reactive.Linq;
using System.Runtime.CompilerServices;
using System.Threading;

namespace HibernatingRhinos.Profiler.BackEnd.LoggingProcessors
{
	public abstract class AbstractLoggingEventProcessor : AbstractMessageProcessor
	{
		private EventsObserver eventsObserver;

		protected AbstractLoggingEventProcessor()
		{
		}

		public override void Attach(EventsObserver events)
		{
			this.eventsObserver = events;
			this.eventsObserver.OfType<LoggingEvent>().Where<LoggingEvent>((LoggingEvent loggingEvent) => this.LoggerNameApplies(loggingEvent.Logger)).Subscribe<LoggingEvent>((LoggingEvent loggingEvent) => {
				CultureInfo cultureInfo = AbstractLoggingEventProcessor.TrySetCurrentCulture(loggingEvent);
				this.OnLoggingEvent(loggingEvent);
				if (cultureInfo != null)
				{
					Thread.CurrentThread.CurrentCulture = cultureInfo;
				}
			});
		}

		protected abstract bool LoggerNameApplies(string logger);

		protected abstract void OnLoggingEvent(LoggingEvent loggingEvent);

		protected void Publish(object msg)
		{
			this.eventsObserver.Publish(msg);
		}

		private static CultureInfo TrySetCurrentCulture(LoggingEvent loggingEvent)
		{
			CultureInfo cultureInfo;
			if (string.IsNullOrEmpty(loggingEvent.Culture))
			{
				return null;
			}
			if (Thread.CurrentThread.CurrentCulture.Name == loggingEvent.Culture)
			{
				return null;
			}
			try
			{
				CultureInfo currentCulture = Thread.CurrentThread.CurrentCulture;
				Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo(loggingEvent.Culture);
				cultureInfo = currentCulture;
			}
			catch
			{
				return null;
			}
			return cultureInfo;
		}
	}
}