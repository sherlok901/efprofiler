using HibernatingRhinos.Profiler.BackEnd.Messages;
using System;

namespace HibernatingRhinos.Profiler.BackEnd.LoggingProcessors
{
	public class RenameSessionProcessor : AbstractLoggingEventProcessor
	{
		public RenameSessionProcessor()
		{
		}

		protected override bool LoggerNameApplies(string logger)
		{
			return logger == "HibernatingRhinos.Profiler.Session.Rename";
		}

		protected override void OnLoggingEvent(LoggingEvent loggingEvent)
		{
			RenameSession renameSession = new RenameSession()
			{
				SessionId = loggingEvent.SessionId,
				Name = loggingEvent.Message
			};
			base.Publish(renameSession);
		}
	}
}