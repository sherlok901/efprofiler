using System;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd
{
	public class AlertInformation : IEquatable<AlertInformation>
	{
		public readonly static AlertInformation DataBindingQueries;

		public readonly static AlertInformation QueriesFromViews;

		public readonly static AlertInformation TooManyJoins;

		public readonly static AlertInformation UncachedQueryPlan;

		public readonly static AlertInformation CrossThreadSessionUsage;

		public readonly static AlertInformation MultipleSessionsInTheSameRequest;

		public readonly static AlertInformation MultipleWriteSessionsInTheSameRequest;

		public readonly static AlertInformation TransactionDisposedWithoutRollback;

		public readonly static AlertInformation EndsWithWillForceTableScan;

		public readonly static AlertInformation UseOfImplicitTransactionIsDiscouragedWarning;

		public readonly static AlertInformation UseOfImplicitTransactionIsDiscouragedSuggestion;

		public readonly static AlertInformation LargeNumberOfWrites;

		public readonly static AlertInformation ExcessiveNumberOfRowsSuggestion;

		public readonly static AlertInformation ExcessiveNumberOfRowsWarning;

		public readonly static AlertInformation UnboundedResultSet;

		public readonly static AlertInformation TooManyDatabaseCalls;

		public readonly static AlertInformation TooManyDatabaseCallsInTheSameRequest;

		public readonly static AlertInformation TooManyCacheCallsInTheSameRequest;

		public readonly static AlertInformation SelectNPlusOne;

		public readonly static AlertInformation SelectNPlusOneInTheSameRequest;

		public readonly static AlertInformation SuperfluousManyToOneUpdate;

		public readonly static AlertInformation ErrorDetected;

		public readonly static AlertInformation ErrorDetectedSeeNextStatement;

		public readonly static AlertInformation TooManyCachedCalls;

		public readonly static AlertInformation TooManyTables;

		public readonly static AlertInformation ColumnTypeMismatch;

		public readonly static AlertInformation QueryOnUnIndexedColumn;

		public readonly static AlertInformation TooManyNestingSelect;

		public readonly static AlertInformation TooManyWhereClauses;

		public readonly static AlertInformation TooManyExpressionsPerWhereClause;

		public string HelpTopic
		{
			get;
			set;
		}

		public HibernatingRhinos.Profiler.BackEnd.Severity Severity
		{
			get;
			set;
		}

		public string Title
		{
			get;
			set;
		}

		static AlertInformation()
		{
			AlertInformation alertInformation = new AlertInformation()
			{
				Severity = HibernatingRhinos.Profiler.BackEnd.Severity.Warning,
				Title = "This statement executed as a result of databinding, which usually result in bad performance",
				HelpTopic = "DataBindingQueries"
			};
			AlertInformation.DataBindingQueries = alertInformation;
			AlertInformation alertInformation1 = new AlertInformation()
			{
				Severity = HibernatingRhinos.Profiler.BackEnd.Severity.Warning,
				Title = "This statement executed from the view, which can result in bad performance and/or brittle behavior",
				HelpTopic = "QueriesFromViews"
			};
			AlertInformation.QueriesFromViews = alertInformation1;
			AlertInformation alertInformation2 = new AlertInformation()
			{
				Severity = HibernatingRhinos.Profiler.BackEnd.Severity.Warning,
				Title = "This statement has too many joins, which may impact performance",
				HelpTopic = "TooManyJoins"
			};
			AlertInformation.TooManyJoins = alertInformation2;
			AlertInformation alertInformation3 = new AlertInformation()
			{
				Severity = HibernatingRhinos.Profiler.BackEnd.Severity.Warning,
				Title = "Different parameter sizes result in inefficient query plan cache usage",
				HelpTopic = "UncachedQueryPlan"
			};
			AlertInformation.UncachedQueryPlan = alertInformation3;
			AlertInformation alertInformation4 = new AlertInformation()
			{
				Severity = HibernatingRhinos.Profiler.BackEnd.Severity.Warning,
				Title = "Using a single [[session]] in multiple threads is likely a bug",
				HelpTopic = "CrossThreadSessionUsage"
			};
			AlertInformation.CrossThreadSessionUsage = alertInformation4;
			AlertInformation alertInformation5 = new AlertInformation()
			{
				Severity = HibernatingRhinos.Profiler.BackEnd.Severity.Suggestion,
				Title = "Multiple [[sessions]] in a single request lead to poor performance",
				HelpTopic = "MultipleSessionsInTheSameRequest"
			};
			AlertInformation.MultipleSessionsInTheSameRequest = alertInformation5;
			AlertInformation alertInformation6 = new AlertInformation()
			{
				Severity = HibernatingRhinos.Profiler.BackEnd.Severity.Warning,
				Title = "Potential transactional violation! Writes across multiple sessions don't participate in a single transaction",
				HelpTopic = "MultipleWriteSessionsInTheSameRequest"
			};
			AlertInformation.MultipleWriteSessionsInTheSameRequest = alertInformation6;
			AlertInformation alertInformation7 = new AlertInformation()
			{
				Severity = HibernatingRhinos.Profiler.BackEnd.Severity.Warning,
				Title = "Transaction disposed without explicit rollback / commit",
				HelpTopic = "AvoidImplicitRollback"
			};
			AlertInformation.TransactionDisposedWithoutRollback = alertInformation7;
			AlertInformation alertInformation8 = new AlertInformation()
			{
				Severity = HibernatingRhinos.Profiler.BackEnd.Severity.Warning,
				Title = "EndsWith or Contains queries will force a full table scan",
				HelpTopic = "EndsWithWillForceTableScan"
			};
			AlertInformation.EndsWithWillForceTableScan = alertInformation8;
			AlertInformation alertInformation9 = new AlertInformation()
			{
				Severity = HibernatingRhinos.Profiler.BackEnd.Severity.Warning,
				Title = "Use of implicit transactions is discouraged",
				HelpTopic = "DoNotUseImplicitTransactions"
			};
			AlertInformation.UseOfImplicitTransactionIsDiscouragedWarning = alertInformation9;
			AlertInformation alertInformation10 = new AlertInformation()
			{
				Severity = HibernatingRhinos.Profiler.BackEnd.Severity.Suggestion,
				Title = "Use of implicit transactions is discouraged",
				HelpTopic = "DoNotUseImplicitTransactions"
			};
			AlertInformation.UseOfImplicitTransactionIsDiscouragedSuggestion = alertInformation10;
			AlertInformation alertInformation11 = new AlertInformation()
			{
				Severity = HibernatingRhinos.Profiler.BackEnd.Severity.Warning,
				Title = "Large number of individual writes",
				HelpTopic = "LargeNumberOfWrites"
			};
			AlertInformation.LargeNumberOfWrites = alertInformation11;
			AlertInformation alertInformation12 = new AlertInformation()
			{
				Severity = HibernatingRhinos.Profiler.BackEnd.Severity.Suggestion,
				Title = "Large number of rows returned",
				HelpTopic = "ExcessiveNumberOfRows"
			};
			AlertInformation.ExcessiveNumberOfRowsSuggestion = alertInformation12;
			AlertInformation alertInformation13 = new AlertInformation()
			{
				Severity = HibernatingRhinos.Profiler.BackEnd.Severity.Warning,
				Title = "Large number of rows returned",
				HelpTopic = "ExcessiveNumberOfRows"
			};
			AlertInformation.ExcessiveNumberOfRowsWarning = alertInformation13;
			AlertInformation alertInformation14 = new AlertInformation()
			{
				Severity = HibernatingRhinos.Profiler.BackEnd.Severity.Suggestion,
				Title = "Unbounded result set",
				HelpTopic = "UnboundedResultSet"
			};
			AlertInformation.UnboundedResultSet = alertInformation14;
			AlertInformation alertInformation15 = new AlertInformation()
			{
				Severity = HibernatingRhinos.Profiler.BackEnd.Severity.Suggestion,
				Title = "Too many database calls per [[session]]",
				HelpTopic = "TooManyDatabaseCalls"
			};
			AlertInformation.TooManyDatabaseCalls = alertInformation15;
			AlertInformation alertInformation16 = new AlertInformation()
			{
				Severity = HibernatingRhinos.Profiler.BackEnd.Severity.Warning,
				Title = "Too many database calls in the same request",
				HelpTopic = "TooManyDatabaseCallsInTheSameRequest"
			};
			AlertInformation.TooManyDatabaseCallsInTheSameRequest = alertInformation16;
			AlertInformation alertInformation17 = new AlertInformation()
			{
				Severity = HibernatingRhinos.Profiler.BackEnd.Severity.Suggestion,
				Title = "Too many cache calls in the same request should be avoided",
				HelpTopic = "TooManyCacheCallsInTheSameRequest"
			};
			AlertInformation.TooManyCacheCallsInTheSameRequest = alertInformation17;
			AlertInformation alertInformation18 = new AlertInformation()
			{
				Severity = HibernatingRhinos.Profiler.BackEnd.Severity.Warning,
				Title = "SELECT N+1",
				HelpTopic = "SelectNPlusOne"
			};
			AlertInformation.SelectNPlusOne = alertInformation18;
			AlertInformation alertInformation19 = new AlertInformation()
			{
				Severity = HibernatingRhinos.Profiler.BackEnd.Severity.Warning,
				Title = "SELECT N+1 across multiple [[sessions]]",
				HelpTopic = "SelectNPlusOneInTheSameRequest"
			};
			AlertInformation.SelectNPlusOneInTheSameRequest = alertInformation19;
			AlertInformation alertInformation20 = new AlertInformation()
			{
				Severity = HibernatingRhinos.Profiler.BackEnd.Severity.Warning,
				Title = "Superfluous <many-to-one> update - use inverse='true'",
				HelpTopic = "SuperfluousManyToOneUpdate"
			};
			AlertInformation.SuperfluousManyToOneUpdate = alertInformation20;
			AlertInformation alertInformation21 = new AlertInformation()
			{
				Severity = HibernatingRhinos.Profiler.BackEnd.Severity.Warning,
				Title = "Error detected",
				HelpTopic = "error/detected"
			};
			AlertInformation.ErrorDetected = alertInformation21;
			AlertInformation alertInformation22 = new AlertInformation()
			{
				Severity = HibernatingRhinos.Profiler.BackEnd.Severity.Warning,
				Title = "Error detected - full details are shown in the next statement",
				HelpTopic = "ErrorDetected"
			};
			AlertInformation.ErrorDetectedSeeNextStatement = alertInformation22;
			AlertInformation alertInformation23 = new AlertInformation()
			{
				Severity = HibernatingRhinos.Profiler.BackEnd.Severity.Suggestion,
				Title = "Too many cache calls per [[session]]",
				HelpTopic = "TooManyCachedCalls"
			};
			AlertInformation.TooManyCachedCalls = alertInformation23;
			AlertInformation alertInformation24 = new AlertInformation()
			{
				Severity = HibernatingRhinos.Profiler.BackEnd.Severity.Warning,
				Title = "Too many tables in selected statement",
				HelpTopic = "TooManyTables"
			};
			AlertInformation.TooManyTables = alertInformation24;
			AlertInformation alertInformation25 = new AlertInformation()
			{
				Severity = HibernatingRhinos.Profiler.BackEnd.Severity.Warning,
				Title = "Column type mismatch",
				HelpTopic = "ColumnTypeMismatch"
			};
			AlertInformation.ColumnTypeMismatch = alertInformation25;
			AlertInformation alertInformation26 = new AlertInformation()
			{
				Severity = HibernatingRhinos.Profiler.BackEnd.Severity.Warning,
				Title = "Query on un indexed column",
				HelpTopic = "QueryOnUnIndexedColumn"
			};
			AlertInformation.QueryOnUnIndexedColumn = alertInformation26;
			AlertInformation alertInformation27 = new AlertInformation()
			{
				Severity = HibernatingRhinos.Profiler.BackEnd.Severity.Major,
				Title = "Too many nesting select statements",
				HelpTopic = "TooManyNestingSelect"
			};
			AlertInformation.TooManyNestingSelect = alertInformation27;
			AlertInformation alertInformation28 = new AlertInformation()
			{
				Severity = HibernatingRhinos.Profiler.BackEnd.Severity.Suggestion,
				Title = "Too many where clauses in statement",
				HelpTopic = "TooManyWhereClauses"
			};
			AlertInformation.TooManyWhereClauses = alertInformation28;
			AlertInformation alertInformation29 = new AlertInformation()
			{
				Severity = HibernatingRhinos.Profiler.BackEnd.Severity.Suggestion,
				Title = "Too many expressions per where clause",
				HelpTopic = "TooManyExpressionsPerWhereClause"
			};
			AlertInformation.TooManyExpressionsPerWhereClause = alertInformation29;
		}

		public AlertInformation()
		{
		}

		public bool Equals(AlertInformation other)
		{
			if (object.ReferenceEquals(null, other))
			{
				return false;
			}
			if (object.ReferenceEquals(this, other))
			{
				return true;
			}
			if (!object.Equals(other.Severity, this.Severity) || !object.Equals(other.Title, this.Title))
			{
				return false;
			}
			return object.Equals(other.HelpTopic, this.HelpTopic);
		}

		public override bool Equals(object obj)
		{
			if (object.ReferenceEquals(null, obj))
			{
				return false;
			}
			if (object.ReferenceEquals(this, obj))
			{
				return true;
			}
			return this.Equals((AlertInformation)obj);
		}

		public override int GetHashCode()
		{
			return (this.Severity.GetHashCode() * 397 ^ (this.Title != null ? this.Title.GetHashCode() : 0)) * 397 ^ (this.HelpTopic != null ? this.HelpTopic.GetHashCode() : 0);
		}
	}
}