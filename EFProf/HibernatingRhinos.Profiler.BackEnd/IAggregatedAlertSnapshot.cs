using System;

namespace HibernatingRhinos.Profiler.BackEnd
{
	public interface IAggregatedAlertSnapshot
	{
		AlertInformation Alert
		{
			get;
		}

		int Count
		{
			get;
		}
	}
}