using gudusoft.gsqlparser;
using gudusoft.gsqlparser.Units;
using HibernatingRhinos.Profiler.Appender.StackTraces;
using HibernatingRhinos.Profiler.BackEnd.Extensions;
using HibernatingRhinos.Profiler.BackEnd.Messages;
using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace HibernatingRhinos.Profiler.BackEnd
{
	public class SqlStatementProcessor
	{
		public const int MaxSqlLength = 150000;

		private const string optionRecompileToken = "/* option recompile token for nh prof */";

		private ProfilerConfiguration configuration;

		private readonly static Regex optionRecompile;

		private readonly List<Func<string, string>> transformers = new List<Func<string, string>>();

		private readonly SqlStatementMessage message;

		private readonly static IDictionary<string, SqlStatementProcessingResult> formattedStatementCache;

		private readonly static ReaderWriterLockSlim formattedStatementCacheLock;

		private readonly HashSet<char> parameterEndingChars;

		public ProfilerConfiguration Configuration
		{
			get
			{
				ProfilerConfiguration profilerConfiguration = this.configuration;
				if (profilerConfiguration == null)
				{
					ProfilerConfiguration profilerConfiguration1 = new ProfilerConfiguration();
					ProfilerConfiguration profilerConfiguration2 = profilerConfiguration1;
					this.configuration = profilerConfiguration1;
					profilerConfiguration = profilerConfiguration2;
				}
				return profilerConfiguration;
			}
			set
			{
				this.configuration = value;
			}
		}

		static SqlStatementProcessor()
		{
			SqlStatementProcessor.optionRecompile = new Regex("option \\s* \\( \\s* recompile \\s* \\)", RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.Compiled | RegexOptions.IgnorePatternWhitespace);
			SqlStatementProcessor.formattedStatementCache = new Dictionary<string, SqlStatementProcessingResult>();
			SqlStatementProcessor.formattedStatementCacheLock = new ReaderWriterLockSlim();
			lzbasetype.gFmtOpt.case_identifier = TCaseOption.coNoChange;
			lzbasetype.gFmtOpt.case_keywords = TCaseOption.coNoChange;
			lzbasetype.gFmtOpt.case_datatype = TCaseOption.coNoChange;
			lzbasetype.gFmtOpt.case_funcname = TCaseOption.coNoChange;
			lzbasetype.gFmtOpt.Case_Variable_KeepSameAsDeclare = true;
			lzbasetype.gFmtOpt.Insert_Columnlist_Style = TAlignStyle.asStacked;
			lzbasetype.gFmtOpt.Insert_Valuelist_Style = TAlignStyle.asStacked;
			lzbasetype.gFmtOpt.WSPadding_ParenthesesInExpression = false;
			lzbasetype.gFmtOpt.exec_parameters_style = TAlignStyle.asStacked;
			lzbasetype.gFmtOpt.LinebreakBeforeParamInExec = true;
			lzbasetype.gFmtOpt.exec_parameters_align_value = true;
		}

		public SqlStatementProcessor(SqlStatementMessage message)
		{
			this.transformers = new List<Func<string, string>>();
			HashSet<char> chrs = new HashSet<char>();
			chrs.Add(',');
			chrs.Add(' ');
			chrs.Add('\t');
			chrs.Add('\r');
			chrs.Add('\n');
			chrs.Add('-');
			chrs.Add('+');
			chrs.Add(')');
			chrs.Add('(');
			chrs.Add('[');
			chrs.Add(']');
			chrs.Add('*');
			chrs.Add('/');
			chrs.Add('\\');
			chrs.Add(';');
			chrs.Add(':');
			chrs.Add('=');
			this.parameterEndingChars = chrs;

			this.message = message;
		}

		private SqlStatementProcessingResult AddNewLineAtNewStatement(SqlStatementProcessingResult result)
		{
			if (result.Sql == null)
			{
				return result;
			}
			StringWriter stringWriter = new StringWriter();
			StringReader stringReader = new StringReader(result.Sql);
			bool flag = false;
			int num = 0;
			while (true)
			{
				string str = stringReader.ReadLine();
				string str1 = str;
				if (str == null)
				{
					break;
				}
				string str2 = str1.TrimStart(new char[0]);
				if (num > 0 && (str2.IndexOf("select ", StringComparison.InvariantCultureIgnoreCase) == 0 || str2.IndexOf("delete ", StringComparison.InvariantCultureIgnoreCase) == 0 || str2.IndexOf("insert ", StringComparison.InvariantCultureIgnoreCase) == 0 || str2.IndexOf("update ", StringComparison.InvariantCultureIgnoreCase) == 0))
				{
					flag = true;
					stringWriter.WriteLine();
					stringWriter.WriteLine();
				}
				num++;
				stringWriter.WriteLine(str1);
			}
			if (!flag)
			{
				return result;
			}
			result.Sql = stringWriter.GetStringBuilder().ToString();
			return result;
		}

		private Statement CreateStatement(SqlStatementOptions options, SessionIdHolder sessionId, string rawSql, string sql, string shortSql, StackTraceInfo stackTrace, Parameter[] parameters, string url, string starColor, string prefix, string errors, string[] tables, object subStatements, string[] ignoredAlertsTitles, string statementId)
		{
			sql = this.FinalProcessing(sql);
			rawSql = this.FinalProcessing(rawSql);
			shortSql = this.FinalProcessing(shortSql);
			StackTraceProcessor stackTraceProcessor = new StackTraceProcessor(this.configuration);
			Statement statement = new Statement()
			{
				Id = statementId,
				SessionId = sessionId,
				RawSql = rawSql,
				FormattedSql = sql,
				ShortSql = shortSql,
				Parameters = parameters,
				Url = url,
				StarColor = starColor,
				Prefix = prefix,
				Errors = errors,
				IgnoredAlertsTitles = ignoredAlertsTitles,
				Options = options,
				UnfilteredStackTrace = stackTrace,
				StackTrace = stackTraceProcessor.ProcessStackTrace(stackTrace),
				Tables = tables,
				SubStatements = subStatements
			};
			return statement;
		}

		private string FinalProcessing(string sql)
		{
			foreach (Func<string, string> transformer in this.transformers)
			{
				sql = transformer(sql);
			}
			return sql;
		}

		private string InitialProcessing()
		{
			string str = this.message.RawSql.Trim();
			string str1 = SqlStatementProcessor.optionRecompile.Replace(str, "/* option recompile token for nh prof */");
			if (str == str1)
			{
				return str;
			}
			this.transformers.Add(new Func<string, string>((string s) => s.Replace("/* option recompile token for nh prof */", "option(recompile)")));
			return str1;
		}

		public Statement ProcessSql()
		{
			string str = this.InitialProcessing();
			if (this.message.Options.HasFlag(SqlStatementOptions.DoNotTryToParseStatement))
			{
				return this.CreateStatement(this.message.Options, this.message.SessionId, str, str, str.First50Characters(), this.message.StackTrace, this.message.Parameters, this.message.Url, this.message.StarColor, this.message.Prefix, null, new string[0], null, this.Configuration.FindIgnoredAlerts(str, this.message.StackTrace), this.message.StatementId);
			}
			int length = str.Length;
			if (length > 150000)
			{
				string[] strArrays = new string[] { str.FirstCharacters(150000, FormattingOptions.AllowMultiLine), Environment.NewLine, Environment.NewLine, "-- The SQL statement is too big to show in the UI.", Environment.NewLine, string.Format("-- For performance reasons we show just the first {0} out of {1} characters.", 150000, length) };
				str = string.Concat(strArrays);
			}
			SqlStatementProcessingResult sqlStatementProcessingResult = this.ProcessSqlWithCaching(str);
			string str1 = this.ReplaceParametersWithValues(sqlStatementProcessingResult.ShortSql, false);
			string str2 = this.ReplaceParametersWithValues(sqlStatementProcessingResult.Sql, true);
			return this.CreateStatement(this.message.Options, this.message.SessionId, str, str2, str1, this.message.StackTrace, this.message.Parameters, this.message.Url, this.message.StarColor, this.message.Prefix, sqlStatementProcessingResult.Errors, sqlStatementProcessingResult.Tables, sqlStatementProcessingResult.SubStatement, this.Configuration.FindIgnoredAlerts(str, this.message.StackTrace), this.message.StatementId);
		}

		private SqlStatementProcessingResult ProcessSqlWithCaching(string sql)
		{
			SqlStatementProcessingResult sqlStatementProcessingResult;
			SqlStatementProcessingResult sqlStatementProcessingResult1;
			SqlStatementProcessingResult sqlStatementProcessingResult2;
			SqlStatementProcessor.formattedStatementCacheLock.EnterReadLock();
			try
			{
				if (SqlStatementProcessor.formattedStatementCache.TryGetValue(sql, out sqlStatementProcessingResult))
				{
					sqlStatementProcessingResult2 = sqlStatementProcessingResult;
					return sqlStatementProcessingResult2;
				}
			}
			finally
			{
				SqlStatementProcessor.formattedStatementCacheLock.ExitReadLock();
			}
			SqlStatementProcessor.formattedStatementCacheLock.EnterWriteLock();
			try
			{
				if (!SqlStatementProcessor.formattedStatementCache.TryGetValue(sql, out sqlStatementProcessingResult1))
				{
					sqlStatementProcessingResult1 = (new SqlProcessor(sql, this.message.Dialect)).ProcessSql();
					sqlStatementProcessingResult1 = this.AddNewLineAtNewStatement(sqlStatementProcessingResult1);
					SqlStatementProcessor.formattedStatementCache.Add(sql, sqlStatementProcessingResult1);
					sqlStatementProcessingResult2 = sqlStatementProcessingResult1;
				}
				else
				{
					sqlStatementProcessingResult2 = sqlStatementProcessingResult1;
				}
			}
			finally
			{
				SqlStatementProcessor.formattedStatementCacheLock.ExitWriteLock();
			}
			return sqlStatementProcessingResult2;
		}

		private string ReplaceParametersWithValues(string statement, bool useComment)
		{
			int j;
			if (this.message.Parameters == null)
			{
				return statement;
			}
			StringBuilder stringBuilder = new StringBuilder(statement);
			List<SqlStatementProcessor.Range> ranges = new List<SqlStatementProcessor.Range>();
			int num = 0;
			for (int i = 0; i < stringBuilder.Length; i++)
			{
				char chars = stringBuilder[i];
				if (chars == '@' || chars == '?' || chars == ':')
				{
					string str = "";
					for (j = 1; j + i < stringBuilder.Length; j++)
					{
						char chr = stringBuilder[i + j];
						if (this.parameterEndingChars.Contains(chr))
						{
							break;
						}
						str = string.Concat(str, chr);
					}
					Parameter parameters = null;
					if (!this.message.Dialect.ContainsIgnoreCase("DB2"))
					{
						parameters = ((IEnumerable<Parameter>)this.message.Parameters).FirstOrDefault<Parameter>((Parameter p) => {
							if (p == null || p.Name == null)
							{
								return false;
							}
							return p.Name.TrimStart(new char[] { '@', '?', ':' }) == str.TrimStart(new char[] { '@', '?', ':' });
						});
					}
					else if ((int)this.message.Parameters.Length > num)
					{
						int num1 = num;
						num = num1 + 1;
						parameters = this.message.Parameters[num1];
					}
					if (parameters != null && !(parameters.Value == "Structured"))
					{
						string name = parameters.Name;
						if (this.message.Dialect.ContainsIgnoreCase("DB2"))
						{
							name = "?";
						}
						string str1 = (useComment ? string.Concat(parameters.Value, " /* ", parameters.Name, " */") : parameters.Value);
						if (i + j >= stringBuilder.Length || !char.IsDigit(stringBuilder[i + j]))
						{
							if (!ranges.Any<SqlStatementProcessor.Range>((SqlStatementProcessor.Range x) => {
								if (x.Start > i)
								{
									return false;
								}
								return i <= x.End;
							}))
							{
								stringBuilder.Remove(i, name.Length);
								stringBuilder.Insert(i, str1);
								foreach (SqlStatementProcessor.Range range in ranges)
								{
									if (range.Start < i)
									{
										continue;
									}
									SqlStatementProcessor.Range start = range;
									start.Start = start.Start + (str1.Length - name.Length);
									SqlStatementProcessor.Range end = range;
									end.End = end.End + (str1.Length - name.Length);
								}
								SqlStatementProcessor.Range range1 = new SqlStatementProcessor.Range()
								{
									Start = i,
									End = i + str1.Length
								};
								ranges.Add(range1);
								i = i + (str1.Length - 1);
							}
						}
					}
				}
			}
			return stringBuilder.ToString();
		}

		public class Range
		{
			public int Start;

			public int End;

			public Range()
			{
			}
		}
	}
}