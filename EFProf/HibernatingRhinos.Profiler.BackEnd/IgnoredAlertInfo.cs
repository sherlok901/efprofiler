using System;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd
{
	[Serializable]
	public class IgnoredAlertInfo
	{
		public string Alert
		{
			get;
			set;
		}

		public string RawSql
		{
			get;
			set;
		}

		public string[] StackTraceFrames
		{
			get;
			set;
		}

		public IgnoredAlertInfo()
		{
		}
	}
}