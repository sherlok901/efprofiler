using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.Messages;
using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.SessionProcessors
{
	public class WarnOnTransactionsDisposedWithoutCommitOrRollback : AbstractSessionProcessor
	{
		public WarnOnTransactionsDisposedWithoutCommitOrRollback()
		{
		}

		public override void OnSessionClosed(Session session)
		{
			if (session.IsInDtcTransaction)
			{
				return;
			}
			IEnumerable<Statement> statements = session.Statements.Where<Statement>((Statement x) => {
				if (x.Options.HasFlag(SqlStatementOptions.Transaction))
				{
					return true;
				}
				return x.Options.HasFlag(SqlStatementOptions.DtcTransaction);
			}).Where<Statement>((Statement tx) => {
				if (tx.FormattedSql.StartsWith("begin"))
				{
					return true;
				}
				return tx.FormattedSql.StartsWith("enlisted");
			});
			IEnumerable<Statement> statements1 = session.Statements.Where<Statement>((Statement x) => {
				if (x.Options.HasFlag(SqlStatementOptions.Transaction))
				{
					return true;
				}
				return x.Options.HasFlag(SqlStatementOptions.DtcTransaction);
			}).Where<Statement>((Statement tx) => {
				if (tx.FormattedSql.StartsWith("commit"))
				{
					return true;
				}
				return tx.FormattedSql.StartsWith("rollback");
			});
			if (statements.Count<Statement>() != statements1.Count<Statement>())
			{
				Action<AlertInformation> acceptAlertToTheCurrentTransaction = session.AcceptAlertToTheCurrentTransaction;
				if (acceptAlertToTheCurrentTransaction != null)
				{
					acceptAlertToTheCurrentTransaction(AlertInformation.TransactionDisposedWithoutRollback);
				}
			}
		}
	}
}