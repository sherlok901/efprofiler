using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.Messages;
using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.SessionProcessors
{
	public class TooManyDatabaseCallsPerRequest : AbstractSessionProcessor
	{
		public TooManyDatabaseCallsPerRequest()
		{
		}

		public override void OnSessionClosed(Session session)
		{
			if (!session.Id.ThreadId.StartsWith("ASP.Net/"))
			{
				return;
			}
			Session[] sessionsOnSameThread = base.GetSessionsOnSameThread(session);
			if ((int)sessionsOnSameThread.Length <= 1)
			{
				return;
			}
			if (((IEnumerable<Session>)sessionsOnSameThread).Sum<Session>((Session x) => x.CountOfStandardDatabaseStatements) < base.Configuration.MaxNumberOfStatementsPerSession)
			{
				return;
			}
			foreach (Statement statement in session.Statements)
			{
				if (statement.Options.HasFlag(SqlStatementOptions.Cached) || statement.IsTransaction)
				{
					continue;
				}
				statement.AcceptAlert(AlertInformation.TooManyDatabaseCallsInTheSameRequest);
			}
		}
	}
}