using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;

namespace HibernatingRhinos.Profiler.BackEnd.SessionProcessors
{
	public class SelectNPlusOneAcrossRequestProcessor : AbstractSessionProcessor
	{
		private const string mySqlSelectLastIdentity = "SELECT LAST_INSERT_ID()";

		private readonly static Regex OracleSequenceNextVal;

		static SelectNPlusOneAcrossRequestProcessor()
		{
			SelectNPlusOneAcrossRequestProcessor.OracleSequenceNextVal = new Regex("\\.nextval \\s+ from \\s+ dual", RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.IgnorePatternWhitespace);
		}

		public SelectNPlusOneAcrossRequestProcessor()
		{
		}

		public override void OnSessionClosed(Session session)
		{
			Session[] sessionsOnSameThread = base.GetSessionsOnSameThread(session);
			if ((int)sessionsOnSameThread.Length <= 1)
			{
				return;
			}
			foreach (Statement statement in session.Statements)
			{
				if (!statement.IsSelectStatement || statement.RawSql == "SELECT LAST_INSERT_ID()" || SelectNPlusOneAcrossRequestProcessor.OracleSequenceNextVal.IsMatch(statement.RawSql))
				{
					continue;
				}
				if (((IEnumerable<Session>)sessionsOnSameThread).Sum<Session>((Session x) => x.GetCountOfStatementsByRawSql(statement.RawSql)) < base.Configuration.MaxRepeatedStatementsForSelectNPlusOne)
				{
					continue;
				}
				statement.AcceptAlert(AlertInformation.SelectNPlusOneInTheSameRequest);
			}
		}
	}
}