using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.Messages;
using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using System;
using System.Linq;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.SessionProcessors
{
	public class WriteFromMultipleSessionsInSameRequestShouldBeAvoided : AbstractSessionProcessor
	{
		public WriteFromMultipleSessionsInSameRequestShouldBeAvoided()
		{
		}

		private bool IsWriteStatement(Statement statement)
		{
			if (statement.Options.HasFlag(SqlStatementOptions.Cached) || statement.Options.HasFlag(SqlStatementOptions.Warning) || statement.IsTransaction)
			{
				return false;
			}
			return !statement.IsSelectStatement;
		}

		public override void OnSessionClosed(Session session)
		{
			if (!session.Id.ThreadId.StartsWith("ASP.Net/"))
			{
				return;
			}
			Session[] sessionsOnSameThread = base.GetSessionsOnSameThread(session);
			if ((int)sessionsOnSameThread.Length <= 1)
			{
				return;
			}
			Statement[] array = session.Statements.Where<Statement>(new Func<Statement, bool>(this.IsWriteStatement)).ToArray<Statement>();
			if ((int)array.Length == 0)
			{
				return;
			}
			if (!sessionsOnSameThread.Any<Session>((Session x) => x.Statements.Any<Statement>(new Func<Statement, bool>(this.IsWriteStatement))))
			{
				return;
			}
			Statement[] statementArray = array;
			for (int i = 0; i < (int)statementArray.Length; i++)
			{
				statementArray[i].AcceptAlert(AlertInformation.MultipleWriteSessionsInTheSameRequest);
			}
		}
	}
}