using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.Messages;
using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.SessionProcessors
{
	public class HighNumberOfCacheCallsInSameRequestShouldBeAvoided : AbstractSessionProcessor
	{
		public HighNumberOfCacheCallsInSameRequestShouldBeAvoided()
		{
		}

		public override void OnSessionClosed(Session session)
		{
			if (!session.Id.ThreadId.StartsWith("ASP.Net/"))
			{
				return;
			}
			Session[] sessionsOnSameThread = base.GetSessionsOnSameThread(session);
			if ((int)sessionsOnSameThread.Length <= 1)
			{
				return;
			}
			if (((IEnumerable<Session>)sessionsOnSameThread).Sum<Session>((Session x) => x.CountOfCachedStatements) < base.Configuration.MaxNumberOfCachedStatementsPerSession)
			{
				return;
			}
			foreach (Statement statement in session.Statements)
			{
				if (statement.Options.HasFlag(SqlStatementOptions.DDL) || statement.Options.HasFlag(SqlStatementOptions.Transaction) || statement.Options.HasFlag(SqlStatementOptions.DtcTransaction) || !statement.Options.HasFlag(SqlStatementOptions.Cached))
				{
					continue;
				}
				statement.AcceptAlert(AlertInformation.TooManyCacheCallsInTheSameRequest);
			}
		}
	}
}