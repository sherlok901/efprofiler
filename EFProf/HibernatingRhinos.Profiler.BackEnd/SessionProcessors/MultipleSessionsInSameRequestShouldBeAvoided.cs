using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.Messages;
using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using System;
using System.Collections.Generic;

namespace HibernatingRhinos.Profiler.BackEnd.SessionProcessors
{
	public class MultipleSessionsInSameRequestShouldBeAvoided : AbstractSessionProcessor
	{
		public MultipleSessionsInSameRequestShouldBeAvoided()
		{
		}

		public override void OnSessionClosed(Session session)
		{
			if (!session.Id.ThreadId.StartsWith("ASP.Net/"))
			{
				return;
			}
			if ((int)base.GetSessionsOnSameThread(session).Length <= 1)
			{
				return;
			}
			foreach (Statement statement in session.Statements)
			{
				if (statement.Options.HasFlag(SqlStatementOptions.Warning) || statement.Options.HasFlag(SqlStatementOptions.DDL) || statement.IsTransaction)
				{
					continue;
				}
				statement.AcceptAlert(AlertInformation.MultipleSessionsInTheSameRequest);
			}
		}
	}
}