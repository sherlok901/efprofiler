using HibernatingRhinos.Profiler.BackEnd;
using System;

namespace HibernatingRhinos.Profiler.BackEnd.Bridge
{
	public interface IFilterableStatementSnapshot
	{
		QueryDuration Duration
		{
			get;
		}

		bool IsCached
		{
			get;
		}

		bool IsDDL
		{
			get;
		}

		bool IsTransaction
		{
			get;
		}

		string RawSql
		{
			get;
		}

		int SessionStatementCount
		{
			get;
		}
	}
}