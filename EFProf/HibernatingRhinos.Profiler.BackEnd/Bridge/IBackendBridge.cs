using HibernatingRhinos.Profiler.BackEnd.Reports;
using System;
using System.Collections.Generic;
using System.Net;

namespace HibernatingRhinos.Profiler.BackEnd.Bridge
{
	public interface IBackendBridge : IDisposable
	{
		NotificationsBridge Notifications
		{
			get;
		}

		ReportsBridge Reports
		{
			get;
		}

		void CancelLoadFileStatus(Guid loadFileId);

		void Clear();

		void ClearExcept(IList<Guid> sessionIds);

		Guid Connect(IPEndPoint endPoint, string productionPassword, Action<string, bool> handleConnectionStatus, Action reconnectDialog);

		void DeleteTempFile();

		void DisposeOf(Guid guid);

		LoadFileStatus GetLoadFileStatus(Guid loadFileId);

		void Pause();

		void Resume();

		void SaveToFile(string filename);

		Guid Start(int port, string contextFile = null);

		Guid StartLoadFromFile(string filename);
	}
}