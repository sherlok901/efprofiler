using HibernatingRhinos.Profiler.BackEnd;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.Bridge
{
	public class SessionUsageSnapshot
	{
		private Dictionary<AlertInformation, int> aggregatedAlerts;

		public Dictionary<AlertInformation, int> AggregatedAlerts
		{
			get
			{
				Dictionary<AlertInformation, int> alertInformations = this.aggregatedAlerts;
				if (alertInformations == null)
				{
					Dictionary<AlertInformation, int> alertInformations1 = new Dictionary<AlertInformation, int>(new SessionUsageSnapshot.AlertComparer());
					Dictionary<AlertInformation, int> alertInformations2 = alertInformations1;
					this.aggregatedAlerts = alertInformations1;
					alertInformations = alertInformations2;
				}
				return alertInformations;
			}
			set
			{
				this.aggregatedAlerts = value;
			}
		}

		public TimeSpan Duration
		{
			get;
			set;
		}

		public int EntitiesLoaded
		{
			get;
			set;
		}

		public SessionUsageSnapshot()
		{
		}

		private class AlertComparer : IEqualityComparer<AlertInformation>
		{
			public AlertComparer()
			{
			}

			public bool Equals(AlertInformation x, AlertInformation y)
			{
				if (x.Title != y.Title)
				{
					return false;
				}
				return x.Severity == y.Severity;
			}

			public int GetHashCode(AlertInformation obj)
			{
				return obj.Severity.GetHashCode() * 397 ^ (obj.Title != null ? obj.Title.GetHashCode() : 0);
			}
		}
	}
}