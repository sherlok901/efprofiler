using System;
using System.Runtime.CompilerServices;
using System.Threading;

namespace HibernatingRhinos.Profiler.BackEnd.Bridge
{
	public class CancellationToken
	{
		private volatile bool isCancelled;

		public int NumberOfLoadedMessages;

		private volatile bool wasCompleted;

		public System.Exception Exception;

		public long CurrentStremPosition
		{
			get;
			set;
		}

		public bool IsCancelled
		{
			get
			{
				return this.isCancelled;
			}
			set
			{
				this.isCancelled = value;
			}
		}

		public long StreamLength
		{
			get;
			set;
		}

		public bool WasCompleted
		{
			get
			{
				return this.wasCompleted;
			}
			set
			{
				this.wasCompleted = value;
				if (this.wasCompleted)
				{
					this.completed();
				}
			}
		}

		public CancellationToken()
		{
		}

		public void AddLoadedMessage()
		{
			Interlocked.Increment(ref this.NumberOfLoadedMessages);
		}

		public void Cancel()
		{
			this.IsCancelled = true;
		}

		private event Action completed;

		public event Action Completed
		{
			add
			{
				this.completed += value;
				if (this.WasCompleted && value != null)
				{
					value();
				}
			}
			remove
			{
				this.completed -= value;
			}
		}
	}
}