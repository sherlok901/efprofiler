using HibernatingRhinos.Profiler.Appender.StackTraces;
using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.Messages;
using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.Bridge
{
	public class StatementSnapshot
	{
		public AlertInformation[] Alerts
		{
			get;
			set;
		}

		public bool CanExecuteQuery
		{
			get;
			set;
		}

		public int[] CountOfRows
		{
			get;
			set;
		}

		public QueryDuration Duration
		{
			get;
			set;
		}

		public SolutionItem ErrorSolution
		{
			get;
			set;
		}

		public string FormattedSql
		{
			get;
			set;
		}

		public bool IsCached
		{
			get;
			set;
		}

		public bool IsDDL
		{
			get;
			set;
		}

		public bool IsSelectStatement
		{
			get;
			set;
		}

		public bool IsTransaction
		{
			get;
			set;
		}

		public string OrmError
		{
			get;
			set;
		}

		public IEnumerable<Parameter> Parameters
		{
			get;
			set;
		}

		public int Position
		{
			get;
			set;
		}

		public string Prefix
		{
			get;
			set;
		}

		public string RawSql
		{
			get;
			set;
		}

		public Guid SessionId
		{
			get;
			set;
		}

		public int SessionStatementCount
		{
			get;
			set;
		}

		public string ShortSql
		{
			get;
			set;
		}

		public StackTraceInfo StackTrace
		{
			get;
			set;
		}

		public string StarColor
		{
			get;
			set;
		}

		public Guid StatementId
		{
			get;
			set;
		}

		public string[] Tables
		{
			get;
			set;
		}

		public DateTime Timestamp
		{
			get;
			set;
		}

		public string Url
		{
			get;
			set;
		}

		public StatementSnapshot()
		{
		}

		public bool Equals(StatementSnapshot other)
		{
			if (object.ReferenceEquals(null, other))
			{
				return false;
			}
			if (object.ReferenceEquals(this, other))
			{
				return true;
			}
			if (!other.StatementId.Equals(this.StatementId) || !other.SessionId.Equals(this.SessionId) || !object.Equals(other.Duration, this.Duration) || !other.IsCached.Equals(this.IsCached) || !other.IsDDL.Equals(this.IsDDL) || !object.Equals(other.FormattedSql, this.FormattedSql) || !object.Equals(other.RawSql, this.RawSql) || !object.Equals(other.ShortSql, this.ShortSql) || !StatementSnapshot.IsSetEquals<int>(this.CountOfRows, this.CountOfRows) || !object.Equals(other.StackTrace, this.StackTrace) || !StatementSnapshot.IsSetEquals<Parameter>(other.Parameters, this.Parameters) || !object.Equals(other.Url, this.Url) || !object.Equals(other.StarColor, this.StarColor) || !other.IsSelectStatement.Equals(this.IsSelectStatement) || !other.IsTransaction.Equals(this.IsTransaction) || !other.CanExecuteQuery.Equals(this.CanExecuteQuery) || !StatementSnapshot.IsSetEquals<AlertInformation>(other.Alerts, this.Alerts) || !other.Timestamp.Equals(this.Timestamp))
			{
				return false;
			}
			return object.Equals(other.Prefix, this.Prefix);
		}

		public override bool Equals(object obj)
		{
			if (object.ReferenceEquals(null, obj))
			{
				return false;
			}
			if (object.ReferenceEquals(this, obj))
			{
				return true;
			}
			if (obj.GetType() != typeof(StatementSnapshot))
			{
				return false;
			}
			return this.Equals((StatementSnapshot)obj);
		}

		public override int GetHashCode()
		{
			int hashCode = this.StatementId.GetHashCode();
			Guid sessionId = this.SessionId;
			hashCode = hashCode * 397 ^ sessionId.GetHashCode();
			hashCode = hashCode * 397 ^ (this.Duration != null ? this.Duration.GetHashCode() : 0);
			bool isCached = this.IsCached;
			hashCode = hashCode * 397 ^ isCached.GetHashCode();
			bool isDDL = this.IsDDL;
			hashCode = hashCode * 397 ^ isDDL.GetHashCode();
			hashCode = hashCode * 397 ^ (this.FormattedSql != null ? this.FormattedSql.GetHashCode() : 0);
			hashCode = hashCode * 397 ^ (this.RawSql != null ? this.RawSql.GetHashCode() : 0);
			hashCode = hashCode * 397 ^ (this.ShortSql != null ? this.ShortSql.GetHashCode() : 0);
			int[] countOfRows = this.CountOfRows;
			for (int i = 0; i < (int)countOfRows.Length; i++)
			{
				hashCode = hashCode * 397 ^ countOfRows[i];
			}
			hashCode = hashCode * 397 ^ (this.StackTrace != null ? this.StackTrace.GetHashCode() : 0);
			hashCode = hashCode * 397 ^ (this.Parameters != null ? this.Parameters.GetHashCode() : 0);
			hashCode = hashCode * 397 ^ (this.Url != null ? this.Url.GetHashCode() : 0);
			hashCode = hashCode * 397 ^ (this.StarColor != null ? this.StarColor.GetHashCode() : 0);
			bool isSelectStatement = this.IsSelectStatement;
			hashCode = hashCode * 397 ^ isSelectStatement.GetHashCode();
			bool isTransaction = this.IsTransaction;
			hashCode = hashCode * 397 ^ isTransaction.GetHashCode();
			bool canExecuteQuery = this.CanExecuteQuery;
			hashCode = hashCode * 397 ^ canExecuteQuery.GetHashCode();
			hashCode = hashCode * 397 ^ (this.Alerts != null ? this.Alerts.GetHashCode() : 0);
			DateTime timestamp = this.Timestamp;
			hashCode = hashCode * 397 ^ timestamp.GetHashCode();
			hashCode = hashCode * 397 ^ (this.Prefix != null ? this.Prefix.GetHashCode() : 0);
			return hashCode;
		}

		private static bool IsSetEquals<T>(IEnumerable<T> x, IEnumerable<T> y)
		{
			return !x.ZipAll<T, T, bool>(y, (T t, T z) => object.Equals(t, z)).Any<bool>((bool r) => !r);
		}
	}
}