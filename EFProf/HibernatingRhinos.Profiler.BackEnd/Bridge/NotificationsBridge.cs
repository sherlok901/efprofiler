using HibernatingRhinos.Profiler.Appender;
using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.Extensions;
using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using log4net;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;

namespace HibernatingRhinos.Profiler.BackEnd.Bridge
{
	public class NotificationsBridge
	{
		private const int MaxBackendNotifications = 100000;

		private readonly ILog log = LogManager.GetLogger(typeof(NotificationsBridge));

		private ConcurrentQueue<BackendNotification> lastNotifications;

		public ConcurrentQueue<BackendNotification> LastNotifications
		{
			get
			{
				ConcurrentQueue<BackendNotification> backendNotifications = this.lastNotifications;
				if (backendNotifications == null)
				{
					ConcurrentQueue<BackendNotification> backendNotifications1 = new ConcurrentQueue<BackendNotification>();
					ConcurrentQueue<BackendNotification> backendNotifications2 = backendNotifications1;
					this.lastNotifications = backendNotifications1;
					backendNotifications = backendNotifications2;
				}
				return backendNotifications;
			}
			set
			{
				this.lastNotifications = value;
			}
		}

		private ReactiveModelBuilder ModelBuilder
		{
			get;
			set;
		}

		public long NotificationsEtag
		{
			get;
			set;
		}

		public NotificationsBridge(ReactiveModelBuilder modelBuilder)
		{
			this.ModelBuilder = modelBuilder;
		}

		private IEnumerable<ApplicationInstanceInformation> DistinctAttachedApplicationsStatus(ApplicationInstanceInformation[] applications)
		{
			ApplicationInstanceInformation[] applicationInstanceInformationArray = applications;
			foreach (IGrouping<Guid, ApplicationInstanceInformation> guids in 
				from information in (IEnumerable<ApplicationInstanceInformation>)applicationInstanceInformationArray
				group information by information.Id)
			{
				ApplicationInstanceInformation applicationInstanceInformation = guids.Last<ApplicationInstanceInformation>();
				if (!applicationInstanceInformation.IsDetached)
				{
					yield return applicationInstanceInformation;
				}
				else
				{
					if (guids.Count<ApplicationInstanceInformation>() != 1)
					{
						continue;
					}
					yield return applicationInstanceInformation;
				}
			}
		}

		public BackendNotification GetBackendNotificationsSinceLastRequest(long etag)
		{
			BackendNotification backendNotification;
			long notificationsEtag = this.NotificationsEtag;
			if (etag > notificationsEtag)
			{
				BackendNotification backendNotification1 = new BackendNotification()
				{
					Etag = notificationsEtag,
					AttachedApplicationsStatus = new ApplicationInstanceInformation[0]
				};
				return backendNotification1;
			}
			try
			{
				List<BackendNotification> list = this.NotificationsAfterEtag(etag).ToList<BackendNotification>();
				BackendNotification backendNotification2 = new BackendNotification()
				{
					OpenFileInfo = (
						from notification in list
						select notification.OpenFileInfo).LastOrDefault<OpenFileInfo>((OpenFileInfo openFileInfo) => openFileInfo != null),
					BringApplicationToFront = list.Any<BackendNotification>((BackendNotification notification) => notification.BringApplicationToFront),
					ChangedSessions = (
						from notification in list
						where notification.ChangedSessionId.HasValue
						select notification.ChangedSessionId.Value).Distinct<Guid>().Select<Guid, Session>((Guid sessionId) => {
						Session session1;
						if (this.ModelBuilder.SessionsById.TryGetValue(sessionId, out session1))
						{
							return session1;
						}
						return null;
					}).Where<Session>((Session session) => session != null).ToArray<Session>(),
					ChangedStatements = (
						from notification in list
						where notification.ChangedStatementId != null
						select notification.ChangedStatementId).DistinctBy<SessionStatementId, Guid>((SessionStatementId statement) => statement.StatementId).Select<SessionStatementId, Statement>(new Func<SessionStatementId, Statement>(this.SelectStatement)).Where<Statement>((Statement statement) => statement != null).OrderBy<Statement, Guid>((Statement statement) => statement.InternalSessionId).ThenBy<Statement, int>((Statement statement) => statement.Position).ToArray<Statement>(),
					AttachedApplicationsStatus = this.DistinctAttachedApplicationsStatus((
						from notification in list
						where notification.AttachedApplicationsStatus != null
						select notification).SelectMany<BackendNotification, ApplicationInstanceInformation>((BackendNotification notification) => notification.AttachedApplicationsStatus).ToArray<ApplicationInstanceInformation>()).ToArray<ApplicationInstanceInformation>(),
					Etag = (list.Count == 0 ? notificationsEtag : list[list.Count - 1].Etag)
				};
				backendNotification = backendNotification2;
			}
			catch (Exception exception)
			{
				this.log.Error(string.Concat("Error when building backend notification. ", exception));
				throw;
			}
			return backendNotification;
		}

		public SessionUsageSnapshot GetSessionUsageSnapshot(Guid sessionId)
		{
			KeyValuePair<Guid, Session> keyValuePair = this.ModelBuilder.SessionsById.FirstOrDefault<KeyValuePair<Guid, Session>>((KeyValuePair<Guid, Session> x) => x.Value.InternalId == sessionId);
			Session value = keyValuePair.Value;
			if (value == null)
			{
				return null;
			}
			SessionUsageSnapshot sessionUsageSnapshot = new SessionUsageSnapshot();
			SessionUsageSnapshot sessionUsageSnapshot1 = sessionUsageSnapshot;
			TimeSpan? duration = value.Duration;
			sessionUsageSnapshot1.Duration = (duration.HasValue ? duration.GetValueOrDefault() : TimeSpan.Zero);
			sessionUsageSnapshot.EntitiesLoaded = value.EntitySnapshots.Sum<EntitySnapshot>((EntitySnapshot x) => x.Identifiers.Count);
			SessionUsageSnapshot sessionUsageSnapshot2 = sessionUsageSnapshot;
			foreach (AlertInformation alertInformation in value.Statements.SelectMany<Statement, AlertInformation>((Statement x) => x.Alerts))
			{
				if (!sessionUsageSnapshot2.AggregatedAlerts.ContainsKey(alertInformation))
				{
					sessionUsageSnapshot2.AggregatedAlerts[alertInformation] = 0;
				}
				Dictionary<AlertInformation, int> aggregatedAlerts = sessionUsageSnapshot2.AggregatedAlerts;
				Dictionary<AlertInformation, int> alertInformations = aggregatedAlerts;
				AlertInformation alertInformation1 = alertInformation;
				aggregatedAlerts[alertInformation1] = alertInformations[alertInformation1] + 1;
			}
			return sessionUsageSnapshot2;
		}

		public StatisticsSnapshot[] GetStatisticsSnapshots()
		{
			SessionFactoryStats[] array = this.ModelBuilder.CurrentStats.Values.ToArray<SessionFactoryStats>();
			return ((IEnumerable<SessionFactoryStats>)array).Select<SessionFactoryStats, StatisticsSnapshot>((SessionFactoryStats x) => {
				Dictionary<string, object> dictionary = x.Statistics.Cast<DictionaryEntry>().ToDictionary<DictionaryEntry, string, object>((DictionaryEntry entry) => (string)entry.Key, (DictionaryEntry entry) => entry.Value);
				return new StatisticsSnapshot()
				{
					Name = x.Name,
					Statistics = dictionary
				};
			}).OrderByDescending<StatisticsSnapshot, int>(new Func<StatisticsSnapshot, int>(NotificationsBridge.SumOfActiveStatistics)).ToArray<StatisticsSnapshot>();
		}

		private IEnumerable<BackendNotification> NotificationsAfterEtag(long etag)
		{
			return (
				from notification in this.LastNotifications
				where notification.Etag >= etag
				orderby notification.Etag
				select notification).Take<BackendNotification>(1024);
		}

		public void OnApplicationStatusChanged(ApplicationInstanceInformation instanceInformation)
		{
			ConcurrentQueue<BackendNotification> lastNotifications = this.LastNotifications;
			BackendNotification backendNotification = new BackendNotification()
			{
				AttachedApplicationsStatus = new ApplicationInstanceInformation[] { instanceInformation }
			};
			NotificationsBridge notificationsBridge = this;
			long notificationsEtag = notificationsBridge.NotificationsEtag + (long)1;
			long num = notificationsEtag;
			notificationsBridge.NotificationsEtag = notificationsEtag;
			backendNotification.Etag = num;
			lastNotifications.EnqueueMaxCapacity<BackendNotification>(backendNotification, 100000);
		}

		public void OnSessionHaveBeenChanged(Session session)
		{
			ConcurrentQueue<BackendNotification> lastNotifications = this.LastNotifications;
			BackendNotification backendNotification = new BackendNotification()
			{
				ChangedSessionId = new Guid?(session.InternalId)
			};
			NotificationsBridge notificationsBridge = this;
			long notificationsEtag = notificationsBridge.NotificationsEtag + (long)1;
			long num = notificationsEtag;
			notificationsBridge.NotificationsEtag = notificationsEtag;
			backendNotification.Etag = num;
			lastNotifications.EnqueueMaxCapacity<BackendNotification>(backendNotification, 100000);
		}

		public void OnStatementHaveBeenChanged(Statement statement)
		{
			ConcurrentQueue<BackendNotification> lastNotifications = this.LastNotifications;
			BackendNotification backendNotification = new BackendNotification();
			SessionStatementId sessionStatementId = new SessionStatementId()
			{
				SessionId = statement.InternalSessionId,
				StatementId = statement.InternalId
			};
			backendNotification.ChangedStatementId = sessionStatementId;
			backendNotification.ChangedSessionId = new Guid?(statement.InternalSessionId);
			NotificationsBridge notificationsBridge = this;
			long notificationsEtag = notificationsBridge.NotificationsEtag + (long)1;
			long num = notificationsEtag;
			notificationsBridge.NotificationsEtag = notificationsEtag;
			backendNotification.Etag = num;
			lastNotifications.EnqueueMaxCapacity<BackendNotification>(backendNotification, 100000);
		}

		public void RaiseBringApplicationToFront(OpenFileInfo openFileInfo)
		{
			ConcurrentQueue<BackendNotification> lastNotifications = this.LastNotifications;
			BackendNotification backendNotification = new BackendNotification()
			{
				BringApplicationToFront = true,
				OpenFileInfo = openFileInfo
			};
			NotificationsBridge notificationsBridge = this;
			long notificationsEtag = notificationsBridge.NotificationsEtag + (long)1;
			long num = notificationsEtag;
			notificationsBridge.NotificationsEtag = notificationsEtag;
			backendNotification.Etag = num;
			lastNotifications.EnqueueMaxCapacity<BackendNotification>(backendNotification, 100000);
		}

		public void RaiseShutdownAndOutputReport()
		{
			this.ShutdownAndOutputReport();
		}

		private Statement SelectStatement(SessionStatementId statement)
		{
			Session session;
			Statement statement1;
			if (!this.ModelBuilder.SessionsById.TryGetValue(statement.SessionId, out session))
			{
				return null;
			}
			session.StatementsById.TryGetValue(statement.StatementId, out statement1);
			return statement1;
		}

		private static int SumOfActiveStatistics(StatisticsSnapshot statisticsSnapshot)
		{
			int num;
			int num1 = 0;
			foreach (object value in statisticsSnapshot.Statistics.Values)
			{
				string str = value as string;
				if (str != null)
				{
					if (!int.TryParse(str, out num))
					{
						continue;
					}
					num1 += num;
				}
				else if (!(value is int))
				{
					if (value is IEnumerable)
					{
						continue;
					}
					try
					{
						num1 += Convert.ToInt32(value);
					}
					catch (Exception exception)
					{
					}
				}
				else
				{
					num1 += (int)value;
				}
			}
			return num1;
		}

		public event Action ShutdownAndOutputReport;
	}
}