using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.Bridge
{
	public class BackendNotification
	{
		public ApplicationInstanceInformation[] AttachedApplicationsStatus
		{
			get;
			set;
		}

		public bool BringApplicationToFront
		{
			get;
			set;
		}

		public Guid? ChangedSessionId
		{
			get;
			set;
		}

		public IEnumerable<Session> ChangedSessions
		{
			get;
			set;
		}

		public SessionStatementId ChangedStatementId
		{
			get;
			set;
		}

		public IEnumerable<Statement> ChangedStatements
		{
			get;
			set;
		}

		public long Etag
		{
			get;
			set;
		}

		public HibernatingRhinos.Profiler.BackEnd.Bridge.OpenFileInfo OpenFileInfo
		{
			get;
			set;
		}

		public BackendNotification()
		{
		}
	}
}