using System;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.Bridge
{
	public class SessionStatistics
	{
		public bool HasMajor
		{
			get;
			set;
		}

		public bool HasSuggestions
		{
			get;
			set;
		}

		public bool HasWarnings
		{
			get;
			set;
		}

		public string Name
		{
			get;
			set;
		}

		public int NumberOfCachedStatements
		{
			get;
			set;
		}

		public int NumberOfStatements
		{
			get;
			set;
		}

		public int NumberOfTransactionsStatements
		{
			get;
			set;
		}

		public Guid SessionId
		{
			get;
			set;
		}

		public SessionStatistics()
		{
		}
	}
}