using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.Bridge
{
	public class StatisticsSnapshot
	{
		public string Name
		{
			get;
			set;
		}

		public Dictionary<string, object> Statistics
		{
			get;
			set;
		}

		public StatisticsSnapshot()
		{
		}
	}
}