using System;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.Bridge
{
	public class SessionStatementId
	{
		public Guid SessionId
		{
			get;
			set;
		}

		public Guid StatementId
		{
			get;
			set;
		}

		public SessionStatementId()
		{
		}
	}
}