using System;

namespace HibernatingRhinos.Profiler.BackEnd.Bridge
{
	public interface IFilterableSessionSnapshot
	{
		int StatementCount
		{
			get;
		}

		string Url
		{
			get;
		}
	}
}