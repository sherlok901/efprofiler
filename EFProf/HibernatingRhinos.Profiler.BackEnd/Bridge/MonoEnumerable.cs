using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.Bridge
{
	public static class MonoEnumerable
	{
		private static IEnumerable<TResult> CreateZipIterator<TFirst, TSecond, TResult>(IEnumerable<TFirst> first, IEnumerable<TSecond> second, Func<TFirst, TSecond, TResult> selector)
		{
			using (IEnumerator<TFirst> enumerator = first.GetEnumerator())
			{
				using (IEnumerator<TSecond> enumerator1 = second.GetEnumerator())
				{
					while (enumerator.MoveNext())
					{
						if (!enumerator1.MoveNext())
						{
							yield return selector(enumerator.Current, default(TSecond));
						}
						else
						{
							yield return selector(enumerator.Current, enumerator1.Current);
						}
					}
					while (enumerator1.MoveNext())
					{
						yield return selector(default(TFirst), enumerator1.Current);
					}
				}
			}
		}

		public static IEnumerable<TResult> ZipAll<TFirst, TSecond, TResult>(this IEnumerable<TFirst> first, IEnumerable<TSecond> second, Func<TFirst, TSecond, TResult> resultSelector)
		{
			if (first == null)
			{
				throw new ArgumentNullException("first");
			}
			if (second == null)
			{
				throw new ArgumentNullException("second");
			}
			if (resultSelector == null)
			{
				throw new ArgumentNullException("resultSelector");
			}
			return MonoEnumerable.CreateZipIterator<TFirst, TSecond, TResult>(first, second, resultSelector);
		}
	}
}