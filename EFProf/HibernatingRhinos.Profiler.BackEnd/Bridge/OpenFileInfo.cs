using System;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.Bridge
{
	public class OpenFileInfo
	{
		public string FileName
		{
			get;
			set;
		}

		public Guid Token
		{
			get;
			set;
		}

		public OpenFileInfo()
		{
		}
	}
}