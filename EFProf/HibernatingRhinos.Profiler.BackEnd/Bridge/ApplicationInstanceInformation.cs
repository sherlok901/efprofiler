using System;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.Bridge
{
	public class ApplicationInstanceInformation
	{
		public Guid Id
		{
			get;
			set;
		}

		public bool IsDetached
		{
			get;
			set;
		}

		public bool IsProductionApplication
		{
			get;
			set;
		}

		public string Name
		{
			get;
			set;
		}

		public ApplicationInstanceInformation()
		{
		}
	}
}