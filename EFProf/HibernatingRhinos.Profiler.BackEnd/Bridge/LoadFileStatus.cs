using System;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.Bridge
{
	public class LoadFileStatus
	{
		public bool Complete
		{
			get;
			set;
		}

		public string ExceptionMessage
		{
			get;
			set;
		}

		public int NumberOfLoadedMessages
		{
			get;
			set;
		}

		public int NumberOfWaitingMessages
		{
			get;
			set;
		}

		public int PercentLoaded
		{
			get;
			set;
		}

		public LoadFileStatus()
		{
		}
	}
}