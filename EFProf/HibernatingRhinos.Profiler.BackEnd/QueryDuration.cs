using System;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd
{
	[Serializable]
	public class QueryDuration : IEquatable<QueryDuration>
	{
		public Action ValuesRefreshed = new Action(() => {
		});

		private int? inDatabase;

		private int? inNHibernate;

		public int? InDatabase
		{
			get
			{
				return this.inDatabase;
			}
			set
			{
				this.inDatabase = value;
				this.RaiseValuesRefreshed();
			}
		}

		public int? InNHibernate
		{
			get
			{
				return this.inNHibernate;
			}
			set
			{
				this.inNHibernate = value;
				this.RaiseValuesRefreshed();
			}
		}

		public int? Value
		{
			get
			{
				int? inDatabase = this.InDatabase;
				if (!inDatabase.HasValue)
				{
					return this.InNHibernate;
				}
				return new int?(inDatabase.GetValueOrDefault());
			}
		}

		public QueryDuration()
		{
		}

		public bool Equals(QueryDuration other)
		{
			if (object.ReferenceEquals(null, other))
			{
				return false;
			}
			if (object.ReferenceEquals(this, other))
			{
				return true;
			}
			if (!other.InDatabase.Equals(this.InDatabase))
			{
				return false;
			}
			return other.InNHibernate.Equals(this.InNHibernate);
		}

		public override bool Equals(object obj)
		{
			if (object.ReferenceEquals(null, obj))
			{
				return false;
			}
			if (object.ReferenceEquals(this, obj))
			{
				return true;
			}
			return this.Equals((QueryDuration)obj);
		}

		public override int GetHashCode()
		{
			return (this.InDatabase.HasValue ? this.InDatabase.Value : 0) * 397 ^ (this.InNHibernate.HasValue ? this.InNHibernate.Value : 0);
		}

		public void RaiseValuesRefreshed()
		{
			Action valuesRefreshed = this.ValuesRefreshed;
			if (valuesRefreshed != null)
			{
				valuesRefreshed();
			}
		}
	}
}