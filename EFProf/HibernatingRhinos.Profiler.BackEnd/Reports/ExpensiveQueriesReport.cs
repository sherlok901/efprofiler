using HibernatingRhinos.Profiler.BackEnd.Extensions;
using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.Reports
{
	public class ExpensiveQueriesReport
	{
		private readonly IDictionary<string, QueryAggregation> queriesLookup = new Dictionary<string, QueryAggregation>();

		public ExpensiveQueriesReport()
		{
		}

		public void AddQuery(Statement statement)
		{
			this.queriesLookup.AggregateStatement(statement);
		}

		public void Clear()
		{
			this.queriesLookup.Clear();
		}

		public void ClearExcept(string[] sessionIds)
		{
			(
				from pair in this.queriesLookup.ToArray<KeyValuePair<string, QueryAggregation>>()
				where !pair.Value.ClearExcept(sessionIds)
				select pair).ForEach<KeyValuePair<string, QueryAggregation>>((KeyValuePair<string, QueryAggregation> pair) => this.queriesLookup.Remove(pair.Key));
		}

		public QueryAggregationSnapshot[] GetReportSnapshot()
		{
			int num = (int)Math.Ceiling((double)this.queriesLookup.Count / 20);
			int num1 = Math.Max(num, 5);
			return (
				from agg in (IEnumerable<QueryAggregation>)this.queriesLookup.Values.ToArray<QueryAggregation>()
				orderby agg.AverageDuration descending
				select new QueryAggregationSnapshot()
				{
					AverageDuration = agg.AverageDuration,
					Count = agg.Count,
					RawSql = agg.RawSql,
					ShortSql = agg.ShortSql,
					FormattedSql = agg.FormattedSql,
					RelatedSessions = agg.RelatedSessions.ToArray<RelatedSessionSpecification>()
				}).Take<QueryAggregationSnapshot>(num1).ToArray<QueryAggregationSnapshot>();
		}
	}
}