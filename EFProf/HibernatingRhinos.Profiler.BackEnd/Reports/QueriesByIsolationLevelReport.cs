using HibernatingRhinos.Profiler.BackEnd.Extensions;
using HibernatingRhinos.Profiler.BackEnd.Messages;
using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.Reports
{
	public class QueriesByIsolationLevelReport
	{
		private readonly Dictionary<string, QueriesByIsolationLevelReport.IsolationLevelQueries> queriesByIsolationLevel = new Dictionary<string, QueriesByIsolationLevelReport.IsolationLevelQueries>();

		public QueriesByIsolationLevelReport()
		{
		}

		private void AddQuery(Statement statement, string sessionId, string isolationLevel)
		{
			QueriesByIsolationLevelReport.IsolationLevelQueries isolationLevelQuery;
			if (statement.IsTransaction || statement.Options.HasFlag(SqlStatementOptions.Cached) || statement.Options.HasFlag(SqlStatementOptions.Warning))
			{
				return;
			}
			if (string.IsNullOrEmpty(isolationLevel))
			{
				return;
			}
			if (!this.queriesByIsolationLevel.TryGetValue(isolationLevel, out isolationLevelQuery))
			{
				isolationLevelQuery = new QueriesByIsolationLevelReport.IsolationLevelQueries(isolationLevel, sessionId);
				this.queriesByIsolationLevel.Add(isolationLevel, isolationLevelQuery);
			}
			isolationLevelQuery.AddQuery(statement);
		}

		public void AddTransactionalQueries(Session session)
		{
			List<Statement> list = session.Statements.ToList<Statement>();
			foreach (Statement statement in list)
			{
				this.AddQuery(statement, session.Id.ToString(), list[0].IsolationLevel);
			}
		}

		public void Clear()
		{
			this.queriesByIsolationLevel.Clear();
		}

		public void ClearExcept(string[] sessionIds)
		{
			(
				from pair in this.queriesByIsolationLevel.ToArray<KeyValuePair<string, QueriesByIsolationLevelReport.IsolationLevelQueries>>()
				where !pair.Value.ClearExcept(sessionIds)
				select pair).ForEach<KeyValuePair<string, QueriesByIsolationLevelReport.IsolationLevelQueries>>((KeyValuePair<string, QueriesByIsolationLevelReport.IsolationLevelQueries> pair) => this.queriesByIsolationLevel.Remove(pair.Key));
		}

		public IEnumerable<IsolationLevelQueriesSnapshot> GetReportSnapshot()
		{
			return (
				from queryByIsolationLevel in (IEnumerable<QueriesByIsolationLevelReport.IsolationLevelQueries>)this.queriesByIsolationLevel.Values.ToArray<QueriesByIsolationLevelReport.IsolationLevelQueries>()
				select new IsolationLevelQueriesSnapshot()
				{
					IsolationLevel = queryByIsolationLevel.IsolationLevel,
					SessionId = queryByIsolationLevel.SessionId,
					Statements = queryByIsolationLevel.GetStatementSnapshot()
				}).ToArray<IsolationLevelQueriesSnapshot>();
		}

		private class IsolationLevelQueries
		{
			private readonly Dictionary<string, QueryAggregation> statements;

			public string IsolationLevel
			{
				get;
				private set;
			}

			public string SessionId
			{
				get;
				private set;
			}

			public IsolationLevelQueries(string isolationLevel, string sessionId)
			{
				this.IsolationLevel = isolationLevel;
				this.SessionId = sessionId;
			}

			public void AddQuery(Statement statement)
			{
				this.statements.AggregateStatement(statement);
			}

			public bool ClearExcept(string[] sessionIds)
			{
				foreach (KeyValuePair<string, QueryAggregation> keyValuePair in 
					from pair in this.statements.ToArray<KeyValuePair<string, QueryAggregation>>()
					where !pair.Value.ClearExcept(sessionIds)
					select pair)
				{
					this.statements.Remove(keyValuePair.Key);
				}
				return this.statements.Count > 0;
			}

			public List<QueryAggregationSnapshot> GetStatementSnapshot()
			{
				return (
					from agg in this.statements.Values
					select new QueryAggregationSnapshot()
					{
						RawSql = agg.RawSql,
						ShortSql = agg.ShortSql,
						FormattedSql = agg.FormattedSql,
						AverageDuration = agg.AverageDuration,
						Count = agg.Count,
						Options = agg.Options,
						RelatedSessions = agg.RelatedSessions.ToArray<RelatedSessionSpecification>()
					}).ToList<QueryAggregationSnapshot>();
			}
		}
	}
}