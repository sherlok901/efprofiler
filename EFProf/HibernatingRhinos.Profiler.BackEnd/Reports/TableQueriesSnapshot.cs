using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.Reports
{
	public class TableQueriesSnapshot
	{
		public string Name
		{
			get;
			set;
		}

		public List<QueryAggregationSnapshot> Statements
		{
			get;
			set;
		}

		public TableQueriesSnapshot()
		{
		}
	}
}