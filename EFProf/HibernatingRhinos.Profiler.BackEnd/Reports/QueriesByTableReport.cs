using HibernatingRhinos.Profiler.BackEnd.Extensions;
using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.Reports
{
	public class QueriesByTableReport
	{
		private readonly Dictionary<string, QueriesByTableReport.TableQueries> queriesByTables = new Dictionary<string, QueriesByTableReport.TableQueries>();

		public QueriesByTableReport()
		{
		}

		public void AddQuery(Statement statement)
		{
			QueriesByTableReport.TableQueries tableQuery;
			if (statement == null || statement.Tables == null || (int)statement.Tables.Length == 0)
			{
				return;
			}
			string[] tables = statement.Tables;
			for (int i = 0; i < (int)tables.Length; i++)
			{
				string str = tables[i];
				if (!this.queriesByTables.TryGetValue(str, out tableQuery))
				{
					tableQuery = new QueriesByTableReport.TableQueries(str);
					this.queriesByTables[str] = tableQuery;
				}
				tableQuery.AddQuery(statement);
			}
		}

		public void Clear()
		{
			this.queriesByTables.Clear();
		}

		public void ClearExcept(string[] sessionIds)
		{
			(
				from pair in this.queriesByTables.ToArray<KeyValuePair<string, QueriesByTableReport.TableQueries>>()
				where !pair.Value.ClearExcept(sessionIds)
				select pair).ForEach<KeyValuePair<string, QueriesByTableReport.TableQueries>>((KeyValuePair<string, QueriesByTableReport.TableQueries> pair) => this.queriesByTables.Remove(pair.Key));
		}

		public List<TableQueriesSnapshot> GetReportSnapshot()
		{
			return (
				from queryByTable in (IEnumerable<QueriesByTableReport.TableQueries>)this.queriesByTables.Values.ToArray<QueriesByTableReport.TableQueries>()
				select new TableQueriesSnapshot()
				{
					Name = queryByTable.Name,
					Statements = queryByTable.GetStatementSnapshot()
				}).ToList<TableQueriesSnapshot>();
		}

		private class TableQueries
		{
			private readonly Dictionary<string, QueryAggregation> statements;

			public string Name
			{
				get;
				private set;
			}

			public TableQueries(string tableName)
			{
				this.Name = tableName;
			}

			public void AddQuery(Statement statement)
			{
				this.statements.AggregateStatement(statement);
			}

			public bool ClearExcept(string[] sessionIds)
			{
				foreach (KeyValuePair<string, QueryAggregation> keyValuePair in 
					from pair in this.statements.ToArray<KeyValuePair<string, QueryAggregation>>()
					where !pair.Value.ClearExcept(sessionIds)
					select pair)
				{
					this.statements.Remove(keyValuePair.Key);
				}
				return this.statements.Count > 0;
			}

			public List<QueryAggregationSnapshot> GetStatementSnapshot()
			{
				return (
					from agg in this.statements.Values
					select new QueryAggregationSnapshot()
					{
						RawSql = agg.RawSql,
						ShortSql = agg.ShortSql,
						FormattedSql = agg.FormattedSql,
						AverageDuration = agg.AverageDuration,
						Count = agg.Count,
						Options = agg.Options,
						RelatedSessions = agg.RelatedSessions.ToArray<RelatedSessionSpecification>()
					}).ToList<QueryAggregationSnapshot>();
			}
		}
	}
}