using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.Reports
{
	public class MethodQueriesSnapshot
	{
		public string FullName
		{
			get;
			set;
		}

		public string Name
		{
			get;
			set;
		}

		public List<QueryAggregationSnapshot> Statements
		{
			get;
			set;
		}

		public string TypeName
		{
			get;
			set;
		}

		public MethodQueriesSnapshot()
		{
		}
	}
}