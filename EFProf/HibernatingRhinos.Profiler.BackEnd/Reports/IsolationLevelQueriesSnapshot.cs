using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.Reports
{
	public class IsolationLevelQueriesSnapshot
	{
		public string IsolationLevel
		{
			get;
			set;
		}

		public string SessionId
		{
			get;
			set;
		}

		public List<QueryAggregationSnapshot> Statements
		{
			get;
			set;
		}

		public IsolationLevelQueriesSnapshot()
		{
		}
	}
}