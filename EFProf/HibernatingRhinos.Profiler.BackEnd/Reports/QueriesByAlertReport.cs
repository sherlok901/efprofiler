using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.Extensions;
using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.Reports
{
	public class QueriesByAlertReport
	{
		private readonly Dictionary<string, QueriesByAlertReport.AlertQueries> queriesByAlert = new Dictionary<string, QueriesByAlertReport.AlertQueries>();

		public QueriesByAlertReport()
		{
		}

		public void AddQuery(Statement statement)
		{
			QueriesByAlertReport.AlertQueries alertQuery;
			if (statement == null || statement.Alerts == null || statement.Alerts.Count == 0)
			{
				return;
			}
			foreach (AlertInformation alert in statement.Alerts)
			{
				string helpTopic = alert.HelpTopic;
				if (!this.queriesByAlert.TryGetValue(helpTopic, out alertQuery))
				{
					alertQuery = new QueriesByAlertReport.AlertQueries(alert);
					this.queriesByAlert[helpTopic] = alertQuery;
				}
				alertQuery.AddQuery(statement);
			}
		}

		public void Clear()
		{
			this.queriesByAlert.Clear();
		}

		public void ClearExcept(string[] sessionIds)
		{
			(
				from pair in this.queriesByAlert.ToArray<KeyValuePair<string, QueriesByAlertReport.AlertQueries>>()
				where !pair.Value.ClearExcept(sessionIds)
				select pair).ForEach<KeyValuePair<string, QueriesByAlertReport.AlertQueries>>((KeyValuePair<string, QueriesByAlertReport.AlertQueries> pair) => this.queriesByAlert.Remove(pair.Key));
		}

		public List<AlertQueriesSnapshot> GetReportSnapshot()
		{
			return (
				from queryByAlert in (IEnumerable<QueriesByAlertReport.AlertQueries>)this.queriesByAlert.Values.ToArray<QueriesByAlertReport.AlertQueries>()
				select new AlertQueriesSnapshot()
				{
					Alert = queryByAlert.Alert,
					Statements = queryByAlert.GetStatementSnapshot()
				}).ToList<AlertQueriesSnapshot>();
		}

		private class AlertQueries
		{
			private readonly Dictionary<string, QueryAggregation> statements;

			public AlertInformation Alert
			{
				get;
				private set;
			}

			public AlertQueries(AlertInformation alert)
			{
				this.Alert = alert;
			}

			public void AddQuery(Statement statement)
			{
				this.statements.AggregateStatement(statement);
			}

			public bool ClearExcept(string[] sessionIds)
			{
				foreach (KeyValuePair<string, QueryAggregation> keyValuePair in 
					from pair in this.statements.ToArray<KeyValuePair<string, QueryAggregation>>()
					where !pair.Value.ClearExcept(sessionIds)
					select pair)
				{
					this.statements.Remove(keyValuePair.Key);
				}
				return this.statements.Count > 0;
			}

			public List<QueryAggregationSnapshot> GetStatementSnapshot()
			{
				return (
					from agg in this.statements.Values
					select new QueryAggregationSnapshot()
					{
						RawSql = agg.RawSql,
						ShortSql = agg.ShortSql,
						FormattedSql = agg.FormattedSql,
						AverageDuration = agg.AverageDuration,
						Count = agg.Count,
						Options = agg.Options,
						RelatedSessions = agg.RelatedSessions.ToArray<RelatedSessionSpecification>()
					}).ToList<QueryAggregationSnapshot>();
			}
		}
	}
}