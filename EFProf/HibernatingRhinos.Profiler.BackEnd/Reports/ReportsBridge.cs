using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using System;
using System.Collections.Concurrent;
using System.Runtime.CompilerServices;
using System.Threading;

namespace HibernatingRhinos.Profiler.BackEnd.Reports
{
	public class ReportsBridge
	{
		public ExpensiveQueriesReport ExpensiveQueries
		{
			get;
			private set;
		}

		public OverallUsageReport OverallUsage
		{
			get;
			private set;
		}

		public QueriesByAlertReport QueriesByAlert
		{
			get;
			private set;
		}

		public QueriesByIsolationLevelReport QueriesByIsolationLevel
		{
			get;
			private set;
		}

		public QueriesByMethodReport QueriesByMethod
		{
			get;
			private set;
		}

		public QueriesByTableReport QueriesByTable
		{
			get;
			private set;
		}

		public QueriesByUrlReport QueriesByUrl
		{
			get;
			private set;
		}

		public UniqueQueriesReport UniqueQueries
		{
			get;
			private set;
		}

		public ReportsBridge()
		{
			this.OverallUsage = new OverallUsageReport();
			this.UniqueQueries = new UniqueQueriesReport();
			this.QueriesByMethod = new QueriesByMethodReport();
			this.QueriesByTable = new QueriesByTableReport();
			this.QueriesByAlert = new QueriesByAlertReport();
			this.QueriesByUrl = new QueriesByUrlReport();
			this.ExpensiveQueries = new ExpensiveQueriesReport();
			this.QueriesByIsolationLevel = new QueriesByIsolationLevelReport();
		}

		public void AddQuery(Statement statement)
		{
			this.UniqueQueries.AddQuery(statement);
			this.QueriesByMethod.AddQuery(statement);
			this.QueriesByTable.AddQuery(statement);
			this.QueriesByUrl.AddQuery(statement);
			this.ExpensiveQueries.AddQuery(statement);
			this.QueriesByAlert.AddQuery(statement);
		}

		public void Clear()
		{
			this.OverallUsage.Clear();
			this.UniqueQueries.Clear();
			this.QueriesByMethod.Clear();
			this.QueriesByTable.Clear();
			this.QueriesByUrl.Clear();
			this.ExpensiveQueries.Clear();
			this.QueriesByIsolationLevel.Clear();
			this.QueriesByAlert.Clear();
		}

		public void ClearExcept(string[] sessionIds)
		{
			this.OverallUsage.ClearExcept(sessionIds);
			this.UniqueQueries.ClearExcept(sessionIds);
			this.QueriesByMethod.ClearExcept(sessionIds);
			this.QueriesByTable.ClearExcept(sessionIds);
			this.QueriesByUrl.ClearExcept(sessionIds);
			this.ExpensiveQueries.ClearExcept(sessionIds);
			this.QueriesByIsolationLevel.ClearExcept(sessionIds);
			this.QueriesByAlert.ClearExcept(sessionIds);
		}

		private bool ShouldWaitForTimer()
		{
			return this.OverallUsage.PendingStatsGeneration.Count > 1;
		}

		public void WaitForReports()
		{
			DateTime utcNow = DateTime.UtcNow;
			do
			{
				Thread.Sleep(100);
			}
			while (this.ShouldWaitForTimer() && (utcNow - DateTime.UtcNow).Seconds < 4);
		}
	}
}