using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.Reports
{
	public class UrlQueriesSnapshot
	{
		public List<QueryAggregationSnapshot> Statements
		{
			get;
			set;
		}

		public string Url
		{
			get;
			set;
		}

		public UrlQueriesSnapshot()
		{
		}
	}
}