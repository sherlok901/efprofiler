using HibernatingRhinos.Profiler.BackEnd.Extensions;
using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.Reports
{
	public class QueriesByUrlReport
	{
		private readonly Dictionary<string, QueriesByUrlReport.UrlQueries> queriesByUrl = new Dictionary<string, QueriesByUrlReport.UrlQueries>();

		public QueriesByUrlReport()
		{
		}

		public void AddQuery(Statement statement)
		{
			QueriesByUrlReport.UrlQueries urlQuery;
			if (string.IsNullOrEmpty(statement.Url))
			{
				return;
			}
			if (!this.queriesByUrl.TryGetValue(statement.Url, out urlQuery))
			{
				urlQuery = new QueriesByUrlReport.UrlQueries()
				{
					Url = statement.Url
				};
				this.queriesByUrl[statement.Url] = urlQuery;
			}
			urlQuery.AddQuery(statement);
		}

		public void Clear()
		{
			this.queriesByUrl.Clear();
		}

		public void ClearExcept(string[] sessionIds)
		{
			(
				from pair in this.queriesByUrl.ToArray<KeyValuePair<string, QueriesByUrlReport.UrlQueries>>()
				where !pair.Value.ClearExcept(sessionIds)
				select pair).ForEach<KeyValuePair<string, QueriesByUrlReport.UrlQueries>>((KeyValuePair<string, QueriesByUrlReport.UrlQueries> pair) => this.queriesByUrl.Remove(pair.Key));
		}

		public List<UrlQueriesSnapshot> GetReportSnapshot()
		{
			return (
				from queryByUrl in (IEnumerable<QueriesByUrlReport.UrlQueries>)this.queriesByUrl.Values.ToArray<QueriesByUrlReport.UrlQueries>()
				select new UrlQueriesSnapshot()
				{
					Url = queryByUrl.Url,
					Statements = queryByUrl.GetStatementSnapshot()
				}).ToList<UrlQueriesSnapshot>();
		}

		private class UrlQueries
		{
			private readonly Dictionary<string, QueryAggregation> statements;

			public string Url
			{
				get;
				set;
			}

			public UrlQueries()
			{
			}

			public void AddQuery(Statement statement)
			{
				this.statements.AggregateStatement(statement);
			}

			public bool ClearExcept(string[] sessionIds)
			{
				(
					from pair in this.statements.ToArray<KeyValuePair<string, QueryAggregation>>()
					where !pair.Value.ClearExcept(sessionIds)
					select pair).ForEach<KeyValuePair<string, QueryAggregation>>((KeyValuePair<string, QueryAggregation> pair) => this.statements.Remove(pair.Key));
				return this.statements.Count > 0;
			}

			public List<QueryAggregationSnapshot> GetStatementSnapshot()
			{
				return (
					from agg in this.statements.Values
					select new QueryAggregationSnapshot()
					{
						RawSql = agg.RawSql,
						ShortSql = agg.ShortSql,
						FormattedSql = agg.FormattedSql,
						AverageDuration = agg.AverageDuration,
						Count = agg.Count,
						RelatedSessions = agg.RelatedSessions.ToArray<RelatedSessionSpecification>()
					}).ToList<QueryAggregationSnapshot>();
			}
		}
	}
}