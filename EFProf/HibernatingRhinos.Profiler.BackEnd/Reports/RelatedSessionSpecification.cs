using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.Reports
{
	public class RelatedSessionSpecification
	{
		public Guid SessionId
		{
			get;
			set;
		}

		public Guid StatementId
		{
			get;
			set;
		}

		public RelatedSessionSpecification()
		{
		}

		public bool Equals(RelatedSessionSpecification other)
		{
			if (object.ReferenceEquals(null, other))
			{
				return false;
			}
			if (object.ReferenceEquals(this, other))
			{
				return true;
			}
			if (!other.SessionId.Equals(this.SessionId))
			{
				return false;
			}
			return other.StatementId.Equals(this.StatementId);
		}

		public override bool Equals(object obj)
		{
			if (object.ReferenceEquals(null, obj))
			{
				return false;
			}
			if (object.ReferenceEquals(this, obj))
			{
				return true;
			}
			if (!(obj is RelatedSessionSpecification))
			{
				return false;
			}
			return this.Equals((RelatedSessionSpecification)obj);
		}

		public override int GetHashCode()
		{
			Guid sessionId = this.SessionId;
			Guid statementId = this.StatementId;
			return sessionId.GetHashCode() * 397 ^ statementId.GetHashCode();
		}

		public class RelatedSessionsBySessionIdComparer : IEqualityComparer<RelatedSessionSpecification>
		{
			public RelatedSessionsBySessionIdComparer()
			{
			}

			public bool Equals(RelatedSessionSpecification x, RelatedSessionSpecification y)
			{
				return x.SessionId.Equals(y.SessionId);
			}

			public int GetHashCode(RelatedSessionSpecification obj)
			{
				return obj.SessionId.GetHashCode() * 397;
			}
		}
	}
}