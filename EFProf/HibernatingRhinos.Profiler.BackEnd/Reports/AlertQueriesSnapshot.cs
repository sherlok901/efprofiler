using HibernatingRhinos.Profiler.BackEnd;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.Reports
{
	public class AlertQueriesSnapshot
	{
		public AlertInformation Alert
		{
			get;
			set;
		}

		public List<QueryAggregationSnapshot> Statements
		{
			get;
			set;
		}

		public AlertQueriesSnapshot()
		{
		}
	}
}