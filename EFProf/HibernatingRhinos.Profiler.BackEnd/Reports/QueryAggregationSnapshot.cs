using HibernatingRhinos.Profiler.BackEnd.Messages;
using System;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.Reports
{
	public class QueryAggregationSnapshot
	{
		public double? AverageDuration
		{
			get;
			set;
		}

		public int Count
		{
			get;
			set;
		}

		public string FormattedSql
		{
			get;
			set;
		}

		public SqlStatementOptions Options
		{
			get;
			set;
		}

		public string RawSql
		{
			get;
			set;
		}

		public RelatedSessionSpecification[] RelatedSessions
		{
			get;
			set;
		}

		public string ShortSql
		{
			get;
			set;
		}

		public QueryAggregationSnapshot()
		{
		}
	}
}