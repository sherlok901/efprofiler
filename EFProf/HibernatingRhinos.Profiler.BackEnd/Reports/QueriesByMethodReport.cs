using HibernatingRhinos.Profiler.Appender.StackTraces;
using HibernatingRhinos.Profiler.BackEnd.Extensions;
using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.Reports
{
	public class QueriesByMethodReport
	{
		private readonly Dictionary<string, QueriesByMethodReport.MethodQueries> queriesByMethodFullName = new Dictionary<string, QueriesByMethodReport.MethodQueries>();

		public QueriesByMethodReport()
		{
		}

		public void AddQuery(Statement statement)
		{
			QueriesByMethodReport.MethodQueries methodQuery;
			if (statement.StackTrace == null)
			{
				return;
			}
			StackTraceFrame[] frames = statement.StackTrace.Frames;
			for (int i = 0; i < (int)frames.Length; i++)
			{
				StackTraceFrame stackTraceFrame = frames[i];
				string str = string.Concat(stackTraceFrame.Type, ".", stackTraceFrame.Method);
				if (!this.queriesByMethodFullName.TryGetValue(str, out methodQuery))
				{
					methodQuery = new QueriesByMethodReport.MethodQueries(stackTraceFrame);
					this.queriesByMethodFullName[str] = methodQuery;
				}
				methodQuery.AddQuery(statement);
			}
		}

		public void Clear()
		{
			this.queriesByMethodFullName.Clear();
		}

		public void ClearExcept(string[] sessionIds)
		{
			(
				from pair in this.queriesByMethodFullName.ToArray<KeyValuePair<string, QueriesByMethodReport.MethodQueries>>()
				where !pair.Value.ClearExcept(sessionIds)
				select pair).ForEach<KeyValuePair<string, QueriesByMethodReport.MethodQueries>>((KeyValuePair<string, QueriesByMethodReport.MethodQueries> pair) => this.queriesByMethodFullName.Remove(pair.Key));
		}

		public List<MethodQueriesSnapshot> GetReportSnapshot()
		{
			return (
				from queryByMethod in (IEnumerable<QueriesByMethodReport.MethodQueries>)this.queriesByMethodFullName.Values.ToArray<QueriesByMethodReport.MethodQueries>()
				select new MethodQueriesSnapshot()
				{
					FullName = queryByMethod.FullName,
					Name = queryByMethod.Name,
					TypeName = queryByMethod.TypeName,
					Statements = queryByMethod.GetStatementSnapshot()
				}).ToList<MethodQueriesSnapshot>();
		}

		private class MethodQueries
		{
			private readonly Dictionary<string, QueryAggregation> statements;

			public string FullName
			{
				get;
				private set;
			}

			public string Name
			{
				get;
				private set;
			}

			public string TypeName
			{
				get;
				private set;
			}

			public MethodQueries(StackTraceFrame frame)
			{
				this.Name = frame.Method;
				this.TypeName = frame.Type.Substring(frame.Type.LastIndexOf('.') + 1);
				this.FullName = string.Concat(frame.Type, ".", frame.Method);
			}

			public void AddQuery(Statement statement)
			{
				this.statements.AggregateStatement(statement);
			}

			public bool ClearExcept(string[] sessionIds)
			{
				foreach (KeyValuePair<string, QueryAggregation> keyValuePair in 
					from pair in this.statements.ToArray<KeyValuePair<string, QueryAggregation>>()
					where !pair.Value.ClearExcept(sessionIds)
					select pair)
				{
					this.statements.Remove(keyValuePair.Key);
				}
				return this.statements.Count > 0;
			}

			public List<QueryAggregationSnapshot> GetStatementSnapshot()
			{
				return (
					from agg in this.statements.Values
					select new QueryAggregationSnapshot()
					{
						RawSql = agg.RawSql,
						ShortSql = agg.ShortSql,
						FormattedSql = agg.FormattedSql,
						AverageDuration = agg.AverageDuration,
						Count = agg.Count,
						Options = agg.Options,
						RelatedSessions = agg.RelatedSessions.ToArray<RelatedSessionSpecification>()
					}).ToList<QueryAggregationSnapshot>();
			}
		}
	}
}