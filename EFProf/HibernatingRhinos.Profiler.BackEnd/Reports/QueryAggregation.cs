using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.Messages;
using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.Reports
{
	public class QueryAggregation
	{
		private ConcurrentQueue<QueryAggregation.Duration> durations = new ConcurrentQueue<QueryAggregation.Duration>();

		public double? AverageDuration
		{
			get
			{
				double? nullable = this.durations.Average<QueryAggregation.Duration>((QueryAggregation.Duration x) => x.StatementDuration);
				if (!nullable.HasValue)
				{
					return null;
				}
				return new double?(Math.Round(nullable.Value, 2));
			}
		}

		public int Count
		{
			get;
			private set;
		}

		public string FormattedSql
		{
			get;
			set;
		}

		public SqlStatementOptions Options
		{
			get;
			set;
		}

		public string RawSql
		{
			get;
			set;
		}

		public HashSet<RelatedSessionSpecification> RelatedSessions
		{
			get;
			set;
		}

		public string ShortSql
		{
			get;
			set;
		}

		public QueryAggregation(Statement statement)
		{
			this.FormattedSql = statement.FormattedSql;
			this.ShortSql = statement.ShortSql;
			this.RawSql = statement.RawSql;
			this.Options = statement.Options;
			this.RelatedSessions = new HashSet<RelatedSessionSpecification>(new RelatedSessionSpecification.RelatedSessionsBySessionIdComparer());
		}

		public void AddQuery(Statement statement)
		{
			QueryAggregation count = this;
			count.Count = count.Count + 1;
			QueryAggregation.Duration duration = new QueryAggregation.Duration(statement);
			statement.ValuesRefreshed += new Action<Statement>(duration.Update);
			duration.Update(statement);
			this.durations.Enqueue(duration);
			this.Options = statement.Options;
			if (statement.InternalSessionId != Guid.Empty)
			{
				HashSet<RelatedSessionSpecification> relatedSessions = this.RelatedSessions;
				RelatedSessionSpecification relatedSessionSpecification = new RelatedSessionSpecification()
				{
					SessionId = statement.InternalSessionId,
					StatementId = statement.InternalId
				};
				relatedSessions.Add(relatedSessionSpecification);
			}
		}

		public bool ClearExcept(string[] sessionIds)
		{
			foreach (QueryAggregation.Duration duration in this.durations)
			{
				if (sessionIds.Contains<string>(duration.SessionId))
				{
					continue;
				}
				List<QueryAggregation.Duration> list = this.durations.ToList<QueryAggregation.Duration>();
				list.Remove(duration);
				this.durations = new ConcurrentQueue<QueryAggregation.Duration>(list);
			}
			return this.durations.Count > 0;
		}

		private class Duration
		{
			public string SessionId;

			public int? StatementDuration
			{
				get;
				private set;
			}

			public Duration(Statement statement)
			{
				this.Update(statement);
			}

			public void Update(Statement statement)
			{
				int? value;
				this.SessionId = statement.SessionId.SessionId;
				if (statement.Duration == null)
				{
					value = null;
				}
				else
				{
					value = statement.Duration.Value;
				}
				this.StatementDuration = value;
			}
		}
	}
}