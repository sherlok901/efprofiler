using HibernatingRhinos.Profiler.BackEnd.Messages;
using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd
{
	public abstract class AbstractSessionProcessor : AbstractItemProcessor
	{
		public ReactiveModelBuilder ModelBuilder
		{
			get;
			set;
		}

		protected AbstractSessionProcessor()
		{
		}

		protected Session[] GetSessionsOnSameThread(Session session)
		{
			return (
				from pair in this.ModelBuilder.Sessions
				where pair.Key.ThreadId == session.Id.ThreadId
				select pair.Value).ToArray<Session>();
		}

		public abstract void OnSessionClosed(Session session);
	}
}