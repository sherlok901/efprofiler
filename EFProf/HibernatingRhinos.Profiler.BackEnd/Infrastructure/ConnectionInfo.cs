using System;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.Infrastructure
{
	[Serializable]
	public class ConnectionInfo
	{
		public string ConnectionString
		{
			get;
			set;
		}

		public string ConnectionTypeName
		{
			get;
			set;
		}

		public string FullPathToAssembly
		{
			get;
			set;
		}

		public bool IsSelected
		{
			get;
			set;
		}

		public ConnectionInfo()
		{
		}
	}
}