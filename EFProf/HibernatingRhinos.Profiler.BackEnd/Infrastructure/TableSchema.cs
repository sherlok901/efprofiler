using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.Infrastructure
{
	public class TableSchema
	{
		public Dictionary<string, SqlType> Columns = new Dictionary<string, SqlType>();

		public string Name
		{
			get;
			set;
		}

		public TableSchema()
		{
		}
	}
}