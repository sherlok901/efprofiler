using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace HibernatingRhinos.Profiler.BackEnd.Infrastructure
{
	public class SerializationEngine
	{
		public SerializationEngine()
		{
		}

		public T Deserialize<T>(Stream stream, LocationOnFile locationOnFile)
		{
			T t;
			long position = stream.Position;
			try
			{
				stream.Position = locationOnFile.Position;
				T t1 = SerializationEngine.DeserializeBinary<T>(stream);
				locationOnFile.WeakSessionReference = new WeakReference((object)t1);
				t = t1;
			}
			finally
			{
				stream.Position = position;
			}
			return t;
		}

		private static T DeserializeBinary<T>(Stream stream)
		{
			return (T)(new BinaryFormatter()).Deserialize(stream);
		}

		public void Serialize(Stream stream, LocationOnFile locationOnFile, byte[] bytes)
		{
			if (locationOnFile.Position != (long)-1)
			{
				stream.Position = locationOnFile.Position;
			}
			else
			{
				locationOnFile.Position = stream.Position;
			}
			stream.Write(bytes, 0, (int)bytes.Length);
			stream.Position = locationOnFile.Position + (long)locationOnFile.Available;
		}
	}
}