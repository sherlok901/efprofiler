using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using System;

namespace HibernatingRhinos.Profiler.BackEnd.Infrastructure
{
	public class LocationOnFile
	{
		private const int SectorSize = 65536;

		public Guid SessionId;

		public long Position;

		public int Used;

		public int Available;

		public WeakReference WeakSessionReference;

		public LocationOnFile(int size)
		{
			this.Position = (long)-1;
			this.Available = (size / 65536 + (size % 65536 == 0 ? 0 : 1)) * 65536;
		}

		public void UpdateLocation(int usedBytes, Session session)
		{
			this.Used = usedBytes;
			this.SessionId = session.InternalId;
			this.WeakSessionReference = new WeakReference(session);
		}
	}
}