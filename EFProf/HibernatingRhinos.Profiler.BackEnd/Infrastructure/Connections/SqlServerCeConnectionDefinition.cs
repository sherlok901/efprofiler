using System;

namespace HibernatingRhinos.Profiler.BackEnd.Infrastructure.Connections
{
	public class SqlServerCeConnectionDefinition : IExternalConnectionDefinition, IConnectionDefinition
	{
		public string AssemblyName
		{
			get
			{
				return "System.Data.SqlServerCe";
			}
		}

		public string ClassName
		{
			get
			{
				return "System.Data.SqlServerCe.SqlCeConnection";
			}
		}

		public string DisplayName
		{
			get
			{
				return "Sql Server CE";
			}
		}

		public SqlServerCeConnectionDefinition()
		{
		}
	}
}