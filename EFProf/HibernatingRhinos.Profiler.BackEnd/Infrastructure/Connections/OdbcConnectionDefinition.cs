using System;

namespace HibernatingRhinos.Profiler.BackEnd.Infrastructure.Connections
{
	public class OdbcConnectionDefinition : IConnectionDefinition
	{
		public string ClassName
		{
			get
			{
				return "System.Data.Odbc.OdbcConnection";
			}
		}

		public string DisplayName
		{
			get
			{
				return "Odbc";
			}
		}

		public OdbcConnectionDefinition()
		{
		}
	}
}