using System;

namespace HibernatingRhinos.Profiler.BackEnd.Infrastructure.Connections
{
	public interface IConnectionDefinition
	{
		string ClassName
		{
			get;
		}

		string DisplayName
		{
			get;
		}
	}
}