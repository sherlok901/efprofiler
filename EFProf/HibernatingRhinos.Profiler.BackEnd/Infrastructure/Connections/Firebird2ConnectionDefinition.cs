using System;

namespace HibernatingRhinos.Profiler.BackEnd.Infrastructure.Connections
{
	public class Firebird2ConnectionDefinition : IExternalConnectionDefinition, IConnectionDefinition
	{
		public string AssemblyName
		{
			get
			{
				return "FirebirdSql.Data.Firebird";
			}
		}

		public string ClassName
		{
			get
			{
				return "FirebirdSql.Data.FirebirdClient.FbConnection";
			}
		}

		public string DisplayName
		{
			get
			{
				return "Firebird v2.0";
			}
		}

		public Firebird2ConnectionDefinition()
		{
		}
	}
}