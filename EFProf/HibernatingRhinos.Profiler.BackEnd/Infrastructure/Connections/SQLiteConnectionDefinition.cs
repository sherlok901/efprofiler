using System;

namespace HibernatingRhinos.Profiler.BackEnd.Infrastructure.Connections
{
	public class SQLiteConnectionDefinition : IExternalConnectionDefinition, IConnectionDefinition
	{
		public string AssemblyName
		{
			get
			{
				return "System.Data.SQLite";
			}
		}

		public string ClassName
		{
			get
			{
				return "System.Data.SQLite.SQLiteConnection";
			}
		}

		public string DisplayName
		{
			get
			{
				return "SQLite";
			}
		}

		public SQLiteConnectionDefinition()
		{
		}
	}
}