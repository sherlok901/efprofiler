using System;

namespace HibernatingRhinos.Profiler.BackEnd.Infrastructure.Connections
{
	public class ManagedOracleConnectionDefinition : IExternalConnectionDefinition, IConnectionDefinition
	{
		public string AssemblyName
		{
			get
			{
				return "Oracle.ManagedDataAccess";
			}
		}

		public string ClassName
		{
			get
			{
				return "Oracle.ManagedDataAccess.Client.OracleConnection";
			}
		}

		public string DisplayName
		{
			get
			{
				return "ManagedOracle";
			}
		}

		public ManagedOracleConnectionDefinition()
		{
		}
	}
}