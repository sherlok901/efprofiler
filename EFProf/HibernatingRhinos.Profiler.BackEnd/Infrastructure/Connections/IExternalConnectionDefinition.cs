using System;

namespace HibernatingRhinos.Profiler.BackEnd.Infrastructure.Connections
{
	public interface IExternalConnectionDefinition : IConnectionDefinition
	{
		string AssemblyName
		{
			get;
		}
	}
}