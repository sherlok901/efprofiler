using System;

namespace HibernatingRhinos.Profiler.BackEnd.Infrastructure.Connections
{
	public class Firebird25ConnectionDefinition : IExternalConnectionDefinition, IConnectionDefinition
	{
		public string AssemblyName
		{
			get
			{
				return "FirebirdSql.Data.FirebirdClient";
			}
		}

		public string ClassName
		{
			get
			{
				return "FirebirdSql.Data.FirebirdClient.FbConnection";
			}
		}

		public string DisplayName
		{
			get
			{
				return "Firebird v2.5 and higher";
			}
		}

		public Firebird25ConnectionDefinition()
		{
		}
	}
}