using System;

namespace HibernatingRhinos.Profiler.BackEnd.Infrastructure.Connections
{
	public class IbmDb2ConnectionDefinition : IExternalConnectionDefinition, IConnectionDefinition
	{
		public string AssemblyName
		{
			get
			{
				return "IBM.Data.DB2";
			}
		}

		public string ClassName
		{
			get
			{
				return "IBM.Data.DB2.DB2Connection";
			}
		}

		public string DisplayName
		{
			get
			{
				return "IBM DB2";
			}
		}

		public IbmDb2ConnectionDefinition()
		{
		}
	}
}