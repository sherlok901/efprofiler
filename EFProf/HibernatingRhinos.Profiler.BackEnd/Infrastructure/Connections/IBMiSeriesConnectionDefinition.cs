using System;

namespace HibernatingRhinos.Profiler.BackEnd.Infrastructure.Connections
{
	public class IBMiSeriesConnectionDefinition : IExternalConnectionDefinition, IConnectionDefinition
	{
		public string AssemblyName
		{
			get
			{
				return "IBM.Data.DB2.iSeries";
			}
		}

		public string ClassName
		{
			get
			{
				return "IBM.Data.DB2.iSeries.iDB2Connection";
			}
		}

		public string DisplayName
		{
			get
			{
				return "IBM. DB2 iSeries";
			}
		}

		public IBMiSeriesConnectionDefinition()
		{
		}
	}
}