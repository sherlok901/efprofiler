using System;

namespace HibernatingRhinos.Profiler.BackEnd.Infrastructure.Connections
{
	public class OracleConnectionDefinition : IExternalConnectionDefinition, IConnectionDefinition
	{
		public string AssemblyName
		{
			get
			{
				return "Oracle.DataAccess";
			}
		}

		public string ClassName
		{
			get
			{
				return "Oracle.DataAccess.Client.OracleConnection";
			}
		}

		public string DisplayName
		{
			get
			{
				return "Oracle";
			}
		}

		public OracleConnectionDefinition()
		{
		}
	}
}