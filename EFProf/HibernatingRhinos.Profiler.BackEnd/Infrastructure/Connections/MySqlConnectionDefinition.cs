using System;

namespace HibernatingRhinos.Profiler.BackEnd.Infrastructure.Connections
{
	public class MySqlConnectionDefinition : IExternalConnectionDefinition, IConnectionDefinition
	{
		public string AssemblyName
		{
			get
			{
				return "MySql.Data";
			}
		}

		public string ClassName
		{
			get
			{
				return "MySql.Data.MySqlClient.MySqlConnection";
			}
		}

		public string DisplayName
		{
			get
			{
				return "MySql";
			}
		}

		public MySqlConnectionDefinition()
		{
		}
	}
}