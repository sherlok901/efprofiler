using System;

namespace HibernatingRhinos.Profiler.BackEnd.Infrastructure.Connections
{
	public class OleDbConnectionDefinition : IConnectionDefinition
	{
		public string ClassName
		{
			get
			{
				return "System.Data.OleDb.OleDbConnection";
			}
		}

		public string DisplayName
		{
			get
			{
				return "OleDb";
			}
		}

		public OleDbConnectionDefinition()
		{
		}
	}
}