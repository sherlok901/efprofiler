using System;

namespace HibernatingRhinos.Profiler.BackEnd.Infrastructure.Connections
{
	public class SqlServerConnectionDefinition : IConnectionDefinition
	{
		public string ClassName
		{
			get
			{
				return "System.Data.SqlClient.SqlConnection, System.Data, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089";
			}
		}

		public string DisplayName
		{
			get
			{
				return "Sql Server";
			}
		}

		public SqlServerConnectionDefinition()
		{
		}
	}
}