using System;

namespace HibernatingRhinos.Profiler.BackEnd.Infrastructure.Connections
{
	public class Firebird1ConnectionDefinition : IExternalConnectionDefinition, IConnectionDefinition
	{
		public string AssemblyName
		{
			get
			{
				return "FirebirdSql.Data.Firebird";
			}
		}

		public string ClassName
		{
			get
			{
				return "FirebirdSql.Data.Firebird.FbConnection";
			}
		}

		public string DisplayName
		{
			get
			{
				return "Firebird v1.0";
			}
		}

		public Firebird1ConnectionDefinition()
		{
		}
	}
}