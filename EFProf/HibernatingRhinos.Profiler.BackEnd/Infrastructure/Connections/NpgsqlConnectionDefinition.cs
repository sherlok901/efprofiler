using System;

namespace HibernatingRhinos.Profiler.BackEnd.Infrastructure.Connections
{
	public class NpgsqlConnectionDefinition : IExternalConnectionDefinition, IConnectionDefinition
	{
		public string AssemblyName
		{
			get
			{
				return "Npgsql";
			}
		}

		public string ClassName
		{
			get
			{
				return "Npgsql.NpgsqlConnection";
			}
		}

		public string DisplayName
		{
			get
			{
				return "Npgsql";
			}
		}

		public NpgsqlConnectionDefinition()
		{
		}
	}
}