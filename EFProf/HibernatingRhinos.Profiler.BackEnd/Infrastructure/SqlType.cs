using System;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.Infrastructure
{
	public class SqlType
	{
		public string ConstraintType
		{
			get;
			set;
		}

		public int Length
		{
			get;
			set;
		}

		public string Type
		{
			get;
			set;
		}

		public SqlType()
		{
		}
	}
}