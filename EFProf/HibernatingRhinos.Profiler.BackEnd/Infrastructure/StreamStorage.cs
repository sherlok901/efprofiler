using HibernatingRhinos.Profiler.BackEnd.Messages;
using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization.Formatters.Binary;

namespace HibernatingRhinos.Profiler.BackEnd.Infrastructure
{
	public class StreamStorage : IDisposable
	{
		private readonly LinkedList<LocationOnFile> freeList = new LinkedList<LocationOnFile>();

		private readonly IDictionary<Guid, LocationOnFile> sessionByIdToFilePointer = new Dictionary<Guid, LocationOnFile>();

		private readonly IDictionary<string, LocationOnFile> sessionBySessionIdToFilePointer = new Dictionary<string, LocationOnFile>();

		private readonly Stream stream;

		private readonly SerializationEngine serializationEngine = new SerializationEngine();

		public StreamStorage(Stream stream)
		{
			this.stream = stream;
			if (!this.stream.CanSeek)
			{
				throw new ArgumentException("Cannot use a non seekable stream", "stream");
			}
			if (!this.stream.CanWrite)
			{
				throw new ArgumentException("Cannot use a non writable stream", "stream");
			}
		}

		public bool ContainsSession(Session session)
		{
			return this.sessionByIdToFilePointer.ContainsKey(session.InternalId);
		}

		public void Dispose()
		{
			this.stream.Dispose();
		}

		private LocationOnFile GetLocationOnFileAndUpdateFreeList(Guid id, int size)
		{
			LocationOnFile locationOnFile;
			if (this.sessionByIdToFilePointer.TryGetValue(id, out locationOnFile))
			{
				if (locationOnFile.Available > size)
				{
					return locationOnFile;
				}
				this.SendToFreeList(locationOnFile);
			}
			LocationOnFile locationOnFile1 = this.freeList.FirstOrDefault<LocationOnFile>((LocationOnFile x) => x.Available > size);
			if (locationOnFile1 == null)
			{
				return new LocationOnFile(size);
			}
			this.freeList.Remove(locationOnFile1);
			return locationOnFile1;
		}

		public void Remove(Guid idToRemove, SessionIdHolder sessionIdToRemove)
		{
			this.sessionByIdToFilePointer.Remove(idToRemove);
			this.sessionBySessionIdToFilePointer.Remove(sessionIdToRemove.SessionId);
		}

		[MethodImpl(MethodImplOptions.Synchronized)]
		public Session Retrieve(Guid sessionId)
		{
			LocationOnFile locationOnFile;
			if (!this.sessionByIdToFilePointer.TryGetValue(sessionId, out locationOnFile))
			{
				return null;
			}
			object target = locationOnFile.WeakSessionReference.Target;
			if (target != null)
			{
				return (Session)target;
			}
			return this.serializationEngine.Deserialize<Session>(this.stream, locationOnFile);
		}

		[MethodImpl(MethodImplOptions.Synchronized)]
		public Session Retrieve(SessionIdHolder sessionId)
		{
			LocationOnFile locationOnFile;
			if (!this.sessionBySessionIdToFilePointer.TryGetValue(sessionId.SessionId, out locationOnFile))
			{
				return null;
			}
			object target = locationOnFile.WeakSessionReference.Target;
			if (target != null)
			{
				return (Session)target;
			}
			return this.serializationEngine.Deserialize<Session>(this.stream, locationOnFile);
		}

		private void SendToFreeList(LocationOnFile value)
		{
			this.sessionByIdToFilePointer.Remove(value.SessionId);
			value.SessionId = Guid.Empty;
			value.Used = 0;
			this.freeList.AddLast(value);
		}

		private static byte[] SerializeBinary(Session session)
		{
			byte[] array;
			using (MemoryStream memoryStream = new MemoryStream())
			{
				(new BinaryFormatter()).Serialize(memoryStream, session);
				array = memoryStream.ToArray();
			}
			return array;
		}

		[MethodImpl(MethodImplOptions.Synchronized)]
		public void Store(Session session)
		{
			if (this.sessionByIdToFilePointer.ContainsKey(session.InternalId))
			{
				this.sessionByIdToFilePointer.Remove(session.InternalId);
				this.sessionBySessionIdToFilePointer.Remove(session.Id.SessionId);
			}
			byte[] numArray = StreamStorage.SerializeBinary(session);
			LocationOnFile locationOnFileAndUpdateFreeList = this.GetLocationOnFileAndUpdateFreeList(session.InternalId, (int)numArray.Length);
			locationOnFileAndUpdateFreeList.UpdateLocation((int)numArray.Length, session);
			this.sessionByIdToFilePointer[session.InternalId] = locationOnFileAndUpdateFreeList;
			this.sessionBySessionIdToFilePointer[session.Id.SessionId] = locationOnFileAndUpdateFreeList;
			this.serializationEngine.Serialize(this.stream, locationOnFileAndUpdateFreeList, numArray);
		}

		public void Update(Session session)
		{
			this.sessionByIdToFilePointer.Remove(session.InternalId);
			this.sessionBySessionIdToFilePointer.Remove(session.Id.SessionId);
			this.Store(session);
		}
	}
}