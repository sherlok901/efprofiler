using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;

namespace HibernatingRhinos.Profiler.BackEnd.Infrastructure
{
	public class CrudeSqlFormatter
	{
		private readonly static Regex tokenizer;

		private readonly static string[] keywords_requiring_new_line;

		private readonly static string[] keywords_requiring_new_line_and_untab;

		private readonly static string[] symbols;

		private int tab;

		private bool changedTabbing;

		private readonly StringBuilder builder;

		static CrudeSqlFormatter()
		{
			string[] strArrays = new string[] { "select", "update", "insert\\s+into", "delete", "and", "or" };
			CrudeSqlFormatter.keywords_requiring_new_line = strArrays;
			string[] strArrays1 = new string[] { "where", "group by", "having", "order by", "from" };
			CrudeSqlFormatter.keywords_requiring_new_line_and_untab = strArrays1;
			string[] strArrays2 = new string[] { "=", ",", "+", "-", "*", "/", "(", ")", ";" };
			CrudeSqlFormatter.symbols = strArrays2;
			StringBuilder stringBuilder = new StringBuilder();
			foreach (string str in CrudeSqlFormatter.keywords_requiring_new_line.Concat<string>(CrudeSqlFormatter.keywords_requiring_new_line_and_untab))
			{
				stringBuilder.Append("(?<!_|-|\\w|\\d)(").Append(str).Append(")(\\s)|").AppendLine();
			}
			string[] strArrays3 = CrudeSqlFormatter.symbols;
			for (int i = 0; i < (int)strArrays3.Length; i++)
			{
				string str1 = strArrays3[i];
				stringBuilder.Append("(").Append(Regex.Escape(str1)).Append(")|").AppendLine();
			}
			stringBuilder.Append("(\\s+)");
			CrudeSqlFormatter.tokenizer = new Regex(stringBuilder.ToString(), RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.IgnorePatternWhitespace);
		}

		public CrudeSqlFormatter()
		{
			this.builder = new StringBuilder();
		}

		private void AppendLine()
		{
			this.builder.AppendLine().Append(new string('\t', this.tab));
		}

		private void DecrementIfPreviouslyIncreasedTab()
		{
			if (!this.changedTabbing)
			{
				return;
			}
			this.changedTabbing = false;
			this.DecrementTab();
		}

		private void DecrementTab()
		{
			this.tab--;
			if (this.tab < 0)
			{
				this.tab = 0;
			}
		}

		public string Format(string sql)
		{
			string[] strArrays = CrudeSqlFormatter.tokenizer.Split(sql);
			for (int i = 0; i < (int)strArrays.Length; i++)
			{
				string str = strArrays[i];
				if (!string.IsNullOrEmpty(str))
				{
					if (CrudeSqlFormatter.keywords_requiring_new_line_and_untab.Any<string>((string x) => x.Equals(str, StringComparison.InvariantCultureIgnoreCase)))
					{
						this.DecrementIfPreviouslyIncreasedTab();
						this.AppendLine();
					}
					this.builder.Append(str);
					if (!CrudeSqlFormatter.keywords_requiring_new_line.Any<string>((string x) => x.Equals(str, StringComparison.InvariantCultureIgnoreCase)))
					{
						string str1 = str;
						string str2 = str1;
						if (str1 != null)
						{
							if (str2 == "(")
							{
								this.Increment();
								this.AppendLine();
							}
							else if (str2 == ")")
							{
								this.DecrementTab();
								this.AppendLine();
							}
							else if (str2 == ",")
							{
								this.AppendLine();
							}
							else if (str2 == ";")
							{
								this.DecrementTab();
							}
						}
					}
					else
					{
						this.Increment();
						this.AppendLine();
					}
				}
			}
			return this.builder.ToString();
		}

		private void Increment()
		{
			this.tab++;
			this.changedTabbing = true;
		}
	}
}