using log4net;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace HibernatingRhinos.Profiler.BackEnd.Infrastructure
{
	public class SchemaCache
	{
		private readonly static Regex TableNameToOneWord;

		private readonly static ILog Log;

		private ConcurrentDictionary<Tuple<string, ConnectionInfo>, Task<TableSchema>> _cache = new ConcurrentDictionary<Tuple<string, ConnectionInfo>, Task<TableSchema>>();

		private readonly string sqlFormat = "select con.CONSTRAINT_TYPE, usage.CONSTRAINT_NAME ,col.* from INFORMATION_SCHEMA.COLUMNS col left join INFORMATION_SCHEMA.KEY_COLUMN_USAGE usage on usage.COLUMN_NAME=col.COLUMN_NAME and usage.TABLE_NAME=col.TABLE_NAME left join INFORMATION_SCHEMA.TABLE_CONSTRAINTS con on usage.CONSTRAINT_NAME=con.CONSTRAINT_NAME WHERE col.TABLE_NAME = '{0}'";

		static SchemaCache()
		{
			SchemaCache.TableNameToOneWord = new Regex("\\S+\\.\\[?([A-Za-z0-9_]+)\\]?", RegexOptions.Compiled);
			SchemaCache.Log = LogManager.GetLogger(typeof(SchemaCache));
		}

		public SchemaCache()
		{
		}

		public TableSchema GetSchema(string tableName, ConnectionInfo connection)
		{
			Task<TableSchema> task;
			string value = tableName;
			Match match = SchemaCache.TableNameToOneWord.Match(value);
			if (match.Success)
			{
				value = match.Groups[1].Value;
			}
			Tuple<string, ConnectionInfo> tuple = Tuple.Create<string, ConnectionInfo>(value, connection);
			Func<TableSchema> func = () => this.TaskHelper(value, connection);
			if (!this._cache.TryGetValue(tuple, out task))
			{
				task = new Task<TableSchema>(func);
				if (this._cache.TryAdd(tuple, task))
				{
					task.Start();
				}
			}
			if (!task.IsCompleted)
			{
				return null;
			}
			return task.Result;
		}

		private static Type GetTypeForConnection(ConnectionInfo connectionInfo)
		{
			if (string.IsNullOrEmpty(connectionInfo.FullPathToAssembly))
			{
				return Type.GetType(connectionInfo.ConnectionTypeName);
			}
			Assembly assembly = Assembly.LoadFrom(connectionInfo.FullPathToAssembly);
			return assembly.GetType(connectionInfo.ConnectionTypeName);
		}

		private void GotError(string message, Exception e)
		{
			SchemaCache.Log.Error(message, e);
		}

		private TableSchema TaskHelper(string tableName, ConnectionInfo connection)
		{
			IDataReader dataReader;
			TableSchema tableSchema;
			IDbConnection connectionString = null;
			if (string.IsNullOrEmpty(connection.ConnectionString))
			{
				this.GotError("", null);
				return null;
			}
			if (string.IsNullOrEmpty(connection.ConnectionTypeName))
			{
				this.GotError("Can not execute query when the connection type is not specified.", null);
				return null;
			}
			Type typeForConnection = null;
			try
			{
				typeForConnection = SchemaCache.GetTypeForConnection(connection);
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				this.GotError(string.Format("Could not load connection type: {0}. {1}", connection.ConnectionTypeName, exception), null);
			}
			if (typeForConnection == null)
			{
				this.GotError(string.Concat("Could not find connection type: ", connection.ConnectionTypeName), null);
				return null;
			}
			using (connectionString)
			{
				try
				{
					connectionString = (IDbConnection)Activator.CreateInstance(typeForConnection);
					Assembly assembly = connectionString.GetType().Assembly;
					ResolveEventHandler name = (object sender, ResolveEventArgs args) => {
						if (args.Name != assembly.FullName)
						{
							return null;
						}
						return assembly;
					};
					AppDomain.CurrentDomain.AssemblyResolve += name;
					try
					{
						try
						{
							connectionString.ConnectionString = connection.ConnectionString;
							connectionString.Open();
						}
						catch (Exception exception2)
						{
							this.GotError("Could not open connection", exception2);
							tableSchema = null;
							return tableSchema;
						}
					}
					finally
					{
						AppDomain.CurrentDomain.AssemblyResolve -= name;
					}
					string str = string.Format(this.sqlFormat, tableName);
					using (IDbCommand dbCommand = connectionString.CreateCommand())
					{
						dbCommand.CommandText = str;
						try
						{
							dataReader = dbCommand.ExecuteReader();
						}
						catch (Exception exception3)
						{
							this.GotError(string.Concat("Failed to execute query: ", str), exception3);
							tableSchema = null;
							return tableSchema;
						}
						try
						{
							try
							{
								TableSchema tableSchema1 = new TableSchema()
								{
									Name = tableName
								};
								while (dataReader.Read())
								{
									int num = 0;
									object item = dataReader["CHARACTER_MAXIMUM_LENGTH"];
									if (item != null)
									{
										int.TryParse(item.ToString(), out num);
									}
									SqlType sqlType = new SqlType()
									{
										Type = dataReader["DATA_TYPE"].ToString(),
										Length = num,
										ConstraintType = (dataReader["CONSTRAINT_TYPE"] == null ? "" : dataReader["CONSTRAINT_TYPE"].ToString())
									};
									SqlType sqlType1 = sqlType;
									tableSchema1.Columns[dataReader["COLUMN_NAME"].ToString()] = sqlType1;
								}
								tableSchema = tableSchema1;
							}
							catch (Exception exception4)
							{
								this.GotError("Error reading TableSchema: ", exception4);
								tableSchema = null;
							}
						}
						finally
						{
							dataReader.Dispose();
						}
					}
				}
				catch (Exception exception6)
				{
					Exception exception5 = exception6;
					this.GotError(string.Concat("Could not create an instance of ", connection.ConnectionTypeName), exception5);
					tableSchema = null;
				}
			}
			return tableSchema;
		}
	}
}