using System;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.Infrastructure
{
	public class TableNamesAndScema
	{
		public string Alias
		{
			get;
			set;
		}

		public string Name
		{
			get;
			set;
		}

		public HibernatingRhinos.Profiler.BackEnd.Infrastructure.TableSchema TableSchema
		{
			get;
			set;
		}

		public TableNamesAndScema()
		{
		}
	}
}