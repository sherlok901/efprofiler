using System;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;

namespace HibernatingRhinos.Profiler.BackEnd.ModelBuilding
{
	public static class SolutionExplorer
	{
		private readonly static SolutionItem[] solutions;

		static SolutionExplorer()
		{
			SolutionItem[] solutionItemArray = new SolutionItem[1];
			SolutionItem solutionItem = new SolutionItem()
			{
				Regex = "^SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.",
				Slug = "SqlDateTime-overflow",
				Title = "SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM."
			};
			solutionItemArray[0] = solutionItem;
			SolutionExplorer.solutions = solutionItemArray;
		}

		public static SolutionItem FindSolutionForError(string message)
		{
			return SolutionExplorer.solutions.FirstOrDefault<SolutionItem>((SolutionItem item) => Regex.IsMatch(message, item.Regex));
		}
	}
}