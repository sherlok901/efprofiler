using gudusoft.gsqlparser;
using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.BackEnd.Extensions;
using HibernatingRhinos.Profiler.BackEnd.Infrastructure;
using HibernatingRhinos.Profiler.BackEnd.Messages;
using HibernatingRhinos.Profiler.BackEnd.Profiles;
using HibernatingRhinos.Profiler.BackEnd.Reports;
using HibernatingRhinos.Profiler.BackEnd.Util;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace HibernatingRhinos.Profiler.BackEnd.ModelBuilding
{
	public class Session
	{
		public Guid InternalId = Guid.NewGuid();

		public int StatementsCount;

		public ConcurrentQueue<Statement> Statements = new ConcurrentQueue<Statement>();

		public readonly Dictionary<Guid, Statement> StatementsById = new Dictionary<Guid, Statement>();

		public readonly ConcurrentQueue<EntitySnapshot> EntitySnapshots = new ConcurrentQueue<EntitySnapshot>();

		private readonly static Regex insertIntoColumns;

		private readonly static Regex insertIntoParams;

		public Action<AlertInformation> AcceptAlertToTheCurrentTransaction;

		private readonly IDictionary<string, int> countsByRawSql = new Dictionary<string, int>();

		public DateTime? ClosedAt
		{
			get;
			set;
		}

		public SessionConnectionString ConnectionString
		{
			get;
			set;
		}

		public int CountOfCachedStatements
		{
			get;
			set;
		}

		public int CountOfStandardDatabaseStatements
		{
			get;
			set;
		}

		public TimeSpan? Duration
		{
			get;
			set;
		}

		public SessionIdHolder Id
		{
			get;
			set;
		}

		public bool IsInActiveTransaction
		{
			get
			{
				return this.AcceptAlertToTheCurrentTransaction != null;
			}
		}

		public bool IsInDtcTransaction
		{
			get;
			private set;
		}

		public bool IsOpen
		{
			get;
			private set;
		}

		public DateTime? LastGeneratedStats
		{
			get;
			set;
		}

		public ReactiveModelBuilder ModelBuilder
		{
			get;
			set;
		}

		public string Name
		{
			get;
			set;
		}

		public int Position
		{
			get;
			set;
		}

		public string Url
		{
			get;
			set;
		}

		static Session()
		{
			Session.insertIntoColumns = new Regex("\\((\\S+)\\)", RegexOptions.Compiled);
			Session.insertIntoParams = new Regex("\\bvalues\\s*\\((\\S+)\\)", RegexOptions.IgnoreCase | RegexOptions.Compiled);
		}

		public Session(SessionIdHolder sessionId)
		{
			this.Id = sessionId;
		}

		public void AddStatement(Statement statement)
		{
			statement.Position = Interlocked.Increment(ref this.StatementsCount);
			this.countsByRawSql[statement.RawSql] = this.GetCountOfStatementsByRawSql(statement.RawSql) + 1;
			if (statement.Options.HasFlag(SqlStatementOptions.Cached))
			{
				Session countOfCachedStatements = this;
				countOfCachedStatements.CountOfCachedStatements = countOfCachedStatements.CountOfCachedStatements + 1;
			}
			if (!statement.Options.HasFlag(SqlStatementOptions.Cached) && !statement.Options.HasFlag(SqlStatementOptions.Transaction) && !statement.Options.HasFlag(SqlStatementOptions.DtcTransaction) && !statement.Options.HasFlag(SqlStatementOptions.DDL))
			{
				Session countOfStandardDatabaseStatements = this;
				countOfStandardDatabaseStatements.CountOfStandardDatabaseStatements = countOfStandardDatabaseStatements.CountOfStandardDatabaseStatements + 1;
			}
			statement.OnAlertsChanged += new Action<Statement>((Statement statement1) => this.ModelBuilder.Notifications.OnSessionHaveBeenChanged(this));
			this.Statements.Enqueue(statement);
			this.StatementsById[statement.InternalId] = statement;
			this.ModelBuilder.Notifications.OnStatementHaveBeenChanged(statement);
		}

		private void AssociateInsertFormattedSqlColumnsWithParameters(Statement statement, object subStatementObj, List<Parameter> toDoParameters)
		{
			TCustomSqlStatement tCustomSqlStatement = subStatementObj as TCustomSqlStatement;
			if (tCustomSqlStatement.AsText.StartsWith("INSERT", StringComparison.OrdinalIgnoreCase))
			{
				List<Parameter> parameters = new List<Parameter>();
				Match match = Session.insertIntoColumns.Match(tCustomSqlStatement.AsText);
				if (match.Success)
				{
					Match match1 = Session.insertIntoParams.Match(tCustomSqlStatement.AsText, match.Index + match.Length);
					if (match1.Success)
					{
						string[] strArrays = match.Groups[1].Value.Trim().Split(new char[] { ',' });
						string[] strArrays1 = match1.Groups[1].Value.Trim().Split(new char[] { ',' });
						for (int i = 0; i < (int)strArrays.Length; i++)
						{
							Parameter parameter = ((IEnumerable<Parameter>)statement.Parameters).FirstOrDefault<Parameter>((Parameter x) => x.Name == strArrays1[i]);
							if (parameter != null)
							{
								parameter.AssociateColumn = strArrays[i];
								parameters.Add(parameter);
								toDoParameters.Remove(parameter);
							}
						}
					}
				}
				if (parameters.Any<Parameter>())
				{
					StringBuilder stringBuilder = new StringBuilder(statement.FormattedSql);
					int length = 0;
					int num = 0;
					foreach (Parameter parameter1 in parameters)
					{
						if (parameter1.AssociateColumn == null)
						{
							continue;
						}
						int num1 = statement.FormattedSql.IndexOf(parameter1.Name, num);
						if (num1 == -1)
						{
							continue;
						}
						num = num1;
						string str = string.Concat(" - ", parameter1.AssociateColumn);
						stringBuilder.Insert(num1 + parameter1.Name.Length + length, str);
						length += str.Length;
					}
					statement.FormattedSql = stringBuilder.ToString();
				}
			}
		}

		private void AssociateTableAndTypeToParam(Statement statement)
		{
			TLzStatementList subStatements = statement.SubStatements as TLzStatementList;
			if (subStatements == null)
			{
				return;
			}
			statement.StatementsAnalysis = new SqlStatementsAnalysis(statement.SubStatements);
			statement.StatementsAnalysis.DoAnalysis();
			if (this.ModelBuilder.ConnectionInfo == null || !statement.Tables.Any<string>() || statement.Parameters == null || !statement.Parameters.Any<Parameter>())
			{
				return;
			}
			ConnectionInfo connectionInfo = this.ModelBuilder.ConnectionInfo.FirstOrDefault<ConnectionInfo>((ConnectionInfo x) => x.IsSelected);
			if (connectionInfo == null)
			{
				return;
			}
			List<Parameter> list = statement.Parameters.ToList<Parameter>();
			foreach (TCustomSqlStatement subStatement in subStatements)
			{
				if (!list.Any<Parameter>())
				{
					break;
				}
				TLzCustomExpression whereClause = subStatement.WhereClause;
				if (whereClause != null)
				{
					List<TableNamesAndScema> tableNamesAndScemas = new List<TableNamesAndScema>();
					foreach (TLzTable table in subStatement.Tables)
					{
						TableNamesAndScema tableNamesAndScema = this.GetTableNamesAndScema(table, connectionInfo);
						if (tableNamesAndScema == null)
						{
							continue;
						}
						tableNamesAndScemas.Add(tableNamesAndScema);
					}
					if (tableNamesAndScemas.Count == 0)
					{
						continue;
					}
					bool flag = false;
					bool flag1 = false;
					if (subStatement.Tables == null)
					{
						continue;
					}
					WhereClauseParametersProcessor whereClauseParametersProcessor = new WhereClauseParametersProcessor(whereClause);
					whereClauseParametersProcessor.Process(list, tableNamesAndScemas);
					whereClauseParametersProcessor.AddParmeterInfoToFormattedSql(statement);
					string asText = whereClause.AsText;
					foreach (WhereClauseParameter parametersList in whereClauseParametersProcessor.ParametersList)
					{
						if (!parametersList.IsTabaleNameExist || parametersList.Parameter.ColumnType == null)
						{
							continue;
						}
						if (parametersList.Parameter.ColumnType.ConstraintType == "PRIMARY KEY" || parametersList.Parameter.ColumnType.ConstraintType == "FOREIGN KEY")
						{
							flag = true;
						}
						flag1 = true;
					}
					if (flag || !flag1)
					{
						continue;
					}
					statement.IsUnIdexedColumn = true;
				}
				else
				{
					this.AssociateInsertFormattedSqlColumnsWithParameters(statement, subStatement, list);
				}
			}
		}

		public SessionStatistics CreateSessionStatistics()
		{
			SessionStatistics sessionStatistic = new SessionStatistics()
			{
				SessionId = this.InternalId,
				Name = this.Name
			};
			SessionStatistics sessionStatistic1 = sessionStatistic;
			foreach (Statement statement in this.Statements)
			{
				if (statement.Options.HasFlag(SqlStatementOptions.Cached))
				{
					SessionStatistics numberOfCachedStatements = sessionStatistic1;
					numberOfCachedStatements.NumberOfCachedStatements = numberOfCachedStatements.NumberOfCachedStatements + 1;
				}
				else if (!statement.IsTransaction)
				{
					SessionStatistics numberOfStatements = sessionStatistic1;
					numberOfStatements.NumberOfStatements = numberOfStatements.NumberOfStatements + 1;
				}
				else
				{
					SessionStatistics numberOfTransactionsStatements = sessionStatistic1;
					numberOfTransactionsStatements.NumberOfTransactionsStatements = numberOfTransactionsStatements.NumberOfTransactionsStatements + 1;
				}
				if (!sessionStatistic1.HasSuggestions)
				{
					if (statement.Alerts.Any<AlertInformation>((AlertInformation x) => x.Severity == Severity.Suggestion))
					{
						sessionStatistic1.HasSuggestions = true;
					}
				}
				if (!sessionStatistic1.HasWarnings)
				{
					if (statement.Alerts.Any<AlertInformation>((AlertInformation x) => x.Severity == Severity.Warning))
					{
						sessionStatistic1.HasWarnings = true;
					}
				}
				if (sessionStatistic1.HasMajor)
				{
					continue;
				}
				if (!statement.Alerts.Any<AlertInformation>((AlertInformation x) => x.Severity == Severity.Major))
				{
					continue;
				}
				sessionStatistic1.HasMajor = true;
			}
			return sessionStatistic1;
		}

		public int GetCountOfStatementsByRawSql(string rawSql)
		{
			int num;
			this.countsByRawSql.TryGetValue(rawSql, out num);
			return num;
		}

		public StatementSnapshot[] GetStatementsSnapshots()
		{
			return (
				from statement in this.Statements
				select statement.CreateSnapshot() into snapshot
				orderby snapshot.Position
				select snapshot).ToArray<StatementSnapshot>();
		}

		private TableNamesAndScema GetTableNamesAndScema(object table, ConnectionInfo connectionInfo)
		{
			TLzTable tLzTable = table as TLzTable;
			if (tLzTable == null)
			{
				return null;
			}
			string str = (tLzTable.Name.StartsWith("(") ? SqlProcessor.GetTableNameFromNestingStatement(tLzTable.Name) : tLzTable.Name);
			string tableAlias = tLzTable.TableAlias;
			TableSchema schema = this.ModelBuilder.TablesSchema.GetSchema(str, connectionInfo);
			if (schema == null || schema.Columns.Count == 0)
			{
				return null;
			}
			TableNamesAndScema tableNamesAndScema = new TableNamesAndScema()
			{
				Name = str,
				Alias = tableAlias,
				TableSchema = schema
			};
			return tableNamesAndScema;
		}

		private void GetTablesSchema(Statement statement)
		{
			if (this.ModelBuilder.ConnectionInfo == null || !statement.Tables.Any<string>())
			{
				return;
			}
			ConnectionInfo connectionInfo = this.ModelBuilder.ConnectionInfo.FirstOrDefault<ConnectionInfo>((ConnectionInfo x) => x.IsSelected);
			if (connectionInfo == null)
			{
				return;
			}
			string[] tables = statement.Tables;
			for (int i = 0; i < (int)tables.Length; i++)
			{
				string str = tables[i];
				this.ModelBuilder.TablesSchema.GetSchema(str, connectionInfo);
			}
		}

		public void HandleMessage(IHasSessionId message)
		{
			if (message is SessionOpened || message is ChildSessionCreated)
			{
				this.IsOpen = true;
				this.ModelBuilder.Notifications.OnSessionHaveBeenChanged(this);
				return;
			}
			this.InvokeIfMatch<SessionClosed>(message, new Action<SessionClosed>(this.OnSessionClosed));
			this.InvokeIfMatch<LoadedEntity>(message, new Action<LoadedEntity>(this.OnLoadedEntity));
			this.InvokeIfMatch<LoadedEntityFromSecondLevelCache>(message, new Action<LoadedEntityFromSecondLevelCache>(this.OnLoadedEntityFromSecondLevelCache));
			this.InvokeIfMatch<LoadedCollectionFromSecondLevelCache>(message, new Action<LoadedCollectionFromSecondLevelCache>(this.OnLoadedCollectionFromSecondLevelCache));
			this.InvokeIfMatch<SqlStatementMessage>(message, new Action<SqlStatementMessage>(this.OnSqlStatementMessage));
			this.InvokeIfMatch<ErrorDetected>(message, new Action<ErrorDetected>(this.OnErrorDetected));
			this.InvokeIfMatch<TransactionBegan>(message, new Action<TransactionBegan>(this.OnTransactionBegan));
			this.InvokeIfMatch<TransactionRollback>(message, new Action<TransactionRollback>(this.OnTransactionRollback));
			this.InvokeIfMatch<TransactionCommitted>(message, new Action<TransactionCommitted>(this.OnTransactionCommitted));
			this.InvokeIfMatch<TransactionDisposed>(message, new Action<TransactionDisposed>(this.OnTransactionDisposed));
			this.InvokeIfMatch<CountOfRowsInQuery>(message, new Action<CountOfRowsInQuery>(this.OnCountOfRowsInQuery));
			this.InvokeIfMatch<QueryDurationInDatabase>(message, new Action<QueryDurationInDatabase>(this.OnQueryDurationInDatabase));
			this.InvokeIfMatch<QueryDurationInApplication>(message, new Action<QueryDurationInApplication>(this.OnQueryDurationInApplication));
			this.InvokeIfMatch<RenameSession>(message, (RenameSession renameSession) => this.Name = renameSession.Name);
			this.InvokeIfMatch<SessionConnectionString>(message, new Action<SessionConnectionString>(this.OnSessionConnectionString));
		}

		private void InvokeIfMatch<T>(object msg, Action<T> action)
		where T : class
		{
			T t = (T)(msg as T);
			if (t == null)
			{
				return;
			}
			action(t);
		}

		private void OnCountOfRowsInQuery(CountOfRowsInQuery rowCount)
		{
			Statement nullable;
			if (!string.IsNullOrEmpty(rowCount.StatementId))
			{
				nullable = this.Statements.FirstOrDefault<Statement>((Statement x) => x.Id == rowCount.StatementId);
			}
			else
			{
				nullable = this.Statements.LastOrDefault<Statement>((Statement x) => x.SessionId.Timestamp <= rowCount.SessionId.Timestamp);
				if (nullable != null)
				{
					rowCount.StatementId = nullable.Id;
				}
			}
			if (nullable == null)
			{
				return;
			}
			try
			{
				if (!nullable.GotAccurateRowCount || rowCount.IsRows)
				{
					nullable.AddRowCount(rowCount.RowCount);
				}
				if (rowCount.IsRows)
				{
					nullable.GotAccurateRowCount = true;
				}
				if (!nullable.Duration.InNHibernate.HasValue)
				{
					TimeSpan timestamp = rowCount.SessionId.Timestamp - nullable.SessionId.Timestamp;
					nullable.Duration.InNHibernate = new int?((int)timestamp.TotalMilliseconds);
				}
			}
			finally
			{
				this.ModelBuilder.Notifications.OnStatementHaveBeenChanged(nullable);
			}
		}

		private void OnErrorDetected(ErrorDetected errorDetected)
		{
			string message = errorDetected.Message;
			Statement statement = new Statement()
			{
				SessionId = errorDetected.SessionId,
				InternalSessionId = this.InternalId,
				RawSql = message,
				FormattedSql = string.Concat(message, Environment.NewLine, errorDetected.ExceptionString),
				ShortSql = message.First50Characters(),
				UnfilteredStackTrace = errorDetected.StackTrace,
				StackTrace = this.ModelBuilder.StackTraceProcessor.ProcessStackTrace(errorDetected.StackTrace),
				Url = errorDetected.Url,
				Prefix = string.Concat(errorDetected.Severity, ": "),
				IgnoredAlertsTitles = this.ModelBuilder.Configuration.FindIgnoredAlerts(message, errorDetected.StackTrace),
				Options = SqlStatementOptions.Warning,
				OrmError = message,
				ErrorSolution = SolutionExplorer.FindSolutionForError(message)
			};
			Statement statement1 = statement;
			statement1.AcceptAlert(AlertInformation.ErrorDetected);
			Statement statement2 = this.Statements.LastOrDefault<Statement>();
			if (statement2 != null && !statement2.Options.HasFlag(SqlStatementOptions.Warning))
			{
				statement2.AcceptAlert(AlertInformation.ErrorDetectedSeeNextStatement);
				statement2.OrmError = message;
				statement2.ErrorSolution = SolutionExplorer.FindSolutionForError(message);
			}
			this.AddStatement(statement1);
		}

		private void OnLoadedCollectionFromSecondLevelCache(LoadedCollectionFromSecondLevelCache loadedCollectionFromSecondLevelCache)
		{
			int num = loadedCollectionFromSecondLevelCache.EntityName.LastIndexOf('.');
			string str = loadedCollectionFromSecondLevelCache.EntityName.Substring(num + 1);
			string str1 = string.Format("2nd level cache collection load {0}.{1} ({2} /* id */)", str, loadedCollectionFromSecondLevelCache.CollectionName, loadedCollectionFromSecondLevelCache.EntityId);
			Statement statement = new Statement()
			{
				SessionId = loadedCollectionFromSecondLevelCache.SessionId,
				InternalSessionId = this.InternalId,
				RawSql = str1,
				FormattedSql = str1,
				ShortSql = str1,
				UnfilteredStackTrace = loadedCollectionFromSecondLevelCache.StackTrace,
				StackTrace = this.ModelBuilder.StackTraceProcessor.ProcessStackTrace(loadedCollectionFromSecondLevelCache.StackTrace)
			};
			Parameter[] parameter = new Parameter[] { new Parameter("id", loadedCollectionFromSecondLevelCache.EntityId, null, null) };
			statement.Parameters = parameter;
			statement.Url = loadedCollectionFromSecondLevelCache.Url;
			statement.IgnoredAlertsTitles = this.ModelBuilder.Configuration.FindIgnoredAlerts(str1, loadedCollectionFromSecondLevelCache.StackTrace);
			statement.Options = SqlStatementOptions.Cached;
			this.AddStatement(statement);
		}

		private void OnLoadedEntity(LoadedEntity loadedEntity)
		{
			EntitySnapshot entitySnapshot = this.EntitySnapshots.FirstOrDefault<EntitySnapshot>((EntitySnapshot x) => x.Name == loadedEntity.EntityName);
			if (entitySnapshot == null)
			{
				entitySnapshot = new EntitySnapshot()
				{
					Name = loadedEntity.EntityName
				};
				this.EntitySnapshots.Enqueue(entitySnapshot);
			}
			if (!entitySnapshot.Identifiers.Any<string>((string x) => x == loadedEntity.EntityId))
			{
				entitySnapshot.Identifiers.Enqueue(loadedEntity.EntityId);
			}
			this.ModelBuilder.Notifications.OnSessionHaveBeenChanged(this);
		}

		private void OnLoadedEntityFromSecondLevelCache(LoadedEntityFromSecondLevelCache loadedEntityFromSecondLevelCache)
		{
			int num = loadedEntityFromSecondLevelCache.EntityName.LastIndexOf('.');
			string str = loadedEntityFromSecondLevelCache.EntityName.Substring(num + 1);
			string str1 = string.Format("2nd level cache load {0} ({1} /* id */)", str, loadedEntityFromSecondLevelCache.EntityId);
			Statement statement = new Statement()
			{
				SessionId = loadedEntityFromSecondLevelCache.SessionId,
				InternalSessionId = this.InternalId,
				RawSql = str1,
				FormattedSql = str1,
				ShortSql = str1,
				UnfilteredStackTrace = loadedEntityFromSecondLevelCache.StackTrace,
				StackTrace = this.ModelBuilder.StackTraceProcessor.ProcessStackTrace(loadedEntityFromSecondLevelCache.StackTrace)
			};
			Parameter[] parameter = new Parameter[] { new Parameter("id", loadedEntityFromSecondLevelCache.EntityId, null, null) };
			statement.Parameters = parameter;
			statement.Url = loadedEntityFromSecondLevelCache.Url;
			statement.IgnoredAlertsTitles = this.ModelBuilder.Configuration.FindIgnoredAlerts(str1, loadedEntityFromSecondLevelCache.StackTrace);
			statement.Options = SqlStatementOptions.Cached;
			this.AddStatement(statement);
		}

		private void OnQueryDurationInApplication(QueryDurationInApplication queryDurationInApplication)
		{
			Statement nullable = this.Statements.LastOrDefault<Statement>();
			if (nullable == null)
			{
				return;
			}
			nullable.Duration.InNHibernate = new int?(queryDurationInApplication.Milliseconds);
			this.ModelBuilder.Notifications.OnStatementHaveBeenChanged(nullable);
		}

		private void OnQueryDurationInDatabase(QueryDurationInDatabase queryDurationInDatabase)
		{
			Statement nullable = this.Statements.LastOrDefault<Statement>();
			if (nullable == null)
			{
				return;
			}
			nullable.Duration.InDatabase = new int?(queryDurationInDatabase.Milliseconds);
			this.ModelBuilder.Notifications.OnStatementHaveBeenChanged(nullable);
		}

		private void OnSessionClosed(SessionClosed sessionClosed)
		{
			TimeSpan? nullable;
			this.IsOpen = false;
			this.ClosedAt = new DateTime?(sessionClosed.SessionId.Timestamp);
			DateTime? closedAt = this.ClosedAt;
			DateTime timestamp = this.Id.Timestamp;
			if (closedAt.HasValue)
			{
				nullable = new TimeSpan?(closedAt.GetValueOrDefault() - timestamp);
			}
			else
			{
				nullable = null;
			}
			this.Duration = nullable;
			this.ProcessSessionProcessors();
			this.ModelBuilder.Notifications.OnSessionHaveBeenChanged(this);
		}

		private void OnSessionConnectionString(SessionConnectionString message)
		{
			this.ConnectionString = message;
		}

		public void OnSqlStatementMessage(SqlStatementMessage sqlStatementMessage)
		{
			SqlStatementProcessor sqlStatementProcessor = new SqlStatementProcessor(sqlStatementMessage)
			{
				Configuration = this.ModelBuilder.Configuration
			};
			Statement sessionId = sqlStatementProcessor.ProcessSql();
			this.GetTablesSchema(sessionId);
			this.AssociateTableAndTypeToParam(sessionId);
			sessionId.SessionId = sqlStatementMessage.SessionId ?? this.Id;
			sessionId.InternalSessionId = this.InternalId;
			this.AddStatement(sessionId);
			this.ProcessStatementProcessors(sessionId);
			this.ModelBuilder.Reports.AddQuery(sessionId);
		}

		private void OnTransactionBegan(TransactionBegan transactionBegan)
		{
			string str;
			str = (!transactionBegan.IsDtcTransaction ? string.Concat("begin transaction with isolation level: ", transactionBegan.IsolationLevel) : string.Concat("enlisted [[session]] in distributed transaction with isolation level: ", transactionBegan.IsolationLevel));
			str = Profile.Current.Translate(str);
			this.IsInDtcTransaction = transactionBegan.IsDtcTransaction;
			Statement statement = new Statement()
			{
				SessionId = transactionBegan.SessionId,
				InternalSessionId = this.InternalId,
				FormattedSql = str,
				ShortSql = str,
				RawSql = str,
				UnfilteredStackTrace = transactionBegan.StackTrace,
				StackTrace = this.ModelBuilder.StackTraceProcessor.ProcessStackTrace(transactionBegan.StackTrace),
				Url = transactionBegan.Url,
				IgnoredAlertsTitles = this.ModelBuilder.Configuration.FindIgnoredAlerts(str, transactionBegan.StackTrace),
				Options = (transactionBegan.IsDtcTransaction ? SqlStatementOptions.DtcTransaction : SqlStatementOptions.Transaction),
				IsolationLevel = transactionBegan.IsolationLevel
			};
			Statement statement1 = statement;
			this.AcceptAlertToTheCurrentTransaction = new Action<AlertInformation>(statement1.AcceptAlert);
			this.AddStatement(statement1);
			this.ProcessStatementProcessors(statement1);
		}

		private void OnTransactionCommitted(TransactionCommitted transactionCommitted)
		{
			Statement statement = new Statement()
			{
				SessionId = transactionCommitted.SessionId,
				InternalSessionId = this.InternalId,
				FormattedSql = "commit transaction",
				ShortSql = "commit transaction",
				RawSql = "commit transaction",
				UnfilteredStackTrace = transactionCommitted.StackTrace,
				StackTrace = this.ModelBuilder.StackTraceProcessor.ProcessStackTrace(transactionCommitted.StackTrace),
				Url = transactionCommitted.Url,
				IgnoredAlertsTitles = this.ModelBuilder.Configuration.FindIgnoredAlerts("commit transaction", transactionCommitted.StackTrace),
				Options = (this.IsInDtcTransaction ? SqlStatementOptions.DtcTransaction : SqlStatementOptions.Transaction)
			};
			Statement statement1 = statement;
			this.AcceptAlertToTheCurrentTransaction = null;
			this.IsInDtcTransaction = false;
			this.ModelBuilder.Reports.QueriesByIsolationLevel.AddTransactionalQueries(this);
			this.AddStatement(statement1);
			this.ProcessStatementProcessors(statement1);
		}

		private void OnTransactionDisposed(TransactionDisposed transactionDisposed)
		{
			if (!this.IsInActiveTransaction)
			{
				return;
			}
			this.AcceptAlertToTheCurrentTransaction(AlertInformation.TransactionDisposedWithoutRollback);
			this.ModelBuilder.Notifications.OnSessionHaveBeenChanged(this);
		}

		private void OnTransactionRollback(TransactionRollback transactionRollback)
		{
			Statement statement = new Statement()
			{
				SessionId = transactionRollback.SessionId,
				InternalSessionId = this.InternalId,
				FormattedSql = "rollback transaction",
				ShortSql = "rollback transaction",
				RawSql = "rollback transaction",
				UnfilteredStackTrace = transactionRollback.StackTrace,
				StackTrace = this.ModelBuilder.StackTraceProcessor.ProcessStackTrace(transactionRollback.StackTrace),
				Url = transactionRollback.Url,
				IgnoredAlertsTitles = this.ModelBuilder.Configuration.FindIgnoredAlerts("rollback transaction", transactionRollback.StackTrace),
				Options = (this.IsInDtcTransaction ? SqlStatementOptions.DtcTransaction : SqlStatementOptions.Transaction)
			};
			Statement statement1 = statement;
			this.AcceptAlertToTheCurrentTransaction = null;
			this.IsInDtcTransaction = false;
			this.ModelBuilder.Reports.QueriesByIsolationLevel.AddTransactionalQueries(this);
			this.AddStatement(statement1);
			this.ProcessStatementProcessors(statement1);
		}

		private void ProcessSessionProcessors()
		{
			(
				from processor in this.ModelBuilder.SessionProcessors
				where !this.ModelBuilder.Configuration.DisabledAlerts.Contains(processor.Name)
				select processor).ForEach<AbstractSessionProcessor>((AbstractSessionProcessor processor) => processor.OnSessionClosed(this));
		}

		private void ProcessStatementProcessors(Statement statement)
		{
			(
				from processor in this.ModelBuilder.StatementProcessors
				where !this.ModelBuilder.Configuration.DisabledAlerts.Contains(processor.Name)
				select processor).ForEach<AbstractStatementProcessor>((AbstractStatementProcessor processor) => processor.AfterAttachingToSession(this, statement));
		}
	}
}