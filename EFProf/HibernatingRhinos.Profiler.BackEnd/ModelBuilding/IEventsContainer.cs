using System;
using System.Collections.Generic;

namespace HibernatingRhinos.Profiler.BackEnd.ModelBuilding
{
	public interface IEventsContainer
	{
		IList<EventInformation> GetEvents();

		void SetEvents(IList<EventInformation> events);
	}
}