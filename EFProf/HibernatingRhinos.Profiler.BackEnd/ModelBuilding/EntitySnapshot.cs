using System;
using System.Collections.Concurrent;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.ModelBuilding
{
	public class EntitySnapshot
	{
		private ConcurrentQueue<string> identifiers;

		public ConcurrentQueue<string> Identifiers
		{
			get
			{
				ConcurrentQueue<string> strs = this.identifiers;
				if (strs == null)
				{
					ConcurrentQueue<string> strs1 = new ConcurrentQueue<string>();
					ConcurrentQueue<string> strs2 = strs1;
					this.identifiers = strs1;
					strs = strs2;
				}
				return strs;
			}
			set
			{
				this.identifiers = value;
			}
		}

		public string Name
		{
			get;
			set;
		}

		public EntitySnapshot()
		{
		}
	}
}