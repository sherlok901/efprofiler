using System;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.ModelBuilding
{
	public class EventInformation
	{
		public MulticastDelegate AttachedMethods
		{
			get;
			private set;
		}

		public string Name
		{
			get;
			private set;
		}

		public EventInformation(string eventName, MulticastDelegate methods)
		{
			this.Name = eventName;
			this.AttachedMethods = methods;
		}
	}
}