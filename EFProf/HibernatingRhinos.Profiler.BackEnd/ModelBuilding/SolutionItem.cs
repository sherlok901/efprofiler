using System;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.ModelBuilding
{
	public class SolutionItem
	{
		public string Regex
		{
			get;
			set;
		}

		public string Slug
		{
			get;
			set;
		}

		public string Title
		{
			get;
			set;
		}

		public SolutionItem()
		{
		}
	}
}