using HibernatingRhinos.Profiler.Appender;
using HibernatingRhinos.Profiler.Appender.Util;
using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.BackEnd.Extensions;
using HibernatingRhinos.Profiler.BackEnd.Infrastructure;
using HibernatingRhinos.Profiler.BackEnd.Listeners;
using HibernatingRhinos.Profiler.BackEnd.LoggingProcessors;
using HibernatingRhinos.Profiler.BackEnd.Messages;
using HibernatingRhinos.Profiler.BackEnd.Profiles;
using HibernatingRhinos.Profiler.BackEnd.Reports;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd.ModelBuilding
{
	public class ReactiveModelBuilder
	{
		private ProfilerConfiguration configuration;

		private ReportsBridge reports;

		private HibernatingRhinos.Profiler.BackEnd.StackTraceProcessor stackTraceProcessor;

		private AbstractStatementProcessor[] statementProcessors;

		private AbstractSessionProcessor[] sessionProcessors;

		private AbstractMessageProcessor[] messageProcessors;

		private ConcurrentDictionary<SessionIdHolder, Session> sessions;

		private Dictionary<Guid, Session> sessionsById;

		public readonly ConcurrentDictionary<string, SessionFactoryStats> CurrentStats = new ConcurrentDictionary<string, SessionFactoryStats>();

		private int sessionPosition;

		public ProfilerConfiguration Configuration
		{
			get
			{
				ProfilerConfiguration profilerConfiguration = this.configuration;
				if (profilerConfiguration == null)
				{
					ProfilerConfiguration profilerConfiguration1 = new ProfilerConfiguration();
					ProfilerConfiguration profilerConfiguration2 = profilerConfiguration1;
					this.configuration = profilerConfiguration1;
					profilerConfiguration = profilerConfiguration2;
				}
				return profilerConfiguration;
			}
			set
			{
				this.configuration = value;
			}
		}

		public List<HibernatingRhinos.Profiler.BackEnd.Infrastructure.ConnectionInfo> ConnectionInfo
		{
			get;
			set;
		}

		public HibernatingRhinos.Profiler.BackEnd.Listeners.EventsObserver EventsObserver
		{
			get;
			set;
		}

		public AbstractMessageProcessor[] MessageProcessors
		{
			get
			{
				if (this.messageProcessors == null)
				{
					this.messageProcessors = (
						from x in typeof(AbstractMessageProcessor).Assembly.GetLoadableTypes()
						where typeof(AbstractMessageProcessor).IsAssignableFrom(x)
						where !x.IsAbstract
						select x).Where<Type>(new Func<Type, bool>(ReactiveModelBuilder.IsValidForCurrentProfile)).Select<Type, AbstractMessageProcessor>((Type x) => (AbstractMessageProcessor)Activator.CreateInstance(x)).ToArray<AbstractMessageProcessor>();
					((IEnumerable<AbstractMessageProcessor>)this.messageProcessors).ForEach<AbstractMessageProcessor>((AbstractMessageProcessor processor) => {
						processor.ModelBuilder = this;
						processor.Attach(this.EventsObserver);
					});
				}
				return this.messageProcessors;
			}
			set
			{
				this.messageProcessors = value;
			}
		}

		public NotificationsBridge Notifications
		{
			get;
			private set;
		}

		public ReportsBridge Reports
		{
			get
			{
				ReportsBridge reportsBridge = this.reports;
				if (reportsBridge == null)
				{
					ReportsBridge reportsBridge1 = new ReportsBridge();
					ReportsBridge reportsBridge2 = reportsBridge1;
					this.reports = reportsBridge1;
					reportsBridge = reportsBridge2;
				}
				return reportsBridge;
			}
			set
			{
				this.reports = value;
			}
		}

		public AbstractSessionProcessor[] SessionProcessors
		{
			get
			{
				if (this.sessionProcessors == null)
				{
					this.sessionProcessors = (
						from x in typeof(AbstractSessionProcessor).Assembly.GetLoadableTypes()
						where typeof(AbstractSessionProcessor).IsAssignableFrom(x)
						where !x.IsAbstract
						select x).Where<Type>((Type x) => {
						if (x.Namespace == "HibernatingRhinos.Profiler.BackEnd.SessionProcessors")
						{
							return true;
						}
						return x.Namespace.StartsWith(string.Concat("HibernatingRhinos.Profiler.BackEnd.SessionProcessors.", Profile.CurrentProfile));
					}).Select<Type, AbstractSessionProcessor>((Type x) => (AbstractSessionProcessor)Activator.CreateInstance(x)).ToArray<AbstractSessionProcessor>();
					((IEnumerable<AbstractSessionProcessor>)this.sessionProcessors).ForEach<AbstractSessionProcessor>((AbstractSessionProcessor processor) => {
						processor.Configuration = this.Configuration;
						processor.ModelBuilder = this;
					});
				}
				return this.sessionProcessors;
			}
			set
			{
				this.sessionProcessors = value;
			}
		}

		public ConcurrentDictionary<SessionIdHolder, Session> Sessions
		{
			get
			{
				ConcurrentDictionary<SessionIdHolder, Session> sessionIdHolders = this.sessions;
				if (sessionIdHolders == null)
				{
					ConcurrentDictionary<SessionIdHolder, Session> sessionIdHolders1 = new ConcurrentDictionary<SessionIdHolder, Session>();
					ConcurrentDictionary<SessionIdHolder, Session> sessionIdHolders2 = sessionIdHolders1;
					this.sessions = sessionIdHolders1;
					sessionIdHolders = sessionIdHolders2;
				}
				return sessionIdHolders;
			}
			set
			{
				this.sessions = value;
			}
		}

		public Dictionary<Guid, Session> SessionsById
		{
			get
			{
				Dictionary<Guid, Session> guids = this.sessionsById;
				if (guids == null)
				{
					Dictionary<Guid, Session> guids1 = new Dictionary<Guid, Session>();
					Dictionary<Guid, Session> guids2 = guids1;
					this.sessionsById = guids1;
					guids = guids2;
				}
				return guids;
			}
			set
			{
				this.sessionsById = value;
			}
		}

		public HibernatingRhinos.Profiler.BackEnd.StackTraceProcessor StackTraceProcessor
		{
			get
			{
				HibernatingRhinos.Profiler.BackEnd.StackTraceProcessor stackTraceProcessor = this.stackTraceProcessor;
				if (stackTraceProcessor == null)
				{
					HibernatingRhinos.Profiler.BackEnd.StackTraceProcessor stackTraceProcessor1 = new HibernatingRhinos.Profiler.BackEnd.StackTraceProcessor(this.Configuration);
					HibernatingRhinos.Profiler.BackEnd.StackTraceProcessor stackTraceProcessor2 = stackTraceProcessor1;
					this.stackTraceProcessor = stackTraceProcessor1;
					stackTraceProcessor = stackTraceProcessor2;
				}
				return stackTraceProcessor;
			}
			set
			{
				this.stackTraceProcessor = value;
			}
		}

		public AbstractStatementProcessor[] StatementProcessors
		{
			get
			{
				if (this.statementProcessors == null)
				{
					this.statementProcessors = (
						from x in typeof(AbstractStatementProcessor).Assembly.GetLoadableTypes()
						where typeof(AbstractStatementProcessor).IsAssignableFrom(x)
						where !x.IsAbstract
						select x).Where<Type>((Type x) => {
						if (x.Namespace == "HibernatingRhinos.Profiler.BackEnd.StatementProcessors")
						{
							return true;
						}
						return x.Namespace.StartsWith(string.Concat("HibernatingRhinos.Profiler.BackEnd.StatementProcessors.", Profile.CurrentProfile));
					}).Select<Type, AbstractStatementProcessor>((Type x) => (AbstractStatementProcessor)Activator.CreateInstance(x)).ToArray<AbstractStatementProcessor>();
					((IEnumerable<AbstractStatementProcessor>)this.statementProcessors).ForEach<AbstractStatementProcessor>((AbstractStatementProcessor processor) => processor.Configuration = this.Configuration);
				}
				return this.statementProcessors;
			}
			set
			{
				this.statementProcessors = value;
			}
		}

		public SchemaCache TablesSchema
		{
			get;
			set;
		}

		public ReactiveModelBuilder()
		{
			this.Notifications = new NotificationsBridge(this);
		}

		public void Attach(HibernatingRhinos.Profiler.BackEnd.Listeners.EventsObserver eventsObserver)
		{
			this.EventsObserver = eventsObserver;
			eventsObserver.OfType<SessionFactoryStats>().Subscribe<SessionFactoryStats>((SessionFactoryStats stats) => this.CurrentStats.AddOrUpdate(stats.Name, (string name) => stats, (string s, SessionFactoryStats factoryStats) => stats));
			eventsObserver.OfType<IHasSessionId>().Subscribe<IHasSessionId>(new Action<IHasSessionId>(this.RegisterSession));
		}

		public void Clear()
		{
			this.Reports.Clear();
			this.Sessions = null;
			this.SessionsById = null;
			this.Notifications.LastNotifications = this.Notifications.LastNotifications.ClearMatches<BackendNotification>((BackendNotification notification) => notification.AttachedApplicationsStatus == null);
		}

		public void ClearExcept(IList<Guid> sessionIds)
		{
			Session session;
			string[] array = (
				from pair in this.Sessions
				where sessionIds.Contains(pair.Value.InternalId)
				select pair.Key.SessionId).ToArray<string>();
			this.Reports.ClearExcept(array);
			KeyValuePair<Guid, Session>[] keyValuePairArray = (
				from pair in this.SessionsById
				where !sessionIds.Contains(pair.Key)
				select pair).ToArray<KeyValuePair<Guid, Session>>();
			KeyValuePair<Guid, Session>[] keyValuePairArray1 = keyValuePairArray;
			for (int i = 0; i < (int)keyValuePairArray1.Length; i++)
			{
				KeyValuePair<Guid, Session> keyValuePair = keyValuePairArray1[i];
				this.Sessions.TryRemove(keyValuePair.Value.Id, out session);
				this.SessionsById.Remove(keyValuePair.Key);
			}
			this.Notifications.LastNotifications = this.Notifications.LastNotifications.ClearMatches<BackendNotification>((BackendNotification notification) => {
				if (notification.AttachedApplicationsStatus != null)
				{
					return false;
				}
				if (!notification.ChangedSessionId.HasValue)
				{
					return false;
				}
				return !sessionIds.Contains(notification.ChangedSessionId.Value);
			});
		}

		private static bool IsValidForCurrentProfile(Type type)
		{
			if (type.Namespace == "HibernatingRhinos.Profiler.BackEnd.LoggingProcessors")
			{
				return true;
			}
			if (type.Namespace == "HibernatingRhinos.Profiler.BackEnd.ModelBuilding")
			{
				return true;
			}
			if (type.Namespace == "HibernatingRhinos.Profiler.BackEnd.LoggingProcessors.CustomReporting")
			{
				return true;
			}
			return Profile.Current.StatementProcessorNamespaces.Any<string>((string ns) => type.Namespace == string.Concat("HibernatingRhinos.Profiler.BackEnd.LoggingProcessors.", ns));
		}

		private void RegisterSession(IHasSessionId message)
		{
			Func<KeyValuePair<SessionIdHolder, Session>, int> func = null;
			Session session2 = this.Sessions.AddOrUpdate(message.SessionId, (SessionIdHolder id) => {
				Session session3;
				if (this.Sessions.Count > 1000)
				{
					ConcurrentDictionary<SessionIdHolder, Session> sessions = this.Sessions;
					if (func == null)
					{
						func = (KeyValuePair<SessionIdHolder, Session> pair) => pair.Value.Position;
					}
					KeyValuePair<SessionIdHolder, Session>[] array = sessions.OrderBy<KeyValuePair<SessionIdHolder, Session>, int>(func).Take<KeyValuePair<SessionIdHolder, Session>>(this.Sessions.Count - 1000 + 400).ToArray<KeyValuePair<SessionIdHolder, Session>>();
					for (int i = 0; i < (int)array.Length; i++)
					{
						KeyValuePair<SessionIdHolder, Session> keyValuePair = array[i];
						if (this.Sessions.TryRemove(keyValuePair.Key, out session3))
						{
							this.sessionsById.Remove(session3.InternalId);
						}
					}
				}
				Session session1 = new Session(message.SessionId)
				{
					ModelBuilder = this,
					Name = string.Concat("[[session]] #", this.sessionPosition + 1)
				};
				ReactiveModelBuilder u003cu003e4_this = this;
				int num = u003cu003e4_this.sessionPosition + 1;
				int num1 = num;
				u003cu003e4_this.sessionPosition = num;
				session1.Position = num1;
				Session url = session1;
				this.SessionsById[url.InternalId] = url;
				IHasSessionUrl hasSessionUrl = message as IHasSessionUrl;
				if (hasSessionUrl != null)
				{
					url.Url = hasSessionUrl.Url;
				}
				url.HandleMessage(message);
				return url;
			}, (SessionIdHolder holder, Session session) => {
				session.HandleMessage(message);
				return session;
			});
			this.Reports.OverallUsage.OnSessionUpdate(session2);
		}
	}
}