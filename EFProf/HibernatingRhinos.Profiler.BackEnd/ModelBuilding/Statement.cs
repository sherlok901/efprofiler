using HibernatingRhinos.Profiler.Appender.StackTraces;
using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.BackEnd.Extensions;
using HibernatingRhinos.Profiler.BackEnd.Messages;
using HibernatingRhinos.Profiler.BackEnd.Util;
using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;

namespace HibernatingRhinos.Profiler.BackEnd.ModelBuilding
{
	public class Statement
	{
		public Guid InternalId = Guid.NewGuid();

		private ConcurrentQueue<AlertInformation> alerts;

		private bool? isSelectStatement;

		private ConcurrentQueue<int> countOfRows;

		public ConcurrentQueue<AlertInformation> Alerts
		{
			get
			{
				ConcurrentQueue<AlertInformation> alertInformations = this.alerts;
				if (alertInformations == null)
				{
					ConcurrentQueue<AlertInformation> alertInformations1 = new ConcurrentQueue<AlertInformation>();
					ConcurrentQueue<AlertInformation> alertInformations2 = alertInformations1;
					this.alerts = alertInformations1;
					alertInformations = alertInformations2;
				}
				return alertInformations;
			}
			set
			{
				this.alerts = value;
			}
		}

		public bool CanExecuteQuery
		{
			get
			{
				if (!this.IsSelectStatement)
				{
					return false;
				}
				if (this.Parameters != null && (int)this.Parameters.Length != 0)
				{
					return true;
				}
				return (this.RawSql.Contains("=?") || this.RawSql.Contains(" ?") || this.RawSql.Contains("@p0") ? false : !this.RawSql.Contains(":p0"));
			}
		}

		public ConcurrentQueue<int> CountOfRows
		{
			get
			{
				ConcurrentQueue<int> nums = this.countOfRows;
				if (nums == null)
				{
					ConcurrentQueue<int> nums1 = new ConcurrentQueue<int>();
					ConcurrentQueue<int> nums2 = nums1;
					this.countOfRows = nums1;
					nums = nums2;
				}
				return nums;
			}
			set
			{
				this.countOfRows = value;
			}
		}

		public QueryDuration Duration
		{
			get;
			private set;
		}

		public string Errors
		{
			get;
			set;
		}

		public SolutionItem ErrorSolution
		{
			get;
			set;
		}

		public string FormattedSql
		{
			get;
			set;
		}

		public bool GotAccurateRowCount
		{
			get;
			set;
		}

		public string Id
		{
			get;
			set;
		}

		public string[] IgnoredAlertsTitles
		{
			get;
			set;
		}

		public Guid InternalSessionId
		{
			get;
			set;
		}

		public string IsolationLevel
		{
			get;
			set;
		}

		public bool IsSelectStatement
		{
			get
			{
				if (!this.isSelectStatement.HasValue)
				{
					string rawSql = this.RawSql;
					string[] strArrays = new string[] { "\r\n", "\n" };
					string str = (
						from line in rawSql.Split(strArrays, StringSplitOptions.None)
						select line.Trim()).Where<string>((string line) => {
						if (line.StartsWith("USE", StringComparison.OrdinalIgnoreCase) || line.StartsWith("--", StringComparison.Ordinal))
						{
							return false;
						}
						return !line.StartsWith("/*", StringComparison.Ordinal);
					}).FirstOrDefault<string>((string line) => line.StartsWith("SELECT", StringComparison.OrdinalIgnoreCase));
					this.isSelectStatement = new bool?(str != null);
				}
				return this.isSelectStatement.Value;
			}
			set
			{
				this.isSelectStatement = new bool?(value);
			}
		}

		public bool IsTransaction
		{
			get
			{
				if (this.Options.HasFlag(SqlStatementOptions.Transaction))
				{
					return true;
				}
				return this.Options.HasFlag(SqlStatementOptions.DtcTransaction);
			}
		}

		public bool IsUnIdexedColumn
		{
			get;
			set;
		}

		public SqlStatementOptions Options
		{
			get;
			set;
		}

		public string OrmError
		{
			get;
			set;
		}

		public Parameter[] Parameters
		{
			get;
			set;
		}

		public int Position
		{
			get;
			set;
		}

		public string Prefix
		{
			get;
			set;
		}

		public bool QueryCountWasProcessed
		{
			get;
			set;
		}

		public string RawSql
		{
			get;
			set;
		}

		public SessionIdHolder SessionId
		{
			get;
			set;
		}

		public string ShortSql
		{
			get;
			set;
		}

		public StackTraceInfo StackTrace
		{
			get;
			set;
		}

		public string StarColor
		{
			get;
			set;
		}

		public SqlStatementsAnalysis StatementsAnalysis
		{
			get;
			set;
		}

		public object SubStatements
		{
			get;
			set;
		}

		public string[] Tables
		{
			get;
			set;
		}

		public bool UnboundedResultSetWasProcessed
		{
			get;
			set;
		}

		public StackTraceInfo UnfilteredStackTrace
		{
			get;
			set;
		}

		public string Url
		{
			get;
			set;
		}

		public Statement()
		{
			this.Duration = new QueryDuration();
			this.Duration.ValuesRefreshed += new Action(this.RaiseValuesRefreshed);
			this.Parameters = new Parameter[0];
			this.Tables = new string[0];
		}

		public void AcceptAlert(AlertInformation alertInformation)
		{
			if (this.IgnoredAlertsTitles != null && this.IgnoredAlertsTitles.Contains<string>(alertInformation.Title))
			{
				return;
			}
			this.Alerts.Enqueue(alertInformation);
			Action<Statement> action = this.OnAlertsChanged;
			if (action != null)
			{
				action(this);
			}
		}

		public void AddRowCount(int rowsCount)
		{
			int num;
			int num1 = this.RawSql.CountSubstrings("select") + this.RawSql.CountSubstrings("exec");
			while (num1 <= this.CountOfRows.Count && this.CountOfRows.Count > 0)
			{
				this.CountOfRows.TryDequeue(out num);
			}
			this.CountOfRows.Enqueue(rowsCount);
			this.RaiseValuesRefreshed();
		}

		public StatementSnapshot CreateSnapshot()
		{
			StatementSnapshot statementSnapshot = new StatementSnapshot()
			{
				Alerts = this.Alerts.ToArray(),
				CountOfRows = this.CountOfRows.ToArray(),
				IsCached = this.Options.HasFlag(SqlStatementOptions.Cached),
				IsDDL = this.Options.HasFlag(SqlStatementOptions.DDL),
				IsTransaction = this.IsTransaction,
				IsSelectStatement = this.IsSelectStatement,
				CanExecuteQuery = this.CanExecuteQuery,
				Duration = this.Duration,
				RawSql = this.RawSql,
				FormattedSql = this.FormattedSql,
				ShortSql = this.ShortSql,
				OrmError = this.OrmError,
				ErrorSolution = this.ErrorSolution,
				Prefix = this.Prefix,
				Parameters = this.Parameters.Take<Parameter>(50).ToArray<Parameter>(),
				StackTrace = this.StackTrace,
				Timestamp = this.SessionId.Timestamp,
				Url = this.Url,
				StarColor = this.StarColor,
				StatementId = this.InternalId,
				SessionId = this.InternalSessionId,
				SessionStatementCount = this.Position,
				Position = this.Position,
				Tables = this.Tables.ToArray<string>()
			};
			return statementSnapshot;
		}

		public void RaiseValuesRefreshed()
		{
			Action<Statement> action = this.ValuesRefreshed;
			if (action != null)
			{
				action(this);
			}
		}

		public event Action<Statement> OnAlertsChanged;

		public event Action<Statement> ValuesRefreshed;
	}
}