using Borland.Vcl;
using gudusoft.gsqlparser;
using HibernatingRhinos.Profiler.BackEnd.Extensions;
using HibernatingRhinos.Profiler.BackEnd.Infrastructure;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace HibernatingRhinos.Profiler.BackEnd
{
	public class SqlProcessor
	{
		private const int MaxShortSqlLengthProcessing = 10000;

		private const string ExludeSqlBatch = "SQL Batch:";

		private readonly string rawSql;

		private readonly string dialect;

		private readonly static Regex findTableInNestedSelect;

		private readonly static Regex postGresLimitClause;

		private readonly static Regex oracleParam;

		private readonly static Regex mySqlQuoted;

		private readonly static Regex mySqlLimit;

		private readonly static Regex oracleJoin;

		private readonly IDictionary<string, string> replacements = new Dictionary<string, string>();

		private TDbVendor dbVendor;

		static SqlProcessor()
		{
			SqlProcessor.findTableInNestedSelect = new Regex("\\bFROM\\s+([^\\(]\\S+)", RegexOptions.IgnoreCase | RegexOptions.Compiled);
			SqlProcessor.postGresLimitClause = new Regex(" limit :p\\d+", RegexOptions.Compiled);
			SqlProcessor.oracleParam = new Regex("[=<>\\s]\\?([\\w\\d]+)", RegexOptions.IgnoreCase | RegexOptions.Compiled);
			SqlProcessor.mySqlQuoted = new Regex("`[\\w\\d_]+`", RegexOptions.Compiled);
			SqlProcessor.mySqlLimit = new Regex("limit \\d+(\\s+,\\s+\\d+)?", RegexOptions.IgnoreCase | RegexOptions.Compiled);
			SqlProcessor.oracleJoin = new Regex("\\(\\+\\)", RegexOptions.IgnoreCase | RegexOptions.Compiled);
		}

		public SqlProcessor(string rawSql, string dialect)
		{
			this.rawSql = rawSql;
			this.dialect = dialect;
		}

		private static string GetFromTextUsingSql2005Paging(string fromText)
		{
			if (fromText.StartsWith("(") && !fromText.EndsWith(")"))
			{
				int num = fromText.LastIndexOf(")");
				if (num != -1)
				{
					fromText = fromText.Substring(0, num + 1);
				}
			}
			TGSqlParser tGSqlParser = new TGSqlParser(TDbVendor.DbVMssql);
			tGSqlParser.SqlText.Text = fromText;
			TGSqlParser tGSqlParser1 = tGSqlParser;
			if (tGSqlParser1.Parse() != 0)
			{
				return fromText;
			}
			TSelectSqlStatement tSelectSqlStatement = tGSqlParser1.SqlStatements.First() as TSelectSqlStatement;
			if (tSelectSqlStatement == null)
			{
				return fromText;
			}
			TLzTable joinTable = tSelectSqlStatement.JoinTables[0].JoinTable;
			if (joinTable.TableType != TLzTableType.lttSubquery)
			{
				fromText = tSelectSqlStatement.FromClauseText;
			}
			else
			{
				fromText = joinTable.SubQuery.AsText;
			}
			return fromText;
		}

		private string GetShortSql(string sql)
		{
			TGSqlParser tGSqlParser = new TGSqlParser(this.dbVendor);
			tGSqlParser.SqlText.Text = sql.FirstCharacters(10000, FormattingOptions.AllowMultiLine);
			TGSqlParser tGSqlParser1 = tGSqlParser;
			if (tGSqlParser1.PrettyPrint() != 0)
			{
				return sql.First50Characters();
			}
			return this.GetShortSql(tGSqlParser1.SqlStatements);
		}

		private string GetShortSql(TLzStatementList statements)
		{
			StringBuilder stringBuilder = new StringBuilder();
			TCustomSqlStatement tCustomSqlStatement = null;
			int num = 0;
			foreach (TCustomSqlStatement statement in statements)
			{
				if (num < 3 || statements.Count() <= 5)
				{
					num++;
					bool flag = false;
					TSelectSqlStatement tSelectSqlStatement = statement as TSelectSqlStatement;
					if (tSelectSqlStatement != null)
					{
						if (tCustomSqlStatement is TInsertSqlStatement && tSelectSqlStatement.FromClauseText.Empty())
						{
							continue;
						}
						flag = true;
						string fromClauseText = tSelectSqlStatement.FromClauseText;
						if (tSelectSqlStatement.SortClauseText.Contains("__hibernate_sort"))
						{
							fromClauseText = SqlProcessor.GetFromTextUsingSql2005Paging(fromClauseText);
							fromClauseText = SqlProcessor.GetFromTextUsingSql2005Paging(fromClauseText);
						}
						if (!string.IsNullOrEmpty(fromClauseText))
						{
							stringBuilder.Append("SELECT ... FROM ").Append(fromClauseText.First50Characters());
						}
						else
						{
							stringBuilder.Append("SELECT ").Append(tSelectSqlStatement.SelectClauseText.First50Characters());
						}
					}
					TUpdateSqlStatement tUpdateSqlStatement = statement as TUpdateSqlStatement;
					if (tUpdateSqlStatement != null)
					{
						flag = true;
						stringBuilder.Append("UPDATE").Append(tUpdateSqlStatement.UpdateTables.AsText.First50Characters()).Append("...");
					}
					TInsertSqlStatement tInsertSqlStatement = statement as TInsertSqlStatement;
					if (tInsertSqlStatement != null)
					{
						flag = true;
						if (!tInsertSqlStatement.AsText.StartsWith("INSERT INTO", StringComparison.OrdinalIgnoreCase))
						{
							stringBuilder.Append("INSERT ");
						}
						else
						{
							stringBuilder.Append("INSERT INTO ");
						}
						stringBuilder.Append(tInsertSqlStatement.TableText.First50Characters()).Append(" ...");
					}
					TDeleteSqlStatement tDeleteSqlStatement = statement as TDeleteSqlStatement;
					if (tDeleteSqlStatement != null)
					{
						flag = true;
						stringBuilder.Append("DELETE FROM ").Append(tDeleteSqlStatement.TableText.First50Characters());
					}
					if (!flag)
					{
						stringBuilder.Append(statement.RawSqlText.First50Characters());
					}
					if (!string.IsNullOrEmpty(statement.WhereClauseText))
					{
						stringBuilder.Append(" WHERE ").Append(statement.WhereClauseText.First50Characters());
					}
					stringBuilder.AppendLine();
					tCustomSqlStatement = statement;
				}
				else
				{
					stringBuilder.Append(statements.Count() - num);
					stringBuilder.AppendLine(" additional statements ...");
					break;
				}
			}
			if (stringBuilder.Length > 2)
			{
				StringBuilder length = stringBuilder;
				length.Length = length.Length - 2;
			}
			return stringBuilder.ToString();
		}

		public static string GetTableNameFromNestingStatement(string sqlNestingStatement)
		{
			string value = "";
			Match match = SqlProcessor.findTableInNestedSelect.Match(sqlNestingStatement);
			if (match.Success)
			{
				value = match.Groups[1].Value;
			}
			return value;
		}

		private TDbVendor GuessDbType()
		{
			if (this.dialect.ContainsIgnoreCase("PostgreSQL") || SqlProcessor.postGresLimitClause.IsMatch(this.rawSql))
			{
				return TDbVendor.DbVMysql;
			}
			if (this.dialect.ContainsIgnoreCase("mySql") || SqlProcessor.mySqlQuoted.IsMatch(this.rawSql) || SqlProcessor.mySqlLimit.IsMatch(this.rawSql))
			{
				return TDbVendor.DbVMysql;
			}
			if (this.dialect.ContainsIgnoreCase("Oracle") || SqlProcessor.oracleJoin.IsMatch(this.rawSql) || SqlProcessor.oracleParam.IsMatch(this.rawSql))
			{
				return TDbVendor.DbVOracle;
			}
			if (this.dialect.ContainsIgnoreCase("DB2"))
			{
				return TDbVendor.DbVDB2;
			}
			return TDbVendor.DbVMssql;
		}

		private string PostProcessSql(string formattedSql, string sql)
		{
			formattedSql = this.VendorSpecificPostProcessing(sql, formattedSql);
			foreach (KeyValuePair<string, string> replacement in this.replacements)
			{
				formattedSql = formattedSql.Replace(replacement.Key, replacement.Value);
			}
			return formattedSql;
		}

		private string PreProcessSql()
		{
			return this.rawSql;
		}

		public SqlStatementProcessingResult ProcessSql()
		{
			SqlStatementProcessingResult sqlStatementProcessingResult = new SqlStatementProcessingResult();
			try
			{
				this.dbVendor = this.GuessDbType();
				string str = this.PreProcessSql();
				if (str.StartsWith("SQL Batch:", StringComparison.OrdinalIgnoreCase))
				{
					str = str.Substring("SQL Batch:".Length);
				}
				if (str.Length <= 150000)
				{
					TGSqlParser tGSqlParser = new TGSqlParser(this.dbVendor);
					tGSqlParser.SqlText.Text = str;
					TGSqlParser tGSqlParser1 = tGSqlParser;
					HashSet<string> strs = new HashSet<string>();
					if (tGSqlParser1.PrettyPrint() != 0)
					{
						sqlStatementProcessingResult.ShortSql = this.rawSql.First50Characters();
						sqlStatementProcessingResult.Errors = tGSqlParser1.ErrorMessages;
						try
						{
							sqlStatementProcessingResult.Sql = (new CrudeSqlFormatter()).Format(this.rawSql);
						}
						catch (Exception exception)
						{
							sqlStatementProcessingResult.Sql = this.rawSql;
						}
						sqlStatementProcessingResult.Tables = new string[0];
					}
					else
					{
						sqlStatementProcessingResult.Sql = this.PostProcessSql(tGSqlParser1.FormattedSqlText.Text.Trim(), this.rawSql);
						string str1 = (str.Length < 10000 ? this.GetShortSql(tGSqlParser1.SqlStatements) : this.GetShortSql(str));
						sqlStatementProcessingResult.ShortSql = this.PostProcessSql(str1.Trim(), this.rawSql);
						sqlStatementProcessingResult.SubStatement = tGSqlParser1.SqlStatements;
						foreach (TCustomSqlStatement sqlStatement in tGSqlParser1.SqlStatements)
						{
							if (sqlStatement.Tables == null)
							{
								continue;
							}
							foreach (TLzTable table in sqlStatement.Tables)
							{
								strs.Add((table.Name.StartsWith("(") ? SqlProcessor.GetTableNameFromNestingStatement(table.Name) : table.Name));
							}
						}
					}
					sqlStatementProcessingResult.Tables = strs.ToArray<string>();
				}
				else
				{
					string shortSql = this.GetShortSql(str);
					sqlStatementProcessingResult.ShortSql = this.PostProcessSql(shortSql.Trim(), this.rawSql);
					sqlStatementProcessingResult.Sql = (new CrudeSqlFormatter()).Format(str);
					return sqlStatementProcessingResult;
				}
			}
			catch (Exception exception3)
			{
				Exception exception1 = exception3;
				try
				{
					sqlStatementProcessingResult.Sql = (new CrudeSqlFormatter()).Format(this.rawSql);
				}
				catch (Exception exception2)
				{
					sqlStatementProcessingResult.Sql = this.rawSql;
				}
				sqlStatementProcessingResult.ShortSql = this.rawSql.First50Characters();
				sqlStatementProcessingResult.Errors = exception1.ToString();
				if (sqlStatementProcessingResult.Tables == null)
				{
					sqlStatementProcessingResult.Tables = new string[0];
				}
			}
			return sqlStatementProcessingResult;
		}

		private string VendorSpecificPostProcessing(string sql, string formattedSql)
		{
			if (this.dbVendor != TDbVendor.DbVOracle)
			{
				return formattedSql;
			}
			foreach (Match match in SqlProcessor.oracleParam.Matches(sql))
			{
				if (!match.Success)
				{
					continue;
				}
				string value = match.Groups[1].Value;
				formattedSql = formattedSql.Replace(string.Concat("? ", value), string.Concat("?", value));
			}
			return formattedSql;
		}
	}
}