using HibernatingRhinos.Profiler.Appender.Util;
using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd
{
	public abstract class AbstractStatementProcessor : AbstractItemProcessor
	{
		private static IEnumerable<string> allAlerts;

		public static IEnumerable<string> AllAlerts
		{
			get
			{
				if (AbstractStatementProcessor.allAlerts == null)
				{
					AbstractStatementProcessor.allAlerts = (
						from x in typeof(ProfilerConfiguration).Assembly.GetLoadableTypes()
						where typeof(AbstractStatementProcessor).IsAssignableFrom(x)
						where !x.IsAbstract
						select x).Select<Type, string>(new Func<Type, string>(AbstractItemProcessor.StatementTypeToName));
				}
				return AbstractStatementProcessor.allAlerts;
			}
		}

		protected AbstractStatementProcessor()
		{
		}

		public abstract void AfterAttachingToSession(Session session, Statement statement);
	}
}