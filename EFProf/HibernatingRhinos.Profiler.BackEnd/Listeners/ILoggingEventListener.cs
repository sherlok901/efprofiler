using System;
using System.Net;

namespace HibernatingRhinos.Profiler.BackEnd.Listeners
{
	public interface ILoggingEventListener
	{
		void Clear(bool createEmptyFile);

		IDisposable Connect(IPEndPoint endPoint, string productionPassword, Action<string, bool> handleConnectionStatus, Action reconnectDialog);

		void CopyTo(string fileToCopyTo);

		IDisposable Start(int port);
	}
}