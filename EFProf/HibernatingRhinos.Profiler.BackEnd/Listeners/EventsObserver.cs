using HibernatingRhinos.Profiler.Appender;
using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.BackEnd.Messages;
using log4net;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Reactive.Disposables;
using System.Runtime.CompilerServices;
using System.Threading;

namespace HibernatingRhinos.Profiler.BackEnd.Listeners
{
	public class EventsObserver : IObservable<object>, IDisposable
	{
		private readonly static ILog Log;

		private readonly ConcurrentDictionary<IObserver<object>, object> observers = new ConcurrentDictionary<IObserver<object>, object>();

		private long lastTicks;

		private int currentlyProcessing;

		private volatile bool pause;

		static EventsObserver()
		{
			EventsObserver.Log = LogManager.GetLogger(typeof(EventsObserver));
		}

		public EventsObserver()
		{
		}

		public void Dispose()
		{
			foreach (KeyValuePair<IObserver<object>, object> observer in this.observers)
			{
				using (IDisposable key = observer.Key as IDisposable)
				{
					using (IDisposable value = observer.Value as IDisposable)
					{
					}
				}
			}
		}

		public void Pause()
		{
			this.pause = true;
		}

		public void Publish(LoggingEventMessage message)
		{
			string str;
			LoggingEvent loggingEvent = new LoggingEvent()
			{
				Message = message.Message,
				Logger = message.Logger
			};
			SessionIdHolder sessionIdHolder = new SessionIdHolder()
			{
				Timestamp = message.Date,
				SessionId = message.SessionId,
				ThreadId = message.ThreadId,
				ScopeName = message.ScopeName
			};
			loggingEvent.SessionId = sessionIdHolder;
			loggingEvent.StackTrace = message.StackTrace;
			loggingEvent.Url = message.Url;
			loggingEvent.Level = ProtocolBuffersMessageStreamPublisher.FindLevel(message.Level);
			loggingEvent.ThreadContext = message.ThreadContext;
			LoggingEvent loggingEvent1 = loggingEvent;
			if (message.Exception == null)
			{
				str = null;
			}
			else
			{
				str = message.Exception.ToString();
			}
			loggingEvent1.Exception = str;
			loggingEvent.DbDialect = message.DbDialect;
			loggingEvent.Culture = message.Culture;
			loggingEvent.StarColor = message.StarColor;
			this.Publish(loggingEvent);
		}

		public void Publish(object item)
		{
			if (this.pause)
			{
				return;
			}
			try
			{
				Interlocked.Increment(ref this.currentlyProcessing);
				foreach (KeyValuePair<IObserver<object>, object> observer in this.observers)
				{
					try
					{
						observer.Key.OnNext(item);
					}
					catch (Exception exception)
					{
						string str = string.Concat("Error when processing messages: ", Environment.NewLine, exception);
						EventsObserver.Log.Fatal(str);
						throw;
					}
				}
			}
			finally
			{
				DateTime utcNow = DateTime.UtcNow;
				Interlocked.Exchange(ref this.lastTicks, utcNow.Ticks);
				Interlocked.Decrement(ref this.currentlyProcessing);
			}
		}

		public void Resume()
		{
			this.pause = false;
		}

		public IDisposable Subscribe(IObserver<object> observer)
		{
			object obj;
			this.observers.TryAdd(observer, null);
			return Disposable.Create(() => this.observers.TryRemove(observer, out obj));
		}

		public void WaitForAllMessages(HibernatingRhinos.Profiler.BackEnd.Bridge.CancellationToken cancellationToken = null)
		{
			do
			{
				if (cancellationToken != null && cancellationToken.IsCancelled)
				{
					return;
				}
				Thread.Sleep(100);
			}
			while (this.currentlyProcessing > 0 || (DateTime.UtcNow - new DateTime(this.lastTicks)).TotalMilliseconds < 250);
		}
	}
}