using System;

namespace HibernatingRhinos.Profiler.BackEnd.Listeners
{
	public interface IDisposableObservable<T> : IDisposable, IObservable<T>
	{

	}
}