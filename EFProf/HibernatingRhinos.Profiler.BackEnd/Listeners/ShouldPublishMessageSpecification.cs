using HibernatingRhinos.Profiler.Appender.Messages;
using System;
using System.Collections.Generic;
using System.Threading;

namespace HibernatingRhinos.Profiler.BackEnd.Listeners
{
	public class ShouldPublishMessageSpecification
	{
		private readonly LinkedList<string> leastRecentlyUsedIds = new LinkedList<string>();

		private readonly HashSet<string> publishedMessages = new HashSet<string>();

		private readonly ReaderWriterLockSlim locker = new ReaderWriterLockSlim();

		public ShouldPublishMessageSpecification()
		{
		}

		[CLSCompliant(false)]
		public void Published(MessageWrapper wrapper)
		{
			this.locker.EnterWriteLock();
			try
			{
				this.leastRecentlyUsedIds.AddLast(wrapper.MessageId);
				if (this.leastRecentlyUsedIds.Count > 10000)
				{
					string value = this.leastRecentlyUsedIds.First.Value;
					this.leastRecentlyUsedIds.RemoveFirst();
					this.publishedMessages.Remove(value);
				}
				this.publishedMessages.Add(wrapper.MessageId);
			}
			finally
			{
				this.locker.ExitWriteLock();
			}
		}

		[CLSCompliant(false)]
		public bool ShouldPublish(MessageWrapper wrapper)
		{
			bool flag;
			this.locker.EnterReadLock();
			try
			{
				flag = !this.publishedMessages.Contains(wrapper.MessageId);
			}
			finally
			{
				this.locker.ExitReadLock();
			}
			return flag;
		}
	}
}