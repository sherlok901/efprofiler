using Google.ProtocolBuffers;
using HibernatingRhinos.Profiler.Appender;
using HibernatingRhinos.Profiler.Appender.Channels;
using HibernatingRhinos.Profiler.Appender.Channels.SSL;
using HibernatingRhinos.Profiler.Appender.Messages;
using HibernatingRhinos.Profiler.Appender.StackTraces;
using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.BackEnd.Messages;
using HibernatingRhinos.Profiler.BackEnd.Util;
using log4net.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Security;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace HibernatingRhinos.Profiler.BackEnd.Listeners
{
	public class ProtocolBuffersMessageStreamPublisher
	{
		private readonly StreamProvider streamProvider;

		private readonly Action<MessageWrapper> onMessage;

		private readonly ShouldPublishMessageSpecification shouldPublishMessageSpecification;

		private readonly IDictionary<string, int> lastStatisticsHash = new Dictionary<string, int>();

		private readonly EventsObserver eventsObserver;

		private readonly static IDictionary<int, log4net.Core.Level> LevelsByValue;

		private readonly static IDictionary<int, log4net.Core.Level> JavaLevelsByValue;

		public Action<string, bool> HandleConnectionStatus
		{
			get;
			set;
		}

		public bool IsProductionProfiling
		{
			get;
			set;
		}

		public string ProductionPassword
		{
			get;
			set;
		}

		static ProtocolBuffersMessageStreamPublisher()
		{
			ProtocolBuffersMessageStreamPublisher.LevelsByValue = new Dictionary<int, log4net.Core.Level>((
				from field in (IEnumerable<FieldInfo>)typeof(log4net.Core.Level).GetFields(BindingFlags.Static | BindingFlags.Public)
				let level = (log4net.Core.Level)field.GetValue(null)
				group level by level.Value into g
				select g.First<log4net.Core.Level>()).ToDictionary<log4net.Core.Level, int>((log4net.Core.Level x) => x.Value));
			Dictionary<int, log4net.Core.Level> nums = new Dictionary<int, log4net.Core.Level>()
			{
				{ -50000, log4net.Core.Level.Fatal },
				{ -40000, log4net.Core.Level.Error },
				{ -30000, log4net.Core.Level.Warn },
				{ -20000, log4net.Core.Level.Info },
				{ -10000, log4net.Core.Level.Debug },
				{ -5000, log4net.Core.Level.Trace }
			};
			ProtocolBuffersMessageStreamPublisher.JavaLevelsByValue = nums;
		}

		[CLSCompliant(false)]
		public ProtocolBuffersMessageStreamPublisher(EventsObserver eventsObserver, StreamProvider streamProvider, Action<MessageWrapper> onMessage, ShouldPublishMessageSpecification shouldPublishMessageSpecification)
		{
			this.eventsObserver = eventsObserver;
			this.shouldPublishMessageSpecification = shouldPublishMessageSpecification;
			this.streamProvider = streamProvider;
			this.onMessage = onMessage;
		}

		public static log4net.Core.Level FindLevel(int level)
		{
			log4net.Core.Level level1;
			ProtocolBuffersMessageStreamPublisher.LevelsByValue.TryGetValue(level, out level1);
			return level1 ?? ProtocolBuffersMessageStreamPublisher.JavaLevelsByValue[level];
		}

		private static DateTime GetDate(HibernatingRhinos.Profiler.Appender.Messages.LoggingEventMessage message)
		{
			return ProfilerChannelHelpers.UnixEpoch.AddMilliseconds((double)message.DateAsMillisecondsSinceUnixEpoch);
		}

		private Hashtable GetDictionary(IEnumerable<StatisticsInformation.Types.KeyValue> items)
		{
			Hashtable hashtables = new Hashtable();
			foreach (StatisticsInformation.Types.KeyValue item in items)
			{
				if (item.ValueCount == 1)
				{
					hashtables[item.Key] = item.ValueList[0];
				}
				else
				{
					hashtables[item.Key] = item.ValueList;
				}
			}
			return hashtables;
		}

		private StackTraceInfo GetStackTrace(HibernatingRhinos.Profiler.Appender.Messages.LoggingEventMessage message)
		{
			if (message.StackTrace == null)
			{
				return null;
			}
			StackTraceInfo stackTraceInfo = new StackTraceInfo()
			{
				Frames = (
					from x in message.StackTrace.FramesList
					select new StackTraceFrame()
					{
						Column = x.Column,
						FullFilename = x.FullFilename,
						Line = x.Line,
						Method = x.Method,
						Namespace = x.Namespace,
						Type = x.Type
					}).ToArray<StackTraceFrame>()
			};
			return stackTraceInfo;
		}

		private void Publish(object loggingEvent)
		{
			this.eventsObserver.Publish(loggingEvent);
		}

		public void PublishMessagesFromStream()
		{
			this.PublishMessagesFromStream(new CancellationToken());
		}

		public void PublishMessagesFromStream(CancellationToken cancellationToken)
		{
			int num;
			if (cancellationToken.IsCancelled)
			{
				return;
			}
			StreamProvider streamProvider = this.streamProvider;
			if (this.IsProductionProfiling)
			{
				if (string.IsNullOrWhiteSpace(this.ProductionPassword))
				{
					this.HandleConnectionStatus("Password cannot be empty", true);
					return;
				}
				Stream stream = streamProvider();
				SslStream sslStream = new SslStream(stream, false, new RemoteCertificateValidationCallback(TcpListenerProfilerChannel.UserCertificateValidationCallback));
				SslStream sslStream1 = sslStream;
				X509Certificate2Collection x509Certificate2Collection = new X509Certificate2Collection();
				x509Certificate2Collection.Add(CertificateHolder.Certificate);
				sslStream1.AuthenticateAsClient("HibernatingRhinos", x509Certificate2Collection, SslProtocols.Default, true);
				BinaryWriter binaryWriter = new BinaryWriter(sslStream, Encoding.UTF8);
				binaryWriter.Write(this.ProductionPassword);
				binaryWriter.Flush();
				stream.Flush();
				string str = (new BinaryReader(sslStream, Encoding.UTF8)).ReadString();
				if (str != "Connection: OK.")
				{
					this.HandleConnectionStatus("Password is not correct", true);
					return;
				}
				this.HandleConnectionStatus(str, false);
				streamProvider = () => sslStream;
			}
			FileStream fileStream = streamProvider() as FileStream;
			foreach (MessageWrapper messageWrapper in MessageStreamIterator<MessageWrapper>.FromStreamProvider(streamProvider))
			{
				if (fileStream != null)
				{
					cancellationToken.CurrentStremPosition = fileStream.Position;
				}
				if (!cancellationToken.IsCancelled)
				{
					cancellationToken.AddLoadedMessage();
					if (!this.shouldPublishMessageSpecification.ShouldPublish(messageWrapper))
					{
						continue;
					}
					this.shouldPublishMessageSpecification.Published(messageWrapper);
					this.onMessage(messageWrapper);
					switch (messageWrapper.Type)
					{
						case MessageWrapper.Types.MessageType.LogEvent:
						{
							HibernatingRhinos.Profiler.Appender.Messages.LoggingEventMessage message = messageWrapper.Message;
							HibernatingRhinos.Profiler.BackEnd.Messages.LoggingEvent loggingEvent = new HibernatingRhinos.Profiler.BackEnd.Messages.LoggingEvent()
							{
								Message = message.Message,
								Logger = message.Logger
							};
							SessionIdHolder sessionIdHolder = new SessionIdHolder()
							{
								Timestamp = ProtocolBuffersMessageStreamPublisher.GetDate(message),
								SessionId = message.SessionId,
								ThreadId = message.ThreadId,
								ScopeName = message.ScopeName
							};
							loggingEvent.SessionId = sessionIdHolder;
							loggingEvent.StackTrace = this.GetStackTrace(message);
							loggingEvent.Url = message.Url;
							loggingEvent.Level = ProtocolBuffersMessageStreamPublisher.FindLevel(message.Level);
							loggingEvent.ThreadContext = message.ThreadContext;
							loggingEvent.Exception = message.Exception;
							loggingEvent.DbDialect = message.DbDialect;
							loggingEvent.Culture = message.Culture;
							loggingEvent.StarColor = message.StarColor;
							this.Publish(loggingEvent);
							continue;
						}
						case MessageWrapper.Types.MessageType.Statistics:
						{
							StatisticsInformation stats = messageWrapper.Stats;
							SessionFactoryStats sessionFactoryStat = new SessionFactoryStats()
							{
								Name = stats.Name,
								Statistics = this.GetDictionary(stats.ItemsList)
							};
							SessionFactoryStats sessionFactoryStat1 = sessionFactoryStat;
							int hashCode = sessionFactoryStat1.GetHashCode();
							if (this.lastStatisticsHash.TryGetValue(sessionFactoryStat1.Name, out num) && num == hashCode)
							{
								continue;
							}
							this.lastStatisticsHash[sessionFactoryStat1.Name] = hashCode;
							this.Publish(sessionFactoryStat1);
							continue;
						}
						case MessageWrapper.Types.MessageType.BringApplicationToFront:
						{
							this.Publish(new BringApplicationToFront()
							{
								ContextFile = messageWrapper.ContextFile
							});
							continue;
						}
						case MessageWrapper.Types.MessageType.ShutdownAndOutputReport:
						{
							this.Publish(new ShutdownAndOutputReport());
							continue;
						}
						case MessageWrapper.Types.MessageType.ApplicationAttached:
						{
							Guid guid = new Guid(messageWrapper.Application.Id);
							string name = messageWrapper.Application.Name;
							LocalStorage.AttachedApplicationId = guid;
							ApplicationAttached applicationAttached = new ApplicationAttached()
							{
								Id = guid,
								Name = name,
								IsProductionApplication = this.IsProductionProfiling
							};
							this.Publish(applicationAttached);
							continue;
						}
					}
					throw new ArgumentOutOfRangeException(string.Concat("Cannot understand message type ", messageWrapper.Type));
				}
				else
				{
					return;
				}
			}
		}
	}
}