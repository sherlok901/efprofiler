using Google.ProtocolBuffers;
using HibernatingRhinos.Profiler.Appender.Messages;
using HibernatingRhinos.Profiler.Appender.Util;
using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.Messages;
using HibernatingRhinos.Profiler.BackEnd.Util;
using log4net;
using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Threading;

namespace HibernatingRhinos.Profiler.BackEnd.Listeners
{
	public class ProtocolBuffersLoggingEventListener : ILoggingEventListener
	{
		private readonly ILog logger = LogManager.GetLogger(typeof(ProtocolBuffersLoggingEventListener));

		private readonly object copiedStreamLock = new object();

		private TcpListener listener;

		private Stream copiedStream;

		private readonly EventsObserver eventsObserver;

		private string copiedMessagesPath;

		private MessageStreamWriter<MessageWrapper> writer;

		private readonly ShouldPublishMessageSpecification shouldPublishMessageSpecification = new ShouldPublishMessageSpecification();

		private readonly ProfilerConfiguration profilerConfiguration;

		public ProtocolBuffersLoggingEventListener(EventsObserver eventsObserver, ProfilerConfiguration configuration)
		{
			this.eventsObserver = eventsObserver;
			this.profilerConfiguration = configuration;
		}

		private void BeginAcceptTcpClient(IAsyncResult ar)
		{
			try
			{
				this.listener.BeginAcceptTcpClient(new AsyncCallback(this.BeginAcceptTcpClient), null);
			}
			catch (Exception exception)
			{
			}
			try
			{
				TcpClient tcpClient = this.listener.EndAcceptTcpClient(ar);
				this.CreateThreadToReadFromRemoteSocket(tcpClient, false, null, null, null);
			}
			catch (ObjectDisposedException objectDisposedException)
			{
			}
			catch (Exception exception1)
			{
				this.logger.Info("Error when trying to accept a connection from profiled application", exception1);
			}
		}

		public void Clear(bool createEmptyFile)
		{
			lock (this.copiedStreamLock)
			{
				if (this.copiedStream != null)
				{
					this.copiedStream.Dispose();
				}
				if (this.copiedMessagesPath != null)
				{
					try
					{
						File.Delete(this.copiedMessagesPath);
					}
					catch (IOException oException)
					{
					}
				}
				if (createEmptyFile)
				{
					this.CreateCopiedStream();
				}
			}
		}

		public IDisposable Connect(IPEndPoint endPoint, string productionPassword, Action<string, bool> handleConnectionStatus, Action reconnectDialog)
		{
			this.CreateCopiedStream();
			TcpClient tcpClient = new TcpClient();
			tcpClient.Connect(endPoint);
			this.CreateThreadToReadFromRemoteSocket(tcpClient, true, productionPassword, handleConnectionStatus, reconnectDialog);
			return new DisposableAction(() => {
				if (tcpClient.Client != null)
				{
					tcpClient.Client.Close();
				}
				tcpClient.Close();
				this.Clear(true);
			});
		}

		public void CopyTo(string fileToCopyTo)
		{
			lock (this.copiedStreamLock)
			{
				this.writer.Flush();
				this.copiedStream.Flush();
				this.copiedStream.Dispose();
				File.Delete(fileToCopyTo);
				File.Move(this.copiedMessagesPath, fileToCopyTo);
				this.CreateCopiedStream();
			}
		}

		private void CreateCopiedStream()
		{
			this.copiedMessagesPath = this.profilerConfiguration.GetTempFileName();
			this.copiedStream = File.Create(this.copiedMessagesPath);
			this.writer = new MessageStreamWriter<MessageWrapper>(this.copiedStream);
		}

		private void CreateThreadToReadFromRemoteSocket(TcpClient client, bool isProductionProfiling, string productionPassword, Action<string, bool> handleConnectionStatus, Action reconnectDialog)
		{
			Thread thread = new Thread(() => {
				try
				{
					using (client)
					{
						this.HandleConnection(client, isProductionProfiling, productionPassword, handleConnectionStatus, reconnectDialog);
					}
				}
				catch (Exception exception)
				{
					this.logger.Error("Error when reading messages from profiled application", exception);
				}
			})
			{
				Name = string.Concat("Profiler - Listening to ", client.Client.RemoteEndPoint),
				IsBackground = true
			};
			thread.Start();
		}

		private void HandleClosedConnection()
		{
			this.eventsObserver.Publish(new ApplicationDetached(LocalStorage.AttachedApplicationId));
		}

		private void HandleConnection(TcpClient tcpClient, bool isProductionProfiling, string productionPassword, Action<string, bool> handleConnectionStatus, Action reconnectDialog)
		{
			try
			{
				try
				{
					ProtocolBuffersMessageStreamPublisher protocolBuffersMessageStreamPublisher = new ProtocolBuffersMessageStreamPublisher(this.eventsObserver, new StreamProvider(tcpClient.GetStream), (MessageWrapper wrapper) => {
						try
						{
							lock (this.copiedStreamLock)
							{
								if (this.copiedStream.Length > (long)1073741824)
								{
									this.Clear(true);
								}
								this.writer.Write(wrapper);
							}
						}
						catch (Exception exception)
						{
						}
					}, this.shouldPublishMessageSpecification)
					{
						IsProductionProfiling = isProductionProfiling,
						ProductionPassword = productionPassword,
						HandleConnectionStatus = handleConnectionStatus
					};
					protocolBuffersMessageStreamPublisher.PublishMessagesFromStream();
				}
				catch (ObjectDisposedException objectDisposedException)
				{
				}
				catch (IOException oException)
				{
					if (reconnectDialog != null)
					{
						reconnectDialog();
					}
				}
				catch (SocketException socketException)
				{
				}
				catch (UninitializedMessageException uninitializedMessageException)
				{
				}
			}
			finally
			{
				this.HandleClosedConnection();
			}
		}

		public IDisposable Start(int port)
		{
			this.listener = new TcpListener(IPAddress.Any, port);
			this.listener.Start();
			this.CreateCopiedStream();
			this.listener.BeginAcceptTcpClient(new AsyncCallback(this.BeginAcceptTcpClient), null);
			return new DisposableAction(() => {
				this.listener.Stop();
				this.Clear(true);
			});
		}
	}
}