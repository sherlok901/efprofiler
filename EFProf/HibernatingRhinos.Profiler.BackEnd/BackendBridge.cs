using Google.ProtocolBuffers;
using HibernatingRhinos.Profiler.Appender.Messages;
using HibernatingRhinos.Profiler.Appender.Util;
using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.BackEnd.Exceptions;
using HibernatingRhinos.Profiler.BackEnd.Infrastructure;
using HibernatingRhinos.Profiler.BackEnd.Listeners;
using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using HibernatingRhinos.Profiler.BackEnd.Reports;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace HibernatingRhinos.Profiler.BackEnd
{
	public class BackendBridge : IBackendBridge, IDisposable
	{
		private readonly IDictionary<Guid, CancellationToken> cancellationTokens = new Dictionary<Guid, CancellationToken>();

		private readonly IDictionary<Guid, IDisposable> disposables = new Dictionary<Guid, IDisposable>();

		private bool wasInitialized;

		public readonly HibernatingRhinos.Profiler.BackEnd.Listeners.EventsObserver EventsObserver = new HibernatingRhinos.Profiler.BackEnd.Listeners.EventsObserver();

		private ILoggingEventListener loggingEventListener;

		private IDisposable listeningStopAction;

		private ProfilerConfiguration configuration;

		public ProfilerConfiguration Configuration
		{
			get
			{
				ProfilerConfiguration profilerConfiguration = this.configuration;
				if (profilerConfiguration == null)
				{
					ProfilerConfiguration profilerConfiguration1 = new ProfilerConfiguration();
					ProfilerConfiguration profilerConfiguration2 = profilerConfiguration1;
					this.configuration = profilerConfiguration1;
					profilerConfiguration = profilerConfiguration2;
				}
				return profilerConfiguration;
			}
			set
			{
				this.configuration = value;
			}
		}

		public List<HibernatingRhinos.Profiler.BackEnd.Infrastructure.ConnectionInfo> ConnectionInfo
		{
			get;
			set;
		}

		public ReactiveModelBuilder ModelBuilder
		{
			get;
			private set;
		}

		public NotificationsBridge Notifications
		{
			get
			{
				return this.ModelBuilder.Notifications;
			}
		}

		public ReportsBridge Reports
		{
			get
			{
				return this.ModelBuilder.Reports;
			}
		}

		public SchemaCache TablesSchema
		{
			get;
			private set;
		}

		public BackendBridge()
		{
		}

		public void CancelLoadFileStatus(Guid loadFileId)
		{
			CancellationToken cancellationToken;
			if (!this.cancellationTokens.TryGetValue(loadFileId, out cancellationToken))
			{
				return;
			}
			cancellationToken.Cancel();
			this.cancellationTokens.Remove(loadFileId);
		}

		public void Clear()
		{
			this.loggingEventListener.Clear(true);
			this.ModelBuilder.Clear();
		}

		public void ClearExcept(IList<Guid> sessionIds)
		{
			this.ModelBuilder.ClearExcept(sessionIds);
		}

		public Guid Connect(IPEndPoint endPoint, string productionPassword, Action<string, bool> handleConnectionStatus, Action reconnectDialog)
		{
			if (!this.wasInitialized)
			{
				throw new InvalidOperationException("Cannot connect to remote endpoint before bridge initialization");
			}
			IDisposable disposable = this.loggingEventListener.Connect(endPoint, productionPassword, handleConnectionStatus, reconnectDialog);
			Guid guid = Guid.NewGuid();
			this.disposables.Add(guid, disposable);
			return guid;
		}

		public void DeleteTempFile()
		{
			this.loggingEventListener.Clear(false);
		}

		public void Dispose()
		{
			if (this.listeningStopAction != null)
			{
				this.listeningStopAction.Dispose();
			}
			this.EventsObserver.Dispose();
		}

		public void DisposeOf(Guid guid)
		{
			IDisposable disposable;
			if (!this.disposables.TryGetValue(guid, out disposable))
			{
				return;
			}
			disposable.Dispose();
		}

		public LoadFileStatus GetLoadFileStatus(Guid loadFileId)
		{
			CancellationToken cancellationToken;
			string message;
			if (!this.cancellationTokens.TryGetValue(loadFileId, out cancellationToken))
			{
				return new LoadFileStatus()
				{
					Complete = true
				};
			}
			bool flag = (cancellationToken.IsCancelled ? true : cancellationToken.WasCompleted);
			if (flag)
			{
				this.cancellationTokens.Remove(loadFileId);
			}
			int currentStremPosition = 100;
			if (cancellationToken.StreamLength > (long)0)
			{
				currentStremPosition = (int)(cancellationToken.CurrentStremPosition * (long)100 / cancellationToken.StreamLength);
			}
			LoadFileStatus loadFileStatu = new LoadFileStatus()
			{
				Complete = flag
			};
			LoadFileStatus loadFileStatu1 = loadFileStatu;
			if (cancellationToken.Exception != null)
			{
				message = cancellationToken.Exception.Message;
			}
			else
			{
				message = null;
			}
			loadFileStatu1.ExceptionMessage = message;
			loadFileStatu.NumberOfLoadedMessages = cancellationToken.NumberOfLoadedMessages;
			loadFileStatu.PercentLoaded = currentStremPosition;
			return loadFileStatu;
		}

		public void Initialize()
		{
			this.loggingEventListener = new ProtocolBuffersLoggingEventListener(this.EventsObserver, this.Configuration);
			this.TablesSchema = new SchemaCache();
			ReactiveModelBuilder reactiveModelBuilder = new ReactiveModelBuilder()
			{
				Configuration = this.Configuration,
				ConnectionInfo = this.ConnectionInfo,
				TablesSchema = this.TablesSchema
			};
			this.ModelBuilder = reactiveModelBuilder;
			this.ModelBuilder.Attach(this.EventsObserver);
			if (this.ModelBuilder.MessageProcessors == null)
			{
				throw new InvalidOperationException("MessageProcessors is not initialized.");
			}
			this.wasInitialized = true;
		}

		public void Pause()
		{
			this.EventsObserver.Pause();
		}

		public void Resume()
		{
			this.EventsObserver.Resume();
		}

		public void SaveToFile(string filename)
		{
			this.loggingEventListener.CopyTo(filename);
		}

		public Guid Start(int port, string contextFile = null)
		{
			this.Initialize();
			try
			{
				this.listeningStopAction = this.loggingEventListener.Start(port);
			}
			catch (SocketException socketException)
			{
				if (socketException.ErrorCode == 10048)
				{
					try
					{
						using (TcpClient tcpClient = new TcpClient())
						{
							tcpClient.Connect(IPAddress.Loopback, port);
							using (NetworkStream stream = tcpClient.GetStream())
							{
								MessageStreamWriter<MessageWrapper> messageStreamWriter = new MessageStreamWriter<MessageWrapper>(stream);
								MessageStreamWriter<MessageWrapper> messageStreamWriter1 = messageStreamWriter;
								MessageWrapper.Builder builder = new MessageWrapper.Builder()
								{
									MessageId = Guid.NewGuid().ToString(),
									ContextFile = contextFile ?? "",
									Type = MessageWrapper.Types.MessageType.BringApplicationToFront
								};
								messageStreamWriter1.Write(builder.Build());
								messageStreamWriter.Flush();
								stream.Flush();
							}
						}
					}
					catch (Exception exception)
					{
						throw new CouldNotActivateAnotherApplicationInstanceException("Failed to activate another instance of the profiler", exception);
					}
					throw new AnotherApplicationIsAlreadyRunningAndControlWasMovedToItException("Another application is already running, control was moved to it");
				}
				throw;
			}
			DisposableAction disposableAction = new DisposableAction(() => {
				if (this.listeningStopAction != null)
				{
					this.listeningStopAction.Dispose();
				}
				this.listeningStopAction = null;
			});
			Guid guid = Guid.NewGuid();
			this.disposables.Add(guid, disposableAction);
			return guid;
		}

		public Guid StartLoadFromFile(string filename)
		{
			Action<MessageWrapper> action = null;
			CancellationToken cancellationToken = new CancellationToken();
			Guid guid = Guid.NewGuid();
			this.cancellationTokens.Add(guid, cancellationToken);
			Task.Factory.StartNew(() => {
				try
				{
					try
					{
						using (FileStream fileStream = File.OpenRead(filename))
						{
							fileStream.Position = (long)0;
							cancellationToken.StreamLength = fileStream.Length;
							HibernatingRhinos.Profiler.BackEnd.Listeners.EventsObserver eventsObserver = this.EventsObserver;
							StreamProvider streamProvider = () => fileStream;
							if (action == null)
							{
								action = (MessageWrapper wrapper) => {
								};
							}
							(new ProtocolBuffersMessageStreamPublisher(eventsObserver, streamProvider, action, new ShouldPublishMessageSpecification())).PublishMessagesFromStream(cancellationToken);
						}
					}
					catch (Exception exception)
					{
						cancellationToken.Exception = exception;
					}
				}
				finally
				{
					this.EventsObserver.WaitForAllMessages(cancellationToken);
					cancellationToken.WasCompleted = true;
				}
			});
			return guid;
		}
	}
}