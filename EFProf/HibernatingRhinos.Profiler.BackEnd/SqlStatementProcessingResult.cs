using System;
using System.Runtime.CompilerServices;

namespace HibernatingRhinos.Profiler.BackEnd
{
	public class SqlStatementProcessingResult
	{
		public string Errors
		{
			get;
			set;
		}

		public string ShortSql
		{
			get;
			set;
		}

		public string Sql
		{
			get;
			set;
		}

		public object SubStatement
		{
			get;
			set;
		}

		public string[] Tables
		{
			get;
			set;
		}

		public SqlStatementProcessingResult()
		{
			this.Tables = new string[0];
		}
	}
}