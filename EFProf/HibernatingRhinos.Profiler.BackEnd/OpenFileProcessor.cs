using EnvDTE;
using HibernatingRhinos.Profiler.Appender.StackTraces;
using log4net;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HibernatingRhinos.Profiler.BackEnd
{
	public class OpenFileProcessor
	{
		private readonly static ILog Log;

		static OpenFileProcessor()
		{
			OpenFileProcessor.Log = LogManager.GetLogger(typeof(OpenFileProcessor));
		}

		public OpenFileProcessor()
		{
		}

		[DllImport("ole32.dll", CharSet=CharSet.None, ExactSpelling=false)]
		private static extern int CreateBindCtx(int reserved, out IBindCtx ppbc);

		private static DTE CreateVisualStudioInstance(string pathToFile)
		{
			System.Diagnostics.Process process;
			try
			{
				OpenFileProcessor.Log.Debug("Trying to start a new instance of devenv");
				process = System.Diagnostics.Process.Start("devenv", pathToFile);
			}
			catch (Exception exception1)
			{
				try
				{
					OpenFileProcessor.Log.DebugFormat("Trying to open the file '{0}' directly", pathToFile);
					process = System.Diagnostics.Process.Start(pathToFile);
				}
				catch (Exception exception)
				{
					OpenFileProcessor.Log.WarnFormat("Could not open {0}", pathToFile);
					return null;
				}
			}
			if (process != null)
			{
				process.WaitForInputIdle(5000);
			}
			return OpenFileProcessor.FindSuitableVisualStudioInstance(pathToFile);
		}

		private static DTE FindSuitableVisualStudioInstance(string fileName)
		{
			DTE dTE;
			IList<DTE> dTEs = OpenFileProcessor.FindVisualStudioInstances();
			foreach (DTE dTE1 in dTEs)
			{
                //if (!dTE1["{FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF}", fileName])
                //{
                //	continue;
                //}
                //dTE = dTE1;

                //igor, from dotpeak
                if (dTE1.IsOpenFile["{FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF}", fileName])
                    return dTE1;
                //=igor
			}
			OpenFileProcessor.Log.Debug("File is not opened yet so I'm going to scan solutions");
			using (IEnumerator<DTE> enumerator = dTEs.GetEnumerator())
			{
				while (enumerator.MoveNext())
				{
					DTE current = enumerator.Current;
					if (!OpenFileProcessor.IsVisualStudioSuitableToShowFile(current, fileName))
					{
						continue;
					}
					dTE = current;
					return dTE;
				}
				if (dTEs.Count <= 0)
				{
					return null;
				}
				OpenFileProcessor.Log.Debug("I've found any suitable VS instance so I'm returning first on list");
				return dTEs[0];
			}
			return dTE;
		}

		private static IList<DTE> FindVisualStudioInstances()
		{
			IBindCtx bindCtx;
			IRunningObjectTable runningObjectTable;
			IEnumMoniker enumMoniker;
			string str;
			object obj;
			IMoniker[] monikerArray = new IMoniker[1];
			IntPtr zero = IntPtr.Zero;
			IList<DTE> dTEs = new List<DTE>();
			OpenFileProcessor.CreateBindCtx(0, out bindCtx);
			bindCtx.GetRunningObjectTable(out runningObjectTable);
			runningObjectTable.EnumRunning(out enumMoniker);
			enumMoniker.Reset();
			while (enumMoniker.Next(1, monikerArray, zero) == 0)
			{
				monikerArray[0].GetDisplayName(bindCtx, null, out str);
				runningObjectTable.GetObject(monikerArray[0], out obj);
				DTE dTE = obj as DTE;
				if (dTE == null || !str.StartsWith("!VisualStudio.DTE"))
				{
					continue;
				}
				dTEs.Add(dTE);
			}
			return dTEs;
		}

		[DllImport("user32.dll", CharSet=CharSet.None, ExactSpelling=false)]
		private static extern IntPtr GetForegroundWindow();

		private static bool IsProjectSuitableToShowFile(Project project, string fileName)
		{
            if (project.ProjectItems == null)
            {
                OpenFileProcessor.Log.Debug((object)("ProjectItems value is null. Filename is: " + fileName));
                return false;
            }
            foreach (ProjectItem projectItem in project.ProjectItems)
            {
                if (projectItem.Kind == "{EA6618E8-6E24-4528-94BE-6889FE16485C}")
                {
                    Project project1 = projectItem.Object as Project;
                    if (project1 != null)
                        return OpenFileProcessor.IsProjectSuitableToShowFile(project1, fileName);
                }
                else if (projectItem.Kind == "{6BB5F8EE-4483-11D3-8BCF-00C04F8EC28C}" && projectItem.get_FileNames((short)0) == fileName)
                    return true;
            }
            return false;

            /*
			bool showFile;
			if (project.ProjectItems == null)
			{
				OpenFileProcessor.Log.Debug(string.Concat("ProjectItems value is null. Filename is: ", fileName));
				return false;
			}
			System.Collections.IEnumerator enumerator = project.ProjectItems.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					ProjectItem current = (ProjectItem)enumerator.Current;
					if (current.Kind != "{EA6618E8-6E24-4528-94BE-6889FE16485C}")
					{
						if (!(current.Kind == "{6BB5F8EE-4483-11D3-8BCF-00C04F8EC28C}") || !(current[0] == fileName))
						{
							continue;
						}
						showFile = true;
						return showFile;
					}
					else
					{
						Project obj = current.Object as Project;
						if (obj == null)
						{
							continue;
						}
						showFile = OpenFileProcessor.IsProjectSuitableToShowFile(obj, fileName);
						return showFile;
					}
				}
				return false;
                
			}
			finally
			{
				IDisposable disposable = enumerator as IDisposable;
				if (disposable != null)
				{
					disposable.Dispose();
				}
			}
			return showFile;
            */
        }

        private static bool IsVisualStudioSuitableToShowFile(DTE visualStudio, string fileName)
		{
			bool flag;
			if (visualStudio.Solution == null)
			{
				return false;
			}
			try
			{
				flag = visualStudio.Solution.Projects.Cast<Project>().Any<Project>((Project project) => OpenFileProcessor.IsProjectSuitableToShowFile(project, fileName));
			}
			catch (COMException cOMException)
			{
				OpenFileProcessor.Log.Warn("Error in project scanning.", cOMException);
				return false;
			}
			return flag;
		}

		public static void OpenFile(StackTraceFrame frame, string defaultEditor, string editorPath)
		{
			Task.Factory.StartNew(() => {
				try
				{
					string str = defaultEditor;
					string str1 = str;
					if (str != null)
					{
						if (str1 == "Visual Studio")
						{
							OpenFileProcessor.ShowToUserBackground(frame);
						}
						else if (str1 == "NotePad++")
						{
							OpenFileProcessor.ShowNotePadPP(frame);
						}
						else if (str1 == "Other")
						{
							OpenFileProcessor.ShowOther(frame, editorPath);
						}
					}
				}
				catch (Exception exception)
				{
					OpenFileProcessor.Log.Error(string.Concat("Could not show ", frame.FullFilename, " to the user"), exception);
				}
			});
		}

		[DllImport("user32.dll", CharSet=CharSet.None, ExactSpelling=false)]
		private static extern bool SetForegroundWindow(IntPtr hWnd);

		private static void ShowNotePadPP(StackTraceFrame frame)
		{
			try
			{
				StringBuilder stringBuilder = new StringBuilder();
				stringBuilder.AppendFormat("\"{0}\" -n{1} -c{2}", frame.FullFilename, frame.Line, frame.Column);
				System.Diagnostics.Process.Start("Notepad++.exe", stringBuilder.ToString());
			}
			catch (Exception exception)
			{
				OpenFileProcessor.Log.Warn("Error Open Notepad++.", exception);
				OpenFileProcessor.ShowToUserBackground(frame);
			}
		}

		private static void ShowOther(StackTraceFrame frame, string editorPath)
		{
			try
			{
				System.Diagnostics.Process.Start(string.Concat("\"", editorPath, "\""), frame.FullFilename);
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				OpenFileProcessor.Log.Warn(string.Concat("Error Open ", editorPath, "."), exception);
				OpenFileProcessor.ShowToUserBackground(frame);
			}
		}

		private static void ShowSelection(StackTraceFrame frame, Window window)
		{
			TextSelection selection = (TextSelection)window.Document.Selection;
			selection.MoveTo(frame.Line, frame.Column, false);
			window.DTE.MainWindow.Activate();
			System.Threading.Thread.Sleep(500);
			if (OpenFileProcessor.GetForegroundWindow().ToInt32() != window.HWnd)
			{
				OpenFileProcessor.Log.Warn("Trying to ask nicely from VS to activate has failed, trying to use SetForegroundWindow for the task");
				bool flag = OpenFileProcessor.SetForegroundWindow(new IntPtr(window.HWnd));
				OpenFileProcessor.Log.DebugFormat("SetForegroundWindow returned: {0}", flag);
			}
		}

		private static void ShowToUserBackground(StackTraceFrame frame)
		{
			string fullFilename = frame.FullFilename;
			if (string.IsNullOrEmpty(fullFilename) || !File.Exists(fullFilename))
			{
				return;
			}
			OpenFileProcessor.Log.DebugFormat("Need to display in VS: {0}", fullFilename);
			DTE dTE = OpenFileProcessor.FindSuitableVisualStudioInstance(fullFilename);
			if (dTE == null)
			{
				OpenFileProcessor.Log.Debug("Could not find a running version of VS, starting a new one");
				dTE = OpenFileProcessor.CreateVisualStudioInstance(fullFilename);
				if (dTE == null)
				{
					OpenFileProcessor.Log.Warn("Could not start a new version of VS");
					System.Diagnostics.Process.Start("notepad.exe", fullFilename);
					return;
				}
			}
			Window window = dTE.ItemOperations.OpenFile(fullFilename, "{7651A703-06E5-11D1-8EBD-00A0C90F26EA}");
			OpenFileProcessor.TryReallyHardToShowSelection(frame, window, 0);
		}

		private static void TryReallyHardToShowSelection(StackTraceFrame frame, Window window, int tries)
		{
			try
			{
				OpenFileProcessor.ShowSelection(frame, window);
			}
			catch (COMException cOMException1)
			{
				COMException cOMException = cOMException1;
				if (tries >= 4 || !cOMException.Message.Contains("RPC_E_SERVERCALL_RETRYLATER"))
				{
					throw;
				}
				else
				{
					OpenFileProcessor.Log.Warn("Got RPC_E_SERVERCALL_RETRYLATER error when trying to show item, will retry in 0.5 sec");
					System.Threading.Thread.Sleep(500);
					OpenFileProcessor.TryReallyHardToShowSelection(frame, window, tries + 1);
				}
			}
		}
	}
}