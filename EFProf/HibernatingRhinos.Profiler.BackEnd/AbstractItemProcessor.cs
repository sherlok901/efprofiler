using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;

namespace HibernatingRhinos.Profiler.BackEnd
{
	public abstract class AbstractItemProcessor
	{
		private readonly static Regex PascalCaseToWords;

		private string name;

		public ProfilerConfiguration Configuration
		{
			get;
			set;
		}

		public string Name
		{
			get
			{
				string str = this.name;
				if (str == null)
				{
					string name = AbstractItemProcessor.StatementTypeToName(this.GetType());
					string str1 = name;
					this.name = name;
					str = str1;
				}
				return str;
			}
		}

		static AbstractItemProcessor()
		{
			AbstractItemProcessor.PascalCaseToWords = new Regex("\\B([A-Z])", RegexOptions.Compiled);
		}

		protected AbstractItemProcessor()
		{
		}

		public static string StatementTypeToName(Type type)
		{
			string str = type.Name.Replace("StatementProcessor", "");
			return AbstractItemProcessor.PascalCaseToWords.Replace(str, " $1");
		}
	}
}