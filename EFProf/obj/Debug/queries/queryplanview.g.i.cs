﻿#pragma checksum "..\..\..\queries\queryplanview.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "BA0417725D15F155D8E79DCB8BE56BEB"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Caliburn.Micro;
using HibernatingRhinos.Profiler.Client.Controls;
using HibernatingRhinos.Profiler.Client.Queries;
using Microsoft.Expression.Interactivity.Core;
using Microsoft.Expression.Interactivity.Input;
using Microsoft.Expression.Interactivity.Layout;
using Microsoft.Expression.Interactivity.Media;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace HibernatingRhinos.Profiler.Client.Queries {
    
    
    /// <summary>
    /// QueryPlanView
    /// </summary>
    public partial class QueryPlanView : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 51 "..\..\..\queries\queryplanview.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ScrollViewer rootContainer;
        
        #line default
        #line hidden
        
        
        #line 53 "..\..\..\queries\queryplanview.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid gridContainer;
        
        #line default
        #line hidden
        
        
        #line 57 "..\..\..\queries\queryplanview.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Media.ScaleTransform rootScale;
        
        #line default
        #line hidden
        
        
        #line 61 "..\..\..\queries\queryplanview.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Canvas connectionCanvas;
        
        #line default
        #line hidden
        
        
        #line 63 "..\..\..\queries\queryplanview.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ItemsControl root;
        
        #line default
        #line hidden
        
        
        #line 77 "..\..\..\queries\queryplanview.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid ZoomPanel;
        
        #line default
        #line hidden
        
        
        #line 78 "..\..\..\queries\queryplanview.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button button;
        
        #line default
        #line hidden
        
        
        #line 119 "..\..\..\queries\queryplanview.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border MiniMapContainer;
        
        #line default
        #line hidden
        
        
        #line 121 "..\..\..\queries\queryplanview.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Canvas MiniMap;
        
        #line default
        #line hidden
        
        
        #line 123 "..\..\..\queries\queryplanview.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Media.ImageBrush MiniMapBackground;
        
        #line default
        #line hidden
        
        
        #line 126 "..\..\..\queries\queryplanview.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Rectangle VisibleArea;
        
        #line default
        #line hidden
        
        
        #line 142 "..\..\..\queries\queryplanview.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Slider zoomSlider;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/EFProf;component/queries/queryplanview.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\queries\queryplanview.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.rootContainer = ((System.Windows.Controls.ScrollViewer)(target));
            return;
            case 2:
            this.gridContainer = ((System.Windows.Controls.Grid)(target));
            
            #line 52 "..\..\..\queries\queryplanview.xaml"
            this.gridContainer.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.HandleMouseLeftButtonDown);
            
            #line default
            #line hidden
            
            #line 52 "..\..\..\queries\queryplanview.xaml"
            this.gridContainer.MouseMove += new System.Windows.Input.MouseEventHandler(this.HandleMouseMove);
            
            #line default
            #line hidden
            
            #line 53 "..\..\..\queries\queryplanview.xaml"
            this.gridContainer.MouseLeftButtonUp += new System.Windows.Input.MouseButtonEventHandler(this.HandleMouseLeftButtonUp);
            
            #line default
            #line hidden
            
            #line 53 "..\..\..\queries\queryplanview.xaml"
            this.gridContainer.MouseWheel += new System.Windows.Input.MouseWheelEventHandler(this.HandleMouseWheel);
            
            #line default
            #line hidden
            return;
            case 3:
            this.rootScale = ((System.Windows.Media.ScaleTransform)(target));
            return;
            case 4:
            this.connectionCanvas = ((System.Windows.Controls.Canvas)(target));
            return;
            case 5:
            this.root = ((System.Windows.Controls.ItemsControl)(target));
            return;
            case 6:
            this.ZoomPanel = ((System.Windows.Controls.Grid)(target));
            return;
            case 7:
            this.button = ((System.Windows.Controls.Button)(target));
            return;
            case 8:
            this.MiniMapContainer = ((System.Windows.Controls.Border)(target));
            return;
            case 9:
            this.MiniMap = ((System.Windows.Controls.Canvas)(target));
            
            #line 121 "..\..\..\queries\queryplanview.xaml"
            this.MiniMap.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.HandleMiniMapClick);
            
            #line default
            #line hidden
            return;
            case 10:
            this.MiniMapBackground = ((System.Windows.Media.ImageBrush)(target));
            return;
            case 11:
            this.VisibleArea = ((System.Windows.Shapes.Rectangle)(target));
            
            #line 125 "..\..\..\queries\queryplanview.xaml"
            this.VisibleArea.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.VisibleAreaMouseLeftButtonDown);
            
            #line default
            #line hidden
            
            #line 125 "..\..\..\queries\queryplanview.xaml"
            this.VisibleArea.MouseMove += new System.Windows.Input.MouseEventHandler(this.VisibleAreaMouseMove);
            
            #line default
            #line hidden
            
            #line 126 "..\..\..\queries\queryplanview.xaml"
            this.VisibleArea.MouseLeftButtonUp += new System.Windows.Input.MouseButtonEventHandler(this.VisibleAreaMouseLeftButtonUp);
            
            #line default
            #line hidden
            return;
            case 12:
            
            #line 138 "..\..\..\queries\queryplanview.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.HandleResetClick);
            
            #line default
            #line hidden
            return;
            case 13:
            this.zoomSlider = ((System.Windows.Controls.Slider)(target));
            
            #line 142 "..\..\..\queries\queryplanview.xaml"
            this.zoomSlider.ValueChanged += new System.Windows.RoutedPropertyChangedEventHandler<double>(this.SliderValueChanged);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

