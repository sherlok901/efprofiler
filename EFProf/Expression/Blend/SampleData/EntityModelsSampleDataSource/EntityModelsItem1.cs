﻿// Decompiled with JetBrains decompiler
// Type: Expression.Blend.SampleData.EntityModelsSampleDataSource.EntityModelsItem1
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System.ComponentModel;

namespace Expression.Blend.SampleData.EntityModelsSampleDataSource
{
  public class EntityModelsItem1 : INotifyPropertyChanged
  {
    private Identifiers1 _Identifiers = new Identifiers1();
    private string _DisplayName = string.Empty;

    public event PropertyChangedEventHandler PropertyChanged;

    protected virtual void OnPropertyChanged(string propertyName)
    {
      if (this.PropertyChanged == null)
        return;
      this.PropertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }

    public Identifiers1 Identifiers
    {
      get
      {
        return this._Identifiers;
      }
    }

    public string DisplayName
    {
      get
      {
        return this._DisplayName;
      }
      set
      {
        if (!(this._DisplayName != value))
          return;
        this._DisplayName = value;
        this.OnPropertyChanged(nameof (DisplayName));
      }
    }
  }
}
