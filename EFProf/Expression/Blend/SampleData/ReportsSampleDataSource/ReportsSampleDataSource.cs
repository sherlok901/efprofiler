﻿// Decompiled with JetBrains decompiler
// Type: Expression.Blend.SampleData.ReportsSampleDataSource.ReportsSampleDataSource
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System;
using System.ComponentModel;
using System.Windows;

namespace Expression.Blend.SampleData.ReportsSampleDataSource
{
  public class ReportsSampleDataSource : INotifyPropertyChanged
  {
    private Items _Items = new Items();

    public event PropertyChangedEventHandler PropertyChanged;

    protected virtual void OnPropertyChanged(string propertyName)
    {
      if (this.PropertyChanged == null)
        return;
      this.PropertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }

    public ReportsSampleDataSource()
    {
      try
      {
        Uri uri = new Uri("/HibernatingRhinos.Profiler.Client;component/SampleData/ReportsSampleDataSource/ReportsSampleDataSource.xaml", UriKind.Relative);
        if (Application.GetResourceStream(uri) == null)
          return;
        Application.LoadComponent((object) this, uri);
      }
      catch (Exception ex)
      {
      }
    }

    public Items Items
    {
      get
      {
        return this._Items;
      }
    }
  }
}
