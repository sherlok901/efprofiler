﻿// Decompiled with JetBrains decompiler
// Type: Expression.Blend.SampleData.SampleDataSource.SampleDataSource
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System;
using System.ComponentModel;
using System.Windows;

namespace Expression.Blend.SampleData.SampleDataSource
{
  public class SampleDataSource : INotifyPropertyChanged
  {
    private Items _Items = new Items();
    private ActiveItem _ActiveItem = new ActiveItem();

    public event PropertyChangedEventHandler PropertyChanged;

    protected virtual void OnPropertyChanged(string propertyName)
    {
      if (this.PropertyChanged == null)
        return;
      this.PropertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }

    public SampleDataSource()
    {
      try
      {
        Uri uri = new Uri("/HibernatingRhinos.Profiler.Client;component/SampleData/SampleDataSource/SampleDataSource.xaml", UriKind.Relative);
        if (Application.GetResourceStream(uri) == null)
          return;
        Application.LoadComponent((object) this, uri);
      }
      catch (Exception ex)
      {
      }
    }

    public Items Items
    {
      get
      {
        return this._Items;
      }
    }

    public ActiveItem ActiveItem
    {
      get
      {
        return this._ActiveItem;
      }
      set
      {
        if (this._ActiveItem == value)
          return;
        this._ActiveItem = value;
        this.OnPropertyChanged(nameof (ActiveItem));
      }
    }
  }
}
