﻿// Decompiled with JetBrains decompiler
// Type: Expression.Blend.SampleData.SessionListSampleDataSource.Duration
// Assembly: EFProf, Version=3.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: E8E280FB-3ABD-40BC-8ABE-134CAE290863
// Assembly location: C:\Users\Igor\Desktop\EntityFramework.Profiler-v3.0-Build-3091\EFProf.exe

using System.ComponentModel;

namespace Expression.Blend.SampleData.SessionListSampleDataSource
{
  public class Duration : INotifyPropertyChanged
  {
    private double _TotalSeconds;

    public event PropertyChangedEventHandler PropertyChanged;

    protected virtual void OnPropertyChanged(string propertyName)
    {
      if (this.PropertyChanged == null)
        return;
      this.PropertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }

    public double TotalSeconds
    {
      get
      {
        return this._TotalSeconds;
      }
      set
      {
        if (this._TotalSeconds == value)
          return;
        this._TotalSeconds = value;
        this.OnPropertyChanged(nameof (TotalSeconds));
      }
    }
  }
}
